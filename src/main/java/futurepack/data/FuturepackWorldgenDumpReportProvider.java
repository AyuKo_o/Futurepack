package futurepack.data;

import java.nio.file.Path;
import java.util.OptionalLong;
import java.util.Map.Entry;
import java.util.function.Function;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.gson.JsonElement;
import com.mojang.datafixers.util.Pair;
import com.mojang.serialization.DynamicOps;
import com.mojang.serialization.JsonOps;
import com.mojang.serialization.Lifecycle;

import futurepack.api.Constants;
import net.minecraft.core.Holder;
import net.minecraft.core.MappedRegistry;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.WritableRegistry;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.HashCache;
import net.minecraft.data.info.WorldgenRegistryDumpReport;
import net.minecraft.resources.RegistryOps;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.biome.Climate;
import net.minecraft.world.level.biome.MultiNoiseBiomeSource;
import net.minecraft.world.level.biome.OverworldBiomeBuilder;
import net.minecraft.world.level.biome.TheEndBiomeSource;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.dimension.LevelStem;
import net.minecraft.world.level.levelgen.NoiseBasedChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.WorldGenSettings;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.synth.NormalNoise;

public class FuturepackWorldgenDumpReportProvider extends WorldgenRegistryDumpReport
{

	public static final ResourceKey<LevelStem> MENELAUS = ResourceKey.create(Registry.LEVEL_STEM_REGISTRY, new ResourceLocation(Constants.MOD_ID, "menelaus"));
	public static final ResourceKey<LevelStem> TYROS = ResourceKey.create(Registry.LEVEL_STEM_REGISTRY, new ResourceLocation(Constants.MOD_ID, "tyros"));

	public FuturepackWorldgenDumpReportProvider(DataGenerator p_194679_)
	{
		super(p_194679_);
	}

	@Override
	public void run(HashCache p_194682_)
	{
		Path path = this.generator.getOutputFolder();
		RegistryAccess registryaccess = RegistryAccess.BUILTIN.get();
		int i = 0;
		Registry<LevelStem> registry = defaultDimensions(registryaccess, 0L, false);

		DynamicOps<JsonElement> dynamicops = RegistryOps.create(JsonOps.INSTANCE, registryaccess);
		RegistryAccess.knownRegistries().forEach((p_194713_) -> {
			dumpRegistryCap(p_194682_, path, registryaccess, dynamicops, p_194713_);
		});
		dumpRegistry(path, p_194682_, dynamicops, Registry.LEVEL_STEM_REGISTRY, registry, LevelStem.CODEC);
	}

	public Registry<LevelStem> defaultDimensions(RegistryAccess registryAccess, long seed, boolean hugeBiomes)
	{
		WritableRegistry<LevelStem> writableregistry = new MappedRegistry<>(Registry.LEVEL_STEM_REGISTRY, Lifecycle.experimental(), (Function<LevelStem, Holder.Reference<LevelStem>>) null);
		MappedRegistry<DimensionType> registry = (MappedRegistry<DimensionType>) registryAccess.registryOrThrow(Registry.DIMENSION_TYPE_REGISTRY);

		registry.unfreeze();
		registerDimensionTypes(registry);
		registry.freeze();

		MappedRegistry<Biome> registry1 = (MappedRegistry<Biome>) registryAccess.registryOrThrow(Registry.BIOME_REGISTRY);
		registry1.unfreeze();
		registerBiomes(registry1);
		registry1.freeze();


		Registry<StructureSet> registry2 = registryAccess.registryOrThrow(Registry.STRUCTURE_SET_REGISTRY);
		Registry<NoiseGeneratorSettings> registry3 = registryAccess.registryOrThrow(Registry.NOISE_GENERATOR_SETTINGS_REGISTRY);
		Registry<NormalNoise.NoiseParameters> registry4 = registryAccess.registryOrThrow(Registry.NOISE_REGISTRY);
//		writableregistry
//				.register(LevelStem.NETHER,
//						new LevelStem(registry.getOrCreateHolder(DimensionType.NETHER_LOCATION), new NoiseBasedChunkGenerator(registry2, registry4,
//								MultiNoiseBiomeSource.Preset.NETHER.biomeSource(registry1, p_204496_), p_204495_, registry3.getOrCreateHolder(NoiseGeneratorSettings.NETHER))),
//						Lifecycle.stable());
//		writableregistry.register(LevelStem.END,
//				new LevelStem(registry.getOrCreateHolder(DimensionType.END_LOCATION),
//						new NoiseBasedChunkGenerator(registry2, registry4, new TheEndBiomeSource(registry1, p_204495_), p_204495_, registry3.getOrCreateHolder(NoiseGeneratorSettings.END))),
//				Lifecycle.stable());

		writableregistry.register(MENELAUS, new LevelStem(registry.getOrCreateHolder(FPWorldData.MENELAUS_LOCATION), FPWorldGenSettings.makeDefaultMenelaus(registryAccess, 0L, false)), Lifecycle.stable());
		writableregistry.register(TYROS, new LevelStem(registry.getOrCreateHolder(FPWorldData.TYROS_LOCATION), FPWorldGenSettings.makeDefaultTyros(registryAccess, 0L, false)), Lifecycle.stable());
		return writableregistry;
	}

	public void registerDimensionTypes(Registry<DimensionType> registry)
	{
		Registry.register(registry, FPWorldData.MENELAUS_LOCATION, FPWorldData.DEFAULT_MENELAUS);
		Registry.register(registry, FPWorldData.TYROS_LOCATION, FPWorldData.DEFAULT_TYROS);
	}

	public void registerBiomes(Registry<Biome> registry)
	{
		Biome desert = registry.get(Biomes.DESERT);

		//TODO actually also make biomes
		Registry.register(registry, FPWorldData.MENELAUS, Biome.BiomeBuilder.from(desert).build());
		Registry.register(registry, FPWorldData.MENELAUS_FLAT, Biome.BiomeBuilder.from(desert).build());
		Registry.register(registry, FPWorldData.MENELAUS_FOREST, Biome.BiomeBuilder.from(desert).build());
		Registry.register(registry, FPWorldData.MENELAUS_MUSHROOM, Biome.BiomeBuilder.from(desert).build());
		Registry.register(registry, FPWorldData.MENELAUS_PLATAU, Biome.BiomeBuilder.from(desert).build());
		Registry.register(registry, FPWorldData.MENELAUS_SEA, Biome.BiomeBuilder.from(desert).build());

		Biome jungle = registry.get(Biomes.JUNGLE);

		Registry.register(registry, FPWorldData.TYROS, Biome.BiomeBuilder.from(jungle).build());
		Registry.register(registry, FPWorldData.TYROS_MOUNTAIN, Biome.BiomeBuilder.from(jungle).build());
		Registry.register(registry, FPWorldData.TYROS_PALIRIE_FOREST, Biome.BiomeBuilder.from(jungle).build());
		Registry.register(registry, FPWorldData.TYROS_ROCK_DESERT, Biome.BiomeBuilder.from(jungle).build());
		Registry.register(registry, FPWorldData.TYROS_ROCKDESERT_FLAT, Biome.BiomeBuilder.from(jungle).build());
		Registry.register(registry, FPWorldData.TYROS_SWAMP, Biome.BiomeBuilder.from(jungle).build());
		Registry.register(registry, FPWorldData.TYROS_DARKFOREST, Biome.BiomeBuilder.from(jungle).build());

	}
}
