package futurepack.extensions.albedo;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.function.Consumer;

import com.google.common.base.Predicates;

public class LightList
{
	public static ArrayList<Object[]> list = new ArrayList<Object[]>();
	public static ArrayList<Object[]> staticLight = new ArrayList<Object[]>();
	
	
	public static void addLight(float x, float y, float z, float r, float g, float b, float a, float radius)
	{
		if(radius < 0.1)
			return;
		
		if(AlbedoMain.isAvailable)
			list.add(new Object[] {x,y,z,r,g,b,a,radius});
	}
	
	/**
	 * 
	 * @param x
	 * @param y
	 * @param z
	 * @param r
	 * @param g
	 * @param b
	 * @param a
	 * @param radius in blocks
	 * @param lifespan in seconds
	 */
	public static void addStaticLight(float x, float y, float z, float r, float g, float b, float a, float radius, float lifespan)
	{
		addStaticLightWithFate(x, y, z, r, g, b, a, radius, lifespan, true);
	}
	
	public static void addStaticLightWithFate(float x, float y, float z, float r, float g, float b, float a, float radius, float lifespan, boolean fate)
	{
		if(AlbedoMain.isAvailable)
			staticLight.add(new Object[] {x,y,z ,r,g,b, a,radius, lifespan, System.currentTimeMillis(), fate});
	}
	
	
	public static void addToList(Consumer<Object> addLights)
	{
		addStaticLights();
		list.stream().map(LightList::convert).filter(Predicates.notNull()).forEach(addLights);
		list.clear();
	}
	
	private static void addStaticLights()
	{
		Iterator<Object[]> iter = staticLight.iterator();
		while(iter.hasNext())
		{
			Object[] sl = iter.next();
			double passed  = System.currentTimeMillis() - (long)sl[9];			
			double maxLife = (float)sl[8]*1000D;
			double life = passed / maxLife;
			if(life>1)
			{
				iter.remove();
				continue;
			}
			float fate = 1F;
			if((boolean) sl[10])
				fate =  (float)( (Math.sin(Math.PI*life) ));
			list.add(new Object[] {sl[0], sl[1], sl[2], sl[3], sl[4], sl[5], sl[6], (float)sl[7]*fate});
		}
	}
	
	static Class<?> c_light = null;
	static Constructor<?> con_light = null;
	static
	{
		try
		{
			c_light = Class.forName("elucent.albedo.lighting.Light");
			con_light = c_light.getConstructor(float.class, float.class, float.class,  float.class, float.class, float.class, float.class, float.class);
		} catch (ClassNotFoundException | NoSuchMethodException | SecurityException e)
		{
			c_light = null;
			con_light = null;
		}
	}
	
	private static Object convert(Object[] lightData)
	{
		if(con_light!=null)
		{
			try {
				return con_light.newInstance(lightData);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		con_light = null;
		return null;
	}
}
