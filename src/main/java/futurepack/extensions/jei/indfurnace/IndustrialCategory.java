package futurepack.extensions.jei.indfurnace;

import java.util.ArrayList;
import java.util.List;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.api.ItemPredicateBase;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.recipes.industrialfurnace.IndRecipe;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableAnimated;
import mezz.jei.api.gui.drawable.IDrawableAnimated.StartDirection;
import mezz.jei.api.gui.drawable.IDrawableStatic;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class IndustrialCategory extends BaseRecipeCategory<IndRecipe>
{
	private IDrawableAnimated arrow;
	
	public IndustrialCategory(IGuiHelper gui)
	{
		super(gui, InventoryBlocks.industrial_furnace, FuturepackUids.INDUTRIALFURNACE, 116-27, 38-17);
	}	

	@Override
	public void draw(IndRecipe recipe, PoseStack matrixStack, double mouseX, double mouseY) 
	{
		arrow.draw(matrixStack, 113-27, 40-17);
		super.draw(recipe, matrixStack, mouseX, mouseY);
	}


	@Override
	public Class<? extends IndRecipe> getRecipeClass() 
	{
		return IndRecipe.class;
	}

	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, IndRecipe recipe, IFocusGroup focuses) 
	{
		List<List<ItemStack>> inputs = new ArrayList<List<ItemStack>>();
		for(ItemPredicateBase it : recipe.getInputs())
		{
			inputs.add(it.collectAcceptedItems(new ArrayList<>()));
		}
		while(inputs.size()<3)
		{
			inputs.add(new ArrayList<>());
		}
		builder.addSlot(RecipeIngredientRole.INPUT, 32-27+1, 46-17+1).addItemStacks(inputs.get(0));
		builder.addSlot(RecipeIngredientRole.INPUT, 61-27+1, 46-17+1).addItemStacks(inputs.get(1));
		builder.addSlot(RecipeIngredientRole.INPUT, 90-27+1, 46-17+1).addItemStacks(inputs.get(2));
		
		builder.addSlot(RecipeIngredientRole.OUTPUT, 151-27+1, 40-17+1).addItemStack(recipe.getOutput());
		
		
	}
	
//	@Override
//	public void setIngredients(IndRecipe rec, IIngredients ingredients) 
//	{
//		List<List<ItemStack>> inputs = new ArrayList<List<ItemStack>>();
//		for(ItemPredicateBase it : rec.getInputs())
//		{
//			inputs.add(it.collectAcceptedItems(new ArrayList<>()));
//		}
//		ingredients.setInputLists(VanillaTypes.ITEM, inputs);
//		
//		ingredients.setOutput(VanillaTypes.ITEM, rec.getOutput());
//	}
//
//	@Override
//	public void setRecipe(IRecipeLayout recipeLayout, IndRecipe recipe, IIngredients ingredients) 
//	{
//		IGuiItemStackGroup itemstacks = recipeLayout.getItemStacks();
//		
//		itemstacks.init(0, true, 32-27, 46-17);
//		itemstacks.init(1, true, 61-27, 46-17);
//		itemstacks.init(2, true, 90-27, 46-17);
//		
//		itemstacks.init(3, false, 151-27, 40-17);
//		
//		itemstacks.set(ingredients);
//	}

	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/industrieofen.png");
		IDrawable bg = gui.createDrawable(res, 27, 17, 142, 77);
		IDrawableStatic base = gui.createDrawable(res, 192, 0, 29, 18);
		arrow = gui.createAnimatedDrawable(base, 400, StartDirection.LEFT, false);
		return bg;
	}

	@Override
	public boolean isResearched(IndRecipe rec) 
	{
		return rec.isLocalResearched();
	}

}
