package futurepack.extensions.jei.watercooler;

import java.nio.charset.StandardCharsets;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.inventory.WaterCoolerManager.CoolingEntry;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.forge.ForgeTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.client.Minecraft;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.fluids.FluidStack;

public class WaterCoolerCategory extends BaseRecipeCategory<CoolingEntry>
{
	public WaterCoolerCategory(IGuiHelper gui)
	{
		super(gui, InventoryBlocks.water_cooler, FuturepackUids.WATERCOOLER, 0,0);
	}

	@Override
	public Class<? extends CoolingEntry> getRecipeClass() 
	{
		return CoolingEntry.class;
	}

	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, CoolingEntry recipe, IFocusGroup focuses) 
	{
		FluidStack input = new FluidStack(recipe.input, (int)(recipe.usedAmount * 200.0F));
		FluidStack output = new FluidStack(recipe.output, (int)(recipe.usedAmount * 200.0F));
		builder.addSlot(RecipeIngredientRole.INPUT, 10+1, 23+1).setFluidRenderer(200, false, 16, 52).addIngredient(ForgeTypes.FLUID_STACK, input);
		builder.addSlot(RecipeIngredientRole.OUTPUT, 82+1, 23+1).setFluidRenderer(200, false, 16, 52).addIngredient(ForgeTypes.FLUID_STACK, output);
	}
	
//	@Override
//	public void setIngredients(CoolingEntry rec, IIngredients ingredients) 
//	{
//		FluidStack input = new FluidStack(rec.input, (int)(rec.usedAmount * 200.0F));
//		ingredients.setInput(VanillaTypes.FLUID, input);
//		
//		FluidStack outpit = new FluidStack(rec.output,  (int)(rec.usedAmountOutput * 200.0F));
//		ingredients.setOutput(VanillaTypes.FLUID, outpit);
//	}
//
//	@Override
//	public void setRecipe(IRecipeLayout recipeLayout, CoolingEntry recipe, IIngredients ingredients) {
//		IGuiFluidStackGroup guiFluidStack = recipeLayout.getFluidStacks();
//
//		guiFluidStack.init(0, true, 10, 23, 16, 52, 5000, true, null);
//		guiFluidStack.init(1, false, 82, 23, 16, 52, 1000, true, null);
//		
//		
//		guiFluidStack.set(ingredients);
//	}

	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/water_cooler.png");
		return gui.createDrawable(res, 34, 5, 112, 85);
	}

	@Override
	public boolean isResearched(CoolingEntry rec) 
	{
		return true;
	}
	
	@SuppressWarnings("resource")
	@Override
	public void draw(CoolingEntry rec, PoseStack matrixStack, double mouseX, double mouseY) 
	{
		String s = new String(("min "+ rec.targetTemp + "°C").getBytes(), StandardCharsets.UTF_8);
		
		int w = Minecraft.getInstance().font.width(s) / 2;
		Minecraft.getInstance().font.draw(matrixStack, s, 54 - w, 44, 0xff000000);
		super.draw(rec, matrixStack, mouseX, mouseY);
	}

}
