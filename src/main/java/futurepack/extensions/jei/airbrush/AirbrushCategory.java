package futurepack.extensions.jei.airbrush;

import futurepack.api.Constants;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.recipes.airbrush.AirbrushRecipeJEI;
import futurepack.extensions.jei.BaseRecipeCategory;
import futurepack.extensions.jei.FuturepackUids;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Blocks;

public class AirbrushCategory extends BaseRecipeCategory<AirbrushRecipeJEI>
{

	public AirbrushCategory(IGuiHelper gui)
	{
		super(gui, ToolItems.airBrush, FuturepackUids.AIRBRUSH, 0, 0);
	}

	@Override
	public Class<? extends AirbrushRecipeJEI> getRecipeClass() 
	{
		return AirbrushRecipeJEI.class;
	}
	
	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, AirbrushRecipeJEI recipe, IFocusGroup focuses) 
	{
		builder.addSlot(RecipeIngredientRole.INPUT, 100, 25-7).addItemStack(recipe.getInput());
		var input2 = builder.addSlot(RecipeIngredientRole.INPUT, 6, 49-7);
		input2.addItemStack(recipe.getColorItem());
		
		if(recipe.getColorItem()!=null && recipe.getColorItem().getItem()==Blocks.SAND.asItem())
		{
			input2.addItemStack(new ItemStack(Blocks.GRAVEL));
		}
		
		builder.addSlot(RecipeIngredientRole.OUTPUT, 160, 25-7).addItemStack(recipe.getOutput());
	}
	
	@Override
	protected IDrawable createBackground(IGuiHelper gui) 
	{
		ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/lackier_rezept.png");
		return gui.createDrawable(res, 0, 7, 185, 63);
	}

	@Override
	public boolean isResearched(AirbrushRecipeJEI rec) 
	{
		return true;
	}

}
