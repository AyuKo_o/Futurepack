package futurepack.extensions.jei.crafting;

import java.util.List;

import javax.annotation.Nullable;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.recipes.crafting.ShapedRecipeWithResearch;
import futurepack.extensions.jei.BaseRecipeCategory;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.ingredient.ICraftingGridHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.category.extensions.vanilla.crafting.ICraftingCategoryExtension;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.common.util.Size2i;

public class ShapedResearchCraftingExtension<T extends ShapedRecipeWithResearch> implements ICraftingCategoryExtension
{
	protected final T recipe;

	private float scale = 21F/256F;
	private int x,y;

	public ShapedResearchCraftingExtension(T recipe)
	{
		this.recipe = recipe;
		x = 61;
		y = 16;
	}

	@Override
	public void setRecipe(IRecipeLayoutBuilder builder, ICraftingGridHelper craftingGridHelper, IFocusGroup focuses) {
		List<List<ItemStack>> inputs = recipe.getIngredients().stream()
			.map(ingredient -> List.of(ingredient.getItems()))
			.toList();
		ItemStack resultItem = recipe.getResultItem();

		int width = getWidth();
		int height = getHeight();
		craftingGridHelper.setOutputs(builder, VanillaTypes.ITEM_STACK, List.of(resultItem));
		craftingGridHelper.setInputs(builder, VanillaTypes.ITEM_STACK, inputs, width, height);
	}

	@Nullable
	@Override
	public ResourceLocation getRegistryName()
	{
		return recipe.getId();
	}

	@Nullable
	@Override
	public Size2i getSize()
	{
		return new Size2i(recipe.getRecipeWidth(), recipe.getRecipeHeight());
	}

	@Override
	public void drawInfo(int recipeWidth, int recipeHeight, PoseStack matrixStack, double mouseX, double mouseY)
	{
		if(!isResearched())
		{
			matrixStack.pushPose();
			matrixStack.scale(scale, scale, 1);
			RenderSystem.setShaderColor(1f, 1f, 1f, 1f);
			BaseRecipeCategory.blockedIcon.draw(matrixStack, (int)(x/scale), (int)(y/scale));
			matrixStack.popPose();
		}
	}

	public boolean isResearched()
	{
		return recipe.isLocalResearched();
	}

}
