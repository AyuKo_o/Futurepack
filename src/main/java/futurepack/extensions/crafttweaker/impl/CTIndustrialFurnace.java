package futurepack.extensions.crafttweaker.impl;

import org.openzen.zencode.java.ZenCodeType;

import com.blamejared.crafttweaker.api.CraftTweakerAPI;
import com.blamejared.crafttweaker.api.annotation.ZenRegister;
import com.blamejared.crafttweaker.api.ingredient.IIngredient;
import com.blamejared.crafttweaker.api.item.IItemStack;

import futurepack.common.recipes.industrialfurnace.FPIndustrialFurnaceManager;
import futurepack.common.recipes.industrialfurnace.IndRecipe;
import futurepack.extensions.crafttweaker.CrafttweakerExtension;
import futurepack.extensions.crafttweaker.RecipeActionBase;

@ZenRegister
@ZenCodeType.Name("mods.futurepack.industrial")
public class CTIndustrialFurnace
{
	static class IndRecipeAddAction extends RecipeActionBase<IndRecipe>
	{
		private IndRecipe r;

		public IndRecipeAddAction(String id, IItemStack output, IIngredient[] input)
		{
			super(Type.ADD, () -> FPIndustrialFurnaceManager.instance);

			r = new IndRecipe(id, output.getInternal(), CrafttweakerExtension.convert(input));
		}

		@Override
		public IndRecipe createRecipe()
		{
			return r;
		}

	}

	static class IndRecipeRemoveAction extends RecipeActionBase<IndRecipe>
	{
		private IItemStack[] input;

		public IndRecipeRemoveAction(IItemStack[] input)
		{
			super(Type.REMOVE, () -> FPIndustrialFurnaceManager.instance);

			this.input = input;
		}

		@Override
		public IndRecipe createRecipe()
		{
			return ((FPIndustrialFurnaceManager)recipeManager.get()).getMatchingRecipes(CrafttweakerExtension.convert(input), null)[0];
		}

	}

	@ZenCodeType.Method
	public static void add(String id, IItemStack output, IIngredient[] input)
	{
		CraftTweakerAPI.apply(new IndRecipeAddAction(id, output, input));
	}

	@ZenCodeType.Method
	public static void remove(IItemStack[] input)
	{
		CraftTweakerAPI.apply(new IndRecipeRemoveAction(input));
	}

}
