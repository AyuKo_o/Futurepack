package futurepack.extensions.crafttweaker.impl;

import org.openzen.zencode.java.ZenCodeType;

import com.blamejared.crafttweaker.api.annotation.ZenRegister;
import com.blamejared.crafttweaker.api.item.IItemStack;
import com.blamejared.crafttweaker_annotations.annotations.Document;

import futurepack.common.research.ResearchPage;
import futurepack.common.research.ResearchPage.EnumPageVisibility;
import net.minecraft.world.item.ItemStack;

@ZenRegister
@ZenCodeType.Name("mods.futurepack.ResearchPageWrapper")
@Document(value="futurepack/extensions/crafttweaker/impl/ResearchPageWrapper")
public class ResearchPageWrapper
{
	final ResearchPage p;

	public ResearchPageWrapper(ResearchPage p)
	{
		this.p = p;
	}

	@ZenCodeType.Method
	public void setIcon(IItemStack it)
	{
		p.setIcon(it.getInternal());
	}
	@ZenCodeType.Method
	public void setHidden(boolean v)
	{
		p.setVisiblity(v? EnumPageVisibility.HIDDEN : EnumPageVisibility.ALWAYS);
	}
	
	@ZenCodeType.Method
	public boolean isHidden()
	{
		return p.getVisibility() == EnumPageVisibility.HIDDEN;
	}

	@ZenCodeType.Method
	public String getTranslationKey()
	{
		return p.getTranslationKey();
	}
	@ZenCodeType.Method
	public String getLocalizedName()
	{
		return p.getLocalizedName();
	}
	@ZenCodeType.Method
	public String getBackground()
	{
		return p.getBackground().toString();
	}
	@ZenCodeType.Method
	public ResearchWrapper[] getEntries()
	{
		return p.getEntries().stream().map(ResearchWrapper::new).toArray(ResearchWrapper[]::new);
	}

	@ZenCodeType.Method
	public int getWidth()
	{
		return p.getWidth();
	}
	@ZenCodeType.Method
	public int getHeight()
	{
		return p.getHeight();
	}

	@ZenCodeType.Method
	public String toString()
	{
		return p.toString();
	}
	
	

}
