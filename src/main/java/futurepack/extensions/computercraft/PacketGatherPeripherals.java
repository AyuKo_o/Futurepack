package futurepack.extensions.computercraft;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dan200.computercraft.api.lua.IArguments;
import dan200.computercraft.api.lua.ILuaContext;
import dan200.computercraft.api.lua.LuaException;
import dan200.computercraft.api.lua.MethodResult;
import dan200.computercraft.api.peripheral.IComputerAccess;
import dan200.computercraft.api.peripheral.IPeripheral;
import dan200.computercraft.core.apis.PeripheralAPI;
import dan200.computercraft.core.asm.PeripheralMethod;
import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.common.util.NonNullConsumer;

public class PacketGatherPeripherals extends PacketBase
{

	private List<PeripheralWrapped> peripherals = new ArrayList<>();
	public final IComputerAccess computer;
	
	public PacketGatherPeripherals(BlockPos src, ITileNetwork net, IComputerAccess computer) 
	{
		super(src, net);
		this.computer = computer;
	}

	public List<PeripheralWrapped> getPeripherals() 
	{
		return peripherals;
	}
	
	@SuppressWarnings("unchecked")
	public PeripheralWrappedBuilder createBuilder(IPeripheral[] wrappedP, Direction d, BlockPos pos, ResourceLocation dimension) 
	{
		return new PeripheralWrappedBuilder(wrappedP, d, pos, dimension, new NonNullConsumer[] {null});
	}
	
	public record PeripheralWrappedBuilder(IPeripheral[] wrappedP, Direction direction, BlockPos pos, ResourceLocation dimension, NonNullConsumer<Object>[] invaldateCallback) implements NonNullConsumer<Object>
	{
		public PeripheralWrapped build(PacketGatherPeripherals pkt, LazyOptional<ITileNetwork> networkModem, IComputerAccess access)
		{
			LazyOptional<IPeripheral> optPeripheral = LazyOptional.of(() -> wrappedP[0]);
			invaldateCallback[0] = obj -> optPeripheral.invalidate();
			String name = String.format("[ %s %s %s %s]", pos.getX(), pos.getY(), pos.getZ(), dimension.toString());
			PeripheralWrapped pw = new PeripheralWrapped(optPeripheral, direction, pos, dimension, wrappedP[0], name, access, networkModem);
			
			pkt.peripherals.add(pw);
			
			return pw;
		}
		

		@Override
		public void accept(Object t) 
		{
			wrappedP[0] = null;
			if(invaldateCallback[0]!=null)
				invaldateCallback[0].accept(t);
		}
	}
	
	public record PeripheralWrapped(LazyOptional<IPeripheral> optPeripheral, Direction direction, BlockPos pos, ResourceLocation dimension, String type, Set<String> additionalTypes, Map<String, PeripheralMethod> methodMap, String name, DelegateComputerAccess computer)
	{

		
		public PeripheralWrapped(LazyOptional<IPeripheral> optPeripheral2, Direction direction2, BlockPos pos2, ResourceLocation dimension2, IPeripheral unwrapped, String name, IComputerAccess access,  LazyOptional<ITileNetwork> networkModem)
		{
			this(optPeripheral2, direction2, pos2, dimension2, unwrapped.getType(), unwrapped.getAdditionalTypes(), PeripheralAPI.getMethods(unwrapped), name, new DelegateComputerAccess(access, networkModem::isPresent));
		}

		public String getType() 
		{
			return type();
		}

		public Set<String> getAdditionalTypes() 
		{
			return additionalTypes();
		}

		public Collection<String> getMethodNames() 
		{
			return methodMap().keySet();
		}

		public MethodResult callMethod(ILuaContext context, String methodName, IArguments drop) throws LuaException
		{
			PeripheralMethod method = methodMap.get( methodName );
            if( method == null ) throw new LuaException( "No such method " + methodName );
            return method.apply( optPeripheral.orElseThrow(() -> new LuaException("Peripheral was invalid")), context, computer(), drop );
		}
		
	}

}
