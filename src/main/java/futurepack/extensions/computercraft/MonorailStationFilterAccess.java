package futurepack.extensions.computercraft;

import futurepack.common.block.logistic.monorail.TileEntityMonorailStation;
import net.minecraft.world.item.ItemStack;

public class MonorailStationFilterAccess extends FilterAccessBase<TileEntityMonorailStation>
{

	public MonorailStationFilterAccess(TileEntityMonorailStation block) 
	{
		super(block);
	}

	@Override
	public String[] getFilterGroups() 
	{
		return new String[]{"import", "export"};
	}

	@Override
	public int getSlots(String group) 
	{
		return super.filter.getContainerSize() / 2;
	}

	@Override
	public ItemStack getItem(String group, int slot) 
	{
		return filter.getItem(slot + ("export".equals(group) ? 5 : 0));
	}

	@Override
	public void setItem(String group, int slot, ItemStack stack) 
	{
		filter.setItem(slot + ("export".equals(group) ? 5 : 0) , stack);
	}

}
