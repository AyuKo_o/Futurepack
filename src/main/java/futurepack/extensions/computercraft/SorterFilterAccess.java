package futurepack.extensions.computercraft;

import futurepack.common.block.modification.machines.TileEntitySorter;
import futurepack.common.block.modification.machines.TileEntitySorter.EnumSortType;
import net.minecraft.world.item.ItemStack;

public class SorterFilterAccess extends FilterAccessBase<TileEntitySorter>
{
	private static final String[] groups = {"RED", "GREEN", "BLUE", "YELLOW"};
	
	public SorterFilterAccess(TileEntitySorter sorter) 
	{
		super(sorter);
	}

	@Override
	public String[] getFilterGroups() 
	{
		return groups;
	}

	@Override
	public int getSlots(String group) 
	{
		return EnumSortType.valueOf(group).getLenght();
	}

	@Override
	public ItemStack getItem(String group, int slot) 
	{
		return filter.getItem(EnumSortType.valueOf(group).getSlot() + slot);
	}

	@Override
	public void setItem(String group, int slot, ItemStack stack) 
	{
		filter.setItem(EnumSortType.valueOf(group).getSlot() + slot, stack);
	}

}
