package futurepack.extensions.computercraft;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import dan200.computercraft.api.lua.IArguments;
import dan200.computercraft.api.lua.ILuaContext;
import dan200.computercraft.api.lua.LuaException;
import dan200.computercraft.api.lua.LuaFunction;
import dan200.computercraft.api.lua.MethodResult;
import dan200.computercraft.api.peripheral.IComputerAccess;
import dan200.computercraft.api.peripheral.IPeripheral;
import dan200.computercraft.shared.Peripherals;
import dan200.computercraft.shared.util.LuaUtil;
import futurepack.api.FacingUtil;
import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.block.TileEntityNetworkMaschine;
import futurepack.common.network.NetworkManager;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.extensions.computercraft.PacketGatherPeripherals.PeripheralWrapped;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityCCModem extends TileEntityNetworkMaschine implements INetworkUser
{
	private LazyOptional<ModemMethods> opt = null;
	private LazyOptional<ITileNetwork> optTile = null;

	public TileEntityCCModem(BlockEntityType<? extends TileEntityCCModem> tileEntityTypeIn, BlockPos pos, BlockState state)
	{
		super(tileEntityTypeIn, pos, state);
	}
	
	public TileEntityCCModem(BlockPos pos, BlockState state)
	{
		this((BlockEntityType)ComputerCraftIntegration.NETWORK_TILEENTITY_TYPE.get(), pos, state);
	}
	
	@Override
	public void invalidateCaps() 
	{
		HelperEnergyTransfer.invalidateCaps(opt, optTile);
		super.invalidateCaps();
	}

	@Override
	public void onFunkPacket(PacketBase pkt) 
	{
		if(pkt instanceof PacketGatherPeripherals peri)
		{
			
			for(Direction d : FacingUtil.VALUES)
			{
				IPeripheral[] wrappedP = new IPeripheral[] {null};
				BlockPos pos = this.worldPosition.relative(d);
				
				if(level.getBlockState(pos).getBlock() != ComputerCraftIntegration.NETWORK_MODEM.get())
				{
					var wrapper = peri.createBuilder(wrappedP, d, pos, level.dimension().location());
					wrappedP[0] = NetworkManager.supplyAsync(level, () -> Peripherals.getPeripheral(getLevel(), pos, d, wrapper));
					if(wrappedP[0]!=null)
					{
						if(optTile==null)
						{
							optTile = LazyOptional.of(() -> this);
						}
						wrapper.build(peri, optTile, peri.computer);
					}
				}
			}
		}
		super.onFunkPacket(pkt);
	}

	@Override
	public Level getSenderWorld() {
		return level;
	}

	@Override
	public BlockPos getSenderPosition() {
		return worldPosition;
	}

	@Override
	public void postPacketSend(PacketBase pkt) 
	{
		if(pkt instanceof PacketGatherPeripherals peri)
		{
			
		}
	}
	
	private ConcurrentMap<String, PacketGatherPeripherals.PeripheralWrapped> peripherals;
	private long time;
	
	public synchronized ConcurrentMap<String, PacketGatherPeripherals.PeripheralWrapped> searchPeripherals(IComputerAccess computer) throws LuaException
	{
		if(peripherals!=null)
		{
			if(level.getGameTime() - time < 20*10) //every 10s refresh to check if block of the network get removed
			{
				return peripherals;
			}
		}
		
		PacketGatherPeripherals peri = new PacketGatherPeripherals(worldPosition, this, computer);
		try 
		{
			NetworkManager.sendPacketAndWait(this, peri);
		}
		catch (InterruptedException e)
		{
			throw new LuaException(e.toString());
		}
		
		peripherals = new ConcurrentHashMap<>();
		
		time = level.getGameTime();
		var list = peri.getPeripherals();
		list.forEach(w -> {
			w.optPeripheral().addListener(o -> peripherals = null);
			peripherals.put(w.name(), w);
		});
		return peripherals;
	}
	
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) 
	{
		if(cap == ComputerCraftIntegration.CAPABILITY_PERIPHERAL)
		{
			if(opt!=null)
				return opt.cast();
			else
			{
				opt = LazyOptional.of(() -> new ModemMethods());
				opt.addListener(o -> opt = null);
				return opt.cast();
			}
		}
		return super.getCapability(cap, side);
	}
	
	public class ModemMethods implements IPeripheral
	{

		@Override
		public String getType() 
		{
			return "modem";
		}
		
		@Override
		public void attach(IComputerAccess computer) {
			// TODO Auto-generated method stub
			IPeripheral.super.attach(computer);
		}
		
		@Override
		public void detach(IComputerAccess computer) {
			// TODO Auto-generated method stub
			IPeripheral.super.detach(computer);
		}

		@Override
		public boolean equals(IPeripheral other) 
		{
			return this == other || (other!=null && getTarget() == other.getTarget());
		}  

		
		@Override
		public Object getTarget() 
		{
			return TileEntityCCModem.this;
		}

		@LuaFunction()
		public boolean isWireless()
		{
			return false;
		}
		
		private ConcurrentMap<String, PeripheralWrapped> getWrappers(IComputerAccess computer) throws LuaException 
		{
			return searchPeripherals(computer);
		}
		
		@LuaFunction
	    public final boolean isPresentRemote( IComputerAccess computer, String name ) throws LuaException 
	    {
	        return getWrapper( computer, name ) != null;
	    }
		
		private PeripheralWrapped getWrapper(IComputerAccess computer, String name)  throws LuaException 
		{
			ConcurrentMap<String, PeripheralWrapped> wrappers = getWrappers( computer );
			return wrappers == null ? null : wrappers.get( name );
		}

		@LuaFunction
	    public final Collection<String> getNamesRemote( IComputerAccess computer ) throws LuaException 
	    {
	        return getWrappers( computer ).keySet();
	    }

		@LuaFunction
		public final Object[] getTypeRemote( IComputerAccess computer, String name ) throws LuaException 
		{
			PeripheralWrapped wrapper = getWrapper( computer, name );
			return wrapper == null ? null : LuaUtil.consArray( wrapper.getType(), wrapper.getAdditionalTypes() );
		}
		 
		@LuaFunction
		public final Object[] hasTypeRemote( IComputerAccess computer, String name, String type ) throws LuaException 
		{
			PeripheralWrapped wrapper = getWrapper( computer, name );
			return wrapper == null ? null : new Object[] { wrapper.getType().equals( type ) || wrapper.getAdditionalTypes().contains( type ) };
		}

		 
		@LuaFunction
		public final Object[] getMethodsRemote( IComputerAccess computer, String name ) throws LuaException 
		{
			PeripheralWrapped wrapper = getWrapper( computer, name );
			if( wrapper == null ) return null;

			return new Object[] { wrapper.getMethodNames() };
		}

		@LuaFunction
		public final MethodResult callRemote( IComputerAccess computer, ILuaContext context, IArguments arguments ) throws LuaException
		{
			String remoteName = arguments.getString( 0 );
			String methodName = arguments.getString( 1 );
			PeripheralWrapped wrapper = getWrapper( computer, remoteName );
			if( wrapper == null ) throw new LuaException( "No peripheral: " + remoteName );

			return wrapper.callMethod( context, methodName, arguments.drop( 2 ) );
		}
		 

	}
	
}
