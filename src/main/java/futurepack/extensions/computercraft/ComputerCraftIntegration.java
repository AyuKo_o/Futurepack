package futurepack.extensions.computercraft;

import java.util.function.Function;

import dan200.computercraft.api.ComputerCraftAPI;
import dan200.computercraft.api.peripheral.IPeripheral;
import futurepack.api.Constants;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.interfaces.tilentity.ITileBoardComputer;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.inventory.TileEntityBoardComputer;
import futurepack.common.block.inventory.TileEntityPusher;
import futurepack.common.block.logistic.TileEntityInsertNode;
import futurepack.common.block.logistic.monorail.TileEntityMonorailStation;
import futurepack.common.block.modification.machines.TileEntitySorter;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ComputerCraftIntegration 
{
	
	public static final Capability<IFilterAccess> cap_FILTER = CapabilityManager.get(new CapabilityToken<>(){});
	public static final Capability<ITileBoardComputer> cap_BOARDCOMPUTER = CapabilityManager.get(new CapabilityToken<>(){});
	public static final Capability<IPeripheral> CAPABILITY_PERIPHERAL = CapabilityManager.get( new CapabilityToken<>() {} );
	
	public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, Constants.MOD_ID);
	public static final DeferredRegister<BlockEntityType<?>> TILE_ENTITY_TYPES = DeferredRegister.create(ForgeRegistries.BLOCK_ENTITIES, Constants.MOD_ID);
	//public static final RegistryObject<LootItemFunctionType> TEST_RECIPE_TYPE = LOOT_FUNCTION_TYPE.register("fp_setup_chip", () ->  new LootFunctionSetupChip.Storage());
	
	public static final RegistryObject<Block> NETWORK_MODEM = BLOCKS.register("network_modem", () ->  new BlockNetworkModem(InventoryBlocks.maschine_light_gray));
	public static final RegistryObject<BlockEntityType<?>> NETWORK_TILEENTITY_TYPE = TILE_ENTITY_TYPES.register("network_modem_type", () ->  BlockEntityType.Builder.of(TileEntityCCModem::new, NETWORK_MODEM.get()).build(null));
	
	
	
	public static void init()
	{
		IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();
		modBus.addListener(ComputerCraftIntegration::commonInit);
		modBus.addListener(ComputerCraftIntegration::registerCapabilities);
		
		BLOCKS.register(modBus);
		TILE_ENTITY_TYPES.register(modBus);
		
		MinecraftForge.EVENT_BUS.addGenericListener(BlockEntity.class, ComputerCraftIntegration::attachCapabilities);
	}
	
	public static void commonInit(FMLCommonSetupEvent event)
	{
		ComputerCraftAPI.registerGenericSource(new NeonMethods());
		ComputerCraftAPI.registerGenericSource(new SupportMethods());
		ComputerCraftAPI.registerGenericSource(new LogisticMethods());
		ComputerCraftAPI.registerGenericSource(new FilterAccessMethods());
		ComputerCraftAPI.registerGenericSource(new BoardcomputerMethods());
		
		event.enqueueWork(ComputerCraftIntegration::registerProviders);
	}
	
	public static void registerProviders()
    {
		ComputerCraftAPI.registerGenericCapability(CapabilityNeon.cap_NEON);
		ComputerCraftAPI.registerGenericCapability(CapabilitySupport.cap_SUPPORT);
		ComputerCraftAPI.registerGenericCapability(CapabilityLogistic.cap_LOGISTIC);
		ComputerCraftAPI.registerGenericCapability(cap_FILTER);
		ComputerCraftAPI.registerGenericCapability(cap_BOARDCOMPUTER);
    }
	
	public static void registerCapabilities(RegisterCapabilitiesEvent event)
	{
		event.register(IFilterAccess.class);
		event.register(ITileBoardComputer.class);
	}
	
	public static void attachCapabilities(AttachCapabilitiesEvent<BlockEntity> event)
	{
		if(event.getObject() instanceof TileEntitySorter)
		{
			FilterCapProvider<TileEntitySorter> prov = new FilterCapProvider<TileEntitySorter>((TileEntitySorter) event.getObject(), SorterFilterAccess::new);
			event.addCapability(new ResourceLocation(Constants.MOD_ID, "filter_access"), prov);
			event.addListener(prov::invalidate);
		}
		if(event.getObject() instanceof TileEntityPusher || event.getObject() instanceof TileEntityInsertNode)
		{
			FilterCapProvider<ITileInventoryProvider> prov = new FilterCapProvider<ITileInventoryProvider>((ITileInventoryProvider) event.getObject(), PusherFilterAccess::new);
			event.addCapability(new ResourceLocation(Constants.MOD_ID, "filter_access"), prov);
			event.addListener(prov::invalidate);
		}
		if(event.getObject() instanceof TileEntityMonorailStation)
		{
			FilterCapProvider<TileEntityMonorailStation> prov = new FilterCapProvider<TileEntityMonorailStation>((TileEntityMonorailStation) event.getObject(), MonorailStationFilterAccess::new);
			event.addCapability(new ResourceLocation(Constants.MOD_ID, "filter_access"), prov);
			event.addListener(prov::invalidate);
		}
		if(event.getObject() instanceof TileEntityBoardComputer)
		{
			BoardcomputerCapProvider prov = new BoardcomputerCapProvider((TileEntityBoardComputer) event.getObject());
			event.addCapability(new ResourceLocation(Constants.MOD_ID, "boardcomputer"), prov);
			event.addListener(prov::invalidate);
		}
	}
	
	private static class FilterCapProvider<T> implements ICapabilityProvider
	{
		private final T filter;
		private final Function<T, IFilterAccess> constructor;
		private LazyOptional<IFilterAccess> opt;

		public FilterCapProvider(T filter, Function<T, IFilterAccess> constructor) 
		{
			super();
			this.filter = filter;
			this.constructor = constructor;
			opt = LazyOptional.of(() -> constructor.apply(filter));
		}

		@Override
		public <R> LazyOptional<R> getCapability(Capability<R> cap, Direction side) 
		{
			if(cap == cap_FILTER)
			{
				if(!opt.isPresent())
				{
					opt = LazyOptional.of(() -> constructor.apply(filter));
				}
				return opt.cast();
			}
			return LazyOptional.empty();
		}
		
		public void invalidate()
		{
			opt.invalidate();
		}
		
	}
	
	private static class BoardcomputerCapProvider implements ICapabilityProvider
	{
		private final TileEntityBoardComputer comp;
		private LazyOptional<ITileBoardComputer> opt;

		public BoardcomputerCapProvider(TileEntityBoardComputer comp) 
		{
			super();
			this.comp = comp;
			opt = LazyOptional.of(() -> comp);
		}

		@Override
		public <R> LazyOptional<R> getCapability(Capability<R> cap, Direction side) 
		{
			if(cap == cap_BOARDCOMPUTER)
			{
				if(!opt.isPresent())
				{
					opt = LazyOptional.of(() -> comp);
				}
				return opt.cast();
			}
			return LazyOptional.empty();
		}
		
		public void invalidate()
		{
			opt.invalidate();
		}
		
	}

}
