package futurepack.world.gen.carver;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.chunk.CarvingMask;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.levelgen.Aquifer;
import net.minecraft.world.level.levelgen.carver.CarverConfiguration;
import net.minecraft.world.level.levelgen.carver.CarvingContext;
import net.minecraft.world.level.levelgen.carver.WorldCarver;

public abstract class WorldCaverMultiChunkStructures<C extends CarverConfiguration, T> extends WorldCarver<C> 
{
	protected WeakReference<Map<Integer, T>> ref;
	protected int maxRadius;

	public float chancePerChunk;
	public int minY, maxY;
	
	
	public WorldCaverMultiChunkStructures(Codec<C> codec) 
	{
		super(codec);
	}
	
	public static Random getRandomWithSeed(long worldSeed, long seed, int x, int z)
    {
        return new Random(worldSeed + x * x * 4987142 + x * 5947611 + z * z * 4392871L + z * 389711 ^ seed);
    }
	
	public static float jitter(Random r, float min, float max)
	{
		float d = max - min;
		return min + d * r.nextFloat();
	}
	
	public List<T> gatherStructures(BlockGetter w, int chunkX, int chunkZ, int radiusChunks, long worldSeed, Map<Integer, T> map)
	{
		ArrayList<T> list = new ArrayList<>(radiusChunks*radiusChunks);
		
		for(int k = chunkX - radiusChunks; k < chunkX+radiusChunks;k++)
		{
			for(int j = chunkZ - radiusChunks; j < chunkZ+radiusChunks;j++)
			{
				T c = getStructure(w, k, j, worldSeed, map);
				if(c!=null)
					list.add(c);
			}
		}
		return list;
	}
	
	public T getStructure(BlockGetter w, int chunkX, int chunkZ, long worldSeed, Map<Integer, T> map)
	{
		Integer i = 10000 * chunkX + chunkZ;
		
		if(map.containsKey(i))
		{
			return map.get(i);
		}
		else
		{
			Random r = getRandomWithSeed(worldSeed, 745679321L, chunkX, chunkZ);
			if(r.nextFloat() < chancePerChunk)
			{
				T t = getStructureBase(w, chunkX, chunkZ, worldSeed, map, r);
				map.put(i, t);
				return t;
			}
			map.put(i, null);
			return null;
		}	
	}
	
	@Override
	public boolean carve(CarvingContext pContext, C pConfig, ChunkAccess pChunk, Function<BlockPos, Holder<Biome>> pBiomeAccessor, Random pRandom, Aquifer pAquifer, ChunkPos pChunkPos, CarvingMask pCarvingMask)
	{
		//public boolean carve(ChunkAccess region, Function<BlockPos, Biome> pos_to_biome, Random random, int seaLevel, int chunkX, int chunkZ, int originalX, int originalZ, BitSet mask, C config) 
		
		int r = maxRadius / 16 + 2;
		
		Map<Integer, T> map;
		synchronized (this)
		{
			if(ref==null || (map=ref.get()) == null)
			{
				map = Collections.synchronizedMap(new HashMap<>(4 * r*r));
				ref = new WeakReference<Map<Integer,T>>(map);
			}
		}
//		map = new Int2ObjectOpenHashMap<>(4 * r*r);
		boolean b = this.recursiveGenerate(pChunk, pChunkPos.x, pChunkPos.z, map, pCarvingMask, pConfig);
		map = null;
		
		
		return b;
	}

	protected abstract boolean recursiveGenerate(ChunkAccess w, int chunkX, int chunkZ, Map<Integer, T> map, CarvingMask mask, C config);
	
	public abstract T getStructureBase(BlockGetter w, int chunkX, int chunkZ, long worldSeed, Map<Integer, T> map, Random r);
	
}
