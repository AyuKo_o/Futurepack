package futurepack.world.gen.feature;

import java.util.Random;

import futurepack.common.block.misc.MiscBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.ticks.TickPriority;

public class TyrosTreeAsync extends Feature<NoneFeatureConfiguration>
{

	public TyrosTreeAsync() 
	{
		super(NoneFeatureConfiguration.CODEC);
	}

	public boolean place(WorldGenLevel w, ChunkGenerator generator, Random rand, BlockPos pos, NoneFeatureConfiguration config) 
	{
		w.setBlock(pos, MiscBlocks.tyros_tree_gen.defaultBlockState(), 3);
		w.scheduleTick(pos,  MiscBlocks.tyros_tree_gen, 20*3, TickPriority.LOW);
		
		return false;
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> pContext) 
	{
		return place(pContext.level(), pContext.chunkGenerator(), pContext.random(), pContext.origin(), pContext.config());
	}
}
