package futurepack.world.gen.feature;

import java.util.Random;
import java.util.Set;

import com.mojang.serialization.Codec;

import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.dim.structures.LoaderStructures;
import net.minecraft.core.BlockPos;
import net.minecraft.core.BlockPos.MutableBlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.LevelWriter;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.TreeFeature;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.material.Material;

//TODO: rewrite Palirie tree from scratch, they are hardcoded
public class PalirieTreeFeature extends Feature<NoneFeatureConfiguration>
{
	private static class PlacableBlock
	{
		final int x,y,z;
		final boolean isLog;
		final BlockState state;

		public PlacableBlock(int x, int y, int z, boolean isLog, BlockState state)
		{
			super();
			this.x = x;
			this.y = y;
			this.z = z;
			this.state = state;
			this.isLog = isLog;
		}
	}

	private static PlacableBlock[][] trees_rotated = null;

	public PalirieTreeFeature(Codec<NoneFeatureConfiguration> serialiser)
	{
		super(serialiser);
	}

	public static void createBlockArrays(BlockState leaves, BlockState log) {

		if(trees_rotated != null) {
			return;
		}

		trees_rotated = new PlacableBlock[4][];
		for(int i=0;i<4;i++)
			trees_rotated[i] = createBlockArray(leaves.getBlock(), log.getBlock(), i);
	}

	public static PlacableBlock[] createBlockArray(Block bleaves, Block blog, int rot)
	{
		BlockState leaves = bleaves.defaultBlockState();
		BlockState logX = blog.defaultBlockState().setValue(RotatedPillarBlock.AXIS, Direction.Axis.X);
		BlockState logY = blog.defaultBlockState().setValue(RotatedPillarBlock.AXIS, Direction.Axis.Y);
		BlockState logZ = blog.defaultBlockState().setValue(RotatedPillarBlock.AXIS, Direction.Axis.Z);

		int[][] raw = new int[][] {
				{0,3,8,1},
				{0,4,7,1},
				{0,4,8,1},
				{0,5,6,1},
				{0,5,7,1},
				{0,5,8,1},
				{0,6,6,1},
				{0,6,7,1},
				{0,6,8,1},
				{0,7,6,1},
				{0,7,7,2},
				{0,7,8,1},
				{0,8,6,1},
				{0,8,7,1},
				{0,8,8,1},
				{0,9,7,1},
				{1,4,6,1},
				{1,5,6,1},
				{1,5,7,1},
				{1,5,8,1},
				{1,6,6,1},
				{1,6,7,1},
				{1,6,8,1},
				{1,7,6,1},
				{1,7,7,2},
				{1,7,8,1},
				{1,8,6,1},
				{1,8,7,1},
				{1,8,8,1},
				{1,9,6,1},
				{1,9,7,1},
				{1,9,8,1},
				{1,9,9,1},
				{1,10,8,1},
				{1,10,9,1},
				{1,10,10,1},
				{1,10,11,1},
				{1,11,8,1},
				{1,11,9,1},
				{1,11,10,1},
				{1,11,11,1},
				{1,12,8,1},
				{1,12,9,1},
				{1,12,10,1},
				{1,12,11,1},
				{1,13,9,1},
				{1,13,10,1},
				{1,13,11,1},
				{1,14,9,1},
				{1,14,10,1},
				{1,14,11,1},
				{1,15,10,1},
				{1,15,11,1},
				{2,5,3,1},
				{2,5,4,1},
				{2,6,2,1},
				{2,6,3,1},
				{2,6,4,1},
				{2,6,6,1},
				{2,6,7,1},
				{2,7,2,1},
				{2,7,3,1},
				{2,7,4,1},
				{2,7,5,1},
				{2,7,6,1},
				{2,7,7,2},
				{2,7,8,1},
				{2,8,2,1},
				{2,8,4,1},
				{2,8,5,1},
				{2,8,6,1},
				{2,8,7,1},
				{2,8,8,1},
				{2,8,9,1},
				{2,9,2,1},
				{2,9,4,1},
				{2,9,7,1},
				{2,9,8,1},
				{2,9,9,1},
				{2,9,10,1},
				{2,10,2,1},
				{2,10,3,1},
				{2,10,4,1},
				{2,10,8,1},
				{2,10,9,1},
				{2,10,10,1},
				{2,11,3,1},
				{2,11,8,1},
				{2,11,9,1},
				{2,11,10,1},
				{2,11,11,1},
				{2,12,8,1},
				{2,12,9,1},
				{2,12,10,1},
				{2,12,11,1},
				{2,13,8,1},
				{2,13,9,1},
				{2,13,10,1},
				{2,13,11,1},
				{2,14,8,1},
				{2,14,9,1},
				{2,14,10,1},
				{2,14,11,1},
				{2,15,9,1},
				{2,15,10,1},
				{2,15,11,1},
				{2,16,10,1},
				{3,4,3,1},
				{3,4,4,1},
				{3,5,2,1},
				{3,5,3,1},
				{3,5,4,1},
				{3,6,2,1},
				{3,6,3,1},
				{3,6,4,1},
				{3,6,5,1},
				{3,6,6,1},
				{3,6,7,1},
				{3,6,8,1},
				{3,7,2,1},
				{3,7,3,1},
				{3,7,4,1},
				{3,7,5,1},
				{3,7,6,1},
				{3,7,7,3},
				{3,7,8,4},
				{3,7,9,1},
				{3,8,2,1},
				{3,8,3,1},
				{3,8,4,1},
				{3,8,5,1},
				{3,8,7,2},
				{3,8,9,1},
				{3,9,1,1},
				{3,9,2,1},
				{3,9,3,1},
				{3,9,4,1},
				{3,9,5,1},
				{3,9,7,1},
				{3,9,9,1},
				{3,9,11,1},
				{3,10,1,1},
				{3,10,2,1},
				{3,10,3,1},
				{3,10,4,1},
				{3,10,5,1},
				{3,10,9,1},
				{3,10,10,1},
				{3,10,11,1},
				{3,11,2,1},
				{3,11,3,1},
				{3,11,4,1},
				{3,11,8,1},
				{3,11,9,2},
				{3,11,10,4},
				{3,11,11,1},
				{3,12,3,1},
				{3,12,8,1},
				{3,12,9,1},
				{3,12,10,3},
				{3,12,11,1},
				{3,13,8,1},
				{3,13,9,1},
				{3,13,10,3},
				{3,13,11,1},
				{3,14,8,1},
				{3,14,9,1},
				{3,14,10,1},
				{3,14,11,1},
				{3,15,8,1},
				{3,15,9,1},
				{3,15,10,1},
				{3,15,11,1},
				{3,16,9,1},
				{3,16,10,1},
				{3,16,11,1},
				{4,4,3,1},
				{4,4,4,1},
				{4,5,2,1},
				{4,5,3,1},
				{4,5,4,1},
				{4,6,2,1},
				{4,6,3,1},
				{4,6,4,1},
				{4,7,2,1},
				{4,7,3,1},
				{4,7,4,1},
				{4,7,5,1},
				{4,8,2,1},
				{4,8,3,2},
				{4,8,4,1},
				{4,8,5,1},
				{4,8,7,2},
				{4,8,9,1},
				{4,8,10,1},
				{4,9,1,1},
				{4,9,2,1},
				{4,9,3,3},
				{4,9,4,1},
				{4,9,5,1},
				{4,9,7,1},
				{4,9,9,1},
				{4,9,10,1},
				{4,10,1,1},
				{4,10,2,1},
				{4,10,3,3},
				{4,10,4,1},
				{4,10,5,1},
				{4,10,8,2},
				{4,10,9,1},
				{4,10,10,1},
				{4,11,2,1},
				{4,11,3,3},
				{4,11,4,1},
				{4,11,8,3},
				{4,11,9,4},
				{4,11,10,1},
				{4,12,2,1},
				{4,12,3,1},
				{4,12,4,1},
				{4,12,8,1},
				{4,12,9,1},
				{4,12,10,1},
				{4,12,11,1},
				{4,13,8,1},
				{4,13,9,1},
				{4,13,10,1},
				{4,13,11,1},
				{4,14,8,1},
				{4,14,9,1},
				{4,14,10,1},
				{4,14,11,1},
				{4,15,9,1},
				{4,15,10,1},
				{4,15,11,1},
				{4,16,10,1},
				{5,0,6,3},
				{5,0,7,3},
				{5,6,2,1},
				{5,6,3,1},
				{5,7,2,1},
				{5,7,3,1},
				{5,7,4,1},
				{5,7,5,1},
				{5,8,2,1},
				{5,8,3,4},
				{5,8,4,4},
				{5,8,5,2},
				{5,8,7,2},
				{5,8,8,1},
				{5,8,9,1},
				{5,8,11,1},
				{5,9,2,1},
				{5,9,3,1},
				{5,9,4,1},
				{5,9,5,1},
				{5,9,8,1},
				{5,9,9,1},
				{5,9,11,1},
				{5,10,1,1},
				{5,10,2,1},
				{5,10,3,1},
				{5,10,4,1},
				{5,10,5,1},
				{5,10,7,2},
				{5,10,8,4},
				{5,10,11,1},
				{5,11,2,1},
				{5,11,3,1},
				{5,11,4,1},
				{5,11,10,1},
				{5,11,11,1},
				{5,12,2,1},
				{5,12,3,1},
				{5,12,4,1},
				{5,12,9,1},
				{5,12,10,1},
				{5,12,11,1},
				{5,13,2,1},
				{5,13,3,1},
				{5,13,4,1},
				{5,13,9,1},
				{5,13,10,1},
				{5,13,11,1},
				{5,14,2,1},
				{5,14,3,1},
				{5,14,4,1},
				{5,14,10,1},
				{5,14,11,1},
				{5,15,3,1},
				{6,0,5,2},
				{6,0,6,2},
				{6,0,7,2},
				{6,0,8,4},
				{6,0,9,3},
				{6,1,6,3},
				{6,1,7,4},
				{6,1,8,3},
				{6,2,6,3},
				{6,2,7,3},
				{6,2,8,3},
				{6,3,6,3},
				{6,3,7,3},
				{6,3,8,3},
				{6,4,6,3},
				{6,4,7,3},
				{6,5,6,3},
				{6,5,7,3},
				{6,5,11,1},
				{6,6,6,3},
				{6,6,7,3},
				{6,6,11,1},
				{6,7,3,1},
				{6,7,4,1},
				{6,7,5,1},
				{6,7,6,3},
				{6,7,7,3},
				{6,7,8,4},
				{6,7,9,4},
				{6,7,10,4},
				{6,7,11,1},
				{6,8,2,1},
				{6,8,3,1},
				{6,8,4,1},
				{6,8,5,4},
				{6,8,6,3},
				{6,8,7,2},
				{6,8,8,1},
				{6,8,9,1},
				{6,8,11,1},
				{6,9,2,1},
				{6,9,3,1},
				{6,9,4,1},
				{6,9,7,3},
				{6,10,2,1},
				{6,10,3,1},
				{6,10,4,1},
				{6,10,7,3},
				{6,10,8,1},
				{6,11,2,1},
				{6,11,3,1},
				{6,11,4,1},
				{6,11,7,1},
				{6,11,8,1},
				{6,12,2,1},
				{6,12,3,1},
				{6,12,4,1},
				{6,12,5,2},
				{6,12,7,2},
				{6,12,8,1},
				{6,13,2,1},
				{6,13,3,4},
				{6,13,4,4},
				{6,13,5,3},
				{6,13,7,1},
				{6,13,8,1},
				{6,14,2,1},
				{6,14,3,3},
				{6,14,4,1},
				{6,14,7,1},
				{6,15,2,1},
				{6,15,3,1},
				{6,15,4,1},
				{7,0,4,3},
				{7,0,5,2},
				{7,0,6,3},
				{7,0,7,3},
				{7,0,8,4},
				{7,0,9,4},
				{7,1,5,4},
				{7,1,6,3},
				{7,1,7,3},
				{7,1,8,2},
				{7,2,6,3},
				{7,2,7,3},
				{7,2,8,3},
				{7,3,6,3},
				{7,3,7,3},
				{7,3,8,3},
				{7,3,11,1},
				{7,3,12,1},
				{7,4,6,3},
				{7,4,7,3},
				{7,4,10,1},
				{7,4,11,1},
				{7,4,12,1},
				{7,5,6,3},
				{7,5,7,3},
				{7,5,10,1},
				{7,5,11,1},
				{7,5,12,1},
				{7,6,3,1},
				{7,6,6,3},
				{7,6,7,3},
				{7,6,10,1},
				{7,6,11,1},
				{7,6,12,1},
				{7,7,2,1},
				{7,7,3,1},
				{7,7,4,1},
				{7,7,5,1},
				{7,7,6,3},
				{7,7,7,3},
				{7,7,10,2},
				{7,7,11,4},
				{7,7,12,1},
				{7,8,2,1},
				{7,8,3,1},
				{7,8,4,1},
				{7,8,6,3},
				{7,8,7,3},
				{7,8,8,1},
				{7,8,10,1},
				{7,8,11,1},
				{7,8,12,1},
				{7,9,2,1},
				{7,9,3,1},
				{7,9,4,1},
				{7,9,6,3},
				{7,9,7,1},
				{7,9,8,1},
				{7,9,9,1},
				{7,9,11,1},
				{7,10,2,1},
				{7,10,3,1},
				{7,10,4,1},
				{7,10,5,4},
				{7,10,6,3},
				{7,10,7,1},
				{7,11,2,1},
				{7,11,3,1},
				{7,11,4,1},
				{7,11,6,3},
				{7,11,7,1},
				{7,12,2,1},
				{7,12,3,1},
				{7,12,4,1},
				{7,12,5,4},
				{7,12,6,3},
				{7,12,7,4},
				{7,12,8,1},
				{7,13,2,1},
				{7,13,3,1},
				{7,13,4,1},
				{7,13,6,1},
				{7,13,7,3},
				{7,13,8,1},
				{7,14,2,1},
				{7,14,3,1},
				{7,14,4,1},
				{7,14,6,1},
				{7,14,7,1},
				{7,14,8,1},
				{7,15,3,1},
				{8,0,4,3},
				{8,0,5,3},
				{8,0,6,3},
				{8,0,7,3},
				{8,0,8,4},
				{8,1,5,4},
				{8,1,6,3},
				{8,1,7,3},
				{8,1,11,1},
				{8,2,6,3},
				{8,2,7,3},
				{8,2,11,1},
				{8,3,6,3},
				{8,3,7,3},
				{8,3,10,1},
				{8,3,11,1},
				{8,4,10,1},
				{8,4,11,1},
				{8,4,12,1},
				{8,5,10,1},
				{8,5,11,1},
				{8,5,12,1},
				{8,6,10,1},
				{8,6,11,1},
				{8,6,12,1},
				{8,7,5,4},
				{8,7,6,2},
				{8,7,10,1},
				{8,7,11,2},
				{8,7,12,1},
				{8,8,5,1},
				{8,8,6,1},
				{8,8,7,2},
				{8,8,8,1},
				{8,8,10,1},
				{8,8,11,1},
				{8,8,12,1},
				{8,9,1,1},
				{8,9,2,1},
				{8,9,6,1},
				{8,9,7,1},
				{8,9,10,1},
				{8,9,11,1},
				{8,9,12,1},
				{8,10,1,1},
				{8,10,2,1},
				{8,10,3,1},
				{8,10,4,4},
				{8,10,5,2},
				{8,11,1,1},
				{8,11,2,1},
				{8,11,3,1},
				{8,11,4,3},
				{8,11,7,1},
				{8,11,8,1},
				{8,12,6,1},
				{8,12,7,1},
				{8,12,8,1},
				{8,13,6,1},
				{8,13,7,1},
				{8,13,8,1},
				{8,14,7,1},
				{9,0,5,3},
				{9,0,6,3},
				{9,1,7,2},
				{9,2,12,1},
				{9,3,12,1},
				{9,4,10,1},
				{9,4,11,1},
				{9,4,12,1},
				{9,5,10,1},
				{9,5,11,1},
				{9,5,12,1},
				{9,6,10,1},
				{9,6,11,1},
				{9,6,12,1},
				{9,7,5,2},
				{9,7,6,1},
				{9,7,8,1},
				{9,7,9,1},
				{9,7,10,1},
				{9,7,11,1},
				{9,7,12,1},
				{9,8,2,1},
				{9,8,5,1},
				{9,8,6,1},
				{9,8,7,2},
				{9,8,8,4},
				{9,8,9,1},
				{9,8,10,1},
				{9,8,11,1},
				{9,9,1,1},
				{9,9,2,1},
				{9,9,7,1},
				{9,9,8,1},
				{9,9,11,1},
				{9,10,1,1},
				{9,10,2,1},
				{9,10,3,1},
				{9,11,1,1},
				{9,11,2,4},
				{9,11,3,4},
				{9,11,4,2},
				{9,12,2,1},
				{9,12,3,1},
				{9,13,2,1},
				{10,0,7,3},
				{10,6,10,1},
				{10,7,2,1},
				{10,7,4,1},
				{10,7,5,2},
				{10,7,6,1},
				{10,7,8,1},
				{10,7,9,1},
				{10,7,10,1},
				{10,8,1,1},
				{10,8,2,1},
				{10,8,4,4},
				{10,8,5,3},
				{10,8,6,1},
				{10,8,7,1},
				{10,8,8,2},
				{10,8,9,4},
				{10,8,10,1},
				{10,9,0,1},
				{10,9,1,1},
				{10,9,2,1},
				{10,9,4,1},
				{10,9,5,1},
				{10,9,7,1},
				{10,9,8,1},
				{10,9,9,1},
				{10,9,10,1},
				{10,10,0,1},
				{10,10,1,1},
				{10,10,2,1},
				{10,10,3,1},
				{10,10,4,1},
				{10,10,7,1},
				{10,10,8,1},
				{10,10,9,1},
				{10,10,10,1},
				{10,11,0,1},
				{10,11,1,1},
				{10,11,2,2},
				{10,11,3,1},
				{10,11,4,1},
				{10,11,8,1},
				{10,11,9,1},
				{10,11,10,1},
				{10,12,1,1},
				{10,12,2,3},
				{10,12,3,1},
				{10,12,9,1},
				{10,13,1,1},
				{10,13,2,1},
				{11,6,10,1},
				{11,6,11,1},
				{11,7,4,1},
				{11,7,5,1},
				{11,7,6,1},
				{11,7,9,1},
				{11,7,10,1},
				{11,7,11,1},
				{11,8,1,1},
				{11,8,2,1},
				{11,8,4,1},
				{11,8,5,1},
				{11,8,6,1},
				{11,8,7,1},
				{11,8,8,1},
				{11,8,9,2},
				{11,8,10,1},
				{11,8,11,1},
				{11,9,1,1},
				{11,9,2,1},
				{11,9,3,1},
				{11,9,4,1},
				{11,9,6,1},
				{11,9,7,1},
				{11,9,8,1},
				{11,9,9,3},
				{11,9,10,1},
				{11,9,11,1},
				{11,10,1,1},
				{11,10,2,1},
				{11,10,3,1},
				{11,10,7,1},
				{11,10,8,1},
				{11,10,9,3},
				{11,10,10,1},
				{11,10,11,1},
				{11,11,1,1},
				{11,11,2,1},
				{11,11,3,1},
				{11,11,8,1},
				{11,11,9,1},
				{11,11,10,1},
				{11,12,1,1},
				{11,12,2,1},
				{11,12,9,1},
				{11,12,10,1},
				{11,13,2,1},
				{12,5,10,1},
				{12,6,8,1},
				{12,6,9,1},
				{12,6,10,1},
				{12,7,8,1},
				{12,7,9,1},
				{12,7,10,1},
				{12,8,5,1},
				{12,8,6,1},
				{12,8,8,1},
				{12,8,9,1},
				{12,9,7,1},
				{12,9,8,1},
				{12,9,9,1},
				{12,9,10,1},
				{12,10,7,1},
				{12,10,8,1},
				{12,10,9,1},
				{12,10,10,1},
				{12,11,8,1},
				{12,11,9,1},
				{12,11,10,1},
				{12,12,8,1},
				{12,12,9,1},
				{13,8,6,1},
				{13,9,9,1},
				{13,9,10,1},
				{13,10,8,1},
				{13,10,9,1},
				{13,10,10,1},
				{13,11,9,1},
				{13,11,10,1},
				{14,10,9,1}
		};
		int w = 14;
		int d = 12;
		for(int i=0;i<rot;i++)
		{
			raw = rotateAroundY(raw);
			int g = w;
			w = d;
			d = g;
		}


		PlacableBlock[] pb = new PlacableBlock[raw.length];
		int index = 0;
		for(int[] i : raw)
		{
			BlockState state;
			switch(i[3])
			{
			case 1:
				state = leaves;
				break;
			case 2:
				state = logX;
				break;
			case 3:
				state = logY;
				break;
			case 4:
				state = logZ;
				break;
			default:
				state = null;
				break;
			}
			pb[index] = new PlacableBlock(i[0] - w/2, i[1], i[2] - d/2, i[3]>1, state);
			index++;
		}

		return pb;
	}

	private static int[][] rotateAroundY(int[][] raw)
	{
		int w=0,h=0,d=0;
		for(int[] i : raw)
		{
			w = Math.max(w, i[0]);
			h = Math.max(h, i[1]);
			d = Math.max(d, i[2]);
		}

		int[][] output = new int[raw.length][4];

		for(int i=0;i<raw.length;i++)
		{
			int[] r = raw[i];
			output[i][0] = r[0];
			output[i][1] = r[1];
			output[i][2] = r[2];
			LoaderStructures.rotateAtY(output[i], w, h, d);
			if(r[3]==2)
				output[i][3] = 4;
			else if(r[3]==4)
				output[i][3] = 2;
			else
				output[i][3] = r[3];
		}

		return output;
	}

	private static boolean isValidGround(BlockState state)
	{
		return state.is(BlockTags.DIRT) || state.getBlock() == Blocks.GRASS_BLOCK || state.getMaterial() == Material.DIRT || state.getMaterial() == Material.PLANT;
	}

	//FIXME: use correct vanilla method here
	protected final void setLogState(Set<BlockPos> changedBlocks, LevelWriter worldIn, BlockPos pos, BlockState state, BoundingBox box)
	{
		this.setBlock(worldIn, pos, state);
//		box.expandTo(new MutableBoundingBox(pos, pos));
//		if (BlockTags.LOGS.contains(state.getBlock()))
//		{
//			changedBlocks.add(pos.toImmutable());
//		}
	}

	public boolean place(WorldGenLevel worldIn, ChunkGenerator generator, Random rand, BlockPos position, NoneFeatureConfiguration config)
	{

		createBlockArrays(TerrainBlocks.leaves_palirie.get().defaultBlockState(), TerrainBlocks.log_palirie.get().defaultBlockState());

		int height = 16;
		final int world_height = 128;
		boolean flag = true;
		position = worldIn.getHeightmapPos(Types.MOTION_BLOCKING_NO_LEAVES, position);



		if(!worldIn.isStateAtPosition(position.below(), PalirieTreeFeature::isValidGround))
		{
			return false;
		}
		if (position.getY() >= 1 && position.getY() + height + 1 <= world_height)
		{
			LoopY:
			for(int y = position.getY(); y <= position.getY() + 1 + height; ++y)
			{
				int radius = 1;
				if (y == position.getY()) {
					radius = 3;
				}
				if (y >= position.getY() + 4) {
					radius = 5;
				}

				BlockPos.MutableBlockPos blockpos$Mutable = new BlockPos.MutableBlockPos();

				LoopX:
				for(int x = position.getX() - radius; x <= position.getX() + radius && flag; ++x)
				{
					LoopZ:
					for(int z = position.getZ() - radius; z <= position.getZ() + radius && flag; ++z)
					{
						if (y >= 0 && y <world_height)
						{
							if (!canBeReplacedByLogs(worldIn, blockpos$Mutable.set(x, y, z))) //can grow into
							{
								flag = false;
								break LoopY;
							}
						}
						else
						{
							flag = false;
							break LoopY;
						}
					}
				}
			}

			if (!flag)
			{
				return false;
			}
			else
			{
				int rot = rand.nextInt(4);
				PlacableBlock[] blocks = trees_rotated[rot];

				BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos();
				for(PlacableBlock b : blocks)
				{
					pos.set(position.getX()+b.x, position.getY()+b.y, position.getZ()+b.z);
					if(b.isLog)
					{
						setLogState(null, worldIn, pos, b.state, null);
						if(b.y==0)
						{
							BlockState log = b.state.setValue(RotatedPillarBlock.AXIS, Direction.Axis.Y);
							while(isAir(worldIn, pos.move(0, -1, 0)) && pos.getY() > 30)
							{
								if(rand.nextInt(5) == 0)
								{
									break;
								}
								else
								{
									setLogState(null, worldIn, pos, log, null);
								}
							}
						}
					}
					else
					{
						setBlock(worldIn, pos, b.state);
					}
				}
				return true;
			}
		}
		return false;
	}

	private boolean canBeReplacedByLogs(WorldGenLevel worldIn, MutableBlockPos setPos)
	{
		return TreeFeature.validTreePos(worldIn, setPos);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> pContext)
	{
		return place(pContext.level(), pContext.chunkGenerator(), pContext.random(), pContext.origin(), pContext.config());
	}
}
