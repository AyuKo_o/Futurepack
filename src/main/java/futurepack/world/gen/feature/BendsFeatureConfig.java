package futurepack.world.gen.feature;

import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;

public class BendsFeatureConfig implements FeatureConfiguration 
{
	public static final Codec<BendsFeatureConfig> CODEC = RecordCodecBuilder.create((p) -> {
		return p.group(BlockState.CODEC.fieldOf("fillerblock").forGetter((c) -> {
			return c.fillerblock;
		}), BlockState.CODEC.fieldOf("topblock").forGetter((c) -> {
			return c.topblock;
		}), 
			Codec.FLOAT.fieldOf("min_a").forGetter(c -> c.min_a),
			Codec.FLOAT.fieldOf("max_a").forGetter(c -> c.max_a),
			Codec.FLOAT.fieldOf("min_height").forGetter(c -> c.min_height),
			Codec.FLOAT.fieldOf("max_height").forGetter(c -> c.max_height),
			Codec.FLOAT.fieldOf("chance_per_chunk").forGetter(c -> c.chance_per_chunk),
			Codec.FLOAT.fieldOf("min_thickness").forGetter(c -> c.min_thickness),
			Codec.FLOAT.fieldOf("max_thickness").forGetter(c -> c.max_thickness)
		).apply(p, BendsFeatureConfig::new);
	});
	
	public final BlockState fillerblock, topblock;

	public final float min_a, max_a, min_height, max_height, chance_per_chunk, min_thickness, max_thickness;
	
	//y = -a(x-c)^2 + b; a,b input; c = 
	// 0 = y, b = ax^2; b/a = x^2; x = sqrt(b/a) = c
	
	private final float max_width;
	
	public BendsFeatureConfig(BlockState fillerblock, BlockState topblock, float min_a, float max_a, float min_height, float max_height, float chance_per_chunk, float min_thickness, float max_thickness)
	{
		super();
		this.fillerblock = fillerblock;
		this.topblock = topblock;
		
		this.min_a = min_a;
		this.max_a = max_a;
		this.min_height = min_height;
		this.max_height = max_height;
		
		this.chance_per_chunk = chance_per_chunk;
		this.min_thickness = min_thickness;
		this.max_thickness = max_thickness;
		
		max_width = (float) Math.sqrt(max_height/min_a)* 2;
	}
	
	public float getMaxWidth()
	{
		return max_width;
	}
	
	public int getMaxWidthInChunk()
	{
		return (int)(max_width/16F) +1;
	}

}
