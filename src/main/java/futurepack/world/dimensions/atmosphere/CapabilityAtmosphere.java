package futurepack.world.dimensions.atmosphere;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.IntConsumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Predicates;

import futurepack.api.FacingUtil;
import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.api.interfaces.IPlanet;
import futurepack.common.spaceships.FPPlanetRegistry;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ChunkHolder;
import net.minecraft.server.level.ChunkMap;
import net.minecraft.server.level.ServerChunkCache;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraftforge.common.util.LazyOptional;

public class CapabilityAtmosphere implements IChunkAtmosphere
{
//	public static class Storage implements IStorage<IChunkAtmosphere>
//	{
//		@Override
//		public Tag writeNBT(Capability<IChunkAtmosphere> capability, IChunkAtmosphere instance, Direction side)
//		{
//			int[] air = new int[16 * 16 * 256];
//			boolean zero = true;
//			for(int x=0;x<16;x++)
//				for(int z=0;z<16;z++)
//					for(int y=0;y<256;y++)
//					{
//						air[x<<12|z<<8|y] = instance.getAirAt(x, y, z);
//						if(air[x<<12|z<<8|y] != 0)
//							zero = false;
//					}
//			if(zero)
//			{
//				return ByteTag.valueOf((byte)10);
//			}
//
//			return new IntArrayTag(air);
//		}
//
//		@Override
//		public void readNBT(Capability<IChunkAtmosphere> capability, IChunkAtmosphere instance, Direction side, Tag nbt)
//		{
//			if(nbt instanceof IntArrayTag)
//			{
//				int[] air = ((IntArrayTag)nbt).getAsIntArray();
//				for(int x=0;x<16;x++)
//					for(int z=0;z<16;z++)
//						for(int y=0;y<256;y++)
//							instance.setAir(x, y, z, air[x<<12|z<<8|y]);
//			}
//			else if(nbt instanceof ByteTag)
//			{
//				if(((ByteTag) nbt).getAsByte() == 10)
//				{
//					for(int x=0;x<16;x++)
//						for(int z=0;z<16;z++)
//							for(int y=0;y<256;y++)
//								instance.setAir(x, y, z, 0);
//				}
//			}
//		}
//	}

	private short count = 0;

	public final int maxCapacity;
	public final int heightSections;
	public final int minHeight;
	private short[][] air;

	private ChunkPos pos;

	public CapabilityAtmosphere(int minBuildHeight, int maxBuildHeight)
	{
		this(36000, minBuildHeight, maxBuildHeight);
	}

	public CapabilityAtmosphere(int capacity, int minBuildHeight, int maxBuildHeight)
	{
		this.maxCapacity = capacity;
		int h = maxBuildHeight - minBuildHeight;
		int sections = h / 16;
		heightSections = sections * 16 < h ? sections+1 : sections;
		minHeight = minBuildHeight;
		air = new short[heightSections][];
	}

	@Override
	public void setChunkPos(ChunkPos pos)
	{
		this.pos = pos;
	}

	@Override
	public ChunkPos getChunkPos()
	{
		return pos;
	}

	@Override
	public boolean needsUpdate(int section)
	{
		if(++count>1000)
		{
			count=0;
			optimize();
		}

		if(section==-1)
		{
			for(short[] air : this.air)
				if(air!=null)
					return true;
		}
		else
		{
			return this.air[section] != null;
		}
		return false;
	}

	private void optimize()
	{
		for(int j=0;j<air.length;j++)
		{
			short[] ints = air[j];
			if(ints!=null)
			{
				boolean zero = true;
				for(int i : ints)
				{
					if(i>0)
					{
						zero = false;
						break;
					}
				}
				if(zero)
				{
					air[j] = null;
				}
			}
		}
	}

	@Override
	public int getMaxAir()
	{
		return maxCapacity;
	}

	@Override
	public int getAirAt(int x, int y, int z)
	{
		return getAir(getIndex(x, y, z));
	}

	@Override
	public int addAir(int x, int y, int z, int amount)
	{
		int i = getIndex(x, y, z);
		int space = maxCapacity - getAir(i);
		if(space>=amount)
		{
			 setAir(i, getAir(i) + amount);
			return amount;
		}
		else
		{
			 setAir(i, maxCapacity);
			return space;
		}
	}

	@Override
	public int removeAir(int x, int y, int z, int amount)
	{
		int i = getIndex(x, y, z);
		if(getAir(i)>=amount)
		{
			 setAir(i, getAir(i) - amount);
			return amount;
		}
		else
		{
			int removed = getAir(i);
			setAir(i, 0);
			return removed;
		}
	}

	private Level level;

	@Override
	public int setAir(int x, int y, int z, int amount)
	{
		int i = getIndex(x, y, z);

		final boolean debug = true;

		if(w!=null)
			level = w;
		if(debug && amount > 0 && this.pos!=null && level!=null)
		{
			BlockPos pos = new BlockPos(this.pos.getMinBlockX() + x, y, this.pos.getMinBlockZ() + z);
			if(FullBlockCache.isFullBlock(pos, level))
			{
				return 0;
			}
		}

		amount = Math.min(maxCapacity, amount);
		setAir(i, amount);
		return amount;
	}

	@Override
	public int getHeightSections()
	{
		return heightSections;
	}

	private int getIndex(int x, int y, int z)
	{
		if(x<0 || y<minHeight || z<0)
			throw new IllegalArgumentException("Only positiv values are allowed");
		if(x>15)
			throw new IllegalArgumentException("x coord is bigger than 15");
		if(z>15)
			throw new IllegalArgumentException("z coord is bigger than 15");
		if(y>255)
			throw new IllegalArgumentException("y coord is bigger than 255");

		return  (y-minHeight)<<8 | x<<4 | z;
	}

	private synchronized void setAir(int index, int amount)
	{
		int y = (index >> 12) & 15;
		short[] air = this.air[y];
		if(air==null && amount==0)
			return;
		if(air==null && amount!=0)
		{
			air = new short[4096];//16 ^3 = 4096
			air[index & 0xFFF] = (short) amount;
			this.air[y] = air;
		}
		else
		{
			air[index & 0xFFF] =  (short) amount;
		}

	}

	private synchronized int getAir(int index)
	{
		int y = (index >> 12) & 15;
		short[] air = this.air[y];
		if(air==null)
			return 0;
		else
			return ((int)air[index & 0xFFF] & 0xFFFF);
	}

	private static Level w;

	public static void tickChunk(LevelChunk c)
	{
		w = c.getLevel();

		LazyOptional<IChunkAtmosphere> opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
		if(opt==null || !opt.isPresent())
			return;
		IChunkAtmosphere atm = opt.orElseThrow(NullPointerException::new);
		if(!atm.needsUpdate(-1))
			return;

		if(atm.getChunkPos() == null)
		{
			atm.setChunkPos(c.getPos());
		}

		int blockX = c.getPos().x * 16;
		int blockZ = c.getPos().z * 16;

		IPlanet planet = FPPlanetRegistry.instance.getPlanetByDimension(c.getLevel().dimension());
		Random r = new Random(System.currentTimeMillis() ^ 142621051999L);

		float spread = planet.getSpreadVelocity() / (20F -missedTicks +1);
		float gravity = planet.getGravityVelocity() / (20F - missedTicks +1);

		boolean sink = gravity < 0;
		gravity = Math.abs(gravity);
		boolean hasGravity = gravity > 1e-6F;

		boolean doSpread = spread > 1e-6F;

		Heightmap map = c.getOrCreateHeightmapUnprimed(Types.MOTION_BLOCKING); //getHeightmap

		BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos();
		BlockPos.MutableBlockPos posUp = new BlockPos.MutableBlockPos();
		BlockPos.MutableBlockPos posDown = new BlockPos.MutableBlockPos();
		for(int x=0;x<16;x++)
		{
			for(int z=0;z<16;z++)
			{
				atm.setAir(x, 255, z, 0);
				atm.setAir(x, 0, z, 0);
				int precipitationHeight = map.getFirstAvailable(x, z);

				for(int ymin = atm.getHeightSections()-1;ymin>=0;ymin--)
				{
					if(!atm.needsUpdate(ymin))
						continue;

					int ym = ymin * 16;
					for(int y=ym + 15;y>=ym;y--)
					{
						int air = atm.getAirAt(x, y, z);
						if(air==0)
							continue;

						if(r.nextInt(100) < 1+missedTicks && false)
						{
							if(y >= precipitationHeight)
							{
								if(c.getWorldForge() instanceof ServerLevel)
								{
									((ServerLevel)c.getWorldForge()).sendParticles(ParticleTypes.NOTE, blockX+x+0.5D, y+0.5D, blockZ+z+0.5D, 1, 0.0, 0.0, 0.0, 0.0);
								}

								atm.setAir(x, y, z, 0);
								continue;
							}
						}

//						if((System.currentTimeMillis()/1000)%10 == 0)
//							((WorldServer)c.getWorld()).spawnParticle(ParticleTypes.NOTE, blockX+x + 0.5, y + 0.5, blockZ+z + 0.5, 1, 0F, 0F, 0F, 0D);

						pos.set(blockX+x,y,blockZ+z);
						posUp.set(blockX+x,y+1,blockZ+z);
						posDown.set(blockX+x,y-1,blockZ+z);
						boolean full = FullBlockCache.isFullBlock(pos, c.getLevel());
						if(full)
						{
							int old = atm.getAirAt(x, y, z);
							if(old!=0)
							{
								atm.setAir(x, y, z, 0);
								((ServerLevel)c.getWorldForge()).sendParticles(ParticleTypes.NOTE, blockX+x+0.5D, y+0.5D, blockZ+z+0.5D, 1, 0.0, 0.0, 0.0, 0.0);
							}

							continue;
						}

						if(hasGravity)
						{
							if(gravity >= r.nextFloat())
							{
								int added = 0;
								if(sink)
								{
									full = FullBlockCache.isFullBlock(posDown, c.getLevel());
									if(!full && y > 0)
										added = atm.addAir(x, y-1, z, air);
								}
								else
								{
									full = FullBlockCache.isFullBlock(posUp, c.getLevel());
									if(!full)
										added = atm.addAir(x, y+1, z, air);
								}
								if(added>0)
								{
									atm.removeAir(x, y, z, added);
									if(atm.getAirAt(x, y, z)==0)
										continue;
								}
							}

						}

						if(doSpread)
						{
							if(spread >= r.nextFloat())
							{
								spreadAir(x,y,z,c,atm, hasGravity && !sink, hasGravity && sink);
							}
						}
					}
				}
			}
		}
		w = null;
	}

	public static void spreadAirChunkBoarders(BlockPos pos, Level w, IChunkAtmosphere atm, boolean spreadUp, boolean spreadDown)
	{
		final int airTotal = atm.getAirAt(pos.getX() & 15, pos.getY() & 255, pos.getZ() & 15);

		if(airTotal < 140)
		{
			Direction face = Direction.getRandom(new Random());
			BlockPos b = pos.relative(face);
			if(w.hasChunkAt(b) && !FullBlockCache.isFullBlock(b, w))
			{
				LazyOptional<IChunkAtmosphere> opt = w.getChunkAt(b).getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
				opt.ifPresent(other -> {
					int added = other.addAir(b.getX()&15, b.getY()&255, b.getZ()&15, airTotal);
					if(added>0)
						atm.removeAir(pos.getX() & 15, pos.getY() & 255, pos.getZ() & 15, added);
				});
				if(opt.isPresent())
					return;
			}
		}
		else
		{
			List<IntConsumer> airL = new ArrayList<>(7);
			airL.add(i -> atm.setAir(pos.getX() & 15, pos.getY() & 255, pos.getZ() & 15, i));

			int airT = airTotal;
			for(Direction face : FacingUtil.VALUES)
			{
				if(!spreadDown && face== Direction.DOWN)
					continue;
				if(!spreadUp && face== Direction.UP)
					continue;

				BlockPos b = pos.relative(face);
				if(w.hasChunkAt(b) && !FullBlockCache.isFullBlock(b, w))
				{
					IChunkAtmosphere air = w.getChunkAt(b).getCapability(AtmosphereManager.cap_ATMOSPHERE, null).orElse(null);
					if(air!=null)
					{
						airT += air.getAirAt(b.getX() & 15, b.getY() & 255, b.getZ() & 15);
						airL.add(i -> air.setAir(b.getX() & 15, b.getY() & 255, b.getZ() & 15, i));
					}
				}
			}
			int each = airT / airL.size();
			if(each==atm.getMaxAir())
				return;

			int rest = airT % airL.size();
			for(IntConsumer setter : airL)
			{
				setter.accept(rest + each);
				rest = 0;
			}
		}
	}

	public static void spreadAir(int x, int y, int z, LevelChunk c, IChunkAtmosphere atm, boolean spreadUp, boolean spreadDown)
	{
		if(x>0 && y>0 && z>0 && x<15 && y<255 && z<15)
		{
			int airTotal = atm.getAirAt(x, y, z);

			BlockPos pos = new BlockPos(c.getPos().x*16 + x, y, c.getPos().z*16+z);

			FullBlockCache cache = FullBlockCache.getChunkCache(pos, c.getLevel());
			Predicate<BlockPos> fullBlock = cache!=null ? cache::isFullBlock : p -> FullBlockCache.isFullBlock(p, c.getLevel());

			if(airTotal < 140)//7(max blocks) * 20 (air for one second)
			{
				Direction face = Direction.getRandom(new Random());
				BlockPos b = pos.relative(face);
				if(!fullBlock.test(b))
				{
					int added = atm.addAir(b.getX()&15, b.getY()&255, b.getZ()&15, airTotal);
					if(added>0)
						atm.removeAir(x, y, z, added);

					return;
				}
			}
			else
			{
				List<BlockPos> blocks = new ArrayList<BlockPos>(7);
				blocks.add(pos);

				for(Direction face : FacingUtil.VALUES)
				{
					if(!spreadDown && face== Direction.DOWN)
						continue;
					if(!spreadUp && face== Direction.UP)
						continue;

					BlockPos b = pos.relative(face);
					if(!fullBlock.test(b))
					{
						airTotal += atm.getAirAt(b.getX()&15, b.getY()&255, b.getZ()&15);
						blocks.add(b);
					}
				}

				int each = airTotal / blocks.size();
				if(each==atm.getMaxAir())
					return;

				int rest = airTotal % blocks.size();
				for(BlockPos b : blocks)
				{
					if(!FullBlockCache.isFullBlock(b, w))
					{
						atm.setAir(b.getX()&15, b.getY()&255, b.getZ()&15, each + rest);
						rest = 0;
					}
					else
					{
						boolean r = fullBlock.test(b);
						if(r)
						{
							System.out.println("AHH!");
						}
					}
				}
			}
		}
		else
		{
			spreadAirChunkBoarders(new BlockPos(c.getPos().x*16 + x, y, c.getPos().z*16+z), c.getLevel(), atm, spreadUp, spreadDown);
		}

	}

	public static byte missedTicks = 0;

	public static void updateAthmosphere(ServerLevel serv)
	{
		long nano = System.currentTimeMillis();
		ServerChunkCache prov = serv.getChunkSource();
		ChunkMap cm = prov.chunkMap;
		Stream<ChunkHolder> status = cm.visibleChunkMap.values().stream();//AT are great
		List<LevelChunk> list = status.map(ChunkHolder::getTickingChunk).filter(Predicates.notNull()).collect(Collectors.toList());
		for(LevelChunk c : list)
		{
			if(c!=null)
				tickChunk(c);
		}
		nano = System.currentTimeMillis() - nano;

		if(nano > 500)
		{
			missedTicks = (byte) (20 - 1000/nano);
			System.out.println("Atmoshphere update took " + nano + " ms (Chunks:" + list.size() + "), Skipping " + missedTicks + " ticks");
		}
		else
		{
			missedTicks = 0;
		}
	}
}
