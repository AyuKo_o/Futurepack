//package futurepack.world.dimensions;
//
//import futurepack.api.interfaces.IPlanet;
//import futurepack.common.FPSounds;
//import futurepack.common.spaceships.FPSpaceShipSelector;
//import futurepack.common.spaceships.SpaceshipRegistry;
//import net.minecraft.entity.Entity;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.entity.player.ServerPlayerEntity;
//import net.minecraft.network.play.server.SPlaySoundEffectPacket;
//import net.minecraft.util.SoundCategory;
//import net.minecraft.world.Teleporter;
//import net.minecraft.world.server.ServerWorld;
//
//public class SpaceTeleporter extends Teleporter
//{
//	private final ServerWorld newDimesion;
//	private final FPSpaceShipSelector sel;
//	private int h;
//	public SpaceTeleporter(ServerWorld s, FPSpaceShipSelector c)
//	{
//		super(s);
//		newDimesion = s;
//		sel = c;
//	}
//
//	public SpaceTeleporter(ServerWorld s, int c)
//	{
//		super(s);
//		newDimesion = s;
//		sel = null;
//		h = c;
//	}
//	
//	@Override
//	public boolean placeInPortal(Entity toTransfer, float rotYaw)
//	{
//		if(sel!=null)
//		{
//			h =  sel.transferShips(newDimesion);			
//		}
//		toTransfer.setPosition(toTransfer.getPosX(), ((int)toTransfer.getPosY())+h+1, toTransfer.getPosZ());
//		if(toTransfer instanceof PlayerEntity)
//		{
//			PlayerEntity pl = ((PlayerEntity)toTransfer);
//			IPlanet pa = SpaceshipRegistry.instance.getPlanetByDimension(newDimesion.getDimensionType());
//			if(pa!=null && pa!=SpaceshipRegistry.instance.MINECRAFT)
//			{
//				pl.setSpawnPoint(pl.getPosition(), true, false, newDimesion.getDimensionType());
//			}			
//		}
//		
//		if(toTransfer instanceof ServerPlayerEntity)
//		{
//			((ServerPlayerEntity) toTransfer).connection.sendPacket(new SPlaySoundEffectPacket(FPSounds.JUMP, SoundCategory.MASTER, toTransfer.getPosX(), toTransfer.getPosY(), toTransfer.getPosZ(), 1.0F, 1.0F));
//		}
//		
//		return true;
//	}
//	
//	@Override
//	public boolean makePortal(Entity p_85188_1_)
//	{
//		return true;
//	}
//	
////	@Override
////	public boolean placeInPortal(Entity p_222268_1_, float p_222268_2_) 
////	{
////		return super.placeInPortal(p_222268_1_, p_222268_2_);
////	}
//	
////	
////	protected RayTraceResult getRayTraceResultFromPlayer(World p_77621_1_, Entity p_77621_2_, boolean p_77621_3_)
////    {
////        float f = 1.0F;
////        float f1 = p_77621_2_.prevRotationPitch + (p_77621_2_.rotationPitch - p_77621_2_.prevRotationPitch) * f;
////        float f2 = p_77621_2_.prevRotationYaw + (p_77621_2_.rotationYaw - p_77621_2_.prevRotationYaw) * f;
////        double d0 = p_77621_2_.prevPosX + (p_77621_2_.getPosX() - p_77621_2_.prevPosX) * (double)f;
////        double d1 = p_77621_2_.prevPosY + (p_77621_2_.getPosY() - p_77621_2_.prevPosY) * (double)f + (double)(p_77621_1_.isRemote ? p_77621_2_.getStandingEyeHeight() - p_77621_2_.getStandingEyeHeight(): p_77621_2_.getStandingEyeHeight()); // isRemote check to revert changes to ray trace position due to adding the eye height clientside and player yOffset differences
////        double d2 = p_77621_2_.prevPosZ + (p_77621_2_.getPosZ() - p_77621_2_.prevPosZ) * (double)f;
////        Vector3d Vector3d = Vector3d.createVectorHelper(d0, d1, d2);
////        float f3 = MathHelper.cos(-f2 * 0.017453292F - (float)Math.PI);
////        float f4 = MathHelper.sin(-f2 * 0.017453292F - (float)Math.PI);
////        float f5 = -MathHelper.cos(-f1 * 0.017453292F);
////        float f6 = MathHelper.sin(-f1 * 0.017453292F);
////        float f7 = f4 * f5;
////        float f8 = f3 * f5;
////        double d3 = 5.0D;
////        Vector3d Vector3d1 = Vector3d.addVector((double)f7 * d3, (double)f6 * d3, (double)f8 * d3);
////        return p_77621_1_.func_147447_a(Vector3d, Vector3d1, p_77621_3_, !p_77621_3_, false);
////    }
//}
