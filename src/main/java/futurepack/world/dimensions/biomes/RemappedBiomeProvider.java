package futurepack.world.dimensions.biomes;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import com.google.common.base.Suppliers;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import it.unimi.dsi.fastutil.objects.ObjectLinkedOpenHashSet;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.resources.RegistryOps;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.biome.Climate;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.StructureFeature;

public class RemappedBiomeProvider extends BiomeSource 
{
	public static final Codec<RemappedBiomeProvider> CODEC = RecordCodecBuilder.create((p) -> 
	{
		return p.group(
				BiomeSource.CODEC.fieldOf("biomeprovider").forGetter(RemappedBiomeProvider::getBiomeProviderBase),
				Codec.unboundedMap(ResourceLocation.CODEC, ResourceLocation.CODEC).fieldOf("biome_map").forGetter(RemappedBiomeProvider::getBiomeMap),
				ResourceLocation.CODEC.fieldOf("default_biome").forGetter(c -> c.defaultBiome),
				RegistryOps.retrieveRegistry(Registry.BIOME_REGISTRY).forGetter((overworldProvider) -> {
			         return overworldProvider.lookupRegistry;
			      })
		).apply(p, RemappedBiomeProvider::new);
	});
	
	private BiomeSource base;
	private Supplier<Map<Biome, Biome>> biomeOverrides;
	private Map<ResourceLocation,  ResourceLocation> rawBiomeOverrides;
	private ResourceLocation defaultBiome;
	private Supplier<Set<Holder<Biome>>> list;
	private final Registry<Biome> lookupRegistry;
	
	protected RemappedBiomeProvider(BiomeSource base, Map<ResourceLocation, ResourceLocation> biomeOverrides, ResourceLocation defaultBiome, Registry<Biome> lookupRegistry)
	{
		super((List<Holder<Biome>>)null);
		this.biomeOverrides = convert(biomeOverrides, lookupRegistry);
		list = Suppliers.memoize(() -> remap(base.possibleBiomes(), this.biomeOverrides.get(), convert(defaultBiome, lookupRegistry)));
		this.base = base;
		this.rawBiomeOverrides = biomeOverrides;
		this.defaultBiome = defaultBiome;
		this.lookupRegistry = lookupRegistry;
	}
	
	@Override
	public Set<Holder<Biome>> possibleBiomes() 
	{
		return list.get();
	}

	@Override
	public Holder<Biome> getNoiseBiome(int x, int y, int z, Climate.Sampler p_204241_) 
	{
		return Holder.direct(remap(base.getNoiseBiome(x, y, z, p_204241_), biomeOverrides.get(), convert(defaultBiome, lookupRegistry)));
	}

	@Override
	protected Codec<? extends BiomeSource> codec() 
	{
		return CODEC;
	}

	@Override
	public BiomeSource withSeed(long seed) 
	{
		return new RemappedBiomeProvider(base.withSeed(seed), rawBiomeOverrides, defaultBiome, lookupRegistry);
	}
	
	public BiomeSource getBiomeProviderBase() 
	{
		return base;
	}
	
	public Map<ResourceLocation, ResourceLocation> getBiomeMap()
	{
		return rawBiomeOverrides;
	}
	
	
	
//	@Override
//	public boolean canGenerateStructure(StructureFeature<?> structureIn) 
//	{
//		return this.supportedStructures.computeIfAbsent(structureIn, (structure) -> 
//		{
//			return this.list.get().stream().anyMatch((biome) -> 
//			{
//				return biome.getGenerationSettings().isValidStart(structure);
//			});
//		});
//	}
//
//	@Override
//	public Set<BlockState> getSurfaceBlocks() 
//	{
//		if (this.surfaceBlocks.isEmpty()) 
//		{
//			for(Biome biome : this.list.get()) 
//			{
//				this.surfaceBlocks.add(biome.getGenerationSettings().getSurfaceBuilderConfig().getTopMaterial());
//			}
//		}
//
//		return this.surfaceBlocks;
//	}
	
	public static Biome remap(Holder<Biome> b, Map<Biome, Biome> remapping, Holder<Biome> defaultBiome)
	{
		Biome mapped = remapping.getOrDefault(b.value(), defaultBiome.value());
		if(mapped==null)
			throw new NullPointerException(mapped +" is null");
		return mapped;
	}
	
	public static Set<Holder<Biome>> remap(Collection<Holder<Biome>> biomes, Map<Biome, Biome> remapping, Holder<Biome> defaultBiome)
	{
		return new ObjectLinkedOpenHashSet(
				(List<Holder<Biome>>)
				biomes.parallelStream()
				.map(b -> Holder.direct(remap(b, remapping, defaultBiome)))
				.distinct()
				.collect(Collectors.toList()));
	}
	
	public static Supplier<Map<Biome, Biome>> convert(Map<ResourceLocation, ResourceLocation> biomeOverrides2, Registry<Biome> registry)
	{
		return Suppliers.memoize(() -> {
			HashMap<Biome, Biome> map = new HashMap<>();
			biomeOverrides2.entrySet().forEach(e -> {
				Biome key = registry.getOrThrow(ResourceKey.create(Registry.BIOME_REGISTRY, e.getKey()));
				Biome value = registry.getOrThrow(ResourceKey.create(Registry.BIOME_REGISTRY, e.getValue()));
				
				map.put(key, value);
			});
			
			return map;
		});
	}
	
	public static Holder<Biome> convert(ResourceLocation biome, Registry<Biome> registry)
	{
		return registry.getHolderOrThrow(ResourceKey.create(Registry.BIOME_REGISTRY, biome));
	}
}
