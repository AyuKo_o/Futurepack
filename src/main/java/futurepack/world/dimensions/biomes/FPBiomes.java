package futurepack.world.dimensions.biomes;

import java.util.function.Consumer;

import futurepack.api.Constants;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.depend.api.helper.HelperItems;
import futurepack.world.gen.carver.LargeCanyonWorldCaver;
import futurepack.world.gen.carver.LargeMenelausCavesWorldCaver;
import futurepack.world.gen.carver.SuperCanyoneWorldCaver;
import futurepack.world.gen.carver.WorldCarverCrater;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.carver.WorldCarver;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.featuresize.TwoLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.foliageplacers.BlobFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.trunkplacers.StraightTrunkPlacer;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.event.RegistryEvent;

public class FPBiomes
{
//	public static final BlockState SAND = TerrainBlocks.sand_m.getDefaultState();
	public static final BlockState SANDSTONE = TerrainBlocks.sandstone_m.get().defaultBlockState();
	public static final BlockState GRAVEL = TerrainBlocks.gravel_m.get().defaultBlockState();
	public static final BlockState STONE = TerrainBlocks.stone_m.get().defaultBlockState();
	public static final BlockState DIRT = TerrainBlocks.dirt_m.get().defaultBlockState();
//	public static final BlockState V_DIRT = Blocks.DIRT.getDefaultState();
//	public static final BlockState V_GRAVEL = Blocks.GRAVEL.getDefaultState();
//	public static final BlockState GRASS = TerrainBlocks.grass_t.getDefaultState();

//	public static final SurfaceBuilderConfig M_SAND_SANDSTONE_GRAVEL = new SurfaceBuilderConfig(SAND, SANDSTONE, GRAVEL);
//	public static final SurfaceBuilderConfig M_SANDSTONE_SANDSTONE_GRAVEL = new SurfaceBuilderConfig(SANDSTONE, SANDSTONE, GRAVEL);
	/**
	public static final SurfaceBuilderBaseConfiguration M_SANDSTONE_STONE_GRAVEL = new SurfaceBuilderBaseConfiguration(SANDSTONE, STONE, GRAVEL);
	public static final SurfaceBuilderBaseConfiguration M_DIRT_GRAVEL_SANDSTONE = new SurfaceBuilderBaseConfiguration(DIRT, GRAVEL, SANDSTONE);
	*/
//	public static final SurfaceBuilderConfig T_GRASS_DIRT_GRAVEL = new SurfaceBuilderConfig(GRASS, V_DIRT, V_GRAVEL);
	/**
	public static final SurfaceBuilder<SurfaceBuilderBaseConfiguration> MENELAUS_SURFACE = new SurfaceBuilderSpecialMix(SurfaceBuilderBaseConfiguration.CODEC, FPBiomes.M_SANDSTONE_STONE_GRAVEL, 1.0F);
	public static final SurfaceBuilder<SurfaceBuilderBaseConfiguration> MENELAUS_SURFACE_FOREST = new SurfaceBuilderSpecialMix(SurfaceBuilderBaseConfiguration.CODEC, FPBiomes.M_DIRT_GRAVEL_SANDSTONE, 0.8F);
	public static final SurfaceBuilder<SurfaceBuilderBaseConfiguration> TYROS_SURFACE = new SurfaceBuilderSpecialMix(SurfaceBuilderBaseConfiguration.CODEC, SurfaceBuilder.CONFIG_GRASS, 1.0F);
	*/


	public static final TreeConfiguration TYROS_TREE_CONFIG = (new TreeConfiguration.TreeConfigurationBuilder(
			BlockStateProvider.simple(TerrainBlocks.log_tyros.get().defaultBlockState()),
			new StraightTrunkPlacer(5, 2, 4),
			BlockStateProvider.simple(PlantBlocks.leaves_tyros.defaultBlockState()),
			//BlockStateProvider.simple(PlantBlocks.sapling_tyros.defaultBlockState()),
			new BlobFoliagePlacer(ConstantInt.of(2) , ConstantInt.of(0), 3),
			new TwoLayersFeatureSize(1, 0, 1)))
			.ignoreVines()
			.build();



//	public static final BiomeBase menelaus = new BiomeMenelaus("menelaus", (new Biome.Builder())
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(MENELAUS_SURFACE, M_SAND_SANDSTONE_GRAVEL))
//			.precipitation(Biome.RainType.NONE)
//			.category(Biome.Category.DESERT)
//			.depth(0.8F)
//			.scale(0.7F)
//			.temperature(2.0F).downfall(0.0F)
//			.waterColor(0x7c7c4f).waterFogColor(329011)
//			.parent((String)null));
//	public static final BiomeBase menelaus_sea = new BiomeMenelaus("menelaus_sea", (new Biome.Builder())
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(SurfaceBuilder.DEFAULT, M_SAND_SANDSTONE_GRAVEL))
//			.precipitation(Biome.RainType.NONE)
//			.category(Biome.Category.DESERT)
//			.depth(-0.1F)
//			.scale(0.5F)
//			.temperature(1.8F).downfall(0.3F)
//			.waterColor(0x4f7c53).waterFogColor(329011)
//			.parent((String)null));
//	public static final BiomeBase menelaus_flat = new BiomeMenelaus("menelaus_flat", (new Biome.Builder())
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(SurfaceBuilder.DEFAULT, M_SAND_SANDSTONE_GRAVEL))
//			.precipitation(Biome.RainType.NONE)
//			.category(Biome.Category.DESERT)
//			.depth(0.5F)
//			.scale(0.08F)
//			.temperature(1.8F).downfall(0.2F)
//			.waterColor(0x7c654f).waterFogColor(329011)
//			.parent((String)null));
//	public static final BiomeBase menelaus_platau = new BiomeMenelaus("menelaus_platau", (new Biome.Builder())
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(MENELAUS_SURFACE, M_SANDSTONE_SANDSTONE_GRAVEL))
//			.precipitation(Biome.RainType.NONE)
//			.category(Biome.Category.DESERT)
//			.depth(4.5F)
//			.scale(0.1F)
//			.temperature(1.8F).downfall(0.2F)
//			.waterColor(0x7c654f).waterFogColor(329011)
//			.parent((String)null));
//	public static final BiomeBase menelaus_forest = new BiomeMenelaus("menelaus_forest", (new Biome.Builder())
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(MENELAUS_SURFACE_FOREST, M_SAND_SANDSTONE_GRAVEL))
//			.precipitation(Biome.RainType.NONE)
//			.category(Biome.Category.DESERT)
//			.depth(0.34F)
//			.scale(0.1F)
//			.temperature(1.5F).downfall(0.4F)
//			.waterColor(0x4f7c53).waterFogColor(329011)
//			.parent((String)null));
//	public static final BiomeBase menelaus_mushroom = new BiomeMenelausMuhsroom("menelaus_mushroom", (new Biome.Builder())
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(SurfaceBuilder.DEFAULT, SurfaceBuilder.MYCELIUM_DIRT_GRAVEL_CONFIG))
//			.precipitation(Biome.RainType.NONE)
//			.category(Biome.Category.MUSHROOM)
//			.depth(0.8F)
//			.scale(0.7F)
//			.temperature(2.0F).downfall(0.0F)
//			.waterColor(0x7c7c4f).waterFogColor(329011)
//			.parent((String)null));

//	public static final BiomeBase tyros = new BiomeTyros("tyros", new Biome.Builder()
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(TYROS_SURFACE, T_GRASS_DIRT_GRAVEL))
//			.precipitation(Biome.RainType.RAIN)
//			.category(Biome.Category.JUNGLE)
//			.waterColor(0x00bbff).waterFogColor(329011)
//			.temperature(1.4F).downfall(2F)
//			.depth(0.2F).scale(0.21F)
//			.parent((String)null));
//	public static final BiomeBase tyros_swamp = new BiomeTyros("tyros_swamp", new Biome.Builder()
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(TYROS_SURFACE, T_GRASS_DIRT_GRAVEL))
//			.precipitation(Biome.RainType.RAIN)
//			.category(Biome.Category.JUNGLE)
//			.waterColor(0x00bbff).waterFogColor(329011)
//			.temperature(1.4F).downfall(1.5F)
//			.depth(-0.2F).scale(0.1F)
//			.parent((String)null));
//	public static final BiomeBase tyros_mountain = new BiomeTyros("tyros_mountain", new Biome.Builder()
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(TYROS_SURFACE, T_GRASS_DIRT_GRAVEL))
//			.precipitation(Biome.RainType.RAIN)
//			.category(Biome.Category.JUNGLE)
//			.waterColor(0x00bbff).waterFogColor(329011)
//			.temperature(0.9F).downfall(2F)
//			.depth(0.6F).scale(0.61F)
//			.parent((String)null));
//	public static final BiomeBase tyros_palirie_forest = new BiomeTyros("tyros_palirie_forest", new Biome.Builder()
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(TYROS_SURFACE, T_GRASS_DIRT_GRAVEL))
//			.precipitation(Biome.RainType.RAIN)
//			.category(Biome.Category.FOREST)
//			.waterColor(4159204).waterFogColor(329011)
//			.temperature(0.6F).downfall(1.2F)
//			.depth(0.15F).scale(0.32F)
//			.parent((String)null));

//	public static final BiomeBase tyros_rockdesert = new BiomeTyrosRockDesert("tyros_rockdesert", new Biome.Builder()
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(SurfaceBuilder.DEFAULT, SurfaceBuilder.STONE_STONE_GRAVEL_CONFIG))
//			.precipitation(Biome.RainType.RAIN)
//			.category(Biome.Category.NONE)
//			.waterColor(0x1e371f).waterFogColor(0x654321)
//			.temperature(1.7F).downfall(0.1F)
//			.depth(0.2F).scale(0.31F));
//	public static final BiomeBase tyros_rockdesertflat = new BiomeTyrosRockDesert("tyros_rockdesertflat", new Biome.Builder()
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(SurfaceBuilder.DEFAULT,  SurfaceBuilder.STONE_STONE_GRAVEL_CONFIG))
//			.precipitation(Biome.RainType.RAIN)
//			.category(Biome.Category.NONE)
//			.waterColor(0x1e371f).waterFogColor(0x654321)
//			.temperature(1.7F).downfall(0.1F)
//			.depth(0.6F).scale(0.11F));

	//Entros-Configurations
	//public static final Biome menelaus = new BiomeMenelaus(new BiomeProperties("Entros").setWaterColor(0x00aaaa).setRainDisabled().setTemperature(2F).setRainfall(0F).depth(-0.8F).scale(2.7F));

//	public static final BiomeBase envia = new BiomeEnvia("envia", new Biome.Builder()
//			.surfaceBuilder(new ConfiguredSurfaceBuilder<>(SurfaceBuilder.DEFAULT,  SurfaceBuilder.STONE_STONE_GRAVEL_CONFIG))
//			.category(Biome.Category.NONE)
//			.precipitation(Biome.RainType.NONE)
//			.depth(2.1F).scale(0.4F)
//			.waterColor(0x8800FF).waterFogColor(0x8800FF)
//			.temperature(0.0F).downfall(0.3F));

	public static void register(RegistryEvent.Register<Biome> e)
	{
		addTypes("menelaus",			BiomeDictionary.Type.HOT, BiomeDictionary.Type.DRY, BiomeDictionary.Type.SANDY, BiomeDictionary.Type.HILLS, BiomeDictionary.Type.MOUNTAIN);
		addTypes("menelaus_sea",		BiomeDictionary.Type.HOT, BiomeDictionary.Type.DRY, BiomeDictionary.Type.SANDY, BiomeDictionary.Type.BEACH);
		addTypes("menelaus_flat",		BiomeDictionary.Type.HOT, BiomeDictionary.Type.DRY, BiomeDictionary.Type.SANDY, BiomeDictionary.Type.PLAINS);
		addTypes("menelaus_platau",		BiomeDictionary.Type.HOT, BiomeDictionary.Type.DRY, BiomeDictionary.Type.SANDY, BiomeDictionary.Type.HILLS, BiomeDictionary.Type.MESA);
		addTypes("menelaus_forest",		BiomeDictionary.Type.HOT, BiomeDictionary.Type.DRY, BiomeDictionary.Type.SANDY, BiomeDictionary.Type.FOREST);
		addTypes("menelaus_mushroom",	BiomeDictionary.Type.MUSHROOM, BiomeDictionary.Type.HOT, BiomeDictionary.Type.DRY);

		addTypes("tyros",					BiomeDictionary.Type.HOT, BiomeDictionary.Type.WET, BiomeDictionary.Type.JUNGLE);
		addTypes("tyros_swamp",				BiomeDictionary.Type.HOT, BiomeDictionary.Type.WET, BiomeDictionary.Type.JUNGLE, BiomeDictionary.Type.SWAMP);
		addTypes("tyros_mountain",			BiomeDictionary.Type.HOT, BiomeDictionary.Type.WET, BiomeDictionary.Type.JUNGLE, BiomeDictionary.Type.MOUNTAIN);
		addTypes("tyros_rockdesert",		BiomeDictionary.Type.HOT, BiomeDictionary.Type.DEAD, BiomeDictionary.Type.SPARSE);
		addTypes("tyros_rockdesertflat",	BiomeDictionary.Type.HOT, BiomeDictionary.Type.DEAD, BiomeDictionary.Type.SPARSE);
		addTypes("tyros_palirie_forest",	BiomeDictionary.Type.WET, BiomeDictionary.Type.FOREST);

		addTypes("envia", BiomeDictionary.Type.COLD, BiomeDictionary.Type.DRY, BiomeDictionary.Type.DEAD, BiomeDictionary.Type.LUSH);

	}

	private static void addTypes(String biome, BiomeDictionary.Type... types)
    {
		ResourceKey<Biome> bio = ResourceKey.create(Registry.BIOME_REGISTRY, new ResourceLocation(Constants.MOD_ID, biome));
		BiomeDictionary.addTypes(bio, types);
    }

	/**
	public static void registerSurfaceBuilder(Consumer<SurfaceBuilder<?>> register)
	{
		register.accept(MENELAUS_SURFACE.setRegistryName(Constants.MOD_ID, "menelaus_surface"));
		register.accept(MENELAUS_SURFACE_FOREST.setRegistryName(Constants.MOD_ID, "menelaus_surface_forest"));
		register.accept(TYROS_SURFACE.setRegistryName(Constants.MOD_ID, "tyros_surface"));
	}
	*/

	public static void registerCarvers(Consumer<WorldCarver<?>> register)
	{
//		ForgeRegistries.WORLD_CARVERS;

		register.accept(HelperItems.setRegistryName(new LargeMenelausCavesWorldCaver(), Constants.MOD_ID, "menelaus_caves"));
		register.accept(HelperItems.setRegistryName(new WorldCarverCrater(150, 50), Constants.MOD_ID, "large_crater"));
		register.accept(HelperItems.setRegistryName(new LargeCanyonWorldCaver(), Constants.MOD_ID, "large_canyon"));
		register.accept(HelperItems.setRegistryName(new SuperCanyoneWorldCaver(), Constants.MOD_ID, "super_canyon"));
	}
}
