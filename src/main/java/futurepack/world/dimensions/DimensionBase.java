//package futurepack.world.dimensions;
//
//import java.util.Random;
//
//import javax.annotation.Nullable;
//
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.math.ChunkPos;
//import net.minecraft.world.DimensionType;
//import net.minecraft.world.World;
//
//public abstract class DimensionBase extends Dimension 
//{
//	protected final DimensionType type;
//	
//	public DimensionBase(World w, DimensionType type, float baseLight)
//	{
//		super(w, type, baseLight);
//		this.type = type;
//	}
//	
//	@Override
//	public boolean isSurfaceWorld() 
//	{
//		return false;
//	}
//
//	@Override
//	public DimensionType getType() 
//	{
//		return type;
//	}
//	
//	@Override
//	@Nullable
//	public BlockPos findSpawn(ChunkPos chunk, boolean checkValid) 
//	{
//		Random random = new Random(this.world.getSeed());
//		BlockPos blockpos = new BlockPos(chunk.getXStart() + random.nextInt(15), 0, chunk.getZEnd() + random.nextInt(15));
//		return this.world.getGroundAboveSeaLevel(blockpos).getMaterial().blocksMovement() ? blockpos : null;
//	}
//
//	@Override
//	public BlockPos findSpawn(int p_206921_1_, int p_206921_2_, boolean checkValid)
//	{
//		return this.findSpawn(new ChunkPos(p_206921_1_ >> 4, p_206921_2_ >> 4), checkValid);//copy from EndDimension
//	}
//	
//	@Override
//	public float calculateCelestialAngle(long worldTime, float partialTicks) 
//	{
//		int i = (int)(worldTime % 24000L);
//		float f = (i + partialTicks) / 24000.0F - 0.25F;
//		if (f < 0.0F) {
//			++f;
//		}
//
//		if (f > 1.0F) {
//			--f;
//		}
//
//		float f1 = 1.0F - (float)((Math.cos(f * Math.PI) + 1.0D) / 2.0D);
//		f = f + (f1 - f) / 3.0F;
//		return f;
//	}
//}
