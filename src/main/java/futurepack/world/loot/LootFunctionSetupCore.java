package futurepack.world.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import futurepack.common.FPLootFunctions;
import futurepack.common.item.ItemCore;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.Serializer;
import net.minecraft.world.level.storage.loot.functions.LootItemFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraft.world.level.storage.loot.providers.number.NumberProvider;

public class LootFunctionSetupCore implements LootItemFunction
{
	private final Boolean isToasted;
    private final NumberProvider core;
    private final String[] coreid;

    public LootFunctionSetupCore(Boolean toastedIn, NumberProvider coreRangeIn, String...coreidIn)
    {
        isToasted = toastedIn;
        core = coreRangeIn;
        coreid = coreidIn;
    }

    @Override
	public ItemStack apply(ItemStack stack, LootContext context)
    {
    	CompoundTag innerNBT = new CompoundTag();
    	
//    	if(core.getMax() > 10 || core.getMin() < 0)
//    		FPLog.logger.warn("Core power is out of normal range!");
    		
    	innerNBT.putFloat("core", core.getInt(context));
    	
    	if(!isToasted)
    	{
    		if(ItemCore.getCore(stack)!=null)
    		{
    			stack.setTag(innerNBT);
    		}
    		else
    			throw new IllegalStateException("Only cores are working! but got " + stack);
    	}
    	else
    	{
    		CompoundTag outerNBT = new CompoundTag();
    		outerNBT.put("tag", innerNBT);

    		if(coreid!=null)
    		{
	    		if(coreid.length >= 2)
	    		{
	        		outerNBT.putString("id", coreid[context.getRandom().nextInt(coreid.length)]);
	    		}
	    		else if(coreid.length == 1)
	    		{
	    			outerNBT.putString("id", coreid[0]);
	    		}
    		}
    		outerNBT.putBoolean("toasted", true);
    		outerNBT.putInt("Count", 1);
    		stack.setTag(outerNBT);
//    		stack.setItemDamage(ItemSpaceship.toasted_core);
    	}

        return stack;
    }

    public static class Storage implements Serializer<LootFunctionSetupCore>
    {
            @Override
			public void serialize(JsonObject object, LootFunctionSetupCore functionClazz, JsonSerializationContext serializationContext)
            {
            	object.add("toasted", serializationContext.serialize(functionClazz.isToasted));
            	object.add("core", serializationContext.serialize(functionClazz.core));
            	if(functionClazz.isToasted)
            		object.add("coreid", serializationContext.serialize(functionClazz.coreid));
            }

            @Override
			public LootFunctionSetupCore deserialize(JsonObject object, JsonDeserializationContext deserializationContext)
            {
            	Boolean toasted = GsonHelper.getAsObject(object, "toasted", deserializationContext, Boolean.class);
            	NumberProvider core = GsonHelper.getAsObject(object, "core", deserializationContext, NumberProvider.class);
            	String[] coreid = null;
            	if(toasted)
            	{
            		coreid = GsonHelper.getAsObject(object, "coreid", deserializationContext, String[].class);
            	}
            	
                return new LootFunctionSetupCore(toasted, core, coreid);
            }
    }

	@Override
	public LootItemFunctionType getType() 
	{
		return FPLootFunctions.SETUP_CORE.get();
	}
    
}
