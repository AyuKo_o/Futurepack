package futurepack.world.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import futurepack.common.FPLootFunctions;
import futurepack.common.item.misc.ItemBatterie;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.Serializer;
import net.minecraft.world.level.storage.loot.functions.LootItemFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;

public class LootFunctionSetupBattery implements LootItemFunction 
{
	private final int meanNE, deviationNE;
	
	public LootFunctionSetupBattery(int mean, int deviation) 
	{
		this.meanNE = mean;
		this.deviationNE = deviation;
	}
	
	@Override
	public ItemStack apply(ItemStack t, LootContext u) 
	{
		int mean = meanNE, deviation = deviationNE;
		if(meanNE == -1 && deviationNE == -1)
		{	
			if(t.getItem() instanceof ItemBatterie)
			{
				ItemBatterie bat = (ItemBatterie) t.getItem();
				mean = bat.getDefaultNeon();
				deviation = bat.getDefaultNeon() / 2;
			}
			else
			{
				mean = 200;
				deviation = 100;
			}
		}
		mean *= (1 + u.getLuck()); 
		int maxNe = (int) (u.getRandom().nextGaussian() * deviation + mean);
		
		//System.out.printf("%f %d %d => %d\n", u.getLuck(), mean, deviation, maxNe);
		
		if(maxNe<0)
			maxNe = 0;
		
		CompoundTag nbt =  t.getOrCreateTagElement("neon");
		nbt.putInt("ne", maxNe);
		nbt.putInt("maxNE", maxNe);
		
		return t;
	}

	public static class Storage implements Serializer<LootFunctionSetupBattery>
	{
			@Override
			public void serialize(JsonObject object, LootFunctionSetupBattery functionClazz, JsonSerializationContext serializationContext)
			{
				object.add("mean", serializationContext.serialize(functionClazz.meanNE));
				object.add("deviation", serializationContext.serialize(functionClazz.deviationNE));
			}

			@Override
			public LootFunctionSetupBattery deserialize(JsonObject object, JsonDeserializationContext deserializationContext)
			{
				if(object.has("mean") && object.has("deviation"))
				{
					return new LootFunctionSetupBattery(object.get("mean").getAsInt(), object.get("deviation").getAsInt());
				}
				else
				{
					return new LootFunctionSetupBattery(-1, -1);
				}
			}
	}

	@Override
	public LootItemFunctionType getType() 
	{
		return FPLootFunctions.SETUP_BATTERY.get();
	}
}
