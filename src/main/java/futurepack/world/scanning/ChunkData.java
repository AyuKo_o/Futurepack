package futurepack.world.scanning;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import futurepack.api.helper.HelperTags;
import futurepack.depend.api.helper.HelperChunks;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;

public class ChunkData
{
	protected final LevelAccessor w;
	private final BlockPos coords;
	/**
	 * contains tag path and ore count per chunk<br>
	 * <b>example:</b><br>
	 * <code>
	 * forge:ores/iron=58<br>
	 * forge:ores/lapis=18<br>
	 * forge:ores/coal=13<br>
	 * forge:ores/zinc=66<br>
	 * forge:ores/magnetite=35<br>
	 * forge:ores/redstone=32<br>
	 * forge:ores/tin=30<br>
	 * forge:ores/gold=28<br>
	 * forge:ores/copper=76<br>
	 * forge:ores/diamond=11<br>
	 * forge:ores/bauxite=29<br>
	 * </code>
	 */
	private Map<String, Integer> ores;
	private BlockState[][][] chunk;
	private int totalOres = -1;

	private static final HashMap<Block, String> isOre = new HashMap<Block, String>();
	public static final TagKey<Block> ORES = BlockTags.create(new ResourceLocation("forge:ores"));

	protected static String getOre(LevelAccessor w, BlockState st)
	{
		Block block = st.getBlock();

		if(block==Blocks.AIR)
		{
			return null;
		}

		String s = null;

		synchronized (isOre)
		{
			s = isOre.get(block);
		}
		if(s!=null)
		{
			if(s.equals("-1"))
				return null;
			return s;
		}
		else
		{
			String ores = getBlockOreTag(block);

			if(ores == null)
			{
				synchronized (isOre)
				{
					isOre.put(block, "-1");
					return null;
				}
			}
			else
			{
				synchronized (isOre)
				{
					isOre.put(block, ores);
					return ores;
				}
			}
		}
	}

	@Nullable
	private static String getBlockOreTag(Block state)
	{
		if(HelperTags.isBlock(state, ORES))
		{
			var opt = state.builtInRegistryHolder().getTagKeys()
				.map(TagKey::location)
				.filter(r -> r.getNamespace().equals("forge"))
				.filter(r -> r.getPath().startsWith("ores/"))
				.sequential()
				.sorted((a,b) -> a.getPath().compareTo(b.getPath()))
				.findFirst();
			return opt.map(ResourceLocation::toString).orElse(null);

		}
		else
		{
			return null;
		}
	}

//	private static class Catcher<T> implements Function<ResourceLocation, SetTag<T>>
//	{
//		protected SetTag<T> tag;
//		private final Function<ResourceLocation, SetTag<T>> resolver;
//
//		public Catcher(Function<ResourceLocation, SetTag<T>> resolver)
//		{
//			super();
//			this.resolver = resolver;
//		}
//
//		@Override
//		public SetTag<T> apply(ResourceLocation t)
//		{
//			tag = resolver.apply(t);
//			return tag;
//		}
//	}

	public ChunkData(LevelAccessor w, BlockPos pos, Map<String, Integer> data)
	{
		if(w==null)
			throw new NullPointerException("World is null");

		this.w = w;
		coords = pos;
		ores = data;
		chunk = null;
	}

	protected ChunkData(LevelAccessor w, BlockPos pos, BlockState[][][] data)
	{
		if(w==null)
			throw new NullPointerException("World is null");

		this.w = w;
		coords = pos;
		chunk = data;
		ores = null;
	}

	/**
	 * @param w is closed at the end.
	 * @throws IOException
	 */
	protected void save(BufferedWriter w) throws IOException
	{
		for(Entry<String, Integer> e : getMap().entrySet())
		{
			w.write(e.getKey() + "=" + Integer.toString(e.getValue()));
			w.newLine();
		}
		w.flush();
		w.close();
	}

	/**
	 * @param r is closed at the end.
	 * @throws IOException
	 */
	public void load(BufferedReader r) throws IOException
	{
		String s = r.readLine();
		while(s!=null)
		{
			String[] parts = s.split("=");
			if(parts.length!=2)
			{
				throw new IllegalStateException("wrong format of line data: '" + s + "'");
			}
			getMap().put(parts[0], Integer.valueOf(parts[1]));
			s = r.readLine();
		}
		r.close();
	}

	protected String getFilename()
	{
		return ( (coords.getX()>>4) & 31 ) + "_" + ( (coords.getZ()>>4) & 31 );
	}

	protected URI file()
	{
		File dir = HelperChunks.getDimensionDir(w);
		dir = new File(dir, "ores");
		dir.mkdirs();

		return URI.create("jar:" + dir.toURI() + String.format("c_%s_%s.dat", coords.getX()>>9, coords.getZ()>>9));
	}

	public Map<String, Integer> getMap()
	{
		if(ores!=null)
			return ores;
		if(chunk!=null)
		{
			ores = new HashMap<String, Integer>();

			for(int x=0;x<chunk.length;x++)
			{
				for(int y=0;y<chunk[x].length;y++)
				{
					for(int z=0;z<chunk[x][y].length;z++)
					{
						BlockState state = chunk[x][y][z] ;

						String ore = getOre(w, state);
						if(ore!=null)
						{
							Integer now = ores.get(ore);
							if(now==null)
							{
								now = 0;
							}
							ores.put(ore, now + 1);
						}
					}
				}
			}
		}
		return ores;
	}

	public int getTotalOres()
	{
		if(totalOres!=-1)
			return totalOres;
		else
		{
			Map<String, Integer> ores = getMap();
			totalOres = 0;
			ores.entrySet().forEach(e -> {totalOres += e.getValue();});
			return totalOres;
		}
	}

	/**
	 * @param random between 0 and <total ore size>
	 * @return a resourceLocation to a block tag
	 */
	public String getRandomOre(int random)
	{
		if(random<0)
			return null;
		Map<String, Integer> ores = getMap();
		for(Entry<String, Integer> e : ores.entrySet())
		{
			random -= e.getValue();
			if(random<0)
				return e.getKey();
		}

		throw new IndexOutOfBoundsException("Random number is " + random + " after removing all ores from it");
	}

	public BlockPos getCoords()
	{
		return coords;
	}
}
