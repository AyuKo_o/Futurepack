//package futurepack.world;
//
//import java.util.List;
//import java.util.Random;
//import java.util.Set;
//import java.util.UUID;
//import java.util.function.Predicate;
//import java.util.stream.Stream;
//
//import net.minecraft.block.Block;
//import net.minecraft.block.BlockState;
//import net.minecraft.entity.Entity;
//import net.minecraft.entity.EntityPredicate;
//import net.minecraft.entity.LivingEntity;
//import net.minecraft.entity.player.PlayerEntity;
//import net.minecraft.fluid.Fluid;
//import net.minecraft.fluid.FluidState;
//import net.minecraft.particles.IParticleData;
//import net.minecraft.tileentity.TileEntity;
//import net.minecraft.util.Direction;
//import net.minecraft.util.SoundCategory;
//import net.minecraft.util.SoundEvent;
//import net.minecraft.util.math.AxisAlignedBB;
//import net.minecraft.util.math.BlockPos;
//import net.minecraft.util.math.BlockRayTraceResult;
//import net.minecraft.util.math.RayTraceContext;
//import net.minecraft.util.math.shapes.VoxelShape;
//import net.minecraft.util.math.vector.Vector3d;
//import net.minecraft.world.Difficulty;
//import net.minecraft.world.DifficultyInstance;
//import net.minecraft.world.ITickList;
//import net.minecraft.world.IWorld;
//import net.minecraft.world.LightType;
//import net.minecraft.world.World;
//import net.minecraft.world.biome.Biome;
//import net.minecraft.world.biome.BiomeManager;
//import net.minecraft.world.border.WorldBorder;
//import net.minecraft.world.chunk.AbstractChunkProvider;
//import net.minecraft.world.chunk.ChunkStatus;
//import net.minecraft.world.chunk.IChunk;
//import net.minecraft.world.gen.Heightmap.Type;
//import net.minecraft.world.lighting.WorldLightManager;
//import net.minecraft.world.storage.WorldInfo;
//
//public class WorldWrapperBase implements IWorld {
//	public final IWorld delegate;
//
//	public WorldWrapperBase(IWorld delegate) {
//		super();
//		this.delegate = delegate;
//	}
//
//	@Override
//	public boolean setBlockState(BlockPos pos, BlockState newState, int flags) {
//		return delegate.setBlockState(pos, newState, flags);
//	}
//
//	@Override
//	public boolean hasBlockState(BlockPos p_217375_1_, Predicate<BlockState> p_217375_2_) {
//		return delegate.hasBlockState(p_217375_1_, p_217375_2_);
//	}
//
//	@Override
//	public Biome getBiome(BlockPos pos) {
//		return delegate.getBiome(pos);
//	}
//
//	@Override
//	public int getLightFor(LightType type, BlockPos pos) {
//		return delegate.getLightFor(type, pos);
//	}
//
//	@Override
//	public int getMaxHeight() {
//		return delegate.getMaxHeight();
//	}
//
//	@Override
//	public TileEntity getTileEntity(BlockPos pos) {
//		return delegate.getTileEntity(pos);
//	}
//
//	@Override
//	public BlockState getBlockState(BlockPos pos) {
//		return delegate.getBlockState(pos);
//	}
//
//	@Override
//	public FluidState getFluidState(BlockPos pos) {
//		return delegate.getFluidState(pos);
//	}
//
//	@Override
//	public List<Entity> getEntitiesInAABBexcluding(Entity entityIn, AxisAlignedBB boundingBox,
//			Predicate<? super Entity> predicate) {
//		return delegate.getEntitiesInAABBexcluding(entityIn, boundingBox, predicate);
//	}
//
//	@Override
//	public boolean removeBlock(BlockPos pos, boolean isMoving) {
//		return delegate.removeBlock(pos, isMoving);
//	}
//
//	@Override
//	public int getLightValue(BlockPos pos) {
//		return delegate.getLightValue(pos);
//	}
//
//	@Override
//	public boolean destroyBlock(BlockPos pos, boolean dropBlock) {
//		return delegate.destroyBlock(pos, dropBlock);
//	}
//
//	@Override
//	public int getMaxLightLevel() {
//		return delegate.getMaxLightLevel();
//	}
//
//	@Override
//	public int getHeight() {
//		return delegate.getHeight();
//	}
//
//	@Override
//	public long getSeed() {
//		return delegate.getSeed();
//	}
//
//	@Override
//	public <T extends Entity> List<T> getEntitiesWithinAABB(Class<? extends T> clazz, AxisAlignedBB aabb,
//			Predicate<? super T> filter) {
//		return delegate.getEntitiesWithinAABB(clazz, aabb, filter);
//	}
//
//	@Override
//	public boolean addEntity(Entity entityIn) {
//		return delegate.addEntity(entityIn);
//	}
//
//	@Override
//	public BlockRayTraceResult rayTraceBlocks(RayTraceContext context) {
//		return delegate.rayTraceBlocks(context);
//	}
//
//	@Override
//	public float getCurrentMoonPhaseFactor() {
//		return delegate.getCurrentMoonPhaseFactor();
//	}
//
//	@Override
//	public <T extends Entity> List<T> getLoadedEntitiesWithinAABB(Class<? extends T> p_225316_1_, AxisAlignedBB p_225316_2_,
//			Predicate<? super T> p_225316_3_) {
//		return delegate.getLoadedEntitiesWithinAABB(p_225316_1_, p_225316_2_, p_225316_3_);
//	}
//
//	@Override
//	public boolean isAirBlock(BlockPos pos) {
//		return delegate.isAirBlock(pos);
//	}
//
//	@Override
//	public float getCelestialAngle(float partialTicks) {
//		return delegate.getCelestialAngle(partialTicks);
//	}
//
//	@Override
//	public List<? extends PlayerEntity> getPlayers() {
//		return delegate.getPlayers();
//	}
//
//	@Override
//	public List<Entity> getEntitiesWithinAABBExcludingEntity(Entity entityIn, AxisAlignedBB bb) {
//		return delegate.getEntitiesWithinAABBExcludingEntity(entityIn, bb);
//	}
//
//	@Override
//	public int getMoonPhase() {
//		return delegate.getMoonPhase();
//	}
//
//	@Override
//	public boolean canBlockSeeSky(BlockPos pos) {
//		return delegate.canBlockSeeSky(pos);
//	}
//
//	@Override
//	public ITickList<Block> getPendingBlockTicks() {
//		return delegate.getPendingBlockTicks();
//	}
//
//	@Override
//	public ITickList<Fluid> getPendingFluidTicks() {
//		return delegate.getPendingFluidTicks();
//	}
//
//	@Override
//	public World getWorld() {
//		return delegate.getWorld();
//	}
//
//	@Override
//	public WorldInfo getWorldInfo() {
//		return delegate.getWorldInfo();
//	}
//
//	@Override
//	public DifficultyInstance getDifficultyForLocation(BlockPos pos) {
//		return delegate.getDifficultyForLocation(pos);
//	}
//
//	@Override
//	public Difficulty getDifficulty() {
//		return delegate.getDifficulty();
//	}
//
//	@Override
//	public AbstractChunkProvider getChunkProvider() {
//		return delegate.getChunkProvider();
//	}
//
//	@Override
//	public boolean chunkExists(int chunkX, int chunkZ) {
//		return delegate.chunkExists(chunkX, chunkZ);
//	}
//
//	@Override
//	public Random getRandom() {
//		return delegate.getRandom();
//	}
//
//	@Override
//	public void notifyNeighborsOfStateChange(BlockPos pos, Block blockIn) {
//		delegate.notifyNeighborsOfStateChange(pos, blockIn);
//	}
//
//	@Override
//	public BlockPos getSpawnPoint() {
//		return delegate.getSpawnPoint();
//	}
//
//	@Override
//	public int getLightSubtracted(BlockPos pos, int amount) {
//		return delegate.getLightSubtracted(pos, amount);
//	}
//
//	@Override
//	public <T extends Entity> List<T> getEntitiesWithinAABB(Class<? extends T> p_217357_1_, AxisAlignedBB p_217357_2_) {
//		return delegate.getEntitiesWithinAABB(p_217357_1_, p_217357_2_);
//	}
//
//	@Override
//	public IChunk getChunk(int x, int z, ChunkStatus requiredStatus, boolean nonnull) {
//		return delegate.getChunk(x, z, requiredStatus, nonnull);
//	}
//
//	@Override
//	public void playSound(PlayerEntity player, BlockPos pos, SoundEvent soundIn, SoundCategory category, float volume,
//			float pitch) {
//		delegate.playSound(player, pos, soundIn, category, volume, pitch);
//	}
//
//	@Override
//	public BlockRayTraceResult rayTraceBlocks(Vector3d p_217296_1_, Vector3d p_217296_2_, BlockPos p_217296_3_,
//			VoxelShape p_217296_4_, BlockState p_217296_5_) {
//		return delegate.rayTraceBlocks(p_217296_1_, p_217296_2_, p_217296_3_, p_217296_4_, p_217296_5_);
//	}
//
//	@Override
//	public BlockPos getHeight(Type heightmapType, BlockPos pos) {
//		return delegate.getHeight(heightmapType, pos);
//	}
//
//	@Override
//	public <T extends Entity> List<T> getLoadedEntitiesWithinAABB(Class<? extends T> p_225317_1_, AxisAlignedBB p_225317_2_) {
//		return delegate.getLoadedEntitiesWithinAABB(p_225317_1_, p_225317_2_);
//	}
//
//	@Override
//	public int getHeight(Type heightmapType, int x, int z) {
//		return delegate.getHeight(heightmapType, x, z);
//	}
//
//	@Override
//	public void addParticle(IParticleData particleData, double x, double y, double z, double xSpeed, double ySpeed,
//			double zSpeed) {
//		delegate.addParticle(particleData, x, y, z, xSpeed, ySpeed, zSpeed);
//	}
//
//	@Override
//	public float getBrightness(BlockPos pos) {
//		return delegate.getBrightness(pos);
//	}
//
//	@Override
//	public void playEvent(PlayerEntity player, int type, BlockPos pos, int data) {
//		delegate.playEvent(player, type, pos, data);
//	}
//
//	@Override
//	public int getSkylightSubtracted() {
//		return delegate.getSkylightSubtracted();
//	}
//
//	@Override
//	public WorldBorder getWorldBorder() {
//		return delegate.getWorldBorder();
//	}
//
//	@Override
//	public void playEvent(int type, BlockPos pos, int data) {
//		delegate.playEvent(type, pos, data);
//	}
//
//	@Override
//	public int getStrongPower(BlockPos pos, Direction direction) {
//		return delegate.getStrongPower(pos, direction);
//	}
//
//	@Override
//	public Stream<VoxelShape> getEmptyCollisionShapes(Entity entityIn, AxisAlignedBB aabb,
//			Set<Entity> entitiesToIgnore) {
//		return delegate.getEmptyCollisionShapes(entityIn, aabb, entitiesToIgnore);
//	}
//
//	@Override
//	public boolean isRemote() {
//		return delegate.isRemote();
//	}
//
//	@Override
//	public int getSeaLevel() {
//		return delegate.getSeaLevel();
//	}
//
//	@Override
//	public IChunk getChunk(BlockPos pos) {
//		return delegate.getChunk(pos);
//	}
//
//	@Override
//	public boolean checkNoEntityCollision(Entity entityIn, VoxelShape shape) {
//		return delegate.checkNoEntityCollision(entityIn, shape);
//	}
//
//	@Override
//	public IChunk getChunk(int chunkX, int chunkZ) {
//		return delegate.getChunk(chunkX, chunkZ);
//	}
//
//	@Override
//	public IChunk getChunk(int chunkX, int chunkZ, ChunkStatus requiredStatus) {
//		return delegate.getChunk(chunkX, chunkZ, requiredStatus);
//	}
//
//	@Override
//	public PlayerEntity getClosestPlayer(double x, double y, double z, double distance, Predicate<Entity> predicate) {
//		return delegate.getClosestPlayer(x, y, z, distance, predicate);
//	}
//
//	@Override
//	public PlayerEntity getClosestPlayer(Entity p_217362_1_, double distance) {
//		return delegate.getClosestPlayer(p_217362_1_, distance);
//	}
//
//	@Override
//	public PlayerEntity getClosestPlayer(double x, double y, double z, double distance, boolean creativePlayers) {
//		return delegate.getClosestPlayer(x, y, z, distance, creativePlayers);
//	}
//
//	@Override
//	public PlayerEntity getClosestPlayer(double p_217365_1_, double p_217365_3_, double p_217365_5_) {
//		return delegate.getClosestPlayer(p_217365_1_, p_217365_3_, p_217365_5_);
//	}
//
//	@Override
//	public boolean isPlayerWithin(double x, double y, double z, double distance) {
//		return delegate.isPlayerWithin(x, y, z, distance);
//	}
//
//	@Override
//	public PlayerEntity getClosestPlayer(EntityPredicate p_217370_1_, LivingEntity p_217370_2_) {
//		return delegate.getClosestPlayer(p_217370_1_, p_217370_2_);
//	}
//
//	@Override
//	public PlayerEntity getClosestPlayer(EntityPredicate p_217372_1_, LivingEntity p_217372_2_, double p_217372_3_,
//			double p_217372_5_, double p_217372_7_) {
//		return delegate.getClosestPlayer(p_217372_1_, p_217372_2_, p_217372_3_, p_217372_5_, p_217372_7_);
//	}
//
//	@Override
//	public PlayerEntity getClosestPlayer(EntityPredicate p_217359_1_, double p_217359_2_, double p_217359_4_,
//			double p_217359_6_) {
//		return delegate.getClosestPlayer(p_217359_1_, p_217359_2_, p_217359_4_, p_217359_6_);
//	}
//
//	@Override
//	public <T extends LivingEntity> T getClosestEntityWithinAABB(Class<? extends T> p_217360_1_,
//			EntityPredicate p_217360_2_, LivingEntity p_217360_3_, double p_217360_4_, double p_217360_6_,
//			double p_217360_8_, AxisAlignedBB p_217360_10_) {
//		return delegate.getClosestEntityWithinAABB(p_217360_1_, p_217360_2_, p_217360_3_, p_217360_4_, p_217360_6_,
//				p_217360_8_, p_217360_10_);
//	}
//
//	@Override
//	public <T extends LivingEntity> T getNearestLoadedEntity(Class<? extends T> p_225318_1_, EntityPredicate p_225318_2_,
//			LivingEntity p_225318_3_, double p_225318_4_, double p_225318_6_, double p_225318_8_,
//			AxisAlignedBB p_225318_10_) {
//		return delegate.getNearestLoadedEntity(p_225318_1_, p_225318_2_, p_225318_3_, p_225318_4_, p_225318_6_, p_225318_8_,
//				p_225318_10_);
//	}
//
//	@Override
//	public <T extends LivingEntity> T getClosestEntity(List<? extends T> p_217361_1_, EntityPredicate p_217361_2_,
//			LivingEntity p_217361_3_, double p_217361_4_, double p_217361_6_, double p_217361_8_) {
//		return delegate.getClosestEntity(p_217361_1_, p_217361_2_, p_217361_3_, p_217361_4_, p_217361_6_, p_217361_8_);
//	}
//
//	@Override
//	public List<PlayerEntity> getTargettablePlayersWithinAABB(EntityPredicate p_217373_1_, LivingEntity p_217373_2_,
//			AxisAlignedBB p_217373_3_) {
//		return delegate.getTargettablePlayersWithinAABB(p_217373_1_, p_217373_2_, p_217373_3_);
//	}
//
//	@Override
//	public boolean hasWater(BlockPos pos) {
//		return delegate.hasWater(pos);
//	}
//
//	@Override
//	public boolean containsAnyLiquid(AxisAlignedBB bb) {
//		return delegate.containsAnyLiquid(bb);
//	}
//
//	@Override
//	public <T extends LivingEntity> List<T> getTargettableEntitiesWithinAABB(Class<? extends T> p_217374_1_,
//			EntityPredicate p_217374_2_, LivingEntity p_217374_3_, AxisAlignedBB p_217374_4_) {
//		return delegate.getTargettableEntitiesWithinAABB(p_217374_1_, p_217374_2_, p_217374_3_, p_217374_4_);
//	}
//
//	@Override
//	public PlayerEntity getPlayerByUuid(UUID uniqueIdIn) {
//		return delegate.getPlayerByUuid(uniqueIdIn);
//	}
//
//	@Override
//	public int getLight(BlockPos pos) {
//		return delegate.getLight(pos);
//	}
//
//	@Override
//	public int getNeighborAwareLightSubtracted(BlockPos pos, int amount) {
//		return delegate.getNeighborAwareLightSubtracted(pos, amount);
//	}
//
//	@Override
//	public boolean isBlockLoaded(BlockPos pos) {
//		return delegate.isBlockLoaded(pos);
//	}
//
//	@Override
//	public boolean isAreaLoaded(BlockPos center, int range) {
//		return delegate.isAreaLoaded(center, range);
//	}
//
//	@Override
//	public boolean isAreaLoaded(BlockPos from, BlockPos to) {
//		return delegate.isAreaLoaded(from, to);
//	}
//
//	@Override
//	public boolean isAreaLoaded(int p_217344_1_, int p_217344_2_, int p_217344_3_, int p_217344_4_, int p_217344_5_,
//			int p_217344_6_) {
//		return delegate.isAreaLoaded(p_217344_1_, p_217344_2_, p_217344_3_, p_217344_4_, p_217344_5_, p_217344_6_);
//	}
//
//	@Override
//	public Dimension getDimension() {
//		return delegate.getDimension();
//	}
//
//	@Override
//	public BiomeManager getBiomeManager() {
//		return delegate.getBiomeManager();
//	}
//
//	@Override
//	public Biome getNoiseBiomeRaw(int x, int y, int z) {
//		return delegate.getNoiseBiomeRaw(x, y, z);
//	}
//	
//	@Override
//	public WorldLightManager getLightManager() {
//		return delegate.getLightManager();
//	}
//
//	@Override
//	public boolean destroyBlock(BlockPos p_225521_1_, boolean p_225521_2_, Entity p_225521_3_) {
//		return delegate.destroyBlock(p_225521_1_, p_225521_2_, p_225521_3_);
//	}
//
//}
