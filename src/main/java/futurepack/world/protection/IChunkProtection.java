package futurepack.world.protection;

import net.minecraft.core.BlockPos;

public interface IChunkProtection
{
	public static final byte BLOCK_NOT_BREAKABLE = 1;	// 000 0001
	public static final byte BLOCK_NOT_PLACABLE  = 2;	// 000 0010
	public static final byte RESERVED_1 = 4; 			// 000 0100
	public static final byte RESERVED_2 = 8;	 		// 000 1000
	public static final byte RESERVED_3 = 16;			// 001 0000
	public static final byte RESERVED_4 = 32;			// 010 0000
	public static final byte RESERVED_5 = 64;			// 100 0000
	
	public byte getRawProtectionState(BlockPos pos);
	
	public void setRawProtectionState(BlockPos pos, byte state);
	
	public boolean hasChunkProtection();
	
	
	public static int getPosition(int x, int y, int z)
	{
		x %=16;
		z %=16;
		y %=256;
		
		return x + z*16 + y*256;
	}
	
	public static int getPosition(BlockPos pos)
	{
		return getPosition(pos.getX(), pos.getY(), pos.getZ());
	}
	
	public static boolean check(byte state, byte flag)
	{
		return (state & flag) == flag;
	}
}
