package futurepack.common.dim.structures;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;

public class OpenDoor implements Comparable<OpenDoor>
{
	protected BlockPos pos;
	protected Direction direction;
	protected int w,h,d;
	
	protected EnumDoorType type;
	
	public OpenDoor(BlockPos pos, Direction direction, int w, int h, int d, EnumDoorType type)
	{
		super();
		this.pos = pos;
		this.direction = direction;
		this.w = w;
		this.h = h;
		this.d = d;
		this.type = type;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if(o==null)
			return false;
		else if(o instanceof OpenDoor)	
		{
			OpenDoor od = (OpenDoor) o;
			return pos.equals(od.pos) && direction==od.direction && w==od.w && h==od.h && d==od.d && type==od.type;
		}
		else
			return false;
	}

	@Override
	public String toString()
	{
		return w+"x"+h+"x"+d + " Door("+direction.name()+", " + type.toString() + ", " + pos + ")";
	}
	
	
	public boolean isSameSizeAndType(OpenDoor door)
	{
		return w==door.w && h==door.h && d==door.d && type == door.type;
	}
	
	public OpenDoor offset(Vec3i offset)
	{
		return offset(offset.getX(), offset.getY(), offset.getZ());
	}
	
	public OpenDoor offset(int x, int y, int z)
	{
		return new OpenDoor(pos.offset(x,y,z), direction, w, h, d, this.type);
	}

	public BlockPos getPos()
	{
		return pos;
	}

	public Direction getDirection()
	{
		return direction;
	}

	public int getWeidth()
	{
		return w;
	}

	public int getHeight()
	{
		return h;
	}

	public int getDepth()
	{
		return d;
	}

	@Override
	public int compareTo(OpenDoor t)
	{
		return hashCode() - t.hashCode();
	}
	
	public OpenDoor createMatchingDoor()
	{
		return new OpenDoor(pos.relative(direction), direction.getOpposite(), w, h, d, this.type);	
	}
	
	
}
