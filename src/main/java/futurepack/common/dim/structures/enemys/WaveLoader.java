package futurepack.common.dim.structures.enemys;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import net.minecraft.nbt.CompoundTag;

public class WaveLoader
{
	
	public static EnemyWave createWaveFromId(String s)
	{
		InputStream in = ClassLoader.getSystemClassLoader().getResourceAsStream("futurepack/common/dim/structures/enemys/" + s + ".json");
		if(in==null)
		{
			in = WaveLoader.class.getResourceAsStream(s + ".json");
		}
		if(in!=null)
		{
			Gson gson = new Gson();
			JsonObject jo = gson.fromJson(new InputStreamReader(in), JsonObject.class);
			try
			{
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			EnemyWave wave = new EnemyWave(jo, s);
			return wave;
		}
		throw new IllegalArgumentException(s + ".json Not Founed!");
	}
	
	public static EnemyWave createWaveFromNBT(CompoundTag nbt)
	{
		EnemyWave wave = createWaveFromId(nbt.getString("id"));
		wave.deserializeNBT(nbt);
		return wave;
	}
	
	public static CompoundTag waveToNBT(EnemyWave wave)
	{
		return wave.serializeNBT();
	}
}
