package futurepack.common.dim.structures;

import java.util.Random;

import futurepack.api.Constants;
import futurepack.common.FPLog;
import futurepack.common.block.inventory.TileEntityCompositeChest;
import futurepack.common.block.inventory.TileEntityWardrobe;
import futurepack.common.block.logistic.IInvWrapper;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.data.BuiltinRegistries;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.Container;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.BarrelBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.ChestBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.RandomizableContainerBlockEntity;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.LootTables;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class DungeonChest
{
	protected final BlockPos pos;
	protected final ResourceLocation loottable;
	protected final StructureBase base;
	
	public boolean lootr_support = true;
	
	public DungeonChest(BlockPos pos, ResourceLocation res, StructureBase base)
	{
		super();
		this.pos = pos;
		loottable = res;
		this.base = base;
	}
	
	public DungeonChest(BlockPos pos, StructureBase base)
	{
		this(pos, new ResourceLocation(Constants.MOD_ID), base);
	}	
	
	public void genLoot(ServerLevel w, Random rand, BlockPos start, CompoundTag nbt)
	{
		genLoot(w, rand, start, nbt, w.getServer().getLootTables());
	}	
	
	public static RegistryObject<Block> lootr_chest = RegistryObject.createOptional(new ResourceLocation("lootr:lootr_chest"), ForgeRegistries.Keys.BLOCKS, Constants.MOD_ID);
	public static RegistryObject<Block> lootr_barrel = RegistryObject.createOptional(new ResourceLocation("lootr:lootr_barrel"), ForgeRegistries.Keys.BLOCKS, Constants.MOD_ID);

	public void genLoot(ServerLevelAccessor w, Random rand, BlockPos start, CompoundTag nbt, LootTables manager)
	{
		BlockPos lootpos = start.offset(pos);
		BlockEntity tile =  w.getBlockEntity(lootpos);	    	
		if(tile==null)
		{
			FPLog.logger.error("The TileEntity is null! This mistake is in " + base);
			return;
		}
		
		if(lootr_support && (lootr_chest.isPresent() || lootr_barrel.isPresent()) && ModList.get().isLoaded("lootr"))
		{
			if(tile instanceof TileEntityWardrobe)
			{
				if(w.isStateAtPosition(lootpos.above(), s -> s.isAir()) && lootr_chest.isPresent())
				{
					HelperItems.disableItemSpawn();
					w.setBlock(lootpos, lootr_chest.get().defaultBlockState(), 3);
					tile = w.getBlockEntity(lootpos);
					HelperItems.enableItemSpawn();
				}
				else if(lootr_barrel.isPresent())
				{
					HelperItems.disableItemSpawn();
					w.setBlock(lootpos, lootr_barrel.get().defaultBlockState().setValue(BarrelBlock.FACING, Direction.UP), 3);
					tile = w.getBlockEntity(lootpos);
					HelperItems.enableItemSpawn();
				}
				
			}
			if(tile instanceof TileEntityCompositeChest)
			{
				if(lootr_chest.isPresent())
				{
					HelperItems.disableItemSpawn();
					Direction rot = tile.getBlockState().getValue(ChestBlock.FACING);
					w.setBlock(lootpos, lootr_chest.get().defaultBlockState().setValue(ChestBlock.FACING, rot), 3);
					tile = w.getBlockEntity(lootpos);
					HelperItems.enableItemSpawn();
				}
			}
		}
		
		
		LootTable loottable;
		int lvl = nbt.getInt("tecLevel");
		ResourceLocation res = new ResourceLocation(this.loottable+"_"+lvl);
		loottable = manager.get(res);
		
		if(loottable==LootTable.EMPTY)
		{
			loottable = manager.get(this.loottable);
			
			if(loottable==LootTable.EMPTY)
			{
				FPLog.logger.error("Missing Loot Table detected: " + res.toString() + " and " + this.loottable.toString());
			}
		}
		
		if(lootr_support && tile instanceof RandomizableContainerBlockEntity lootr)
		{
			loottable.getLootTableId();
			lootr.setLootTable(loottable.getLootTableId(), rand.nextLong());
			return;
		}
		
		
		Container inv = null;
		if(tile instanceof Container)
		{
			inv = (Container) tile;
		}
		else
		{
			inv = new IInvWrapper(tile);
		}
			
		
		
		LootContext.Builder builder = new LootContext.Builder((ServerLevel)w.getLevel());
		builder.withParameter(LootContextParams.ORIGIN, Vec3.atCenterOf(lootpos));
		
		loottable.fill(inv, builder.create(LootContextParamSets.CHEST));
		
		lvl = nbt.getInt("level");
		if(lvl>=3)
		{
			if(rand.nextInt(100) < 5)
			{
				res = new ResourceLocation(Constants.MOD_ID, "loot_tables/chests/blueprints/laserbow");
				loottable = manager.get(this.loottable);
				loottable.fill(inv, builder.create(LootContextParamSets.CHEST));
			}
		}
	}
}
