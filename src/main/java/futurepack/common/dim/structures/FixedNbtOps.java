package futurepack.common.dim.structures;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.PeekingIterator;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.serialization.DynamicOps;
import com.mojang.serialization.JsonOps;

import net.minecraft.nbt.ByteArrayTag;
import net.minecraft.nbt.ByteTag;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.DoubleTag;
import net.minecraft.nbt.EndTag;
import net.minecraft.nbt.FloatTag;
import net.minecraft.nbt.IntArrayTag;
import net.minecraft.nbt.IntTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.LongArrayTag;
import net.minecraft.nbt.LongTag;
import net.minecraft.nbt.NbtOps;
import net.minecraft.nbt.NumericTag;
import net.minecraft.nbt.ShortTag;
import net.minecraft.nbt.Tag;

public class FixedNbtOps extends NbtOps
{
	public static final FixedNbtOps INSTANCE = new FixedNbtOps();


	@Override
	public <U> U convertTo(DynamicOps<U> otherOps, Tag tag)
	{
		U result = super.convertTo(otherOps, tag);

		if(result instanceof JsonObject elm && tag instanceof CompoundTag ct)
		{
			CompoundTag recreated = (CompoundTag) otherOps.convertTo(this, result);

			JsonObject diff = createTypeDiff(ct, recreated);

			elm.add("_type", diff);
		}

		return result;
	}

	public static JsonObject createTypeDiff(CompoundTag original, CompoundTag recreated)
	{
		JsonObject diff = new JsonObject();

		for(String key : original.getAllKeys())
		{
			Tag thisTag = original.get(key);
			Tag otherTag = recreated.get(key);

			if(thisTag.getId() != otherTag.getId())
			{
				diff.addProperty(key, thisTag.getId());
			}

			if(thisTag instanceof CompoundTag)
			{
				JsonObject innerDiff = createTypeDiff((CompoundTag) thisTag, (CompoundTag)otherTag);
				diff.add(key, innerDiff);
			}
		}

		return diff;
	}

	public Tag createList(Stream<Tag> p_129052_)
	{
		Tag[] tags = p_129052_.toArray(Tag[]::new);

		if (tags.length == 0)
			return new ListTag();

		int type = Arrays.stream(tags).mapToInt(tag -> {
			if (tag instanceof ByteTag)
			{
				return 1;
			}
			if (tag instanceof IntTag)
			{
				return 2;
			}
			if (tag instanceof LongTag)
			{
				return 3;
			} else
			{
				return 4;
			}
		}).max().orElse(4);

		PeekingIterator<Tag> peekingiterator = Iterators.peekingIterator(Arrays.stream(tags).iterator());

		switch (type)
		{
		case 1:
			List<Byte> list2 = Lists.newArrayList(Iterators.transform(peekingiterator, (p_129142_) -> {
				return ((NumericTag) p_129142_).getAsByte();
			}));
			return new ByteArrayTag(list2);
		case 2:
			List<Integer> list1 = Lists.newArrayList(Iterators.transform(peekingiterator, (p_129140_) -> {
				return ((NumericTag) p_129140_).getAsInt();
			}));
			return new IntArrayTag(list1);
		case 3:
			List<Long> list = Lists.newArrayList(Iterators.transform(peekingiterator, (p_129138_) -> {
				return ((NumericTag) p_129138_).getAsLong();
			}));
			return new LongArrayTag(list);
		case 4:
		default:
			ListTag listtag = new ListTag();

			while (peekingiterator.hasNext())
			{
				Tag tag1 = peekingiterator.next();
				if (!(tag1 instanceof EndTag))
				{
					listtag.add(tag1);
				}
			}

			return listtag;
		}
	}

	public static void fixNbt(CompoundTag nbt)
	{
		if(nbt.contains("_type"))
		{
			CompoundTag diff = nbt.getCompound("_type");
			nbt.remove("_type");
			String[] keys = nbt.getAllKeys().toArray(String[]::new);
			for(String key : keys)
			{
				byte wantedType = diff.getByte(key);
				Tag have = nbt.get(key);

				if(have.getId() != wantedType)
				{
					Tag fixed = null;

					switch(wantedType)
					{
					case Tag.TAG_BYTE:
						fixed = ByteTag.valueOf( ((NumericTag)have).getAsByte());
						break;
					case Tag.TAG_SHORT:
						fixed = ShortTag.valueOf( ((NumericTag)have).getAsShort());
						break;
					case Tag.TAG_INT:
						fixed = IntTag.valueOf( ((NumericTag)have).getAsInt());
						break;
					case Tag.TAG_LONG:
						fixed = LongTag.valueOf( ((NumericTag)have).getAsLong());
						break;
					case Tag.TAG_FLOAT:
						fixed = FloatTag.valueOf( ((NumericTag)have).getAsFloat());
						break;
					case Tag.TAG_DOUBLE:
						fixed = DoubleTag.valueOf( ((NumericTag)have).getAsDouble());
						break;
					default:
						break;
					}


					if(fixed!=null)
					{
						nbt.remove(key);
						nbt.put(key, fixed);
						have = fixed;
					}
				}

				if(have instanceof CompoundTag ct)
				{
					fixNbt(ct);
				}
				else if(have instanceof ListTag list)
				{
					if(list.getElementType() == Tag.TAG_COMPOUND)
					{
						for(int i=0;i<list.size();i++)
						{
							fixNbt(list.getCompound(i));
						}
					}
				}
			}
		}
	}
}
