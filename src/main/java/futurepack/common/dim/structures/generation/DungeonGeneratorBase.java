package futurepack.common.dim.structures.generation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.dim.structures.DungeonChest;
import futurepack.common.dim.structures.NameGenerator;
import futurepack.common.dim.structures.OpenDoor;
import futurepack.common.dim.structures.StructureBase;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;

public class DungeonGeneratorBase
{
	public static final int MAX_TRIES = 100;


	Random rand;
	private ArrayList<Entry>[] entrys;

	private int rooms_min, rooms_random_max;

	private BakedDungeon baking;
	/**
	 * level: passed boss rooms/teclocks
	 * tecLevel: tec level for loot
	 * tecLocks: amount of tec locks alredy passed
	 * tecLockLevel: tec level for locks
	 */
	private CompoundTag specialData = new CompoundTag();


	public int maxTecLocks = 3;

	public DungeonGeneratorBase(long seed)
	{
		rand = new Random(seed);
		entrys = new ArrayList[EnumGenerationType.values().length];
		for(int i=0;i<entrys.length;i++)
		{
			entrys[i] = new ArrayList<Entry>();
		}

		rooms_min = 10;
		rooms_random_max = 15;
	}

	public void addEntry(StructureBase base, EnumGenerationType type, int weight)
	{
		Entry e = new Entry(base, type, weight);
		entrys[type.ordinal()].add(e);
	}

	private void clean()
	{
		baking = null;
		specialData = new CompoundTag();
	}

	public Entry getRandomEntry(List<Entry> entrys)
	{
		int totalWieght = 0;
		for(Entry e : entrys)
		{
			totalWieght += e.weight;
		}
		int random = rand.nextInt(totalWieght);
		for(Entry e : entrys)
		{
			if(random>0)
			{
				random -= e.weight;
			}
			else
			{
				return e;
			}
		}

		return entrys.get(entrys.size()-1);
	}

	public Entry getRandomEntryMatchingDoor(List<Entry> entrys, OpenDoor matchingDoor)
	{
		ArrayList<Entry> copy = new ArrayList<Entry>(entrys);
		copy.removeIf(e -> {
			for(OpenDoor door : e.structure.getRawDoors())
			{
				if(matchingDoor.isSameSizeAndType(door) && matchingDoor.getDirection() == door.getDirection())
					return false; //dont remove
			}
			return true; //remove this entry, there are no matching doors
		});
		if(copy.isEmpty())
			return null;
		return getRandomEntry(copy);
	}

	@Nullable
	public BakedDungeon generate()
	{
		int levels = 7 + rand.nextInt(5);
		int startTecLvl = 0;

		return generate(levels, startTecLvl);
	}

	@Nullable
	public BakedDungeon generate(int levels, int startTecLvl)
	{
		/* What is the goal:
		 * B - Bossroom (every time)
		 * b - Random number of Bossrooms >= 0
		 * T - Teclock (every time)
		 * E - Endroom (every time)
		 *
		 * Target Dungeon Structure: BbTBbTBbTBbE
		 * Tec Levels for chests:    000111222333
		 *
		 * Offset by startTecLvl is still possible
		 * Levels can be exceeded in an limited amount in rare cases, to finish dungeon properly
		 *
		 */

		int placedLocks = 0;

		baking = new BakedDungeon(rand);
		List<OpenDoor> smalOpenDoors = new ArrayList<OpenDoor>();


		//Adding entrace
		Entry e = getRandomEntry(entrys[EnumGenerationType.ENTRACE.ordinal()]);
		baking.addStructure(e.structure, new BlockPos(0,0,0), specialData);
		final String name = NameGenerator.DUNGEON.getRandomName(rand);

		//Between tow TecLocks a bossroom should be placed
		boolean forceBossRoom = true;

		Levels:
		for(int lvl=0;lvl<levels;lvl++)
		{
			specialData = new CompoundTag(); //we make a new instance to prevent overwriting data
			specialData.putString("name", name);

			//Force the generation of an TecLock after current segment
			boolean forceTecLock = false;

			//Finish Dungeon as soon as possible (out of levels)	-- hard condition
			if(lvl+2 >= levels)
			{
				if(startTecLvl < maxTecLocks)
				{
					//startTecLvl = 2;
					forceTecLock = true;

					//Not enough levels left to finish well, add more
					levels += (maxTecLocks - startTecLvl - 1) * 2;
				}
			}

			//Prevent generating to much bosses in a row, which can result in out of levels, before the dungeon is finished -- soft condition
			if(lvl > placedLocks * (levels / maxTecLocks) + 2)
			{
				forceTecLock = true;
			}

			specialData.putInt("level", lvl);
			specialData.putInt("tecLocks", placedLocks);
			specialData.putInt("tecLockLevel", startTecLvl);

			if(placedLocks>0)
			{
				specialData.putInt("tecLevel", startTecLvl);
			}
			else
			{
				specialData.putInt("tecLevel", 0);
			}

			int parts = rooms_min + rand.nextInt(rooms_random_max);
			generateRandomParts(parts);

			//find doors
			//boss raum spawnen
			//close doors

			baking.doors.sort(new Comparator<OpenDoor>()
			{
				@Override
				public int compare(OpenDoor o1, OpenDoor o2)
				{
					return getDistanceToWall(o2.getPos()) - getDistanceToWall(o1.getPos());
				}
			});

			for(int i=0;i<10;i++)
			{
				Entry boss = null;
				boolean isTecLock = false;
				if(forceTecLock)
				{
					boss = getRandomEntry(entrys[EnumGenerationType.TECLOCK.ordinal()]);
					isTecLock = true;
				}
				else if(!forceBossRoom && startTecLvl < maxTecLocks && rand.nextBoolean())
				{
					boss = getRandomEntry(entrys[EnumGenerationType.TECLOCK.ordinal()]);
					isTecLock = true;
				}
				else
				{
					boss = getRandomEntry(entrys[EnumGenerationType.BOSS.ordinal()]);
				}

				for(OpenDoor first : baking.doors)
				{
					OpenDoor nextToBoss = first.createMatchingDoor();
					BlockPos bossPos = getPosForDoor(nextToBoss, boss.structure, true);
					if(bossPos!=null && baking.isSpaceAvailable(boss.structure.getBoundingBox(bossPos)))
					{
						BakedDungeon test = baking.clone();
						{
							test.addStructure(boss.structure, bossPos, specialData);
							OpenDoor bossDoor = test.doors.get(test.doors.size()-1); //Warning: make sure the last door entry is the exit

							BlockPos pos = bossDoor.getPos().relative(bossDoor.getDirection());

							int w = bossDoor.getWeidth();
							int h = bossDoor.getHeight();
							int d = bossDoor.getDepth();

							switch (bossDoor.getDirection().getAxis())
							{
							case X: w*=bossDoor.getDirection().getStepX();break;
							case Y: h*=bossDoor.getDirection().getStepY();break;
							case Z: d*=bossDoor.getDirection().getStepZ();break;
							}

							BoundingBox box = BoundingBox.fromCorners(pos, pos.offset(w,h,d));
							if(test.isSpaceAvailable(box))
							{
								baking.clear();
								baking = test;
								test = null;
							}
							else
							{
								continue;
							}

						}
						OpenDoor bossDoor = baking.doors.get(baking.doors.size()-1); //Warning: make sure the last door entry is the exit

						closeAllDoors(bossDoor);
						List<OpenDoor> stillOpen = baking.doors;
						stillOpen.remove(bossDoor);
						smalOpenDoors.addAll(stillOpen);

						baking.doors = new ArrayList<OpenDoor>();
						baking.doors.add(bossDoor);

						if(isTecLock)
						{
							startTecLvl++;
							placedLocks++;
						}

						//force one BossRoom after an TecLock
						forceBossRoom = isTecLock;

						continue Levels;
					}
				}
			}
			System.out.println("Cant gen Boss Room for Lvl " + lvl);
			//break Levels; //Boss cant get spawned
			return null;
		}

		int parts = rooms_min + rand.nextInt(rooms_random_max);
		generateRandomParts(parts);

		boolean spawned = generateEndRoom();
		if(!spawned)
		{
			System.out.println("Could not spawn end room");
			return null;
		}



		closeAllDoors();
		closeClean(smalOpenDoors);
		if(!baking.doors.isEmpty())
		{
			closeAllDoors();
			if(!baking.doors.isEmpty())
			{
				closeClean(baking.doors);
			}
		}

		BakedDungeon finished = baking;
		clean();
		return finished;
	}

	public int getDistanceToWall(BlockPos pos)
	{
		BoundingBox box = baking.totalBox;
		int dx = Math.min(box.maxX()-pos.getX(), pos.getX()-box.minX());
		int dy = Math.min(box.maxY()-pos.getY(), pos.getY()-box.minY());
		int dz = Math.min(box.maxZ()-pos.getZ(), pos.getZ()-box.minZ());

		return Math.min(dx, Math.min(dy, dz));
	}

	public void generateRandomParts(int parts)
	{
		int fails=0;
		while(parts>0)
		{
			OpenDoor[] openDoors = baking.doors.toArray(new OpenDoor[baking.doors.size()]);
			for(OpenDoor open : openDoors)
			{
				OpenDoor next = open.createMatchingDoor();
				Entry ent = getRandomEntryMatchingDoor(genSaveSpawnList(parts>3), next);
				if(ent != null)
				{
					BlockPos pos=getPosForDoor(next, ent.structure, false);
					if(pos!=null)
					{
						BoundingBox box = ent.structure.getBoundingBox(pos);
						if(baking.isSpaceAvailable(box))
						{
							baking.addStructure(ent.structure, pos, specialData);
							parts--;
							fails=0;
							if(parts<=0)
								return;
						}
					}
					else
					{
						if(++fails>MAX_TRIES)
							return;
					}
				}
				else
				{
					if(++fails>MAX_TRIES)
						return;
				}

			}
			if(openDoors.length==0)
				return;
		}
	}

	private List<Entry> genSaveSpawnList(boolean noEndings)
	{
		List<Entry> l = this.entrys[EnumGenerationType.NORMAL.ordinal()];
		if(baking.doors.size()<=2 || noEndings)
		{
			l = new ArrayList<Entry>(l); //copy of the list
			l.removeIf(e -> e.structure.getRawDoors().length<=1);
			return l;
		}
		else
		{
			return l;
		}
	}

	public void closeAllDoors(OpenDoor...except)
	{
		Arrays.sort(except);

		List<OpenDoor> openDoors = new ArrayList<OpenDoor>(baking.doors);
		for(OpenDoor door : openDoors)
		{
			if(Arrays.binarySearch(except, door)>=0)
				continue;

			List<Entry> endings = getPossibleEndings();
			for(int i=0;i<10;i++)
			{
				OpenDoor next = door.createMatchingDoor();
				Entry e = getRandomEntryMatchingDoor(endings, next);
				if(e !=null)
				{
					BlockPos pos = getPosForDoor(next, e.structure, false);
					if(pos!=null)
					{
						baking.addStructure(e.structure, pos, specialData);
						break;
					}
					else
					{
						endings.remove(e);
						if(endings.isEmpty())
							break;
					}
				}
				else
				{
					endings.remove(e);
					if(endings.isEmpty())
						break;
				}
			}
		}
	}

	public BlockPos getPosForDoor(OpenDoor next, StructureBase structure, boolean isBoss)
	{
		//isBiss will ignore the last door because this is the exit
		for(int i=0;i<structure.getRawDoors().length-(isBoss?1:0);i++)
		{
			OpenDoor door = structure.getRawDoors()[i];
			if(next.isSameSizeAndType(door) && door.getDirection() == next.getDirection())
			{
				BlockPos pos = new BlockPos(-door.getPos().getX(), -door.getPos().getY(), -door.getPos().getZ());
		        pos = next.getPos().offset(pos); //WUGI
				if(baking.isSpaceAvailable(structure.getBoundingBox(pos)))
				{
					return pos;
				}
			}
		}

		return null;
	}

//	private List<Entry> getConnectionParts(boolean strict)
//	{
//		ArrayList<Entry> list = new ArrayList(entrys[EnumGenerationType.NORMAL.ordinal()]);
//		list.removeIf(new Predicate<Entry>()
//		{
//			@Override
//			public boolean test(Entry t)
//			{
//				OpenDoor[] doors = t.structure.getRawDoors();
//				if(doors==null || (strict && doors.length != 2) || (!strict && doors.length >= 2))
//					return true;
//
//				return doors[0].getDirection().getAxis() != doors[1].getDirection().getAxis();
//			}
//		});
//		return list;
//
//	}

	private List<Entry> getPossibleEndings()
	{
		ArrayList<Entry> list = new ArrayList<Entry>(entrys[EnumGenerationType.NORMAL.ordinal()]);
		list.removeIf(new Predicate<Entry>()
		{
			@Override
			public boolean test(Entry t)
			{
				OpenDoor[] doors = t.structure.getRawDoors();
				return doors==null || doors.length != 1;
			}
		});
		return list;
	}

	private void closeClean(List<OpenDoor> list)
	{
		for(OpenDoor door : list)
		{
			BlockPos pos = door.getPos();

			BlockState[][][] blocks = new BlockState[door.getWeidth()][door.getHeight()][door.getDepth()];
			for(int y=0;y<door.getHeight();y++)
			{
				BlockState state = DecoBlocks.color_iron_gray.get().defaultBlockState();

				int h = door.getHeight()/2;
				if(h==y)
				{
					state = DecoBlocks.color_iron_purple.get().defaultBlockState();
				}
				else if(h*2 == door.getHeight())
				{
					if(h+1 == y)
					{
						state = DecoBlocks.color_iron_purple.get().defaultBlockState();
					}
				}

				for(int x=0;x<door.getWeidth();x++)
				{
					for(int z=0;z<door.getDepth();z++)
					{
						blocks[x][y][z] = state;
					}
				}
			}

			StructureBase speedDoor = new StructureBase("generic_door", blocks);
			speedDoor.setChests(new DungeonChest[0]);
			speedDoor.setOpenDoors(new OpenDoor[0]);

			baking.addStructure(speedDoor, pos, specialData);

		}
	}

//	private static StructureBoundingBox getBox(DungeonRoom room)
//	{
//		return room.structure.getBoundingBox(room.pos);
//	}

	private boolean generateEndRoom()
	{
		Entry end = getRandomEntry(entrys[EnumGenerationType.ENDROOM.ordinal()]);
		for(int failes = 0;failes < 10;failes++)
		{
			for(OpenDoor first : baking.doors)
			{
				OpenDoor nextToBoss = first.createMatchingDoor();
				BlockPos bossPos = getPosForDoor(nextToBoss, end.structure, true);
				if(bossPos!=null && baking.isSpaceAvailable(end.structure.getBoundingBox(bossPos)))
				{
					baking.addStructure(end.structure, bossPos, specialData);
					return true;
				}
			}
			generateRandomParts(10);
		}
		return false;
	}
}
