package futurepack.common.dim.structures.generation;

public enum EnumGenerationType
{
	ENTRACE,
	BOSS,
	NORMAL,
	TECLOCK,
	ENDROOM;
}
