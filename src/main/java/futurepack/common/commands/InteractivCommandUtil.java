package futurepack.common.commands;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import futurepack.depend.api.helper.HelperItems;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.arguments.coordinates.Coordinates;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.phys.Vec3;

public class InteractivCommandUtil 
{
	private static InteractivCommandUtil currentProcess = null;
	
	public static int doBlockPosSelect(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new TextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == BlockPos.class)
		{
			Coordinates loc = src.getArgument("pos", Coordinates.class);
			currentProcess.result = 	loc.getBlockPos(src.getSource());
			return Command.SINGLE_SUCCESS;
		}
		else
		{
			src.getSource().sendFailure(new TextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	public static int doYesQuestion(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new TextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == boolean.class || currentProcess.expectedType == Boolean.class)
		{
			currentProcess.result = true;
			return Command.SINGLE_SUCCESS;
		}
		else
		{
			src.getSource().sendFailure(new TextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	public static int doNoQuestion(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new TextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == boolean.class || currentProcess.expectedType == Boolean.class)
		{
			currentProcess.result = false;
			return Command.SINGLE_SUCCESS;
		}
		else
		{
			src.getSource().sendFailure(new TextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	public static int doSkipQuestion(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new TextComponent("No structure porting in progress."));
			return 0;
		}
		
		currentProcess.result = new Object();
		return Command.SINGLE_SUCCESS;
	}
	
	public static int doAnswerQuestion(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new TextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == String.class)
		{
			currentProcess.result = src.getArgument("text", String.class);
			return Command.SINGLE_SUCCESS;
		}
		else
		{
			src.getSource().sendFailure(new TextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	public static int answerQuestionNoArgument(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		if(currentProcess==null)
		{
			src.getSource().sendFailure(new TextComponent("No structure porting in progress."));
			return 0;
		}
		
		if(currentProcess.expectedType == String.class)
		{
			for(ItemStack it : src.getSource().getEntity().getHandSlots())
			{
				if(!it.isEmpty())
				{
					currentProcess.result = HelperItems.getRegistryName(it.getItem()).toString();
					return Command.SINGLE_SUCCESS;
				}
			}
			src.getSource().sendFailure(new TextComponent("Must hold an item in the hands."));
			return 0;
		}
		else
		{
			src.getSource().sendFailure(new TextComponent("Unexpected answer type."));
			return 0;
		}
	}
	
	
	private final CommandSourceStack interact;
	
	public InteractivCommandUtil(CommandSourceStack interact) 
	{
		super();
		this.interact = interact;
		
		if(currentProcess!=null)
			throw new IllegalStateException("one process is alreay running");
		
		currentProcess = this;
	}

	private Class<?> expectedType;
	private Object result;
	
	private boolean interrupted = false;
	
	@SuppressWarnings("unchecked")
	public <T> T waitForCommand(Class<T> type)
	{
		expectedType = type;
		result = null;
		
		while(result==null)
		{
			try 
			{
				Thread.sleep(50);//wait ms until try again
			} 
			catch (InterruptedException e) 
			{
				e.printStackTrace();
				interrupted = true;
				return null;
			}
		}
		return (T) result;
	}

	public void sendString(String string) 
	{
		interact.sendSuccess(new TextComponent(string), false);
	}

	public Vec3 getPos() 
	{
		return interact.getPosition();
	}

	public ServerLevel getWorld()
	{
		return interact.getLevel();
	}

	public void sendErrorMessage(TextComponent message)
	{
		interact.sendFailure(message);
	}

	public boolean isInterrupted() 
	{
		return interrupted;
	}

	public static void remove(InteractivCommandUtil util) 
	{
		if(currentProcess==util)
			currentProcess = null;
	}
}
