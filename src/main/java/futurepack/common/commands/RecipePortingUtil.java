package futurepack.common.commands;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import it.unimi.dsi.fastutil.objects.Object2IntMap;
import it.unimi.dsi.fastutil.objects.Object2IntRBTreeMap;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.DyeColor;
import net.minecraftforge.registries.ForgeRegistries;

public class RecipePortingUtil implements Runnable
{
	public static final Gson GSON_IN = new GsonBuilder().create();
	public static final Gson GSON_OUT = new GsonBuilder().setPrettyPrinting().create();
	
	private static RecipePortingUtil currentProcess;
	
	public static int doItemPorting(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		if(currentProcess!=null)
		{
			if(currentProcess.thread.isAlive())
			{
				src.getSource().sendFailure(new TextComponent("Recipe porting already in progress. Killing that"));
				currentProcess.thread.interrupt();
				return 0;
			}
			else
			{
				currentProcess = null;
			}
		}		
		currentProcess = new RecipePortingUtil(src.getSource());
		
		//starts thread, will look at "./old" and converts these strructure jsons to "converted" . Will make a mapping.json were all maping from old to new blocks are stored
		return Command.SINGLE_SUCCESS;
	}
	
	private RecipePortingUtil(CommandSourceStack src)
	{
		util = new InteractivCommandUtil(src);
		thread = new Thread(this);
		thread.setDaemon(true);
		thread.start();
	}
	
	private InteractivCommandUtil util;
	private Thread thread;
	
	@Override
	public void run() 
	{
		CustomRecipes toport = new CustomRecipes(util);
		VanillaRecipes van = new VanillaRecipes(util);
		toport.portFiles();
		van.portFiles();
		currentProcess = null;
		InteractivCommandUtil.remove(util);
	}
	
	@FunctionalInterface
	public interface FilePorting<T, U>
	{
	    void accept(T t, U u) throws Exception;
	}
	
	public static class VanillaRecipes
	{
		private	final ItemMapper mapper;
		private final InteractivCommandUtil interact;
		
		public VanillaRecipes(InteractivCommandUtil util) 
		{
			super();
			this.mapper = new ItemMapper(util);
			this.interact = util;
		}

		public void portFiles()
		{
			//portFiles(new File("./data_/futurepack/recipes"), new File("./data/futurepack/recipes"), this::portRecipeFile);
			portFiles(new File("./data_/futurepack/advancements"), new File("./data/futurepack/advancements"), this::portAdvancementFile);
			
		}
		
		public void portFiles(File inputdir, File outputdir, FilePorting<File, File> consume)
		{
			for(File f : inputdir.listFiles())
			{
				if(interact.isInterrupted())
					break;
				
				if(f.isFile() && f.getName().endsWith(".json"))
				{
					try
					{
						consume.accept(f, outputdir);
					}
					catch(Exception e)
					{
						interact.sendErrorMessage(new TextComponent(e.toString()));
						e.printStackTrace();
					}
				}
			}
			interact.sendString("Done");
		}
		
		public void portRecipeFile(File input, File outputdir) throws Exception
		{
			File output = new File(outputdir, input.getName());
			if(output.exists())
			{
				input.delete();
				return;
			}
			
			interact.sendString("Porting " + input.getName());
			FileReader read = new FileReader(input);
			JsonElement outputObj;
			if(input.getName().equals("_constants.json"))
			{
				JsonObject[] raw = GSON_IN.fromJson(read, JsonObject[].class);
				JsonArray arr = new JsonArray();
				for(JsonObject con : raw)
				{
					JsonObject ingredient = con.getAsJsonObject("ingredient");
					JsonObject ported = handleJSONsObj(ingredient);
					
					if(ported.has("item"))
					{
						ingredient = new JsonObject();
						ingredient.add("item", ported);
						ported = ingredient;
					}
					String s = con.remove("name").getAsString();
					ported.addProperty("name", transformConstantNames(s));
					arr.add(ported);
				}
				outputObj = arr;
			}
			else
			{
				JsonObject raw = GSON_IN.fromJson(read, JsonObject.class);
				System.out.println("\n\n"+GSON_OUT.toJson(raw)+ "\n\n");
				if(raw.has("key"))
				{
					JsonObject keys = raw.getAsJsonObject("key");
					
					JsonObject keysOut = new JsonObject();
					for(Entry<String, JsonElement> e : keys.entrySet())
					{
						keysOut.add(e.getKey(), handleJSONs(e.getValue()));
					}
					raw.remove("key");
					raw.add("key", keysOut);
					keys = null;
					JsonObject reciperesult = raw.remove("result").getAsJsonObject();
					raw.add("result", handleJSONs(reciperesult));
					
					outputObj = raw;
				}
				else
				{
					JsonArray ingreds = raw.remove("ingredients").getAsJsonArray();
					JsonArray out = new JsonArray();
					for(JsonElement elm : ingreds)
					{
						out.add(handleJSONs(elm));
					}
					raw.add("ingredients", out);
					
					JsonObject reciperesult = raw.remove("result").getAsJsonObject();
					raw.add("result", handleJSONs(reciperesult));
					
					outputObj = raw;
				}
			}
			FileWriter write = new FileWriter(output);
			GSON_OUT.toJson(outputObj, outputObj.isJsonObject()? JsonObject.class: JsonArray.class, GSON_OUT.newJsonWriter(write));
			write.close();
		}
		
		public static String transformConstantNames(String input)
		{
			String s = input;
			for(char c = 'A'; c <= 'Z';c++)
			{
				s = s.replace("" + c, ("_" +c).toLowerCase());
			}
			return s;
		}
		
		public JsonElement handleJSONs(JsonElement ingredient)
		{
			if(ingredient.isJsonObject())
				return handleJSONsObj(ingredient.getAsJsonObject());
			else if(ingredient.isJsonArray())
			{
				JsonArray arr = new JsonArray();
				for(JsonElement elm : ingredient.getAsJsonArray())
				{
					arr.add(handleJSONs(elm));
				}
				return arr;
			}
			else
			{
				throw new IllegalArgumentException("Unknwon ingredient format " + ingredient.toString());
			}
		}
		
		public JsonObject handleJSONsObj(JsonObject ingredient)
		{
			if(ingredient.has("type"))
			{
				String type = ingredient.get("type").getAsString();
				if(type.equals("forge:ore_dict"))
				{
					return forgeOreDict(ingredient);
				}
				else if(type.equals("minecraft:item_nbt") || type.equals("itemstack_nbt"))
				{
					JsonObject o = defaultType(ingredient);
					o.add("nbt", ingredient.get("nbt"));
					return o;
				}
				else
				{
					throw new IllegalArgumentException("Unknown type " + type);
				}
			}
			else
			{
				return defaultType(ingredient);
			}
		}
		
		private JsonObject defaultType(JsonObject ingredient)
		{
			int meta = 0;
			if(ingredient.has("data"))
				meta = ingredient.get("data").getAsInt();
			String item = ingredient.get("item").getAsString();
			JsonObject o = new JsonObject();
			if(item.startsWith("#"))
			{
				o.addProperty("constant", "futurepack:" +transformConstantNames(item.substring(1)));
			}
			else
			{
				o.addProperty("item", mapper.map(item, meta));
			}
			
			
			if(ingredient.has("count"))
			{
				o.add("count", ingredient.get("count"));
			}
			return o;
		}
		
		private JsonObject forgeOreDict(JsonObject ingredient)
		{
			String ore = ingredient.get("ore").getAsString();
			String tagName = oreDictToTagName(ore);
			JsonObject result = new JsonObject();
			result.addProperty("tag", tagName);
			return result;
		}
	
		
		public void portAdvancementFile(File input, File outputdir) throws Exception
		{ 
			File output = new File(outputdir, input.getName());
			if(output.exists())
			{
				input.delete();
				return;
			}
			
			interact.sendString("Porting " + input.getName());
			FileReader read = new FileReader(input);
			JsonElement outputObj;
			{
				JsonObject raw = GSON_IN.fromJson(read, JsonObject.class);
				System.out.println("\n\n"+GSON_OUT.toJson(raw)+ "\n\n");
				
				JsonObject display = raw.getAsJsonObject("display");
				JsonObject icon = display.getAsJsonObject("icon");
				icon = defaultType(icon);
				display.add("icon", icon);
				
				JsonObject criteria = raw.getAsJsonObject("criteria");
				
				for(Entry<String, JsonElement> e : criteria.entrySet())
				{
					if(e.getValue().isJsonObject())
					{
						JsonObject task = e.getValue().getAsJsonObject();
						String trigger = task.get("trigger").getAsString();
						if("minecraft:inventory_changed".equals(trigger))
						{
							JsonObject conditions = task.getAsJsonObject("conditions");
							JsonArray items = conditions.getAsJsonArray("items");
							JsonArray fixed = new JsonArray();
							for(JsonElement elm : items)
							{
								fixed.add(defaultType(elm.getAsJsonObject()));
							}
							conditions.add("items", fixed);
						}
						else if("minecraft:changed_dimension".equals(trigger))
						{
							//
						}
						else
						{
							System.out.println(trigger);
						}
					}
						
				}
					
				
				outputObj = raw;
			}
			FileWriter write = new FileWriter(output);
			GSON_OUT.toJson(outputObj, outputObj.isJsonObject()? JsonObject.class: JsonArray.class, GSON_OUT.newJsonWriter(write));
			write.close();
		}
	}
	
	public static String oreDictToTagName(String ore)
	{
		String[][] hardcoded = new String[][]{
			{"paneGlass", "forge:glass_panes"},
			{"sand", "forge:sand"},
			{"dirt", "forge:dirt"},
			{"leather", "forge:leather"},
			{"obsidian", "forge:obsidian"},
			{"treeSapling", "forge:tree_saplings"},
			{"treeLeaves", "forge:tree_leaves"},
			{"ingredientSaladTop", "futurepack:salad/top"},
			{"ingredientSaladBase", "futurepack:salad/base"},
			{"keramik", "futurepack:ceramics"},
			{"gunpowder", "forge:gunpowder"},
			{"workbench", "forge:workbenchs"},
			{"paper", "forge:paper"},
			{"stickIron", "forge:rods/iron"},
			{"string", "forge:string"},
			{"enderpearl", "forge:ender_pearls"},
			{"slimeball", "forge:slimeballs"},
			{"chestWood", "forge:chests/wooden"},
			{"cobblestone", "forge:cobblestone"},
			{"sandstone", "forge:sandstone"},
			{"gravel", "forge:gravel"},
			{"stairWood", "forge:stairs/wooden"},
			{"slabWood", "forge:slabs/wooden"},
			{"bone", "forge:bones"},
			{"stickWood", "forge:rods/wooden"},
			{"plankWood", "forge:planks/wooden"}
		};
		for(String[] maps : hardcoded)
		{
			if(maps[0].equals(ore))
			{
				String tagName = maps[1];
				return tagName;
			}
		}
		
		String[] prefixes = new String[]{"block", "gem", "ingot", "dust", "dye", "crop", "ore", "nugget"};
		for(String p : prefixes)
		{
			if(ore.startsWith(p))
			{
				String rest = ore.substring(p.length()).toLowerCase();
				String tagName = "forge:"+p+"s/"+rest;
				return tagName;
			}
		}
		throw new IllegalArgumentException("Unknown oredict type " + ore);
	}
	
	public static class CustomRecipes
	{
		private	final ItemMapper mapper;
		private final InteractivCommandUtil interact;
		
		public CustomRecipes(InteractivCommandUtil util) 
		{
			super();
			this.mapper = new ItemMapper(util);
			this.interact = util;
		}
		
		public void portFiles()
		{
			portFiles(new File("./data_/futurepack/custom"), new File("./data/futurepack/custom"));
			
		}
		
		public void portFiles(File inputdir, File outputdir)
		{
			for(File f : inputdir.listFiles())
			{
				if(interact.isInterrupted())
					break;
				
				if(f.isFile() && f.getName().endsWith(".json"))
				{
					try
					{
						portFile(f, outputdir);
					}
					catch(Exception e)
					{
						interact.sendErrorMessage(new TextComponent(e.toString()));
						e.printStackTrace();
					}
				}
			}
			interact.sendString("Done");
		}
		
		
		public void portFile(File input, File outputdir) throws Exception
		{
			File output = new File(outputdir, input.getName());
			if(output.exists())
			{
				input.delete();
				return;
			}
			
			interact.sendString("Porting " + input.getName());
			FileReader read = new FileReader(input);
			JsonElement outputObj;
			
			JsonArray raw = GSON_IN.fromJson(read, JsonArray.class);
			if("research.json".equals(input.getName()))
			{
				JsonArray out = new JsonArray();
				for(JsonElement elm : raw)
				{
					out.add(handleResearchEntry(elm.getAsJsonObject()));
				}
				outputObj = out;
			}
			else
			{
				outputObj = portJsonArrayRecipeFile(raw);
			}
		
			
			FileWriter write = new FileWriter(output);
			GSON_OUT.toJson(outputObj, outputObj.isJsonObject()? JsonObject.class: JsonArray.class, GSON_OUT.newJsonWriter(write));
			write.close();
		}
		
		public JsonArray portJsonArrayRecipeFile(JsonArray arr)
		{
			JsonArray out = new JsonArray();
			for(JsonElement elm : arr)
			{
				out.add(handleRecipeEntry(elm.getAsJsonObject()));
			}
			return out;
		}
		
		public JsonObject handleRecipeEntry(JsonObject obj)
		{
			System.out.println("\n\n"+GSON_OUT.toJson(obj)+ "\n\n");
			
			JsonElement out = obj.remove("output");
			JsonElement input = obj.remove("input");
			out = handleItemJSON(out);
			input = handleItemJSON(input);
			if(out!=null)
				obj.add("output", out);
			if(input!=null)
				obj.add("input", input);
			
			return obj;
		}
		
		public JsonElement handleItemJSON(JsonElement elm)
		{
			if(elm == null || elm.isJsonNull())
				return elm;
			
			if(elm.isJsonArray())
			{
				JsonArray out = new JsonArray();
				for(JsonElement inner : elm.getAsJsonArray())
				{
					out.add(handleItemJSON(inner));
				}
				return out;
			}
			else if(elm.isJsonObject())
			{
				JsonObject itemDef = elm.getAsJsonObject();
				JsonObject output = new JsonObject();
				if(itemDef.has("size"))
					output.add("size", itemDef.get("size"));
				
				if(itemDef.has("OreDict"))
				{
					String ore = itemDef.get("OreDict").getAsString();
					String tag = oreDictToTagName(ore);
					output.addProperty("tag", tag);
				}
				else if(itemDef.has("name"))
				{
					int meta = 0;
					if(itemDef.has("meta"))
					{
						JsonPrimitive prim = itemDef.get("meta").getAsJsonPrimitive();
						if(prim.isString())
							meta = mapper.getMetaFromStringMeta(prim.getAsString());
						else
							meta = prim.getAsInt();
					}
					String old = itemDef.get("name").getAsString();
					String item = mapper.map(old, meta);
					output.addProperty("name", item);
				}
				return output;
			}
			else if(elm.isJsonPrimitive())
			{
				String s = elm.getAsString();
				if(s.startsWith("OreDict:"))
				{
					String ore = s.substring("OreDict:".length());
					String tagname = oreDictToTagName(ore);
					return new JsonPrimitive("tag:" + tagname);
				}
				else
				{
					String mapped = mapper.map(s, 0);
					return new JsonPrimitive(mapped);
				}
			}
			else
			{
				throw new IllegalArgumentException("How did we get here? " + elm);
			}
		}
	
		public JsonObject handleResearchEntry(JsonObject obj)
		{
			System.out.println("\n\n"+GSON_OUT.toJson(obj)+ "\n\n");
			
			String[] itemencoded = new String[] {"need", "icon", "enables"};
			for(String p : itemencoded)
			{
				if(obj.has(p))
				{
					JsonElement elm = obj.remove(p);
					elm = handleItemJSON(elm);
					if(elm.isJsonArray())
					{
						addMissingColors(elm.getAsJsonArray());
					}
					else if(p.equals("need") || p.equals("enables"))
					{
						JsonArray arr = new JsonArray();
						arr.add(elm);
						addMissingColors(arr);
						elm = arr;
					}
					obj.add(p, elm);
				}
			}
			return obj;
		}
		
		private void addMissingColors(JsonArray arr)
		{
			Map<String, Integer> items = new HashMap<String, Integer>(arr.size());
			
			JsonArray tobeadded = new JsonArray();
			for(JsonElement elm : arr)
			{
				int size = 1;
				String name = null;
				if(elm.isJsonObject())
				{
					JsonObject itemdef = elm.getAsJsonObject();
					if(itemdef.has("name"))
					{
						name = itemdef.get("name").getAsString();
					}
					if(itemdef.has("size"))
					{
						size = itemdef.get("size").getAsInt();
					}
				}
				else if(elm.isJsonPrimitive())
				{
					name = elm.getAsString();
					if(name.startsWith("tag:"))
						continue;
				}
				
				if(name!=null)
				{
					items.put(name, size);
				}	
			}
			
			for(Entry<String, Integer> e : items.entrySet())
			{
				String name = e.getKey();
				int size = e.getValue().intValue();
				
				if(name.endsWith("_white") && !name.equals("futurepack:lense_white"))
				{
					DyeColor[] dyes = DyeColor.values();
					for(DyeColor c : dyes)
					{
						if(c!= DyeColor.WHITE)
						{
							String newName = name.replace("_white", "_" + c.getName());
							
							if(items.containsKey(newName))
								continue;
							
							ResourceLocation res = new ResourceLocation(newName);
							if(ForgeRegistries.ITEMS.containsKey(res))
							{
								JsonObject colored = new JsonObject();
								colored.addProperty("size", size);
								colored.addProperty("name", res.toString());
								tobeadded.add(colored);
							}
						}
					}
				}
			}
			if(tobeadded.size() > 0)
				arr.addAll(tobeadded);
		}
	}
	
	
	public static class ItemMapper
	{
		private final InteractivCommandUtil interact;
		
		public ItemMapper(InteractivCommandUtil interact) 
		{
			super();
			this.interact = interact;
		}

		public String map(String itemName, int meta)
		{
			if(itemName.startsWith("fp:"))
				itemName = itemName.replace("fp:", "futurepack:");
			
			String hash = itemName + ":" + meta;
			String mapped = getItemMap().getOrDefault(hash, null);
			if(mapped!=null)
			{
				return mapped;
			}
			else
			{
				mapped = askForMapping(itemName, meta);
				getItemMap().put(hash, mapped);
				saveMap();
				return mapped;
			}
		}
		
		public String tryMap(String itemName, int meta)
		{
			itemName = itemName.toLowerCase();
			if(itemName.startsWith("fp:"))
				itemName = itemName.replace("fp:", "futurepack:");
			
			ResourceLocation res = new ResourceLocation(itemName);
			if(ForgeRegistries.ITEMS.containsKey(res))
			{
				return itemName;
			}
			if("futurepack:spaceship".equals(itemName))
			{
				switch (meta)
				{
					case 3: return "futurepack:coil_copper";
					case 4: return "futurepack:coil_iron";
					case 5: return "futurepack:coil_neon";
					case 6: return "futurepack:coil_gold";
					case 54: return "futurepack:coil_quantanium";
					
					case 7: return "futurepack:parts_iron";
					case 8: return "futurepack:parts_diamond";
					case 9: return "futurepack:parts_quartz";
					case 10: return "futurepack:parts_neon";
					case 11: return "futurepack:parts_copper";
					case 28: return "futurepack:parts_gold";
					
					case 12: return "futurepack:iron_stick";
					case 13: return "futurepack:ingot_aluminium";
					case 14: return "futurepack:composite_metal";
					case 15: return "futurepack:ingot_silicon";
					case 16: return "futurepack:lack_tank_empty";
					case 17: return "futurepack:maschineboard";
					case 18: return "futurepack:drone_engine";
					case 19: return "futurepack:double_maschineboard";
					case 20: return "futurepack:astrofood_empty";
					
					case 21: return "futurepack:dungeon_key_0";
					case 22: return "futurepack:dungeon_key_1";
					case 23: return "futurepack:dungeon_key_2";
					case 24: return "futurepack:dungeon_key_3";
					case 25: return "futurepack:topinambur_flower";
					case 27: return "futurepack:display";
					
					case 29: return "futurepack:laserdiode";
					case 30: return "futurepack:lense_purple";
					case 31: return "futurepack:lense_green";
					case 32: return "futurepack:lense_red";
					case 33: return "futurepack:lense_white";
					case 34: return "futurepack:fragment_core";
					case 35: return "futurepack:fragment_ram";
					
					case 36: return "futurepack:fuel_rods";
					case 37: return "futurepack:dust_obsidian";
					case 38: return "futurepack:ceramic";
					
					case 39: return "futurepack:spawn_note";
					case 40: return "futurepack:spacecoordinats";
					case 52: return "futurepack:polymer";
					
					case 41: return "futurepack:fibers";
					case 42: return "futurepack:wood_chips";
					case 43: return "futurepack:kompost";
					
					case 44: return "futurepack:toasted_core";
					case 45: return "futurepack:toasted_ram";
					case 46: return "futurepack:toasted_chip";
					case 47: return "futurepack:mendel_flower";
					case 48: return "futurepack:sword_handle";
					
					case 53: return "futurepack:chemicals_x";
					

				default:
					break;
				}
			}
			if(itemName.startsWith("futurepack:lacktank"))
			{
				String num = itemName.substring("futurepack:lacktank".length());
				int d = Integer.parseInt(num);
				
				String newName = "futurepack:lack_tank_" + DyeColor.byId(d);
				res = new ResourceLocation(newName);
				if(ForgeRegistries.ITEMS.containsKey(res))
				{
					return res.toString();
				}
				
			}
			if(itemName.startsWith("futurepack:color"))
			{
				String numEnd = null;
				char c = itemName.charAt(itemName.length()-1);
				
				if(c >= '0' && c <= '9')
				{
					numEnd = "" + c;
					c = itemName.charAt(itemName.length()-2);
					if(c >= '0' && c <= '9')
					{
						numEnd = c + numEnd;
					}
				}
					
				String numberless = itemName;
				boolean lastpartnumber;
				int num; 
				if(numEnd != null)
				{
					try
					{
						num =Integer.parseInt(numEnd);
						lastpartnumber = true;
						numberless = itemName.substring(0, itemName.length() - numEnd.length());
						if(numberless.charAt(numberless.length()-1) != '_')
						{
							numberless += "_";
						}
					}
					catch(NumberFormatException e)
					{
						num = -1;
						lastpartnumber = false;
					}
				}
				else
				{
					lastpartnumber = false;
					num = -1;
				}
				
				
				if(meta == 0 && lastpartnumber)
				{
					String newName = numberless + DyeColor.byId(num);
					res = new ResourceLocation(newName);
					if(ForgeRegistries.ITEMS.containsKey(res))
					{
						return res.toString();
					}
					
					
					interact.sendString("Meta " + num + " is " + DyeColor.byId(num));
				}
				else
				{
					String newName = itemName +"_" + DyeColor.byId(meta);
					res = new ResourceLocation(newName);
					if(ForgeRegistries.ITEMS.containsKey(res))
					{
						return res.toString();
					}
					
					interact.sendString("Meta " + meta + " is " + DyeColor.byId(meta));
				}
			}
			if(meta == 10 || meta == 6 || meta == 2)
			{
				String s = (meta == 10) ? "black" :  meta==6 ? "gray" : meta==2 ? "white" : "";
				String newName = itemName +"_" + s;
				res = new ResourceLocation(newName);
				if(ForgeRegistries.ITEMS.containsKey(res))
				{
					return res.toString();
				}
			}
			
			return null;
		}
		
		public String askForMapping(String itemName, int meta)
		{
			interact.sendString("What is " + itemName + " meta: " + meta + " ?");
			String suggestion = tryMap(itemName, meta);
			if(suggestion!=null)
			{
				interact.sendString("Is it " + suggestion + " ?");
				if(interact.waitForCommand(boolean.class))
				{
					return suggestion;
				}
				else
				{
					interact.sendString("fpdebug answer <text>");
				}
			}
			else
			{
				interact.sendString("fpdebug answer <text>");
			}
			return interact.waitForCommand(String.class);
		}
		
		private Object2IntMap<String> metaMap;
		
		public int getMetaFromStringMeta(String meta)
		{
			if(metaMap == null)
			{
				metaMap = new Object2IntRBTreeMap<String>();
				
				metaMap.put("spaceship.CopperCoil", 3);
				metaMap.put("spaceship.IronCoil", 4);
				metaMap.put("spaceship.NeonCoil", 5);
				metaMap.put("spaceship.GoldCoil", 6);
				metaMap.put("spaceship.IronParts", 7);
				metaMap.put("spaceship.DiaParts", 8);
				metaMap.put("spaceship.QuartzParts", 9);
				metaMap.put("spaceship.NeonParts", 10);
				metaMap.put("spaceship.CopperParts", 11);
				metaMap.put("spaceship.IronStick", 12);
				metaMap.put("spaceship.Aluminum", 13);
				metaMap.put("spaceship.CompositeMetall", 14);
				metaMap.put("spaceship.Silizium", 15);
				metaMap.put("spaceship.Tank", 16);
				metaMap.put("spaceship.tank", 16);
				metaMap.put("spaceship.Maschinboard", 17);
				metaMap.put("spaceship.DroneEngine", 18);
				metaMap.put("spaceship.DoubleMaschinboard", 19);
				metaMap.put("spaceship.AstroFoodEmpty", 20);
				metaMap.put("spaceship.dungeonKey0", 21);
				metaMap.put("spaceship.dungeonKey1", 22);
				metaMap.put("spaceship.dungeonKey2", 23);
				metaMap.put("spaceship.dungeonKey3", 24);
				metaMap.put("spaceship.topinambur_flower", 25);
				metaMap.put("spaceship.X8", 26);
				metaMap.put("spaceship.Display", 27);
				metaMap.put("spaceship.GoldParts", 28);
				metaMap.put("spaceship.Laserdiode", 29);
				metaMap.put("spaceship.LinseL", 30);
				metaMap.put("spaceship.LinseG", 31);
				metaMap.put("spaceship.LinseR", 32);
				metaMap.put("spaceship.LinseW", 33);
				metaMap.put("spaceship.fragmentCore", 34);
				metaMap.put("spaceship.fragmentRam", 35);
				metaMap.put("spaceship.fuelCell", 36);
				metaMap.put("spaceship.obsidianDust", 37);
				metaMap.put("spaceship.keramik", 38);
				metaMap.put("spaceship.SpawnNote", 39);
				metaMap.put("spaceship.spaceCoordinats", 40);
				metaMap.put("spaceship.Fibers", 41);
				metaMap.put("spaceship.HackSchnitzel", 42);
				metaMap.put("spaceship.Kompost", 43);
				metaMap.put("spaceship.toasted_core", 44);
				metaMap.put("spaceship.toasted_ram", 45);
				metaMap.put("spaceship.toasted_chip", 46);
				metaMap.put("spaceship.mendel_flower", 47);
				metaMap.put("spaceship.swordHandle", 48);
				metaMap.put("spaceship.X9", 49);
				metaMap.put("spaceship.X10", 50);
				metaMap.put("spaceship.X11", 51);
				metaMap.put("spaceship.Polymer", 52);
				metaMap.put("spaceship.Chemicals", 53);
				metaMap.put("spaceship.QuantaniumCoil", 54);
				
				metaMap.put("chip.LogicChip", 0);
				metaMap.put("chip.KIChip", 1);
				metaMap.put("chip.LogisticChip", 2);
				metaMap.put("chip.NavigationChip", 3);
				metaMap.put("chip.NetworkChip", 4);
				metaMap.put("chip.ProduktionChip", 5);
				metaMap.put("chip.RedstoneChip", 6);
				metaMap.put("chip.SupportChip", 7);
				metaMap.put("chip.TacticChip", 8);
				metaMap.put("chip.UltimateChip", 9);
				metaMap.put("chip.DamageControlChip", 10);
				
				
			}
			
			if(meta.startsWith("<") && meta.endsWith(">"))
			{
				meta = meta.substring(1, meta.length()-1);
				int m =  metaMap.getOrDefault(meta, -1);
				if(m == -1)
				{
					throw new IllegalArgumentException("unknown meta " + meta);
				}
			}
			else
			{
				return Integer.parseInt(meta);
			}
			return metaMap.getOrDefault(meta, -1);	
		}
		
		private Map<String, String> itemMapper;
		
		private Map<String, String> getItemMap()
		{
			if(itemMapper == null)
			{
				File f = new File("./../item_map.json");
				if(f.exists())
				{
					try
					{
						FileReader read = new FileReader(f);
						itemMapper = GSON_IN.fromJson(read, Map.class);
						read.close();
					}
					catch(IOException e)
					{
						e.printStackTrace();
					}
				}
				else
				{
					itemMapper =  new HashMap<String, String>();
				}
			}
			
			return itemMapper;
		}
		
		private void saveMap()
		{
			try
			{
				File f = new File("./../item_map.json");
				FileWriter wr = new FileWriter(f);
				GSON_OUT.toJson(itemMapper, Map.class, wr);
				wr.close();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		}
	}
}
