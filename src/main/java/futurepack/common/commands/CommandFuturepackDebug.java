package futurepack.common.commands;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import futurepack.common.FPLog;
import futurepack.common.dim.structures.LoaderStructures;
import futurepack.common.dim.structures.ManagerDungeonStructures;
import futurepack.common.dim.structures.OpenDoor;
import futurepack.common.dim.structures.StructureBase;
import futurepack.common.dim.structures.StructureToJSON;
import futurepack.common.dim.structures.generation.BakedDungeon;
import futurepack.common.dim.structures.generation.DungeonGeneratorBase;
import futurepack.common.research.CircularDependencyTest;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchLoader;
import futurepack.depend.api.helper.HelperChunks;
import futurepack.world.scanning.ChunkData;
import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.coordinates.BlockPosArgument;
import net.minecraft.commands.arguments.coordinates.Coordinates;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;

public class CommandFuturepackDebug
{
	public static void register(CommandDispatcher<CommandSourceStack> dispatcher)
	{
		dispatcher.register(Commands.literal("fpdebug").requires((pl) ->
		{
			return pl.hasPermission(2);
		})
			.then(Commands.literal("ores")
				.then(Commands.literal("scan").executes(CommandFuturepackDebug::doOreScan))
				.then(Commands.literal("stats").executes(CommandFuturepackDebug::doOreStats))
			)
			.then(Commands.literal("structures")
				.then(Commands.literal("scan")
					.then(Commands.argument("from", BlockPosArgument.blockPos()).then(Commands.argument("to", BlockPosArgument.blockPos()).executes(context -> {
						return doStructureScan(context.getSource().getLevel(), context.getArgument("from", Coordinates.class).getBlockPos(context.getSource()), context.getArgument("to", Coordinates.class).getBlockPos(context.getSource()), null);
					}).then(Commands.argument("name", StringArgumentType.word()).executes(context -> {
						return doStructureScan(context.getSource().getLevel(), context.getArgument("from", Coordinates.class).getBlockPos(context.getSource()), context.getArgument("to", Coordinates.class).getBlockPos(context.getSource()), context.getArgument("name", String.class));
					}))))
				)
				.then(Commands.literal("load")
						.then(Commands.argument("name", StringArgumentType.string()).then(Commands.argument("rotation", IntegerArgumentType.integer(0, 3)).then(Commands.argument("raw_structure", BoolArgumentType.bool())
								.executes(CommandFuturepackDebug::loadStructure))))
				)
				.then(Commands.literal("dungeon")
						.then(Commands.argument("threaded", BoolArgumentType.bool())
								.executes(CommandFuturepackDebug::spawnDungeon))
				)
				.then(Commands.literal("convert")
						.executes(StructurePortingUtil::doOreScan)
				)
				.then(Commands.literal("select")
						.then(Commands.argument("pos", BlockPosArgument.blockPos())
								.executes(InteractivCommandUtil::doBlockPosSelect)
						)
				)
			)
			.then(Commands.literal("research")
					.then(Commands.literal("reload").executes(ctx -> {
						FPLog.logger.info("Reloding Researches");
						ResearchLoader.instance.init();
						return Command.SINGLE_SUCCESS;
					}))
					.then(Commands.literal("check").executes(CommandFuturepackDebug::checkResearchTreeDeadLocks))
			)
			.then(Commands.literal("yes")
					.executes(InteractivCommandUtil::doYesQuestion)
			)
			.then(Commands.literal("no")
					.executes(InteractivCommandUtil::doNoQuestion)
			)
			.then(Commands.literal("recipes")
				.then(Commands.literal("answer")
					.then(Commands.argument("text", StringArgumentType.string())
						.executes(InteractivCommandUtil::doAnswerQuestion)
					)
					.executes(InteractivCommandUtil::answerQuestionNoArgument)
				)
				.then(Commands.literal("porting")
						.executes(RecipePortingUtil::doItemPorting)
				)
				.then(Commands.literal("skip")
						.executes(InteractivCommandUtil::doSkipQuestion)
				)
			)
			.then(Commands.literal("tiles_entitys")
					.then(Commands.literal("stop")
							.then(Commands.argument("from", BlockPosArgument.blockPos()).then(Commands.argument("to", BlockPosArgument.blockPos()).executes(TileEntityTickingUitl::stopServerTicks)))
					)
			)
		);

	}

	public static int doOreScan(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		final OreSearcher search = new OreSearcher(new BlockPos(src.getSource().getPosition()));
		Thread t = new Thread(new Runnable()
		{

			@Override
			public void run()
			{
				src.getSource().sendSuccess(new TextComponent("Start Searching..."), false);
				HashMap<BlockState, Integer> map = search.search(src.getSource().getLevel());
				Set<Entry<BlockState,Integer>> set = map.entrySet();
				for(Entry<BlockState,Integer> e : set)
				{
					Block bl = e.getKey().getBlock();
					ItemStack it = new ItemStack(bl,1);
					TranslatableComponent st = new TranslatableComponent(it.getDescriptionId());
					st.append(new TextComponent(" ("+e.getValue() + ")"));
					src.getSource().sendSuccess(st, true);
				}
				src.getSource().sendSuccess(new TextComponent("Done."), false);
			}
		}, "Ore Search");
		t.start();
		return Command.SINGLE_SUCCESS;
	}

	public static int doOreStats(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		CommandSourceStack sender = src.getSource();
		Level w = sender.getLevel();
		File dir = new File(HelperChunks.getDimensionDir(w), "ores");
		dir.mkdirs();
		long ores = 0;
		long chunks = 0;
		for(String f : dir.list())
		{
			if(f.endsWith(".dat"))
			{
				URI uri = URI.create("jar:" + new File(dir, f).toURI());
				sender.sendSuccess(new TextComponent("Searching in file " + f), false);
				Map<String,String> map = new HashMap<String, String>();
				map.put("create", "true");

				try
				{
					FileSystem fs = FileSystems.newFileSystem(uri, map);
					for(Path root : fs.getRootDirectories())
					{
						Iterable<Path> iter = Files.list(root)::iterator;
						for(Path sub : iter)
						{
							BufferedReader reader = Files.newBufferedReader(sub, StandardCharsets.UTF_8);
							ChunkData data = new ChunkData(w, new BlockPos(sender.getPosition()), new HashMap<String, Integer>());
							data.load(reader);
							reader.close();

							int total = data.getTotalOres();
							System.out.println("" + total);
							ores += total;
							chunks++;
						}
					}
					fs.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
		double d = (double)ores / (double)chunks;
		sender.sendSuccess(new TextComponent("Searched in " + chunks + " chunks"), true);
		sender.sendSuccess(new TextComponent("Average " + d + " ores per chunk "), true);

		return Command.SINGLE_SUCCESS;
	}

	public static int doStructureScan(Level w, BlockPos s, BlockPos e, String name) throws CommandSyntaxException
	{
		try
		{
			StructureToJSON gen = new StructureToJSON(w);
			if(name!=null)
			{
				gen.fileName = name;
			}
			gen.generate(s, e);
		}
		catch (Exception ee)
		{
			ee.printStackTrace();
		}

		return Command.SINGLE_SUCCESS;
	}

	public static int loadStructure(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		String name = src.getArgument("name", String.class) +".json";
		int rot = src.getArgument("rotation", int.class);
		boolean raw = src.getArgument("raw_structure", boolean.class);

		try
		{
			CommandSourceStack sender = src.getSource();

			InputStream in = LoaderStructures.class.getResourceAsStream("./"+name );
			if(in == null)
			{
				in = ClassLoader.getSystemClassLoader().getResourceAsStream("futurepack/common/dim/structures/" + name);
				if(in == null)
				{
					in = new FileInputStream("./"+name);
				}
			}
			StructureBase struc = LoaderStructures.getFromStream(in, rot, name);
			in.close();

			BlockPos pos = new BlockPos(sender.getPosition()).above(2);

			struc.hide = false;
			struc.generate(sender.getLevel(), pos, new ArrayList<>());
			struc.addChestContentBase(sender.getLevel(), pos, sender.getLevel().random, new CompoundTag(), sender.getLevel().getServer().getLootTables());

			if(raw)
			{
				BlockState[][][] blocks = struc.getBlocks();
				BlockState bedrock = Blocks.BEDROCK.defaultBlockState();
				for(int x=0;x<blocks.length;x++)
				{
					for(int y=0;y<blocks[x].length;y++)
					{
						for(int z=0;z<blocks[x][y].length;z++)
						{
							if(blocks[x][y][z]==null)
							{
								sender.getLevel().setBlockAndUpdate(pos.offset(x,y,z), bedrock);
							}
						}
					}
				}

				for(OpenDoor door : struc.getRawDoors())
				{
					BlockPos xyz = pos.offset(door.getPos());
					for(int x=0;x<door.getWeidth();x++)
					{
						for(int y=0;y<door.getHeight();y++)
						{
							for(int z=0;z<door.getDepth();z++)
							{
								BlockState state = Blocks.END_ROD.defaultBlockState().setValue(DirectionalBlock.FACING, door.getDirection());
								sender.getLevel().setBlockAndUpdate(xyz.offset(x,y,z), state);
							}
						}
					}
				}
			}


		}
		catch (IOException e)
		{
			src.getSource().sendFailure(new TextComponent(e.toString()));
		}
		catch (Exception e)
		{
			src.getSource().sendFailure(new TextComponent(e.toString()));
			e.printStackTrace();
		}

		return 0;
	}

	public static int spawnDungeon(CommandContext<CommandSourceStack> src) throws CommandSyntaxException
	{
		//WorldGenBigFPDungeon fp = new WorldGenBigFPDungeon(sender.getEntityWorld(), sender.getPosition());
		//fp.start();

		long time = System.currentTimeMillis();

		DungeonGeneratorBase gen = new DungeonGeneratorBase(System.currentTimeMillis());
		ManagerDungeonStructures.init(gen, true);
		BakedDungeon bd = null;
		for(int i=0;i<10;i++)
		{
			bd = gen.generate();
			if(bd!=null)
				break;
		}

		CommandSourceStack sender = src.getSource();
		sender.sendSuccess(new TextComponent("Dungeon baking in: " + (System.currentTimeMillis()-time) + " ms"), true);
		time = System.currentTimeMillis();

		BlockPos pos = new BlockPos(sender.getPosition());
		if(pos.getY() + bd.totalBox.minY() < 1)
		{
			pos = pos.above(Math.abs(bd.totalBox.minY()) +3);
		}

		if(src.getArgument("threaded", boolean.class))
		{
			bd.spawnThreaded(sender.getLevel(), pos, 100);
		}
		else
		{
			bd.spawnDungeon(sender.getLevel(), pos);
		}

		sender.sendSuccess(new TextComponent("Dungeon generated in: " + (System.currentTimeMillis()-time) + " ms"), true);

		return Command.SINGLE_SUCCESS;
	}

	public static int checkResearchTreeDeadLocks(CommandContext<CommandSourceStack> sender) throws CommandSyntaxException
	{
		List<String> ss = CircularDependencyTest.checkResearchTreeDeadLocks();
		for(String s : ss)
		{
			sender.getSource().sendFailure(new TextComponent(s));
		}
		return Command.SINGLE_SUCCESS;
	}



//	@Override
//	public void execute(MinecraftServer server, final ICommandSender sender, String[] args) throws CommandException
//	{
//
//		else if(menuPoint("dimensions", args, 1))
//		{
//			//int dim = parseInt(args[1]);
//			ItemStack it = new ItemStack(FPItems.tools,1,ItemSpaceship.spaceCoordinats);
//			it.setTag(new NBTTagCompound());
//			it.getTag().setString("Planet", args[1]);
//			EntityPlayerMP mp = getCommandSenderAsPlayer(sender);
//			mp.inventory.addItemStackToInventory(it);
//		}
//		else if(menuPoint("atmosphere", args, 1))
//		{
//			if(menuPoint("add", args, 2))
//			{
//				World w = sender.getEntityWorld();
//				if(w.isBlockLoaded(sender.getPosition()))
//				{
//					Chunk c = w.getChunk(sender.getPosition());
//					if(c.hasCapability(AtmosphereManager.cap_ATMOSPHERE, null))
//					{
//						IChunkAtmosphere atm = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
//						BlockPos pos = sender.getPosition();
//						int added = atm.addAir(pos.getX()&15, pos.getY()&255, pos.getZ()&15, 1000000);
//						sender.sendMessage(new TextComponentString("Succsefully added " + added +" AIR"));
//					}
//					else
//					{
//						throw new CommandException("Chunk %s has not capability", c);
//					}
//				}
//				else
//				{
//					throw new CommandException("chunk is not loaded");
//				}
//			}
//			else if(menuPoint("count", args, 2))
//			{
//				World w = sender.getEntityWorld();
//				BlockPos pos = sender.getPosition();
//				Chunk c = w.getChunk(pos);
//				if(c.hasCapability(AtmosphereManager.cap_ATMOSPHERE, null))
//				{
//					IChunkAtmosphere atm = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
//					Long allTHeAIR = 0L;
//					for(int x=0;x<16;x++)
//						for(int z=0;z<16;z++)
//							for(int y=0;y<255;y++)
//							{
//								allTHeAIR += atm.getAirAt(x, y, z);
//							}
//
//					sender.sendMessage(new TextComponentString("Succsefully found " + allTHeAIR +" AIR"));
//				}
//				else
//				{
//					throw new CommandException("Chunk %s has not capability", c);
//				}
//			}
//			else if(menuPoint("show", args, 2))
//			{
//				WorldServer w = (WorldServer) sender.getEntityWorld();
//				BlockPos pos = sender.getPosition();
//				Chunk c = w.getChunk(pos);
//				if(c.hasCapability(AtmosphereManager.cap_ATMOSPHERE, null))
//				{
//					IChunkAtmosphere atm = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
//					for(int x=0;x<16;x++)
//						for(int z=0;z<16;z++)
//							for(int y=0;y<255;y++)
//							{
//								int air = atm.getAirAt(x, y, z);
//								if(air>0)
//								{
//									int meta = (air* 15 / 36000);
//									int color = 35 + 4096 * meta;
//									w.spawnParticle(ParticleTypes.FALLING_DUST, c.x* 16 + x+ 0.5, y + 0.5, c.z*16 +z + 0.5, 10, 0F, 0F, 0F, 0F, color);
//								}
//							}
//				}
//				else
//				{
//					throw new CommandException("Chunk %s has not capability", c);
//				}
//			}
//		}
//	}

	private void enable(CustomPlayerData toAdd, Research r)
	{
		Research[] rr = r.getParents();

		if(rr!=null)
		{
			for(Research r3 : rr)
			{
				enable(toAdd, r3);
			}
		}

		toAdd.addResearch(r);
	}




}
