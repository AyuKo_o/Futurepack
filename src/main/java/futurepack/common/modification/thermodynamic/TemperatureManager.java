package futurepack.common.modification.thermodynamic;

import java.util.Map;
import java.util.WeakHashMap;

import com.google.common.collect.ImmutableList;

import futurepack.common.item.ComputerItems;
import futurepack.common.item.ResourceItems;
import futurepack.depend.api.helper.HelperOreDict;
import it.unimi.dsi.fastutil.longs.Long2FloatLinkedOpenHashMap;
import it.unimi.dsi.fastutil.objects.Object2FloatOpenHashMap;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biome.BiomeCategory;
import net.minecraft.world.level.levelgen.LegacyRandomSource;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.level.levelgen.synth.PerlinSimplexNoise;

public class TemperatureManager 
{
	public static final TemperatureManager INSTANCE  = new TemperatureManager();
	
	private Map<Item, Float> itemToTemp = new Object2FloatOpenHashMap<Item>();
	private float globalMinHeat = 10e4F;
	
	private boolean isInited = false;
	
	
	public static void init()
	{
		System.out.println("Initing Temps of Items.");
		
		register(new ItemStack(ResourceItems.ingot_silicon),1414F);
		register(new ItemStack(ResourceItems.ingot_aluminium),660F);
		register(new ItemStack(ResourceItems.composite_metal),1600F);
		register(new ItemStack(Items.QUARTZ,1),1600F);	
		register(new ItemStack(Items.GOLD_INGOT,1),1064F);
		register(new ItemStack(Items.GOLD_NUGGET,1),1064F);
		register(new ItemStack(Items.IRON_INGOT,1),1538F);
		register(new ItemStack(Items.REDSTONE,1),527F);
		register(new ItemStack(ResourceItems.ingot_copper),1085F);
		register(new ItemStack(ResourceItems.ingot_tin),232F);
		register(new ItemStack(ResourceItems.ingot_zinc),420F);
		register(new ItemStack(ResourceItems.ingot_gadolinium),1312F);
		register(new ItemStack(ResourceItems.ingot_lithium),920F);
		register(new ItemStack(ResourceItems.ingot_neodymium),1021F);
		register(new ItemStack(ResourceItems.ingot_magnet),1500F);
		register(new ItemStack(ResourceItems.ingot_wakurium),970F);
		register(new ItemStack(ResourceItems.ingot_quantanium),996F);
		
		register(new ItemStack(ComputerItems.standart_core,1),125F + 50F);
		register(new ItemStack(ComputerItems.a1_core,1),150F + 50F);
		register(new ItemStack(ComputerItems.p2_core,1),200F + 50F);
		register(new ItemStack(ComputerItems.tct_core,1),225F + 50F);
		register(new ItemStack(ComputerItems.master_core,1),800F + 50F);
		register(new ItemStack(ComputerItems.non_core,1),400F + 50F);
		register(new ItemStack(ComputerItems.dungeon_core,1),1000F + 100F);
		register(new ItemStack(ComputerItems.torus_core,1),2000F + 100F);
		
		register(new ItemStack(ComputerItems.standart_ram,1),150F);
		register(new ItemStack(ComputerItems.a_ram,1),175F);
		register(new ItemStack(ComputerItems.p_ram,1),200F);
		register(new ItemStack(ComputerItems.tct_ram,1),225F);
		register(new ItemStack(ComputerItems.master_ram,1),800F);
		register(new ItemStack(ComputerItems.non_ram,1),400F);
		register(new ItemStack(ComputerItems.dungeon_ram,1),1000F);
		register(new ItemStack(ComputerItems.torus_ram,1),2000F);
		
		register(new ItemStack(ComputerItems.logic_chip,1),200F);
		register(new ItemStack(ComputerItems.ai_chip,1),600F);
		register(new ItemStack(ComputerItems.transport_chip,1),450F);
		register(new ItemStack(ComputerItems.navigation_chip,1),350F);
		register(new ItemStack(ComputerItems.network_chip,1),600F);
		register(new ItemStack(ComputerItems.industrie_chip,1),175F);
		register(new ItemStack(ComputerItems.redstone_chip,1),400F);
		register(new ItemStack(ComputerItems.support_chip,1),800F);
		register(new ItemStack(ComputerItems.tactic_chip,1),350F);
		register(new ItemStack(ComputerItems.ultimate_chip,1),2000F);
		register(new ItemStack(ComputerItems.damage_control_chip,1),500F);
		
		INSTANCE.isInited = true;
	}
	
	
	public static void register(ItemStack it, float temp)
	{
		INSTANCE.itemToTemp.put(it.getItem(), temp);
		if(temp<INSTANCE.globalMinHeat)
			INSTANCE.globalMinHeat = temp;
	}
	
	public static void registerFromRecipe(ItemStack toRegister, ItemStack...ingredients)
	{
		if(ingredients.length==0)
			return;
		
		float min = 10e4F;
		for(int i=0;i<ingredients.length;i++)
		{
			min = Math.min(min, getTemp(ingredients[i]));
		}
		
		register(toRegister, min);
	}
	
	public static float getTemp(ItemStack it)
	{
		if(!INSTANCE.isInited)
			init();
		
		return INSTANCE.itemToTemp.getOrDefault(it.getItem(), 10e4F);
	}
	
	public static float getTempWithOreRegistrs(ItemStack it)
	{
		it = HelperOreDict.FuturepackConveter.getChangedItem(it);
		return getTemp(it);
	}
	
	private static final Map<Biome, ThreadLocal<Long2FloatLinkedOpenHashMap>> temperatureCache = new WeakHashMap<Biome, ThreadLocal<Long2FloatLinkedOpenHashMap>>();
	
	public static float getTempDegrees(Holder<Biome> holder, BlockPos pos)
	{
		
		if(Biome.getBiomeCategory(holder) == BiomeCategory.NETHER)
			return 200;
		else if (Biome.getBiomeCategory(holder) == BiomeCategory.THEEND)
			return -200;
		
		float f = getTemperature(holder.value(), pos);
		f *= 25;
		return f;
	}
	
	//from Biome.class
	
	private static final PerlinSimplexNoise TEMPERATURE_NOISE = new PerlinSimplexNoise(new WorldgenRandom(new LegacyRandomSource(1234L)), ImmutableList.of(0));
	
	private static float getHeightAdjustedTemperature(Biome b, BlockPos pPos) 
	{
		float f = b.getBaseTemperature();
		if (pPos.getY() > 80) {
			float f1 = (float)(TEMPERATURE_NOISE.getValue((double)((float)pPos.getX() / 8.0F), (double)((float)pPos.getZ() / 8.0F), false) * 8.0D);
			return f - (f1 + (float)pPos.getY() - 80.0F) * 0.05F / 40.0F;
		} else {
			return f;
		}
	}
	
	private static Long2FloatLinkedOpenHashMap getTemperatureCache(Biome b)
	{
		return temperatureCache.computeIfAbsent(b, bb -> ThreadLocal.withInitial(() -> 
		{
			return Util.make(() -> {
				Long2FloatLinkedOpenHashMap long2floatlinkedopenhashmap = new Long2FloatLinkedOpenHashMap(1024, 0.25F) {
					protected void rehash(int p_47580_) {
					}
				};
				long2floatlinkedopenhashmap.defaultReturnValue(Float.NaN);
				return long2floatlinkedopenhashmap;
			});
		})).get();
	}

	private static float getTemperature(Biome b, BlockPos pPos) {
		long i = pPos.asLong();
	      Long2FloatLinkedOpenHashMap long2floatlinkedopenhashmap = getTemperatureCache(b);
	      float f = long2floatlinkedopenhashmap.get(i);
	      if (!Float.isNaN(f)) {
	         return f;
	      } else {
	         float f1 = getHeightAdjustedTemperature(b, pPos);
	         if (long2floatlinkedopenhashmap.size() == 1024) {
	            long2floatlinkedopenhashmap.removeFirstFloat();
	         }

	         long2floatlinkedopenhashmap.put(i, f1);
	         return f1;
	      }
	   }
	
	public static float getGlobalMinHeat()
	{
		return INSTANCE.globalMinHeat;
	}
}
