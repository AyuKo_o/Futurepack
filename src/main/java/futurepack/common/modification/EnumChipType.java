package futurepack.common.modification;

public enum EnumChipType
{
	LOGIC,
	TRANSPORT,
	NETWORK,
	REDSTONE,
	SUPPORT,
	ULTIMATE,
	AI,
	INDUSTRIE,
	TACTIC,
	DAMAGE_CONTROL,
	NAVIGATION;
	
}
