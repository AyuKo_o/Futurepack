package futurepack.common.modification;

public class EmptyJoinedParts extends JoinedParts 
{
	public static EmptyJoinedParts EMPTY = new EmptyJoinedParts();
	
	public EmptyJoinedParts() 
	{
		super(null);
	}
	
	@Override
	public boolean isChip() 
	{
		return false;
	}
	
	@Override
	public boolean isRam() 
	{
		return false;
	}
	
	@Override
	public boolean isCore() 
	{
		return false;
	}
	
	@Override
	public int getCorePower(EnumCorePowerType type) 
	{
		return 0;
	}
	
	@Override
	public float getRamSpeed() 
	{
		return 0F;
	}
	
	@Override
	public float getChipPower(EnumChipType type)
	{
		return 0F;
	}

}
