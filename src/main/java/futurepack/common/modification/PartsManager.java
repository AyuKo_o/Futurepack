package futurepack.common.modification;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.annotation.Nullable;

import futurepack.common.item.ItemChip;
import futurepack.common.item.ItemCore;
import futurepack.common.item.ItemRam;
import net.minecraft.world.item.ItemStack;

public class PartsManager
{
	private static List<BiFunction<ItemStack, EnumChipType, Float>> chipList;
	
	private static List<Function<ItemStack, IModificationPart>> functionList;
	
	static
	{
		chipList = new ArrayList<>(1);
		functionList = new ArrayList<>(3);
		functionList.add(ItemChip::getChip);
		functionList.add(ItemCore::getCore);
		functionList.add(ItemRam::getRam);
		
	}
	
	@Nullable
	private static <T,R,U> U getFirstNonNull(Collection<BiFunction<T,R,U>> list, T input, R type)
	{
		for(BiFunction<T, R, U> func : list)
		{
			U u = func.apply(input, type);
			if(u!=null)
				return u;
		}
		return null;
	}
	
	@Nullable
	private static <T,R> R getFirstNonNull(Collection<Function<T,R>> list, T input)
	{
		for(Function<T, R> func : list)
		{
			R u = func.apply(input);
			if(u!=null)
				return u;
		}
		return null;
	}
	
	public static float getChipPower(ItemStack it, EnumChipType type)
	{
		Float fl = getFirstNonNull(chipList, it, type);
		if(fl!=null)
		{
			return fl.floatValue();
		}
		else
		{
			return 0F;
		}
	}
	
	@Nullable
	public static IModificationPart getPartFromItem(ItemStack it)
	{
		return getFirstNonNull(functionList, it);
	}
	
	public static JoinedParts joinUpdateable(IModificationPart...parts)
	{
		if(parts.length==0)
			return EmptyJoinedParts.EMPTY;
		return new JoinedParts(parts);
	}
}
