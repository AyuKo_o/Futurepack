package futurepack.common.player;

import com.mojang.authlib.GameProfile;

import net.minecraft.network.Connection;
import net.minecraft.network.protocol.PacketFlow;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.network.ServerGamePacketListenerImpl;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.util.FakePlayer;

public class FuturepackPlayer extends FakePlayer
{

	public FuturepackPlayer(ServerLevel world, GameProfile name)
	{
		super(world, name);
		Connection manager = new Connection(PacketFlow.SERVERBOUND);
		this.connection = new ServerGamePacketListenerImpl(world.getServer(), manager, this);
	}
	
	@Override
	public Vec3 position()
    {
        return new Vec3(this.getX(), this.getY(), this.getZ());
    }
}
