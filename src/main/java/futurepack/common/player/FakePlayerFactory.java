package futurepack.common.player;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;

import com.mojang.authlib.GameProfile;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class FakePlayerFactory
{
	public static GameProfile FUTUREPACK = new GameProfile(UUID.fromString("32A91F07-7AdB-4125-BA67-23E2B78DAF76"), "[Futurepack]");
	public static final FakePlayerFactory INSTANCE = new FakePlayerFactory();
	
	private Map<ServerLevel,FuturepackPlayer> players = new WeakHashMap<ServerLevel, FuturepackPlayer>();
	
	public FakePlayerFactory()
	{
		MinecraftForge.EVENT_BUS.register(this);
	}
	
	public FuturepackPlayer getPlayer(ServerLevel server)
	{
		FuturepackPlayer pl = players.get(server);
		if(pl==null)
		{
			pl = new FuturepackPlayer(server, FUTUREPACK);
			players.put(server, pl);
		}
		return pl;
	}
	
	@SubscribeEvent
	public void onWorldUnload(WorldEvent.Unload event)
	{
//		ArrayList<FakePlayer> pls = players.get(event.world);
		players.remove(event.getWorld());
	}
	
	
	public static InteractionResult itemUseBase(Level w, BlockPos pos, Direction face, ItemStack it, ServerPlayer pl, InteractionHand hand)
	{
		BlockHitResult ray = new BlockHitResult(new Vec3(1, 1, 1), face, pos, false);
		UseOnContext context = new ExtendedUseContext(w, pl, hand, it, ray);
		
		Item item = it.getItem();
		if(item instanceof BlockItem)
		{
			BlockState state = w.getBlockState(pos);
			if(!state.canBeReplaced(new BlockPlaceContext(context)))
			{
				return InteractionResult.FAIL;
			}
		}
		return item.useOn(context);
	}
	
	public static InteractionResultHolder<ItemStack> itemRightClickBase(Level w, ItemStack it, ServerPlayer pl, InteractionHand hand)
	{
		Item item = it.getItem();
		return item.use(w, pl, hand);
	}
	
	public static InteractionResult itemEntityClick(Level w, BlockPos pos, ItemStack it, ServerPlayer pl, InteractionHand hand)
	{
		List<LivingEntity> list =  w.getEntitiesOfClass(LivingEntity.class, new AABB(pos, pos.offset(1, 1, 1)));
		if(!list.isEmpty())
		{
			return it.getItem().interactLivingEntity(it, pl, list.get(0), hand);
		}
		return InteractionResult.PASS;
	}
	
	public static InteractionResultHolder<ItemStack> itemClick(Level w, BlockPos pos, Direction face, ItemStack it, ServerPlayer pl, InteractionHand hand)
	{
		InteractionResult result = itemEntityClick(w, pos, it, pl, hand);
		if(result.consumesAction())
		{
			return new InteractionResultHolder<ItemStack>(result, it);
		}
		else
		{
			result = itemUseBase(w, pos, face, it, pl, hand);
			if(result.consumesAction())
			{
				return new InteractionResultHolder<ItemStack>(result, it);
			}
			else
			{
				return itemRightClickBase(w, it, pl, hand);
			}
		}
	}
	
	public static void setupPlayer(ServerPlayer pl, BlockPos blockPos, Direction face)
	{
		double x = blockPos.getX()+0.5 + 0.5*face.getStepX();
		double y = blockPos.getY()-1 - 0.5*face.getStepY();
		double z = blockPos.getZ()+0.5 + 0.5*face.getStepZ();
		
		pl.setPos(x, y, z);
		
		//pl.rotationYaw - N-O-S-W;
		//pl.rotationPitch - Oben-Unten;
		switch(face)
		{
		case DOWN:
			pl.setXRot(90);
			pl.setYRot(0);
			break;
		case UP:
			pl.setXRot(-90);
			pl.setYRot(0);
			break;
		case SOUTH:
			pl.setXRot(45);
			pl.setYRot(0);
			break;
		case NORTH:
			pl.setXRot(45);
			pl.setYRot(-180);
			break;
		case WEST:
			pl.setXRot(45);
			pl.setYRot(90);
			break;
		case EAST:
			pl.setXRot(45);
			pl.setYRot(-90);
			break;
		default:
			break;
		}
	}
}
