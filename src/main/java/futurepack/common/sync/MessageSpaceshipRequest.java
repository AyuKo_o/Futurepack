package futurepack.common.sync;

import java.util.UUID;
import java.util.function.Supplier;

import futurepack.common.spaceships.MovingShip;
import futurepack.common.spaceships.SpaceshipCashServer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;

/** Request send to the server to send spaceship data  by UUID*/
public class MessageSpaceshipRequest
{
	UUID uid;
	
	public MessageSpaceshipRequest(UUID uid)
	{
		this.uid = uid;
	}
	
	public static MessageSpaceshipRequest decode(FriendlyByteBuf buf) 
	{
		return new MessageSpaceshipRequest(new UUID(buf.readLong(), buf.readLong()));
	}
	
	public static void encode(MessageSpaceshipRequest msg, FriendlyByteBuf buf) 
	{
		buf.writeLong(msg.uid.getMostSignificantBits());
		buf.writeLong(msg.uid.getLeastSignificantBits());
	}
	
	public static void consume(MessageSpaceshipRequest message, Supplier<NetworkEvent.Context> ctx) 
	{
		MovingShip ship = SpaceshipCashServer.getShipByUUID(message.uid);
		if(ship!=null)
		{
			MessageSpaceshipResponse m = new MessageSpaceshipResponse(message.uid, ship.getBlocks());
			FPPacketHandler.CHANNEL_FUTUREPACK.reply(m, ctx.get());
		}
		ctx.get().setPacketHandled(true);
	}
	
}
