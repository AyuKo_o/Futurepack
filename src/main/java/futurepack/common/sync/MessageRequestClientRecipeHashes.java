package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.common.recipes.RecipeManagerSyncer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

public class MessageRequestClientRecipeHashes 
{

	public MessageRequestClientRecipeHashes()
	{
	}

	public static void consume(MessageRequestClientRecipeHashes message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> RecipeManagerSyncer.INSTANCE::onServerRequestHashes);
		ctx.get().setPacketHandled(true);
	}
	
	public static MessageRequestClientRecipeHashes decode(FriendlyByteBuf buf) 
	{
		return new MessageRequestClientRecipeHashes();
	}
		
	public static void encode(MessageRequestClientRecipeHashes msg, FriendlyByteBuf buf) 
	{

	}
}
