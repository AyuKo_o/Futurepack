package futurepack.common.sync;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Supplier;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import futurepack.client.render.RenderRoomAnalyzer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;

public class MessageAirFilledRoom
{
	byte cw,ch,cd;
	byte[] airData;
	int x,y,z;
	
	public MessageAirFilledRoom()
	{
		
	}
	
	public MessageAirFilledRoom(BlockPos pos, byte[] data, Vec3i size)
	{
		x=pos.getX();
		y=pos.getY();
		z=pos.getZ();
		cw = (byte) size.getX();
		ch = (byte) size.getY();
		cd = (byte) size.getZ();
		airData = data;
		
		if(4096 * cw*ch*cd != airData.length)
		{
			throw new IllegalArgumentException(String.format("Data array has not expected size!, expected %s, got %s", 4096 * cw*ch*cd, airData.length));
		}
	}

	public static MessageAirFilledRoom decode(FriendlyByteBuf buf) 
	{
		Vec3i size = new Vec3i(buf.readByte(), buf.readByte(), buf.readByte());
		BlockPos pos = new BlockPos(buf.readVarInt() * 16, buf.readByte() * 16, buf.readVarInt() * 16);

		byte[] compressed = new byte[buf.readVarInt()];
		buf.readBytes(compressed);
		byte[] airData = new byte[4096 * size.getX()*size.getY()*size.getZ()];
		try
		{
			ByteArrayInputStream in = new ByteArrayInputStream(compressed);
			GZIPInputStream gin = new GZIPInputStream(in);
			
			int len = 0;
			while(len < airData.length)
			{
				len += gin.read(airData, len, airData.length-len);
			}
			if(len < airData.length)
			{
				
				Arrays.fill(airData, len, airData.length-1, Byte.MIN_VALUE);
				System.out.println("Lost " + (airData.length - len) + " bytes");
			}
			gin.close();
			in.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		return new MessageAirFilledRoom(pos, airData, size);
	}
	
	public static void encode(MessageAirFilledRoom msg, FriendlyByteBuf buf) 
	{
		buf.writeByte(msg.cw);
		buf.writeByte(msg.ch);
		buf.writeByte(msg.cd);
		buf.writeVarInt(msg.x>>4);
		buf.writeByte(msg.y>>4);
		buf.writeVarInt(msg.z>>4);
		
		try
		{
			ByteArrayOutputStream out = new ByteArrayOutputStream(msg.airData.length);
			GZIPOutputStream gout = new GZIPOutputStream(out);
			gout.write(msg.airData);
			gout.flush();
			gout.close();
			out.close();
			byte[] compressed = out.toByteArray();
			buf.writeVarInt(compressed.length);
			buf.writeBytes(compressed);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void consume(MessageAirFilledRoom message, Supplier<NetworkEvent.Context> ctx) 
	{
		RenderRoomAnalyzer.addAirDataReceived(new BlockPos(message.x,message.y,message.z), message.airData, message.cw, message.ch, message.cd);
		ctx.get().setPacketHandled(true);
	}
}
