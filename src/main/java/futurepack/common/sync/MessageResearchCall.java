package futurepack.common.sync;

import java.util.function.Supplier;

import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;

/**
 * Sended to Server when need a Players Research Data
*/
public class MessageResearchCall
{
	public static MessageResearchCall decode(FriendlyByteBuf buf) 
	{
		return new MessageResearchCall();
	}
	
	public static void encode(MessageResearchCall msg, FriendlyByteBuf buf) 
	{
		
	}
	
	public static void consume(MessageResearchCall message, Supplier<NetworkEvent.Context> ctx) 
	{
		NetworkHandler.sendResearchesToPlayer(ctx.get().getSender());
		ctx.get().setPacketHandled(true);
	}
	
}
