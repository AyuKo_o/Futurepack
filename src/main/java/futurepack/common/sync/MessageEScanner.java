package futurepack.common.sync;

import java.util.List;
import java.util.function.Supplier;

import futurepack.common.FPLog;
import futurepack.common.gui.escanner.GuiDungeonTabletPageBase;
import futurepack.common.gui.escanner.GuiScannerPageBase;
import futurepack.common.gui.escanner.GuiScannerPageInfo;
import futurepack.depend.api.helper.HelperJSON;
import net.minecraft.client.Minecraft;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Component;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

/**Used to send the Server data in JSOn form to the client for EScanner*/
public class MessageEScanner
{
	private String json;
	private boolean showInDungeon = false;

	public MessageEScanner(Component...components)
	{
		this(HelperJSON.ChatToJSON(components));
	}
	
	public MessageEScanner(boolean showInDungeon, Component...components)
	{
		this(components);
		this.showInDungeon = showInDungeon;
	}
	
	private MessageEScanner(String s)
	{
		json = s;
		FPLog.logger.debug("Create EScanner massage with %s",s);
	}

	private static void showGUi(final List<Component> list, boolean showInDungeon)
	{			
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				Minecraft.getInstance().submitAsync(() -> {
					GuiScannerPageBase gui;
					if(showInDungeon)
					{
						gui = new GuiDungeonTabletPageBase(list.toArray(new Component[list.size()]));
					}
					else
					{
						gui = new GuiScannerPageInfo(showInDungeon, list.toArray(new Component[list.size()]));
					}
					
					Minecraft.getInstance().setScreen(gui);
				});
			}
		});
		
	}
	
	public static MessageEScanner decode(FriendlyByteBuf buf) 
	{
		boolean showInDungeon = buf.readBoolean();
		byte[] data = new byte[buf.readVarInt()];
		buf.readBytes(data);
		String json = new String(data);
		int l = buf.readVarInt();
		if(json.length() < l)
		{
			FPLog.logger.error("Something has gone wrong. The received String is shorter than the sended one");
			if(json.endsWith("\""))
			{
				json += "\"}";
			}
		}
		MessageEScanner msg = new MessageEScanner(json);
		msg.showInDungeon = showInDungeon;
		return msg;
	}
		
	public static void encode(MessageEScanner msg, FriendlyByteBuf buf) 
	{
		buf.writeBoolean(msg.showInDungeon);
		byte[] data = msg.json.getBytes();
		buf.writeVarInt(data.length);
		buf.writeBytes(data);
		buf.writeVarInt(msg.json.length());
	}
			
	public static void consume(MessageEScanner message, Supplier<NetworkEvent.Context> ctx) 
	{
		try
		{
			Component comp = Component.Serializer.fromJsonLenient(message.json);	
			showGUi(comp.getSiblings(), message.showInDungeon);
			ctx.get().setPacketHandled(true);
		}
		catch(NoSuchMethodError e)
		{ }
		catch(Exception e)
		{
			e.printStackTrace();
			FPLog.logger.error(e);
			FPLog.logger.error(message.json);
		}
	}
	
}
