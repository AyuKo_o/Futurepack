package futurepack.common.sync;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.function.Supplier;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import futurepack.common.gui.inventory.GuiBoardComputer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.network.NetworkEvent;

/** Send the Requested terrain from the server to the client */
public class MessageSendTerrain
{
	int w,h;
	BlockState[] blocks;
	byte[] highMap;
	
	public MessageSendTerrain(byte[] b) throws IOException
	{
		setBytes(b);	
	}
	
	public MessageSendTerrain(int w, int h, BlockState[] states, int[] highMap)
	{
		this.w = w;
		this.h = h;
		this.blocks = states;
		this.highMap = fixMap(highMap);
	}
	
	private static byte[] fixMap(int...ints)
	{
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		
		for(int i : ints)
		{
			min = Math.min(i, min);
			max = Math.max(i,  max);
		}
		
		int delta = max-min;
		float fix = 127F / delta;
		
		byte[] bytes = new byte[ints.length];
		
		for(int i=0;i<ints.length;i++)
		{
			bytes[i] = (byte) ((ints[i]-min)*fix);
		}
		
		return bytes;
	}
	
	
	public static MessageSendTerrain decode(FriendlyByteBuf buf) 
	{
		MessageSendTerrain msg = null;
		byte[] bytes = new byte[buf.readVarInt()];
		buf.readBytes(bytes);
		try 
		{
			msg = new MessageSendTerrain(bytes);
			return msg;
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		return null;
	}

	public static void encode(MessageSendTerrain msg, FriendlyByteBuf buf) 
	{
		try
		{
			byte[] bytes = msg.getBytes();
			buf.writeVarInt(bytes.length);
			buf.writeBytes(bytes);
		}
		catch (IOException e) {
			e.printStackTrace();
		}	
	}
		
		
	public static void consume(MessageSendTerrain message, Supplier<NetworkEvent.Context> ctx) 
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				Screen gui = Minecraft.getInstance().screen;
				if(gui instanceof GuiBoardComputer)
				{
					((GuiBoardComputer)gui).setData(message.w,message.h,message.blocks,message.highMap);
				}
			}
					
		});
		ctx.get().setPacketHandled(true);
	}
	
	private byte[] getBytes() throws IOException
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		DataOutputStream data = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(out)));
		
		data.writeInt(w);
		data.writeInt(h);
		
		for(BlockState state : blocks)
		{
			data.writeInt(Block.getId(state));
		}
		data.write(highMap);
		
		data.flush();
		data.close();
		return out.toByteArray();
	}
	
	private void setBytes(byte[] bytes) throws IOException
	{
		ByteArrayInputStream in = new ByteArrayInputStream(bytes);
		DataInputStream data = new DataInputStream(new BufferedInputStream(new GZIPInputStream(in)));
		
		w = data.readInt();
		h = data.readInt();
		
		blocks = new BlockState[w*h];
		highMap = new byte[w*h];
		
		for(int i=0;i<blocks.length;i++)
		{
			blocks[i] = Block.stateById(data.readInt());
		}
		
		data.readFully(highMap);
		
		data.close();
		in.close();
	}
}
