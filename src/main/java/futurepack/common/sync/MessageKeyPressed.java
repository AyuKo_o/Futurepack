package futurepack.common.sync;

import java.util.function.Supplier;

import futurepack.common.sync.KeyManager.EnumKeyTypes;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;

/**
 * Send a pressed key to the server
*/
public class MessageKeyPressed
{
	private int id;
	
	public MessageKeyPressed(int id)
	{
		this.id = id;
	}
	
	public MessageKeyPressed(EnumKeyTypes type)
	{
		this.id = type.ordinal();
	}

	public static MessageKeyPressed decode(FriendlyByteBuf buf) 
	{
		return new MessageKeyPressed(buf.readVarInt());
	}
	
	public static void encode(MessageKeyPressed msg, FriendlyByteBuf buf) 
	{
		buf.writeVarInt(msg.id);
	}
	
	public static void consume(MessageKeyPressed message, Supplier<NetworkEvent.Context> ctx) 
	{
		KeyManager.onPlayerKeyInput(ctx.get().getSender(), EnumKeyTypes.values()[message.id]);
		ctx.get().setPacketHandled(true);
	}
}
