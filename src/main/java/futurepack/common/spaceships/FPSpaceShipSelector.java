package futurepack.common.spaceships;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.function.Predicate;

import futurepack.api.ParentCoords;
import futurepack.api.helper.HelperTags;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.ISelector;
import futurepack.api.interfaces.ISpaceshipSelector;
import futurepack.api.interfaces.ISpaceshipUpgrade;
import futurepack.api.interfaces.tilentity.ITileFuelProvider;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPLog;
import futurepack.common.FPSounds;
import futurepack.common.FuturepackTags;
import futurepack.common.PredicateStatisticsManager;
import futurepack.common.SelectInterface;
import futurepack.common.block.inventory.TileEntityBoardComputer;
import futurepack.common.entity.CapabilityPlayerData;
import futurepack.depend.api.helper.HelperChunks;
import futurepack.depend.api.helper.HelperEntities;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.game.ClientboundSoundPacket;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.decoration.HangingEntity;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.AABB;

public class FPSpaceShipSelector implements ISpaceshipSelector
{
	private static final ArrayList<Predicate<Block>> predicates = new ArrayList<>(10);
	
	public static final Predicate<Block> thrusters =  b -> HelperTags.isBlock(b, FuturepackTags.thruster);
	public static final Predicate<Block> teleporter = b -> HelperTags.isBlock(b, FuturepackTags.teleporter);
	public static final Predicate<Block> neon_producer = b -> HelperTags.isBlock(b, FuturepackTags.neon_producer);
	
	static
	{
		registerPredicate(thrusters);
		registerPredicate(teleporter);
		registerPredicate(neon_producer);
	}
	
	public static void registerPredicate(Predicate<Block> p)
	{
		predicates.add(p);
	}
	
	@SuppressWarnings("unchecked")
	public static PredicateStatisticsManager getSpaceshipStatManager()
	{
		predicates.trimToSize();
		return new PredicateStatisticsManager(predicates.toArray(new Predicate[predicates.size()]));
	}
	
	protected final FPBlockSelector select;
	private final Level w;
	private BlockPos computer_pos;
	
	
	//Start: Only Used For Ships
	private int sx, sy, sz;
	private int ex, ey, ez;
	
	private int width, height, depth;
	
	private int[][] maxHeight;
	private int[][] minHeight;
	
	private int allblocks = -1;
	private UUID uid = null;
	
	private Collection<ParentCoords> blocks; 
	
	private HashMap<ISpaceshipUpgrade,Boolean> upgradeMap = new HashMap<ISpaceshipUpgrade, Boolean>();
	
	private Predicate<BlockPos> heightMap = FPSpaceShipSelector.this::isInArea;
	
	
//	public FPSpaceShipSelector(World w) 
//	{
//		this(w,base);
//	}
//	
	/**
	 * Note: this
	 */
	public FPSpaceShipSelector(FPBlockSelector sel) 
	{
		this.w = sel.getWorld();
		this.select = sel;
		if(sel.getStatisticsManager() == null)
			throw new IllegalArgumentException("BlockSelector Needs Statistcs Manager!");
	}
	
	/**
	 * Short , this Starts the Selection of Blocks by {@link IBlockSelector}
	*/
	public int selectShip(BlockPos pos, Material m)
	{
		if(w.getBlockState(pos).getMaterial() == m)
		{
			int size = select.getBlocks(pos, m);
			blocks = select.getAllBlocks();
			sx = ex = pos.getX();
			sy = ey = pos.getY();
			sz = ez = pos.getZ();
			setPosition();
			calcHighMap();
			return size;
		}
		return -1;
	}
	
	
//	/**
//	 * Check if the Block already is in the Block List
//	*/
//	private boolean isRegistert(BlockPos pos)
//	{
//		return blocks.containsKey(pos);
//	}
	
	/**
	 * Create a Start and end Position of Current Blocks
	*/
	private void setPosition()
	{
		for(BlockPos c : blocks)
		{
			sx = Math.min(sx, c.getX());
			sy = Math.min(sy, c.getY());
			sz = Math.min(sz, c.getZ());
			
			ex = Math.max(ex, c.getX());
			ey = Math.max(ey, c.getY());
			ez = Math.max(ez, c.getZ());
		}
		ex++;
		ey++;
		ez++;
	}
	
	/**
	 * This calc's a Map of wich max/min high Blocks will selectet
	*/
	private void calcHighMap()
	{
		width = Math.abs(ex - sx);
		depth = Math.abs(ez - sz);
		height = ey - sy;
		
		maxHeight = new int[width][depth];
		minHeight = new int[width][depth];
		
		for(int[] i : minHeight)
			Arrays.fill(i, w.getMaxBuildHeight());
		for(int[] i : maxHeight)
			Arrays.fill(i, 0);
		
		for(BlockPos c : blocks)
		{
			//int m = (c.getPosX()-sx) + (c.getPosZ()-sz) * width;
			
			maxHeight[c.getX()-sx][c.getZ()-sz] = Math.max(c.getY(), maxHeight[c.getX()-sx][c.getZ()-sz]);
			
			minHeight[c.getX()-sx][c.getZ()-sz] = Math.min(c.getY(), minHeight[c.getX()-sx][c.getZ()-sz]);
			
		}
	}
	
	/**
	 * Checks if the Blocks are inside of the Above Created High Map 
	*/
	@Override
	public boolean isInHeight(int x, int y, int z)
	{
		boolean b1 = y >= minHeight[x][z];
		boolean b2 = y <= maxHeight[x][z];

		return b1 && b2;		
	}
	
	/**
	 *  Checks if the Blocks are inside of the sx,sz and ex,ez and checks the HighMap
	 */
	@Override
	public boolean isInArea(int x, int y, int z)
	{
		boolean b1 = x >= sx && x < ex;
		boolean b2 = z >= sz && z < ez;
		
		if(b1 && b2)
			return isInHeight(x-sx, y, z-sz);
		
		return false;
		
	}
	
	@Override
	public boolean isInArea(BlockPos pos)
	{
		return isInArea(pos.getX(), pos.getY(), pos.getZ());
	}
	
	
/*	public ItemStack copieBlocks(boolean b1)
	{
		NBTTagList tiles = new NBTTagList();
		int[] blocks = new int[(width+1) * (height+1) * (depth+1)];
		byte[] metas = new byte[(width+1) * (height+1) * (depth+1)];
		
		for(int j=0;j<width;j++)
		{
			for(int k=0;k<height;k++)
			{
				for(int l=0;l<depth;l++)
				{
					int x = sx + j;
					int y = sy + k;
					int z = sz + l;
					
					if(isInHeight(j, y, l))
					{
						BlockPos xyz = new BlockPos(x,y,z);
						IBlockState st = w.getBlockState(xyz);
						Block b = st.getBlock();
						byte meta = (byte) b.getMetaFromState(st);
						
						select.addBlockToStatistiks(b);
						
						int p = getPos(j, k, l, width+1, height+1, depth+1);
						blocks[p] = Block.getIdFromBlock(b);
						metas[p] = meta;
	
						TileEntity tile = w.getTileEntity(xyz);
						if(tile != null)
						{
							NBTTagCompound nbt = new NBTTagCompound();
							tile.write(nbt);
							nbt.putInt("x", j);
				            nbt.putInt("y", k);
				            nbt.putInt("z", l);
				            
				            tiles.add(nbt);

						}
						
						if(b1)
						{
							w.removeBlock(xyz);
						}
					}
				}
			}
		}
		
		ItemStack it = new ItemStack(FPItems.tools, 1, 1);
		it.setTag(new NBTTagCompound());
		it.getTag().setTag("tiles", tiles);
		it.getTag().setIntArray("blocks", blocks);
		it.getTag().setByteArray("metas", metas);
		it.getTag().setInteger("w", width);
		it.getTag().setInteger("h", height);
		it.getTag().setInteger("d", depth);
		it.getTag().setBoolean("remove", b1);
		if(computer_pos!=null)
		{
			NBTTagCompound nbt = new NBTTagCompound();	
			nbt.putInt("x", computer_pos.getX()-sx);
			nbt.putInt("y", computer_pos.getY()-sy);
			nbt.putInt("z", computer_pos.getZ()-sz);
			it.getTag().setTag("pos", nbt);
		}
		return it;
	}

 	public void createSpaceship(BlockPos xyz, ItemStack it)
	{
		if(it.getItem() instanceof ItemSpaceship && it.getTag() != null && it.getItemDamage()==1)
		{
			width = it.getTag().getInteger("w")+1;
			height = it.getTag().getInteger("h")+1;
			depth = it.getTag().getInteger("d")+1;
			int[] blocks = it.getTag().getIntArray("blocks");
			byte[] metas = it.getTag().getByteArray("metas");
			
			if(it.getTag().hasKey("pos"))
			{
				NBTTagCompound nbt = it.getTag().getCompoundTag("pos");
				xyz = xyz.add( -nbt.getInteger("x"), -nbt.getInteger("y"), -nbt.getInteger("z"));

			}
			
			for(int j=0;j<width-1;j++)
			{
				for(int k=0;k<height-1;k++)
				{
					for(int l=0;l<depth-1;l++)
					{
						int p = getPos(j, k, l, width, height, depth);
						Block b= Block.getBlockById(blocks[p]);
						byte meta = metas[p];
						@SuppressWarnings("deprecation")
						IBlockState state = b.getStateFromMeta(meta);
						
						w.setBlockState(xyz.add(j,k,l), state, 2);
						
					}
				}
			}
			
			NBTTagList li = it.getTag().getTagList("tiles", 10);
			for(int i=0;i<li.size();i++)
			{			
				NBTTagCompound nbt = li.getCompound(i);
				BlockPos jkl = xyz.add(nbt.getInteger("x"),nbt.getInteger("y"),nbt.getInteger("z"));
				nbt.putInt("x", jkl.getX());
				nbt.putInt("y", jkl.getY());
				nbt.putInt("z", jkl.getZ());
					
				TileEntity ti = w.getTileEntity(new BlockPos(jkl));
				if(ti != null)
				{
					System.out.println("set Tile " + ti);
					ti.read(nbt);
					System.out.println(nbt);
				
				}
			}
		}
	}*/
	
 	
	public int transferShips(ServerLevel newWorld)
 	{
		return transferShips(newWorld, sx, sz);
 	}
	
 	public int transferShips(ServerLevel newWorld, int newX, int newZ)
 	{
 		if(newWorld==w)
 		{
 			FPLog.logger.fatal("Tried to Jump to the same world!, this whould result in destruction of the Spaceship!!!");
 			return 0;
 		}
 		FPLog.logger.info("JUMPING!");
 		
 		int h_ = 200;
 		int h1 = h_ - sy;
 		int h2 = ey + h1;
 		int h3 = newWorld.getMaxBuildHeight()-1 - h2;
 		if(h3<0)
 		{
 			h_ += h3;
 		}
 		int dif_ = h_-sy;
 		TileEntityBoardComputer bcomp = (TileEntityBoardComputer) w.getBlockEntity(computer_pos);
 		if(bcomp==null)
 		{
 			FPLog.logger.error("Nope cant Teleport without a Computer");
 			return 0;
 		}
 			
 		if(newWorld.dimension().location().equals(new ResourceLocation("overworld")))
		{	
 			int h = bcomp.getOldSy();
 			if(h!=-1)
 			{
 				h_ = h;
 			}
 			dif_ = h_ - sy;
		}
 		else if(w.dimension().location().equals(new ResourceLocation("overworld")))
 		{
 			bcomp.setOldPos(sy,ey);
 		}
 		
 		final int dY = dif_;
 		final int h = h_;
 		
 		SafeBlockMover move = new SafeBlockMover((ServerLevel) w, newWorld);
 		move.setStartCoords(new BlockPos(sx,sy,sz), width, height, depth, heightMap);
 		move.setEndCoords(new BlockPos(newX,h,newZ));
 		

 		FPLog.logger.info("New Pos at %s,%s,%s",sx,h,sz);
 		
 		moveEntities(newWorld, newX-sx, dY, newZ-sz);
 		
 		move.copie();
 		move.delete();
 		
 		
		FPLog.logger.info("Done");
 		return dY;
 	}
 	
 	/**
 	 * this moves all Entitys inside the Ship into the new Dimension
 	 */
	private void moveEntities(ServerLevel newWorld, int dX, int dY, int dZ)
	{
		//Teleporting Entities
 		List<Entity> list = getEntitysInShip();
 		
 		for(Entity e : list)
 		{		
 			//no client; no deleted Entity
 	        if (!e.level.isClientSide && e.isAlive())
 	        {
 	        	ServerLevel worldserver = (ServerLevel) e.level;
 	         
 	        	//keep double precision -> no entities in walls
 	        	double x = e.getX() + dX;
 	        	int y = (int) e.getY() + dY;
 	        	double z = e.getZ() + dZ;
 			
 	        	if(e instanceof ServerPlayer)
 	        	{
	 				ServerPlayer thePlayer = (ServerPlayer) e;
	 				CompoundTag fp = CapabilityPlayerData.getPlayerdata(thePlayer);
	 				fp.putLong("lastJump", System.currentTimeMillis());
	 				
	 				thePlayer.connection.send(new ClientboundSoundPacket(FPSounds.JUMP, SoundSource.BLOCKS, thePlayer.getX(), thePlayer.getY(), thePlayer.getZ(), 1.0F, 1.0F));
	 				
//	 				newWorld.getServer().getPlayerList().changePlayerDimension(thePlayer, newWorld.getDimensionType(), new SpaceTeleporter(newWorld, dif));
	 				HelperEntities.transportPlayerToDimnsion(thePlayer, newWorld);
	 				
	 				thePlayer.teleportTo(x, y+0.1, z);	
 	        	}
 	        	else
 	        	{	
// 	        		e.world.removeEntity(e);
 	        		//keep them alive for ticking
// 	        		e.removed = false;	
 	        		e.remove(Entity.RemovalReason.UNLOADED_TO_CHUNK);
 	        		//give them a last update
// 	        		worldserver.tickEntity(e, false);
 	        		e.tick();

 	        		Entity newE = e.getType().create(newWorld);
 	        		//copy nbt
 	        		copyDataFromOld(e, newE);
 	        		
 	        		//stupid pictures need special love
	 		        if(e instanceof HangingEntity)
	 		        {
	 		        	BlockPos bp = ((HangingEntity)e).getPos();
	 		        	newE.setPos(bp.getX() + dX , bp.getY() + dY, bp.getZ() + dZ);  //no update - it would break the chunk	
	 		        	newE.xo = newE.getX();
	 		        	newE.yo = newE.getY();
	 		        	newE.zo = newE.getZ();
	 		        	newE.xOld = newE.getX();
	 		        	newE.yOld = newE.getY();
	 		        	newE.zOld = newE.getZ();
	 		        }
	 		        else
	 		        {
	 		        	newE.setPos(x, y+0.1, z); //no update - it would break the chunk	
	 		        }
	 		        
	 		        //force spawn in new world   -- see chengeDimension()
	 		        boolean flag = newE.isAddedToWorld();
	 		        newE.onAddedToWorld();
	 		        newWorld.addFreshEntity(newE);
	 		        if(flag)
	 		        	 newE.onAddedToWorld();
	 		        else
	 		        	newE.onRemovedFromWorld();
	 		        
	 		        //tick the new copy
//	 		        newWorld.tickEntity(newE, false);
	 		        newE.tick();
	 		        
//	 		        e.revive();
                
	 		        //allow them to die
	 		        e.discard();;

 	        	}
 			}	
 		}
	}
	
	private List<Entity> getEntitysInShip()
	{
		return w.getEntitiesOfClass(Entity.class, new AABB(sx, sy, sz, ex, ey, ez), new Predicate<Entity>()
 		{	
 			@Override
 			public boolean test(Entity e) 
 			{
 				return isInArea(Mth.floor(e.getX()), Mth.floor(e.getY()), Mth.floor(e.getZ()));
 			}
 		});
	}
	
    private void copyDataFromOld(Entity en, Entity eo)
    {
        CompoundTag nbttagcompound = en.saveWithoutId(new CompoundTag());
        nbttagcompound.remove("Dimension");
        eo.load(nbttagcompound);
        //eo.lastPortalPos = en.lastPortalPos;
        //eo.lastPortalVec = en.lastPortalVec;
        //eo.teleportDirection = en.teleportDirection;
    }
	
	public void useFuel(int amountPerThruster)
	{		
		int t1 = select.getStatisticsManager().getBlockCountFromPredicate(thrusters);
		Collection<ParentCoords> fuel = select.getValidBlocks(new SelectInterface(ITileFuelProvider.class));
		
		for(ParentCoords pc : fuel)
		{
			ITileFuelProvider prov = (ITileFuelProvider) w.getBlockEntity(pc);
			while(prov.useFuel(amountPerThruster))
			{
				t1--;
				if(t1<=0)
				{
					return;
				}
			}		
		}
	}
	
	public static int getPos(int x, int y, int z, int w, int h, int d)
	{
		if(x>=0 && y>=0 && z>=0 && x<w && y<h && z<d)
		{
			return ((y)*w*d) + z * w + x; 
		}
		
		return -1; 
	}

	public void setComputerPos(BlockPos pos2)
	{
		this.computer_pos = pos2;
	}

	public void clear()
	{
		computer_pos=null;
		blocks.clear();
		sx=0;sy=0;sz=0;
		ex=0;ey=0;ez=0;
		
		width=0;height=0;depth=0;
		
		maxHeight=null;
		minHeight=null;
	}
	
	public boolean preloadWorld(Level w)
	{
		if(w == null)
		{
			FPLog.logger.fatal("Tried to preload null as world!");
			return false;
		}
		
		FPLog.logger.info("Preloding world %s(%s) for Jump ", w.dimension(), w.dimensionType());

		int sx = (this.sx>>4) -1;
		int sz = (this.sz>>4) -1;
		int ex = (this.ex>>4) +1;
		int ez = (this.ez>>4) +1;
		
		int amount = (ex-sx) * (ez-sz);
		Set<ChunkPos> forcedChunks = new HashSet<>(amount);
		
		for(int x = sx>>4-5;x<ex>>4+5;x++)
		{
			for(int z=sz>>4-5;z<ez>>4+5;z++)
			{		
				if(HelperChunks.forceChunkIfNeeded(w, x, z))
				{
					forcedChunks.add(new ChunkPos(x, z));
				}
			}
		}
		
		for(int x = sx>>4-10;x<ex>>4+10;x++)
		{
			for(int z=sz>>4-10;z<ez>>4+10;z++)
			{		
				LevelChunk c = w.getChunk(x, z);
			}
		}
		
		forcedChunks.forEach(p -> HelperChunks.unforceChunks(w, p.x, p.z));
		
		return true;
	}

	
	
	@Override
	public int getBlockTotal()
	{
		if(allblocks==-1)
		{
			for(int j=0;j<width;j++)
			{
				for(int k=0;k<height;k++)
				{
					for(int l=0;l<depth;l++)
					{
						int x = sx + j;
						int y = sy + k;
						int z = sz + l;					
						if(isInHeight(j, y, l))
						{
							if(!w.isEmptyBlock(new BlockPos(x,y,z)))
							{
								allblocks++;
							}
						}
					}
				}
			}
		}
		return allblocks;
	}
	
	@Override
	public boolean hasUpgrade(ISpaceshipUpgrade up)
	{
		Boolean state = upgradeMap.computeIfAbsent(up, p -> up.isUpgradeInstalled(this));
		return state;
	}
	
	
	public ISpaceshipUpgrade[] getMissingUpgrades()
	{
		return upgradeMap.entrySet().stream().filter(e -> e.getValue() == false).map(e -> e.getKey()).toArray(ISpaceshipUpgrade[]::new);
	}


	@Override
	public int getWidth()
	{
		return width;
	}


	@Override
	public int getHeight()
	{
		return height;
	}


	@Override
	public int getDepth()
	{
		return depth;
	}


	@Override
	public Level getWorld()
	{
		return w;
	}


	@Override
	public BlockPos getStart()
	{
		return new BlockPos(sx,sy,sz);
	}


	@Override
	public BlockPos getEnd()
	{
		return new BlockPos(ex,ey,ez);
	}


	@Override
	public ISelector getSelector()
	{
		return select;
	}


	
	//This is for calculation needed fuel
	//1mb bitripentium = 1200 blocks
	
	public static int getFuel(BlockPos start, BlockPos end)
	{
		double dis = Math.sqrt(start.distSqr(end));
		return getFuel(dis);		
	}
	
	public static int getFuel(double dis)
	{
		dis /= 1200D;
		
		int fuel = Math.round((float) dis);
		if(fuel==0 && dis>0)
		{
			fuel = 1;
		}
		return fuel;	
	}
	
	public boolean isSpaceAvailable(BlockPos newStart)
	{
		BlockPos diff = newStart.offset(-sx,-sy,-sz);
		for(int x=sx;x<=ex;x++)
		{
			for(int z=sz;z<=ez;z++)
			{
				for(int y=sy;y<=ey;y++)
				{
					if(isInArea(x,y,z))
					{
						if(!w.isEmptyBlock(diff.offset(x,y,z)))
							return false;
					}
				}
			}
		}
		return true;
	}
	
	public void moveShip(BlockPos newStart)
	{
		FPLog.logger.info("JUMPING!");
		
		SafeBlockMover move = new SafeBlockMover((ServerLevel) w, (ServerLevel)w);
 		move.setStartCoords(new BlockPos(sx,sy,sz), width, height, depth, heightMap);
 		move.setEndCoords(newStart);
 		
 		if(!move.checkWorldBorders())
 		{
 			throw new IllegalArgumentException("Try to teleprt outside of World borders!");
 		}
 		move.copie(); //This will need imense time due to chunk loading and generating and stuff
 		
 		
 		BlockPos diff = newStart.offset(-sx,-sy,-sz);
 		
 		List<Entity> list = getEntitysInShip();
 		
 		list.forEach( e -> 
 		{
 			double x = e.getX() + diff.getX();
 			double y = e.getY() + diff.getY();
 			double z = e.getZ() + diff.getZ();
 			e.teleportTo(x, y, z);
 		});
 		
 		move.delete();

 		FPLog.logger.info("New Pos at %s",newStart);
	}
	
//	public MovingShip createMovingShip(BlockPos newStart)
//	{
//		FPLog.logger.info("Flying!");
//	
//		//Experimental!!!
//
//		SafeBlockMover move = new SafeBlockMover((ServerWorld) w, (ServerWorld)w);
// 		move.setStartCoords(new BlockPos(sx,sy,sz), width, height, depth, heightMap);
// 		move.setEndCoords(newStart);
// 		
// 		if(!move.checkWorldBorders())
// 		{
// 			throw new IllegalArgumentException("Try to teleprt outside of World borders!");
// 		}
//
// 		MovingShip ship = move.createMovableShip(null);
// 		return ship;
//	}
	
	public UUID getUniceID()
	{
		if(uid!=null)
			return uid;
		
		if(blocks==null)
			throw new IllegalStateException("Blocks for ship have not been selected yet!");
		
		return SpaceshipHasher.hash(SpaceshipHasher.getAllBlocks(w, blocks));
	}
}
