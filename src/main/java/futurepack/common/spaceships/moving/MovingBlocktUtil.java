package futurepack.common.spaceships.moving;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import futurepack.common.FPBlockSelector;
import futurepack.common.block.logistic.LogisticBlocks;
import futurepack.common.block.logistic.frames.TileEntityMovingBlocks;
import futurepack.depend.api.MiniWorld;
import futurepack.depend.api.StableConstants.BlockFlags;
import futurepack.depend.api.helper.HelperItems;
import futurepack.world.dimensions.TreeUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.ticks.LevelTicks;

public class MovingBlocktUtil
{

	private static List<BiConsumer<Level, BlockPos>> listPreTileEntiyRemove = new ArrayList<>();


	public static TileEntityMovingBlocks beginMoveBlocks(FPBlockSelector selected, Vec3i direction, Consumer<SimpleCollision> callback)
	{
		if(!selected.getWorld().isClientSide())
		{
			SimpleCollision col = new SimpleCollision(selected, direction, pos -> selected.getWorld().isEmptyBlock(pos) || selected.getWorld().getBlockState(pos).getPistonPushReaction() == PushReaction.DESTROY);

			if(col.canMove())
			{
				MiniWorld mw = createFromBlockSelector(selected);
				//original blocks are still there

				if(callback!=null)
					callback.accept(col);

				MovingSheduledTicks<Block> pendingBlockTicks = MovingSheduledTicks.createBlock((ServerLevel) selected.getWorld(), col, direction);
				MovingSheduledTicks<Fluid> pendingFluidTicks = MovingSheduledTicks.createFluid((ServerLevel) selected.getWorld(), col, direction);


				replaceBlocksFast(selected.getWorld(), col);
				//on old positons is air
				//an all positions where block will be are barrier blocks

				BlockPos randomNewPos = col.getPositionsToCheck().stream().findAny().get();

				selected.getWorld().setBlock(randomNewPos, LogisticBlocks.moving_blocks.defaultBlockState(), BlockFlags.DEFAULT | 128);
				TileEntityMovingBlocks mb = (TileEntityMovingBlocks) selected.getWorld().getBlockEntity(randomNewPos);
	//			mw.start = randomNewPos;

				mb.setMiniWorld(mw);
				mb.setDirection(direction);
				mb.setPendingTicks(pendingBlockTicks, pendingFluidTicks);

				return mb;
			}
		}
		return null;
	}

	public static void endMoveBlocks(Level w, MiniWorld mw, Vec3i direction, @Nullable MovingSheduledTicks<Block> pendingBlockTicks, @Nullable MovingSheduledTicks<Fluid> pendingFluidTicks)
	{
		if(!w.isClientSide())
		{
			place(mw, w, mw.start.offset(direction));
			if(pendingBlockTicks!=null)
			{
				pendingBlockTicks.sheduleTicks((LevelTicks<Block>) w.getBlockTicks());
			}
			if(pendingFluidTicks!=null)
			{
				pendingFluidTicks.sheduleTicks((LevelTicks<Fluid>)w.getFluidTicks());
			}
		}
	}

	public static MiniWorld createFromBlockSelector(FPBlockSelector selected)
	{
		return TreeUtils.copyFromWorld((WorldGenLevel)selected.getWorld(), selected.getAllBlocks());
	}

	public static void replaceBlocksFast(Level w, SimpleCollision collision)
	{
		int flag = BlockFlags.DEFAULT | BlockFlags.IS_MOVING | Block.UPDATE_SUPPRESS_LIGHT | BlockFlags.NO_NEIGHBOR_DROPS; //the 128 prevents light updates - yes this will look ugly probaply, but its fast
		//HelperItems.disableItemSpawn();


		Predicate<ItemEntity> stopDrops = e -> collision.contains(e.blockPosition(), true, false);

		HelperItems.addItemRemover(stopDrops);

		Consumer<BlockPos> remTileEntity = p -> {
			listPreTileEntiyRemove.forEach(c -> c.accept(w, p));
			w.removeBlockEntity(p);
		};

		collision.getOldPositions().forEach(remTileEntity);
		collision.getUnchangedPositions().forEach(remTileEntity);
		collision.getPositionsToCheck().forEach(remTileEntity);

		collision.getOldPositions().forEach(pos -> {
			FluidState fluidstate = w.getFluidState(pos);
			w.setBlock(pos, fluidstate.createLegacyBlock(), BlockFlags.DEFAULT); //no neighbour updates
	    });
		collision.getUnchangedPositions().forEach(pos -> w.setBlock(pos, Blocks.BARRIER.defaultBlockState(), flag));
		collision.getPositionsToCheck().forEach(pos -> w.setBlock(pos, Blocks.BARRIER.defaultBlockState(), flag));

		//HelperItems.enableItemSpawn();
		HelperItems.removeItemRemover(stopDrops);
	}

	@FunctionalInterface
	public static interface TileEntityCreator<T extends BlockEntity>
	{
		@Nullable
		T createNew(BlockPos.MutableBlockPos pos, BlockState state, T oldTile, MiniWorld mw, Level w, int x, int y, int z);
	}

	private static Map<BlockEntityType<?>, TileEntityCreator<?>> mapCreator = new IdentityHashMap<>();

	public static void place(MiniWorld mw, Level w, BlockPos pos)
	{
		BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos();
		BlockState void_air = Blocks.VOID_AIR.defaultBlockState();
		for(int x=0;x<mw.width;x++)
		{
			mut.setX(x + pos.getX());

			for(int y=0;y<mw.height;y++)
			{
				mut.setY(y + pos.getY());

				for(int z=0;z<mw.depth;z++)
				{
					mut.setZ(z + pos.getZ());

					if(mw.states[x][y][z]!=void_air)
					{
						w.setBlock(mut, mw.states[x][y][z], BlockFlags.DEFAULT_AND_RERENDER);
						if(mw.tiles[x][y][z]!=null)
						{
							TileEntityCreator creator = mapCreator.get(mw.tiles[x][y][z].getType());
							if(creator!=null)
							{
								BlockEntity tile = creator.createNew(mut, mw.states[x][y][z], mw.tiles[x][y][z], mw, w, x, y, z);
								if(tile!=null)
								{
									w.setBlockEntity(tile);
									continue;
								}
							}
							w.setBlockEntity(BlockEntity.loadStatic(mut.immutable(), mw.states[x][y][z], mw.tiles[x][y][z].saveWithFullMetadata())); //create a copy at the new position
						}
					}
				}
			}
		}
	}

	public static void registerTileEntityRemoveCallback( BiConsumer<Level, BlockPos> callback)
	{
		listPreTileEntiyRemove.add(callback);
	}

	public static <T extends BlockEntity> void registerTileEntityCreator(BlockEntityType<T> type, TileEntityCreator<T> creator)
	{
		mapCreator.put(type, creator);
	}
}
