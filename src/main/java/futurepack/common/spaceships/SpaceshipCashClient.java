package futurepack.common.spaceships;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.Callable;

import futurepack.common.AsyncTaskManager;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageSpaceshipRequest;
import futurepack.depend.api.MiniWorld;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtIo;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;

public class SpaceshipCashClient
{
	private static WeakHashMap<UUID, MiniWorld> loaded = new WeakHashMap<UUID, MiniWorld>();
	
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public static MiniWorld getShipFromID(Level w,UUID uid)
	{
		MiniWorld world = null;
		world = loaded.get(uid);
		if(world!=null)
			return world;
		world = getFromLocalCash(w, uid);
		if(world!=null)
		{
			loaded.put(uid, world);
			return world;
		}
		
		sendRequestToServer(uid);
				
		return null;
	}

	private static MiniWorld getFromLocalCash(Level w, UUID uid)
	{
		File folder = new File("./spaceships");
		folder.mkdirs();	
		File file = new File(folder, uid + ".dat");
		
		CompoundTag nbt;
		try {
			nbt = NbtIo.read(file);
			if(nbt!=null)
			{
				MiniWorld world = new MiniWorld(w, nbt);
				return world;
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	private static void sendRequestToServer(UUID uid)
	{
		FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageSpaceshipRequest(uid));
	}

	public static void addSpaceship(UUID uid, BlockState[][][] blocks)
	{
		if(loaded.containsKey(uid))
		{
			System.out.println("Already have data!");
			return;
		}
		
		int w = blocks.length;
		int h = blocks[0].length;
		int d = blocks[0][0].length;
		
		MiniWorld mini = new MiniWorld(new BlockPos(0, 0, 0), new BlockPos(w,h,d), 89);
		for(int x=0;x<w+1;x++)
			for(int y=0;y<h+1;y++)
				for(int z=0;z<d+1;z++)
					mini.states[x][y][z] = (x<w && y<h && z<d) ? blocks[x][y][z] : Blocks.AIR.defaultBlockState();
		
		
		for(Biome[] bio : mini.bioms)
			Arrays.fill(bio, Biomes.PLAINS);
		
		for(Integer[][] in1 : mini.blocklight)
			for(Integer[] in2 : in1)
				Arrays.fill(in2, 15);
		
		for(Integer[][] in1 : mini.skylight)
			for(Integer[] in2 : in1)
				Arrays.fill(in2, 15);
		
		for(Integer[][][] in1 : mini.redstone)
			for(Integer[][] in2 : in1)
				for(Integer[] in3 : in2)
					Arrays.fill(in3, 0);
		
		mini.rotationpoint = new Vec3(w/2D, h/2D, d/2D);
		mini.face= Direction.UP;
		
		save(uid, mini);
		loaded.put(uid, mini);
	}
	
	private static void save(UUID uid, MiniWorld mini)
	{
		AsyncTaskManager.addTask(AsyncTaskManager.FILE_IO, new Callable<Boolean>()
		{	
			@Override
			public Boolean call() throws Exception 
			{
				File folder = new File("./spaceships");
				folder.mkdirs();	
				File file = new File(folder, uid + ".dat");
				if(!file.exists()) 
				{
					try
					{
						CompoundTag nbt = mini.serializeNBT();
						NbtIo.write(nbt, file);
					}
					catch (IOException e) {
						e.printStackTrace();
					}
				}
				return true;
			}
		});
	}
}
