package futurepack.common;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import futurepack.common.conditions.ConditionRegistry;
import futurepack.common.recipes.Json2Recipes;
import futurepack.common.recipes.RecipeManagerSyncer;
import futurepack.common.research.MagnetismManager;
import futurepack.common.spaceships.Json2PlanetReader;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.packs.resources.Resource;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;

public class DataFolderWalker 
{
	public static Gson gson = new GsonBuilder().create();
	private static List<DWEntry> list = new ArrayList<DataFolderWalker.DWEntry>();
	private static List<Consumer<String>> callbacks = new ArrayList<Consumer<String>>();
	
	private static MinecraftServer server;
	
	
	public static class RecipeReloadListener extends SimplePreparableReloadListener
	{
		public RecipeReloadListener() 
		{
			
		}
		
		@Override
		protected Object prepare(ResourceManager resourceManagerIn, ProfilerFiller profilerIn) 
		{
			DataFolderWalker.scanDataDir(resourceManagerIn, profilerIn);
			return null;
		}

		@Override
		protected void apply(Object splashList, ResourceManager resourceManagerIn, ProfilerFiller profilerIn) 
		{
			FuturepackMain.INSTANCE.reloadAllRecipes();
			AsyncTaskManager.RESOURCE_RELOAD.joinWithStats();
			if(DataFolderWalker.server!=null)
				RecipeManagerSyncer.INSTANCE.onRecipeReload(DataFolderWalker.server);
		}
	};
	
	static
	{
		registerFolderSearch(ConditionRegistry.FOLDER_NAME, ConditionRegistry::readJson);
		registerFolderSearch("planets", Json2PlanetReader::readerJsons);
		registerFolderSearch("fp_recipes", Json2Recipes::readerJson);
		registerFolderSearch("magnetism", MagnetismManager::readJson);
		
		registerCallback(ConditionRegistry::loadingFolderFinishedCallback);
	}
	
	public static void registerFolderSearch(String folderName, BiConsumer<ResourceLocation, JsonObject> consum) 
	{
		list.add(new DWEntry(folderName, consum));
	}

	public static void registerCallback(Consumer<String> callback) 
	{
		callbacks.add(callback);
	}
	
	public static void scanDataDir(ResourceManager rman, ProfilerFiller profilerIn)
	{
		list.parallelStream().forEach(entry -> 
		{
			profilerIn.startTick();
			profilerIn.push("data/" +  entry.folderName);
			for(ResourceLocation res : rman.listResources(entry.folderName, n -> n.endsWith(".json")))
			{
				JsonObject result = null;
				try 
				{
					for(Resource ir : rman.getResources(res))
					{
						InputStreamReader inr = new InputStreamReader(ir.getInputStream());
						JsonObject obj = gson.fromJson(inr, JsonObject.class);
						inr.close();
						
						if(result!=null)
							result = merge(result, obj);
						else
							result = obj;
					}
					ResourceLocation internalID = new ResourceLocation(res.getNamespace(), res.getPath().substring(entry.folderName.length() +1, res.getPath().length() - 5));
					entry.consum.accept(internalID, result);
				}
				catch (JsonSyntaxException e) {
					FPLog.logger.error("json error [%s] while reading %s", e.getMessage(), res.toString());
					e.printStackTrace();
				} catch (JsonIOException e) {
					FPLog.logger.error("json read error [%s] while reading %s", e.getMessage(), res.toString());
					e.printStackTrace();
				} catch (IOException e) {
					FPLog.logger.error("read error [%s] while reading %s", e.getMessage(), res.toString());
					e.printStackTrace();
				}
			}
			callbacks.forEach(c -> c.accept(entry.folderName));
			profilerIn.pop();
			profilerIn.endTick();
		});
	}
	
	private static JsonObject merge(final JsonObject old, JsonObject overrite)
	{
		for(Entry<String, JsonElement> e : overrite.entrySet())
		{
			JsonElement overwritten = old.remove(e.getKey());
			if(overwritten!=null)
			{
				if(overwritten.isJsonObject() && e.getValue().isJsonObject())
				{
					overwritten = merge(overwritten.getAsJsonObject(), e.getValue().getAsJsonObject());
					old.add(e.getKey(), overwritten);
				}
				else
				{
					old.add(e.getKey(), e.getValue());
				}
			}
			else
			{
				old.add(e.getKey(), e.getValue());
			}
			
			
		}
		return old;
	}
	
	private static class DWEntry
	{
		private final String folderName;
		private final BiConsumer<ResourceLocation, JsonObject> consum;
		
		public DWEntry(String folderName, BiConsumer<ResourceLocation, JsonObject> consum) 
		{
			super();
			this.folderName = folderName;
			this.consum = consum;
		}
		
		
	}

	public static void setServer(MinecraftServer server2)
	{
		DataFolderWalker.server = server2;
		RecipeManagerSyncer.INSTANCE.onRecipeReload(server2);
	}
}
