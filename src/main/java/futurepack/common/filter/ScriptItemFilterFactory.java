package futurepack.common.filter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.api.interfaces.filter.IItemFilterFactory;
import futurepack.common.item.misc.MiscItems;
import futurepack.depend.api.helper.HelperCompression;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Blocks;

public class ScriptItemFilterFactory implements IItemFilterFactory
{

	@Override
	public IItemFilter createFilter(ItemStack stack)
	{
		if(stack.getItem() == MiscItems.script_filter && stack.hasTag())
		{
			CompoundTag nbt = stack.getTagElement("script");
			if(nbt != null)
			{
				return new ScriptItemFilter(stack);
			}
		}
		return null;
	}


	private static String getOrCreateFilterName(ItemStack stack)
	{
		if(stack.getItem() == MiscItems.script_filter)
		{
			if(!stack.hasTag())
			{
				stack.setTag(new CompoundTag());
			}

			CompoundTag nbt = stack.getOrCreateTagElement("script");
			if(nbt.contains("script_name"))
			{
				return nbt.getString("script_name");
			}
			else
			{
				int number = (int) System.currentTimeMillis();
				String hex = Integer.toHexString(number);
				String name = "item_" + hex+".js";
				nbt.putString("script_name", name);
				Writer out;
				try
				{
					out = getFilterScriptWriter(stack);
					out.write(getBaseScript());
					out.close();
				}
				catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}

				return name;
			}
		}
		throw new IllegalArgumentException("not a filter item!");
	}

	public static Reader getFilterScript(ItemStack stack) throws FileNotFoundException
	{
		if(stack.getItem() == MiscItems.script_filter)
		{
			stack.getOrCreateTag();
			CompoundTag nbt = stack.getOrCreateTagElement("script");
			if(nbt.contains("script"))
			{
				StringReader reader = new StringReader(HelperCompression.decompressString(nbt.getByteArray("script")));
				return reader;
			}
			else if(nbt.contains("script_name"))
			{
				return getFilterScriptFromFile(nbt.getString("script_name"));
			}
		}



		return null;
	}

	public static Writer getFilterScriptWriter(ItemStack stack) throws FileNotFoundException
	{
		CallbackStringWriter w = new CallbackStringWriter(writer -> {
			String uncompressed = writer.toString();
			byte[] compressed = HelperCompression.compressString(uncompressed);

			if(compressed.length > 1024 * 1024) //1MB
			{
				Writer file = getFilterScriptWriterFromFile(getOrCreateFilterName(stack));

				stack.getOrCreateTag();
				CompoundTag nbt = stack.getOrCreateTagElement("script");
				if(nbt.contains("script"))
				{
					nbt.remove("script");
				}
				file.write(uncompressed);
				file.close();
			}
			else
			{
				stack.getOrCreateTag();
				CompoundTag nbt = stack.getOrCreateTagElement("script");
				nbt.putByteArray("script", compressed);
				if(nbt.contains("script_name"))
				{
					nbt.remove("script_name");
				}
			}
		});

		return w;
	}

	public static Reader getFilterScriptFromFile(String scriptname) throws FileNotFoundException
	{
		File script = new File("./filter_scripts/", scriptname);
		return new InputStreamReader(new FileInputStream(script), StandardCharsets.UTF_8);
	}

	public static Writer getFilterScriptWriterFromFile(String scriptname) throws FileNotFoundException
	{
		File scriptDir = new File("./filter_scripts/");
		scriptDir.mkdirs();
		File script = new File(scriptDir, scriptname);
		return new OutputStreamWriter(new FileOutputStream(script), StandardCharsets.UTF_8);
	}

	private static String getBaseScript()
	{
		return ""
				+ "//if you need to save additional information use the 'data' variable, it will keep the information accross sessions.\n"
				+ "\n"
				+ "/*\n"
				+ " * Needed.\n"
				+ " * This is called for each item passing the filter, see IItem for more info\n"
				+ "*/\n"
				+ "function filterItem(item)\n"
				+ "{\n"
				+ "  return true;\n"
				+ "}\n"
				+ "\n"
				+ "/*\n"
				+ " * Optional.\n"
				+ " * If you need to do anything stacksize dependant here is a nice callback. This is called each time an item actually passes the filter with the amount that was acually tranfered.\n"
				+ "*/\n"
				+ "function transferItemCallback(item)\n"
				+ "{\n"
				+ "}\n\n";
	}

	public static Exception compileAndTest(ItemStack filterItem)
	{
		Exception e = ScriptItemFilter.compileScript(filterItem, null);
		if(e==null)
		{
			//ItemStack dummy = new ItemStack(MiscItems.script_filter, 1);
			//CompoundTag nbt = dummy.getOrCreateTagElement("script");
			//nbt.putString("script_name", name);
			ScriptItemFilter filter = new ScriptItemFilter(filterItem);

			try
			{
				filter.loadBindings();
				filter.testUnsafe(new ItemStack(Blocks.DIRT));
				filter.amountTransferedUnsafe(new ItemStack(Blocks.DIRT));
			}
			catch(Exception ee)
			{
				return ee;
			}
		}
		return e;
	}

}
