package futurepack.common.filter;

import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.depend.api.helper.HelperItemFilter;
import net.minecraft.world.item.ItemStack;

public class ItemStackFilter implements IItemFilter
{
	private ItemStack stack;


	public ItemStackFilter()
	{ }

	public ItemStackFilter(ItemStack it)
	{
		stack = it;
	}

	@Override
	public boolean test(ItemStack input)
	{
		if(input==null)
			return false;

		return stack.sameItem(input) && HelperItemFilter.tagMatches(stack, input);
	}
}
