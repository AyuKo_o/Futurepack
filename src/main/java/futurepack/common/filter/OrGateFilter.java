package futurepack.common.filter;

import futurepack.api.interfaces.filter.IItemFilter;
import net.minecraft.world.item.ItemStack;

public class OrGateFilter implements IItemFilter
{
	private IItemFilter[] filters;
	
	public OrGateFilter(IItemFilter...filters)
	{
		this.filters = filters;
	}
	
	public OrGateFilter() 
	{
	}

	@Override
	public boolean test(ItemStack input)
	{
		for(IItemFilter base : filters)
		{
			if(base.test(input))
			{
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void amountTransfered(ItemStack transfered) 
	{
		for(IItemFilter f : filters)
			f.amountTransfered(transfered);
	}
	
	public boolean isEmpty()
	{
		return filters==null || filters.length==0;
	}
}
