package futurepack.common.fluids;

import java.util.function.Function;

import futurepack.api.Constants;
import futurepack.client.AdditionalTextures;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.BucketItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.LiquidBlock;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fluids.FluidAttributes;
import net.minecraftforge.fluids.ForgeFlowingFluid;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistry;

public class FPFluids {

	public static final Fluid OLD_LAVA = Fluids.LAVA;
	
	public static final ForgeFlowingFluid.Properties props_neon = new ForgeFlowingFluid.Properties(() -> FPFluids.neonFluidStill, () -> FPFluids.neonFluid_flowing, 
			FluidAttributes.builder(AdditionalTextures.NEON_FLUID_STILL, AdditionalTextures.NEON_FLUID_FLOW)
			.translationKey("liquid.neon")
			.temperature(273 +200)
			.luminosity(10)
			.viscosity(1500)
			.density(200))
			.block(() -> FPFluids.neon_fluid_block)
			.bucket(() -> FPFluids.neon_fluid_bucket);
	public static final ForgeFlowingFluid.Properties props_bitripentium = new ForgeFlowingFluid.Properties(() -> FPFluids.bitripentiumFluidStill, () -> FPFluids.bitripentiumFluid_flowing,
			FluidAttributes.builder(AdditionalTextures.BITRIPTENTIUM_FLUID_STILL, AdditionalTextures.BITRIPTENTIUM_FLUID_FLOW)
			.translationKey("liquid.bitripentium")
			.temperature(200)
			.viscosity(10000)
			.density(26000));
	public static final ForgeFlowingFluid.Properties props_salt_water = new ForgeFlowingFluid.Properties(() -> FPFluids.salt_waterStill, () -> FPFluids.salt_water_flowing,
			FluidAttributes.builder(AdditionalTextures.SALTWATER_FLUID_STILL, AdditionalTextures.SALTWATER_FLUID_FLOW)
			.translationKey("saltwater")
			.density(1240)
			.viscosity(1040))
			.block(() -> FPFluids.salt_water_fluid_block)
			.bucket(() -> FPFluids.salt_water_fluid_bucket);
	public static final ForgeFlowingFluid.Properties props_biogas = new ForgeFlowingFluid.Properties(() -> FPFluids.biogasFluidStill, () -> FPFluids.biogasFluid_flowing,
			FluidAttributes.builder(AdditionalTextures.BIOGAS_FLUID, AdditionalTextures.BIOGAS_FLUID)
			.translationKey("liquid.biogas")
			.temperature(273+60)
			.viscosity(100)
			.density(-79)//former -100, -100 is now methan
			.gaseous());
	public static final ForgeFlowingFluid.Properties props_oxygen_gas = new ForgeFlowingFluid.Properties(() -> FPFluids.oxygenGasStill, () -> FPFluids.oxygenGas_flowing,
			FluidAttributes.builder(AdditionalTextures.GENERIC_FLUID_STILL, AdditionalTextures.GENERIC_FLUID_FLOW)
			.translationKey("gaseous.oxygen")
			.temperature(273+25)
			.viscosity(100)
			.density(-50)
			.gaseous()
			.color(0xFF58f1ff))
			.bucket(() -> FPFluids.oxygen_gas_bucket);
	public static final ForgeFlowingFluid.Properties props_hydrogen_gas = new ForgeFlowingFluid.Properties(() -> FPFluids.hydrogenGasStill, () -> FPFluids.hydrogenGas_flowing,
			FluidAttributes.builder(AdditionalTextures.GENERIC_FLUID_STILL, AdditionalTextures.GENERIC_FLUID_FLOW)
			.translationKey("gaseous.hydrogen")
			.temperature(273+25)
			.viscosity(50)
			.density(-500)
			.gaseous()
			.color(0xFFc1f3ff))
			.bucket(() -> FPFluids.hydrogen_gas_bucket);
	public static final ForgeFlowingFluid.Properties props_plasma = new ForgeFlowingFluid.Properties(() -> FPFluids.plasmaStill, () -> FPFluids.plasma_flowing,
			FluidAttributes.builder(AdditionalTextures.GENERIC_FLUID_STILL, AdditionalTextures.GENERIC_FLUID_FLOW)
			.translationKey("energy.plasma")
			.temperature(15000)
			.viscosity(500)
			.density(-25)
			.gaseous()
			.color(0xff8922ff));

	//biagas -> 66% methan, 33% CO2
	// 2 x methan -> ethan + wasserstoff
	//propen + wasserstoff -> propan

	//dichte in kg * m^-3 * -69,444444 = gas wert;

	//Sauerstoff: 1,429 kg/m^3 (0°C) = -100
	
	//biogas: 2/3 * 0,72 + 1/3 * 1,98 = 1,14 -> -79
	//methan  0,72 kg/m^3 (0°C, 1013hPa) = -50
	//methan: 25: gas ; -165 flüssig - no
	//ethan 1,36 g/l = 1,36 kg/m^3 = -94
	//ethan: 25°C: gas ; -90°C flüssig - no
	//propan 2,01 kg/m^3 -> -140
	//propan: -43 flüssig
	//propen 1,91 kg/m^3 -> -133
	//propen: -48 flüssig
	//wasserstoff/hydrogen 0,08999 kg/m^3 -> -6,24
	//H2
	//kohlenstoffdioxid/carbondioxide
	//Kohlenstoff dioxide: 1,98 kg/m^3 (0°C 1013 hPa) = -137
	
	public static final ForgeFlowingFluid neonFluidStill = (ForgeFlowingFluid) HelperItems.setRegistryName(new NeonFluidBlock.Source(props_neon), Constants.MOD_ID, "neon");
	public static final ForgeFlowingFluid bitripentiumFluidStill = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Source(props_bitripentium), Constants.MOD_ID, "bitripentium");
	public static final ForgeFlowingFluid salt_waterStill = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Source(props_salt_water), Constants.MOD_ID, "salt_water");
	public static final ForgeFlowingFluid biogasFluidStill = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Source(props_biogas), Constants.MOD_ID, "biogas");
	public static final ForgeFlowingFluid oxygenGasStill = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Source(props_oxygen_gas), Constants.MOD_ID, "oxygen");
	public static final ForgeFlowingFluid hydrogenGasStill = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Source(props_hydrogen_gas), Constants.MOD_ID, "hydrogen");
	public static final ForgeFlowingFluid plasmaStill = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Source(props_plasma), Constants.MOD_ID, "plasma");
	
	//only needed for inw orld stuff, so dont use this
	public static final ForgeFlowingFluid neonFluid_flowing = (ForgeFlowingFluid) HelperItems.setRegistryName(new NeonFluidBlock.Flowing(props_neon), Constants.MOD_ID, "neon_flowing");
	private static final ForgeFlowingFluid bitripentiumFluid_flowing = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Flowing(props_bitripentium), Constants.MOD_ID, "bitripentium_flowing");
	public static final ForgeFlowingFluid salt_water_flowing = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Flowing(props_salt_water), Constants.MOD_ID, "salt_water_flowing");
	private static final ForgeFlowingFluid biogasFluid_flowing = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Flowing(props_biogas), Constants.MOD_ID, "biogas_flowing");
	private static final ForgeFlowingFluid oxygenGas_flowing = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Flowing(props_oxygen_gas), Constants.MOD_ID, "oxygen_flowing");
	private static final ForgeFlowingFluid hydrogenGas_flowing = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Flowing(props_hydrogen_gas), Constants.MOD_ID, "hydrogen_flowing");
	private static final ForgeFlowingFluid plasma_flowing = (ForgeFlowingFluid) HelperItems.setRegistryName(new ForgeFlowingFluid.Flowing(props_plasma), Constants.MOD_ID, "plasma_flowing");
	
	
	public static LiquidBlock neon_fluid_block = (LiquidBlock) HelperItems.setRegistryName(new LiquidBlock(() -> FPFluids.neonFluid_flowing, Block.Properties.of(Material.WATER).strength(100.0F).noDrops()), Constants.MOD_ID, "neon_fluid");
	public static LiquidBlock salt_water_fluid_block = (LiquidBlock) HelperItems.setRegistryName(new LiquidBlock(() -> FPFluids.salt_water_flowing, Block.Properties.of(Material.WATER).strength(100.0F).noDrops()), Constants.MOD_ID, "salt_water_fluid");
	
	public static Item neon_fluid_bucket = HelperItems.setRegistryName(new BucketItem( () -> FPFluids.neonFluidStill, new Item.Properties().craftRemainder(Items.BUCKET).stacksTo(1).tab(CreativeModeTab.TAB_MISC) ), Constants.MOD_ID, "neon_bucket");
	public static Item salt_water_fluid_bucket = HelperItems.setRegistryName(new BucketItem( () -> FPFluids.salt_waterStill, new Item.Properties().craftRemainder(Items.BUCKET).stacksTo(1).tab(CreativeModeTab.TAB_MISC) ), Constants.MOD_ID, "salt_water_bucket");
	public static Item hydrogen_gas_bucket = HelperItems.setRegistryName(new BucketItem( () -> FPFluids.hydrogenGasStill, new Item.Properties().craftRemainder(Items.BUCKET).stacksTo(1).tab(CreativeModeTab.TAB_MISC) ), Constants.MOD_ID, "hydrogen_gas_bucket");
	public static Item oxygen_gas_bucket = HelperItems.setRegistryName(new BucketItem( () -> FPFluids.oxygenGasStill, new Item.Properties().craftRemainder(Items.BUCKET).stacksTo(1).tab(CreativeModeTab.TAB_MISC) ), Constants.MOD_ID, "oxygen_gas_bucket");
	
	public static void register(RegistryEvent.Register<Fluid> event)
	{
		IForgeRegistry<Fluid> r = event.getRegistry();
		r.registerAll(neonFluidStill, biogasFluidStill, bitripentiumFluidStill, salt_waterStill, oxygenGasStill, hydrogenGasStill,  plasmaStill);
		r.registerAll(neonFluid_flowing, bitripentiumFluid_flowing, salt_water_flowing, biogasFluid_flowing, oxygenGas_flowing, hydrogenGas_flowing, plasma_flowing);
		
		if(ModList.get().isLoaded(Constants.TINKERS_CONSTRUCT))
		{
			registerTinkersFluids(event);
		}
	}
	
	public static void registerBlocks(RegistryEvent.Register<Block> event)
	{
		IForgeRegistry<Block> r = event.getRegistry();
		r.registerAll(neon_fluid_block, salt_water_fluid_block);
	}
	
	public static void registerItems(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		r.registerAll(neon_fluid_bucket, salt_water_fluid_bucket, hydrogen_gas_bucket, oxygen_gas_bucket);
	}
	
	
	private static void registerTinkersFluids(RegistryEvent.Register<Fluid> event)
	{
		
		//String[] materials = {"composite_metal", "neon", "retium", "glowtite", "bioterium"};
		String[] materials = {"molten_composite_metal"};
		int[] temperature = {900};
		int[] density = {(int) ((8960+7874) * 0.5)};//dichte eisen + kupfer
		int[] viscosity = {13000};
		int[] color = {0xFFea7542};
		ResourceLocation[] stillTex = {new ResourceLocation(Constants.TINKERS_CONSTRUCT, "block/fluid/molten/still")};
		ResourceLocation[] flowingTex = {new ResourceLocation(Constants.TINKERS_CONSTRUCT, "block/fluid/molten/flowing")};
		
		final Function<ResourceLocation, Fluid> getFluidFunc = ForgeRegistries.FLUIDS::getValue;
		
		for(int i=0;i<materials.length;i++)
		{
			final ResourceLocation still = new ResourceLocation(Constants.MOD_ID, materials[i]);
			final ResourceLocation flowing = new ResourceLocation(Constants.MOD_ID, materials[i]+ "_flowing");
			
			ForgeFlowingFluid.Properties props_molten_material = new ForgeFlowingFluid.Properties(() -> getFluidFunc.apply(still), () -> getFluidFunc.apply(flowing),
					FluidAttributes.builder(stillTex[i]==null ? AdditionalTextures.get(still) : stillTex[i], flowingTex[i]==null ? AdditionalTextures.get(still) : flowingTex[i])
					.translationKey("futurepack." + materials[i])
					.temperature(273+temperature[i])
					.viscosity(viscosity[i])
					.density(density[i])
					.color(color[i])
					);
			
			ForgeFlowingFluid props_molten_material_still = (ForgeFlowingFluid) new ForgeFlowingFluid.Source(props_molten_material).setRegistryName(still);
			
			ForgeFlowingFluid props_molten_material_flowing = (ForgeFlowingFluid) new ForgeFlowingFluid.Flowing(props_molten_material).setRegistryName(flowing);
			
			event.getRegistry().register(props_molten_material_still);
			event.getRegistry().register(props_molten_material_flowing);
		}
		
		
	}

}
