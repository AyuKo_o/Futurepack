package futurepack.common;

//Vanilla has now the force load command.
public class FPChunckManager// implements LoadingCallback
{
//	private static Map<Entity,Ticket> entityMap = new WeakHashMap<Entity, ForgeChunkManager.Ticket>();
//	private static Map<World,Ticket> worlds = new WeakHashMap<World, ForgeChunkManager.Ticket>();
//	private static Map<World, Map<String, Ticket>> playerMap = new WeakHashMap<World, Map<String, Ticket>>();
//	
//	
	public static void clean()
	{
//		entityMap.clear();
//		worlds.clear();
//		playerMap.forEach((w,m)->m.clear());
//		playerMap.clear();
	}
//	
//	/**
//	 * This only registers the Tickets, the Tickets have no chunks linked to them anymore :/
//	 */
//	@Override
//	public void ticketsLoaded(List<Ticket> tickets, World world)
//	{
//		for(Ticket ticket : tickets)
//		{
//			if(ticket.isPlayerTicket())
//			{
//				ForgeChunkManager.releaseTicket(ticket);
//			}
//			else if(ticket.getEntity()!=null)
//			{
//				entityMap.put(ticket.getEntity(), ticket);
//				FPLog.logger.debug("Loding Chunks(FP) for %s in %s",ticket.getEntity(),world.dimension.getDimension());
//				int x = MathHelper.floor(ticket.getEntity().posX) >>4;
//				int z = MathHelper.floor(ticket.getEntity().posZ) >>4;
//				ForgeChunkManager.forceChunk(ticket, new ChunkPos(x, z));
//				
//				if(world.getEntityByID(ticket.getEntity().getEntityId())!=ticket.getEntity())
//				{
//					world.addEntity(ticket.getEntity());
//				}
//			}
//			else if(ticket.getType() ==Type.NORMAL)
//			{
//				if(worlds.containsKey(ticket.world))
//				{
//					ForgeChunkManager.releaseTicket(ticket);
//				}
//				else
//				{
//					worlds.put(ticket.world, ticket);
//				}
//				
//			}
//		}
//	}
//	
//	public static Ticket getTicketForEntity(Entity e)
//	{
//		if(entityMap.containsKey(e))
//		{
//			return entityMap.get(e);
//		}
//		Ticket t = ForgeChunkManager.requestTicket(FPMain.instance, e.world, Type.ENTITY);
//		t.bindEntity(e);
//		entityMap.put(e, t);
//		return t;
//	}
//	
//	@SubscribeEvent
//	public void onWorldLoad(WorldEvent.Load event)
//	{	
//		World w = event.getWorld();
//		if(w.isRemote)
//			return;
//		String subDir = w.dimension.getSaveFolder();
//		subDir = subDir==null?"":subDir;
//		File dir = new File(w.getSaveHandler().getWorldDirectory(),subDir);
//		File file = new File(dir,"FPChunks.dat");
//		FPLog.logger.info("Loading Chunks(FP) for "+dir);
//		JsonReader reader = null;
//		if(file.exists())
//		{
//			try
//			{
//				GZIPInputStream in = new GZIPInputStream(new FileInputStream(file));
//				reader = new JsonReader(new InputStreamReader(in));
//				Gson gson = new Gson();
//				JsonArray array = gson.fromJson(reader, JsonArray.class);
//							
//				if(array==null)
//				{
//					reader.close();
//					return;
//				}
//				for(int i=0;i<array.size();i++)
//				{
//					JsonObject obj = (JsonObject) array.get(i);
//					if(obj.has("player"))
//					{
//						Ticket t = getClaimeTicketForPlayer(w, obj.get("player").getAsString());
//						ChunkPos chunk = new ChunkPos(obj.get("chunkX").getAsInt(),obj.get("chunkZ").getAsInt());
//						ForgeChunkManager.forceChunk(t, chunk);
//					}
//					else
//					{
//						Ticket t = getClaimeTicketForWorld(w);
//						ChunkPos chunk = new ChunkPos(obj.get("chunkX").getAsInt(),obj.get("chunkZ").getAsInt());
//						ForgeChunkManager.forceChunk(t, chunk);
//					}
//				}
//			}
//			catch(Exception e)
//			{
//				e.printStackTrace();
//				System.err.println("What the Hell has happend ?!");
//				System.err.println("Deleting Chunkloader-File");
//				file.delete();
//			}
//		}
//		if(reader!=null)
//		{
//			try {
//				reader.close();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//		
//	}
//	
//	@SubscribeEvent
//	public void onWorldSave(WorldEvent.Save event) throws Exception
//	{
//		World w = event.getWorld();
//		if(w.isRemote)
//			return;
//		String subDir = w.dimension.getSaveFolder();
//		subDir = subDir==null?"":subDir;
//		File dir = new File(w.getSaveHandler().getWorldDirectory(),subDir);
//		File file = new File(dir,"FPChunks.dat");
//		JsonArray array = new JsonArray();
//		if(worlds.containsKey(w))
//		{
//			Ticket claimes = worlds.get(w);
//			saveTicketInArray(w, claimes, array);
//		}
//		if(playerMap.containsKey(w))
//		{
//			Map<String, Ticket> claimes = playerMap.get(w);
//			claimes.forEach((p,t) -> saveTicketInArray(w, t, array));
//		}
//		try
//		{
//			GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(file));
//			JsonWriter writer = new JsonWriter(new OutputStreamWriter(out));
//			Gson gson = new Gson();
//			gson.toJson(array, writer);
//			writer.close();
//		}
//		catch(FileNotFoundException e)
//		{
//			FPLog.logger.error("Saving ForcedChunks Failed!");
//		}
//	}
//	
//	private void saveTicketInArray(World w, Ticket t, JsonArray array)
//	{
//		if(t!=null)
//		{
//			ImmutableSet<ChunkPos> chunks = t.getChunkList();
//			for(ChunkPos chunk : chunks)
//			{
//				if(validateChunkLoad(w, t, chunk))
//				{
//					JsonObject obj = new JsonObject();
//					obj.add("chunkX",new JsonPrimitive(chunk.x));
//					obj.add("chunkZ",new JsonPrimitive(chunk.z));
//					if(t.isPlayerTicket())
//					{
//						obj.addProperty("player", t.getPlayerName());
//					}
//					array.add(obj);
//				}
//				
//			}
//		}
//	}
//	
//	public boolean validateChunkLoad(World w, Ticket t, ChunkPos chunk)
//	{
//		Chunk c = w.getChunk(chunk.x, chunk.z);
//		for(Entry<BlockPos, TileEntity> e : c.getTileEntityMap().entrySet())
//		{
//			if(e.getValue().getClass() == TileEntityClaime.class)
//				return true;
//		}
//		
//		return false;
//	}
//	
//	
////	private static final Field list = getField();
////	
////	private static Field getField()
////	{
////		try 
////		{
////			Class<Ticket> ticket = ForgeChunkManager.Ticket.class;
////			Field list = ticket.getDeclaredField("requestedChunks");
////			list.setAccessible(true);
////			return list;
////		}
////		catch (Exception e)
////		{
////			e.printStackTrace();
////		} 
////		return null;
////	};
////	
////	public static LinkedHashSet<ChunkPos> getFullList(Ticket t)
////	{
////		if(t==null)
////			return null;
////		try {
////			return 	(LinkedHashSet<ChunkPos>) list.get(t);
////		} 
////		catch (IllegalArgumentException e) {
////			e.printStackTrace();
////		} catch (IllegalAccessException e) {
////			e.printStackTrace();
////		}
////		return null;
////	}
//	
//	@Deprecated
//	public static Ticket getClaimeTicketForWorld(World w)
//	{
//		if(worlds.containsKey(w))
//		{
//			return worlds.get(w);
//		}
//		Ticket t = ForgeChunkManager.requestTicket(FPMain.instance, w, Type.NORMAL);
//		worlds.put(w, t);
//		return t;
//		
//	}
//
//	public static Ticket getClaimeTicketForPlayer(World world, String playerName)
//	{
//		Map<String, Ticket> perDim = playerMap.getOrDefault(world, null);
//		if(perDim == null)
//		{
//			perDim = new HashMap<String, Ticket>();
//			playerMap.put(world, perDim);
//		}
//		
//		Ticket t = perDim.getOrDefault(playerName, null);
//		if(t == null)
//		{
//			t = ForgeChunkManager.requestPlayerTicket(FPMain.instance, playerName, world, Type.NORMAL);
//			perDim.put(playerName, t);
//		}
//		return t;
//	}
}
