package futurepack.common;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.WeakHashMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.collect.AbstractIterator;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.common.network.NetworkManager;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.client.Minecraft;
import net.minecraft.core.BlockPos;
import net.minecraft.core.BlockPos.MutableBlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraftforge.common.util.BlockSnapshot;

public class FPSelectorHelper
{
	private static Map<ResourceKey<Level>, Map<IBlockSelector, FPSelectorHelper>> mapDimHelper = new IdentityHashMap<ResourceKey<Level>, Map<IBlockSelector, FPSelectorHelper>>();


	public static Map<IBlockSelector, FPSelectorHelper> getHelperForWorld(Level w)
	{
		if(!mapDimHelper.containsKey(w.dimension()))
		{
//			mapDimHelper.put(w.dimension.getDimensionId(), new FPSelectorHelper(w));
			Map<IBlockSelector, FPSelectorHelper> map = Collections.synchronizedMap(new WeakHashMap<IBlockSelector, FPSelectorHelper>());
			mapDimHelper.put(w.dimension(), map);
			return map;
		}
		return mapDimHelper.get(w.dimension());
	}



	public static FPBlockSelector getSelectorSave(Level w, BlockPos pos, IBlockSelector bs, boolean onlyServerThread)
	{
		Map<IBlockSelector, FPSelectorHelper> map = getHelperForWorld(w);
		FPSelectorHelper help = map.get(bs);
		if(help==null)
		{
			help = new FPSelectorHelper(w);
			map.put(bs, help);
		}
		FPBlockSelector sel = help.getSelectorFor(pos);
		if(sel == null)
		{
			final FPBlockSelector sel2 = new FPBlockSelector(w, bs);
			if(onlyServerThread && !isWorldThread(w))
			{
				Runnable task = new Runnable()
				{
					@Override
					public void run()
					{
						w.getProfiler().push("Block Selection");
						sel2.selectBlocks(pos);
						w.getProfiler().pop();
					}
				};
				//synchronized (task)
				{
					long start = System.currentTimeMillis();
					long warned = 0;
					CompletableFuture<Void> listener = null;
					if(!w.isClientSide)
					{
						listener = w.getServer().submitAsync(task);
					}
					else
					{
						listener = Minecraft.getInstance().submitAsync(task);
					}

					int i=0;
					while(sel2.getNeededTime()<0F)
					{
						i++;
						Thread.yield(); //Not using wait to prevent look of server and network thread
						if(i>1000)
						{
							i=0;
							if(System.currentTimeMillis() - start >= 1000)
							{
								if(System.currentTimeMillis() - warned >= 10 * 60 * 1000)
								{
									System.out.println("FPSelectorHelper.getSelectorSave() is taking oddly long, bloking Thread:"+ Thread.currentThread().getName());
									warned = System.currentTimeMillis();
								}

								if(listener.isDone() || listener.isCancelled())
								{
									start = System.currentTimeMillis();
									if(!w.isClientSide)
									{
										listener = w.getServer().submitAsync(task);
									}
									else
									{
										listener = Minecraft.getInstance().submitAsync(task);
									}
								}
							}
						}
					}
				}
			}
			else
			{
				sel2.selectBlocks(pos);
			}
			help.init(pos, sel2);
			sel = sel2;

		}
		return sel;
	}

	@Deprecated
	public static FPBlockSelector getSelector(Level w, BlockPos pos, IBlockSelector bs)
	{
		return getSelectorSave(w, pos, bs, !FPConfig.SERVER.enforceExtraThreadForNetwork.get());
	}

	public static boolean isWorldThread(Level serv)
	{
		return NetworkManager.isWorldThread(serv);
	}

	public static void clean()
	{
		mapDimHelper.clear();
	}

	public static void onBlockUpdate(Level w, BlockPos pos)
	{
		ResourceKey<Level> type = (w).dimension();
		Map<IBlockSelector, FPSelectorHelper> selectors = mapDimHelper.get(type);
		if(selectors!=null)
		{
			synchronized (selectors)
			{
				for(Entry<IBlockSelector, FPSelectorHelper> e : selectors.entrySet())
				{
					e.getValue().notifyBlockUpdate(pos);
				}
			}
		}
		HelperEnergyTransfer.updateBlock(w, pos);
	}

	public static void onBlockUpdate(Level world, List<BlockSnapshot> replacedBlockSnapshots)
	{
		ResourceKey<Level> type = world.dimension();
		Map<IBlockSelector, FPSelectorHelper> selectors = mapDimHelper.get(type);
		BlockPos[] posses = replacedBlockSnapshots.stream().map(BlockSnapshot::getPos).toArray(BlockPos[]::new);
		if(selectors!=null)
		{
			synchronized (selectors)
			{
				for(Entry<IBlockSelector, FPSelectorHelper> e : selectors.entrySet())
				{
					for(BlockPos pos : posses)
					{
						e.getValue().notifyBlockUpdate(pos);
					}
				}
			}
		}
		HelperEnergyTransfer.updateBlock(world, posses);
	}

	@Nonnull
	private final Level w;
	@Nonnull
	private final Map<Vec3i,SelectorWrapper> PosToShipMap = new TreeMap<Vec3i,SelectorWrapper>();
	private final ReadWriteLock rwlock;

//	public FPSelectorHelper(int dimension)
//	{
//		this(MinecraftServer.getServer().worldServerForDimension(dimension));
//	}

	public FPSelectorHelper(Level dimension)
	{
		this.w = dimension;
		rwlock = new ReentrantReadWriteLock();
	}

	public FPBlockSelector getSelectorFor(Vec3i xyz)
	{
		rwlock.readLock().lock();
		SelectorWrapper wrapper = PosToShipMap.get(xyz);
		rwlock.readLock().unlock();

		if(wrapper==null)
			return null;

		return  wrapper.getSelector();
	}

	public void remove(SelectorWrapper sel)
	{
		area = null;

		Collection<ParentCoords> blocks = sel.blocks.getAllBlocks();
		rwlock.writeLock().lock();
		for(Vec3i xyz : blocks)
		{
			PosToShipMap.remove(xyz);
		}
		rwlock.writeLock().unlock();
		sel.blocks.clear();
	}

	private void init(BlockPos pos, FPBlockSelector sel)
	{
		area = null;

		SelectorWrapper wrapper = new SelectorWrapper(sel);
		rwlock.writeLock().lock();
		SelectorWrapper old = PosToShipMap.put(pos, wrapper);
		rwlock.writeLock().unlock();
		if(old!=null)
			old.invalidate();

		Iterator<ParentCoords> iter = sel.getAllBlocks().iterator();
		rwlock.writeLock().lock();
		while(iter.hasNext())
		{
			old = PosToShipMap.put(iter.next(), wrapper);
			if(old!=null && old != wrapper)
				old.invalidate();
		}
		rwlock.writeLock().unlock();
	}

	private void removeIfPossible(Iterable<BlockPos.MutableBlockPos> pos)
	{
		if(PosToShipMap.isEmpty())
			return;

		Set<SelectorWrapper> wrappers = new HashSet<SelectorWrapper>();
		for(Vec3i vec : pos)
		{
			debugPos(ParticleTypes.NOTE, vec);

			rwlock.readLock().lock();
			SelectorWrapper s = PosToShipMap.get(vec);
			rwlock.readLock().unlock();

			if(s!=null)
			{
				wrappers.add(s);
			}
		}
		if(!wrappers.isEmpty())
		{
			w.getProfiler().push("RemovingSelector");
			wrappers.forEach(FPSelectorHelper.this::remove);
			wrappers.clear();
			wrappers = null;
			w.getProfiler().pop();
		}
	}

	public void notifyBlockUpdate(BlockPos pos)
	{
		if(getArea().isInside(pos))
		{
			debugPos(ParticleTypes.CAMPFIRE_SIGNAL_SMOKE, pos);

			AbstractIterator<BlockPos.MutableBlockPos> iter = new AbstractIterator<BlockPos.MutableBlockPos>()
			{
				int j=0;
				BlockPos.MutableBlockPos mut = pos.mutable();

				@Override
				protected MutableBlockPos computeNext()
				{
					switch (j)
					{
					case 0:
						break;
					case 1:
						mut.move(Direction.UP);
						break;
					case 2:
						mut.move(Direction.DOWN, 2);
						break;
					case 3:
						mut.move(0,1,-1);
						break;
					case 4:
						mut.move(Direction.SOUTH, 2);
						break;
					case 5:
						mut.move(-1, 0, -1);
						break;
					case 6:
						mut.move(Direction.EAST, 2);
						break;
					default:
						endOfData();
						break;
					}
					j++;
					return mut;
				}

			};
			removeIfPossible(() -> iter);
		}
		else
		{
			debugPos(ParticleTypes.INSTANT_EFFECT, pos);
		}
	}

	@SuppressWarnings("unused")
	private void debugPos(ParticleOptions particle, Vec3i pos)
	{
		final boolean debug = false;
		if(debug && w instanceof ServerLevel)
		{
			((ServerLevel)w).sendParticles(particle, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, 1, 0F, 0F, 0F, 0F);
		}
	}

	private BoundingBox area = null;

	public BoundingBox getArea()
	{
		if(area!=null)
		{
			return area;
		}
		else
		{
			int x0=Integer.MAX_VALUE,y0=Integer.MAX_VALUE,z0=Integer.MAX_VALUE;
			int x1=Integer.MIN_VALUE,y1=Integer.MIN_VALUE,z1=Integer.MIN_VALUE;
			boolean empty = true;
			rwlock.readLock().lock();
			for(Vec3i e : PosToShipMap.keySet())
			{
				empty = false;
				x0 = Math.min(x0, e.getX());
				x1 = Math.max(x1, e.getX());
				y0 = Math.min(y0, e.getY());
				y1 = Math.max(y1, e.getY());
				z0 = Math.min(z0, e.getZ());
				z1 = Math.max(z1, e.getZ());
			}
			rwlock.readLock().unlock();

			if(empty)
			{
				return area = new BoundingBox(0, 0, 0, 0, 0, 0);
			}

			x0-=2;
			y0-=2;
			z0-=2;
			x1+=2;
			y1+=2;
			z1+=2;
			return area = new BoundingBox(x0, y0, z0, x1, y1, z1);
		}
	}



	private class SelectorWrapper
	{
		private final FPBlockSelector blocks;
		private final long creationTime;
		public boolean valid;

		SelectorWrapper(FPBlockSelector blocks)
		{
			valid = true;
			this.blocks = blocks;
			creationTime = System.currentTimeMillis();
		}

		public boolean isValid()
		{
			if(valid)
			{
				valid = System.currentTimeMillis() - creationTime < 1000 * 30; //not older then  30 seconds
				return valid;
			}
			else
			{
				return false;
			}
		}

		public void invalidate()
		{
			if(valid)
			{
				valid = false;
			}
		}

		@Nullable
		public FPBlockSelector getSelector()
		{
			if(isValid())
				return blocks;
			else
			{
				return null;
			}
		}
	}


}
