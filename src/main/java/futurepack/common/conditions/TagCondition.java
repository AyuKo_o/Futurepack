package futurepack.common.conditions;

import java.util.function.BooleanSupplier;

import com.google.gson.JsonElement;

import futurepack.api.helper.HelperTags;
import futurepack.depend.api.helper.HelperOreDict;
import net.minecraft.resources.ResourceLocation;

public class TagCondition implements BooleanSupplier
{
	private static ResourceLocation tagName;

	public TagCondition(ResourceLocation tagName)
	{
		this.tagName = tagName;
	}

	//doing this late Tags are loaded
	
	@Override
	public boolean getAsBoolean()
	{
		return HelperTags.getValues(HelperOreDict.getOptionalTag(tagName)).findAny().isPresent();
	}

	public static TagCondition fromJson(JsonElement elm)
	{
		return new TagCondition(new ResourceLocation(elm.getAsString()));
	}
}
