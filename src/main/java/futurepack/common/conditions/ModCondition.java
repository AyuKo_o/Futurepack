package futurepack.common.conditions;

import java.util.function.BooleanSupplier;

import com.google.gson.JsonElement;

import net.minecraftforge.fml.ModList;

public class ModCondition implements BooleanSupplier
{
	private final String modId;

	public ModCondition(String modId)
	{
		this.modId = modId;
	}

	@Override
	public boolean getAsBoolean()
	{
		
		ModList.get().isLoaded(modId);
		return false;
	}
	
	public static ModCondition fromJson(JsonElement elm)
	{
		return new ModCondition(elm.getAsString());
	}

}
