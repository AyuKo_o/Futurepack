package futurepack.common.conditions;

import java.util.function.BooleanSupplier;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryManager;

public class RegistryEntryCondition implements BooleanSupplier
{
	private final IForgeRegistry<?> registry;
	private final ResourceLocation entryKey;

	public RegistryEntryCondition(ResourceLocation registryKey, ResourceLocation entryKey)
	{
		this.registry = RegistryManager.ACTIVE.getRegistry(registryKey);
		Registry r =Registry.REGISTRY.get(registryKey);
		this.entryKey = entryKey;
	}

	@Override
	public boolean getAsBoolean()
	{
		return registry == null ? false : registry.containsKey(entryKey);
	}
	
	public static RegistryEntryCondition fromJson(JsonElement elm)
	{
		JsonObject obj = elm.getAsJsonObject();
		return new RegistryEntryCondition(new ResourceLocation(obj.get("registry").getAsString()), new ResourceLocation(obj.get("entry").getAsString()));
	}

}
