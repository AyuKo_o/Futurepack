package futurepack.common.conditions;

import java.util.Set;
import java.util.function.BooleanSupplier;

import com.google.gson.JsonElement;

public class AndCondition implements BooleanSupplier
{
	private final Set<BooleanSupplier> conditions;

	public AndCondition(Set<BooleanSupplier> conditions)
	{
		this.conditions = conditions;
	}
	
	public AndCondition(BooleanSupplier...conditions)
	{
		this(Set.of(conditions));
	}

	@Override
	public boolean getAsBoolean()
	{
		for(var c : conditions)
		{
			if(!c.getAsBoolean())
				return false;
		}
		return true;
	}

	public static AndCondition fromJson(JsonElement elm)
	{
		return new AndCondition(ConditionRegistry.load(elm.getAsJsonArray()));
	}
}
