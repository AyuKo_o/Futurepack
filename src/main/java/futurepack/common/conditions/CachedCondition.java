package futurepack.common.conditions;

import java.util.function.BooleanSupplier;

public class CachedCondition implements BooleanSupplier
{
	private BooleanSupplier condition;
	private boolean result;

	public CachedCondition(BooleanSupplier condition)
	{
		this.condition = condition;
	}

	@Override
	public boolean getAsBoolean()
	{
		if(condition!=null)
		{
			result = condition.getAsBoolean();
			condition = null;
		}
		return result;
	}

}
