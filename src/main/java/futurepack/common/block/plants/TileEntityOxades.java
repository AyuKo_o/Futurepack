package futurepack.common.block.plants;

import futurepack.api.interfaces.IChunkAtmosphere;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityOxades extends FPTileEntityBase implements ITileServerTickable
{
	private int ticks = 0;
	private int puff = 0;
	
	
	public TileEntityOxades(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.OXADES, pos, state);
	}
	
	public static final SoundEvent[] sound = new SoundEvent[]{SoundEvents.GRAVEL_HIT, SoundEvents.WOOL_HIT, SoundEvents.THORNS_HIT};
	
	private LazyOptional<IChunkAtmosphere> opt;
	
	@Override
	public void tickServer(Level level, BlockPos ppos, BlockState pstate)
	{
		ticks++;
		if(ticks>200)
		{
			int age = pstate.getValue(BlockOxades.AGE);
			age -= 6;
			if(age > 0)
			{
				int oxy = age * ticks;
				LevelChunk c = level.getChunkAt(worldPosition);
				if(level.isClientSide)
				{
					puff = 3+age;
					float pitch = 1.5F + level.random.nextFloat();
					level.playSound(null, worldPosition.getX()+0.5, worldPosition.getY()+0.7, worldPosition.getZ()+0.5, sound[level.random.nextInt(sound.length)], SoundSource.BLOCKS, 0.2F, pitch);
				}
				else if(opt == null)
				{
					opt = c.getCapability(AtmosphereManager.cap_ATMOSPHERE, null);
					opt.addListener( p -> opt = null);
				}
				if(opt!=null)
				{
					opt.ifPresent(atm -> {
						int o = oxy - atm.addAir(worldPosition.getX()&15, worldPosition.getY(), worldPosition.getZ()&15, oxy);
						if(o > 0)
						{
							atm.addAir(worldPosition.getX()&15, worldPosition.getY(), worldPosition.getZ()&15, o);
						}
					});
				}
			}
			ticks = 0;
		}
		if(puff>0)
		{
			for(int i=0;i<puff;i++)
			{
				float x = (level.random.nextFloat() - 0.5F) * 0.1F;
				float z = (level.random.nextFloat() - 0.5F) * 0.1F;
				level.addParticle(ParticleTypes.CLOUD, worldPosition.getX()+0.5, worldPosition.getY()+0.7, worldPosition.getZ()+0.5, x, 0.1, z);
			}
			puff--;
			
		}
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{	
		nbt.putInt("ticks", ticks);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		ticks = nbt.getInt("ticks");
		super.readDataUnsynced(nbt);
	}
}
