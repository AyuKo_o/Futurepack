package futurepack.common.block.plants;

import java.util.Random;

import futurepack.common.FPConfig;
import futurepack.common.item.FoodItems;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FallingBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockGlowmelo extends FallingBlock
{
	static final int maxAge = 6;
	public static IntegerProperty AGE_0_6 = IntegerProperty.create("age", 0, maxAge);
	
	private final VoxelShape[] boxes;
	
	public BlockGlowmelo(Block.Properties props)
	{
		super(props);
		boxes=new VoxelShape[7];
		for(int i=0;i<boxes.length;i++)
		{
			boxes[i] = Shapes.box( 0.4375F-i*0.0625F, 0.75F-i*0.125F, 0.4375F-i*0.0625F, 0.5625F+i*0.0625F, 1F, 0.5625F+i*0.0625F);
		}
	}
	
	@Override
	public ItemStack getCloneItemStack(BlockGetter worldIn, BlockPos pos, BlockState state)
	{
		return new ItemStack(FoodItems.glowmelo);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		int age = state.getValue(AGE_0_6);
		return boxes[age];
	}
	
	
	@SuppressWarnings("deprecation")
	@Override
	public void randomTick(BlockState state, ServerLevel w, BlockPos pos, Random random)
	{
		super.randomTick(state, w, pos, random);
		if(random.nextInt(5)==0)
		{	
			if(state.getValue(AGE_0_6)< maxAge)
			{
				state = state.setValue(AGE_0_6, state.getValue(AGE_0_6)+1);
				w.setBlockAndUpdate(pos, state);
			}
			else if(w.isEmptyBlock(pos.below()) && random.nextInt(5)==0 && FPConfig.SERVER.glowmelow_drop.get())
			{
				FallingBlockEntity entityfallingblock = new FallingBlockEntity(w, pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D, w.getBlockState(pos));
				entityfallingblock.setHurtsEntities(1F, 20);
				w.addFreshEntity(entityfallingblock);
			}
			else if(random.nextInt(10)==0)
			{
				w.removeBlock(pos, false);
			}
		}
	}

	
	@Override
	public void tick(BlockState state, ServerLevel worldIn, BlockPos pos, Random random)
	{
//		super.tick(state, worldIn, pos, random);
		if (!worldIn.isClientSide)
		{
			checkFall(worldIn, pos, state);
		}
	}
	
	private void checkFall(Level w, BlockPos pos, BlockState state)
	{
		if(w.getBlockState(pos.above()).getMaterial() != Material.WOOD)
		{
			if (isFree(w.getBlockState(pos.below())) && pos.getY() >= 0) 
			{
				FallingBlockEntity entityfallingblock = new FallingBlockEntity(w, pos.getX() + 0.5D, pos.getY(), pos.getZ() + 0.5D, w.getBlockState(pos));
				entityfallingblock.setHurtsEntities(1F, 20);
				this.falling(entityfallingblock);
				w.addFreshEntity(entityfallingblock);
			}
		}
	}
	
//	@Override
//	public boolean isOpaqueCube(IBlockState state)
//	{
//		return false;
//	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(AGE_0_6);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context) 
	{
		if(context.getItemInHand().getItem() == FoodItems.glowmelo)
		{
			return this.defaultBlockState().setValue(BlockGlowmelo.AGE_0_6, 6);
		}
		return super.getStateForPlacement(context);
	}
}
