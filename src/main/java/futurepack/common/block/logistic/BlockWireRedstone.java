package futurepack.common.block.logistic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.annotation.Nullable;

import futurepack.api.FacingUtil;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.logistic.RedstoneSystem.EnumRedstone;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;

public class BlockWireRedstone extends BlockWireBase<TileEntityWireRedstone>
{
	public static final BooleanProperty REDSTONE = BooleanProperty.create("redstone");
	public static final BooleanProperty REDSTONE_DUST = BooleanProperty.create("redstone_dust");
	
	public BlockWireRedstone(Block.Properties props) 
	{
		super(props);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
		tooltip.add(new TranslatableComponent("tooltip.futurepack.block.conduct.redstone"));
	}
	
	@Override
	public boolean canConnectRedstone(BlockState state, BlockGetter world, BlockPos pos, Direction side)
	{
		return true;
	}
	
	
	
	@Override
	public int getSignal(BlockState state, BlockGetter w, BlockPos pos, Direction face)
	{
		return RedstoneSystem.getWeakPower(state, w, pos, face);
	}
	
	
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block block, BlockPos blockPos, boolean isMoving)
	{
		RedstoneSystem.neighborChanged(this, w, pos);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onPlace(BlockState state, Level w, BlockPos jkl, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, w, jkl, oldState, isMoving);
		RedstoneSystem.onBlockAdded(state, w, jkl, oldState, isMoving);
		this.checkRedstoneRender(state, w, jkl);
	}
	
	private void checkRedstoneRender(BlockState state, Level w, BlockPos pos) {
		
		ArrayList<Direction> neighbours = new ArrayList<>();
		
		for(Direction direction : FacingUtil.VALUES) {
			BlockState neighbor = w.getBlockState(pos.relative(direction));
			if(!(neighbor.getBlock() instanceof BlockWireRedstone) && !(neighbor.getBlock() instanceof BlockPipeRedstone) && neighbor.canRedstoneConnectTo(w, pos.relative(direction), direction.getOpposite())) 
			{
				neighbours.add(direction);
				
				BlockState newState = this.updateShape(state, direction, neighbor, w, pos, pos.relative(direction));
				if(!newState.equals(state))
					w.setBlock(pos, this.updateShape(state, direction, neighbor, w, pos, pos.relative(direction)), 2);
			}
		}
		
		BlockState downState = w.getBlockState(pos.below());
		
		boolean showDust = (neighbours.contains(Direction.NORTH) || neighbours.contains(Direction.EAST) || neighbours.contains(Direction.SOUTH) || neighbours.contains(Direction.WEST)) 
				&& !neighbours.contains(Direction.DOWN) && downState.isRedstoneConductor(w, pos.below()) && !(downState.getBlock() instanceof BlockWireBase);

		if(state.getValue(REDSTONE) != !neighbours.isEmpty() || state.getValue(REDSTONE_DUST) != showDust) {
			w.setBlock(pos, state.setValue(REDSTONE, !neighbours.isEmpty()).setValue(REDSTONE_DUST, showDust), 2);
		}
	}
	
	@Override
	public boolean canConnect(LevelAccessor w, BlockPos pos, BlockPos xyz, BlockState facingState, @Nullable BlockEntity tile, Direction face)
	{
		return RedstoneSystem.canConnectToRedstone(w, pos, xyz, facingState, face) || super.canConnect(w, pos, xyz, facingState, tile, face);
	}
	
	@Override
	public boolean canConnect(LevelAccessor w, BlockPos pos, BlockPos xyz, BlockState state, BlockState facingState, TileEntityWireBase wire, @Nullable BlockEntity tile, Direction face)
	{		
		return RedstoneSystem.canConnectToRedstone(w, pos, xyz, facingState, face) || super.canConnect(w, pos, xyz, state, facingState, wire, tile, face);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos jkl, Random rand)
	{
		super.tick(state,w, jkl,  rand);
		RedstoneSystem.tick(state, w, jkl, rand);	
		this.checkRedstoneRender(state, w, jkl);
	}
	
	@Override
	public boolean isSignalSource(BlockState state)
	{
		return true;
	}

	@Override
	protected int getMaxNeon() 
	{
		return 500;
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return super.getStateForPlacement(context).setValue(REDSTONE, false).setValue(RedstoneSystem.STATE, EnumRedstone.OFF).setValue(REDSTONE_DUST, false);
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(RedstoneSystem.STATE, REDSTONE, REDSTONE_DUST);
	}

	@Override
	public BlockEntityType<TileEntityWireRedstone> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.WIRE_REDSTONE;
	}
}
