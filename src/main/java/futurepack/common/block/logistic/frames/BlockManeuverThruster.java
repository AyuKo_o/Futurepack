package futurepack.common.block.logistic.frames;

import java.util.Random;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;

public class BlockManeuverThruster extends BlockRotateableTile implements IBlockServerOnlyTickingEntity<TileEntitySpaceshipMover>
{
	public static final BooleanProperty powered = BlockStateProperties.POWERED;

	public BlockManeuverThruster(Properties props) 
	{
		super(props);
		registerDefaultState(this.stateDefinition.any().setValue(powered, false));
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(powered);
		super.createBlockStateDefinition(builder);
	}
	
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) 
	{
		super.neighborChanged(state, w, pos, blockIn, fromPos, isMoving);
		boolean bb = w.getBestNeighborSignal(pos) > 0;
		w.setBlock(pos, state.setValue(powered, bb), 3);
	}
	
	@Override
	public void animateTick(BlockState state, Level w, BlockPos pos, Random rand)
	{
		if(state.getValue(powered))
		{
			Direction f = state.getValue(FACING);
			ParticleOptions red = ParticleTypes.SMOKE;
			w.addParticle(red, pos.getX()+w.random.nextFloat(), pos.getY()+w.random.nextFloat(), pos.getZ()+w.random.nextFloat(), f.getStepX()/2D*w.random.nextFloat(),f.getStepY()/2D*w.random.nextFloat(),f.getStepZ()/2D*w.random.nextFloat());
	        w.addParticle(red, pos.getX()+w.random.nextFloat(), pos.getY()+w.random.nextFloat(), pos.getZ()+w.random.nextFloat(), f.getStepX()/2D*w.random.nextFloat(),f.getStepY()/2D*w.random.nextFloat(),f.getStepZ()/2D*w.random.nextFloat());
		
		}
		super.animateTick(state, w, pos, rand);
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return super.getStateForPlacement(context);
	}

	@Override
	public BlockEntityType<TileEntitySpaceshipMover> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.SPACESHIP_MOVER;
	}

}
