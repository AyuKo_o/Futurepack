package futurepack.common.block.logistic.frames;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.spaceships.moving.MovingBlocktUtil;
import futurepack.common.spaceships.moving.MovingSheduledTicks;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.MiniWorld;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.ticks.ScheduledTick;
import net.minecraft.world.ticks.TickPriority;
import net.minecraftforge.registries.ForgeRegistries;

public class TileEntityMovingBlocks extends TileEntityWithMiniWorldBase implements ITileServerTickable, ITileClientTickable
{

	public TileEntityMovingBlocks(BlockEntityType<TileEntityMovingBlocks> type, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
	}
	
	public TileEntityMovingBlocks(BlockPos pos, BlockState state)
	{
		this(FPTileEntitys.MOVING_BLOCKS, pos, state);
	}

	private Vec3i direction;
	private MovingSheduledTicks<Block> pendingBlockTicks = null;
	private MovingSheduledTicks<Fluid> pendingFluidTicks = null;
	private CompoundTag pendingBlock = null, pendingFluid = null;

	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
//		if(level.isClientSide)
//		{
//			this.level.addParticle(ParticleTypes.POOF, true, this.worldPosition.getX() + 0.5,  this.worldPosition.getY() + 0.5,  this.worldPosition.getZ() + 0.5, 0D, 0D, 0D);
//			
//			
//		}
		
		if(!level.isClientSide() && ticks +(maxticks/10) == maxticks)
		{
			FPPacketHandler.sendTileEntityPacketToAllClients(this);
			System.out.println("resending");
		}
		
		ticks--;
//		ticks = 15;
		if(!level.isClientSide)
		{
	//		BlockPos start = w.start.add(w.rotationpoint.xCoord, w.rotationpoint.yCoord, w.rotationpoint.zCoord);
			if(w==null || ticks<=0)
			{			
				level.removeBlock(worldPosition, false);
				
				MovingBlocktUtil.endMoveBlocks(getLevel(), getMiniWorld(), direction, pendingBlockTicks, pendingFluidTicks);
			}
		}
		
	}	
	
	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tickServer(pLevel, pPos, pState);
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		nbt.putByteArray("direction", new byte[] {(byte) direction.getX(), (byte) direction.getY(), (byte) direction.getZ()});
		
		if(pendingBlockTicks!=null)
			nbt.put("pendingBlockTicks", pendingBlockTicks.serializeNBT());
		if(pendingFluidTicks!=null)
			nbt.put("pendingFluidTicks", pendingFluidTicks.serializeNBT());
		
		
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		byte[] d = nbt.getByteArray("direction");
		direction = new Vec3i(d[0], d[1], d[2]);
		
		
		if(level==null)
		{
			pendingBlock =nbt.getCompound("pendingBlockTicks");
			pendingFluid = nbt.getCompound("pendingFluidTicks");
		}
		else
		{
			setPendingTicks(MovingSheduledTicks.load(ForgeRegistries.BLOCKS, nbt.getCompound("pendingBlockTicks"), level::getGameTime), MovingSheduledTicks.load(ForgeRegistries.FLUIDS, nbt.getCompound("pendingFluidTicks"), level::getGameTime));
		}
		super.readDataUnsynced(nbt);
	}

	@Override
	public void setLevel(Level worldIn) 
	{
		super.setLevel(worldIn);
		if(pendingBlock!=null)
		{
			pendingBlockTicks = (MovingSheduledTicks.load(ForgeRegistries.BLOCKS, pendingBlock, level::getGameTime));
		}
		if(pendingFluid!=null)
		{
			pendingFluidTicks = (MovingSheduledTicks.load(ForgeRegistries.FLUIDS, pendingFluid, level::getGameTime));
		}
	}
	
	@Override
	public void writeDataSynced(CompoundTag nbt)
	{
		nbt.putByteArray("direction", new byte[] {(byte) direction.getX(), (byte) direction.getY(), (byte) direction.getZ()});
		super.writeDataSynced(nbt);
	}
	
	@Override
	public void setMiniWorld(MiniWorld w)
	{
		this.w = w;
		w.rotationpoint = Vec3.atLowerCornerOf(this.getBlockPos()).subtract(Vec3.atLowerCornerOf(w.start)).add(0.5, 0, 0.5);
		if(w.face==null)
			w.face = Direction.UP;
	
		maxticks = ticks = Math.max(10, w.depth * w.height * w.width / 500);
	}
	
	public void setDirection(Vec3i dir)
	{
		this.direction =  dir;
		maxticks = ticks = maxticks * ( Math.abs(dir.getX()) + Math.abs(dir.getY()) + Math.abs(dir.getZ()) ) ;
	}
	
	public Vec3i getDirection()
	{
		return direction;
	}

	public void setPendingTicks(MovingSheduledTicks<Block> pendingTicks, MovingSheduledTicks<Fluid> pendingFluidTicks) 
	{
		this.pendingBlockTicks = pendingTicks;
		this.pendingFluidTicks = pendingFluidTicks;
	}
}
