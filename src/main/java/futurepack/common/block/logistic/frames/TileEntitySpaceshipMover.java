package futurepack.common.block.logistic.frames;

import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.spaceships.moving.SpaceShipSelecterUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntitySpaceshipMover extends FPTileEntityBase implements ITileServerTickable
{

	private boolean redstone = true;
	
	public TileEntitySpaceshipMover(BlockEntityType<? extends TileEntitySpaceshipMover> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
	}
	
	public TileEntitySpaceshipMover(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.SPACESHIP_MOVER, pos, state);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt) 
	{
		super.readDataUnsynced(nbt);
		redstone = nbt.getBoolean("redstone");
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt) 
	{
		nbt.putBoolean("redstone", redstone);
		return super.writeDataUnsynced(nbt);
	}
	

	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
		boolean now = level.getBestNeighborSignal(worldPosition)>0;
		if(now && !redstone)
		{
			redstone = now;
			move();
		}
		redstone = now;
	}
	
	public void move()
	{
		BlockPos pos = this.getBlockPos();
		Vec3i direction = this.getBlockState().getValue(BlockRotateableTile.FACING).getOpposite().getNormal();
		
		TileEntityMovingBlocks mb = SpaceShipSelecterUtil.moveSpaceship(getLevel(), pos, direction);
//		mb.maxticks = mb.ticks = 20 * 10;
	}

}
