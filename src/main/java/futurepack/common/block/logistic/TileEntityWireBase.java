package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.FacingUtil;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.tilentity.ITileHologramAble;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.block.FPTileEntityBase;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperHologram;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public abstract class TileEntityWireBase extends FPTileEntityBase implements ITileHologramAble, ITileServerTickable
{
	public NeonWire power = new NeonWire();
	
	boolean[] redpower = new boolean[FacingUtil.VALUES.length];
	private LogisticStorage log;
	
	private BlockState overlay;
	private LazyOptional<INeonEnergyStorage>[] neonOpt;
	private LazyOptional<ILogisticInterface>[] logOpt;
	
	@SuppressWarnings("unchecked")
	public TileEntityWireBase(BlockEntityType<? extends TileEntityWireBase> type, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
		
		neonOpt = new LazyOptional[6];
		logOpt = new LazyOptional[6];
		
		log = new LogisticStorage(this, this::onLogisticChange, getTypes());
		configureLogistic(getLogisticStorage());
	}
	
	protected EnumLogisticType[] getTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE};
	}
	
	protected void configureLogistic(LogisticStorage log)
	{
		log.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		log.removeState(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		log.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
	}
	
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.ENERGIE)
		{
			if(neonOpt[face.get3DDataValue()]!=null)
			{
				neonOpt[face.get3DDataValue()].invalidate();
				neonOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing == null)
			return LazyOptional.empty();
		
		if(capability == CapabilityNeon.cap_NEON)
		{
			return HelperEnergyTransfer.getNeonCap(neonOpt, facing, this::getLogisticStorage, ()->power);
		}
		else if(capability == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
			else
			{
				logOpt[facing.get3DDataValue()] = LazyOptional.of(() -> getLogisticStorage().getInterfaceforSide(facing));
				logOpt[facing.get3DDataValue()].addListener(p -> logOpt[facing.get3DDataValue()] = null);
				return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
			
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(neonOpt);
		HelperEnergyTransfer.invalidateCaps(logOpt);
		super.setRemoved();
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("energy", power.serializeNBT());
		if(overlay!=null)
		{
			nbt.put("hologram", HelperHologram.toNBT(overlay));
		}
		nbt.put("logisticFaces", getLogisticStorage().serializeNBT());
		return nbt;
	}
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
		
		if(nbt.contains("hologram"))
		{
			setHologram(HelperHologram.fromNBT(nbt.getCompound("hologram")));
		}
		else if(nbt.contains("BlockStateContainer"))
		{
			setHologram(HelperHologram.fromNBT(nbt.getCompound("BlockStateContainer")));
		}
		getLogisticStorage().deserializeNBT(nbt.getCompound("logisticFaces"));
	}
  
	private int idleTicks=0, timeOut=0;
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		if(--timeOut>0)
			return;
		
		if( (power.get()>= power.getMax() || (System.currentTimeMillis()%1000)/50 == 0))
		{
			HelperEnergyTransfer.powerLowestBlock(this);
			if(power.get()>= power.getMax())
			{
				idleTicks++;
			}
			else
			{
				idleTicks = 0;
			}
			if(idleTicks>100)
				timeOut = 20;
		}
	}
	
	@Override
	public BlockState getHologram()
	{
		if(overlay!=null && overlay == getBlockState())
		{
			setHologram(null);
		}
		return overlay;
	}
	@Override
	public boolean hasHologram()
	{
		return overlay != null;
	}	
	@Override
	public void setHologram(BlockState state)
	{
		BlockState oldHologram = overlay;
		overlay = state;
		if(level!=null && !level.isClientSide)
		{
			BlockState wire = level.getBlockState(worldPosition);
			if(oldHologram == state)
			{
				wire = wire.setValue(BlockWireBase.hologram, !wire.getValue(BlockWireBase.hologram));
				level.setBlockAndUpdate(worldPosition, wire);
			}
			else
			{
				if(wire.getValue(BlockWireBase.hologram) != hasHologram())
				{
					wire = wire.setValue(BlockWireBase.hologram, hasHologram());
					level.setBlockAndUpdate(worldPosition, wire);
				}
			}
			
		}
	}
	
	@Override
	public AABB getRenderBoundingBox()
	{
		return super.getRenderBoundingBox();
	}
  
//	@Override
//	public boolean hasFastRenderer()
//	{
//		return HelperHologram.hasFastRenderer(this);
//	}
	
	public abstract int getMaxEnergy();
	
	public LogisticStorage getLogisticStorage() 
	{
		return log;
	}

	public class NeonWire extends CapabilityNeon
	{
		public NeonWire()
		{
			super(1000, EnumEnergyMode.WIRE);
		}
		
		@Override
		public int getMax() 
		{
			return getMaxEnergy();
		}
		
		@Override
		public int add(int added)
		{
			int d =  super.add(added);
			if(super.get() > 0)
			{
				HelperEnergyTransfer.powerLowestBlock(TileEntityWireBase.this);
			}
			return d;
		}
		
		@Override
		public boolean canTransferTo(INeonEnergyStorage other)
		{
			if(other.getType() == EnumEnergyMode.WIRE)
				return true;
			return super.canTransferTo(other);
		}
	}
}
