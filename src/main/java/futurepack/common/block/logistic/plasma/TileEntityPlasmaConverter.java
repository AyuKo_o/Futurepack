package futurepack.common.block.logistic.plasma;

import java.util.function.IntFunction;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.fluids.FPFluids;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;

public class TileEntityPlasmaConverter extends TileEntityPlasmaTransporter implements ITilePropertyStorage
{
	// 4rf -> 1 ne
	// 1000 ne -> 1 plasma
	
	private CapabilityNeon neon_storage;
	private RfStorage rf_storage;
	private LazyOptional<INeonEnergyStorage> optNeon;
	private LazyOptional<IEnergyStorage> optRf;
	
	private int tier, progress;
	
	protected boolean isGettingFatserOverTime = true;
	private IntFunction<Integer> calcNeededProgressFromTier;
	
	
	public TileEntityPlasmaConverter(BlockEntityType<? extends TileEntityPlasmaConverter> tileEntityTypeIn, int maxTransfer, BlockPos pos, BlockState state) 
	{
		super(tileEntityTypeIn, maxTransfer, pos, state);
		neon_storage = new CapabilityNeon(maxTransfer*1000 * 10, EnumEnergyMode.USE);
		rf_storage = new RfStorage(maxTransfer*4000 * 10);
		setProgressCalculationFunction(tier ->  200 - tier*10);
	}
	
	public static TileEntityPlasmaConverter converterT1(BlockPos pos, BlockState state)
	{
		return new TileEntityPlasmaConverter(FPTileEntitys.PLASMA_CONVRTER_T1, (int) EnumPlasmaTiers.Tier1.getPlasmaTranferPipe(), pos, state);
	}
	
	public static TileEntityPlasmaConverter converterT0(BlockPos pos, BlockState state)
	{
		TileEntityPlasmaConverter te = new TileEntityPlasmaConverter(FPTileEntitys.PLASMA_CONVRTER_T0, (int) EnumPlasmaTiers.Tier0.getPlasmaTranferPipe(), pos, state);
		te.isGettingFatserOverTime = false;
		te.setProgressCalculationFunction(tier -> 500);
		return te;
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(optNeon, optRf);
		super.setRemoved();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(optNeon!=null)
				return (LazyOptional<T>) optNeon;
			else
			{
				optNeon = LazyOptional.of(() -> neon_storage);
				optNeon.addListener(p -> optNeon = null);
				return (LazyOptional<T>) optNeon;
			}
		}
		if(capability == CapabilityEnergy.ENERGY)
		{
			if(optRf!=null)
				return (LazyOptional<T>) optRf;
			else
			{
				optRf = LazyOptional.of(() -> rf_storage);
				optRf.addListener(p -> optRf = null);
				return (LazyOptional<T>) optRf;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt) 
	{
		nbt.put("neon", neon_storage.serializeNBT());
		nbt.put("rf", rf_storage.serializeNBT());
		nbt.putInt("progress", progress);
		nbt.putInt("tier", tier);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt) 
	{
		neon_storage.deserializeNBT(nbt.getCompound("neon"));
		rf_storage.deserializeNBT(nbt.getCompound("rf"));
		progress = nbt.getInt("progress");
		tier = nbt.getInt("tier");
		super.readDataUnsynced(nbt);
	}
	
	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
		super.tickServer(level, worldPosition, pState);
		
		int e = neon_storage.get() + rf_storage.getEnergyStored() / 4;
		boolean space = this.getTank().getSpace() > 0;		
		if(e >= 1000 && space)
		{
			progress++;
			if(progress >= getMaxProgress())
			{
				progress = 0;
				maxProgress = null;
				if(tier < 1)
					tier = 1;
				
				boolean upgrade = true;
				for(int i=0;i<tier;i++)
				{
					if(!convert())
					{
						upgrade = false;
						break;
					}
				}
				if(upgrade && isGettingFatserOverTime)
				{
					tier++;
				}
				else
				{
					tier--;
				}
			}
		}
	}
	
	private Integer maxProgress = null;
	
	protected int getMaxProgress()
	{
		maxProgress = maxProgress==null?calcNeededProgressFromTier.apply(tier) : maxProgress;
		return maxProgress;
	}

	private boolean convert()
	{
		int e = neon_storage.get() + rf_storage.getEnergyStored() / 4;
		boolean space = (this.getTank().getSpace()) > 0;		
		if(e >= 1000 && space)
		{
			if(rf_storage.getEnergyStored() >= 4000)
			{
				if(getTank().fill(new FluidStack(FPFluids.plasmaStill, 1), FluidAction.EXECUTE) > 0)
				{
					rf_storage.extractEnergy(4000, false);
				}
				return true;
			}
			else if(neon_storage.get() >= 1000)
			{
				if(getTank().fill(new FluidStack(FPFluids.plasmaStill, 1), FluidAction.EXECUTE) > 0)
				{
					neon_storage.use(1000);
				}
				return true;
			}
			else
			{
				if(getTank().fill(new FluidStack(FPFluids.plasmaStill, 1), FluidAction.EXECUTE) > 0)
				{
					int rf = rf_storage.getEnergyStored();
					int ne = 1000 - rf / 4;
					rf_storage.extractEnergy(rf, false);
					neon_storage.use(ne);
				}
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	@Override
	public int getProperty(int id) 
	{
		switch (id) 
		{
		case 0:
			return (short) getTank().getFluidAmount();
		case 1:
			return neon_storage.get();
		case 2:
			return rf_storage.getEnergyStored();
		case 3:
			return tier;
		case 4:
			return progress;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value) 
	{
		switch (id)
		{
		case 0:
			getTank().setFluid(new FluidStack(FPFluids.plasmaStill, value));
			break;
		case 1:
			neon_storage.set(value);
			break;
		case 2:
			rf_storage.setEnergy(value);
			break;
		case 3:
			tier = value;
			break;
		case 4:
			progress = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 5;
	}
	
	public INeonEnergyStorage getNeon() 
	{
		return neon_storage;
	}

	public int getRf() 
	{
		return rf_storage.getEnergyStored();
	}

	public int getMaxRf() 
	{
		return rf_storage.getMaxEnergyStored();
	}

	public int getPlasma() 
	{
		return (int) getTank().getFluidAmount();
	}
	
	protected void setProgressCalculationFunction(IntFunction<Integer> tier2maxProgress)
	{
		this.calcNeededProgressFromTier = tier2maxProgress;
	}
}
