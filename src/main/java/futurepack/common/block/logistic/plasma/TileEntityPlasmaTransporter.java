package futurepack.common.block.logistic.plasma;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.logistic.TileEntityFluidTank;
import futurepack.common.fluids.FPFluids;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.ticks.TickPriority;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;

/**
 * Can transfer plasma in one direction
 * 
 * @author MCenderdragon
 */
public class TileEntityPlasmaTransporter extends TileEntityFluidTank
{
	private static final int MAX_RANGE = 50; //
	
	private Boolean noPlasma = null;

	public TileEntityPlasmaTransporter(BlockEntityType<? extends TileEntityPlasmaTransporter> tileEntityTypeIn, int maxTransfer, BlockPos pos, BlockState state)
	{
		super(tileEntityTypeIn, maxTransfer, pos, state);
		getTank().setValidator(s -> s.getFluid() == FPFluids.plasmaStill);
		canTransferHorizontal = false;
		canTransferVertical = false;
	}

	public static TileEntityPlasmaTransporter pipeT1(BlockPos pos, BlockState state)
	{
		return new TileEntityPlasmaTransporter(FPTileEntitys.PLASMA_PIPE_T1, (int) EnumPlasmaTiers.Tier1.getPlasmaTranferPipe(), pos, state);
	}
	
	public static TileEntityPlasmaTransporter pipeT0(BlockPos pos, BlockState state)
	{
		return new TileEntityPlasmaTransporter(FPTileEntitys.PLASMA_PIPE_T0, (int) EnumPlasmaTiers.Tier0.getPlasmaTranferPipe(), pos, state);
	}

	private LazyOptional<IFluidHandler> otherPlasma;

	private void transferTo(IFluidHandler storage)
	{
		FluidUtil.tryFluidTransfer(storage, getTank(), getTank().getCapacity(), true);
	}

	public Direction getOutput()
	{
		return getBlockState().getValue(BlockStateProperties.FACING);
	}

	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
		super.tickServer(level, worldPosition, pState);
		
		int r = 1;
		while (otherPlasma == null && r < MAX_RANGE)
		{
			Direction dir = getOutput();
			BlockEntity tile = level.getBlockEntity(worldPosition.relative(dir, r));
			if (tile != null)
			{
				otherPlasma = tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, dir.getOpposite());
				otherPlasma.addListener(p -> otherPlasma = null);
			}
			r++;
		}
		if (otherPlasma != null)
		{
			otherPlasma.ifPresent(this::transferTo);
		}
		
		Boolean empty = this.getTank().getFluidAmount() == 0;
		if(empty != this.noPlasma)
		{
			this.level.scheduleTick(worldPosition, pState.getBlock(), 1, TickPriority.LOW);
			this.noPlasma = empty;
		}
	}
}
