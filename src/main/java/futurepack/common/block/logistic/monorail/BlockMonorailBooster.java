package futurepack.common.block.logistic.monorail;

import futurepack.common.entity.monocart.EntityMonocartBase;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;

public class BlockMonorailBooster extends BlockMonorailBasic
{
	public static final BooleanProperty powered = BooleanProperty.create("powered");
	
	public BlockMonorailBooster(Block.Properties props)
	{
		super(props);
		this.registerDefaultState(this.defaultBlockState().setValue(powered, false));
	}
	
	@Override
	public boolean isBendable()
	{
		return false;
	}

	@Override
	public void onMonocartPasses(Level w, BlockPos pos, BlockState state, EntityMonocartBase cart)
	{
		boolean rd = w.hasNeighborSignal(pos);
		cart.setHighSpeed(rd);
		super.onMonocartPasses(w, pos, state, cart);
	}

	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block neighborBlock, BlockPos nBlock, boolean isMoving)
	{
		boolean rd = w.hasNeighborSignal(pos);
		state = state.setValue(powered, rd);
		w.setBlockAndUpdate(pos, state);
		super.neighborChanged(state, w, pos, neighborBlock, nBlock, isMoving);
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(powered);
		super.createBlockStateDefinition(builder);
	}
}
