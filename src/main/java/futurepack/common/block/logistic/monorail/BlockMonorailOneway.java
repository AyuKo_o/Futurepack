package futurepack.common.block.logistic.monorail;

import futurepack.api.FacingUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DirectionProperty;


public class BlockMonorailOneway extends BlockMonorailBasic
{
	public static final BooleanProperty direction = BooleanProperty.create("direction");
	public static final DirectionProperty horizontal = BlockStateProperties.HORIZONTAL_FACING;
	
	public BlockMonorailOneway(Block.Properties props)
	{
		super(props);
	}
	
	@Override
	public boolean isBendable()
	{
		return false;
	}
	
	@Override
	public boolean canMonocartPass(Level w, BlockPos pos, BlockState state, BlockPos from, BlockState fromState)
	{
		EnumMonorailStates shape = state.getValue(getPropertie(this));
		boolean direction = state.getValue(BlockMonorailOneway.direction);
		
		Direction face = FacingUtil.getSide(from.getX(), pos.getY(), from.getZ(), pos.getX(), pos.getY(), pos.getZ()) ; //same Y so it work even if diagonal
		
		if(shape==EnumMonorailStates.NORTH_SOUTH)
		{
			if(direction && face== Direction.SOUTH)
				return false;
			else if(!direction && face== Direction.NORTH)
				return false;	
		}
		else if(shape==EnumMonorailStates.EAST_WEST)
		{
			if(direction && face== Direction.WEST)
				return false;
			else if(!direction && face== Direction.EAST)
				return false;	
		}
		else if(shape==EnumMonorailStates.ASCENDING_NORTH)
		{
			if(direction && face== Direction.SOUTH)
				return false;
			else if(!direction && face== Direction.NORTH)
				return false;	
		}
		else if(shape==EnumMonorailStates.ASCENDING_SOUTH)
		{
			if(direction && face== Direction.NORTH)
				return false;
			else if(!direction && face== Direction.SOUTH)
				return false;	
		}
		else if(shape==EnumMonorailStates.ASCENDING_WEST)
		{
			if(direction && face== Direction.EAST)
				return false;
			else if(!direction && face== Direction.WEST)
				return false;	
		}
		else if(shape==EnumMonorailStates.ASCENDING_EAST)
		{
			if(direction && face== Direction.WEST)
				return false;
			else if(!direction && face== Direction.EAST)
				return false;	
		}	
		
		boolean b =  super.canMonocartPass(w, pos, state, from, fromState);
		if(!b)
			return false;
		
		return b;
	}
	
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block neighborBlock, BlockPos nBlock, boolean isMoving)
	{
		EnumMonorailStates shape = state.getValue(getPropertie(this));
		
		boolean now = computeDirection(state.getValue(horizontal), shape);
		if(w.hasNeighborSignal(pos))
			now = !now;
		state = state.setValue(direction, now);
		w.setBlockAndUpdate(pos, state);	
		super.neighborChanged(state, w, pos, neighborBlock, nBlock, isMoving);
	}
	
	private boolean computeDirection(Direction face, EnumMonorailStates shape)
	{
		boolean dir = true;
		
		if(shape==EnumMonorailStates.NORTH_SOUTH)
		{
			if(face== Direction.SOUTH)
				dir = false;
		}
		else if(shape==EnumMonorailStates.EAST_WEST)
		{
			if(face== Direction.WEST)
				dir = false;
		}
		else if(shape==EnumMonorailStates.ASCENDING_NORTH)
		{
			if(face== Direction.SOUTH)
				dir = false;	
		}
		else if(shape==EnumMonorailStates.ASCENDING_SOUTH)
		{
			if(face== Direction.NORTH)
				dir = false;		
		}
		else if(shape==EnumMonorailStates.ASCENDING_WEST)
		{
			if(face== Direction.EAST)
				dir = false;	
		}
		else if(shape==EnumMonorailStates.ASCENDING_EAST)
		{
			if(face== Direction.WEST)
				dir = false;
		}	
		
		
		return dir;
	}
	
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		BlockState state = super.getStateForPlacement(context);
		Direction face = context.getHorizontalDirection();
		state = state.setValue(horizontal, face);
		
		EnumMonorailStates shape = state.getValue(getPropertie(this));	
		boolean dir = computeDirection(face, shape);
		state = state.setValue(direction, dir);
		
		return state;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		builder.add(direction, horizontal);
		super.createBlockStateDefinition(builder);
	}


}
