package futurepack.common.block.logistic.monorail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Stream;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import futurepack.api.FacingUtil;
import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.api.interfaces.tilentity.ITileRenameable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.inventory.TileEntityInventoryBase;
import futurepack.common.block.logistic.CombinedWrapper;
import futurepack.common.entity.monocart.EntityMonocart;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperItemFilter;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.Container;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

public class TileEntityMonorailStation extends TileEntityInventoryBase implements WorldlyContainer, ITileRenameable
{
	private String name = null;
	
	public boolean blacklist_insert = false, blacklist_extract = false;
	
	public TileEntityMonorailStation(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.MONORAIL_STATION, pos, state);
	}
	
	/**
	 * 0..4 insert
	 * 5...9 extract
	 */
	@Override
	protected int getInventorySize()
	{
		return 10;
	}

	@Override
	public int[] getSlotsForFace(Direction side)
	{
		return new int[]{};
	}

	@Override
	public boolean canPlaceItemThroughFace(int index, ItemStack itemStackIn, Direction direction)
	{
		return false;
	}

	@Override
	public boolean canTakeItemThroughFace(int index, ItemStack stack, Direction direction)
	{
		return false;
	}
	
	@Override
	public void onLoad()
	{
		super.onLoad();
		if(name==null)
			name = "Monorail Station " +Arrays.toString(new int[]{worldPosition.getX(),worldPosition.getY(),worldPosition.getZ()});
	}
	
	public void onNeighbourChange()
	{
		if(item_opt!=null)
		{
			item_opt.invalidate();
			item_opt = null;
		}
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		if(name!=null)
			nbt.putString("name", name);
		nbt.putBoolean("blacklist_insert", blacklist_insert);
		nbt.putBoolean("blacklist_extract", blacklist_extract);
		return super.writeDataUnsynced(nbt);
	}

	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		if(nbt.contains("name"))
			name = nbt.getString("name");
		blacklist_insert = nbt.getBoolean("blacklist_insert");
		blacklist_extract = nbt.getBoolean("blacklist_extract");
	}

	public void progressMonoCart(EntityMonocart cart)
	{
		LazyOptional<IItemHandler> opthandler = getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, Direction.UP);
		LazyOptional<IItemHandler> optcartHandler = cart.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, Direction.DOWN);
		
		if(optcartHandler.isPresent() && optcartHandler.isPresent())
		{
			IItemHandler cartHandler = optcartHandler.orElseThrow(NullPointerException::new);
			IItemHandler handler = opthandler.orElseThrow(NullPointerException::new);
			
			//from cart in inv
			final IItemFilter[] extracts = getFilters(new ItemStack[]{items.get(5), items.get(6), items.get(7), items.get(8), items.get(9)});		
			if(blacklist_extract)
			{
				Stream<Predicate<ItemStack>> stream = Arrays.stream(extracts).filter(Predicates.notNull()).map(s -> Predicates.not(s::test));
				Predicate<ItemStack>[] not = stream.toArray(Predicate[]::new);
				
				Predicate<ItemStack> pred;
				if(not.length==0)
					pred = Predicates.alwaysTrue();
				else
					pred = Predicates.and(not);			
				
				Integer[] slotsO = getExtractableSlot(cartHandler, pred);
				tryTransfer(slotsO, cartHandler, handler, Integer.MAX_VALUE, it -> 
				{
					for(IItemFilter filter : extracts)
						if(filter!=null)
						{
						filter.amountTransfered(it);
						}
					});
			}
			else
			{
				for(int i=0;i<extracts.length;i++)
				{
					IItemFilter extract = extracts[i];
					if(extract==null)
						continue;
	
					Integer[] slotsO = getExtractableSlot(cartHandler, extract::test);	
					int max = items.get(5+i).getCount();
						
					if(max >= 65)
					{
						max = Short.MAX_VALUE;  //infinite = more then possible
					}
						
					tryTransfer(slotsO, cartHandler, handler, max, extract::amountTransfered);
				}	
			}
			
			//from inv in cart
			IItemFilter[] inserts = getFilters(new ItemStack[]{items.get(0), items.get(1), items.get(2), items.get(3), items.get(4)});
			if(blacklist_insert)
			{
				Stream<Predicate<ItemStack>> stream = Arrays.stream(inserts).filter(Predicates.notNull()).map(s -> Predicates.not(s::test));//TODO: test if monoril station inserting into cart works are planed;
				Predicate<ItemStack>[] not = stream.toArray(Predicate[]::new);																				//before stream of extracts -> no stream of inserts; I think it must be insert but I could be wrong so we need to test this
				Predicate<ItemStack> pred;
				if(not.length==0)
					pred = Predicates.alwaysTrue();
				else
					pred = Predicates.and(not);
				
				Integer[] slotsI = getExtractableSlot(cartHandler, pred);
				tryTransfer(slotsI, handler, cartHandler, Integer.MAX_VALUE, it -> 
				{
					for(IItemFilter filter : inserts)
					{
						if(filter!=null)
						{
							filter.amountTransfered(it);
						}
					}
						
				});
			}
			else
			{
				for(int i=0;i<inserts.length;i++)
				{
					IItemFilter insert = inserts[i];
					if(insert==null)
						continue;
						
					Integer[] slotsI = getExtractableSlot(handler, insert::test);
					Integer[] allreayInside = getExtractableSlot(cartHandler, insert::test);
					int max = items.get(i).getCount();
						
					if(max >= 65)
					{
						max = Short.MAX_VALUE;  //infinite = more then possible
					}
						
					for(int s : allreayInside)
					{
						ItemStack item = cartHandler.getStackInSlot(s);//the station should only insert items until the defined stacksize is reached
						if(!item.isEmpty())
						{
							max-=item.getCount();
						}
					}
						
					tryTransfer(slotsI, handler, cartHandler, max, insert::amountTransfered);
				}
			}
		}
	}
	
	public static IItemFilter[] getFilters(ItemStack...it)
	{
		//TODO: maybe store the filters
		IItemFilter[] filters = new IItemFilter[it.length];
		for(int i=0;i<it.length;i++)
		{
			if(!it[i].isEmpty())
				filters[i] = HelperItemFilter.getFilter(it[i]);
		}
		return filters;
	}
	
	public static int getFirstSlot(Container inv, ItemStack it)
	{
		int[] ints = null;
		if(inv instanceof WorldlyContainer)
		{
			ints = ((WorldlyContainer) inv).getSlotsForFace(Direction.UP);
		}
		if(ints==null)
		{
			ints = new int[inv.getContainerSize()];
			for(int i=0;i<ints.length;i++)
			{
				ints[i]=i;
			}
		}
		
		int empty = -1;
		for(int i=0;i<ints.length;i++)
		{
			ItemStack in = inv.getItem(ints[i]);
			if(in.isEmpty() && empty==-1)
			{
				empty=i;
			}
			else if(in.sameItem(it) && ItemStack.tagMatches(it, in) && in.getCount() < in.getMaxStackSize())
			{
				return i;
			}
		}
		
		return empty;
	}

	public static Integer[] getExtractableSlot(IItemHandler cart, Predicate<ItemStack> valid)
	{
		List<Integer> slots = new ArrayList(cart.getSlots());
		for(int i=0;i<cart.getSlots();i++)
		{
			if(valid.test(cart.getStackInSlot(i)))
			{
				slots.add(i);
			}
		}
		
		return slots.toArray(new Integer[slots.size()]);
	}
	
	public static void tryTransfer(Integer[] slots, IItemHandler extract, IItemHandler insert, int max, Consumer<ItemStack> transferedCallback)
	{
		for(int s : slots)
		{				
			ItemStack it = extract.extractItem(s, max, false);
			if(it==null || it.isEmpty())
				continue;
			max-=it.getCount();
			ItemStack is = ItemHandlerHelper.insertItem(insert, it, false);
			ItemStack tranfered = HelperItemFilter.getTranferedItem(it, is);
			if(!tranfered.isEmpty())
			{
				transferedCallback.accept(tranfered);
			}
			if(!is.isEmpty())
			{
				max+=is.getCount();
				extract.insertItem(s, is, false);
			}
			if(max<=0)
				break;
		}
	}
	
//	public boolean hasCapability(Capability<?> capability, EnumFacing facing)
//	{
//		if(facing==null)
//			return false;
//		
//		if(facing == EnumFacing.UP && capability==CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
//		{
//			return true;
//		}
//		return super.hasCapability(capability, facing);
//	}
	
	private LazyOptional<IItemHandler> item_opt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if((facing == Direction.UP || facing == null) && capability==CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(item_opt != null)
			{
				return (LazyOptional<T>) item_opt;
			}
			else
			{	
				ArrayList<IItemHandler> handler = new ArrayList<IItemHandler>();
				for(Direction sides : FacingUtil.VALUES)
				{
					if(sides!= Direction.UP)
					{
						BlockPos xyz = this.worldPosition.relative(sides);
						BlockEntity tile = level.getBlockEntity(xyz);
						IItemHandler ha = HelperInventory.getHandler(tile, sides.getOpposite());
						if(ha!=null)
						{
							handler.add(ha);
						}
					}
				}
				item_opt = LazyOptional.of( () -> new CombinedWrapper(handler.toArray(new IItemHandler[handler.size()])));
				item_opt.addListener(p -> item_opt = null);
				return (LazyOptional<T>) item_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(item_opt);
		super.setRemoved();
	}
	
//	@Override
//	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
//	{
//		return oldState.getBlock() != newSate.getBlock();
//	}

	@Override
	public boolean hasCustomName()
	{
		return name!=null;
	}

	@Override
	public Component getName()
	{
		return new TextComponent(name);
	}
	
	@Override
	public void setName(String name)
	{
		this.name = name;
		this.setChanged();
	}
	
	@Override
	public String getGUITitle() {
		return "gui.futurepack.monorail_station.title";
	}
}
