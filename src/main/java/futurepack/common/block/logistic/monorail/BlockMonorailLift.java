package futurepack.common.block.logistic.monorail;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockMonorailLift extends BlockMonorailBasic 
{
	private final VoxelShape shape_rail_and_collum;
	private final VoxelShape shape_collum;
	
	public BlockMonorailLift(Properties props) 
	{
		super(props);
		shape_collum = Shapes.or(Shapes.or(Block.box(0, 0, 0, 2, 16, 2), Block.box(14, 0, 0, 16, 16, 2)), Shapes.or(Block.box(14, 0, 14, 16, 16, 16), Block.box(0, 0, 14, 2, 16, 16)));
		shape_rail_and_collum = Shapes.or(shape_collum, Shapes.box(0, 0, 0, 1F, 0.0625F, 1F));
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		if(state.getValue(getPropertie(this)) == EnumMonorailStates.UP_DOWN)
			return shape_collum;
		else
			return shape_rail_and_collum;
	}
	
	@Override
	public boolean isBendable()
	{
		return false;
	}
	
	@Override
	public boolean isLift()
	{
		return true;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public boolean canSurvive(BlockState state, LevelReader worldIn, BlockPos pos)
	{
		EnumMonorailStates monorail = state.getValue(getPropertie(this));
		if(monorail == EnumMonorailStates.UP_DOWN || monorail == EnumMonorailStates.NORTH_EAST_SOUTH_WEST_DOWN)
		{
			return true;
		}
		return super.canSurvive(state, worldIn, pos);
	}
	
	
}
