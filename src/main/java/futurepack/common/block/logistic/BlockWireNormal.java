package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockWireNormal extends BlockWireBase<TileEntityWireNormal> 
{

	public BlockWireNormal(Properties props) 
	{
		super(props);
	}

	@Override
	protected int getMaxNeon() 
	{
		return 500;
	}

	@Override
	public BlockEntityType<TileEntityWireNormal> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.WIRE_NORMAL;
	}

}
