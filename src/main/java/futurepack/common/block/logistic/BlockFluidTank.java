package futurepack.common.block.logistic;

import java.util.List;

import futurepack.api.interfaces.IBlockTickableEntity;
import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraftforge.fluids.FluidUtil;

public class BlockFluidTank extends BlockHoldingTile implements IBlockTickableEntity
{	
	public static final EnumProperty<EnumConnection> up = EnumProperty.create("zcon",EnumConnection.class);
	
	public final int tank_capacity;
	
	public BlockFluidTank(Block.Properties props, int capacity)
	{
		super(props);
		this.tank_capacity = capacity;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(up);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, LevelAccessor w, BlockPos pos, BlockPos facingPos) 
	{
		BlockState bs =  super.updateShape(stateIn, facing, facingState, w, pos, facingPos);
		bs = bs.setValue(up, EnumConnection.get(pos, w, this));
		return bs;
	}
	
	public static boolean check(Direction face, BlockPos pos, LevelReader w, Block bl)
	{
		return w.getBlockState(pos.relative(face)).getBlock() == bl;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean skipRendering(BlockState state, BlockState adjacentBlockState, Direction side)
	{
		if(adjacentBlockState.getBlock() == this)
		{
			return true;
		}
		return super.skipRendering(state, adjacentBlockState, side);
	}
	
	public static enum EnumConnection implements StringRepresentable
	{
		none,up,down,both;

		@Override
		public String getSerializedName()
		{
			return name().toLowerCase();
		}
		
		public static EnumConnection get(BlockPos pos, LevelReader w, Block bl)
		{
			boolean flag1 = w.getBlockState(pos.above()).getBlock()==bl;
			boolean flag2 = w.getBlockState(pos.below()).getBlock()==bl;
			if(flag1&&flag2)
				return both;
			else if(flag1)
				return down;
			else if(flag2)
				return up;
			
			return none;
		}
	}


	@Override
	public boolean hasAnalogOutputSignal(BlockState state)
	{
		return true;
	}
	
	@Override
	public int getAnalogOutputSignal(BlockState blockState, Level w, BlockPos pos)
	{
		TileEntityFluidTank tank = (TileEntityFluidTank) w.getBlockEntity(pos);
		if(!tank.getTankInfo().isEmpty())
		{
			return (15 * tank.getTankInfo().getFluidStack().getAmount()) / tank.getTankInfo().getCapacity();
		}
		return super.getAnalogOutputSignal(blockState, w, pos);
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state) 
	{
		return new TileEntityFluidTank(tank_capacity, pos, state);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player player, InteractionHand handIn, BlockHitResult hit) 
	{
		if(FluidUtil.interactWithFluidHandler(player, handIn, worldIn, pos, hit.getDirection()))
		{
			return InteractionResult.SUCCESS;
		}
		return super.use(state, worldIn, pos, player, handIn, hit);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		tooltip.add(new TranslatableComponent("%s mb", this.tank_capacity));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
	
	@Override
	public boolean propagatesSkylightDown(BlockState state, BlockGetter reader, BlockPos pos) 
	{
		return true;
	}

	@Override
	public BlockEntityType<? extends ITileClientTickable> getClientEnityType(Level pLevel, BlockState pState)
	{
		return null;
	}

	@Override
	public BlockEntityType<? extends ITileServerTickable> getServerEntityType(Level pLevel, BlockState pState)
	{
		return FPTileEntitys.FLUID_TANK;
	}
	
	
//	Because of vanilla chnages we can o longer do glwoign tanks with lava.
//	@Override
//	public int getLightValue(BlockState state, BlockGetter world, BlockPos pos) 
//	{
//		int light = super.getLightValue(state, world, pos);
//		BlockEntity tile = world.getBlockEntity(pos);
//		if(tile==null)
//			return light;
//		light = Math.max(light, tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, Direction.UP).map(handler -> 
//		{
//			Integer li = 0;
//			for(int i=0;i<handler.getTanks();i++)
//			{
//				FluidStack stack = handler.getFluidInTank(i);
//				if(!stack.isEmpty())
//				{
//					li = Math.max(li, stack.getFluid().getAttributes().getLuminosity(stack));
//				}
//			}
//			return li;
//		}).orElse(0));
//		return light;
//	}
}
