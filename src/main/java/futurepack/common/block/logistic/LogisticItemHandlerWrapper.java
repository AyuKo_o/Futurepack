package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticType;
import futurepack.api.capabilities.ILogisticInterface;
import net.minecraftforge.items.IItemHandler;

public class LogisticItemHandlerWrapper extends RestrictedItemHandlerWrapper
{
	public LogisticItemHandlerWrapper(ILogisticInterface log, IItemHandler base)
	{
		super(log.getMode(EnumLogisticType.ITEMS)::canInsert, log.getMode(EnumLogisticType.ITEMS)::canExtract, base);
	}
}
