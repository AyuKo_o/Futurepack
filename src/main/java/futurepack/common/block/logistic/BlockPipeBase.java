package futurepack.common.block.logistic;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.Container;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.items.CapabilityItemHandler;

public abstract class BlockPipeBase<T extends TileEntityPipeBase> extends BlockWaterLoggedHologram implements IBlockBothSidesTickingEntity<T>
{
	public static final VoxelShape BOX_BASE = Shapes.box(0.25, 0.25, 0.25, 0.75, 0.75, 0.75);
	public static final VoxelShape BOX_NORTH = Shapes.box(0.25, 0.25, 0, 0.75, 0.75, 0.25);
	public static final VoxelShape BOX_EAST = Shapes.box(0.75, 0.25, 0.25, 1, 0.75, 0.75);
	public static final VoxelShape BOX_SOUTH = Shapes.box(0.25, 0.25, 0.75, 0.75, 0.75, 1);
	public static final VoxelShape BOX_WEST = Shapes.box(0, 0.25, 0.25, 0.25, 0.75, 0.75);
	public static final VoxelShape BOX_UP = Shapes.box(0.25, 0.75, 0.25, 0.75, 1, 0.75);
	public static final VoxelShape BOX_DOWN = Shapes.box(0.25, 0, 0.25, 0.75, 0.25, 0.75);
	public static final VoxelShape BOX_NORTH_CABLE = Shapes.join(Shapes.box(0.25, 0.25, 0.185, 0.75, 0.75, 0.25), Shapes.box(0.375, 0.375, 0, 0.625, 0.625, 0.185), BooleanOp.OR);
	public static final VoxelShape BOX_SOUTH_CABLE = Shapes.join(Shapes.box(0.25, 0.25, 0.75, 0.75, 0.75, 0.81), Shapes.box(0.375, 0.375, 0.81, 0.625, 0.625, 1), BooleanOp.OR);
	public static final VoxelShape BOX_EAST_CABLE = Shapes.join(Shapes.box(0.75, 0.25, 0.25, 0.81, 0.75, 0.75), Shapes.box(0.81, 0.375, 0.375, 1, 0.625, 0.625), BooleanOp.OR);
	public static final VoxelShape BOX_WEST_CABLE = Shapes.join(Shapes.box(0.185, 0.25, 0.25, 0.25, 0.75, 0.75), Shapes.box(0, 0.375, 0.375, 0.185, 0.625, 0.625), BooleanOp.OR);
	public static final VoxelShape BOX_DOWN_CABLE = Shapes.join(Shapes.box(0.25, 0.185, 0.25, 0.75, 0.25, 0.75), Shapes.box(0.375, 0, 0.375, 0.625, 0.185, 0.625), BooleanOp.OR);
	public static final VoxelShape BOX_UP_CABLE = Shapes.join(Shapes.box(0.25, 0.75, 0.25, 0.75, 0.81, 0.75), Shapes.box(0.375, 0.81, 0.375, 0.625, 1, 0.625), BooleanOp.OR);

	public static final EnumProperty<EnumSide> UP    = EnumProperty.create("up", EnumSide.class);
	public static final EnumProperty<EnumSide> DOWN  = EnumProperty.create("down", EnumSide.class);
	public static final EnumProperty<EnumSide> NORTH = EnumProperty.create("north", EnumSide.class);
	public static final EnumProperty<EnumSide> EAST  = EnumProperty.create("east", EnumSide.class);
	public static final EnumProperty<EnumSide> SOUTH = EnumProperty.create("south", EnumSide.class);
	public static final EnumProperty<EnumSide> WEST  = EnumProperty.create("west", EnumSide.class);

	public static final BooleanProperty HOLOGRAM = BooleanProperty.create("hologram");

	protected BlockPipeBase(Block.Properties props, boolean hasNBTCustomDrops)
	{
		super(props, hasNBTCustomDrops);
		registerDefaultState(this.stateDefinition.any().setValue(HOLOGRAM, false).setValue(UP, EnumSide.OFF).setValue(DOWN, EnumSide.OFF).setValue(NORTH, EnumSide.OFF).setValue(SOUTH, EnumSide.OFF).setValue(EAST, EnumSide.OFF).setValue(WEST, EnumSide.OFF).setValue(WATERLOGGED, false));
	}

	protected BlockPipeBase(Block.Properties props)
	{
		this(props, false);
	}

	private EnumProperty<EnumSide> directionToProperty(Direction d) {
		switch(d) {
			case NORTH: return NORTH;
			case EAST: return EAST;
			case SOUTH: return SOUTH;
			case WEST: return WEST;
			case UP: return UP;
			case DOWN: return DOWN;
			default: return UP;
		}
	}

	private EnumSide getConnectionState(TileEntityPipeBase pipe, Direction face) {
		if(pipe.isSideLocked(face) && !pipe.isIgnoreLockSub(face)) {
			return EnumSide.OFF;
		}
		else if(pipe.isSideLocked(face) && pipe.isIgnoreLockSub(face) && !pipe.getLevel().getBlockState(pipe.getBlockPos().relative(face)).isAir()) {
			return EnumSide.CABLE;
		}

		BlockPos jkl = pipe.getBlockPos().relative(face);

		BlockEntity tile = pipe.getLevel().getBlockEntity(jkl);

		if(tile != null) {
			if(tile instanceof TileEntityPipeBase) {
				TileEntityPipeBase otherPipe = (TileEntityPipeBase) tile;

				if(!otherPipe.isSideLocked(face.getOpposite())) {
					return EnumSide.ON;
				}
				else {
					return otherPipe.isIgnoreLockSub(face.getOpposite()) ? EnumSide.CABLE : EnumSide.OFF;
				}
			}
			else if(tile instanceof Container) {
				return EnumSide.ON;
			}
			else if(tile.getCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, face.getOpposite()).isPresent()) {
				return EnumSide.ON;
			}
		}

		return this.getAdditionalConnections(pipe, tile, face);
	}

	protected EnumSide getAdditionalConnections(TileEntityPipeBase pipe, @Nullable BlockEntity otherTile, Direction face) {
		return EnumSide.OFF;
	}

	public BlockState updatePipePlacement(BlockState state, LevelAccessor worldIn, BlockPos pos, boolean setStateToWorld) {
		if(!worldIn.isClientSide()) {
			if(worldIn.getBlockEntity(pos) instanceof TileEntityPipeBase) {
				TileEntityPipeBase pipe = (TileEntityPipeBase) worldIn.getBlockEntity(pos);

				Map<EnumProperty<EnumSide>, EnumSide> changes = new HashMap<>();

				for(Direction d : Direction.values()) {
					EnumSide connect = this.getConnectionState(pipe, d);
					EnumProperty<EnumSide> prop = this.directionToProperty(d);
					if(state.getValue(prop) != connect) {
						changes.put(prop, connect);
					}
				}

				if(!changes.isEmpty()) {
					for(Entry<EnumProperty<EnumSide>, EnumSide> e : changes.entrySet()) {
						state = state.setValue(e.getKey(), e.getValue());
					}

					if(setStateToWorld)
						worldIn.setBlock(pos, state, 3);
				}

			}
			else {
				//This happens for initial state without te, gets called again after placement...
			}
		}

		return state;
	}

	@Override
	public void neighborChanged(BlockState state, Level worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving) {
		super.neighborChanged(state, worldIn, pos, blockIn, fromPos, isMoving);

		this.updatePipePlacement(state, worldIn, pos, true);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		BlockState state = super.getStateForPlacement(context);

		return this.updatePipePlacement(state, context.getLevel(), context.getClickedPos(), false);
	}

	@Override
	public BlockState updateShape(BlockState state, Direction face, BlockState facingState, LevelAccessor worldIn, BlockPos pos, BlockPos xyz)
	{
		return this.updatePipePlacement(state, worldIn, pos, false);
	}

	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		if(Boolean.TRUE.equals(state.getValue(HOLOGRAM)))
			return RenderShape.INVISIBLE;

		return RenderShape.MODEL;
	}

	protected static HashMap<Integer, VoxelShape> voxelShadeCache = new HashMap<Integer, VoxelShape>();

	protected static Integer getVoxelShadeCacheKey(BlockState state)
	{
		return
				((state.getValue(NORTH) == EnumSide.ON ? 1 : 0) << 0) |
				((state.getValue(EAST) == EnumSide.ON ? 1 : 0) << 1) |
				((state.getValue(SOUTH) == EnumSide.ON ? 1 : 0) << 2) |
				((state.getValue(WEST) == EnumSide.ON ? 1 : 0) << 3) |
				((state.getValue(UP) == EnumSide.ON ? 1 : 0) << 4) |
				((state.getValue(DOWN) == EnumSide.ON ? 1 : 0) << 5) |
				((state.getValue(NORTH) == EnumSide.CABLE ? 1 : 0) << 6) |
				((state.getValue(EAST) == EnumSide.CABLE ? 1 : 0) << 7) |
				((state.getValue(SOUTH) == EnumSide.CABLE ? 1 : 0) << 8) |
				((state.getValue(WEST) == EnumSide.CABLE ? 1 : 0) << 9) |
				((state.getValue(UP) == EnumSide.CABLE ? 1 : 0) << 10) |
				((state.getValue(DOWN) == EnumSide.CABLE ? 1 : 0) << 11)
				;
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter w, BlockPos pos, CollisionContext sel)
	{

		if(Boolean.TRUE.equals(state.getValue(HOLOGRAM))) {
			return super.getShape(state, w, pos, sel);
		}

		if(sel.isHoldingItem(ToolItems.scrench) || sel.isHoldingItem(ToolItems.logisticEditor)) {
			return BOX_BASE;
		}

		Integer cacheKey = getVoxelShadeCacheKey(state);

		VoxelShape shape = voxelShadeCache.get(cacheKey);

		if(shape != null)
		{
			return shape;
		}

		shape = BOX_BASE;

		if(state.getValue(NORTH) == EnumSide.ON) {
			shape = Shapes.join(shape, BOX_NORTH, BooleanOp.OR);
		}

		if(state.getValue(EAST) == EnumSide.ON) {
			shape = Shapes.join(shape, BOX_EAST, BooleanOp.OR);
		}

		if(state.getValue(SOUTH) == EnumSide.ON) {
			shape = Shapes.join(shape, BOX_SOUTH, BooleanOp.OR);
		}

		if(state.getValue(WEST) == EnumSide.ON) {
			shape = Shapes.join(shape, BOX_WEST, BooleanOp.OR);
		}

		if(state.getValue(UP) == EnumSide.ON) {
			shape = Shapes.join(shape, BOX_UP, BooleanOp.OR);
		}

		if(state.getValue(DOWN) == EnumSide.ON) {
			shape = Shapes.join(shape, BOX_DOWN, BooleanOp.OR);
		}


		if(state.getValue(NORTH) == EnumSide.CABLE) {
			shape = Shapes.join(shape, BOX_NORTH_CABLE, BooleanOp.OR);
		}

		if(state.getValue(SOUTH) == EnumSide.CABLE) {
			shape = Shapes.join(shape, BOX_SOUTH_CABLE, BooleanOp.OR);
		}

		if(state.getValue(EAST) == EnumSide.CABLE) {
			shape = Shapes.join(shape, BOX_EAST_CABLE, BooleanOp.OR);
		}

		if(state.getValue(WEST) == EnumSide.CABLE) {
			shape = Shapes.join(shape, BOX_WEST_CABLE, BooleanOp.OR);
		}

		if(state.getValue(DOWN) == EnumSide.CABLE) {
			shape = Shapes.join(shape, BOX_DOWN_CABLE, BooleanOp.OR);
		}

		if(state.getValue(UP) == EnumSide.CABLE) {
			shape = Shapes.join(shape, BOX_UP_CABLE, BooleanOp.OR);
		}

		voxelShadeCache.put(cacheKey, shape);

		return shape;
	}

	@Override
	public void onRemove(BlockState state, Level w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.getBlock() != this)
		{
			if(w.getBlockEntity(pos) instanceof TileEntityPipeBase p)
			{
				p.onBlockDestroy(hasNBTCustomDrops);

				super.onRemove(state, w, pos, newState, isMoving);
			}
		}
	}

	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(pl.getItemInHand(InteractionHand.MAIN_HAND)!=null && pl.getItemInHand(InteractionHand.MAIN_HAND).getItem()==ToolItems.scrench)
		{
			TileEntityPipeBase p = (TileEntityPipeBase) w.getBlockEntity(pos);

			Direction face = hit.getDirection();
			if(p.isSideLocked(face) && p.isIgnoreLockSub(face))
			{
				p.toggelLock(face);
				p.toggelIgnoreLockSub(face);
				this.updatePipePlacement(state, w, pos, true);
				return InteractionResult.SUCCESS;
			}
			if(!p.isSideLocked(face) && !p.isIgnoreLockSub(face))
			{
				p.toggelLock(face);
				this.updatePipePlacement(state, w, pos, true);
				return InteractionResult.SUCCESS;
			}
			if(p.isSideLocked(face) && !p.isIgnoreLockSub(face))
			{
				if(hasSpecial())
				{
					p.toggelIgnoreLockSub(face);
				}
				else
				{
					p.toggelLock(face);
				}
				this.updatePipePlacement(state, w, pos, true);
				return InteractionResult.SUCCESS;
			}

			if(!p.isSideLocked(face) && p.isIgnoreLockSub(face))
			{
				p.toggelIgnoreLockSub(face);
				this.updatePipePlacement(state, w, pos, true);
				return InteractionResult.SUCCESS;
			}
		}
		return InteractionResult.PASS;
	}

	public void onPlace(BlockState state, Level worldIn, BlockPos pos, BlockState oldState, boolean isMoving) {
		this.updatePipePlacement(state, worldIn, pos, true);
	}

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(UP, DOWN, NORTH, EAST, SOUTH, WEST, HOLOGRAM);
	}

	public abstract boolean hasSpecial();

	@Override
	public abstract BlockEntityType<T> getTileEntityType(BlockState pState);

	public enum EnumSide implements StringRepresentable
	{
		OFF,
		ON,
		CABLE;

		@Override
		public String getSerializedName()
		{
			return toString().toLowerCase();
		}

	}
}
