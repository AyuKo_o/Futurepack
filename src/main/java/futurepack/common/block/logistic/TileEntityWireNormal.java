package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityWireNormal extends TileEntityWireBase 
{

	public TileEntityWireNormal(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.WIRE_NORMAL, pos, state);
	}

	@Override
	public int getMaxEnergy() 
	{
		return 500;
	}

}
