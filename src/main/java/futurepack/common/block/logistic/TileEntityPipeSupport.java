package futurepack.common.block.logistic;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityPipeSupport extends TileEntityPipeBase
{
	public SupportWire support = new SupportWire();
	private int AI = 0;
	
	private LazyOptional<ISupportStorage>[] supportOpt;
	private LazyOptional<ILogisticInterface>[] logOpt;
		
	
	@SuppressWarnings("unchecked")
	public TileEntityPipeSupport(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.PIPE_SUPPORT, pos, state);
		supportOpt = new LazyOptional[6];
		logOpt = new LazyOptional[6];
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("support", support.serializeNBT());
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		support.deserializeNBT(nbt.getCompound("support"));
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState state)
	{
		super.tickServer(pLevel, pPos, state);
		
		if(AI>0 != state.getValue(BlockPipeSupport.POWERED))
		{
			level.setBlockAndUpdate(worldPosition, state.setValue(BlockPipeSupport.POWERED, AI>0));
			setChanged();
		}
		if(AI > 0)
			AI--;
		
		if( support.get() >= support.getMax() )
		{
			HelperEnergyTransfer.sendSupportPoints(this);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing == null)
			return LazyOptional.empty();
		
		if(capability == CapabilitySupport.cap_SUPPORT)
		{
			if(supportOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) supportOpt[facing.get3DDataValue()];
			}
			else
			{
				boolean neon = false;
				if(isSideLocked(facing))
				{
					if(isIgnoreLockSub(facing))
					{
						neon = true;
					}
					else
					{
						return LazyOptional.empty(); 
					}
				}
				else
					neon = true;
				
				if(neon)
				{
					supportOpt[facing.get3DDataValue()] = LazyOptional.of(() -> this.support);
					supportOpt[facing.get3DDataValue()].addListener(p -> supportOpt[facing.get3DDataValue()]= null);
					return (LazyOptional<T>) supportOpt[facing.get3DDataValue()];
				}
			}
		}
		else if(capability == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
			else
			{
				 logOpt[facing.get3DDataValue()] =  LazyOptional.of(() -> new LogisticWrapper(facing));
				 logOpt[facing.get3DDataValue()].addListener(p -> logOpt[facing.get3DDataValue()] = null);
				 return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(logOpt);
		HelperEnergyTransfer.invalidateCaps(supportOpt);
		super.setRemoved();
	}
	
	
	public class SupportWire extends CapabilitySupport
	{
		public SupportWire()
		{
			super(1, EnumEnergyMode.WIRE);
		}
		
		@Override
		public int add(int added)
		{
			int a = super.add(added);
			if(super.get() > 0)
			{
				HelperEnergyTransfer.sendSupportPoints(TileEntityPipeSupport.this);
			}
			return a;
		}
		
		@Override
		public EnumEnergyMode getType()
		{
			BlockState state = level.getBlockState(worldPosition);
			if(state.getBlock()== Blocks.AIR)
			{
				//yes this can happen
				return EnumEnergyMode.NONE;
			}
			return EnumEnergyMode.WIRE;
		}
		
		@Override
		public boolean canTransferTo(ISupportStorage other)
		{
			if(other.getType() == EnumEnergyMode.WIRE)
				return true;
			return super.canTransferTo(other);
		}
		
		@Override
		public void support()
		{
			AI = 10;
		}
	}
	
	public class LogisticWrapper extends TileEntityPipeBase.LogisticWrapper
	{

		public LogisticWrapper(Direction face)
		{
			super(face);
		}
		
		@Override
		public EnumLogisticIO getMode(EnumLogisticType mode)
		{
			if(mode == EnumLogisticType.SUPPORT)
			{
				if(isSideLocked(face))
				{
					if(isIgnoreLockSub(face))
					{
						return EnumLogisticIO.INOUT;
					}
					else
					{
						return EnumLogisticIO.NONE;
					}
				}
				else
				{
					return EnumLogisticIO.INOUT;
				}
			}
			return super.getMode(mode);
		}

		@Override
		public boolean setMode(EnumLogisticIO log, EnumLogisticType mode)
		{
			if(mode == EnumLogisticType.SUPPORT)
			{
				if(isSideLocked(face))
				{
					if(log == EnumLogisticIO.INOUT && !isIgnoreLockSub(face))
					{
						toggelIgnoreLockSub(face);
						return true;
					}
					else if(log == EnumLogisticIO.NONE && isIgnoreLockSub(face))
					{
						toggelIgnoreLockSub(face);
						return true;
					}
				}
			}
			return super.setMode(log, mode);
		}

		@Override
		public boolean isTypeSupported(EnumLogisticType type)
		{
			if(type==EnumLogisticType.SUPPORT)
				return true;
			
			return super.isTypeSupported(type);
		}
	}
}
