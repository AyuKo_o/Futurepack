package futurepack.common.block.logistic;

import futurepack.common.FPTileEntitys;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockPipeNormal extends BlockPipeBase<TileEntityPipeNormal> 
{

	protected BlockPipeNormal(Properties props) 
	{
		super(props);
	}

	@Override
	public boolean hasSpecial() 
	{
		return false;
	}

	@Override
	public BlockEntityType<TileEntityPipeNormal> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.PIPE_NORMAL;
	}
}
