package futurepack.common.block.misc;

import java.util.List;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.util.Mth;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockDungeonSpawner extends BlockRotateableTile implements IBlockServerOnlyTickingEntity<TileEntityDungeonSpawner>
{
	public static final DirectionProperty FACING = DirectionalBlock.FACING;
	public static final IntegerProperty rotation = IntegerProperty.create("rot", 0, 3);
	
	
	public static final VoxelShape UP = Shapes.or(Block.box(0, 0, 0, 16, 3, 16), Block.box(6, 0, 6, 10, 12, 10));
	public static final VoxelShape DOWN = Shapes.or(Block.box(0, 13, 0, 16, 16, 16), Block.box(6, 4, 6, 10, 16, 10));
	public static final VoxelShape NORTH = Block.box(0, 0, 11, 16, 16, 16);
	public static final VoxelShape SOUTH = Block.box(0, 0, 0, 16, 16, 5);
	public static final VoxelShape WEST = Block.box(11, 0, 0, 16, 16, 16);
	public static final VoxelShape EAST = Block.box(0, 0, 0, 5, 16, 16);
	
	
	protected BlockDungeonSpawner(Block.Properties props)
	{
		super(props);
//		super(Material.IRON);
//		setCreativeTab(FPMain.tab_deco);
		
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		Direction face = state.getValue(FACING);
		switch(face)
		{
		case DOWN:
			return DOWN;
		case NORTH:
			return NORTH;
		case SOUTH:
			return SOUTH;
		case WEST:
			return WEST;
		case EAST:
			return EAST;
		case UP:
		default:
			return UP;
		}
	}
	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		BlockState state = super.getStateForPlacement(context);
		state = state.setValue(FACING, context.getClickedFace());
		if(context.getClickedFace().getAxis()==Axis.Y)
		{
			int rot = Mth.floor(context.getRotation() * 4.0F / 360.0F + 0.5D) & 3;							
			state = state.setValue(rotation, rot);
		}
		return state;
	}
	
	@Override
	public void onPlace(BlockState state, Level w, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, w, pos, oldState, isMoving);
		if(oldState.getBlock() == this)
		{
			Direction face = state.getValue(FACING);
			if(face.getAxis()!=Axis.Y)
			{
				int h = face.get2DDataValue();
				if(state.getValue(rotation) != h)
				{
					state = state.setValue(rotation, h);
					w.setBlockAndUpdate(pos, state);
				}
				
			}
		}
	}
	
	@Override
    public boolean isSignalSource(BlockState state)
    {
        return true;
    }
	
	@Override
	public int getSignal(BlockState state, BlockGetter w, BlockPos pos, Direction side)
	{
		if(side != state.getValue(FACING))
    		return 0;
    	
    	TileEntityDungeonSpawner tile = (TileEntityDungeonSpawner) w.getBlockEntity(pos);
        return tile.getRedstonePower();
	}
	

	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(rotation);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		tooltip.add(new TranslatableComponent("block.futurepack.dungeon_spawner.tooltip"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
	
	public BlockState rotate(BlockState state, Rotation rot) {	
		
		int remove = 1;
		
		if(rot.equals(Rotation.CLOCKWISE_180)) {
			remove = 2;
		}
		else if(rot.equals(Rotation.COUNTERCLOCKWISE_90)) {
			remove = 3;
		}
		
		int newRotation = state.getValue(rotation)-remove;
		if(newRotation < 0) {
			newRotation += 4;
		}
		
		return state.setValue(FACING, rot.rotate(state.getValue(FACING))).setValue(rotation, newRotation);
	}

	@Override
	public BlockEntityType<TileEntityDungeonSpawner> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.DUNGEON_SPAWNER;
	}
}
