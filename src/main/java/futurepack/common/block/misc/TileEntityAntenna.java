package futurepack.common.block.misc;

import java.io.IOException;
import java.util.function.Consumer;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileAntenne;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPSelectorHelper;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.network.EventWirelessFunk;
import futurepack.common.network.NetworkManager;
import futurepack.common.network.NetworkManager.Wireless;
import futurepack.depend.api.helper.HelperChunks;
import futurepack.world.protection.FPDungeonProtection;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class TileEntityAntenna extends FPTileEntityBase implements ITileAntenne, INetworkUser, ITilePropertyStorage
{
	private int frequenz = 0;
	private int lastRange = -1;

	private final Consumer<EventWirelessFunk> method;
	private boolean checked = false;
	private boolean loaded = false;

	public TileEntityAntenna(BlockPos pos, BlockState state)
	{
		this(FPTileEntitys.ANTENNA, pos, state);
	}

	public TileEntityAntenna(BlockEntityType<? extends TileEntityAntenna> type, BlockPos  pos, BlockState state)
	{
		super(type, pos, state);
		method = this::onFunkEvent;
	}

	@Override
	public void onLoad()
	{
		super.onLoad();
		if(hasLevel() &&!loaded)
		{
			Wireless.registerWirelessTile(method, getLevel());
			loaded = true;
		}
	}

	@Override
	public void setLevel(Level pLevel)
	{
		super.setLevel(pLevel);
		if(!loaded)
		{
			Wireless.registerWirelessTile(method, pLevel);
			loaded = true;
		}
	}

	@Override
	public void onChunkUnloaded()
	{
		Wireless.unregisterWirelessTile(method, getLevel());
		super.onChunkUnloaded();
	}

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		nbt.putInt("frequenz", frequenz);
		return super.writeDataUnsynced(nbt);
	}

	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		frequenz = nbt.getInt("frequenz");
		super.readDataUnsynced(nbt);
	}

	public void onFunkEvent(EventWirelessFunk event)
	{
		if(!level.isClientSide)
		{
			if(this.isRemoved())
			{
				Wireless.unregisterWirelessTile(method, getLevel());
				return;
			}
			if(!level.hasChunkAt(worldPosition))
			{
				Wireless.unregisterWirelessTile(method, getLevel());
				return;
			}

			if(event.canRecive(this))
			{
				if(event.frequenz == this.frequenz)
				{
					processMatchingPacket(event);
				}
			}
		}
		else
		{
			Wireless.unregisterWirelessTile(method, getLevel());
		}
	}


	protected void processMatchingPacket(EventWirelessFunk event)
	{
		float dis = (float) Math.sqrt(event.source.distSqr(getSenderPosition()));
		if(dis < event.range + this.getRange())
		{
			NetworkManager.sendPacketUnthreaded(this, event.objPassed, event.networkDepth);
		}
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	long lastCheck;

	@Override
	public int getRange()
	{
		if(level.isClientSide)
			return lastRange;
		else
		{
			if(lastRange == -1 || System.currentTimeMillis() - lastCheck > lastRange) //dont do this often if lastRange is big
			{
				lastCheck = System.currentTimeMillis();
				if(FPDungeonProtection.isUnbreakable(level, getBlockPos()))
				{
					lastCheck += 10 * 60 * 1000;
				}
				try(HelperChunks.BlockCache cache = new HelperChunks.BlockCache())
				{
					//the IBLockSelector is not saved anyway so the HelperSelectorBlock cant store it (IBLockSelector is the key in th weak hashMap)
					//it also caused huge memory probolems on servers
					FPBlockSelector sp = new FPBlockSelector(level, FPBlockSelector.getBase(cache));
					lastRange = NetworkManager.supplyAsync(getLevel(), () -> sp.getBlocks(getSenderPosition(), Material.METAL));//this will take too long if done off thread as basicly each getChunkCall needs 1 tick
					sp.clear();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			return lastRange;
		}
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		if(!checked)
		{
			Wireless.checkRegistered(method, getLevel());
			checked = true;
		}
		Wireless.sendEvent(new EventWirelessFunk(getSenderPosition(), frequenz, getRange(), pkt, pkt.getNetworkDepth()+1), level);
	}

	@Override
	public void postPacketSend(PacketBase pkt)
	{

	}

	@Override
	public Level getSenderWorld()
	{
		return this.level;
	}

	@Override
	public BlockPos getSenderPosition()
	{
		return this.worldPosition;
	}

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return frequenz;
		case 1:
			if(lastRange==0)
				return getRange();
			return lastRange;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			frequenz = value;
			break;
		case 1:
			lastRange = value;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 2;
	}

	public int getFrequenz()
	{
		return frequenz;
	}

	public void setFrequenz(int integer)
	{
		frequenz = integer;
	}
}
