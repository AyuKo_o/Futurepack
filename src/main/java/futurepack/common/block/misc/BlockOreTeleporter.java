package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockOreTeleporter extends BlockRotateableTile implements IBlockServerOnlyTickingEntity<TileEntityOreTeleporter>
{

	public BlockOreTeleporter(Properties props)
	{
		super(props);
	}

	@Override
	public BlockEntityType<TileEntityOreTeleporter> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.ORE_TELEPORTER;
	}

}
