package futurepack.common.block.misc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.IntSupplier;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.FuturepackTags;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.chunk.LevelChunkSection;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityOreTeleporter extends FPTileEntityBase implements ITileServerTickable
{
	private static final int MAX_RANGE = 100;

	private CapabilityNeon power = new CapabilityNeon(MAX_RANGE * 5, EnumEnergyMode.USE);
	private LazyOptional<INeonEnergyStorage> neon_opt;
	private LinkedList<BlockPos> orePosList = new LinkedList<>();

	private int fails;
	private int scannedChunks;
	private List<Supplier<List<BlockPos>>> tasks = new ArrayList<>();

	public TileEntityOreTeleporter(BlockPos pos, BlockState state)
	{
		this(FPTileEntitys.ORE_TELEPORTER, pos, state);
	}

	public TileEntityOreTeleporter(BlockEntityType type, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
	}

	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState state)
	{
		final int chunkRange = (2*MAX_RANGE) >> 4;
		if(!tasks.isEmpty())
		{
			orePosList.addAll(tasks.remove(tasks.size()-1).get());
			if(tasks.isEmpty())
			{
				scannedChunks++;
			}
		}
		else if(orePosList.size() < 16)
		{
			BlockPos randomOre = findOre(level, worldPosition, MAX_RANGE);
			if(randomOre!=null)
			{
				orePosList.addLast(randomOre);
				fails &= ~0xFFFFFF;
			}
			else if(scannedChunks <= chunkRange*chunkRange)
			{
				fails++;
				if((fails & 0xFFFFFF) > 20 * 60 * 5)// seit 5 minuten kein erz mehr gefuden, alias 6000 blöcke gescannt
				{
					ChunkPos start = new ChunkPos(worldPosition.offset(-MAX_RANGE, 0, -MAX_RANGE));
					int dx = scannedChunks % (chunkRange);
					int dz = scannedChunks / (chunkRange);
					ChunkPos scan = new ChunkPos(start.x + dx, start.z + dz);
					tasks.addAll(scanChunkForOre(level, scan, worldPosition.getY()-MAX_RANGE, worldPosition.getY()+MAX_RANGE));

				}
			}
		}
		if(power.get() >= power.getMax() && !orePosList.isEmpty())
		{
			int f = fails >> 24;
			if(f <= 0)
			{
				Direction dir = state.getValue(BlockRotateableTile.FACING);
				BlockPos xyz = worldPosition.relative(dir);
				if(level.isEmptyBlock(xyz))
				{
					BlockPos orePos = orePosList.removeFirst();
					if(level.setBlockAndUpdate(xyz, level.getBlockState(orePos)))
					{
						level.destroyBlock(orePos, false);
						power.use(worldPosition.distManhattan(orePos));//this results in maximum of MAX_RANGE * 3 NE for a block move
						fails = (10 << 24) | (fails & 0xFFFFFF);
					}
				}
				else
				{
					fails = (7 << 24) | (fails & 0xFFFFFF);
				}
			}
			else
			{
				f--;
				fails = (f << 24) | (fails & 0xFFFFFF);
			}
		}
	}

	@Nullable
	public static BlockPos findOre(Level level, BlockPos origin, int range)
	{
		final IntSupplier randomInt = () -> level.getRandom().nextInt(range) - level.getRandom().nextInt(range);
		BlockPos randomPos = origin.offset(randomInt.getAsInt(), randomInt.getAsInt(), randomInt.getAsInt());
		BlockState state = level.getBlockState(randomPos);
		if(state.is(FuturepackTags.BLOCK_ORES))
		{
			return randomPos;
		}
		else
		{
			return null;
		}
	}

	/**
	 * Divide Chunk into parts with the height of 16 blockls
	 *
	 * @param level
	 * @param pos
	 * @param minY
	 * @param maxY
	 * @return
	 */
	public static List<Supplier<List<BlockPos>>> scanChunkForOre(Level level, ChunkPos pos, int minY, int maxY)
	{
		LevelChunk chunk = level.getChunk(pos.x, pos.z);
		int min = Math.max(minY, chunk.getMinBuildHeight());
		int max = Math.min(maxY, chunk.getMaxBuildHeight());
		Set<LevelChunkSection> sections = new HashSet<>();
		for(int y=min;y<max;y+=16)
		{
			sections.add(chunk.getSections()[chunk.getSectionIndex(y)]);
		}
		sections.add(chunk.getSections()[chunk.getSectionIndex(max)]);
		return sections.stream().map(s -> scanChunkForOre(s, pos)).toList();
	}

	/**
	 * scan a 16x16x16 block area in 1 chunk
	 * @param section the CHunkSection to scan
	 * @param chunkPos the Chunks position
	 * @return a Callable to execute which returns a List wiht all the ores block position it found in that chunk Section
	 */
	public static Supplier<List<BlockPos>> scanChunkForOre(LevelChunkSection section, ChunkPos chunkPos)
	{
		return () -> {

			List<BlockPos> ores = new ArrayList<>();
			BlockPos origin = chunkPos.getWorldPosition().offset(0, section.bottomBlockY(), 0);
			for(int x=0;x<16;x++)
			{
				for(int y=0;y<16;y++)
				{
					for(int z=0;z<16;z++)
					{
						BlockState state = section.getBlockState(x&16, y&16, z&16);
						if(state.is(FuturepackTags.BLOCK_ORES))
						{
							ores.add(origin.offset(x, y, z));
						}
					}
				}
			}
			return ores;
		};
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(neon_opt!=null)
				return (LazyOptional<T>) neon_opt;
			else
			{
				neon_opt = LazyOptional.of(() -> power);
				neon_opt.addListener(p -> neon_opt = null);
				return (LazyOptional<T>) neon_opt;
			}
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public void setRemoved()
	{
		HelperEnergyTransfer.invalidateCaps(neon_opt);
		super.setRemoved();
	}







	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("energy", power.serializeNBT());
		nbt.putInt("scannedChunks", scannedChunks);
		return nbt;
	}

	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
		scannedChunks = nbt.getInt("scannedChunks");
	}


}
