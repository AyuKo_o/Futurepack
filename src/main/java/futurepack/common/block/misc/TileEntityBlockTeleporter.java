package futurepack.common.block.misc;

import futurepack.api.PacketBase;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileAntenne;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.network.FunkPacketBlockTeleporter;
import futurepack.common.network.NetworkManager;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityBlockTeleporter extends FPTileEntityBase implements ITileAntenne, INetworkUser, ITilePropertyStorage, ITileServerTickable
{
	public CapabilityNeon power = new CapabilityNeon(5000, EnumEnergyMode.USE);
	private LazyOptional<INeonEnergyStorage> neonOpt;

	public TileEntityBlockTeleporter(BlockEntityType<? extends TileEntityBlockTeleporter> pType, BlockPos pWorldPosition, BlockState pBlockState)
	{
		super(pType, pWorldPosition, pBlockState);
	}

	public TileEntityBlockTeleporter(BlockPos pWorldPosition, BlockState pBlockState)
	{
		this(FPTileEntitys.BLOCK_TELEPORTER, pWorldPosition, pBlockState);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction side)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(neonOpt==null)
			{
				neonOpt= LazyOptional.of(()->power);
				neonOpt.addListener(p -> neonOpt = null);
			}
			return (LazyOptional<T>) neonOpt;
		}

		return super.getCapability(capability, side);
	}

	@Override
	public void setRemoved()
	{
		HelperEnergyTransfer.invalidateCaps(neonOpt);
		super.setRemoved();
	}

	public void openGui(Player pl)
	{
		FPGuiHandler.ANTENNA.openGui(pl, worldPosition);
	}

	private int frequenz = 0;

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		nbt.putInt("frequenz", frequenz);
		return super.writeDataUnsynced(nbt);
	}

	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		frequenz = nbt.getInt("frequenz");
		super.readDataUnsynced(nbt);
	}

	public void writeDataSynced(CompoundTag nbt)
	{
		super.writeDataSynced(nbt);
		nbt.put("energy", power.serializeNBT());
	}

	public void readDataSynced(CompoundTag nbt)
	{
		power.deserializeNBT(nbt.getCompound("energy"));
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public int getRange()
	{
		return -1;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		if(pkt instanceof FunkPacketBlockTeleporter)
		{
			((FunkPacketBlockTeleporter)pkt).addTeleporter(this);
		}
	}

	@Override
	public void postPacketSend(PacketBase pkt)
	{
		if(pkt instanceof FunkPacketBlockTeleporter)
		{
			((FunkPacketBlockTeleporter)pkt).getTeleporters().stream().filter(t -> t.getFrequenz() == this.getFrequenz()).unordered().findAny().ifPresent(this::switchBlockData);
		}
	}

	private void switchBlockData(TileEntityBlockTeleporter other)
	{
		NetworkManager.supplyAsync(getLevel(), () -> {
			BlockEntity otherTE = other.getLevel().getBlockEntity(other.getFrontPos());
			BlockEntity thisTE = this.getLevel().getBlockEntity(this.getFrontPos());

			if(otherTE.getClass() == thisTE.getClass())
			{
				CompoundTag tagOther = otherTE.saveWithFullMetadata();
				CompoundTag tagThis = thisTE.saveWithFullMetadata();

				otherTE.load(tagThis);
				thisTE.load(tagOther);

				otherTE.setChanged();
				thisTE.setChanged();

				//2003 is the broken ender eye effect
				other.getLevel().levelEvent(2003, other.getFrontPos(), Block.getId(otherTE.getBlockState()));
				this.getLevel().levelEvent(2003, this.getFrontPos(), Block.getId(thisTE.getBlockState()));

				this.power.use(1000);
			}
			return null;
		});
	}

	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		if(power.get() >= 1000)
		{
			if(pLevel.random.nextInt(20 * 60) == 0)
			{
				BlockState infront = getState();
				if(infront!=null)
				{
					NetworkManager.sendPacketThreaded(this, new FunkPacketBlockTeleporter(worldPosition, this, infront));
				}
			}
		}
	}

	@Override
	public Level getSenderWorld()
	{
		return this.level;
	}

	@Override
	public BlockPos getSenderPosition()
	{
		return this.worldPosition;
	}

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return frequenz;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			frequenz = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 1;
	}

	public int getFrequenz()
	{
		return frequenz;
	}

	public void setFrequenz(int integer)
	{
		frequenz = integer;
	}

	public BlockState getState()
	{
		BlockPos pos = getFrontPos();
		if(level.getBlockEntity(pos) != null)
		{
			return level.getBlockState(pos);
		}
		else
			return null;
	}

	public BlockPos getFrontPos()
	{
		return  worldPosition.relative(getBlockState().getValue(BlockRotateable.HORIZONTAL_FACING) );
	}
}
