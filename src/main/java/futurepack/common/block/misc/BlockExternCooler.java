package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SimpleWaterloggedBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockExternCooler extends BlockRotateableTile implements SimpleWaterloggedBlock, IBlockBothSidesTickingEntity<TileEntityExternCooler>
{
	public static final IntegerProperty TEMPERATUR = IntegerProperty.create("temp", 0, 2);
	public static final BooleanProperty WATERLOGGED = BlockStateProperties.WATERLOGGED;
	   
	private final VoxelShape SHAPE_UP = Shapes.or(Block.box(0, 0, 0, 16, 4, 16), Block.box(1, 4, 1, 15, 15, 15));
	private final VoxelShape SHAPE_DOWN = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 180F, 0F);
	private final VoxelShape SHAPE_NORTH = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 90F, 0F);
	private final VoxelShape SHAPE_SOUTH = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 90F, 180F);
	private final VoxelShape SHAPE_WEST = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 90F, 90F);
	private final VoxelShape SHAPE_EAST = HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 90F, 270F);
	
	public BlockExternCooler(Block.Properties props) 
	{
		super(props);
		this.registerDefaultState(this.defaultBlockState().setValue(TEMPERATUR, 1).setValue(WATERLOGGED, Boolean.valueOf(false)));
		
		HelperBoundingBoxes.createBlockShape(new double[][] {{0,0,0,16,4,16}, {1,4,1,15,15,15}}, 0F, 0F);
		
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(TEMPERATUR, WATERLOGGED);
	}
	
	@Override
	public BlockState updateShape(BlockState stateIn, Direction facing, BlockState facingState, LevelAccessor w, BlockPos pos, BlockPos facingPos)
	{
		if (stateIn.getValue(WATERLOGGED)) 
		{
			w.scheduleTick(pos, Fluids.WATER, Fluids.WATER.getTickDelay(w));
		}

		
		@SuppressWarnings("deprecation")
        BlockState state = super.updateShape(stateIn, facing, facingState, w, pos, facingPos);
		TileEntityExternCooler t = (TileEntityExternCooler) w.getBlockEntity(pos);
		if(t==null)
			return state;
		else
			return state.setValue(TEMPERATUR, Math.max((int)(t.f - 1), 0));
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context) 
	{
		switch(state.getValue(FACING))
		{
		case DOWN:
			return SHAPE_DOWN;
		case NORTH:
			return SHAPE_NORTH;
		case SOUTH:
			return SHAPE_SOUTH;
		case WEST:
			return SHAPE_WEST;
		case EAST:
			return SHAPE_EAST;
		case UP:
		default:
			return SHAPE_UP;
		}
	}
	
	@Override
	public FluidState getFluidState(BlockState state)
	{
		return state.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(state);
	}
	

	
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return super.getStateForPlacement(context).setValue(FACING, context.getClickedFace());
	}

	@Override
	public BlockEntityType<TileEntityExternCooler> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.EXTERN_COOLER;
	}
}
