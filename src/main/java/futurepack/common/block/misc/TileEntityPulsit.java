package futurepack.common.block.misc;

import java.util.List;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.decoration.HangingEntity;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.phys.AABB;

public class TileEntityPulsit extends FPTileEntityBase implements ITileServerTickable, ITileClientTickable
{
	boolean last = false;
	boolean deferedTick = false;


	public TileEntityPulsit(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.GRAVITY_PULSER, pos, state);
	}

	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}

	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}

	public void tick()
	{
		boolean now =  level.getBestNeighborSignal(worldPosition)>0;

		if(!last&&now || deferedTick)
		{
			deferedTick = false;
			Direction face = getBlockState().getValue(BlockRotateableTile.FACING);

			if(createFallingBlock(level, worldPosition.relative(face)) && !deferedTick)
			{
				deferedTick = true;
				return;
			}

			int range = 20;
			List<Entity> list = level.getEntities(null, new AABB(-range, -range, -range, range, range, range).move(worldPosition.getX()+0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5));//, null);//getEntitiesWithinAABBExcludingEntity

			for(Entity item : list)
			{
				if(item instanceof HangingEntity)
				{
					continue;
				}

//				double[] d = invertDirektion(d3, d4, d5, dir);
//				d3 = d[0];
//				d4 = d[1];
//				d5 = d[2];
//
//				d3 = mincheck(d3);
//				d4 = mincheck(d4);
//				d5 = mincheck(d5);
//
//				double dis = Math.sqrt(getDistanceSq(item.posX, item.posY, item.posZ));
//
//				d3 = d3/dis *2;
//				d4 = d4/dis *2;
//				d5 = d5/dis *2;

				double dis = Math.sqrt(worldPosition.distToLowCornerSqr(item.getX(), item.getY(), item.getZ()));
				double mX = face.getStepX()*2;
				double mY = face.getStepY()*2;
				double mZ = face.getStepZ()*2;
				mX *= (20/dis/20);
				mY *= (20/dis/20);
				mZ *= (20/dis/20);
				item.push(mX, mY, mZ);
				item.fallDistance = 0F;
			}
		}
		last = now;
	}

	public static boolean createFallingBlock(Level w, BlockPos pos)
	{
//		if(!w.isRemote)
		{
			BlockState state = w.getBlockState(pos);
			if(state.isAir())
				return false;

			PushReaction reaction = state.getPistonPushReaction();
			if(reaction == PushReaction.NORMAL || reaction == PushReaction.PUSH_ONLY)
			{
				FallingBlockEntity falling = FallingBlockEntity.fall(w, pos, state);
				falling.setPos(falling.getX(), falling.getY()+0.1, falling.getZ());

//				FallingBlockEntity falling = new FallingBlockEntity(w, pos.getX()+0.5, pos.getY()+0.1, pos.getZ()+0.5, state);
				return falling.isAddedToWorld();


			}
		}

		return false;
	}
}
