package futurepack.common.block.misc;

import java.util.HashMap;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.world.scanning.ChunkData;
import futurepack.world.scanning.FPChunkScanner;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityBedrockRift extends FPTileEntityBase
{
	public TileEntityBedrockRift(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.BEDROCK_RIFT, pos, state);
	}

	private double fillrate = 1.0;
	private boolean scanned = false;

	private ChunkData data = null;

	private CompoundTag fakeData;

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		nbt.putDouble("fillrate", fillrate);
		nbt.putBoolean("scanned", scanned);
		if(fakeData!=null)
			nbt.put("fakeEntries", fakeData);
		return super.writeDataUnsynced(nbt);
	}

	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		fillrate = nbt.getDouble("fillrate");
		scanned = nbt.getBoolean("scanned");
		if(nbt.contains("fakeEntries"))
		{
			fakeData = nbt.getCompound("fakeEntries");
		}
		super.readDataUnsynced(nbt);
	}


	public ChunkData getData()
	{
		if(!scanned)
			return null;

		if(data!=null)
			return data;

		if(fakeData!=null)
		{
			HashMap<String, Integer> map = new HashMap<>();
			for(String key : fakeData.getAllKeys())
			{
				map.put(key, fakeData.getInt(key));
			}
			data = new ChunkData(level, worldPosition, map);
			return data;
		}
		data = FPChunkScanner.INSTANCE.getData(level, worldPosition);
		return data;
	}

	public double getFillrate()
	{
		return fillrate;
	}

	public void removeOres(double removed)
	{
		fillrate -= removed;
		if(fillrate <= 0.0)
			fillrate = 0.0;
	}

	public void regenerate(double d)
	{
		fillrate += d;
	}

	public boolean isScanned()
	{
		return scanned;
	}

	public void setScanned(boolean b)
	{
		scanned = b;
	}

	public int getComparatorOutput()
	{
		int redstone = (int) (fillrate * 15);
		if(fillrate>=1)
			redstone = 15;
		else if(redstone==0 && fillrate>0)
			redstone=1;

		return redstone;
	}
}
