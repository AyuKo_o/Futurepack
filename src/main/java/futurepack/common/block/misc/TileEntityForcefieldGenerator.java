package futurepack.common.block.misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiPredicate;

import futurepack.api.FacingUtil;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperHologram;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Vec3i;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityForcefieldGenerator extends TileEntityClaime implements ITileServerTickable, BiPredicate<TileEntityForceField, Entity>
{
	
	private boolean[] directions = new boolean[6];
	
	public boolean letProjectilesPass = false;
	public boolean letMobsPass = false;
	public boolean letAnimalsPass = false;
	public boolean letBabysPass = false;
	public boolean letPlayerPass = false;
	
	//could also sue a tree set, but array is more memory efficient; and I expect huge ships to be fully engulved in force fields... 
	// its sorted so access time fro both is O(log n)
	private BlockPos[] forcefieldBlocks = null;

	public TileEntityForcefieldGenerator(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.FORCEFIELD_GENERATOR, pos, state);
		Arrays.fill(directions, true);
	}

	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState) 
	{		
		generateShape().forEach(p -> {
			if(pLevel.getBlockState(p).isAir())
			{
				pLevel.setBlockAndUpdate(p, MiscBlocks.force_field.defaultBlockState());
			}
		});
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.putBoolean("projectiles", letProjectilesPass);
		nbt.putBoolean("mobs", letMobsPass);
		nbt.putBoolean("animals", letAnimalsPass);
		nbt.putBoolean("babys", letBabysPass);
		nbt.putBoolean("player", letPlayerPass);
		
		for(int i=0;i<directions.length;i++)
		{
			nbt.putBoolean("direction"+i, directions[i]);
		}
		
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		letProjectilesPass = nbt.getBoolean("projectiles");
		letMobsPass = nbt.getBoolean("mobs");
		letAnimalsPass = nbt.getBoolean("animals");
		letBabysPass = nbt.getBoolean("babys");
		letPlayerPass = nbt.getBoolean("player");
		
		for(int i=0;i<directions.length;i++)
		{
			directions[i] = nbt.getBoolean("direction"+i);
		}
	}

	protected boolean isInside(int x, int y, int z)
	{
		if(x >= -super.x || x <= super.x+1)
		{
			if(z >= -super.z || z <= super.z+1)
			{
				if(y >= -super.y || y <= super.y+1)
				{
					
					double dx = (double)x / super.x;
					double dy = (double)y / super.y;
					double dz = (double)z / super.z;
					
					return dx*dx + dy*dy + dz*dz < 1;
				}
			}
		}
		
		return false;
	}
	
	
	public List<BlockPos> generateShape()
	{
		ArrayList<BlockPos> blocks = new ArrayList<>();
		for(Direction dir : FacingUtil.VALUES)
		{
			if(directions[dir.ordinal()])
			{
				List<int[]> list = makeSide(dir);	
				blocks.ensureCapacity(list.size());
				int offsetX = super.mx + getBlockPos().getX();
				int offsetY = super.my + getBlockPos().getY();
				int offsetZ = super.mz + getBlockPos().getZ();
				list.stream().map(i -> {i[0]+=offsetX;i[1]+=offsetY;i[2]+=offsetZ;return i;}).map(i -> new BlockPos(i[0], i[1], i[2])).forEach(blocks::add);
			}
		}
		blocks.trimToSize();
		blocks.sort(null);
		return blocks;
	}
	
	
	protected List<int[]> makeSide(Direction dir)
	{
		int minX = -super.x-1;
		int maxX = super.x+1;
		int minY = -super.y-1;
		int maxY = super.y+1;
		int minZ = -super.z-1;
		int maxZ = super.z+1;
		
		List<int[]> blocks = new ArrayList<>();
		
		switch(dir)
		{
		case UP:
			for(int x=minX;x<=maxX;x++)
			{
				for(int z=minZ;z<=maxZ;z++)
				{
					for(int y=maxY;y>=minY;y--)
					{
						if(isInside(x, y, z))
						{
							blocks.add(new int[] {x,y,z});
							break;
						}
					}
				}
			}
			break;
		case DOWN:
			for(int x=minX;x<=maxX;x++)
			{
				for(int z=minZ;z<=maxZ;z++)
				{
					for(int y=minY;y<=maxY;y++)
					{
						if(isInside(x, y, z))
						{
							blocks.add(new int[] {x,y,z});
							break;
						}
					}
				}
			}
			break;
		case SOUTH:
			for(int x=minX;x<=maxX;x++)
			{
				for(int y=minY;y<=maxY;y++)
				{
					for(int z=maxZ;z>=minZ;z--)
					{
						if(isInside(x, y, z))
						{
							blocks.add(new int[] {x,y,z});
							break;
						}
					}
				}
			}
			break;
		case NORTH:
			for(int x=minX;x<=maxX;x++)
			{
				for(int y=minY;y<=maxY;y++)
				{
					for(int z=minZ;z<=maxZ;z++)
					{
						if(isInside(x, y, z))
						{
							blocks.add(new int[] {x,y,z});
							break;
						}
					}
				}
			}
			break;
		case EAST:
			for(int y=minY;y<=maxY;y++)
			{
				for(int z=minZ;z<=maxZ;z++)
				{
					for(int x=maxX;x>=minX;x--)
					{
						if(isInside(x, y, z))
						{
							blocks.add(new int[] {x,y,z});
							break;
						}
					}
				}
			}
			break;
		case WEST:
			for(int y=minY;y<=maxY;y++)
			{
				for(int z=minZ;z<=maxZ;z++)
				{
					for(int x=minX;x<=maxX;x++)
					{
						if(isInside(x, y, z))
						{
							blocks.add(new int[] {x,y,z});
							break;
						}
					}
				}
			}
			break;
		}
		
		
		return blocks;
	}
	
	public BlockPos[] getForcefieldBlocks()
	{
		return forcefieldBlocks != null ? forcefieldBlocks : (forcefieldBlocks = generateShape().toArray(BlockPos[]::new));
	}
	
	public boolean isForcefieldAtPos(BlockPos pos)
	{
		return Arrays.binarySearch(getForcefieldBlocks(), pos, null) >= 0;
	}
	

	LazyOptional<BiPredicate<TileEntityForceField, Entity>> opt = null;
	
	@Override
	public void setRemoved()
	{
		HelperEnergyTransfer.invalidateCaps(opt);
		super.setRemoved();
	}
	
	public LazyOptional<BiPredicate<TileEntityForceField, Entity>> getCallback(TileEntityForceField t) 
	{
		if(isForcefieldAtPos(t.getBlockPos()))
		{
			return opt!=null ? opt : (opt = LazyOptional.of(() -> this));
		}
		else
		{
			return LazyOptional.empty();
		}
	}

	@Override
	public boolean test(TileEntityForceField t, Entity e) 
	{
		if(e instanceof Projectile && letProjectilesPass)
		{
			return true;
		}		
		
		if(e instanceof LivingEntity base)
		{
			if(base.isBaby() && letBabysPass)
			{
				return true;
			}
			if(isOwner(base))
			{
				return true;
			}
			
		}
		
		if(e instanceof Animal && letAnimalsPass)
		{
			return true;
		}
		
		if(e instanceof Enemy && letMobsPass)
		{
			return true;
		}
		
		if(e instanceof Player pl && letPlayerPass)
		{
			return true;
		}
		
		return false;
	}
	
	
}
