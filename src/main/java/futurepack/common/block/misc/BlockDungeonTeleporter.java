package futurepack.common.block.misc;

import futurepack.world.WorldGenTecDungeon;
import net.minecraft.core.BlockPos;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockDungeonTeleporter extends Block implements EntityBlock
{
	private final boolean up, down;

	protected BlockDungeonTeleporter(Properties props, boolean up, boolean down) 
	{
		super(props);
		this.up = up;
		this.down = down;
		
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(w.isClientSide)
			return InteractionResult.SUCCESS;
			
		MinecraftServer serv = w.getServer();
		if(serv!=null)
		{
			WorldGenTecDungeon.handleTeleporterClick(w, serv, pos, pl, state);
		}
		return InteractionResult.SUCCESS;
	}

	public boolean isUp()
	{
		return up;
	}
	
	public boolean isDown()
	{
		return down;
	}
	

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state) 
	{
		return new TileEntityDungeonTeleporter(pos, state);
	}
}
