package futurepack.common.block.misc;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.google.common.base.Predicates;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.logistic.frames.TileEntityWithMiniWorldBase;
import futurepack.depend.api.MiniWorld;
import futurepack.world.dimensions.TreeUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

public class TileEntityFallingTree extends TileEntityWithMiniWorldBase implements ITileServerTickable, ITileClientTickable
{
	public TileEntityFallingTree(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.FALLING_TREE, pos, state);
		ticks = maxticks = 30;
	}

	public ArrayList<ItemStack>[][] drops;
	public Direction fall;
	public ArrayList<WeakReference<Entity>> spawned = new ArrayList<WeakReference<Entity>>();
	public ArrayList<Consumer<TileEntityFallingTree>> listeners = new ArrayList<Consumer<TileEntityFallingTree>>();
	
	public void tick()
	{
		ticks--;
		
//		BlockPos start = w.start.add(w.rotationpoint.xCoord, w.rotationpoint.yCoord, w.rotationpoint.zCoord);
		if(w==null || ticks<=0)
		{			
			spawnDestroyParticleTypes();
			level.removeBlock(worldPosition, false);
			onBlockBreak();
			
			for(Consumer<TileEntityFallingTree> fkt : listeners)
			{
				fkt.accept(this);
			}		
		}
	}	
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	@Override
	public void tickClient(Level pLevel, BlockPos pPos, BlockState pState)
	{
		tick();
	}
	
	
	
	private void addEntity(ItemEntity e)
	{
		if(level.addFreshEntity(e) && e.isAlive())
		{
			WeakReference<Entity> ref = new WeakReference<Entity>(e);
			spawned.add(ref);
		}
		else
		{
			AABB bb = new AABB(e.getX() -0.5, e.getY() -0.5, e.getZ() -0.5, e.getX() +0.5, e.getY() +0.5, e.getZ() +0.5);
			List<ItemEntity> list = level.getEntitiesOfClass(ItemEntity.class, bb);
			
			list.forEach(item -> {
				WeakReference<Entity> ref = new WeakReference<Entity>(item);
				spawned.add(ref);
			});
		}
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{		
		if(drops!=null)
		{
			CompoundTag items = new CompoundTag();
			items.putInt("j", drops.length);
			items.putInt("k", drops[0].length);
			for(int j=0;j<drops.length;j++)
			{
				for(int k=0;k<drops[0].length;k++)
				{
					ArrayList<ItemStack> list = drops[j][k];
					if(list==null || list.isEmpty())
					{
						continue;
					}
					
					compress(list);
					int pos = j<<16 | ( k & 0xFFFF );
					ListTag tags = new ListTag();
					for(ItemStack it : list)
					{
						tags.add(it.save(new CompoundTag()));
					}
					items.put(""+pos, tags);
				}
			}
			nbt.putString("items", getSaveString(items));
		}
		
		return super.writeDataUnsynced(nbt);
	}
	
	
	private void compress(ArrayList<ItemStack> list)
	{
		for(int i=0;i<list.size();i++)
		{
			ItemStack it = list.get(i);
			for(int j=i;j<list.size();j++)
			{
				if(i!=j)
				{
					ItemStack it2 = list.get(j);
					if(it.sameItem(it2) && ItemStack.tagMatches(it, it2))
					{
						it.grow(it2.getCount());
						list.remove(j);
					}
				}
			}
		}
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		drops = new ArrayList[nbt.getInt("j")][nbt.getInt("k")];
		if(nbt.contains("items"))
		{
			CompoundTag items = fromSaveString(nbt.getString("items"));
			if(items!=null)
			{
				for(int j=0;j<drops.length;j++)
				{
					for(int k=0;k<drops[0].length;k++)
					{
						int pos = j<<16 | ( k & 0xFFFF );
						if(items.contains(""+pos))
						{
							drops[j][k] = new ArrayList<ItemStack>();
						
							ListTag tags = items.getList(""+pos, 10);
							for(int i=0;i<tags.size();i++)
							{
						drops[j][k].add(ItemStack.of(tags.getCompound(i)));
							}
						}
						
					}
				}	
			}
		}
		
		super.readDataUnsynced(nbt);
	}

	
	public void setMiniWorld(MiniWorld w)
	{
		this.w = w;
		maxticks = ticks = (int) (w.height/10F * 30F);
	}
	
	private void spawnDestroyParticleTypes()
	{
		if(w!=null && level.isClientSide)
		{
			float[] rotation = new float[]{w.face.getStepX(),  w.face.getStepY(),  w.face.getStepZ()};
			double rad = Math.toRadians(90D * (1D - (double)ticks/(double)maxticks));
			rotation[0]*=rad;
			rotation[1]*=rad;
			rotation[2]*=rad;
			
			float[] rot = new float[6];
			rot[0] = (float) Math.sin(rotation[0]);
			rot[1] = (float) Math.sin(rotation[1]);
			rot[2] = (float) Math.sin(rotation[2]);
			
			rot[3] = (float) Math.cos(rotation[0]);
			rot[4] = (float) Math.cos(rotation[1]);
			rot[5] = (float) Math.cos(rotation[2]);
			
			Predicate<BlockState> states = Predicates.or(TreeUtils.getLogPredicate(), TreeUtils.getMushroomPredicate());
			
			BlockPos delta = this.worldPosition.subtract(w.start);
			
			float[] pos = new float[3];
			
			for(int y=0;y < w.height;y++)
			{
				for(int x=0;x < w.width;x++)
				{
					for(int z=0;z < w.depth;z++)
					{
						if(!states.test(w.states[x][y][z]))
							continue;
						
						pos[0] = x - delta.getX();
						pos[1] = y - delta.getY();
						pos[2] = z - delta.getZ();
						
						float f1,f2;
						f1 = pos[1] * rot[3] - pos[2] * rot[0];///this is the roation
						f2 = pos[1] * rot[0] + pos[2] * rot[3];
						pos[1]=f1;
						pos[2]=f2;
						
						f1 = pos[0] * rot[4] + pos[2] * rot[1];
						f2 = -pos[0] * rot[1] + pos[2] * rot[4];
						pos[0]=f1;
						pos[2]=f2;
						
						f1 = pos[0] * rot[5] - pos[1] * rot[2];
						f2 = pos[0] * rot[2] + pos[1] * rot[5];
						pos[0]=f1;
						pos[1]=f2;
						
						level.levelEvent(2001, this.worldPosition.offset(pos[0], pos[1], pos[2]), Block.getId(w.states[x][y][z]));
					}
				}
			}
		}
	}

	public void onBlockBreak() 
	{
		if(w!=null && !level.isClientSide && drops!=null)
		{
			for(int x=0;x<drops.length;x++)
			{
				for(int z=0;z<drops[x].length;z++)
				{		
					ArrayList<ItemStack> arr = drops[x][z];
					if(arr!=null)
					{		
						if(arr.isEmpty())
							continue;
						
						Direction f2 = fall.getClockWise();
						BlockPos pos = getBlockPos().relative(fall, -z).relative(f2, -x);
						pos = pos.offset(new BlockPos(w.rotationpoint.x*f2.getStepX(), w.rotationpoint.y*f2.getStepY(), w.rotationpoint.z*f2.getStepZ()));
						while(!level.isEmptyBlock(pos))
						{
							pos = pos.above();
						}
						
//						if (worldObj instanceof WorldServer)
//		                {
//		                    WorldServer worldserver = (WorldServer)worldObj;
//
//		                    worldserver.spawnParticle(ParticleTypes.NOTE, true,  pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, 1, 0D, 0D, 0D, 0D);
//		                }
						
						for(ItemStack it : arr)
						{
							addEntity(new ItemEntity(level, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, it));
						}
					}
				}
			}
		}
		drops = null;
	}
	
//	@Override
//	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
//	{
//		boolean b = super.shouldRefresh(world, pos, oldState, newSate);
//		if(b)
//		{
//			spawnDestroyParticleTypes();
//		}
//		return b;
//	}
	
	
}
