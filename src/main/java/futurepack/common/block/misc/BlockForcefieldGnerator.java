package futurepack.common.block.misc;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockForcefieldGnerator extends BlockHoldingTile implements IBlockServerOnlyTickingEntity<TileEntityForcefieldGenerator>
{
	private static final VoxelShape box = Shapes.box(0.3F, 0.3F, 0.3F, 0.7F, 0.7F, 0.7F);


	public BlockForcefieldGnerator(Properties props) 
	{
		super(props);
	}
	
	@Override
	public BlockEntityType<TileEntityForcefieldGenerator> getTileEntityType(BlockState pState) 
	{
		return FPTileEntitys.FORCEFIELD_GENERATOR;
	}
	
	
	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.MODEL;
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		return box;
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.CLAIME.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public void setPlacedBy(Level w, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack)
	{
		super.setPlacedBy(w, pos, state, placer, stack);
		BlockEntity tile = w.getBlockEntity(pos);
		if(tile!=null)
		{
			((TileEntityClaime)tile).onPlaced(placer);
		}
		
	}

}
