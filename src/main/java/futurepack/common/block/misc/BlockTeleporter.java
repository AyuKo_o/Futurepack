package futurepack.common.block.misc;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.item.tools.ToolItems;
import futurepack.depend.api.helper.HelperEntities;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.BlockHitResult;

public class BlockTeleporter extends BlockHoldingTile
{	
	private final boolean up, down;
	
	
	protected BlockTeleporter(Block.Properties props, boolean up, boolean down)
	{
		super(props);
		this.up = up;
		this.down = down;
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(w.isClientSide)
			return InteractionResult.SUCCESS;
			
		if(pl.getItemInHand(InteractionHand.MAIN_HAND)!=null && pl.getItemInHand(InteractionHand.MAIN_HAND).getItem() == ToolItems.scrench)
		{
			if(up & down)
			{
				if(w.isEmptyBlock(pos.below()))
				{
//					HelperItems
					HelperEntities.disableItemSpawn();
					w.setBlock(pos, MiscBlocks.teleporter_down.defaultBlockState(), 2);
					HelperEntities.enableItemSpawn();
					FallingBlockEntity falling = new FallingBlockEntity(w, pos.getX()+0.5,  pos.getY()-1,  pos.getZ()+0.5, MiscBlocks.teleporter_up.defaultBlockState());
					falling.time = 1;
					w.addFreshEntity(falling);
					
					
				}
				else if(w.getBlockState(pos.below()).getMaterial() == Material.METAL)
				{
					BlockPos xyz = pos.below();
					while(w.getBlockState(xyz).getMaterial() == Material.METAL)
					{
						xyz = xyz.below();
					}
					if(w.isEmptyBlock(xyz))
					{
						HelperEntities.disableItemSpawn();
						w.setBlock(pos, MiscBlocks.teleporter_down.defaultBlockState(), 2);
						HelperEntities.enableItemSpawn();
						FallingBlockEntity falling = new FallingBlockEntity(w, xyz.getX()+0.5,  xyz.getY(),  xyz.getZ()+0.5, MiscBlocks.teleporter_up.defaultBlockState());
						falling.time = 1;
						w.addFreshEntity(falling);
					}
				}
			}
			else if(up)
			{
				BlockPos pos2 = getNextTeleporter(w, pos, true);
				if(pos2!=null)
				{
					BlockState state2 = w.getBlockState(pos2);
					if(state2.getBlock() == MiscBlocks.teleporter_down)
					{
						HelperEntities.disableItemSpawn();
						w.setBlock(pos, MiscBlocks.teleporter_both.defaultBlockState(), 2);
						w.removeBlock(pos2, false);
						HelperEntities.enableItemSpawn();
					}
				}
			}
			else if(down)
			{
				BlockPos pos2 = getNextTeleporter(w, pos, false);
				if(pos2!=null)
				{
					BlockState state2 = w.getBlockState(pos2);
					if(state2.getBlock() == MiscBlocks.teleporter_up)
					{
						HelperEntities.disableItemSpawn();
						w.setBlock(pos, MiscBlocks.teleporter_both.defaultBlockState(), 2);
						w.removeBlock(pos2, false);
						HelperEntities.enableItemSpawn();
					}
				}
			}
		}
		else
		{
			if(up && down)
			{
				if(hit.getLocation().y==1F && hit.getLocation().x>=0.125F && hit.getLocation().x<=0.875F)
				{
					if(hit.getLocation().z>=0.125F && hit.getLocation().z<0.5F)
						up(w,pos,pl);
					if(hit.getLocation().z>0.5F && hit.getLocation().z<=0.875F)
						down(w,pos,pl);
				}
				
				return InteractionResult.SUCCESS;
			}
			if(up)
			{
				up(w,pos,pl);
				return InteractionResult.SUCCESS;
			}
			if(down)
			{
				down(w,pos,pl);
				return InteractionResult.SUCCESS;
			}
		}
		return InteractionResult.PASS;
	}
	
	protected void up(Level w, BlockPos pos, Player pl)
	{
		if(w.isClientSide)
			return;
		for(int i=1;i<w.getMaxBuildHeight()-pos.getY();i++)
		{
			BlockPos jkl = pos.above(i);
			if(isTeleporter(w.getBlockState(jkl)))
			{
				TileEntityTeleporter b = (TileEntityTeleporter) w.getBlockEntity(pos);
//				boolean inf = w.getBlockState(pos).get(INFINITE);
				if(b.energy.use(i) > 0)
				{
					pl.teleportTo(jkl.getX()+0.5, jkl.getY()+1.2, jkl.getZ()+0.5);
//					if(inf)
//					{
//						Research r = ResearchManager.getResearch("story.kryptographie");
//						CustomPlayerData.getDataFromPlayer(pl).addResearch(r);
//					}
				}
				else //if(!w.isRemote)
				{
					float f = b.energy.get() / b.energy.getMax();
					pl.sendMessage(new TranslatableComponent("chat.beam.lowEnergie",f), Util.NIL_UUID);
				}
				return;
				
			}
		}
		pl.sendMessage(new TranslatableComponent("chat.beam.noBlockAbove"), Util.NIL_UUID);
	}
	
	protected void down(Level w, BlockPos pos, Player pl)
	{
		if(w.isClientSide)
			return;
		for(int i=1;i<pos.getY()-w.getMinBuildHeight();i++)
		{
			BlockPos jkl = pos.below(i);
			if(isTeleporter(w.getBlockState(jkl)))
			{
				TileEntityTeleporter b = (TileEntityTeleporter) w.getBlockEntity(pos);
				if(b.energy.use(i) > 0)
				{
					pl.teleportTo(jkl.getX()+0.5, jkl.getY()+1.2, jkl.getZ()+0.5);
//					if(inf)
//					{
//						Research r = ResearchManager.getResearch("story.kryptographie");
//						CustomPlayerData.getDataFromPlayer(pl).addResearch(r);
//					}
//					
				}
				else //if(!w.isRemote)
				{
					float f = b.energy.get() / b.energy.getMax();
					pl.sendMessage(new TranslatableComponent("chat.beam.lowEnergie",f), Util.NIL_UUID);
				}
				return;
			}
		}
		pl.sendMessage(new TranslatableComponent("chat.beam.noBlockBelow"), Util.NIL_UUID);
	}
	
	private BlockPos getNextTeleporter(Level w, BlockPos pos, boolean up)
	{
		while(pos.getY()>0 && pos.getY()<w.getMaxBuildHeight())
		{
			pos = up ? pos.above() : pos.below();
			if(isTeleporter(w.getBlockState(pos)))
			{
				return pos;
			}
		}
		return null;
	}
	
	public boolean isTeleporter(BlockState state)
	{
		Block bl = state.getBlock();
		return bl == MiscBlocks.teleporter || bl == MiscBlocks.teleporter_both || bl == MiscBlocks.teleporter_down || bl == MiscBlocks.teleporter_up;
	}
	
//	@Override
//	public void getSubBlocks(ItemGroup tab, NonNullList l) 
//	{
//		l.add(new ItemStack(this, 1, 0));
//		l.add(new ItemStack(this, 1, 1));
//		l.add(new ItemStack(this, 1, 2));
//		l.add(new ItemStack(this, 1, 3));
//	}

//	@Override
//	public String getMetaNameSub(ItemStack is)
//	{
//		switch(is.getItemDamage())
//		{
//		case 0:return "button";
//		case 1:return "up";
//		case 2:return "down";
//		
//		case 4:return "inf_button";
//		case 5:return "inf_up";
//		case 6:return "inf_down";
//		case 7:return "inf";
//
//		default: return "";
//		}
//	}
//
//	@Override
//	public int getMaxMetas()
//	{
//		return 8;
//	}
//
//	@Override
//	public String getMetaName(int meta)
//	{
//		switch(meta)
//		{
//		case 0:return "beam_button";
//		case 1:return "beam_up";
//		case 2:return "beam_down";
//		case 3:return "beam";
//		case 4:return "beam_inf_button";
//		case 5:return "beam_inf_up";
//		case 6:return "beam_inf_down";
//		case 7:return "beam_inf";
//		default: return "beam";
//		}
//	}
	
//	@Override
//	public float getBlockHardness(IBlockState blockState, World worldIn, BlockPos pos)
//	{
//		if(blockState.get(INFINITE))
//			return -1F;
//		return super.getBlockHardness(blockState, worldIn, pos);
//	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new TileEntityTeleporter(pos, state);
	}
}
