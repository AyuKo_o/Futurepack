package futurepack.common.block.misc;

import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityDungeonTeleporter extends FPTileEntityBase 
{

	public TileEntityDungeonTeleporter(BlockEntityType<TileEntityDungeonTeleporter> tileEntityTypeIn, BlockPos pos, BlockState state) 
	{
		super(tileEntityTypeIn, pos, state);
	}

	public TileEntityDungeonTeleporter(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.TELEPORTER_DUNGEON, pos, state);
	}
	
	private BlockPos otherTeleporter = null;
	private ResourceLocation targetDimension = null;
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt) 
	{
		if(otherTeleporter!=null)
		{
			nbt.putIntArray("teleport", new int[]{otherTeleporter.getX(), otherTeleporter.getY(), otherTeleporter.getZ()});
		}
		if(targetDimension!=null)
		{
			nbt.putString("dimension", targetDimension.toString());
		}
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt) 
	{
		super.readDataUnsynced(nbt);
		if(nbt.contains("dimension"))
		{
			targetDimension = new ResourceLocation(nbt.getString("dimension"));
		}
		if(nbt.contains("teleport"))
		{
			int[] t = nbt.getIntArray("teleport");
			otherTeleporter = new BlockPos(t[0], t[1], t[2]);
		}
	}
	
	public void setTargetTeleporter(BlockPos pos, Level w)
	{
		otherTeleporter = pos;
		targetDimension = w.dimension().location();
		this.setChanged();
	}

	public BlockPos getOtherTeleporter() {
		return otherTeleporter;
	}

	public ResourceKey<Level> getTargetDimension() 
	{
		ResourceKey<Level> registrykey = ResourceKey.create(Registry.DIMENSION_REGISTRY, targetDimension);
		return registrykey;
	}
	
	
}
