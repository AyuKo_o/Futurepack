package futurepack.common.block.terrain;

import java.util.List;
import java.util.Random;

import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.ParticleEngine;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BlockPrismid extends Block
{
	private final VoxelShape box = Block.box(1, 0, 1, 15, 15, 15);

	public BlockPrismid(Block.Properties props)
	{
		super(props.randomTicks().noOcclusion());
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		return box;
	}

	@Override
	public boolean canSurvive(BlockState state, LevelReader w, BlockPos pos)
	{
		BlockState stateDown = w.getBlockState(pos.below());
		return (stateDown.getBlock() == TerrainBlocks.prismide_stone.get());
	}

	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random r)
	{
		super.tick(state, w, pos, r);

		neighborChanged(state, w, pos, Blocks.AIR, null, false);
		if(r.nextInt(1)==0 && !w.isClientSide)
		{
			List<LivingEntity> targets = w.getEntitiesOfClass(LivingEntity.class, new AABB(pos.offset(-4, -4, -4), pos.offset(4, 4, 4)));
			for (LivingEntity entity : targets)
			{
				entity.addEffect(new MobEffectInstance(MobEffects.WITHER, 100, 2));
			}
		}

		if(w.isClientSide)
		{
			w.addParticle(ParticleTypes.FIREWORK, pos.getX(), pos.getY()+0.4, pos.getZ()+0.5, r.nextGaussian() * 0.05D, 0.5D, r.nextGaussian() * 0.05D);
		}
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, Level w, BlockPos pos, Random rand)
	{
		if(rand.nextFloat() <= 0.001)
	{

		ParticleEngine aman = Minecraft.getInstance().particleEngine;

		for(int i = 0; i < 20; i++)
		{
			float x = pos.getX() + rand.nextFloat();
			float y = pos.getY() + rand.nextFloat();
			float z = pos.getZ() + rand.nextFloat();

//			SimpleAnimatedParticle sp = new FireworkParticle.Spark(w, x, y, z, rand.nextFloat() - 0.5, -0.2 + rand.nextFloat() / 2.0  , rand.nextFloat() - 0.5, aman);
//			sp.setAlphaF(0.7F);
//
//			sp.setColorFade(0x000000);
//
//			sp.setColor(0x114400 * rand.nextInt(4));
//
//			sp.multipleParticleScaleBy(0.8F);
//
//			aman.addEffect(sp);

			w.addParticle(ParticleTypes.FIREWORK, x, y, z, rand.nextFloat() - 0.5, -0.2 + rand.nextFloat() / 2.0  , rand.nextFloat() - 0.5);

		}
	}
	}

	@SuppressWarnings(value = { "deprecation" })
	@Override
	public void neighborChanged(BlockState state, Level w, BlockPos pos, Block bl, BlockPos nBlock, boolean isMoving)
	{
		if(!canSurvive(state, w, pos) && !w.isClientSide)
		{
			w.destroyBlock(pos, true);
		}
		super.neighborChanged(state, w, pos, bl, nBlock, isMoving);
	}

	@Override
	public void onPlace(BlockState state, Level w, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		neighborChanged(state, w, pos, Blocks.AIR, null, isMoving);
		super.onPlace(state, w, pos, oldState, isMoving);
	}

	@Override
	public void onRemove(BlockState state, Level worldIn, BlockPos pos, BlockState newState, boolean isMoving)
	{
		if(newState.getBlock()!=this)
		{
			if(!worldIn.isClientSide)
			{
				List<LivingEntity> targets = worldIn.getEntitiesOfClass(LivingEntity.class, new AABB(pos.offset(-4, -4, -4), pos.offset(4, 4, 4)));
				for (LivingEntity entity : targets)
				{
					entity.addEffect(new MobEffectInstance(MobEffects.WITHER, 100, 2));
				}
			}
		}
		super.onRemove(state, worldIn, pos, newState, isMoving);
	}

}
