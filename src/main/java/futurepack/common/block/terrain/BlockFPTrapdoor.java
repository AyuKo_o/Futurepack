package futurepack.common.block.terrain;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.BlockHitResult;

public class BlockFPTrapdoor extends TrapDoorBlock 
{

	public BlockFPTrapdoor(Properties properties) 
	{
		super(properties);
	}

	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player player, InteractionHand handIn, BlockHitResult rayTraceResult) 
	{
		state = state.cycle(OPEN);
		worldIn.setBlock(pos, state, 2);
		if (state.getValue(WATERLOGGED)) {
			worldIn.scheduleTick(pos, Fluids.WATER, Fluids.WATER.getTickDelay(worldIn));
		}
		
		this.playSound(player, worldIn, pos, state.getValue(OPEN));
		return InteractionResult.SUCCESS;
	}
}
