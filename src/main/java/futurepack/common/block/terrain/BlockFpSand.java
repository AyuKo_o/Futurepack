package futurepack.common.block.terrain;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SandBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.PlantType;

public class BlockFpSand extends SandBlock
{

	public BlockFpSand(int p_i48338_1_, Block.Properties properties)
	{
		super(p_i48338_1_, properties);
	}


	@Override
	public boolean canSustainPlant(BlockState state, BlockGetter world, BlockPos pos, Direction facing, IPlantable plantable)
	{
		BlockState plant = plantable.getPlant(world, pos.relative(facing));
		net.minecraftforge.common.PlantType type = plantable.getPlantType(world, pos.relative(facing));

		if (plant.getBlock() == Blocks.CACTUS)
		{
			return true;
		}

		if(type == PlantType.DESERT)
			return true;
		else if(type == PlantType.BEACH)
		{
            boolean isBeach = this.asBlock() == Blocks.GRASS_BLOCK ||  this.defaultBlockState().is(BlockTags.DIRT) || this.asBlock() == Blocks.SAND || this.asBlock() == TerrainBlocks.sand_m.get();
            boolean hasWater = (world.getBlockState(pos.east()).getMaterial() == Material.WATER ||
                    world.getBlockState(pos.west()).getMaterial() == Material.WATER ||
                    world.getBlockState(pos.north()).getMaterial() == Material.WATER ||
                    world.getBlockState(pos.south()).getMaterial() == Material.WATER);
            return isBeach && hasWater;
		}
		else
		{
			return super.canSustainPlant(state, world, pos, facing, plantable);
		}
	}
}
