package futurepack.common.block.terrain;

import java.util.Random;

import futurepack.client.particle.ParticleFireflyMoving;
import futurepack.client.particle.ParticleFireflyStill;
import futurepack.world.dimensions.Dimensions;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class BlockFireflies extends Block
{

	public BlockFireflies(Properties properties) 
	{
		super(properties.noCollission().randomTicks().noOcclusion());
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, Level w, BlockPos pos, Random rand)
	{
		float x = pos.getX() + rand.nextFloat();
		float y = pos.getY() + rand.nextFloat();
		float z = pos.getZ() + rand.nextFloat();
		
		ParticleFireflyStill fly = new ParticleFireflyMoving((ClientLevel) w,x,y,z, rand.nextFloat()-0.5, rand.nextFloat()-0.5, rand.nextFloat()-0.5);
		if(Dimensions.TYROS_ID.equals(w.dimension().location())) // has to be id comparison because DimensionID is ot inited if on server
			fly.initRandomColor(0x9900ff, 0x00FFff);
		Minecraft.getInstance().particleEngine.add(fly); 
		
		fly = new ParticleFireflyStill((ClientLevel) w, x, y, z);
		if(Dimensions.TYROS_ID.equals(w.dimension().location()))
			fly.initRandomColor(0x9900ff, 0x00FFff);
		Minecraft.getInstance().particleEngine.add(fly);
		
		super.animateTick(stateIn, w, pos, rand);
	}
	
	@Override
	public RenderShape getRenderShape(BlockState state)
	{
		return RenderShape.INVISIBLE;
	}
	

	@Override
	public void tick(BlockState state, ServerLevel w, BlockPos pos, Random rand)
	{
		if(w.isDay())
		{
			w.removeBlock(pos, false);
			return;
		}
			
		int h = w.getHeightmapPos(Types.MOTION_BLOCKING, pos).getY();
		
		Direction dir = Direction.UP;
		if(pos.getY()-h > 4)
		{
			dir = Direction.DOWN;
		}
		else
		{
			dir = Direction.from3DDataValue(1+rand.nextInt(5));
		}
			
		BlockPos pos2 = pos.relative(dir);
		if(w.isEmptyBlock(pos2))
		{
			w.setBlockAndUpdate(pos2, state);
			w.removeBlock(pos, false);
			w.scheduleTick(pos2, this, 20 * rand.nextInt(30));
		}
	}

}
