package futurepack.common.block.modification;

import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayDeque;

import futurepack.api.PacketBase;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.INetworkUser;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.network.FunkPacketCalcSolution;
import futurepack.common.network.FunkPacketCalculation;
import futurepack.common.network.FunkPacketCalculation.EnumCalculationType;
import futurepack.common.network.NetworkManager;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityModulT1Calculation extends TileEntityModificationBase implements ITileNetwork, INetworkUser, ITilePropertyStorage
{
	public static final int secondsToStore = 60;

	public static final int calcAmount = 20;
	
	private int maxCalculate;
	private int calculated;
	private BlockPos receiver;
	
	private int nextCalculate;
	private BlockPos nextReceiver;

	private byte ticks = 0;
	private ArrayDeque<Byte> graph_power = new ArrayDeque<>(secondsToStore);
	private ArrayDeque<Byte> graph_working = new ArrayDeque<>(secondsToStore);

	//2*secondsToStore * 4 bit / 8 bit = secondsToStore
	private byte[] fin = new byte[secondsToStore];
	private ShortBuffer shortBuf = ByteBuffer.wrap(fin).asShortBuffer();

	public TileEntityModulT1Calculation(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.MODUL_T1_CALC, new InventoryModificationBase()
		{
			@Override
			public int getRamSlots()
			{
				return 6;
			}
			
			@Override
			public int getCoreSlots()
			{			
				return 2;
			}

			@Override
			public int getChipSlots()
			{
				return 3;
			}		
		}, pos, state);
		
		graph_power.addFirst((byte) 0);
		graph_working.addFirst((byte) 0);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		nbt.putInt("maxC", maxCalculate);
		nbt.putInt("calced", calculated);
		
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}

	@Override
	public void updateTile(int ticks)
	{
		if(maxCalculate>0)
		{
			calculated+=4*ticks;
			if(calculated >= maxCalculate)
			{
				NetworkManager.sendPacketThreaded(this, new FunkPacketCalcSolution(worldPosition, this, EnumCalculationType.NEON, calculated, receiver));
				maxCalculate=0;
				calculated=0;
				
				if(nextCalculate > 0)
				{
					maxCalculate = nextCalculate;
					nextCalculate = 0;
					receiver = nextReceiver;
					nextReceiver = null;
				}
			}
		}
	}

	@Override
	public boolean isWorking()
	{
		return maxCalculate > 0;
	}

	@Override
	public boolean isNetworkAble()
	{
		return true;
	}

	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		if(pkt instanceof FunkPacketCalculation)
		{
			if(maxCalculate==0)
			{	
				FunkPacketCalculation fpc = (FunkPacketCalculation) pkt;
				if(fpc.type == EnumCalculationType.NEON)
				{
					receiver = fpc.getSenderPosition();
					maxCalculate = (int) Math.min(calcAmount + super.getPureRam()*5, fpc.toCalculate - fpc.getInProgress());
					fpc.registerCalculationAmount(maxCalculate);
				}
			}
			else if(nextCalculate == 0)
			{
				FunkPacketCalculation fpc = (FunkPacketCalculation) pkt;
				if(fpc.type == EnumCalculationType.NEON)
				{
					nextCalculate = (int) Math.min(calcAmount + super.getPureRam()*5, fpc.toCalculate - fpc.getInProgress());
					nextReceiver = fpc.getSenderPosition();
					fpc.registerCalculationAmount(nextCalculate/2);
				}
			}

		}
	}

	@Override
	public Level getSenderWorld()
	{
		return level;
	}

	@Override
	public BlockPos getSenderPosition()
	{
		return worldPosition;
	}

	@Override
	public void postPacketSend(PacketBase pkt)
	{
		
	}

	@Override
	public void updateNaturally()
	{
		if(!level.isClientSide())
		{
			ticks++;
			if(ticks>=20)
			{
				ticks = 0;
				updateBuffer();
				if(graph_power.size() + 1 > secondsToStore)
					graph_power.removeLast();
				graph_power.addFirst((byte) 0);
				if(graph_working.size() + 1 > secondsToStore)
					graph_working.removeLast();
				graph_working.addFirst((byte) 0);
			}

			if(getDefaultPowerUsage() < energy.get())
			{
				graph_power.addFirst((byte) (1 + graph_power.removeFirst()));
			}
			if(isWorking() && getDefaultPowerUsage() < energy.get())
			{
				graph_working.addFirst((byte) (1 + graph_working.removeFirst()));
			}
		}	
	}

	private void updateBuffer()
	{
		Byte[] power = graph_power.toArray(new Byte[graph_power.size()]);
		Byte[] working = graph_working.toArray(new Byte[graph_working.size()]);

		for(int i=0;i<fin.length;i++)
		{
			byte bpower = i < power.length ? power[i] : 0;
			byte bworikng = i < working.length ? working[i] : 0;

			byte b1_power = (byte)(bpower/20F * 15F);
			byte b2_working = (byte)(bworikng/20F * 15F);

			b1_power &= 15;
			b2_working &= 15;

			fin[i] = (byte) ((b2_working<<4) | b1_power);
		}
	}

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return energy.get();
		default:
			return shortBuf.get(id-1);
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			int d = value - energy.get();
			if(d > 0)
				energy.add(d);
			else
				energy.use(-d);
			break;
		default:
			shortBuf.put(id-1, (short)value);
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 1 + shortBuf.limit();
	}
	
	/**
	 * if no power is available 
	 * 
	 * @return a byte array
	 */
	public double[] getPowerGraph()
	{
		double[] graph = new double[fin.length];
		for(int i=0;i<fin.length;i++)
		{
			graph[i] = (fin[i] & 15) / 15.0;
		}
		return graph;
	}
	
	public double[] getWorkingGraph()
	{
		double[] graph = new double[fin.length];
		for(int i=0;i<fin.length;i++)
		{
			graph[i] = ((fin[i]>>4) & 15) / 15.0;
		}
		return graph;
	}

}
