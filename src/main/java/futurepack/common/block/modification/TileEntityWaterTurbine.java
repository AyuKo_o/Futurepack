package futurepack.common.block.modification;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.FPTileEntitys;
import futurepack.common.modification.EnumChipType;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;

public class TileEntityWaterTurbine extends TileEntityModificationBase
{
	private int powerCount;
	
	
	public TileEntityWaterTurbine(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.WATER_TURBINE, pos, state);
	}
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.PRODUCE;
	}

	@Override
	public void updateTile(int ticks)
	{
		powerCount = 0;
		for(int i=1;i<256-worldPosition.getY();i++)
		{
			FluidState state = level.getFluidState(worldPosition.above(i));
			if(state.getType() == Fluids.WATER)
			{
				powerCount+=2;
			}
			else if(state.getType() == Fluids.FLOWING_WATER)
			{
				powerCount++;
			}
			else
			{
				break;
			}
		}
		if(powerCount>0)
		{
			BlockState down = level.getBlockState(worldPosition.below());
			FluidState fluid = level.getFluidState(worldPosition.below());
			if(down.getBlock()== Blocks.AIR && (fluid.isEmpty() || !fluid.isSource()))
			{
				//best claculation we found
				powerCount = (int) ((powerCount) * (1.0f + getChipPower(EnumChipType.INDUSTRIE) * 0.15f));
				energy.add(powerCount*ticks);
				
				level.setBlockAndUpdate(worldPosition.below(),Fluids.FLOWING_WATER.defaultFluidState().createLegacyBlock());
			}
		}
	}

	@Override
	public boolean isWorking()
	{
		return true;
	}
	
	@Override
	public int getDefaultPowerUsage() 
	{
		return powerCount;
	}
}
