package futurepack.common.block.modification;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IFluidTankInfo.FluidTankInfo;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.inventory.ItemHandlerGuiOverride;
import futurepack.common.block.inventory.ItemStackHandlerGuis;
import futurepack.common.block.logistic.TileEntityFluidTube;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperFluid;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.DirectionalBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidAttributes;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.IFluidTank;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistry;

public class TileEntityFluidPump extends TileEntityModificationBase implements ITilePropertyStorage
{

	private FluidTank tank = new FluidTank(FluidAttributes.BUCKET_VOLUME * 4, this::canFillFluidType);
	private ItemContainer handler = new ItemContainer();
	private LazyOptional<ItemContainer> itemOpt;
	private LazyOptional<FluidTank> tankOpt;
	
	public TileEntityFluidPump(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.FLUID_PUMP, pos, state);
	}
	
	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY && this.on)
		{
			if(facing == getInput() || facing == getOutput())
			{
				if(tankOpt == null)
				{
					tankOpt = LazyOptional.of(() -> tank);
					tankOpt.addListener(p -> tankOpt = null);
				}
				return (LazyOptional<T>) tankOpt;
			}
		}
		else if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(facing != getInput() && facing != getOutput())
			{
				if(itemOpt==null)
				{
					itemOpt = LazyOptional.of(() -> handler);
					itemOpt.addListener(p -> itemOpt = null);
				}
				return (LazyOptional<T>) itemOpt;
			}
		}
		
		return super.getCapability(capability, facing);
	}
	

	@Override
	public void updateTile(int ticks)
	{
		if(level.isClientSide)
			return;
		
		pumpOutFluid(ticks);
		
		HelperFluid.putFluidFromBucketInTank(handler, 0, 1, tank, tank.getCapacity() - tank.getFluidAmount());
		
		HelperFluid.putFluidFromTankInBucket(handler, 3, 4, tank, tank.getFluidAmount());
		
		pumpInFluid();
	}

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		CompoundTag tank = new CompoundTag();
		this.tank.writeToNBT(tank);
		nbt.put("tank", tank);
		CompoundTag items = handler.serializeNBT();
		nbt.put("items", items);
		return super.writeDataUnsynced(nbt);
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		CompoundTag tank = nbt.getCompound("tank");
		this.tank.readFromNBT(tank);
		handler.deserializeNBT(nbt.getCompound("items"));
		super.readDataUnsynced(nbt);
	}
	
	private LazyOptional<IFluidHandler> pumpOutTarget;
	
	private void pumpOutFluid(int ticks)
	{
		if(tank.getFluidAmount() <= 0)
		{
			return;
		}
		
		int amount = getAmount() * ticks;
		if(this.tank.getFluidAmount() >= this.tank.getCapacity() * 0.75)
		{
			amount += 20 * ticks;
		}
		final int amountF = amount;
		
		if(pumpOutTarget == null)
		{
			Direction out = getOutput();
			BlockPos tankPos = this.worldPosition.relative(out);
			BlockEntity tile = level.getBlockEntity(tankPos);
			if(tile!=null)
			{	
				pumpOutTarget = tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, out.getOpposite());
				if(pumpOutTarget.isPresent())
				{
					pumpOutTarget.addListener(p -> pumpOutTarget = null);
				}
				else
				{
					pumpOutTarget = null;
					return;
				}
			}
		}
		if(pumpOutTarget!=null)
		{
			pumpOutTarget.ifPresent(tankOut -> 
			{
				FluidStack fluid = FluidUtil.tryFluidTransfer(tankOut, this.tank, amountF, true);
				if(fluid!=null)
				{
					energy.use((int) (fluid.getAmount() * 0.1));
				}
			});
		}
		
	}
	
	private List<LazyOptional<IFluidHandler>> pumpInTarget;
	
	private void pumpInFluid()
	{
		if(tank.getFluidAmount() >= tank.getCapacity())
		{
			return;
		}
		int amount = getAmount();
		if(this.tank.getFluidAmount() <= this.tank.getCapacity() * 0.5)
		{
			amount += 10;
		}
		
		if(pumpInTarget != null)
		{
			Iterator<LazyOptional<IFluidHandler>> iter = pumpInTarget.iterator();
			while(iter.hasNext())
			{
				LazyOptional<IFluidHandler> pumpIn = iter.next();
				if(pumpIn.isPresent())
				{
					FluidUtil.tryFluidTransfer(this.tank, pumpIn.orElseThrow(() -> new NullPointerException("it returned it was not null!")), amount, true);
				}
				else
				{
					iter.remove();
				}
			}
			
			if(pumpInTarget.isEmpty())
				pumpInTarget = null;
			
			return;
		}
		
		Direction in = getInput();
		BlockPos tankPos = this.worldPosition.relative(in);
		BlockEntity tile = level.getBlockEntity(tankPos);
		if(tile!=null)
		{
			if(tile instanceof TileEntityFluidTube)
			{
				List<IFluidHandler> list = ((TileEntityFluidTube)tile).getConnectedTanks(this.worldPosition);
				for(IFluidHandler tankIn : list)
				{
					FluidUtil.tryFluidTransfer(this.tank, tankIn, amount, true);
					
					if(this.tank.getFluidAmount() >= this.tank.getCapacity())
					{
						return;
					}
				}
			}
			else
			{
				if(pumpInTarget==null)
					pumpInTarget = new ArrayList<LazyOptional<IFluidHandler>>(8);
					
				LazyOptional<IFluidHandler> pumpIn = tile.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, in.getOpposite());
				if(pumpIn.isPresent())
				{
					if(!pumpInTarget.contains(pumpIn))
					{
						pumpIn.addListener(pumpInTarget::remove);
						pumpInTarget.add(pumpIn);
					}
				}
				else
				{
					return;
				}
			}				
			
		}
	}
	
	@Override
	public boolean isWorking()
	{
		return this.tank.getFluidAmount() > 0 && this.tank.getFluidAmount() < this.tank.getCapacity();
	}
	
	private Direction getOutput()
	{
		BlockState state = getBlockState();
		if(state.getBlock() == ModifiableBlocks.fluid_pump)
		{
			return state.getValue(DirectionalBlock.FACING);
		}
		return Direction.UP;
	}
	
	private Direction getInput()
	{
		return getOutput().getOpposite();
	}
	
	private int getAmount()
	{
		return (int) (50 + (25 * getChipPower(EnumChipType.TRANSPORT)));
	}
	
	private class ItemContainer extends ItemStackHandlerGuis
	{
		/* 0 - Full Bucket Input
		 * 1 - Empty Bucket Output
		 * 2 - Fluid Filter
		 * 3 - Empty Bucket Input
		 * 4 - Full Bucket Output
		 */
		public ItemContainer()
		{
			super(5);
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(isItemValid(slot, stack, gui))		
				return super.insertItem(slot, stack, simulate, gui);
			
			return stack;
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack, boolean gui)
		{
			if(gui && slot==2)
				return (FluidUtil.getFluidHandler(stack).orElse(null) != null);
			
			//only full with matching fluid
			if(slot == 0)
			{
				LazyOptional<IFluidHandlerItem> fh = FluidUtil.getFluidHandler(stack);			
				FluidStack fs = fh.map(p->p.getFluidInTank(0)).orElse(null);
					
				if(fs != null && !fs.equals(FluidStack.EMPTY) && canFillFluidType(fs))
					return true;		
			}
				
			//only empty
			if(slot == 3)
			{
				LazyOptional<IFluidHandlerItem> fh = FluidUtil.getFluidHandler(stack);	
				FluidStack fs = fh.map(p->p.getFluidInTank(0)).orElse(null);
					
				if(fs != null && fs.equals(FluidStack.EMPTY))
					return true;		
			}

			return false;
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(slot==1 || slot==4 || gui)
				return super.extractItem(slot, amount, simulate, gui);
			return ItemStack.EMPTY;
		}
		
		@Override
		public int getSlotLimit(int slot)
		{
			return 1;
		}
	}

	public boolean canFillFluidType(FluidStack fluid)
	{
		if(!handler.getStackInSlot(2).isEmpty())
		{
			FluidStack filter = FluidUtil.getFluidContained(handler.getStackInSlot(2)).orElse(null);
			if(filter!=null)
			{
				if(!filter.equals(fluid)) 
					return false;
			}
		}
		return true;
	}
	
	
	public IItemHandler getGui()
	{
		return new ItemHandlerGuiOverride(handler);
	}

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return getCurrentFluidId();
		case 1:
			return tank.getFluidAmount();
		case 2:
			return energy.get();
		default:
			return 0;
		}	
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			Fluid fl = getSendFluid(value);
			if(fl!=null)
				tank.setFluid(new FluidStack(fl, Math.max(1, tank.getFluidAmount())));
			break;
		case 1:
			if(tank.getFluid()!=null && !tank.getFluid().isEmpty())
			{
				tank.getFluid().setAmount(value);
			}
			break;
		case 2:
			setEnergy(value);
		default:
			break;
		}
	}

	private int getCurrentFluidId(IFluidTank tank)
	{
		if(tank.getFluid()==null)
			return -1;
		
		ForgeRegistry<Fluid> reg = (ForgeRegistry<Fluid>) ForgeRegistries.FLUIDS;
		Fluid fl = tank.getFluid().getFluid();
		return reg.getID(fl);
	}
	
	public static Fluid getSendFluid(int id)
	{
		if(id==-1)
			return null;
		
		ForgeRegistry<Fluid> reg = (ForgeRegistry<Fluid>) ForgeRegistries.FLUIDS;
		return reg.getValue(id);
	}
	
	private int getCurrentFluidId()
	{
		return getCurrentFluidId(tank);
	}
	
	@Override
	public int getPropertyCount()
	{
		return 3;
	}

	public FluidTankInfo getFluid()
	{
		return new FluidTankInfo(tank, 0);
	}
}
