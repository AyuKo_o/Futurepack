package futurepack.common.block.modification.machines;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.Nullable;

import futurepack.common.item.misc.MiscItems;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class BlueprintAssemblyRecipe
{
	private final UUID owner;
	private final String recipeID;
	private final List<AssemblyRecipe> recipes;
	private String output;
	
	public BlueprintAssemblyRecipe(Player owner, AssemblyRecipe used)
	{
		this(owner.getGameProfile().getId(), used.id, used.getOutput().getHoverName().getString());
	}
	
	public BlueprintAssemblyRecipe(UUID owner, String recipeID, String itemName)
	{
		this.owner = owner;
		this.recipeID = recipeID;
		this.output = itemName;
		recipes = new ArrayList<AssemblyRecipe>(Arrays.asList(FPAssemblyManager.instance.getMatchingRecipes(recipeID)));
	}
	
	public ItemStack createRecipeItem()
	{
		ItemStack it = new ItemStack(MiscItems.assembly_recipe,1);
		CompoundTag nbt = it.getOrCreateTagElement("assemblyRecipe");
		nbt.putString("recipeID", recipeID);
		nbt.putLong("uu", owner.getMostSignificantBits());
		nbt.putLong("id", owner.getLeastSignificantBits());
		nbt.putString("output", output);
		return it;
	}
	
	@Nullable
	public static BlueprintAssemblyRecipe createRecipeFromItem(ItemStack recipe)
	{
		if(isBlueprint(recipe))
		{
			CompoundTag nbt = recipe.getTagElement("assemblyRecipe");
			UUID owner = new UUID(nbt.getLong("uu"), nbt.getLong("id"));
			String recipeID = nbt.getString("recipeID");
			String itemName = nbt.getString("owner");
			return new BlueprintAssemblyRecipe(owner, recipeID, itemName);
		}
		return null;
	}

	@Nullable
	public AssemblyRecipe getWorkingRecipe(ItemStack[] input, Level w)
	{
		CustomPlayerData data = CustomPlayerData.getDataFromUUID(owner, w.getServer());
		
		for(int i=0;i<recipes.size();i++)
		{
			if(data.canProduce(recipes.get(i).getOutput()))
			{
				if(recipes.get(i).matches(input))
				{
					return recipes.get(i);
				}
			}
			else
			{
				recipes.remove(i--);
			}
		}
		return null;
	}
	
	public List<AssemblyRecipe> getAllRecipes()
	{
		return recipes;
	}

	public static boolean isBlueprint(ItemStack item)
	{
		if(item.getItem() == MiscItems.assembly_recipe)
		{
			CompoundTag nbt = item.getTagElement("assemblyRecipe");
			return nbt!=null;
		}
		return false;	
	}
	
	
}
