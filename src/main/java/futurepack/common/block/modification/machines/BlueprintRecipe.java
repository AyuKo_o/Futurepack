package futurepack.common.block.modification.machines;

import java.util.UUID;

import futurepack.common.item.misc.MiscItems;
import futurepack.common.recipes.crafting.InventoryCraftingForResearch;
import futurepack.depend.api.helper.HelperInventory;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.inventory.MenuType;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;

public class BlueprintRecipe
{
	private NonNullList<ItemStack> items = NonNullList.withSize(9, ItemStack.EMPTY);
	private String name;
	private Recipe<CraftingContainer> rec;
	private UUID owner;
	private Level w;
	
	public BlueprintRecipe(NonNullList<ItemStack> items, String name, UUID owner, Level w)
	{
		super();
		this.items = items;
		this.name = name;
		this.owner = owner;
		this.w = w;
	}

	public Recipe<CraftingContainer> getRecipe()
	{
		if(rec!=null)
			return rec;
		
		CraftingContainer inv = getCrafting();
		rec = w.getRecipeManager().getRecipeFor(RecipeType.CRAFTING, inv, w).orElse(null);
		return rec; //cashing the recipe
	}
	
	private CraftingContainer getCrafting()
	{
		return getCraftingTable(items, 0);
	}
	
	public boolean isItemValid(ItemStack it, int slot)
	{
		if(getRecipe()!=null)
		{
			CraftingContainer orginal = getCrafting();
			if(rec.matches(orginal, w))
			{
				ItemStack valid = rec.assemble(orginal);
				
				CraftingContainer test = getCrafting();
				test.setItem(slot, it);
				if(rec.matches(test, w))
				{
					ItemStack result = rec.assemble(test);
					return HelperInventory.areItemsEqualNoSize(valid, result);
				}
			}			
		}
		return false;
	}
	
	public CraftingContainer getCraftingTable(NonNullList<ItemStack> items, int start)
	{
		CraftingContainer inv = new InventoryCraftingForResearch(new AbstractContainerMenu(MenuType.CRAFTING, start)
		{	
			@Override
			public boolean stillValid(Player playerIn)
			{
				return true;
			}
		}, 3, 3, owner);
		for(int i=0;i<inv.getContainerSize();i++)
		{
			inv.setItem(i, items.get(i+start));
		}
		return inv;
	}
	
	public void addRecipeToItem(ItemStack item)
	{
		if(!item.hasTag())
			item.setTag(new CompoundTag());
		
		CompoundTag nbt = new CompoundTag();
		ListTag list = new ListTag();
		for(int i=0;i<items.size();i++)
		{
			if(!items.get(i).isEmpty())
			{
				CompoundTag tag = new CompoundTag();
				items.get(i).save(tag);
				tag.putInt("Slot", i);
				list.add(tag);
			}
		}
		nbt.put("items", list);
		nbt.putString("output", name);
		nbt.putLong("uu", owner.getMostSignificantBits());
		nbt.putLong("id", owner.getLeastSignificantBits());
		
		item.getTag().put("recipe", nbt);
	}
	
	public static BlueprintRecipe createFromItem(ItemStack it, Level w)
	{
		if(it.getItem() == MiscItems.crafting_recipe)
		{
			if(it.getTagElement("recipe") != null)
			{
				CompoundTag nbt = it.getTagElement("recipe");
				NonNullList<ItemStack> items = NonNullList.withSize(9, ItemStack.EMPTY);
				ListTag list = nbt.getList("items", 10);
				for(int i=0;i<list.size();i++)
				{
					CompoundTag tag = list.getCompound(i);
					ItemStack is = ItemStack.of(tag);
					items.set(tag.getInt("Slot"), is);
				}
				String name = nbt.getString("output");
				UUID owner = new UUID(nbt.getLong("uu"), nbt.getLong("id"));
				BlueprintRecipe rec = new BlueprintRecipe(items, name, owner, w);
				return rec;
			}
		}
		return null;
	}
}
