package futurepack.common.block.modification.machines;

import java.util.List;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IItemWithRandom;
import futurepack.common.FPTileEntitys;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import net.minecraft.core.BlockPos;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityOptiAssembler extends TileEntityOptiAssemblerBase
{
	public static final int FIELD_PROGRESS = 2;
	
	private ItemStack[] recipeItems;
	private BlueprintAssemblyRecipe[] recipes;
	private boolean wait = false;
	
	
	public TileEntityOptiAssembler(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.OPTI_ASSEMBLER, pos, state);
		recipeItems = new ItemStack[3];
		recipes = new BlueprintAssemblyRecipe[3];
	}
	
	@Override
	public void updateTile(int ticks)
	{
		if(level.isClientSide)
			return;
		
		wait = false;
		int progress = this.progress % 20;
		int state = this.progress / 20;
		
		do
		{
			BlueprintAssemblyRecipe recipe = getRecipe(state);
			if(recipe != null)
			{
				ItemStack[] input = new ItemStack[]{getItem(state*3), getItem(state*3 +1), getItem(state*3 +2)};
				AssemblyRecipe rec = recipe.getWorkingRecipe(input, level);
				if(rec!=null)
				{
					progress+= ticks;
					ticks = 0;
					if(progress >= 20)
					{
						if(getItem(12).isEmpty())
						{
							if(rec.matches(input))
							{
								ItemStack output =  rec.getOutput(input);
								rec.useItems(input);
								setItem(state*3  , input[0]);
								setItem(state*3+1, input[1]);
								setItem(state*3+2, input[2]);
								
								setItem(12, output);
								
								if(output.getItem() instanceof IItemWithRandom)
								{
									int pr = energy.use(5);
									int air =  support.use(5);
									int r = 0;
									if(pr + air > 0)
										r = level.random.nextInt(pr + air);
									IItemWithRandom rand = (IItemWithRandom) output.getItem();
									rand.setRandomNBT(output, r);
								}
								
								insertIntoInput(output);
							}
							
							state++;
							progress -= 20;
						}
						else
						{
							progress = 19;
							wait = true;
						}
						
					}
				}
				else
				{
					state++;
					progress = 0;
				}
			}
			else
			{
				state++;
				progress = 0;
			}
		}
		while(progress >= 20);
		
		this.progress = (state%3)*20 + (progress%20);
	}

	private void insertIntoInput(ItemStack output)
	{
		for(int i=0;i<9;i++)
		{
			ItemStack slot = getItem(i);
			if(slot.isEmpty())
			{
				if(canPlaceItem(i, output))
				{
					setItem(i, output);
					setItem(12, ItemStack.EMPTY);
					return;
				}
			}
			else
			{
				if(ItemStack.matches(slot, output))
				{
					int need = slot.getMaxStackSize() - slot.getCount();
					need = Math.min(need, output.getCount());
					slot.grow(need);
					output.shrink(need);
					return;
				}
			}
		}
	}
	
	@Override
	public boolean isWorking()
	{
		if(level.isClientSide)
			if(!getItem(12).isEmpty())
				return false;
		
		if(wait)
		{
			return false;
		}
		return this.progress % 20 > 0;
	}
	
	@Override
	public boolean canPlaceItem(int slot, ItemStack item)
	{
		if(slot < 9)
		{
			int index = slot / 3;
			BlueprintAssemblyRecipe recipe = getRecipe(index);
			if(recipe != null)
			{
				List<AssemblyRecipe> list = recipe.getAllRecipes();
				for(AssemblyRecipe rec : list)
				{
					ItemPredicateBase input = rec.getInput()[slot % 3];
					if(!input.apply(item, true))
						continue;
					
					return true;
				}
			}
			else
			{
				return false;
			}
		}
		else if(slot < 12)
		{
			return BlueprintAssemblyRecipe.isBlueprint(item);
		}
		return false;
	}

	
	private BlueprintAssemblyRecipe getRecipe(int index)
	{
		if(index<0 || index > 2)
			throw new IllegalArgumentException("Index must be in range [0,2]");
		
		if(recipes[index]!= null && recipeItems[index] == getItem(9 + index))
		{
			return recipes[index];
		}
		else
		{
			recipeItems[index] = getItem(9+index);
			recipes[index] = BlueprintAssemblyRecipe.createRecipeFromItem(recipeItems[index]);
			return recipes[index];
		}
	}
}
