package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.modification.PartsImprover;
import futurepack.common.modification.PartsManager;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityImproveComponents extends TileEntityMachineBase implements ITilePropertyStorage
{
	private int progress = 0;
	private int componentB = -1, componentW = -1;
	private int scanningB = -1, scanningW = -1;
	public final int maxScanning = 20 * 10;
	public final int maxProgress = 70 * 4 + (20 * 4 + 5 *4);

	public TileEntityImproveComponents(BlockEntityType<? extends TileEntityMachineBase> type, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
	}

	public TileEntityImproveComponents(BlockPos pos, BlockState state)
	{
		this(FPTileEntitys.IMPROVE_COMPONENTS, pos, state);
	}

	@Override
	protected int getInventorySize()
	{
		return 6;
	}

	@Override
	public void configureLogisticStorage(LogisticStorage storage)
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);

		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
	}

	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}

	@Override
	public void writeDataSynced(CompoundTag nbt)
	{
		super.writeDataSynced(nbt);
		nbt.putInt("progress", getProgress());
		nbt.putInt("componentB", getComponentB());
		nbt.putInt("componentW", getComponentW());
		nbt.putInt("scanningB", getScanningB());
		nbt.putInt("scanningW", getScanningW());
	}

	@Override
	public void readDataSynced(CompoundTag nbt)
	{
		progress = nbt.getInt("progress");
		componentB = nbt.getInt("componentW");
		componentW = nbt.getInt("componentB");
		scanningB = nbt.getInt("scanningB");
		scanningW = nbt.getInt("scanningW");

		super.readDataSynced(nbt);
	}


	@Override
	public void updateTile(int tickCount)
	{
		if(level.isClientSide())
			return;

		ItemStack better = getItem(2);
		ItemStack worse = getItem(3);

		boolean reset = true;
		if(!better.isEmpty() && !worse.isEmpty())
		{
			if(getScanningB() >= 0 && getScanningB() < maxScanning)
			{
				if(!PartsImprover.isPartAnalyzed(better))
				{
					reset = false;
					scanningB = getScanningB() + tickCount;
					if(getScanningB() >= maxScanning)
					{
						PartsImprover.analyzePart(better, level.random);
					}
				}
				else
				{
					scanningB = maxScanning;
				}
			}
			if(getScanningW() >= 0 && getScanningW() < maxScanning)
			{
				if(!PartsImprover.isPartAnalyzed(worse))
				{
					reset = false;
					scanningW = getScanningW() + tickCount;
					if(getScanningW() >= maxScanning)
					{
						PartsImprover.analyzePart(worse, level.random);
					}
				}
				else
				{
					scanningW = maxScanning;
				}
			}

			if(getComponentB() >= 0 && getComponentW() >= 0)
			{
				if(PartsImprover.isPartAnalyzed(better) && PartsImprover.isPartAnalyzed(worse))
				{
					CompoundTag nbtB = better.getTagElement("analyzed_part");
					CompoundTag nbtW = worse.getTagElement("analyzed_part");

					float b = nbtB.getFloat("p" + getComponentB());
					float w = nbtW.getFloat("p" + getComponentW());

					if(b < w)
					{
						reset = false;
						progress = getProgress() + tickCount;
						if(getProgress() >= maxProgress)
						{
							//switch the part values

							nbtB.putFloat("p" + getComponentB(), w);
							nbtW.putFloat("p" + getComponentW(), b);

							PartsImprover.recalculateValues(better);
							PartsImprover.recalculateValues(worse);

							reset = true;
						}
					}
				}
			}
			else if(isComponentBScanned() && isComponentWScanned())
			{
				reset = true;
			}
		}

		if(reset)
		{
			progress = 0;
			componentB = -1;
			componentW = -1;
			scanningB = -1;
			scanningW = -1;

			boolean flagA = better.isEmpty() && !getItem(0).isEmpty();
			boolean flagB = worse.isEmpty() && !getItem(1).isEmpty();

			if(flagA && flagB)
			{
				if(getItem(0).getItem() == getItem(1).getItem())
				{
					PartsImprover.EnumPartConfig configA = PartsImprover.EnumPartConfig.getPartType(PartsManager.getPartFromItem(getItem(0)));
					PartsImprover.EnumPartConfig configB = PartsImprover.EnumPartConfig.getPartType(PartsManager.getPartFromItem(getItem(1)));

					if(configA == configB & configA!=null)
					{
						ItemStack newItem;
						newItem = getItem(0);
						better = newItem;
						setItem(2, better);
						setItem(0, ItemStack.EMPTY);

						newItem = getItem(1);
						worse = newItem;
						setItem(3, worse);
						setItem(1, ItemStack.EMPTY);
					}
				}
			}

			if(!better.isEmpty() && !worse.isEmpty())
			{
				if(PartsImprover.isPartAnalyzed(better) && PartsImprover.isPartAnalyzed(worse))
				{
					scanningB = scanningW = maxScanning;

					float[] valuesB = PartsImprover.analyzePart(better, level.getRandom());
					float[] valuesW = PartsImprover.analyzePart(worse, level.getRandom());

					float b = valuesB[0];
					float w = valuesW[0];
					int comB=0,comW=0;

					for(int i=0;i<valuesB.length;i++)
					{
						if(valuesB[i] < b)
						{
							b = valuesB[comB = i];
						}
					}
					for(int i=0;i<valuesW.length;i++)
					{
						if(valuesW[i] > w)
						{
							w = valuesW[comW = i];
						}
					}

					if(b < w)
					{
						componentB = comB;
						componentW = comW;
					}
					else if(getItem(4).isEmpty() && getItem(5).isEmpty())
					{
						setItem(4, better);
						setItem(5, worse);

						better = ItemStack.EMPTY;
						setItem(2, ItemStack.EMPTY);
						worse = ItemStack.EMPTY;
						setItem(3, ItemStack.EMPTY);
					}
				}
				else
				{
					scanningB=0;
					scanningW=0;
				}
			}
		}
	}

	@Override
	public boolean canPlaceItem(int slot, ItemStack stack)
	{
		if(slot == 0 || slot == 1)
		{
			return getItem(slot).isEmpty();
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		if(slot == 2 || slot == 3 || slot == 0 || slot == 1)
			return false;

		return super.canTakeItemThroughFace(slot, var2, side);
	}

	@Override
	public int getMaxStackSize()
	{
		return 1;
	}

	@Override
	public boolean isWorking()
	{
		return getProgress() > 0;
	}

	@Override
	public int getDefaultPowerUsage()
	{
		int power = super.getDefaultPowerUsage();
		if(progress >0)
		{
			power += 10;
		}
		return power;
	}

	@Override
	public int getProperty(int id)
	{
		switch(id)
		{
		case 0:
			return energy.get();
		case 1:
			return getProgress();
		case 2:
			return getComponentB();
		case 3:
			return getComponentW();
		case 4:
			return getScanningB();
		case 5:
			return getScanningW();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			setEnergy(value);
			break;
		case 1:
			progress = value;
			break;
		case 2:
			componentB = value;
			break;
		case 3:
			componentW = value;
			break;
		case 4:
			scanningB = value;
			break;
		case 5:
			scanningW = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 6;
	}

	public int getComponentB() {
		return componentB;
	}

	public int getComponentW() {
		return componentW;
	}

	public int getProgress() {
		return progress;
	}

	public boolean isComponentBScanned()
	{
		return getScanningB() >= maxScanning;
	}

	public boolean isComponentWScanned()
	{
		return getScanningW() >= maxScanning;
	}

	public int getScanningB() {
		return scanningB;
	}

	public int getScanningW() {
		return scanningW;
	}

	@Override
	public int getMaxNE()
	{
		return 10000;
	}
}
