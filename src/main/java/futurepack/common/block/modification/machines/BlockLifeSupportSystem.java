package futurepack.common.block.modification.machines;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockLifeSupportSystem extends BlockRotateable implements IBlockBothSidesTickingEntity<TileEntityLifeSupportSystem>
{

	public BlockLifeSupportSystem(Properties props) 
	{
		super(props);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.LIFE_SUPPORT_SYSTEM.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public BlockEntityType<TileEntityLifeSupportSystem> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.LIFE_SUPPORT_SYSTEM;
	}

}
