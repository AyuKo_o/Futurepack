package futurepack.common.block.modification.machines;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IItemSupport;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.modification.EnumChipType;
import futurepack.common.recipes.crushing.CrushingRecipe;
import futurepack.common.recipes.crushing.FPCrushingManager;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityCrusher extends TileEntityMachineSupport implements WorldlyContainer, ITilePropertyStorage
{
	public static final int FIELD_PROGRESS = 0;
	
	private int progress =0;
	private CrushingRecipe last;
	
	public TileEntityCrusher(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.CRUSHER, 1024, EnumEnergyMode.PRODUCE, pos, state);

	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ITEMS);
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.OUT, EnumLogisticType.SUPPORT);
		
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.IN, EnumLogisticType.SUPPORT);
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS, EnumLogisticType.SUPPORT};
	}
	
	@Override
	public void updateTile(int tickCount)
	{
		if(!level.isClientSide)
		{
			if(!items.get(0).isEmpty())
			{
				do
				{
					CrushingRecipe rec = FPCrushingManager.instance.getRecipe(items.get(0));
					if(rec!=null && canWork(rec))
					{
						if(last!=rec)
						{
							progress = 0;
						}			
						progress += tickCount;
						tickCount = 0;
						if(progress>=10)
						{
							progress-=10;
							ItemStack out = rec.getOutput();
							if(items.get(1).isEmpty())
							{
								items.set(1, out);							
							}
							else if(items.get(1).sameItem(out))
							{
								int maxsize = items.get(1).getMaxStackSize();
								if(maxsize >= items.get(1).getCount() + out.getCount())
								{
									items.get(1).grow(out.getCount());
								}
							}
							items.get(0).shrink(rec.getInput().getMinimalItemCount(items.get(0)));
							
							if(support.get() < support.getMax())
								support.add(1);
							
						}
						
					}
					else
					{
						progress=0;
					}			
					last=rec;
					if(items.get(0).getCount()<=0)
						items.set(0,  ItemStack.EMPTY);
				}
				while(progress >= 10);
			}
			else
			{
				progress = 0;
			}
			if(!items.get(2).isEmpty() && support.get()>0)
			{
				if(items.get(2).getItem() instanceof IItemSupport)
				{
					IItemSupport support = (IItemSupport) items.get(2).getItem();
					if(support.getSupport(items.get(2)) < support.getMaxSupport(items.get(2)))
					{
						support.addSupport(items.get(2), 1);
						this.support.use(1);
					}
				}
			}		
			if(support.get()>0 && this.getChipPower(EnumChipType.SUPPORT )>0)
			{
				HelperEnergyTransfer.sendSupportPoints(this);
			}
		}
	}
	
	private boolean canWork(CrushingRecipe rec)
	{
		ItemStack out = rec.getOutput();
		if(items.get(1).isEmpty())
		{
			return true;
		}
		else if(items.get(1).sameItem(out))
		{
			int maxsize = items.get(1).getMaxStackSize();
			if(maxsize >= items.get(1).getCount() + out.getCount())
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public void writeDataSynced(CompoundTag nbt)
	{
		super.writeDataSynced(nbt);
		nbt.putInt("progress", progress);
	}
	
	@Override
	public void readDataSynced(CompoundTag nbt)
	{
		super.readDataSynced(nbt);
		progress = nbt.getInt("progress");
	}

	@Override
	public boolean canPlaceItem(int slot, ItemStack it) 
	{
		if(slot == 0)
		{
			return FPCrushingManager.instance.getRecipe(it) != null;
		}
		if(slot == 2)
		{
			return it.getItem() instanceof IItemSupport;
		}
		return false;
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1};
	}
	
	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{		
		return slot==1 && super.canTakeItemThroughFace(slot, var2, side);
	}

	@Override
	public int getDefaultPowerUsage() 
	{
		return super.getDefaultPowerUsage() + 10;
	}
	
	@Override
	public boolean isWorking() 
	{
		return progress>0;
	}
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.USE;
	}
	
	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return progress;
		case 1:
			return energy.get();
		case 2:
			return support.get();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			progress = value;
			break;
		case 1:
			setEnergy(value);
			break;
		case 2:
			support.set(value);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 3;
	}
	
	@Override
	public void clearContent() { }

	@Override
	protected int getInventorySize()
	{
		return 3;
	}
}
