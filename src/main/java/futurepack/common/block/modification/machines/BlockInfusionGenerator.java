package futurepack.common.block.modification.machines;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FurnaceBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockInfusionGenerator extends BlockRotateable implements IBlockBothSidesTickingEntity<TileEntityInfusionGenerator>
{
	public static final BooleanProperty LIT = FurnaceBlock.LIT;
	
	public BlockInfusionGenerator(Block.Properties props)
	{
		super(props);
	}	
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(LIT);
	}

	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.INFUSION_GENERATOR.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	public BlockEntityType<TileEntityInfusionGenerator> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.PLASMA_GENERATOR;
	}
}
