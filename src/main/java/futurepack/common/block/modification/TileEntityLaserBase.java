package futurepack.common.block.modification;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.WeakHashMap;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileWithOwner;
import futurepack.common.FuturepackMain;
import futurepack.common.block.ItemMoveTicker;
import futurepack.common.modification.EnumChipType;
import futurepack.common.player.FakePlayerFactory;
import futurepack.common.player.FuturepackPlayer;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageEntityForEater;
import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.ClipContext;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.PacketDistributor.PacketTarget;

/**
 * @param <T> is the Entity for the predicate
 */
public abstract class  TileEntityLaserBase<T extends LivingEntity> extends TileEntityModificationBase implements ITileWithOwner, ITilePropertyStorage
{
	private WeakHashMap<Player, Long> warnedPlayers = new WeakHashMap<Player, Long>();
	
	private UUID owner;
	
	public T src;
	public Vec3 entityPos;
	public Vec3 blockPos;
	
	private Class<T> clazz;
	
	public int pass;
	protected int entityTries = 0;
	
	private HashMap<String, Boolean> config = new HashMap<String, Boolean>();
	
	//Only Client
	public long lastMessageTime = 0;
	
	public TileEntityLaserBase(BlockEntityType<? extends TileEntityLaserBase<T>> type, Class<T> cls, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
		clazz = cls;
		
		config.put("attack.player", false);
		config.put("attack.neutral", false);
		config.put("attack.mobs", true);
		config.put("kill.not", false);//dont work anyway
		
		config.put("player.warn", true);
		
		config.put("player.distance", false);
		
		config.put("wait.10", false);
		config.put("wait.15", false);
		config.put("wait.30", false);
		config.put("wait.60", false);

		config.put("has.ai", false);
	}

	public void resetConfig() {
		setConfig("attack.player", false);
		setConfig("attack.neutral", false);
		setConfig("attack.mobs", true);
		setConfig("kill.not", false);//dont work anyway

		setConfig("player.warn", true);

		setConfig("player.distance", false);

		setConfig("wait.10", false);
		setConfig("wait.15", false);
		setConfig("wait.30", false);
		setConfig("wait.60", false);
	}
	
	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}

	@Override
	public void updateTile(int ticks)
	{
		if(!level.isClientSide)
		{
			if(getChipPower(EnumChipType.AI) == 0) {
				resetConfig();
				setConfig("has.ai", false);
			}
			else {
				setConfig("has.ai", true);
			}

			if(shouldWork())
			{
				for(int i=0;i<ticks;i++)
				{
					if(src!=null)
					{
						if(!isEntityValid(src) || !src.isAlive())
						{
							entityPos = null;
							src = null;
							entityTries=0;
						}
					}
					
					if(src==null)
					{
						src = getNextEntity(Predicates.alwaysTrue());
						entityTries=0;
						if(src!=null)
						{
							MessageEntityForEater mes = new MessageEntityForEater(src, this.worldPosition);
							List<ServerPlayer> list = level.getEntitiesOfClass(ServerPlayer.class, new AABB(worldPosition.getX()-20, worldPosition.getY()-20, worldPosition.getZ()-20, worldPosition.getX()+20, worldPosition.getY()+20, worldPosition.getZ()+20), new Predicate<ServerPlayer>() //getEntitiesWithinAABBExcludingEntity
							{			
								@Override
								public boolean apply(ServerPlayer mp)
								{			
									boolean[] b =  FPPacketHandler.map.get(mp.getGameProfile().getId());
									if(b==null || b[1])
									{
										return true;
									}
									return false;
								}
							});
							for(ServerPlayer pl : list)
							{
								PacketTarget p = PacketDistributor.PLAYER.with(() -> pl);
								FPPacketHandler.CHANNEL_FUTUREPACK.send(p, mes);
							}
						}
					}
					if(src!=null && isEntityValid(src) && canEntityBeSeen(src))
					{
						entityTries=0;
						progressEntity(src);			
					}
					else
					{
						entityTries++;
						if(entityTries > 5)
						{
							T newTarget = getNextEntity(e -> e != src);
							if(newTarget!=null)
							{
								src = newTarget;
								entityTries = 0;
							}
						}
					}
				}
			}
			else
			{
				src = null;
				entityTries=0;
			}
			
			
			Iterator<Entry<Player, Long>> iter = warnedPlayers.entrySet().iterator();
			while(iter.hasNext())
			{
				Entry<Player, Long> e = iter.next();
				if(System.currentTimeMillis() - e.getValue() > 1000*60*2)
				{
					iter.remove();
				}
			}
		}
	}
	
	@Override
	public void updateNaturally() 
	{
		if(!level.isClientSide)
		{
			if(src==null)
			{
				entityPos = null;			
			}
			else
			{
				if(!src.isAlive())
				{
					src = null;
					entityPos = null;
				}
			}
		}
		super.updateNaturally();
	}

	@Override
	public boolean isWorking()
	{
		return true;
	}
//	
//	public Predicate<T> getPredicate()
//	{
//		return new Predicate<T>()
//		{
//			@Override
//			public boolean apply(T input)
//			{
//				return true;
//			}
//		};
//	}

	public float getRange()
	{
		float pro = getChipPower(EnumChipType.TACTIC) * 2;
		return (2.5F + pro) * 2F;
	}
	
	protected boolean canEntityBeSeen(Entity p_70685_1_)
    {
		Vec3 entity = new Vec3(p_70685_1_.getX(), p_70685_1_.getY() + p_70685_1_.getEyeHeight(), p_70685_1_.getZ());
		blockPos = getVecPos();
		blockPos = blockPos.add(blockPos.vectorTo(entity).normalize());
		
		HitResult pos =  this.level.clip(new ClipContext(blockPos, entity, ClipContext.Block.COLLIDER, ClipContext.Fluid.ANY, p_70685_1_));
		if(pos.getType() == HitResult.Type.MISS)
		{
			entityPos = entity;
			return true;
		}
		else
		{
			return false;
		}
    }
	
	public Vec3 getVecPos()
	{
		if(getBlockState().getValue(BlockEntityLaserBase.ROTATION_VERTICAL) == Direction.DOWN)
			return new Vec3(worldPosition.getX()+0.5,worldPosition.getY()-0.0625,worldPosition.getZ()+0.5);
		else
			return new Vec3(worldPosition.getX()+0.5,worldPosition.getY()+1.0625,worldPosition.getZ()+0.5);
	}
	
	protected T getNextEntity(java.util.function.Predicate<T> isValid)
	{
		float r = getRange();
		List<T> list = level.getEntitiesOfClass(clazz, new AABB(-r*2, -r*2, -r*2, r*2, r*2, r*2).move(worldPosition.getX()+0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5), new Predicate<LivingEntity>()
		{

			@Override
			public boolean apply(LivingEntity input)
			{
				if(input.hasEffect(MobEffects.GLOWING))
					return true;
				return  worldPosition.distToCenterSqr(input.position()) < r*r;
			}
			
		});
		if(list.isEmpty())
			return null;
		
		list.sort(new Comparator<LivingEntity>()
		{
			@Override
			public int compare(LivingEntity o1, LivingEntity o2)
			{
				boolean b1 = o1.hasEffect(MobEffects.GLOWING);
				boolean b2 = o2.hasEffect(MobEffects.GLOWING);
				
				//^XOR
				if(b1^b2)
				{
					return b1 ? -1 : 1;
				}
				
				double dis = (worldPosition.distToCenterSqr(o1.position()) - worldPosition.distToCenterSqr(o2.position()));
				return dis < 0 ? -1 : (dis > 0 ? 1 : 0);
			}
		});
		
		Iterator<T> iter = list.iterator();
		T e;
		isValid = isValid.and(this::isEntityValid);
		while(iter.hasNext())
		{
			e = iter.next();
			if(!isValid.test(e))
			{
				continue;
			}
			if(canEntityBeSeen(e))
			{
				return e;
			}
		}
		return null;
	}
	
	public abstract boolean isEntityValid(T entity);
	
	public boolean matchConfig(Entity e)
	{
		if(e instanceof Player)
		{	
			if(isOwner((Player) e) && !canAttackOwner())
				return false;
		}
		
		if(getChipPower(EnumChipType.AI) > 0)
		{
			if(e instanceof Player)
			{	
				if(getConfiguration("attack.player"))
				{
					if(isOwner((Player) e) && !canAttackOwner())
						return false;
					
					if(getConfiguration("player.distance") && worldPosition.distToCenterSqr(e.position()) > 25)
						return false;
						
					Long l = warnedPlayers.get(e);
					if(l == null)
					{
						l = System.currentTimeMillis() + getMaxPlayerWaitTime();
						warnedPlayers.put((Player) e, l);
						
						warnPlayer((Player) e);
						return false;
					}
					else if(l < System.currentTimeMillis())
					{			
						return true;
					}
					else
					{
						warnPlayer((Player) e);
						return false;
					}
				}
				return false;
			}
			else if(e instanceof Enemy)
				return getConfiguration("attack.mobs");
			else if(e instanceof LivingEntity)
				return getConfiguration("attack.neutral");
			else
				return getConfiguration("attack.all");
		}
		return true;
	}
	
	/**
	 * @return true if the Entity is finished, else it will stay at this entity
	 */
	public abstract void progressEntity(T entity);
	
	public abstract boolean shouldWork();
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public abstract ResourceLocation getTexture();

	//@ TODO: OnlyIn(Dist.CLIENT)
	public abstract int getLaserColor();
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public abstract ResourceLocation getLaser();
	
	@Override
	public AABB getRenderBoundingBox()
	{
		return INFINITE_EXTENT_AABB;
	}
	
//	@Override
//	public boolean shouldRenderInPass(int pass)
//	{
//		this.pass = pass;
//		return pass == 0 || pass == 1;
//	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public void setEntityLiv(T s)
	{
		this.src = s;
		Vec3 local = entityPos = new Vec3(s.getX(), s.getY() + s.getEyeHeight(), s.getZ());
		blockPos = getVecPos();
		blockPos = blockPos.add(blockPos.vectorTo(local).normalize());
		
		lastMessageTime = System.currentTimeMillis();
	}
	
	public boolean attackEntity(LivingEntity base, float amount)
	{
		if(!base.level.isClientSide)
		{
			float xp = this.getChipPower(EnumChipType.ULTIMATE);
			boolean transport = this.getChipPower(EnumChipType.TRANSPORT)>0;
			if(xp>0)
			{
				ServerLevel world = (ServerLevel) this.level;
				FuturepackPlayer player = FakePlayerFactory.INSTANCE.getPlayer(world);
				player.setPos(this.blockPos.x+0.5, this.blockPos.y, this.blockPos.z+0.5);
				boolean b = base.hurt(DamageSource.playerAttack(player), amount);
				if(transport && base.getHealth()<=0)
				{
					new ItemMoveTicker(world, Vec3.atLowerCornerOf(worldPosition), base.position());
				}
				return b;
			}
			else
			{
				boolean b = base.hurt(FuturepackMain.NEON_DAMAGE, amount);
				if(transport && base.getHealth()<=0)
				{
					new ItemMoveTicker(level, Vec3.atLowerCornerOf(worldPosition), base.position());
				}
				return b;
			}
		}
		return false;
	}
	
	private short[] cashe;
	
	@Override
	public int getProperty(int id)
	{
		if(id<getShortSize())
		{
			return getConfigAsShorts()[id];
		}
		return energy.get();
	}

	@Override
	public void setProperty(int id, int value)
	{
		if(id<getShortSize())
		{
			if(cashe==null || cashe.length !=getShortSize())
				cashe = new short[getShortSize()];
			cashe[id] = (short) value;
			setConfigFromShort(cashe);
		}
		else
			setEnergy(value);
	}

	@Override
	public int getPropertyCount() 
	{
		return getShortSize() + 1;
	}
	
	
	public Iterable<String> getConfigs()
	{
		return config.keySet();
	}
	
	public boolean getConfiguration(String s)
	{
//		Boolean
		
		return config.get(s);
	}
	
	public void setConfig(String s, boolean b)
	{
		config.put(s, b);
	}
	
	private int getShortSize()
	{
		int size = config.size() / Short.SIZE;
		
		if(size * Short.SIZE < config.size())
			size++;
		
		return size;
	}
	
	public short[] getConfigAsShorts()
	{
		short[] data = new short[getShortSize()];
		
		int bit = 0;
		int index = 0;
		for(Entry<String, Boolean> e : config.entrySet())
		{
			data[index] |= (e.getValue()==true ? 1 : 0) << bit;
			bit++;
			
			if(bit>=Short.SIZE)
			{
				bit=0;
				index++;
			}
		}
		
		return data;
	}
	
	public void setConfigFromShort(short[] data)
	{
		int bit = 0;
		int index = 0;
		for(String s : config.keySet())
		{
			boolean b = 1 == ((data[index] >> bit) & 1);
			config.put(s, b);
			
			bit++;
			if(bit>=Short.SIZE)
			{
				bit=0;
				index++;
			}
		}
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		byte[] data = nbt.getByteArray("config");
		short[] shorts = new short[data.length/2];
		if(shorts.length>0) //this crashed because the length was 0 ....
		{
			for(int i=0;i<shorts.length;i++)
			{
				shorts[i] = (short) (data[i*2]&0xFF | ((data[i*2+1]<<8)&0xFF00));
			}
			setConfigFromShort(shorts);
		}	
		if(nbt.hasUUID("owner"))
			owner = nbt.getUUID("owner");
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		short[] shorts = getConfigAsShorts();
		byte[] data = new byte[shorts.length*2];
		for(int i=0;i<shorts.length;i++)
		{
			data[i*2] = (byte) (shorts[i] & 0xFF);
			data[i*2+1] = (byte) ((shorts[i] >> 8) & 0xFF);
		}
		nbt.putByteArray("config", data);
		if(owner!=null)
			nbt.putUUID("owner", owner);
		
		return nbt;
	}
	
	@Override
	public void setOwner(Player pl)
	{
		owner = pl.getGameProfile().getId();
	}
	
	@Override
	public boolean isOwner(Player pl)
	{
		return isOwner(pl.getGameProfile().getId());
	}
	
	@Override
	public boolean isOwner(UUID pl)
	{
		if(owner==null)
			return false;
		
		return owner.equals(pl);
	}
	
	public boolean canAttackOwner()
	{
		return false;
	}
	
	private WeakHashMap<Player, Long> notice = new WeakHashMap<Player, Long>();
	
	public void warnPlayer(Player pl)
	{
		if(level.isClientSide)
			return;
		if(!getConfiguration("player.warn"))
			return;
		
		Long last = notice.get(pl);
		if(last != null && System.currentTimeMillis() - last < 1000)
		{
			return;
		}
		
		int i = 0;
		int l = (int) ((warnedPlayers.get(pl)-System.currentTimeMillis()) / 500);
		switch (l)
		{
		case 120:
			i=60;
			break;
		case 60:
			i=30;
			break;		
		case 30:
			i=15;
			break;
		case 20:
			i=10;
			break;
		case 10:
			i=5;
			break;
		case 2:
			i=1;
			break;
		default:
			i=-1;
			break;
		}
		if(i!=-1)
		{
			TranslatableComponent trans = new TranslatableComponent("laser.attack.warn.time", i);
			pl.sendMessage(trans, Util.NIL_UUID);
			notice.put(pl, System.currentTimeMillis());
		}
	}
	
	/**
	 * @return in milliseconds
	 */
	public long getMaxPlayerWaitTime()
	{
		int wait = 1;
		if(getConfiguration("wait.10"))
			wait += 10;
		if(getConfiguration("wait.15"))
			wait += 15;
		if(getConfiguration("wait.30"))
			wait += 30;
		if(getConfiguration("wait.60"))
			wait += 60;
		
		return wait * 1000;
	}
}
