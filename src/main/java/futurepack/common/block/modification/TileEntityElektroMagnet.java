package futurepack.common.block.modification;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.FPConfig;
import futurepack.common.FPSounds;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperMagnetism;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityElektroMagnet extends TileEntityModificationBase 
{
	long last = 0;
	//boolean lastred = false;
	//ISound sound=null;
	private boolean working=false;
	
	private boolean redstone = false;
	
	public TileEntityElektroMagnet(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.ELECTRO_MAGNET, pos, state);
	}
	
	@Override
	public void updateTile(int ticks) 
	{
		redstone = level.getBestNeighborSignal(worldPosition)>0;
		working=false;
		if(redstone)
		{			
			double x = worldPosition.getX()+0.5;
			double y = worldPosition.getY()+0.5;
			double z = worldPosition.getZ()+0.5;
			
			
			float pro = getChipPower(EnumChipType.INDUSTRIE);
			int range = (int) (10 * (1 + pro));
			
			Direction face = getBlockState().getValue(BlockRotateableTile.FACING);
			
			x += face.getStepX() * 0.6 * ticks;
			y += face.getStepY() * 0.6 * ticks;
			z += face.getStepZ() * 0.6 * ticks;
			
			HelperMagnetism.doMagnetism(level, x, y, z, range, 1.0F+pro);
			
			working=true;
		}
	}
	
	@Override
	public void updateNaturally()
	{
		if(redstone)
		{
			if(System.currentTimeMillis() - last >= 2174)//old: 1200, real 2174
			{
				double x = worldPosition.getX()+0.5;
				double y = worldPosition.getY()+0.5;
				double z = worldPosition.getZ()+0.5;
				level.playLocalSound(x, y, z, FPSounds.ELEKTROFELD, SoundSource.BLOCKS, (float) (1.0F * FPConfig.CLIENT.volume_emagnet.get()), 1.0F, false);
				last = System.currentTimeMillis();
			}
		}
		super.updateNaturally();
	}
	
	@Override
	public EnumEnergyMode getEnergyType() 
	{
		return EnumEnergyMode.USE;
	}
	
	@Override
	public boolean isWorking() 
	{
		return working;
	}
	
	@Override
	public int getDefaultPowerUsage() 
	{
		return super.getDefaultPowerUsage() + 10;
	}
}
