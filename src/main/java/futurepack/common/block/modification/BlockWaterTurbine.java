package futurepack.common.block.modification;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockWaterTurbine extends BlockHoldingTile implements IBlockBothSidesTickingEntity<TileEntityWaterTurbine>
{

	protected BlockWaterTurbine(Block.Properties props)
	{
		super(props);
	}

	@Override
	public BlockEntityType<TileEntityWaterTurbine> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.WATER_TURBINE;
	}
}
