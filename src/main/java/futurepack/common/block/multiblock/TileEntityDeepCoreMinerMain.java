package futurepack.common.block.multiblock;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Nullable;

import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPConfig;
import futurepack.common.FPSounds;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.misc.TileEntityBedrockRift;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.world.scanning.ChunkData;
import it.unimi.dsi.fastutil.objects.Object2IntOpenHashMap;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.Container;
import net.minecraft.world.ContainerHelper;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.pattern.BlockPattern;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntityDeepCoreMinerMain extends TileEntityModificationBase implements ITilePropertyStorage
{
	protected ItemStackHandler oreStorage = new OreStorage(3 * 4*9);//3x  4*9 chests
	protected ItemStackHandler internInventory = new ItemStackHandler(5); // 1 Lense + 3 Filters + 1Active Lense
	private long mod;

	private boolean isRestoring = false;
	private DeepCoreLogic logic;

	public SPStorage support = new SPStorage();

	public boolean isMutliblockWorking = false;
	public boolean clientWorking = false;

	public TileEntityDeepCoreMinerMain(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.DEEPCORE_MAIN, pos, state);
	}

	@Override
	public void updateTile(int ticks)
	{
		if(logic!=null && !level.isClientSide)
		{
			if(!isMutliblockWorking)
				isMutliblockWorking = BlockDeepCoreMiner.isMultiBlockWorking(level, worldPosition, level.getBlockState(worldPosition), false);

			if(isMutliblockWorking)
			{
				logic.update(ticks);

				if(!logic.needSupport() && support.get()>0 && this.getChipPower(EnumChipType.SUPPORT)>0)
				{
					HelperEnergyTransfer.sendSupportPoints(this);
				}
				boolean needUpdate  = clientWorking != logic.getProgress() > 0;
				if(needUpdate)
				{
					clientWorking = logic.getProgress() > 0;
					sendDataUpdatePackage(20);
				}
			}
		}
	}

	private long lasttime;

	@Override
	public void updateNaturally()
	{
		if(level.isClientSide && clientWorking)
		{
			if(System.currentTimeMillis() - lasttime >= 3428)
			{
				level.playLocalSound(worldPosition.getX()+0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5, FPSounds.CORELASER, SoundSource.BLOCKS, 0.4F * FPConfig.CLIENT.volume_deepcoreminer.get().floatValue(), 0.9F + level.random.nextFloat()*0.1F, false);
				lasttime = System.currentTimeMillis();
			}
		}
	}

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		if(logic != null)
			nbt.put("logic", logic.serializeNBT());
		nbt.put("support", support.serializeNBT());
		nbt.put("oreStorage", oreStorage.serializeNBT());
		nbt.put("internInventory", internInventory.serializeNBT());
		return super.writeDataUnsynced(nbt);
	}



	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		if(nbt.contains("logic"))
			logic = new DeepCoreLogic(this, nbt.getCompound("logic"));
		support.deserializeNBT(nbt.getCompound("support"));
		oreStorage.deserializeNBT(nbt.getCompound("oreStorage"));
		internInventory.deserializeNBT(nbt.getCompound("internInventory"));
	}

	@Override
	public void readDataSynced(CompoundTag nbt)
	{
		super.readDataSynced(nbt);
		if(nbt.contains("progress") && level!=null && level.isClientSide)
		{
			clientWorking = nbt.getBoolean("progress");
		}

	}

	@Override
	public void writeDataSynced(CompoundTag nbt)
	{
		super.writeDataSynced(nbt);
		if(level==null || !level.isClientSide)
			nbt.putBoolean("progress", clientWorking);
	}


	@Override
	public EnumEnergyMode getEnergyType()
	{
		return EnumEnergyMode.USE;
	}

	@Override
	public boolean isWorking()
	{
		return logic != null && (logic.isWorking() || (level.isClientSide && clientWorking) );
	}

	public void storeUsedBlocks(BlockPattern.BlockPatternMatch helper)
	{
		logic = new DeepCoreLogic(this, helper);
		ItemStackHandler oldOre = oreStorage;
		oreStorage = new ItemStackHandler(logic.getTotalChestSize());
	}

	public void restoreUsedBlocks()
	{
		if(!isRestoring)
		{
			isRestoring = true;
			logic.restoreUsedBlocks();
			isRestoring = false;
			isMutliblockWorking = false;
			logic = null;
		}
	}

	public DeepCoreInventory getDeepCoreInventory(boolean insert)
	{
		return new DeepCoreInventory(insert);
	}

	@Nullable
	public Direction getFacing()
	{
		return logic !=null ? logic.getFacing() : null;
	}

//	@Override
//	public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
//	{
//
//
//		if(oldState.getBlock() != newSate.getBlock())
//			return true;
//		if(newSate.getBlock() != FPBlocks.deep_core_miner)
//			return true;
//
//		return !newSate.getValue(BlockDeepCoreMiner.variants).isMain();
//	}

	@Override
	public int getDefaultPowerUsage()
	{
		int logic = 0;
		if(this.logic!=null)
		{
			logic += this.logic.getNeededEnergie();
		}

		return super.getDefaultPowerUsage() + logic;
	}

	@Override
	public int getMaxNE()
	{
		return 100000;
	}

	private class DeepCoreInventory implements IItemHandler
	{
		private final boolean allowInsert;

		public DeepCoreInventory(boolean allowInsert)
		{
			this.allowInsert = allowInsert;
		}


		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			if(allowInsert)
				return oreStorage.insertItem(slot, stack, simulate);
			else
				return stack;
		}

		@Override
		public int getSlots()
		{
			return oreStorage.getSlots();
		}

		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return oreStorage.getStackInSlot(slot);
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			return oreStorage.extractItem(slot, amount, simulate);
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return oreStorage.getSlotLimit(slot);
		}

		@Override
		public boolean isItemValid(int slot, ItemStack stack)
		{
			return oreStorage.isItemValid(slot, stack);
		}
	}

	public DeepCoreLogic getLogic()
	{
		return logic;
	}

	@Override
	public AABB getRenderBoundingBox()
	{
		if(logic!=null)
		{
			return new AABB(worldPosition.offset(-2,0,-2), worldPosition.offset(3,3,3));
		}
		return super.getRenderBoundingBox();
	}

	public ItemStack[] getFilters()
	{
		ArrayList<ItemStack> list = new ArrayList<ItemStack>(3);
		for(int i=1;i<4;i++)
		{
			if(!internInventory.getStackInSlot(i).isEmpty())
				list.add(internInventory.getStackInSlot(i));
		}
		return list.toArray(new ItemStack[list.size()]);
	}

	@Override
	public int getProperty(int id)
	{
		switch(id)
		{
		case 0:
			return energy.get()/10; //100.000 ist sonst zu gro�
		case 1:
			return support.get();
		case 2:
			return (int) (logic.getProgress() * 30000);//Short.MAX_VALUE = 32000
		case 3:
			return (int) (logic.getDurability() * 1000);
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch(id)
		{
		case 0:
			setEnergy(value*10);
			break;
		case 1:
			support.set(value);
			break;
		case 2:
			logic.setProgress(value / 30000F);
			break;
		case 3:
			logic.setDurability(value / 1000F);
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 4;
	}

	public ItemStackHandler getInternInvetory()
	{
		return internInventory;
	}

	public IItemHandler getFakeSlots()
	{
		IItemHandlerModifiable handler = new IItemHandlerModifiable()
		{
			NonNullList<ItemStack> list = NonNullList.withSize(4, ItemStack.EMPTY);
			{
				list.set(0, new ItemStack(MiscBlocks.bedrock_rift));
				list.set(1, logic.getChestItem(0));
				list.set(2, logic.getChestItem(1));
				list.set(3, logic.getChestItem(2));

				CompoundTag nbt = list.get(0).getOrCreateTagElement("scanresult");
				TileEntityBedrockRift rift = logic.getRift();
				if(rift!= null  && rift.isScanned() && !level.isClientSide())
				{
					ChunkData data = rift.getData();
					int total = 0;
					ArrayList<Entry<String, Integer>> list = new ArrayList<Entry<String, Integer>>();
					for(Entry<String, Integer> e : data.getMap().entrySet())
					{
						total += e.getValue();
						list.add(e);
					}

					final double d = 1000D /total;
					ListTag tagList = new ListTag();
					list.stream().sorted(new Comparator<Entry<String, Integer>>()
					{
						@Override
						public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2)
						{
							return o2.getValue() - o1.getValue();
						}
					}).map(e -> {
						CompoundTag n =  new CompoundTag();
						n.putInt("v", (int) (e.getValue()*d));
						if(e.getKey().startsWith("forge:ores/"))
							n.putString("k", e.getKey().substring("forge:ores/".length()));
						else
							n.putString("k", e.getKey());
						return n;
					}).forEach(tagList::add);
					nbt.put("list", tagList);
					nbt.putFloat("fill", (float) rift.getFillrate());
				}
			}

			@Override
			public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
			{
				return stack;
			}

			@Override
			public ItemStack getStackInSlot(int slot)
			{
				if(!level.isClientSide && slot==0)
				{
					CompoundTag nbt = list.get(0).getOrCreateTagElement("scanresult");
					TileEntityBedrockRift rift = logic.getRift();
					if(rift!=null)
						nbt.putFloat("fill", (float) rift.getFillrate());
				}
				return list.get(slot);
			}

			@Override
			public int getSlots()
			{
				return 4;
			}

			@Override
			public int getSlotLimit(int slot)
			{
				return 1;
			}

			@Override
			public ItemStack extractItem(int slot, int amount, boolean simulate)
			{
				return ItemStack.EMPTY;
			}

			@Override
			public void setStackInSlot(int slot, ItemStack stack)
			{
				list.set(slot, stack);
			}

			@Override
			public boolean isItemValid(int slot, ItemStack stack)
			{
				return false;
			}
		};
		return handler;
	}


	public Container getCompressedOres()
	{
		return new OreInvWrapper();
	}

	private class OreStorage extends ItemStackHandler
	{
		public OreStorage(int slots)
		{
			super(slots);
		}

		@Override
		protected void onContentsChanged(int slot)
		{
			mod = System.currentTimeMillis();
		}
	}

	private class OreInvWrapper implements Container
	{
		NonNullList<ItemStack> list;
		private long time;

		@Override
		public int getContainerSize()
		{
			createList();
			return list.size();
		}

		@Override
		public boolean isEmpty()
		{
			return false;
		}

		@Override
		public ItemStack getItem(int index)
		{
			createList();
			if(index<list.size())
				return list.get(index);
			return ItemStack.EMPTY;
		}

		@Override
		public ItemStack removeItem(int index, int count)
		{
			return ContainerHelper.removeItem(this.list, index, count);
		}

		@Override
		public ItemStack removeItemNoUpdate(int index)
		{
			ItemStack old = list.get(index);
			list.set(index, ItemStack.EMPTY);
			return old;
		}

		@Override
		public void setItem(int index, ItemStack stack)
		{
			list.set(index, stack);
		}

		@Override
		public int getMaxStackSize()
		{
			return Integer.MAX_VALUE;
		}

		@Override
		public void setChanged() { }

		@Override
		public boolean stillValid(Player player)
		{
			return true;
		}

		@Override
		public void startOpen(Player player) { }

		@Override
		public void stopOpen(Player player) { }

		@Override
		public boolean canPlaceItem(int index, ItemStack stack)
		{
			return false;
		}

		@Override
		public void clearContent() { }

		public void createList()
		{
			if(mod == time && list!=null)
				return;

			Map<Item, Integer> map = new Object2IntOpenHashMap<Item>(oreStorage.getSlots());
			for(int i=0;i<oreStorage.getSlots();i++)
			{
				ItemStack st = oreStorage.getStackInSlot(i);
				if(!st.isEmpty())
				{
					Integer count = map.getOrDefault(st.getItem(), 0);
					count += st.getCount();
					map.put(st.getItem(), count);
				}
			}

			list = NonNullList.withSize(map.size(), ItemStack.EMPTY);
			int j=0; //liast,add is not working so we need this
			for(Entry<Item, Integer> e : map.entrySet())
			{
				ItemStack st = new ItemStack(e.getKey(), e.getValue());
				CompoundTag nbt = new CompoundTag();
				nbt.putInt("c", e.getValue());
				list.set(j++, st);
			}
			this.time = TileEntityDeepCoreMinerMain.this.mod;
		}
	}

	public ItemStackHandler getOreInv()
	{
		return this.oreStorage;
	}

	public int getComparatorOutput()
	{
		ItemStack it = internInventory.getStackInSlot(0);
		if(it.isEmpty())
			return 0;

		return Mth.floor(14F * it.getCount()/64F)+1;
	}

	private LazyOptional<ISupportStorage> supportOpt;
	private LazyOptional<IItemHandler> itemOpt;


	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(logic!=null)
			{
				if(itemOpt!=null)
					return (LazyOptional<T>) itemOpt;
				else
				{
					itemOpt = LazyOptional.of(InventoryWrapper::new);
					itemOpt.addListener( p -> itemOpt = null);
					return (LazyOptional<T>) itemOpt;
				}
			}
		}
		else if(capability == CapabilitySupport.cap_SUPPORT)
		{
			if(supportOpt!=null)
			{
				return (LazyOptional<T>) supportOpt;
			}
			else
			{
				supportOpt = LazyOptional.of(() -> support);
				supportOpt.addListener(p -> supportOpt = null);
				return (LazyOptional<T>) supportOpt;
			}
		}
		return super.getCapability(capability, facing);
	}

	@Override
	public void setChanged()
	{
		super.setChanged();
		if(logic!=null)
		{
			BlockState bl = getBlockState();
			for(int h=0;h<3;h++)
			{
				for(int i=0;i<3;i++)
				{
					 this.level.updateNeighbourForOutputSignal(this.worldPosition.above(h).relative(logic.getFacing(), i), bl.getBlock());
				}
			}

		}
	}


	@Override
	public void setRemoved()
	{
		HelperEnergyTransfer.invalidateCaps(supportOpt, itemOpt);
		super.setRemoved();
	}

	public class SPStorage extends CapabilitySupport
	{
		public SPStorage()
		{
			super(4096, EnumEnergyMode.PRODUCE);
		}

		@Override
		public boolean canAcceptFrom(ISupportStorage other)
		{
			if(logic!=null)
			{
				return get() < getMax() && (getChipPower(EnumChipType.SUPPORT) <=0 || logic.needSupport());
			}
			return false;
		}

		@Override
		public void set(int sp)
		{
			super.energy = sp;
		}

		@Override
		public EnumEnergyMode getType()
		{
			if(logic!=null)
			{
				if(!logic.needSupport() && getChipPower(EnumChipType.SUPPORT)>0)
				{
					return EnumEnergyMode.PRODUCE;
				}
				else
				{
					return EnumEnergyMode.USE;
				}
			}
			return super.getType();
		}
	}

	private class InventoryWrapper implements IItemHandler
	{
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			ItemStack it = internInventory.insertItem(0, stack, simulate);
			setChanged();
			return it;
		}

		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return internInventory.getStackInSlot(slot);
		}

		@Override
		public int getSlots()
		{
			return 1;
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return 64;
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			ItemStack it = internInventory.extractItem(0, amount, simulate);
			setChanged();
			return it;
		}

		@Override
		public boolean isItemValid(int slot, ItemStack stack)
		{
			return internInventory.isItemValid(slot, stack);
		}
	}

}
