package futurepack.common.block.inventory;

import futurepack.api.PacketBase;
import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.IItemSupport;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.TileEntityNetworkMaschine;
import futurepack.common.network.FunkPacketSupport;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class TileEntityModulT2 extends TileEntityNetworkMaschine implements ITilePropertyStorage, ITileServerTickable
{
	public CapabilitySupport support = new CapabilitySupport(512, EnumEnergyMode.USE);
	
//	/**
//	 * 0: input baterie laden
//	 * 1: input baterie entladen
//	 * 2: output baterie voll
//	 * 3: output baterie leer
//	*/
//	private ItemStack[] items = new ItemStack[4];
	private ItemContainer handler = new ItemContainer();
	
	
	public TileEntityModulT2(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.MODUL_T2, pos, state);
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState pState)
	{		
		if(handler.getStackInSlot(0)!=null && handler.getStackInSlot(0).getItem() instanceof IItemSupport)
		{
			IItemSupport support = (IItemSupport) handler.getStackInSlot(0).getItem();
			if(support.isSupportable(handler.getStackInSlot(0)))
			{
				int c = Math.min(20, Math.min(support.getMaxSupport(handler.getStackInSlot(0)) - support.getSupport(handler.getStackInSlot(0)), this.support.get()));
				this.support.use(c);
				support.addSupport(handler.getStackInSlot(0), c);
					
			}
			if(support.getSupport(handler.getStackInSlot(0)) == support.getMaxSupport(handler.getStackInSlot(0)))
			{
				if(handler.getStackInSlot(2).isEmpty())
				{
					handler.setStackInSlot(2, handler.getStackInSlot(0));
					handler.setStackInSlot(0, ItemStack.EMPTY);
				}
			}
		}
		if(handler.getStackInSlot(1)!=null && handler.getStackInSlot(1).getItem() instanceof IItemSupport)
		{
			IItemSupport support = (IItemSupport) handler.getStackInSlot(1).getItem();
			if(support.getSupport(handler.getStackInSlot(1))>0)
			{
				int c = Math.min(20, Math.min(support.getSupport(handler.getStackInSlot(1)), this.support.getMax() - this.support.get()));
				this.support.add(c);
				support.addSupport(handler.getStackInSlot(1), -c);
			}
			if(support.getSupport(handler.getStackInSlot(1))<=0)
			{
				if(handler.getStackInSlot(3).isEmpty())
				{
					handler.setStackInSlot(3, handler.getStackInSlot(1));
					handler.setStackInSlot(1, ItemStack.EMPTY);
				}
			}
		}
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("support", support.serializeNBT());
		nbt.put("itemContainer", handler.serializeNBT());
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		support.deserializeNBT(nbt.getCompound("support"));
		handler.deserializeNBT(nbt.getCompound("itemContainer"));
	}
	
	public boolean isBatterie(ItemStack it, boolean chargeRequired)
	{
		if(it==null || it.isEmpty())
			return false;
		Item item = it.getItem();
		if(item instanceof IItemSupport)
		{
			IItemSupport neon = (IItemSupport) item;
			if(chargeRequired)
			{
				return neon.getSupport(it) > 0;
			}
			else
			{
				return neon.isSupportable(it);
			}
		}
		return false;
	}

	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return support.get();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			support.set(value);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 1;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		super.onFunkPacket(pkt);
		
		if(pkt instanceof FunkPacketSupport)
		{
			FunkPacketSupport sup = (FunkPacketSupport) pkt;
			sup.collected += support.use(sup.needed - sup.collected);
		}
	}
	
	private class ItemContainer extends ItemStackHandlerGuis
	{
		public ItemContainer()
		{
			super(4);
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			if(slot==2 || slot==3 || gui)
				return super.extractItem(slot, amount, simulate, gui);
			else 
				return ItemStack.EMPTY;
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
		{
			if(isItemValid(slot, stack))
				return super.insertItem(slot, stack, simulate, gui);
			else
				return stack;
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack, boolean gui)
		{
			return (slot==0 || slot==1) && isBatterie(stack, slot==1);
		}
	}
	
	private LazyOptional<ISupportStorage> support_opt;
	private LazyOptional<IItemHandler> item_opt;
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{		
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(item_opt!=null)
				return (LazyOptional<T>) item_opt;
			else
			{
				item_opt = LazyOptional.of(() -> handler);
				item_opt.addListener(p -> item_opt = null);
				return (LazyOptional<T>) item_opt;
			}
		}
		else if(capability == CapabilitySupport.cap_SUPPORT)
		{
			if(support_opt!=null)
				return (LazyOptional<T>) support_opt;
			else
			{
				support_opt = LazyOptional.of(() -> support);
				support_opt.addListener(p -> support_opt = null);
				return (LazyOptional<T>) support_opt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(item_opt, support_opt);
		super.setRemoved();
	}
	
	public IItemHandler getGui()
	{
		return new ItemHandlerGuiOverride(handler);
	}
}
