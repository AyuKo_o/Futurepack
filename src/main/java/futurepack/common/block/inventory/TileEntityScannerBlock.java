package futurepack.common.block.inventory;

import java.util.ArrayList;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.FPTileEntitys;
import futurepack.common.item.misc.ItemResearchBlueprint;
import futurepack.common.network.FunkPacketGetResearch;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchManager;
import futurepack.depend.api.EnumAspects;
import futurepack.depend.api.EnumScannerState;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.state.BlockState;

public class TileEntityScannerBlock extends TileEntityInventoryBase implements ITileNetwork, ITilePropertyStorage
{
	public TileEntityScannerBlock(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.SCANNER_BLOCK, pos, state);
	}

	private Research searched;
	private float progress = 0;
	private boolean[] aspects = new boolean[EnumAspects.buttons.length];
	public EnumScannerState scanningState;
	private int posibilities = 0;
	
	
	//items: 0=serach in; 1=research out
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		
		if(searched!=null)
		{
			nbt.putString("searchedN", searched.getName());
		}
		
		nbt.putFloat("progress", progress);
		nbt.putInt("aspects", getAspects());
		nbt.putInt("state", scanningState==null?-1:scanningState.ordinal());		
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt); 
		
		if(nbt.contains("searchedN"))
		{
			searched = ResearchManager.getResearch(nbt.getString("searchedN"));
		}
		else if(nbt.contains("searched"))
		{
			searched = ResearchManager.getById(nbt.getInt("searched")); //back ward compatibility
		}
		progress = nbt.getFloat("progress");
		setAspects(nbt.getInt("aspects"));
		int st = nbt.getInt("state");
		if(st==-1)
		{
			this.scanningState = null;
		}
		else
		{
			this.scanningState = EnumScannerState.values()[st];
		}
	}
	
	@Override
	protected int getInventorySize()
	{
		return 2;
	}

	public void searchForItems(Player pl, ItemStack it)
	{
//		System.out.println("TileEntityScannerBlock.searchForItems()");
		
		searched = null;
		progress = 0;
		scanningState = EnumScannerState.Error;
		items.set(1, ItemStack.EMPTY);
		posibilities = 0;
		
		if(it==null||it.isEmpty())
		{
			return;
		}
		
		if(aspects==null)
		{
			return;
		}
		
		ArrayList<Research> found = getBase(it);
		scanningState = EnumScannerState.WrongItem;
		
		if(found.size()==0)
		{
			return;
		}
			
		int total = found.size();
		
		removeResearched(found, pl);
		scanningState = EnumScannerState.ResearchedEverything;
		int has = found.size();
		progress = 1F - ((float)has / (float)total);
		
		if(found.size()==0)
		{
			return;
		}
			
		removeUnresearchable(found, pl);
		scanningState = EnumScannerState.MissingBasses;
		
		if(found.size()==0)
		{
			return;
		}
		scanningState = EnumScannerState.WrongAspects;
		
		//first check if any research is correct
		for(Research r : found)
		{
			if(r.areAspectsMatching(aspects))
			{
				searched = r;
				scanningState = EnumScannerState.Succses;
				items.set(1, ItemResearchBlueprint.getItemForResearch(searched));
				return;
			}
		}	
		
		for(Research r : found)
		{
			if(r.isPartwhiseCorrect(aspects))
			{
				scanningState = EnumScannerState.Partwise;
				posibilities++;
			}
		}	
	}
	
	private ArrayList<Research> getBase(ItemStack it)
	{
		ArrayList<Research> found = new ArrayList<Research>();
		Main:
		for(int i=0;i<ResearchManager.getResearchCount();i++)
		{
			Research r = ResearchManager.getById(i);
			
			if(r.getParents()==null)
				continue;
			
			for(Research res : r.getParents())
			{
				for(ItemStack is : res.getEnables())
				{
					if(it.getItem() == is.getItem())
					{
						found.add(r);
						continue Main;
					}
				}
			}
		}
		return found;
	}
	
	private void removeResearched(ArrayList<Research> list, Player pl)
	{
		CustomPlayerData cd = CustomPlayerData.getDataFromPlayer(pl);
		list.removeIf(cd::hasResearch);
	}
	
	private void removeUnresearchable(ArrayList<Research> list, Player pl)
	{
		CustomPlayerData cd = CustomPlayerData.getDataFromPlayer(pl);
		list.removeIf(r -> {return !cd.canResearch(r);});
	}
	
	@Override
	public boolean isWire()
	{
		return false;
	}

	@Override
	public void onFunkPacket(PacketBase pkt)
	{
		if(pkt instanceof FunkPacketGetResearch)
		{
			FunkPacketGetResearch get = (FunkPacketGetResearch) pkt;
			if(get.research == null)
			{
				get.research = this.searched;
			}
		}
	}
	
	@Override
	public boolean isNetworkAble()
	{
		return true;
	}
	
	
	@Override
	public void stopOpen(Player pl)
	{
		setChanged();
	}
	
	@Override
	public void setItem(int var1, ItemStack var2)
	{
		super.setItem(var1, var2);
		if(var1==0 && (var2==null || var2.isEmpty()))
		{
			searched = null;
			progress = 0;
		}
	}
	
	@Override
	public boolean canPlaceItem(int var1, ItemStack var2)
	{
		if(var1==1)
		{
			return false;
		}
		return super.canPlaceItem(var1, var2);
	}
	
	
	public boolean isOn()
	{
		return getItem(0) != null;
	}

	public void setAspects(EnumAspects aspects, boolean state)
	{
		this.aspects[aspects.ordinal()] = state;
	}
	
	public boolean getAspect(EnumAspects aspects)
	{
		return this.aspects[aspects.ordinal()];
	}

	public float getProgress()
	{
		return progress;
	}
	
	public static int booleanToInt(boolean[] bools)
	{
		if(bools.length > Short.SIZE)
		{
			throw new IllegalArgumentException("Array is to long");
		}
			
		int n = 0;
		for(int i=0;i<bools.length;i++)
		{
			if(bools[i])
			{
				n = n | 1<<i;
			}			
		}
		
		return n;
	}
	
	public int getAspects()
	{
		return booleanToInt(aspects);
	}
	
	public void setAspects(int asp)
	{
		aspects = intToBool(aspects, asp);
	}
	
	public static boolean[] intToBool(boolean[] bools, int n)
	{
		for(int i=0;i<bools.length;i++)
		{
			bools[i] = ((n>>i) & 1)==1;
		}
		return bools;
	}

	@Override
	public int getProperty(int id)
	{
		switch(id)
		{
		case 0:
			return getAspects();
		case 1:
			if(searched!=null)
			{
				return searched.id;
			}
		case 2:
			return scanningState==null?-1:scanningState.ordinal();
		case 3:
			return (int) (progress * 1000);
		case 4:
			return posibilities;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch(id)
		{
		case 0:
			setAspects(value);
			break;
		case 1:
			if(id>=0)
			{
				searched = ResearchManager.getById(id);
			}
			break;
		case 2:
			scanningState = value==-1?null : EnumScannerState.values()[value];
			break;
		case 3:
			progress = value / 1000F;
		case 4:
			posibilities = value;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 5;
	}
	
	@Override
	public String getGUITitle() {
		return "gui.futurepack.scanner.title";
	}
}
