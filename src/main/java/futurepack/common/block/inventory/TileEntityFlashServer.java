package futurepack.common.block.inventory;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.IItemSupport;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.logistic.LogisticSupportPointsWrapper;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityFlashServer extends TileEntityInventoryLogistics implements WorldlyContainer, ITilePropertyStorage, ITileServerTickable
{
	public SupportStorage support = new SupportStorage();
	private LazyOptional<ISupportStorage>[] supportOpt;
	private int cooldown = 0;
	
	
	@SuppressWarnings("unchecked")
	public TileEntityFlashServer(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.FLASH_SERVER, pos, state);
		
		supportOpt = new LazyOptional[6];
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.SUPPORT);
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes() 
	{
		return new EnumLogisticType[]{EnumLogisticType.SUPPORT, EnumLogisticType.ITEMS};
	}
	
	@Override
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.SUPPORT)
		{
			if(supportOpt[face.get3DDataValue()]!=null)
			{
				supportOpt[face.get3DDataValue()].invalidate();
				supportOpt[face.get3DDataValue()] = null;
			}
		}
		super.onLogisticChange(face, type);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		if(side==null)
			return super.getCapability(cap, side);
		
		if(cap == CapabilitySupport.cap_SUPPORT)
		{
			return HelperEnergyTransfer.getSupportCap(supportOpt, side, this::getLogisticStorage, ()->support);
		}
		
		return super.getCapability(cap, side);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(supportOpt);
		super.setRemoved();
	}
	
	@Override
	protected int getInventorySize()
	{
		return 6;
	}
	
	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
		//boolean powered = world.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
		float ladung = (float)support.get() / (float)support.getMax();
		if(ladung < 0.25)
		{
			laden(true);
		}
		else if (ladung > 0.75)
		{
			laden(false);
		}
		
		if(!items.get(0).isEmpty() && items.get(0).getItem() instanceof IItemSupport)
		{
			ItemStack item = items.get(0);
			IItemSupport ch = (IItemSupport) item.getItem();
			
			if(ch.isSupportable(item))
			{
				int diff = ch.getMaxSupport(item) - ch.getSupport(item);
				if(diff>0)
				{
					int sp = Math.min(30, Math.min(support.get(), diff));
					ch.addSupport(item, sp);
					support.use(sp);
				}				
			}							
		}
		if(!items.get(1).isEmpty() && items.get(1).getItem() instanceof IItemSupport)
		{
			ItemStack item = items.get(1);
			IItemSupport ch = (IItemSupport) item.getItem();
			
			if(ch.isSupportable(item))
			{
				int diff = support.getMax() - support.get();
				if(diff>0)
				{
					int sp = Math.min(30, Math.min(ch.getSupport(item), diff));
					support.add(sp);
					ch.addSupport(item, -sp);
				}
			}		
		}
			
		if(--cooldown <= 0)
		{
			int old = support.get();
			if(old  > 0)
			{
				//HelperEnergyTransfer.sendSupportPoints(this);
				HelperEnergyTransfer.spreadPower(level, worldPosition, support, CapabilitySupport.cap_SUPPORT);
			}
			if(support.get() >= old)
			{
				cooldown = 20;
			}
		}
	}
	
	private void laden(boolean aufab)
	{
		for(int i=2;i<items.size();i++)
		{
			ItemStack it = items.get(i);
			if(it.getCount() >= 1 && it.getItem() instanceof IItemSupport)
			{
				IItemSupport charge = (IItemSupport) it.getItem();
				if(aufab)
				{
					int c = charge.getSupport(it);
					if(c>0)
					{
						support.add(1);
						charge.addSupport(it, -1);
					}
				}
				else
				{
					if(charge.getSupport(it) < charge.getMaxSupport(it))
					{
						charge.addSupport(it, 1);
						support.use(1);
					}
				}				
			}			
		}
	}
	
	public void writeDataSynced(CompoundTag nbt)
	{
		nbt.put("support", support.serializeNBT());
	}
	
	public void readDataSynced(CompoundTag nbt)
	{
		support.deserializeNBT(nbt.getCompound("support"));
	}
	
	@Override
	public boolean canPlaceItem(int var1, ItemStack var2)
	{
		return var2!=null && var2.getItem() instanceof IItemSupport;
	}
	
	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1};
	}

	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		return storage.canInsert(side, EnumLogisticType.ITEMS);
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		return storage.canExtract(side, EnumLogisticType.ITEMS);
	}
	
	@Override
	public void setItem(int var1, ItemStack var2)
	{
		super.setItem(var1, var2);
		if(hasLevel())
		{
			BlockState state = getBlockState();
			state = state.setValue(BlockFlashServer.slot1, !getItem(2).isEmpty());
			state = state.setValue(BlockFlashServer.slot2, !getItem(3).isEmpty());
			state = state.setValue(BlockFlashServer.slot3, !getItem(4).isEmpty());
			state = state.setValue(BlockFlashServer.slot4, !getItem(5).isEmpty());
			level.setBlockAndUpdate(worldPosition, state);
		}
		this.setChanged();
	}


	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return support.get();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			support.set(value);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 1;
	}
	
	public class SupportStorage extends CapabilitySupport
	{
		public SupportStorage()
		{
			super(2048, EnumEnergyMode.USE);
		}
		
		@Override
		public EnumEnergyMode getType()
		{
			return super.getType();
		}
		
		@Override
		public boolean canTransferTo(ISupportStorage other)
		{
			if(other instanceof LogisticSupportPointsWrapper)
			{
				if( ((LogisticSupportPointsWrapper)other).base.getClass() == SupportStorage.class)
					return false;
			}
			if(other.getClass() == SupportStorage.class)
			{
				return false;
			}

			return true;//super.canTransferTo(other);
		}
	}
	
	@Override
	public String getGUITitle() {
		return "gui.futurepack.flashserver.title";
	}
}
