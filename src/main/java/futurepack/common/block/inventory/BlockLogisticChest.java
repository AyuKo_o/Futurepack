package futurepack.common.block.inventory;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;

public class BlockLogisticChest extends BlockHoldingTile implements IBlockServerOnlyTickingEntity<TileEntityLogisticChest>
{
	public static final BooleanProperty FILLED = BlockStateProperties.EXTENDED;
	
	public BlockLogisticChest(Block.Properties props) 
	{
		super(props);
	}	
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(!w.isClientSide)
		{
			FPGuiHandler.GENERIC_CHEST.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder) 
	{
		builder.add(FILLED);
		super.createBlockStateDefinition(builder);
	}


	@Override
	public BlockEntityType<TileEntityLogisticChest> getTileEntityType(BlockState pState) 
	{
		return FPTileEntitys.LOGISTIC_CHEST;
	}


}
