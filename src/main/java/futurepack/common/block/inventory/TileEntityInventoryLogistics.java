package futurepack.common.block.inventory;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.common.block.logistic.LogisticItemHandlerWrapper;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.wrapper.SidedInvWrapper;

public abstract class TileEntityInventoryLogistics extends TileEntityInventoryBase implements WorldlyContainer
{

	private final LazyOptional<ILogisticInterface>[] logisticOpt;
	private final LazyOptional<IItemHandler>[] itemHandlerOpt;
	protected final LogisticStorage storage;
	
	@SuppressWarnings("unchecked")
	public TileEntityInventoryLogistics(BlockEntityType<? extends TileEntityInventoryLogistics> tileEntityTypeIn, BlockPos pos, BlockState state) 
	{
		super(tileEntityTypeIn, pos, state);
		
		logisticOpt = new LazyOptional[6];
		itemHandlerOpt = new LazyOptional[6];
		
		storage = new LogisticStorage(this, this::onLogisticChange, getLogisticTypes());
		configureLogisticStorage(storage);
	}
	
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.ITEMS)
		{
			if(itemHandlerOpt[face.get3DDataValue()]!=null)
			{
				itemHandlerOpt[face.get3DDataValue()].invalidate();
				itemHandlerOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ITEMS};
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		if(side==null)
			return super.getCapability(cap, side);
		
		if(cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemHandlerOpt[side.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) itemHandlerOpt[side.get3DDataValue()];
			}
			else
			{
				if(getLogisticStorage().getModeForFace(side, EnumLogisticType.ITEMS) == EnumLogisticIO.NONE)
				{
					return LazyOptional.empty();
				}
				else
				{
					itemHandlerOpt[side.get3DDataValue()] = LazyOptional.of(() -> new LogisticItemHandlerWrapper(getLogisticStorage().getInterfaceforSide(side), getItemHandler(side)));
					itemHandlerOpt[side.get3DDataValue()].addListener(p -> itemHandlerOpt[side.get3DDataValue()]=null);
					return (LazyOptional<T>) itemHandlerOpt[side.get3DDataValue()];
				}
			}
		}
		else if(cap == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logisticOpt[side.get3DDataValue()] != null)
			{
				return (LazyOptional<T>) logisticOpt[side.get3DDataValue()];
			}
			else
			{
				logisticOpt[side.get3DDataValue()] = LazyOptional.of(() -> getLogisticStorage().getInterfaceforSide(side));
				logisticOpt[side.get3DDataValue()].addListener(p -> logisticOpt[side.get3DDataValue()]=null);
				return (LazyOptional<T>) logisticOpt[side.get3DDataValue()];
			}
		}
		
		return super.getCapability(cap, side);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(itemHandlerOpt);
		HelperEnergyTransfer.invalidateCaps(logisticOpt);
		super.setRemoved();
	}


	@Override
	protected abstract int getInventorySize();
	
	public IItemHandler getItemHandler(Direction side)
	{
		return new SidedInvWrapper(this, side);
	}

	public LogisticStorage getLogisticStorage() 
	{
		return storage;
	}
	
	
	public abstract void configureLogisticStorage(LogisticStorage storage);
	

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		ListTag l = new ListTag();
		for(int i=0;i<items.size();i++)
		{
			if(!items.get(i).isEmpty())
			{
				CompoundTag tag = new CompoundTag();
				items.get(i).save(tag);
				tag.putInt("slot", i);
				l.add(tag);
			}
		}
		nbt.put("Items", l);
		storage.write(nbt);
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		items.clear();
		ListTag l = nbt.getList("Items", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundTag tag = l.getCompound(i);
			ItemStack is = ItemStack.of(tag);
			setItem(tag.getInt("slot"), is);
		}
		storage.read(nbt);
	}

	private int[] allSlots;
	
	@Override
	public int[] getSlotsForFace(Direction side)
	{
		if(allSlots==null)
		{
			allSlots = new int[getContainerSize()];
			for(int i=0;i<allSlots.length;i++)
				allSlots[i] = i;
		}
		return allSlots;
	}
	
	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack it, Direction side)
	{
		return storage.canInsert(side, EnumLogisticType.ITEMS) && canPlaceItem(slot, it);
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{		
		return storage.canExtract(side, EnumLogisticType.ITEMS);
	}
}
