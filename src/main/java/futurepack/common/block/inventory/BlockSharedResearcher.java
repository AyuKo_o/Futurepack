package futurepack.common.block.inventory;

import futurepack.common.FPTileEntitys;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public class BlockSharedResearcher extends BlockForscher 
{
	public BlockSharedResearcher(Properties props) 
	{
		super(props);
	}

	@Override
	public BlockEntityType<TileEntityForscher> getTileEntityType(BlockState pState)
	{
		return (BlockEntityType)FPTileEntitys.SHARED_RESEARCHER;
	}
}
