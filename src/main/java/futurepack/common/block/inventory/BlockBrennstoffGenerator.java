package futurepack.common.block.inventory;

import java.util.Random;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateable;
import futurepack.common.sync.FPGuiHandler;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FurnaceBlock;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class BlockBrennstoffGenerator extends BlockRotateable implements IBlockServerOnlyTickingEntity<TileEntityBrennstoffGenerator>
{
	private static final BooleanProperty LIT = FurnaceBlock.LIT;
	
	private static final VoxelShape[] shape = new VoxelShape[4];
	
	public  BlockBrennstoffGenerator(Block.Properties props)
	{
		super(props);
	}
	
	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext sel)
	{
		return getShape(state.getValue(HORIZONTAL_FACING));
	}

	private VoxelShape getShape(Direction face)
	{
		if(shape[face.get2DDataValue()]!=null)
			return shape[face.get2DDataValue()]; 
		else
		{
			VoxelShape s = Block.box(0, 10, 0, 16, 13, 16);
			VoxelShape side, generator;
			switch (face) 
			{
			case NORTH:
				side = Block.box(13, 0, 0, 16, 11, 16);
				generator = Block.box(0, 0, 3, 13, 11, 13);
				break;
			case SOUTH:
				side = Block.box(0, 0, 0, 3, 11, 16);
				generator = Block.box(3, 0, 3, 16, 11, 13);
				break;
			case EAST:
				side = Block.box(0, 0, 13, 16, 11, 16);
				generator = Block.box(3, 0, 0, 13, 11, 13);
				break;
			case WEST:
				side = Block.box(0, 0, 0, 16, 11, 3);
				generator = Block.box(3, 0, 3, 13, 11, 16);
				break;
			default:
				side = null;
				generator = null;
			}
			
			return shape[face.get2DDataValue()] = Shapes.or(s, Shapes.or(side, generator));
		}
	}
	
	@Override
	public void animateTick(BlockState state, Level w, BlockPos pos, Random rand)
	{
		if (state.getValue(LIT))
		{
			Direction i = state.getValue(HORIZONTAL_FACING);
			double x = pos.getX();
			double y = pos.getY() + 1.1D;
			double z = pos.getZ();
			double da = 0.6875D;
			double db = 0.15625D;

			switch (i)
			{
			case WEST:
				w.addParticle(ParticleTypes.SMOKE, x + 1D - db ,y , z + da, 0.0D, 0.0D, 0.0D);
				break;
			case NORTH:
				w.addParticle(ParticleTypes.SMOKE, x + 1D - da ,y , z + 1D - db, 0.0D, 0.0D, 0.0D);
				break;
			case EAST:
				w.addParticle(ParticleTypes.SMOKE, x + db ,y , z + 1D - da, 0.0D, 0.0D, 0.0D);
				break;
			case SOUTH:
				w.addParticle(ParticleTypes.SMOKE, x + da ,y , z + db, 0.0D, 0.0D, 0.0D);
				break;
			default:
				break;
			}
		}
	}
	
	@Override
	protected void createBlockStateDefinition(Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		builder.add(LIT);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(HelperResearch.canOpen(pl, state))
		{
			FPGuiHandler.T0_GENERATOR.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	public BlockEntityType<TileEntityBrennstoffGenerator> getTileEntityType(BlockState pState)
	{
		return FPTileEntitys.T0_GENERATOR;
	}
	

}
