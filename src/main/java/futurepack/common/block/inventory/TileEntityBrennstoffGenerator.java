package futurepack.common.block.inventory;

import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.IItemNeon;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPConfig;
import futurepack.common.FPSounds;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.FurnaceBlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.ForgeHooks;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;

public class TileEntityBrennstoffGenerator extends FPTileEntityBase implements ITilePropertyStorage, ITileServerTickable
{

//	private ItemStack[] items = new ItemStack[2];
	private int burntime = 0;
	private int maxburntime = 0;

	private CapabilityNeon energy;
	
	long last = 0;
	private ItemContainer items;
	private LazyOptional<IItemHandler> itemOpt;
	private LazyOptional<INeonEnergyStorage> neonOpt;
	
	public TileEntityBrennstoffGenerator(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.T0_GENERATOR, pos, state);
		
		items = new ItemContainer();
		energy = new CapabilityNeon(5000, EnumEnergyMode.PRODUCE);
	}
	
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("itemContainer", items.serializeNBT());
		nbt.putInt("burntime", burntime);
		nbt.putInt("maxburntime", maxburntime);
		nbt.put("energy", energy.serializeNBT());
		return nbt;
	}

	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		items.deserializeNBT(nbt.getCompound("itemContainer"));
		burntime = nbt.getInt("burntime");
		maxburntime = nbt.getInt("maxburntime");
		energy.deserializeNBT(nbt.getCompound("energy"));
	}
	
	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
		boolean bateryFull = true;
		if(!items.getStackInSlot(1).isEmpty() && energy.get() >= 1 && isBattery(items.getStackInSlot(1)))
		{
			IItemNeon ch = (IItemNeon) items.getStackInSlot(1).getItem();
			if(ch.isNeonable(items.getStackInSlot(1)))
			{
				ItemStack it = items.getStackInSlot(1);
				int neon = Math.min(10, Math.min(energy.get(), ch.getMaxNeon(it) - ch.getNeon(it)));
				if(neon>0)
				{
					ch.addNeon(items.getStackInSlot(1), neon);
					energy.use(neon);
					bateryFull = false;
				}
				
			}
				
		}
		
		if(bateryFull)
		{
			if(getEngine()>1 || energy.get() > 50)
				HelperEnergyTransfer.powerLowestBlock(this);
		}
		
		
		
		//ganz wichtig: sound :(
		if(burntime > 0 && System.currentTimeMillis() - last >= 1400)//1500
		{
			level.playSound(null, worldPosition, FPSounds.GENERATOR, SoundSource.BLOCKS, (float) (0.1F * FPConfig.CLIENT.volume_genereator.get()), 1.0F);
			last = System.currentTimeMillis();
		}
		
		Boolean update = false;
		if(burntime > 0)
		{
			burntime--;
			if(burntime % 2 == 0)
			{
				energy.add(4);
				update = true;
			}
		}
		//COAL_BLOCK butntime is 16000 = 32000 NE
		// 1m³ 
		// 1kg Steinkole = 1,016 kg SKE = 29,7765216 MJ
		// 1m³ kohle = 1300kg
		// coal_block = 38709,47808 MJ
		// 1NE = 1,20967119 MJ
		
		
		if(!items.getStackInSlot(0).isEmpty() && burntime<=0)
		{
			if(FurnaceBlockEntity.isFuel(items.getStackInSlot(0)))
			{
				int i = ForgeHooks.getBurnTime(items.getStackInSlot(0), RecipeType.SMELTING);
				if(i>0 && energy.get() < (energy.getMax() - 500))
				{
					burntime=maxburntime=i;
					ItemStack cont = items.getStackInSlot(0).getItem().getContainerItem(items.getStackInSlot(0));
					if(!cont.isEmpty())
					{
						items.setStackInSlot(0, cont.copy());
					}
					else
					{
						items.extractItem(0, 1, false, true);
					}				
					update = true;
				}
			}
		}
					
		if(update)
			setChanged();
	}
	

	public boolean isBattery(ItemStack stack)
	{
		if(stack==null || stack.isEmpty())
			return false;
		return (stack.getItem() instanceof IItemNeon);
	}

	public int getProgress() 
	{
		return burntime;
	}

	public void setProgress(int val) 
	{
		this.burntime = val;
	}

	public boolean isBurning() 
	{
		return burntime > 0;
	}

	
	public int getEngine()
	{
		double d1 = energy.get();
		double d2 = energy.getMax();
		return (int) (d1/d2 * 10D);
	}
	
	public int getBurntime()
	{
		return burntime;	
	}
	
	public int getMaxBurntime()
	{
		return maxburntime;	
	}
	
//	@Override
//	protected int getInventorySize()
//	{
//		return 2;
//	}
	
	@Override
	public int getProperty(int id) 
	{
		switch(id)
		{
		case 0:return this.burntime;
		case 1:return this.maxburntime;
		case 2:return this.energy.get();
		default: return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch(id)
		{
		case 0:
			this.burntime = value;
			break;
		case 1:
			this.maxburntime = value;
			break;
		case 2:
			int d = value - energy.get();
			if(d>0)
				energy.add(d);
			else
				energy.use(-d);
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 3;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(neonOpt != null)
				return (LazyOptional<T>) neonOpt;
			else
			{
				neonOpt = LazyOptional.of(() -> energy);
				neonOpt.addListener( p -> neonOpt = null);
				return (LazyOptional<T>) neonOpt;
			}
		}
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemOpt != null)
			{
				return (LazyOptional<T>) itemOpt;
			}
			else
			{
				itemOpt = LazyOptional.of(() -> items);
				itemOpt.addListener( p -> itemOpt = null);
				return (LazyOptional<T>) itemOpt;
			}
		}
		return super.getCapability(capability, facing);
	}
	
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(neonOpt, itemOpt);
		super.setRemoved();
	}
	
	public IItemHandlerModifiable getGui()
	{
		return new ItemHandlerGuiOverride(items);
	}
	
	private class ItemContainer extends ItemStackHandlerGuis
	{
		public ItemContainer()
		{
			super(2);
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
		{
			if(isItemValid(slot, stack))
			{
				return super.insertItem(slot, stack, simulate, gui);
			}
			else
			{
				return stack;
			}
		}		
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack, boolean gui)
		{
			if(slot == 0 && FurnaceBlockEntity.isFuel(stack))
				return true;
			else if(slot == 1 && isBattery(stack))
				return true;
			else
				return false;
		}
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(gui)
			{
				return super.extractItem(slot, amount, simulate, gui);
			}
			
			if(slot==0 && !FurnaceBlockEntity.isFuel(stacks.get(slot)))
			{
				return super.extractItem(slot, amount, simulate, gui);
			}
			else if(slot==1)
			{
				if(!isBattery(stacks.get(slot)))
				{
					return super.extractItem(slot, amount, simulate, gui);
				}
				else
				{
					IItemNeon neon = (IItemNeon) stacks.get(slot).getItem();
					if(!neon.isNeonable(stacks.get(slot)))
					{
						return super.extractItem(slot, amount, simulate, gui);
					}
				}
			}
			return ItemStack.EMPTY;
		}
	}
}
