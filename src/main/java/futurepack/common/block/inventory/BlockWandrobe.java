package futurepack.common.block.inventory;

import futurepack.common.block.BlockHoldingTile;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockWandrobe extends BlockHoldingTile
{
	public final boolean big;

	public BlockWandrobe(Block.Properties props, boolean big) 
	{
		super(props);
		this.big = big;
	}

	//big = meta==2||meta==3||meta==6||meta==7||meta==10||meta==11;
	
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand hand, BlockHitResult hit)
	{
		if(!w.isClientSide)
		{
			FPGuiHandler.GENERIC_CHEST.openGui(pl, pos);
		}
		return InteractionResult.SUCCESS;
	}
	
	

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return big ? new TileEntityWardrobe.Large(pos, state) : new TileEntityWardrobe.Normal(pos, state);
	}
	
	
}
