package futurepack.common.block.inventory;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.CapabilitySupport;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.IItemNeon;
import futurepack.api.interfaces.IItemSupport;
import futurepack.api.interfaces.IItemWithRandom;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityAssemblyTable extends TileEntityInventoryLogistics implements ITilePropertyStorage, ITileServerTickable
{
//	private ItemStack[] items = new ItemStack[6];
	public CapabilityNeon power = new CapabilityNeon(1000, EnumEnergyMode.USE);
	public CapabilitySupport support = new CapabilitySupport(100, EnumEnergyMode.USE);
	int time = 0;
	public Player user;
	private LazyOptional<INeonEnergyStorage>[] neonOpt;
	private LazyOptional<ISupportStorage>[] supportOpt;
	
	@SuppressWarnings("unchecked")
	public TileEntityAssemblyTable(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.ASSEMBLY_TABLE, pos, state);
		
		neonOpt = new LazyOptional[6];
		supportOpt = new LazyOptional[6];
	}
	
	@Override
	public void configureLogisticStorage(LogisticStorage storage) 
	{
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.removeState(EnumLogisticIO.OUT, EnumLogisticType.SUPPORT);
		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.SUPPORT);
	}
	
	@Override
	protected EnumLogisticType[] getLogisticTypes() 
	{
		return new EnumLogisticType[] {EnumLogisticType.ITEMS, EnumLogisticType.ENERGIE, EnumLogisticType.SUPPORT};
	}
	
	@Override
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.SUPPORT)
		{
			if(supportOpt[face.get3DDataValue()]!=null)
			{
				supportOpt[face.get3DDataValue()].invalidate();
				supportOpt[face.get3DDataValue()] = null;
			}
		}
		else if(type == EnumLogisticType.ENERGIE)
		{
			if(neonOpt[face.get3DDataValue()] != null)
			{
				neonOpt[face.get3DDataValue()].invalidate();
				neonOpt[face.get3DDataValue()] = null;
			}
		}
		super.onLogisticChange(face, type);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction side)
	{
		if(side==null)
			return super.getCapability(capability, side);
		
		if(capability == CapabilityNeon.cap_NEON)
		{
			return HelperEnergyTransfer.getNeonCap(neonOpt, side, this::getLogisticStorage, ()->power);
		}
		else if(capability == CapabilitySupport.cap_SUPPORT)
		{
			return HelperEnergyTransfer.getSupportCap(supportOpt, side, this::getLogisticStorage, ()->support);
		}
		
		return super.getCapability(capability, side);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(neonOpt);
		HelperEnergyTransfer.invalidateCaps(supportOpt);
		super.setRemoved();
	}
	
	@Override
	public void tickServer(Level level, BlockPos pPos, BlockState pState) 
	{
		if(power.get() < power.getMax() && !items.get(0).isEmpty() && items.get(0).getItem() instanceof IItemNeon)
		{
			IItemNeon ch = (IItemNeon) items.get(0).getItem();
			if(ch.getNeon(items.get(0))>0)
			{
				ch.addNeon(items.get(0), -1);
				power.add(1);
			}
		}
		
		if(support.get()<support.getMax() && !items.get(4).isEmpty() && items.get(4).getItem() instanceof IItemSupport)
		{
			IItemSupport ch = (IItemSupport) items.get(4).getItem();
			if(ch.getSupport(items.get(4))>0)
			{
				ch.addSupport(items.get(4), -1);
				support.add(1);
			}
		}
		
		if(user!=null)
		{
			AssemblyRecipe rec = FPAssemblyManager.instance.getMatchingRecipe(new ItemStack[]{items.get(1), items.get(2), items.get(3)});
			if(rec != null)
			{
				int pr = Math.min(power.get(), 5);
				int air =  Math.min(support.get(), 5);
				int r = 0;
				if(pr + air > 0)
					r = level.random.nextInt(pr + air);
					
				ItemStack out = rec.getOutput(new ItemStack[]{items.get(1), items.get(2), items.get(3)});
				if(HelperResearch.isUseable(user, out))
				{
					items.set(5,out);
					
					if(!items.get(5).isEmpty() && items.get(5).getItem() instanceof IItemWithRandom)
					{
						IItemWithRandom rand = (IItemWithRandom) items.get(5).getItem();
						rand.setRandomNBT(items.get(5), r);
					}
				}
				
			}
			else
			{
				items.set(5, ItemStack.EMPTY);
			}
		}
		else
		{
			items.set(5, ItemStack.EMPTY);
		}
		
	}
	
	/**
	 * Called from the Slot, if the Item is taken
	 */
	public void onUse()
	{
		if(!level.isClientSide )//&& items.get(5)!=null)
		{
			AssemblyRecipe rec = FPAssemblyManager.instance.getMatchingRecipe(new ItemStack[]{items.get(1), items.get(2), items.get(3)});
			if(rec != null)
			{
				ItemStack[] its = new ItemStack[]{items.get(1),items.get(2),items.get(3)};
				rec.useItems(its);
				items.set(1, its[0]);
				items.set(2, its[1]);
				items.set(3, its[2]);
				if(rec.getOutput().getItem() instanceof IItemWithRandom)
				{
					int pr = power.use(5);
					int air =  support.use(5);
				}
			}
		}
	}
	
	

	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		storage.write(nbt);
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		storage.read(nbt);
	}
	
	public void writeDataSynced(CompoundTag nbt)
	{
		super.writeDataSynced(nbt);
		nbt.put("energy", power.serializeNBT());
		nbt.put("support", support.serializeNBT());
//		return nbt;
	}
	
	public void readDataSynced(CompoundTag nbt)
	{
		power.deserializeNBT(nbt.getCompound("energy"));
		support.deserializeNBT(nbt.getCompound("support"));
	}

	@Override
	protected int getInventorySize()
	{
		return 6;
	}

	
	@Override
	public boolean canPlaceItem(int slot, ItemStack it) 
	{
		if(slot==0)
			return it.getItem()instanceof IItemNeon && ((IItemNeon)it.getItem()).getNeon(it)>0;
		if(slot==4)
			return it.getItem()instanceof IItemSupport && ((IItemSupport)it.getItem()).getSupport(it)>0;
		if(slot==5)
			return false;
		return true;
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,4};
	}

	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack it, Direction face)
	{	
		return storage.canInsert(face, EnumLogisticType.ITEMS) && canPlaceItem(slot, it);
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack it, Direction face)
	{
		if(storage.canExtract(face, EnumLogisticType.ITEMS))
		{
			if(slot==0)
			{
				if(it.getItem() instanceof IItemNeon)
				{
					return ((IItemNeon)it.getItem()).getNeon(it)<=0;
				}
				else
				{
					return true;
				}
			}
			else if(slot==4)
			{
				if(it.getItem() instanceof IItemSupport)
				{
					return ((IItemSupport)it.getItem()).getSupport(it)<=0;
				}
				else
				{
					return true;
				}
			}
		}
		return false;
	}
	
	@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:
			return power.get();
		case 1:
			return support.get();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			power.set(value);
			break;
		case 1:
			this.support.set(value);
			break;
		default:
			break;
		}
	}
	
	

	@Override
	public int getPropertyCount() 
	{
		return 2;
	}

	@Override
	public void clearContent() { }

	@Override
	public String getGUITitle() {
		return "gui.futurepack.assembly_table.title";
	}

}
