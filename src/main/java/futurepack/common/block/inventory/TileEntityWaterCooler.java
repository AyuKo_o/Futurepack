package futurepack.common.block.inventory;

import java.util.Random;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilityLogistic;
import futurepack.api.capabilities.ILogisticInterface;
import futurepack.api.interfaces.IFluidTankInfo.FluidTankInfo;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPLog;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.inventory.WaterCoolerManager.CoolingEntry;
import futurepack.common.block.logistic.LogisticFluidWrapper;
import futurepack.common.block.logistic.LogisticItemHandlerWrapper;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.Fluids;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.fluids.FluidAttributes;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler;
import net.minecraftforge.fluids.capability.IFluidHandler.FluidAction;
import net.minecraftforge.fluids.capability.IFluidHandlerItem;
import net.minecraftforge.fluids.capability.templates.FluidTank;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.ForgeRegistry;

public class TileEntityWaterCooler extends FPTileEntityBase implements  ITilePropertyStorage, ITileServerTickable
{
	public static final int FIELD_WATER = 0;
	public static final int FIELD_STEAM = 1;
	
	private static Random rand = new Random();
	//private boolean hasSteam;
	
	private FluidTank water = new WaterOnlyTank(FluidAttributes.BUCKET_VOLUME*5);
	private FluidTank steam = new FluidTank(FluidAttributes.BUCKET_VOLUME*50);
	private float steamLoos = 0F, waterLoos = 0F;
	
	private LazyOptional<IFluidHandler>[] fluidOpt;
	private LazyOptional<IItemHandler>[] itemOpt;
	private LazyOptional<ILogisticInterface>[] logOpt;
	
	private BucketContainer bucket;
	
//	public int f ;
	private LogisticStorage storage;
	
	@SuppressWarnings("unchecked")
	public TileEntityWaterCooler(BlockEntityType<? extends TileEntityWaterCooler> type, BlockPos pos, BlockState state)
	{
		super(type, pos, state);
		storage = new LogisticStorage(this, this::onLogisticChange, EnumLogisticType.ITEMS, EnumLogisticType.FLUIDS);
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ITEMS);
		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.FLUIDS);
		
		logOpt = new LazyOptional[6];
		fluidOpt = new LazyOptional[6];
		itemOpt = new LazyOptional[6];
		
		bucket = new BucketContainer();
		//hasSteam = FluidRegistry.isFluidRegistered(FPConfig.STEAM_NAME);
	}
	
	public TileEntityWaterCooler(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.WATER_COOLER, pos, state);
	}
	
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.ITEMS)
		{
			if(itemOpt[face.get3DDataValue()]!=null)
			{
				itemOpt[face.get3DDataValue()].invalidate();
				itemOpt[face.get3DDataValue()] = null;
			}
		}
		if(type == EnumLogisticType.FLUIDS)
		{
			if(fluidOpt[face.get3DDataValue()]!=null)
			{
				fluidOpt[face.get3DDataValue()].invalidate();
				fluidOpt[face.get3DDataValue()] = null;
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(facing==null)
			return super.getCapability(capability, facing);
		
		if(capability==CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY)
		{
			boolean isSteam = facing==getSteamDirection();
			return HelperEnergyTransfer.getFluidCap(fluidOpt, facing, this::getLogisticStorage, () -> new LogisticFluidWrapper(getInterfaceforSide(facing), isSteam ? steam : water));
		}
		else if(capability==CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(itemOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) itemOpt[facing.get3DDataValue()];
			}
			else
			{
				itemOpt[facing.get3DDataValue()] = LazyOptional.of(() -> new LogisticItemHandlerWrapper(getLogisticStorage().getInterfaceforSide(facing), bucket));
				itemOpt[facing.get3DDataValue()].addListener(p -> itemOpt[facing.get3DDataValue()] = null);
				return (LazyOptional<T>) itemOpt[facing.get3DDataValue()];
			}
		}
		else if(capability == CapabilityLogistic.cap_LOGISTIC)
		{
			if(logOpt[facing.get3DDataValue()]!=null)
			{
				return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
			else
			{
				logOpt[facing.get3DDataValue()] = LazyOptional.of(() -> getLogisticStorage().getInterfaceforSide(facing));
				logOpt[facing.get3DDataValue()].addListener(p -> logOpt[facing.get3DDataValue()] = null);
				return (LazyOptional<T>) logOpt[facing.get3DDataValue()];
			}
		}
		return super.getCapability(capability, facing);
	}
	
	private LogisticStorage getLogisticStorage()
	{
		return storage;
	}
	
	private ILogisticInterface getInterfaceforSide(Direction facing)
	{
		if(facing==getSteamDirection())
		{
			return new ILogisticInterface()
			{
				
				@Override
				public boolean isTypeSupported(EnumLogisticType type) 
				{
					return type==EnumLogisticType.FLUIDS;
				}
				
				@Override
				public EnumLogisticIO getMode(EnumLogisticType mode)
				{
					EnumLogisticIO io = storage.getModeForFace(facing, mode);
					if(mode ==EnumLogisticType.FLUIDS)
					{
						if(io.canInsert())
						{
							setMode(io.canExtract() ? EnumLogisticIO.OUT : EnumLogisticIO.NONE, mode);
							return storage.getModeForFace(facing, mode);
						}
					}
					return io;
				}

				@Override
				public boolean setMode(EnumLogisticIO log, EnumLogisticType mode)
				{
					if(mode ==EnumLogisticType.FLUIDS && log.canInsert())
						return false;
					
					return storage.setModeForFace(facing, log, mode);
				}
			};
		}
		return storage.getInterfaceforSide(facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(fluidOpt);
		HelperEnergyTransfer.invalidateCaps(itemOpt);
		HelperEnergyTransfer.invalidateCaps(logOpt);
		super.setRemoved();
	}
	
	@Override
	public void tickServer(Level level, BlockPos worldPosition, BlockState pState)
	{
		
		if(isWaterBucket(bucket.getStackInSlot(0)))
		{
			IFluidHandlerItem handler = bucket.getStackInSlot(0).getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY, null).orElseThrow(NullPointerException::new);
			FluidStack stack = new FluidStack(Fluids.WATER, water.getCapacity() - water.getFluidAmount());
			FluidStack water = handler.drain(stack, FluidAction.EXECUTE);
			
			this.water.fill(water, FluidAction.EXECUTE);
			bucket.setStackInSlot(0, handler.getContainer());
			
		}
		if(steam.getFluidAmount() > 40*FluidAttributes.BUCKET_VOLUME && steam.getFluid().getFluid().getAttributes().isGaseous(steam.getFluid()))
		{
			spawnSmoke();
			steam.drain(1, FluidAction.EXECUTE);	//TODO button
		}
		
		putSteamAway();
		
		if(water.getFluidAmount()<=0)
		{
			return;
		}
		if(steam.getFluidAmount() >= steam.getCapacity())
			return;
		
		if(water.getFluid()==null)
			return;
		
		BlockPos xyz = worldPosition.relative(getCoolingDirection());
		
//		Biome gen = world.getBiomeGenForCoords(pos);
		//f = gen.getFloatTemperature(pos)>1.0 ? 4 : gen.getFloatTemperature(pos)<0.8 ? 2 : 3;
		
		BlockEntity t = level.getBlockEntity(xyz);
		if(t instanceof TileEntityModificationBase)
		{
			TileEntityModificationBase m = (TileEntityModificationBase) t;
			CoolingEntry entry = WaterCoolerManager.INSTANCE.getCooling(water.getFluid().getFluid());
		
			
			float temp = 100.0F;
			float waterPerC = 0.07f;
			float steamPerC = 0.07f;		
			
			if(entry!=null)
			{
				temp = entry.targetTemp;
				waterPerC = entry.usedAmount;
				steamPerC = entry.usedAmountOutput;
			}
			
			
			//prevent overfill and underfill
			float max = Math.min(water.getFluidAmount() / waterPerC, (steam.getCapacity() - steam.getFluidAmount()) / steamPerC);

			float maxCooling = Math.min(max, 20);
			if(maxCooling > 0)
			{
				float used = m.setHeatCool(maxCooling, temp);
				if(used > 0)
				{
					float filled = used*steamPerC;
					if(entry!=null)
					{	
						steamLoos += used*steamPerC;
						filled = steam.fill(new FluidStack(entry.output, (int)steamLoos), FluidAction.EXECUTE);
						steamLoos -= filled;
						if(steamLoos > 10)
						{
							FPLog.logger.error("There is not enough space in the Steam Tank, it is going be over fill!!!, Details: still to fill %s, tilePos: %s", steamLoos, worldPosition);
						}
						
						if(entry.output==null || entry.output.isSame(Fluids.EMPTY))
						{
							steamLoos = 0F;
							spawnSmoke();
						}
					}
					else
						spawnSmoke();
					
					waterLoos += used*waterPerC;
					
					if((int)waterLoos >= 1) //wait if not enoguh
					{
						FluidStack drained = water.drain( (int)waterLoos, FluidAction.EXECUTE);
						
						if(drained==null)
						{
							FPLog.logger.error("Cant drain out of empty Tank! Details: toDrain %s, tank: %s, tilePos %s", waterLoos, water, worldPosition);
						}
						else
						{
							waterLoos -= drained.getAmount();
							
							if(waterLoos > 10)
							{
								FPLog.logger.error("There is not enough fluid in the water Tank, it is going be negative fill!!!, Details: still to empty %s, tilePos: %s", waterLoos, worldPosition);
							}
						}
					}
				}
			}
		}
	}

	public Direction getCoolingDirection()
	{
		return getSteamDirection().getOpposite();
	}
	
	public Direction getSteamDirection()
	{
		BlockState state = level.getBlockState(getBlockPos());
		if(state.isAir())
		{
			return Direction.UP;
		}
		return state.getValue(BlockRotateableTile.FACING);
	}
	
	private void spawnSmoke()
	{
		if(!level.isClientSide) //we send Particle packages
		{
			if(rand.nextInt(20)==0)
			{
				int side = rand.nextInt(8);
				boolean[] mxyz = new boolean[]{((side>>0)&1)==1,((side>>1)&1)==1,((side>>2)&1)==1};		
				((ServerLevel)level).sendParticles(ParticleTypes.CLOUD,  worldPosition.getX() + (mxyz[0]?1:0.5), worldPosition.getY() + (mxyz[1]?1:0.5), worldPosition.getZ() + (mxyz[2]?1:0.5), 3, 0F, 0.075F, 0F, 0.1D);
			}
		}
	}

	private void putSteamAway()
	{
		int st = steam.getFluidAmount();
		if(st<=0)
			return;
		
//		for(int i=0;i<6;i++)
//		{
//			if(i==getBlockMetadata())
//				continue;
//			
		if(!level.isClientSide)
		{	
			Direction face = getSteamDirection();

			if(storage.canExtract(face, EnumLogisticType.FLUIDS))
			{
				BlockPos xyz = worldPosition.relative(face);
				BlockEntity t = level.getBlockEntity(xyz);
				if(t != null)
				{
					LazyOptional<IFluidHandler> optHandler = t.getCapability(CapabilityFluidHandler.FLUID_HANDLER_CAPABILITY, face.getOpposite());
					optHandler.ifPresent(handler -> {
						FluidStack stack = FluidUtil.tryFluidTransfer(handler, steam, 1000, true);
						if(stack==null)
							FPLog.logger.debug("Fluid Transfer Failled!");
					});
				}
			}
		}
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		water.readFromNBT(nbt.getCompound("water"));
		steam.readFromNBT(nbt.getCompound("steam"));
		bucket.deserializeNBT(nbt.getCompound("bucket"));
		storage.read(nbt);
		steamLoos = nbt.getFloat("steamLoos");
		waterLoos = nbt.getFloat("waterLoos");
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		CompoundTag tag = new CompoundTag();
		water.writeToNBT(tag);
		nbt.put("water", tag);
		
		tag  = new CompoundTag();
		steam.writeToNBT(tag);
		nbt.put("steam", tag);
		
		nbt.put("bucket", bucket.serializeNBT());
		
		storage.write(nbt);
		nbt.putFloat("waterLoos", waterLoos);
		nbt.putFloat("steanLoos", steamLoos);
		return nbt;
	}
		

	

	


	@Override
	public int getProperty(int id) 
	{
		switch (id)
		{
		case 0:
			if(!water.getFluid().isEmpty())
				return ((ForgeRegistry<Fluid>)ForgeRegistries.FLUIDS).getID(water.getFluid().getFluid());
			return -1;
		case 1:
			if(!steam.getFluid().isEmpty())
				return ((ForgeRegistry<Fluid>)ForgeRegistries.FLUIDS).getID(steam.getFluid().getFluid());
			return -1;
		case 2:
			return water.getFluidAmount();
		case 3:
			return ( Short.MAX_VALUE * steam.getFluidAmount() ) / steam.getCapacity();
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			if(value==-1)
				water.setFluid(FluidStack.EMPTY);
			else
			{
				water.setFluid(new FluidStack(((ForgeRegistry<Fluid>)ForgeRegistries.FLUIDS).getValue(value), 1));
			}
			break;
		case 1:
			if(value==-1)
				steam.setFluid(FluidStack.EMPTY);
			else
			{
				steam.setFluid(new FluidStack(((ForgeRegistry<Fluid>)ForgeRegistries.FLUIDS).getValue(value), 1));
			}
			break;
		case 2:
			if(!water.getFluid().isEmpty())
				water.getFluid().setAmount(value);
			break;
		case 3:
			if(!steam.getFluid().isEmpty())
				steam.getFluid().setAmount((value * steam.getCapacity()) / Short.MAX_VALUE);
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount() 
	{
		return 4;
	}
	
	public FluidTankInfo getWater()
	{
		return new FluidTankInfo(water, 0);
	}
	
	public FluidTankInfo getSteam()
	{
		return new FluidTankInfo(steam, 0);
	}
	
	private boolean isWaterBucket(ItemStack it)
	{
		if(it==null)
			return false;
		
		LazyOptional<IFluidHandlerItem> handlerOpt = it.getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY, null);
		if(handlerOpt.isPresent())
		{
			IFluidHandlerItem handler = handlerOpt.orElseThrow(NullPointerException::new);
			for(int i=0;i<handler.getTanks();i++)
			{
				if(handler.getFluidInTank(i).getFluid()==Fluids.WATER)
				{
					return true;
				}			
			}
		}
		return false;
	}

//	@Override
//	protected int getInventorySize()
//	{
//		return 1;
//	}
	
	private class BucketContainer extends ItemStackHandlerGuis
	{
		public BucketContainer()
		{
			super(1);
		}
		
		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate, boolean gui)
		{
			if(isItemValid(slot, stack))
				return super.insertItem(slot, stack, simulate, gui);
			else
				return stack;
		}	
		
		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate, boolean gui)
		{
			validateSlotIndex(slot);
			
			if(!isWaterBucket(stacks.get(slot)) || gui)
				return super.extractItem(slot, amount, simulate, gui);
			return ItemStack.EMPTY;
		}
		
		@Override
		public boolean isItemValid(int slot, ItemStack stack, boolean gui)
		{
			return isWaterBucket(stack);
		}
	}
	
	
	public IItemHandler getGui()
	{
		return new ItemHandlerGuiOverride(bucket);
	}
	
	private long lastUpdate = 0;
	
	private void requestServerUpdate()//DO we even need this anymore?
	{
		if(System.currentTimeMillis() - lastUpdate > 2000)
		{
//			FPPacketHandler.CHANNEL_FUTUREPACK.sendToServer(new MessageTileDataRequest(getPos()));
			lastUpdate = System.currentTimeMillis();
		}
	}
	
	public class WaterOnlyTank extends FluidTank
	{
		public WaterOnlyTank(int cap)
		{		
			super(cap, fluid -> {
				if(fluid!=null && (fluid.getFluid() == Fluids.WATER || WaterCoolerManager.INSTANCE.isFluidAccepted(fluid.getFluid())))
				{
					return true;
				}
				return false;
			});
		}
	}

	public class Logistic extends LogisticStorage.LogInf
	{
		public Logistic(Direction side)
		{
			storage.super(side);
		}
		
		@Override
		public boolean setMode(EnumLogisticIO log, EnumLogisticType mode)
		{
			if(side == getCoolingDirection() && mode == EnumLogisticType.ITEMS)
			{
				return false;
			}
			return super.setMode(log, mode);
		}
		
		@Override
		public EnumLogisticIO getMode(EnumLogisticType mode)
		{
			if(side == getCoolingDirection() && mode == EnumLogisticType.ITEMS)
			{
				return EnumLogisticIO.NONE;
			}
			return super.getMode(mode);
		}
		
	}
}
