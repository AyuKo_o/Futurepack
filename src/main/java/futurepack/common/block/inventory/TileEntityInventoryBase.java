package futurepack.common.block.inventory;

import futurepack.common.block.FPTileEntityBase;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public abstract class TileEntityInventoryBase extends FPTileEntityBase implements Container, ITileInventoryProvider
{
	protected NonNullList<ItemStack> items;
	
	public TileEntityInventoryBase(BlockEntityType<?> tileEntityTypeIn, BlockPos pos, BlockState state)
	{
		super(tileEntityTypeIn, pos, state);
		items = NonNullList.withSize(getInventorySize(), ItemStack.EMPTY);
	}

	protected abstract int getInventorySize();
	

	

	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		ListTag l = new ListTag();
		for(int i=0;i<items.size();i++)
		{
			if(!items.get(i).isEmpty())
			{
				CompoundTag tag = new CompoundTag();
				items.get(i).save(tag);
				tag.putInt("slot", i);
				l.add(tag);
			}
		}
		nbt.put("Items", l);
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		items.clear();
		ListTag l = nbt.getList("Items", 10);
		for(int i=0;i<l.size();i++)
		{
			CompoundTag tag = l.getCompound(i);
			ItemStack is = ItemStack.of(tag);
			setItem(tag.getInt("slot"), is);
		}
	}
	
	@Override
	public int getContainerSize() 
	{
		return items.size();
	}

	@Override
	public ItemStack getItem(int var1) 
	{
		if(var1<items.size())
		{
			return items.get(var1);
		}
		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack removeItem(int slot, int amount)
    {
        if (this.items.get(slot) != null)
        {
            ItemStack itemstack;

            if (this.items.get(slot).getCount() <= amount)
            {
                itemstack = this.items.get(slot);
                this.items.set(slot, ItemStack.EMPTY);
                this.setChanged();
                return itemstack;
            }
            else
            {
                itemstack = this.items.get(slot).split(amount);

                if (this.items.get(slot).getCount() == 0)
                {
                	this.items.set(slot, ItemStack.EMPTY);
                }

                this.setChanged();
                return itemstack;
            }
        }
        else
        {
            return ItemStack.EMPTY;
        }
    }

	@Override
	public ItemStack removeItemNoUpdate(int index)
	{
		if(getItem(index)!=null)
		{
			ItemStack stack = items.get(index);
			items.remove(index);
			return stack;
		}
		return null;
	}
	
	@Override
	public void setItem(int var1, ItemStack var2) 
	{
		items.set(var1, var2);
		this.setChanged();
	}

	@Override
	public int getMaxStackSize() 
	{
		return 64;
	}


	@Override
	public void startOpen(Player pl) {}

	@Override
	public void stopOpen(Player pl) {}

	@Override
	public boolean canPlaceItem(int var1, ItemStack var2) 
	{
		return true;
	}

	@Override
	public void clearContent() { }
	

	
	//TODO: this must to ALL TileEntitys
	@Override
	public boolean stillValid(Player player)
	{
		return HelperResearch.isUseable(player, this);
	}
	
	@Override
	public boolean isEmpty()
	{
		return false;
	}
	
	@Override
	public Container getInventory() 
	{
		return this;
	}
}
