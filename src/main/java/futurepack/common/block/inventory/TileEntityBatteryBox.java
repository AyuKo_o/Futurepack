package futurepack.common.block.inventory;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.interfaces.IItemNeon;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.WorldlyContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityBatteryBox extends TileEntityInventoryLogistics implements WorldlyContainer, ITilePropertyStorage, ITileServerTickable
{
	public NeonStorage energy = new NeonStorage();
	//boolean powered;
//	private int lastCharge = 0;

	private boolean powering = false;
	private boolean needSync;

	private LazyOptional<INeonEnergyStorage>[] neonOpt;

	private float averageEnergyAdded=0F, averageEnergyRemoved=0F;

	public TileEntityBatteryBox(BlockPos pos, BlockState state)
	{
		super(FPTileEntitys.BATTERY_BOX, pos, state);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void configureLogisticStorage(LogisticStorage storage)
	{
		neonOpt = new LazyOptional[6];

		storage.removeState(EnumLogisticIO.INOUT, EnumLogisticType.ENERGIE);
		storage.setDefaut(EnumLogisticIO.IN, EnumLogisticType.ENERGIE);
		storage.setModeForFace(Direction.DOWN, EnumLogisticIO.OUT, EnumLogisticType.ENERGIE);

		storage.setDefaut(EnumLogisticIO.INOUT, EnumLogisticType.ITEMS);
	}

	@Override
	protected EnumLogisticType[] getLogisticTypes()
	{
		return new EnumLogisticType[]{EnumLogisticType.ENERGIE, EnumLogisticType.ITEMS};
	}

	@Override
	protected void onLogisticChange(Direction face, EnumLogisticType type)
	{
		if(type == EnumLogisticType.ENERGIE)
		{
			if(neonOpt[face.get3DDataValue()]!=null)
			{
				neonOpt[face.get3DDataValue()].invalidate();
				neonOpt[face.get3DDataValue()] = null;
			}
		}
	}

	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side)
	{
		if(side==null)
			return super.getCapability(cap, side);

		if(cap == CapabilityNeon.cap_NEON)
		{
			return HelperEnergyTransfer.getNeonCap(neonOpt, side, this::getLogisticStorage, ()->energy);
		}

		return super.getCapability(cap, side);
	}

	@Override
	public void setRemoved()
	{
		HelperEnergyTransfer.invalidateCaps(neonOpt);
		super.setRemoved();
	}

	@Override
	protected int getInventorySize()
	{
		return 14;
	}


	@Override
	public void tickServer(Level level, BlockPos pos, BlockState state)
	{
		cooldown--;

		//boolean powered = world.isBlockIndirectlyGettingPowered(xCoord, yCoord, zCoord);
		for(int i=0;i<10;i++)
		{
			float ladung = (float)energy.get() / (float)energy.getMax();
			if(ladung < 0.25)
			{
				laden(true);
			}
			else if (ladung > 0.75)
			{
				laden(false);
			}
		}
		if(!items.get(0).isEmpty() && items.get(0).getItem() instanceof IItemNeon)
		{
			ItemStack item = items.get(0);
			IItemNeon ch = (IItemNeon) item.getItem();

			if(ch.isNeonable(item))
			{
				int diff = ch.getMaxNeon(item)-ch.getNeon(item);
				if(diff>0)
				{
					int neon = Math.min(Math.max(30, ch.getMaxNeon(item) / 100), diff);
					neon = Math.min(neon, energy.get());
					ch.addNeon(item, neon);
					energy.use(neon);
				}
			}
		}
		if(!items.get(1).isEmpty() && items.get(1).getItem() instanceof IItemNeon)
		{
			ItemStack item = items.get(1);
			IItemNeon ch = (IItemNeon) item.getItem();

			if(ch.getNeon(item) > 0)
			{
				int diff = energy.getMax()-energy.get();
				if(diff>0)
				{
					int neon = Math.min(Math.max(30, ch.getMaxNeon(item) / 100), diff);
					neon = Math.min(neon, ch.getNeon(item));
					energy.add(neon);
					ch.addNeon(item, -neon);
				}
			}
		}

		float lambda = 0.1F;//calculating energy added/removed with EWMA algorythm
		averageEnergyAdded = lambda * energy.sumEnergyAdded + (1F-lambda) * averageEnergyAdded;
		averageEnergyRemoved = lambda * energy.sumEnergyRemoved + (1F-lambda) * averageEnergyRemoved;
		energy.sumEnergyAdded=0;
		energy.sumEnergyRemoved=0;

		if(needSync && cooldown <= 0)
		{
			onPowerChanged();
			needSync = false;
		}

		powering=true;
		HelperEnergyTransfer.powerLowestBlock(this);
		powering=false;
	}

	private void laden(boolean aufab)
	{
		energy.isRecording = false;

		for(int i=2;i<items.size();i++)
		{
			ItemStack it = items.get(i);
			if(!it.isEmpty() && it.getItem() instanceof IItemNeon)
			{
				IItemNeon charge = (IItemNeon) it.getItem();
				int maxRate = Math.max(charge.getMaxNeon(it) / 4000, 1);

				if(aufab)
				{
					int amount = Math.min(charge.getNeon(it), maxRate);

					if(amount > 0)
					{
						amount = energy.add(amount);
						charge.addNeon(it, -amount);
					}
				}
				else
				{
					int amount = Math.min(charge.getMaxNeon(it) - charge.getNeon(it), maxRate);

					if(amount > 0)
					{
						amount = energy.use(amount);
						charge.addNeon(it, amount);
					}
				}

			}
		}

		energy.isRecording = true;
	}

	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("energy", energy.serializeNBT());
		return nbt;
	}

	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		energy.deserializeNBT(nbt.getCompound("energy"));
	}

	@Override
	public boolean canPlaceItem(int var1, ItemStack var2)
	{
		return var2!=null && var2.getItem() instanceof IItemNeon;
	}

	@Override
	public int[] getSlotsForFace(Direction var1)
	{
		return new int[]{0,1};
	}

	@Override
	public boolean canPlaceItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		return storage.canInsert(side, EnumLogisticType.ITEMS);
	}

	@Override
	public boolean canTakeItemThroughFace(int slot, ItemStack var2, Direction side)
	{
		return storage.canExtract(side, EnumLogisticType.ITEMS);
	}


	@Override
	public int getProperty(int id)
	{
		switch (id)
		{
		case 0:
			return energy.get();
		case 1:
			return (int) averageEnergyAdded;
		case 2:
			return (int) averageEnergyRemoved;
		default:
			return 0;
		}
	}

	@Override
	public void setProperty(int id, int value)
	{
		switch (id)
		{
		case 0:
			int d = value - energy.get();
			if(d>0)
				energy.add(d);
			else
				energy.use(-d);
			break;
		case 1:
			averageEnergyAdded = value;
			break;
		case 2:
			averageEnergyRemoved = value;
			break;
		default:
			break;
		}
	}

	@Override
	public int getPropertyCount()
	{
		return 3;
	}

	@Override
	public void clearContent() { }

	private int cooldown = 0;

	/**
	 * @param charged: true -> charged ; false -> dischaged
	 */
	private void onPowerChanged()	//TODO this is wrong and not alled often enough
	{
		float p = (float) energy.get() /  (float)energy.getMax();
		int fill = (int) (8 * p);
		int fillH = fill /2;
		if(fillH*2 < fill)
		{
			fillH+=1;
		}

		if(level.setBlockAndUpdate(worldPosition, getBlockState().setValue(BlockBatteryBox.CHARGE, fillH)))
		{
			setChanged();
		}
		cooldown=5;

	}


	@Override
	public int getMaxStackSize()
	{
		return 1;
	}

	public class NeonStorage extends CapabilityNeon
	{
		boolean isRecording = true;
		int sumEnergyAdded = 0;
		int sumEnergyRemoved = 0;

		public NeonStorage()
		{
			super(18000, EnumEnergyMode.USE);
		}

		@Override
		public EnumEnergyMode getType()
		{
			return powering ? EnumEnergyMode.PRODUCE : EnumEnergyMode.USE;
		}

		@Override
		public int add(int added)
		{
			int a = super.add(added);
			if(a>0)
				needSync = true;
			if(isRecording && !level.isClientSide)
				sumEnergyAdded += a;
			return a;
		}

		@Override
		public int use(int used)
		{
			int u = super.use(used);
			if(u>0)
				needSync  = true;
			if(isRecording && !level.isClientSide)
				sumEnergyRemoved += u;
			return u;
		}
	}

	@Override
	public String getGUITitle() {
		return "gui.futurepack.batterybox.title";
	}

	public float getAverageEnergyAdded()
	{
		return averageEnergyAdded;
	}

	public float getAverageEnergyRemoved()
	{
		return averageEnergyRemoved;
	}
}
