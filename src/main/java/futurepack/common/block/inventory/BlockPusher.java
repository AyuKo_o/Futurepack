package futurepack.common.block.inventory;

import java.util.function.Supplier;

import futurepack.api.interfaces.IBlockServerOnlyTickingEntity;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class BlockPusher extends BlockRotateableTile implements IBlockServerOnlyTickingEntity<TileEntityPusher>
{
	private final Supplier<BlockEntityType<? extends TileEntityPusher>> constructor;
	
	protected BlockPusher(Block.Properties props) 
	{
		this(props, () -> FPTileEntitys.PUSHER);
	}

	protected BlockPusher(Block.Properties props, Supplier<BlockEntityType<? extends TileEntityPusher>> supplier) 
	{
		super(props);
		this.constructor = supplier;
	}
	
	@Override
	public void onRemove(BlockState state, Level w, BlockPos pos, BlockState newState, boolean isMoving)
	{
		TileEntityPusher p = (TileEntityPusher) w.getBlockEntity(pos);
		if(p!=null)
			p.onBlockBreak();
		
		super.onRemove(state, w, pos, newState, isMoving);
	}
	
	@Override
	public InteractionResult use(BlockState state, Level w, BlockPos pos, Player pl, InteractionHand handIn, BlockHitResult hit) 
	{
		if(!w.isClientSide)
		{
			FPGuiHandler.GENERIC_CHEST.openGui(pl, pos);
			return InteractionResult.SUCCESS;
		}
		return InteractionResult.SUCCESS;
	}

	@Override
	public BlockEntityType<TileEntityPusher> getTileEntityType(BlockState pState)
	{
		return (BlockEntityType<TileEntityPusher>) constructor.get();
	}
}
