package futurepack.common.block.inventory;

import java.util.function.Supplier;

import futurepack.api.interfaces.IBlockBothSidesTickingEntity;
import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.sync.FPGuiHandler;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;

public class BlockModulClientTicking<T extends BlockEntity & ITileServerTickable & ITileClientTickable> extends BlockModul<T> implements IBlockBothSidesTickingEntity<T>
{
	public BlockModulClientTicking(Block.Properties props, Supplier<BlockEntityType<T>> type, Supplier<FPGuiHandler> supGui)
	{	
		super(props, type, supGui);
	}
}
