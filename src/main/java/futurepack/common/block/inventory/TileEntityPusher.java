package futurepack.common.block.inventory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;

import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.BlockRotateableTile;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.filter.OrGateFilter;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperInventory.SlotContent;
import futurepack.depend.api.helper.HelperItemFilter;
import futurepack.depend.api.interfaces.ITileInventoryProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.Container;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

public class TileEntityPusher extends FPTileEntityBase implements ITileInventoryProvider, ITileServerTickable
{
	boolean last = true;
	private ItemStack item = ItemStack.EMPTY;
	
	private InsertSide insert = new InsertSide();
	private ExtractSide extract = new ExtractSide();
	private LazyOptional<IItemHandler> inputOpt, outputOpt;
	
	private SimpleContainer filter_items;
	private OrGateFilter last_item_filter;
	
	public TileEntityPusher(BlockPos pos, BlockState state) 
	{
		this(FPTileEntitys.PUSHER, pos, state);
	}

	public TileEntityPusher(BlockEntityType<? extends TileEntityPusher> type, BlockPos pos, BlockState state) 
	{
		super(type, pos, state);
		
		filter_items = new SimpleContainer(3);
		filter_items.addListener(inv -> {
			last_item_filter = null;
		});
	}
	
	@Override
	public void tickServer(Level level, BlockPos pos, BlockState pState)
	{
		boolean now = level.getBestNeighborSignal(pos)>0;
		if(now && !last)
		{
			neighborChanged(level, pos, null);
		}
		last = now;
		
		pushOutItems();
	}

	protected void pushOutItems()
	{
		if(!item.isEmpty())
		{
			ArrayList<SlotContent> items = new ArrayList<SlotContent>(Arrays.asList(new SlotContent(extract, 0, item, null)));
			BlockPos target = worldPosition.relative(getOutput());
			List<SlotContent> done = HelperInventory.insertItems(level, target, getInput(), items);
			for(SlotContent slot : done)
			{
				slot.remove();
			}
			done  = HelperInventory.ejectItemsIntoWorld(level, target, items);
			for(SlotContent slot : done)
			{
				slot.remove();
			}
			for(SlotContent slot : items)
			{
				slot.updateSrc();
			}
		}
	}
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.putBoolean("last", last);
		if(!item.isEmpty())
		{
			CompoundTag tag = new CompoundTag();
			item.save(tag);
			nbt.put("item", tag);
		}
		HelperInventory.storeInventory("filters", nbt, filter_items);
		
		return nbt;
	}
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		last = nbt.getBoolean("last");
		item=ItemStack.EMPTY;
		if(nbt.contains("item"))
		{
			CompoundTag tag =  nbt.getCompound("item");
			item = ItemStack.of(tag);
		}
		HelperInventory.loadInventory("filters", nbt, filter_items);
	}
	

	public void neighborChanged(Level w, BlockPos jkl, Block bl) 
	{
		neighborChanged(w, jkl, bl, false);
	}	

	public void neighborChanged(Level w, BlockPos jkl, Block bl, boolean ignoreRedStone) 
	{
		if(!w.isClientSide && (ignoreRedStone || w.getBestNeighborSignal(jkl)>0))
		{
			ArrayList<SlotContent> items = new ArrayList<SlotContent>(getItems(w, jkl, getInput()));
			if(item.isEmpty() && !items.isEmpty())
			{
				for(SlotContent slot : items)
				{
					IItemFilter filter = getFilter();
					if(filter==null || filter.test(slot.item))
					{
						this.item = slot.item;
						slot.remove();
						if(filter!=null)
							filter.amountTransfered(this.item);
						break;
					}
				}
			}
		}			
	}
	
	@Nullable
	private IItemFilter getFilter()
	{
		if(last_item_filter!=null)
			return last_item_filter;
		
		last_item_filter = HelperItemFilter.createBasicFilter(filter_items.getItem(0), filter_items.getItem(1), filter_items.getItem(2));
		if(last_item_filter.isEmpty())
		{
			last_item_filter = null;
		}
		return last_item_filter;
	}
	
	protected ArrayList<SlotContent> getItems(Level w, BlockPos pos, Direction face)
	{
		final ArrayList<SlotContent> items = new ArrayList<SlotContent>();
		BlockPos xyz = pos.relative(face);
		BlockEntity tile = w.getBlockEntity(xyz);
		IItemHandler handler = HelperInventory.getHandler(tile, face.getOpposite());
		if(handler!=null)
		{
			for(int i=0;i<handler.getSlots();i++)
			{
				if(!handler.getStackInSlot(i).isEmpty() )
				{
					ItemStack item = handler.extractItem(i, 64, true);
					if(item!=null && item.getCount()>0 && !item.isEmpty())
						items.add(new SlotContent(handler, i, item, null));
				}
			}		
		}

		w.getEntitiesOfClass(ItemEntity.class, new AABB(xyz.getX(),xyz.getY(),xyz.getZ(), xyz.getX()+1,xyz.getY()+1,xyz.getZ()+1), new Predicate<ItemEntity>() //getEntitiesWithinAABBExcludingEntity
		{		
			@Override
			public boolean apply(ItemEntity var1)
			{
				ItemEntity it = var1;
				if(it.isAlive() && !it.getItem().isEmpty())
				{
					items.add(new SlotContent(null, 0, it.getItem(),it));
					//it.setDead();
				}
				return false;
			}
		});
		return items;
	}
	
	
	private Direction getInput()
	{
		BlockState state = getBlockState();
		if(state.getBlock() != InventoryBlocks.pusher)
		{
			return Direction.UP;
		}
		Direction face = state.getValue(BlockRotateableTile.FACING);
		return face;
	}
	
	private Direction getOutput()
	{
		return getInput().getOpposite();
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY)
		{
			if(facing == getInput())
			{
				if(inputOpt!=null)
				{
					return (LazyOptional<T>) inputOpt;
				}
				else
				{
					inputOpt = LazyOptional.of(()-> insert);
					inputOpt.addListener(p -> inputOpt = null);
					return (LazyOptional<T>) inputOpt;
				}
			}
			else if(facing == getOutput())
			{
				if(outputOpt!=null)
				{
					return (LazyOptional<T>) outputOpt;
				}
				else
				{
					outputOpt = LazyOptional.of(()-> extract);
					outputOpt.addListener(p -> outputOpt = null);
					return (LazyOptional<T>) outputOpt;
				}
			}
		}
		
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(inputOpt, outputOpt);
		super.setRemoved();
	}
	
	public class InsertSide implements IItemHandler
	{

		@Override
		public int getSlots()
		{
			return 1;
		}

		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return slot==0? TileEntityPusher.this.item : ItemStack.EMPTY;
		}

		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
		{
			if (stack == null || stack.isEmpty())
				return ItemStack.EMPTY;
			if(!isItemValid(slot, stack))	
				return stack;
			
			
	        int ms = stack.getMaxStackSize();

	        if (item!=null && !item.isEmpty())
	        {
	            if (!ItemHandlerHelper.canItemStacksStack(stack, item))
	                return stack;

	            ms -= item.getCount();
	        }

	        if (ms <= 0)
	            return stack;

	        boolean big = stack.getCount() > ms;
	        IItemFilter filter = getFilter();
	        
	        if (!simulate)
	        {
	            if (item == null || item.isEmpty())
	            {
	            	item = big ? ItemHandlerHelper.copyStackWithSize(stack, ms) : stack;
	            	
	            	if(filter!=null)
	            	{
	            		filter.amountTransfered(item);
	            	}
	            }
	            else
	            {
	            	  item.grow(big ? ms : stack.getCount());
	            	  if(filter!=null)
	            	  {
	            		  ItemStack st = item.copy();
	            		  st.setCount( big ? ms : stack.getCount());
	            		  filter.amountTransfered(st);
	            	  }
	            }
	        }

	        return big ? ItemHandlerHelper.copyStackWithSize(stack, stack.getCount() - ms) : ItemStack.EMPTY;
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			return ItemStack.EMPTY;
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return 64;
		}

		@Override
		public boolean isItemValid(int slot, ItemStack stack) 
		{
			return (getFilter()==null || getFilter().test(stack)) && !last; // this is to block items - Mantes feature
		}
	}
	
	public class ExtractSide implements IItemHandler
	{

		@Override
		public int getSlots()
		{
			return 1;
		}

		@Override
		public ItemStack getStackInSlot(int slot)
		{
			return slot==0? TileEntityPusher.this.item : ItemStack.EMPTY;
		}

		@Override
		public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) 
		{
			return stack;
		}

		@Override
		public ItemStack extractItem(int slot, int amount, boolean simulate)
		{
			if (amount == 0 && slot!=0)
				return ItemStack.EMPTY;
			if (item == null || item.isEmpty())
				return ItemStack.EMPTY;

			int out = Math.min(amount, item.getMaxStackSize());
			
			if (item.getCount() <= out)
			{
				if (!simulate)
				{
					item = ItemStack.EMPTY;
				}
				return item;
			}
			else
			{
				if (!simulate)
				{
					item = ItemHandlerHelper.copyStackWithSize(item, item.getCount() - out);
				}			
				return ItemHandlerHelper.copyStackWithSize(item, out);
			}
		}

		@Override
		public int getSlotLimit(int slot)
		{
			return 64;
		}

		@Override
		public boolean isItemValid(int slot, ItemStack stack) 
		{
			return false;
		}		
	}

	public void onBlockBreak()
	{
		if(item!=null && !item.isEmpty())
		{
			ItemEntity ent = new ItemEntity(level, worldPosition.getX()+0.5, worldPosition.getY()+0.5, worldPosition.getZ()+0.5, item);
			level.addFreshEntity(ent);
			item = ItemStack.EMPTY;
		}
	}
	
	@Override
	public Container getInventory() 
	{
		return filter_items;
	}

	@Override
	public String getGUITitle() {
		return "gui.futurepack.pusher.title";
	}
}
