package futurepack.common.block;

import net.minecraft.core.Direction;

/**
 * Ported methods from 1.14 to 1.15 because they got removed and are needed by the sorter
 *
 */
public class FPDirectionHelper {
	
	
	public static Direction rotateY(Direction dir) {
	      switch(dir) {
	      case NORTH:
	         return Direction.EAST;
	      case EAST:
	         return Direction.SOUTH;
	      case SOUTH:
	         return Direction.WEST;
	      case WEST:
	         return Direction.NORTH;
	      default:
	         throw new IllegalStateException("Unable to get Y-rotated facing of " + dir);
	      }
	   }

	   /**
	    * Rotate this Facing around the X axis (NORTH => DOWN => SOUTH => UP => NORTH)
	    */
	   private static Direction rotateX(Direction dir) {
	      switch(dir) {
	      case NORTH:
	         return Direction.DOWN;
	      case EAST:
	      case WEST:
	      default:
	         throw new IllegalStateException("Unable to get X-rotated facing of " + dir);
	      case SOUTH:
	         return Direction.UP;
	      case UP:
	         return Direction.NORTH;
	      case DOWN:
	         return Direction.SOUTH;
	      }
	   }

	   /**
	    * Rotate this Facing around the Z axis (EAST => DOWN => WEST => UP => EAST)
	    */
	   private static Direction rotateZ(Direction dir) {
	      switch(dir) {
	      case EAST:
	         return Direction.DOWN;
	      case SOUTH:
	      default:
	         throw new IllegalStateException("Unable to get Z-rotated facing of " + dir);
	      case WEST:
	         return Direction.UP;
	      case UP:
	         return Direction.EAST;
	      case DOWN:
	         return Direction.WEST;
	      }
	   }
	
	public static Direction rotateAround(Direction dir, Direction.Axis axis) {
	      switch(axis) {
	      case X:
	         if (dir != Direction.WEST && dir != Direction.EAST) {
	            return rotateX(dir);
	         }

	         return dir;
	      case Y:
	         if (dir != Direction.UP && dir != Direction.DOWN) {
	            return rotateY(dir);
	         }

	         return dir;
	      case Z:
	         if (dir != Direction.NORTH && dir != Direction.SOUTH) {
	            return rotateZ(dir);
	         }

	         return dir;
	      default:
	         throw new IllegalStateException("Unable to get CW facing for axis " + axis);
	      }
	   }
}
