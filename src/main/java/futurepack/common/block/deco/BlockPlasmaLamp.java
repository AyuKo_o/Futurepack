package futurepack.common.block.deco;

import java.util.Random;

import javax.annotation.Nullable;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.RedstoneLampBlock;
import net.minecraft.world.level.block.state.BlockState;

public class BlockPlasmaLamp extends RedstoneLampBlock
{

	public BlockPlasmaLamp(Properties properties) 
	{
		super(properties);
	}

	@Override
	@Nullable
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return this.defaultBlockState().setValue(LIT, Boolean.valueOf(!context.getLevel().hasNeighborSignal(context.getClickedPos())));
	}

	@Override
	public void neighborChanged(BlockState state, Level worldIn, BlockPos pos, Block blockIn, BlockPos fromPos, boolean isMoving)
	{
		if (!worldIn.isClientSide) 
		{
			boolean flag = state.getValue(LIT);
			if (flag == worldIn.hasNeighborSignal(pos)) 
			{
				if (flag) 
				{
					worldIn.scheduleTick(pos, this, 4);
				}
				else 
				{
					worldIn.setBlock(pos, state.cycle(LIT), 2);
				}
			}

		}
	}

	@Override
	public void tick(BlockState state, ServerLevel worldIn, BlockPos pos, Random random)
	{
		if (!worldIn.isClientSide) 
		{
			if (state.getValue(LIT) && worldIn.hasNeighborSignal(pos)) 
			{
				worldIn.setBlock(pos, state.cycle(LIT), 2);
			}
		}
	}

}
