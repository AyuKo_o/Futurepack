package futurepack.common.block.deco;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.registries.RegistryObject;

public class BlockTreppe extends StairBlock
{
	public BlockTreppe(BlockState state, Block.Properties build)
	{
		super(state, build);
	}

	public BlockTreppe(BlockState state)
	{
		this(state, Block.Properties.copy(state.getBlock()).noOcclusion());
	}

	public BlockTreppe(Block b)
	{
		this(b.defaultBlockState(), Block.Properties.copy(b).noOcclusion());
	}

	public BlockTreppe(RegistryObject<Block> b)
	{
		this(b.get().defaultBlockState(), Block.Properties.copy(b.get()).noOcclusion());
	}
}
