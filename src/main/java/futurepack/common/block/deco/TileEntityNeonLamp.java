package futurepack.common.block.deco;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import futurepack.api.ParentCoords;
import futurepack.api.capabilities.CapabilityNeon;
import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import futurepack.common.FPBlockSelector;
import futurepack.common.FPTileEntitys;
import futurepack.common.block.FPTileEntityBase;
import futurepack.common.block.TileEntityLinkedLight;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.depend.api.helper.HelperEnergyTransfer;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.monster.Husk;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RedstoneLampBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;

public class TileEntityNeonLamp extends FPTileEntityBase implements ITileServerTickable
{

	public TileEntityNeonLamp(BlockPos pos, BlockState state) 
	{
		super(FPTileEntitys.NEON_LAMP, pos, state);
	}

	public CapabilityNeon power = new CapabilityNeon(100, EnumEnergyMode.USE);
	public LazyOptional<CapabilityNeon> opt;
	private int cooldown = 0;
	private Set<BlockPos> blockSet;
	private Iterator<BlockPos> blockLips;
	private boolean allLightsPlaced = false;
	private int checkCooldown = 0;
	
	@Override
	public CompoundTag writeDataUnsynced(CompoundTag nbt)
	{
		super.writeDataUnsynced(nbt);
		nbt.put("energy", power.serializeNBT());
		return nbt;
	} 
	
	@Override
	public void readDataUnsynced(CompoundTag nbt)
	{
		super.readDataUnsynced(nbt);
		power.deserializeNBT(nbt.getCompound("energy"));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction facing)
	{
		if(capability == CapabilityNeon.cap_NEON)
		{
			if(opt==null)
			{
				opt = LazyOptional.of(()->power);
				opt.addListener(p -> opt = null);
			}
			return (LazyOptional<T>) opt;
		}
		return super.getCapability(capability, facing);
	}
	
	@Override
	public void setRemoved() 
	{
		HelperEnergyTransfer.invalidateCaps(opt);
		
		super.setRemoved();
	}
	
	@Override
	public void tickServer(Level pLevel, BlockPos pPos, BlockState state) 
	{
		if(cooldown>0)
			cooldown--;
		else
		{
			cooldown = allLightsPlaced ? 20 : 3;
			
			if(state.hasProperty(RedstoneLampBlock.LIT))
			{
				state.tick((ServerLevel)level, worldPosition, level.random);
					
				if(state.getValue(RedstoneLampBlock.LIT))
				{	
					burnZombies(level, worldPosition, 10);
					placeAdditionalLights();
				}
				else
				{
					blockLips = null;
					allLightsPlaced = false;
						
					if(power.get() >= power.getMax())
					{
						if(level.hasNeighborSignal(worldPosition))
						{
							state.neighborChanged(level, worldPosition, Blocks.AIR, worldPosition.above(), false);
						}
					}
				}
			}
		}
	}
	
	public static void burnZombies(Level w, BlockPos start, int range)
	{
		List<LivingEntity> list = w.getEntitiesOfClass(LivingEntity.class, new AABB(start.offset(-range, -range, -range), start.offset(1+range,1+range,1+range)), new Predicate<LivingEntity>()
		{

			@Override
			public boolean test(LivingEntity t)
			{
				if(t.isInvertedHealAndHarm())
				{
					if(t instanceof Husk)
					{
						return false; // ideal check if the should brun in day, but method is private so for now I will use this...
					}
					return true;	
				}
				return false;
			}
		});
		list.forEach(e -> e.setSecondsOnFire(20));
	}

	public boolean isPowered() 
	{		
		return cooldown==0 ? power.use(1)>0 : power.get() > 0;
	}
	
	final IBlockValidator validatorAllAir = new IBlockValidator()
	{
		@Override
		public boolean isValidBlock(Level w, ParentCoords pos) 
		{
			return w.getBlockState(pos).getMaterial() == Material.AIR;
		}
	};
	
	final IBlockValidator validatorBlockAir = new IBlockValidator()
	{
		@Override
		public boolean isValidBlock(Level w, ParentCoords pos) 
		{
			return w.getBlockState(pos).getBlock() == Blocks.AIR;
		}
	};
	
	
	
	public void placeAdditionalLights()
	{
		if(blockLips == null)
		{
			LightSelector selector = new LightSelector(getBlockPos());
			FPBlockSelector bs = new FPBlockSelector(level, selector);
			bs.selectBlocks(worldPosition);
			blockSet = new HashSet<>(bs.getValidBlocks(validatorAllAir));
			ArrayList<BlockPos> list = new ArrayList<>(bs.getValidBlocks(validatorBlockAir));
			Collections.shuffle(list);
			blockLips = list.iterator();
			bs.clear();
			if(list.isEmpty())
			{
				checkCooldown = 120;
			}
		}
		
		if(blockLips.hasNext())
		{
			BlockPos pos = blockLips.next();
			if(level.isEmptyBlock(pos))
			{
				if(level.setBlockAndUpdate(pos, MiscBlocks.linked_light.defaultBlockState()))
				{
					TileEntityLinkedLight light = (TileEntityLinkedLight) level.getBlockEntity(pos);
					light.setLinkedBlock(getBlockPos(), getBlockState());
					light.stillValid = this::isStillValid;
				}
				
			}
		}
		else if(checkCooldown > 0)
		{
			checkCooldown--;
		}
		else
		{
			allLightsPlaced = true;
			blockLips = null;
		}
	}
	
	public boolean isStillValid(TileEntityLinkedLight tile)
	{
		if(isPowered())
		{
			if(blockSet!=null)
			{
				return  blockSet.contains(tile.getBlockPos());
			}
		}
		return false;
	}
}
