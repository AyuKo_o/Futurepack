package futurepack.common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.Future;

import net.minecraft.Util;
import net.minecraft.network.chat.Component;

public class AsyncTaskManager 
{
	public static <T> Future<T> addTask(Stage<T> stage, Runnable r, T value)
	{
		Future<T> t = getExecutor().submit(r, value);
		stage.add(t);
		return t;
	}
	
	public static <T> Future<T> addTask(Stage<T> stage, Callable<T> call)
	{
		Future<T> t = getExecutor().submit(call);
		stage.add(t);
		return t;
	}
	
	public static ExecutorService getExecutor()
	{
		return Util.backgroundExecutor();
	}
	
	
	static abstract interface Stage<T>
	{
		public abstract void add(Future<T> task);
		
		public abstract List<T> join();
		
		public abstract boolean joinWithStats();
	}
	
	private static class BasicStage implements Stage<Boolean>
	{
		private final Object LOCK = new Object();
		private List<Future<Boolean>> tasks = null;
		private final String name;
		
		public BasicStage(String name) 
		{
			this.name = name;
		}
		
		@Override
		public void add(Future<Boolean> task)
		{
			synchronized(LOCK)
			{
				if(tasks==null)
				{
					tasks = new ArrayList<>();
				}
				tasks.add(task);
			}
		}
		
		@Override
		public List<Boolean> join()
		{
			List<Future<Boolean>> tasks;
			synchronized(LOCK)
			{
				tasks = this.tasks;
				this.tasks = null;
			}
			
			if(tasks==null)
			{
				return Collections.emptyList();
			}
			
			ArrayList<Boolean> results = new ArrayList<Boolean>(tasks.size());
			tasks.forEach(t -> {
				try {
					results.add(t.get());
				} catch (InterruptedException | ExecutionException  e) {
					e.printStackTrace();
					results.add(false);
				}
			});
			return results;
		}
		
		@Override
		public boolean joinWithStats()
		{
			long time = System.currentTimeMillis();
			List<Boolean> result = join();
			time = System.currentTimeMillis() - time;
			int total = result.size();
			result.removeIf(b -> b==null || b.booleanValue());
			int failed = result.size();
			FPLog.logger.info("[%s] Finished %s tasks, %s failed, took %s ms", name,  total, failed, time);
			
			return failed == 0;
		}
	}
	
	public static final Stage<Boolean> ASYNC_RESTARTABLE = new BasicStage("ASYNC");
	
	public static final Stage<Boolean> FILE_IO = new BasicStage("File IO");
	
	public static final Stage<Boolean> RESOURCE_RELOAD = new BasicStage("Reseource Relaod");

	public static <T> T join(Future<T> task) 
	{
		try {
			return task.get();
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		} 
	}
	
}
