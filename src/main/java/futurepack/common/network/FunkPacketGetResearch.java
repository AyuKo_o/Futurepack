package futurepack.common.network;

import futurepack.api.PacketBase;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.research.Research;
import net.minecraft.core.BlockPos;

public class FunkPacketGetResearch extends PacketBase
{
	public Research research;
	
	public FunkPacketGetResearch(BlockPos src, ITileNetwork net)
	{
		super(src, net);
		
	}

}
