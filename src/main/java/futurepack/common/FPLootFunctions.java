package futurepack.common;

import futurepack.api.Constants;
import futurepack.world.loot.LootFunctionSetupBattery;
import futurepack.world.loot.LootFunctionSetupChip;
import futurepack.world.loot.LootFunctionSetupCore;
import futurepack.world.loot.LootFunctionSetupRam;
import net.minecraft.core.Registry;
import net.minecraft.world.level.storage.loot.functions.LootItemFunctionType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

public class FPLootFunctions 
{
	public static final DeferredRegister<LootItemFunctionType> LOOT_FUNCTION_TYPE = DeferredRegister.create(Registry.LOOT_FUNCTION_REGISTRY, Constants.MOD_ID);
	//public static final RegistryObject<LootItemFunctionType> TEST_RECIPE_TYPE = LOOT_FUNCTION_TYPE.register("fp_setup_chip", () ->  new LootFunctionSetupChip.Storage());
	
	
	public static final RegistryObject<LootItemFunctionType> SETUP_CHIP = LOOT_FUNCTION_TYPE.register("fp_setup_chip", () ->  new LootItemFunctionType(new LootFunctionSetupChip.Storage()));
	public static final RegistryObject<LootItemFunctionType> SETUP_CORE = LOOT_FUNCTION_TYPE.register("fp_setup_core", () ->  new LootItemFunctionType(new LootFunctionSetupCore.Storage()));
	public static final RegistryObject<LootItemFunctionType> SETUP_RAM = LOOT_FUNCTION_TYPE.register("fp_setup_ram", () ->  new LootItemFunctionType(new LootFunctionSetupRam.Storage()));
	public static final RegistryObject<LootItemFunctionType> SETUP_BATTERY = LOOT_FUNCTION_TYPE.register("fp_setup_battery", () ->  new LootItemFunctionType(new LootFunctionSetupBattery.Storage()));
	
	public static void init(IEventBus modBus)
	{
		LOOT_FUNCTION_TYPE.register(modBus);
	}
}
