package futurepack.common.gui.inventory;

import java.util.List;

import org.lwjgl.glfw.GLFW;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.common.sync.FPGuiHandler;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperItemFilter;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.components.Button;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.FormattedCharSequence;
import net.minecraft.world.Containers;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ClickType;
import net.minecraft.world.inventory.DataSlot;
import net.minecraft.world.inventory.ResultContainer;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiJSFilterTester extends ActuallyUseableContainerScreen<GuiJSFilterTester.ContainerJSFilterTester>
{
	private Button save;
	private final ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/filter_tester.png");


	public GuiJSFilterTester(Player pl)
	{
		super(new ContainerJSFilterTester(pl.getInventory()), pl.getInventory(), "item.futurepack.script_filter");
		imageWidth = 176;
		imageHeight = 228;
	}

	@Override
	public void init()
	{
		super.init();

		int w = 18+this.font.width("Edit Script");
		addRenderableWidget(save = new Button(leftPos +imageWidth - 7 -w, topPos+17, w, 20, new TextComponent("Edit Script"), b ->  {
			getMenu().type = 0;
			FPPacketHandler.syncWithServer(getMenu());
		}));
		addRenderableWidget(new Button(leftPos + 26, topPos+17, 18+this.font.width("Test"), 20, new TextComponent("Test"), b ->  {
			getMenu().type = 1;
			FPPacketHandler.syncWithServer(getMenu());
		}));


		//TODO: add multiple slot for testing & tets buttons
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int button)
	{

		return super.mouseClicked(mouseX, mouseY, button);
	}

	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int button)
	{

		return super.mouseReleased(mouseX, mouseY, button);
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);


//		this.font.drawSplitString("Compiler: " + getContainer().compileMsg, guiLeft+5, guiTop + (ySize-56)+20+15, xSize-10, 0xFF11FF11);

		//TODO remove resizing, gui has mutliple filter slots; add area with data of filter.

		//93px height, 162px

		int lines =  93 / this.font.lineHeight;
		ItemStack filter = getFilter();
		CompoundTag nbt = filter.getOrCreateTagElement("display");
		if(nbt.contains("Lore"))
		{
			ListTag list = nbt.getList("Lore", 8);//StringNBT
			String s = "";
			for(int i=0;i<Math.min(lines, list.size());i++)
			{
				s += list.getString(i);
			}
			List<FormattedCharSequence> widthWrapped = this.font.split(new TextComponent(s), 160);
			int yOffset = 0;
			for(FormattedCharSequence rp : widthWrapped)
			{
				this.font.drawShadow(matrixStack, rp, leftPos +8, topPos +40+yOffset, 0xFFFFDDDD);
				yOffset+= this.font.lineHeight;
			}

		}
		else
		{
			//get data string

			CompoundTag script = filter.getTagElement("script");
			String s = "";
			if(script != null && script.contains("extraData"))
			{
				s = script.getCompound("extraData").toString();
			}

			List<FormattedCharSequence> widthWrapped = this.font.split(new TextComponent(s), 160);
			int yOffset = 0;
			for(FormattedCharSequence rp : widthWrapped)
			{
				this.font.drawShadow(matrixStack, rp, leftPos +8, topPos +40+yOffset, 0xFFFFFFFF);
				yOffset+= this.font.lineHeight;
			}
		}

		byte state = getMenu().state;
		HelperComponent.renderSymbol(matrixStack, leftPos+74, topPos+18, getBlitOffset(), state==0 ? 16 : (state==2 ? 18 : 19));

	}

	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY)
	{
		super.renderLabels(matrixStack, mouseX, mouseY);

		this.font.draw(matrixStack, this.title.getString(), 8.0F, 6.0F, 4210752);
		this.font.draw(matrixStack, this.playerInventoryTitle.getString(), 8.0F, this.imageHeight - 96 + 2, 4210752);
	}

	@Override
	public void removed()
	{
		super.removed();
	}

	@Override
	public void mouseMoved(double mouseX, double mouseY)
	{
		super.mouseMoved(mouseX, mouseY);
	}

	@Override
	public boolean keyPressed(int keyCode, int scanCode, int modifiers)
	{
		if(hasControlDown() && keyCode == GLFW.GLFW_KEY_S)
		{
			save.mouseClicked(save.x+1, save.y+1, 0);//triggering the button with all actiaveted checks
			return true;
		}
		return super.keyPressed(keyCode, scanCode, modifiers);
	}

	private ItemStack getFilter()
	{
		return getMenu().pl.getInventory().getItem(getMenu().selected);
	}

	public static class ContainerJSFilterTester extends ActuallyUseableContainer implements IGuiSyncronisedContainer
	{
		private Player pl;
		protected final int selected;
		private ResultContainer inventory;

		private byte type;
		protected byte state = 0;

		public ContainerJSFilterTester(Inventory inv)
		{
			this.pl = inv.player;

			selected = inv.selected;
			//test slot at 8, 19s
			inventory = new ResultContainer();
			addSlot(new Slot(inventory, 0, 8, 19));

			HelperContainerSync.addInventorySlots(8, 146, inv, this::addSlot);

			addDataSlot(new DataSlot()
			{
				@Override
				public void set(int p)
				{
					state = (byte) p;
				}

				@Override
				public int get()
				{
					return state;
				}
			});
		}

		@Override
		public void clicked(int slotId, int dragType, ClickType clickTypeIn, Player player)
		{
			if(slotId > 0)
			{
				Slot s = getSlot(slotId);
				if(s.getSlotIndex() == selected)//can not move filter item
				{
					return;
				}
			}

			super.clicked(slotId, dragType, clickTypeIn, player);
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}


		@Override
		public void writeToBuffer(FriendlyByteBuf buf)
		{
			buf.writeByte(type);
		}

		@Override
		public void removed(Player playerIn)
		{
			Containers.dropContents(playerIn.level, playerIn, inventory); //what if everything was as easy as this?
			super.removed(playerIn);
		}

		private void testSlot(int inventorySlot)
		{
			state = 0;
			ItemStack filterItem = pl.getInventory().getItem(selected);
			filterItem.getOrCreateTagElement("display").remove("Lore");
			IItemFilter filter = HelperItemFilter.getFilter(filterItem);
			ItemStack totest = inventory.getItem(inventorySlot);
			if(!totest.isEmpty())
			{
				boolean accepted = filter.test(totest);
				if(accepted)
				{
					filter.amountTransfered(totest);
				}
				state = (byte) (accepted ? 1 : 2);
			}

		}

		@Override
		public void readFromBuffer(FriendlyByteBuf buf)
		{
			type = buf.readByte();
			switch(type)
			{
			case 0:
				FPGuiHandler.JS_FILTER_EDITOR.openGui((ServerPlayer)pl, (Object[])null);
				break;
			case 1:
				testSlot(0);
				break;
			}
		}
	}
}
