package futurepack.common.gui.inventory;

import futurepack.common.block.inventory.TileEntityTechtable;
import futurepack.common.recipes.crafting.InventoryCraftingForResearch;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.item.ItemStack;

public class InventoryCraftingTechtable extends InventoryCraftingForResearch
{
	private final TileEntityTechtable tile;
	private final AbstractContainerMenu eventHandler;

	public InventoryCraftingTechtable(AbstractContainerMenu con, int width, int height, Player user, TileEntityTechtable tech)
	{
		super(con, width, height, user);
		eventHandler = con;
		tile = tech;
	}
	
	
	@Override
	public ItemStack getItem(int index) 
	{
		return tile.getItem(index);
	}
	
	@Override
	public ItemStack removeItemNoUpdate(int index)
	{
		ItemStack it = tile.removeItemNoUpdate(index);
		eventHandler.slotsChanged(this);
		return it;
	}
	
	@Override
	public ItemStack removeItem(int index, int count)
	{
		ItemStack it = tile.removeItem(index, count);
		eventHandler.slotsChanged(this);
		return it;
	}
	
	@Override
	public void setItem(int index, ItemStack stack)
	{
		tile.setItem(index, stack);
		eventHandler.slotsChanged(this);
	}
	
	@Override
	public void clearContent()
	{
		super.clearContent();
	}
}
