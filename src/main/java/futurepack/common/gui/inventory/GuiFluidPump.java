package futurepack.common.gui.inventory;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.interfaces.IFluidTankInfo.FluidTankInfo;
import futurepack.common.block.modification.TileEntityFluidPump;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.SlotItemHandler;

public class GuiFluidPump extends GuiModificationBase<TileEntityFluidPump>
{
	public GuiFluidPump(Player pl, TileEntityFluidPump tile)
	{
		super(new ContainerPump(pl.getInventory(), tile), "fluid_pump.png", pl.getInventory());
		
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);

		FluidTankInfo info = tile().getFluid();
		if(!info.isEmpty())
		{
			HelperGui.renderFluidTank(leftPos+80, topPos+29, 16, 48, info, mouseX, mouseY, 10F);
		}
		
		RenderSystem.setShaderTexture(0, super.res);
		GlStateManager._enableBlend();
		this.setBlitOffset(20);
		this.blit(matrixStack, leftPos+79, topPos+28, 176, 0, 18, 50);
		this.setBlitOffset(0);
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
        
        FluidTankInfo info = tile().getFluid();    
		if(!info.isEmpty())
		{
			HelperGui.renderFluidTankTooltip(matrixStack, leftPos+80, topPos+29, 16, 48, info, mouseX, mouseY);
		}
    }
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY)
	{
		super.renderLabels(matrixStack, mouseX, mouseY);
	}
	
	@Override
	public TileEntityFluidPump tile()
	{
		return ((ContainerPump)this.getMenu()).pump;
	}

	public static class ContainerPump extends ContainerSyncBase
	{
		TileEntityFluidPump pump;

		public ContainerPump(Inventory inv, TileEntityFluidPump tile)
		{
			super(tile, tile.getLevel().isClientSide());
			pump = tile;
			
			this.addSlot(new SlotItemHandler(tile.getGui(), 0, 44, 27));
			this.addSlot(new SlotItemHandler(tile.getGui(), 1, 44, 63));
			
			this.addSlot(new SlotItemHandler(tile.getGui(), 2, 80, 7));
			
			this.addSlot(new SlotItemHandler(tile.getGui(), 3, 116, 27));
			this.addSlot(new SlotItemHandler(tile.getGui(), 4, 116, 63));
			
			int l;
			int i1;
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return HelperResearch.isUseable(playerIn, pump);
		}
		
		@Override
		public ItemStack quickMoveStack(Player playerIn, int index)
		{
			
			return ItemStack.EMPTY;
		}
	}
}
