package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.TileEntityModulT1Calculation;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperResearch;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiModulT1Calculation extends GuiModificationBase<TileEntityModulT1Calculation>
{
	public GuiModulT1Calculation(Player player, TileEntityModulT1Calculation tile)
	{
		super(new ContainerModulT1Calculation(player.getInventory(), tile), "calculation.png", player.getInventory());
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
//		renderSun(matrixStack, leftPos +67, topPos +13, tile().light);
		
		//h 72, w : 145
		fill(matrixStack, leftPos +24, topPos+7, leftPos +169, topPos+79, 0xFF888888);
		
		double[] p = tile().getPowerGraph();
		double[] w = tile().getWorkingGraph();
		
		
		matrixStack.pushPose();
		
		int diagramm_width = 145 -2;
		int diagramm_height = (72 -4) / 2;
		
		int bar_w = 4;
		
		float x_scale = diagramm_width / (float)(bar_w*Math.max(p.length, w.length)); //3 px width;
		
		matrixStack.translate(leftPos +24+1, topPos+7+1, 0);
		matrixStack.scale(x_scale, 1F, 1F);
		
		for(int i=0;i<p.length;i++)
		{
			int d_y_ende = diagramm_height;
			int d_y_start = (int)(d_y_ende - p[i]*diagramm_height);
			
			if(d_y_start < d_y_ende)
				fillGradient(matrixStack, i*bar_w, d_y_start, i*bar_w+bar_w-1, d_y_ende, 0xFF11FFFF, 0xFF0099aa);
			else
				fillGradient(matrixStack, i*bar_w, d_y_ende-1, i*bar_w+bar_w-1, d_y_ende, 0xFF11FFFF, 0xFF0099aa);
			
			d_y_ende = diagramm_height*2 +2;
			d_y_start = (int)(d_y_ende - w[i]*diagramm_height);
			
			if(d_y_start < d_y_ende)
				fillGradient(matrixStack, i*bar_w, d_y_start, i*bar_w+bar_w-1, d_y_ende, 0xFFFF1111, 0xFFaa0000);
			else
				fillGradient(matrixStack, i*bar_w, d_y_ende-1, i*bar_w+bar_w-1, d_y_ende, 0xFFFF1111, 0xFFaa0000);
		}
		
		matrixStack.popPose();
	}
	
	@Override
	public TileEntityModulT1Calculation tile()
	{
		return ((ContainerModulT1Calculation)this.getMenu()).tile;
	}
	
	public static class ContainerModulT1Calculation extends ContainerSyncBase
	{

		protected final TileEntityModulT1Calculation tile;
		
		public ContainerModulT1Calculation(Inventory inv, TileEntityModulT1Calculation tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
//			this.addSlot(new SlotBaseXPOutput(inv.player, tile, 0, 26, 11));
			
			 
			
			HelperContainerSync.addInventorySlots(8, 84, inv, this::addSlot);
		}
		
		@Override
		public boolean stillValid(Player var1)
		{
			return HelperResearch.isUseable(var1, tile);
		}
		
		@Override
		public ItemStack quickMoveStack(Player par1EntityPlayer, int par2)
		{
			 Slot slot = this.slots.get(par2);
			 if(slot !=null && slot.hasItem())
			 {
				 if(par2==0)
				 {
					 this.moveItemStackTo(slot.getItem(), 1, this.slots.size(), false);
				 }
				 else
				 {
					 this.moveItemStackTo(slot.getItem(), 0, 1, false);
				 }
				 
				 if(slot.getItem().getCount()<=0)
				 {
					 slot.set(ItemStack.EMPTY);
				 }
				 this.broadcastChanges();
			 }
			return ItemStack.EMPTY;
		}
		
	}
}
