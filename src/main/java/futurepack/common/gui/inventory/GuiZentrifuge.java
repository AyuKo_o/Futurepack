package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.machines.TileEntityZentrifuge;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotUses;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.FurnaceResultSlot;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiZentrifuge extends GuiMachineSupport<TileEntityZentrifuge>
{
	public GuiZentrifuge(Player pl, TileEntityZentrifuge tile)
	{
		super(new ContainerZentrifuge(pl.getInventory(), tile), "zentrifuge.png", pl.getInventory());
	}
	
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		
		int f2 = (int) (tile().getProgress() * 24);
		this.blit(matrixStack, leftPos+92, topPos+58, 176, 0, f2, 17);
		
		//PartRenderer.renderSupport(matrixStack, guiLeft+158, guiTop+7, tile().getSupport(), mouseX, mouseY);
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		super.renderLabels(matrixStack, p_146979_1_, p_146979_2_);
		//PartRenderer.renderSupportTooltip(matrixStack, guiLeft, guiTop, 158, 7, tile().getSupport(), p_146979_1_, p_146979_2_);
	}

	@Override
	public TileEntityZentrifuge tile()
	{
		return ((ContainerZentrifuge)this.getMenu()).tile;
	}
	
	public static class ContainerZentrifuge extends ContainerSyncBase
	{
		TileEntityZentrifuge tile;
		
		public ContainerZentrifuge(Inventory pl, TileEntityZentrifuge tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			
			this.addSlot(new SlotUses(this.tile, 0, 62, 34)); //Smelt Input
			
			this.addSlot(new FurnaceResultSlot(pl.player, this.tile, 1, 125, 9)); //Smelt Output
			this.addSlot(new FurnaceResultSlot(pl.player, this.tile, 2, 134, 34)); //Smelt Output
			this.addSlot(new FurnaceResultSlot(pl.player, this.tile, 3, 125, 59)); //Smelt Output
		
			int l;
			int i1;
			
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(pl, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(pl, l, 8 + l * 18, 142));
			}
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int index)
		{
			 Slot slot = this.slots.get(index);
			 ItemStack itemstack = ItemStack.EMPTY;
			 
			 if (slot != null && slot.hasItem())
			 {
				 ItemStack itemstack1 = slot.getItem();
				 itemstack = itemstack1.copy();
				 
				 if(index<4)
				 {
					 if (!this.moveItemStackTo(itemstack1, 4, 40, true))
					 {
						 return ItemStack.EMPTY;
					 }

					 slot.onQuickCraft(itemstack1, itemstack);
				 }
				 else
				 {
					 if (!this.moveItemStackTo(itemstack1, 0, 1, true))
					 {
						 return ItemStack.EMPTY;
					 }

					 slot.onQuickCraft(itemstack1, itemstack);
				 }
				 
				 
				 if (itemstack1.getCount() == 0)
				 {
					 slot.set(ItemStack.EMPTY);
				 }
				 else
				 {
					 slot.setChanged();
				 }

				 if (itemstack1.getCount() == itemstack.getCount())
				 {
					 return ItemStack.EMPTY;
				 }
			 }
			return itemstack;
		}
		
		
	}
}
