package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.FuturepackTags;
import futurepack.common.block.modification.machines.BlueprintAssemblyRecipe;
import futurepack.common.gui.SlotUses;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.common.research.CustomPlayerData;
import futurepack.common.sync.FPGuiHandler;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageResearchResponse;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.core.BlockPos;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ResultContainer;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.network.PacketDistributor;

public class GuiAssemblyRezeptCreator extends ActuallyUseableContainerScreen<GuiAssemblyRezeptCreator.ContainerAssemblyRezeptCreator>
{
	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/rezeptassembly.png");	
	
	public GuiAssemblyRezeptCreator(Player pl, BlockPos pos)
	{
		super(new ContainerAssemblyRezeptCreator(pl, pos), pl.getInventory(), "gui.assemply.recipe.creator");
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	public void init() 
	{
		super.init();
		HelperGui.RestoreCursorPos();
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
    {
        this.font.draw(matrixStack, I18n.get("gui.futurepack.assembly.recipe.title", new Object[0]), 28-21, 6, 4210752);
        this.font.draw(matrixStack, I18n.get("container.inventory", new Object[0]), 28-15, this.imageHeight - 96 +3, 4210752);
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float var1, int mx,int my) 
	{
		 HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		 RenderSystem.setShaderTexture(0, res);
		 int k = (this.width - this.imageWidth) / 2;
		 int l = (this.height - this.imageHeight) / 2;
		 this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);		 
	}
	
	@Override
	public boolean mouseReleased(double mx, double my, int w) 
	{
		ContainerAssemblyRezeptCreator c = getMenu();
		if(HelperComponent.isInBox(mx -leftPos, my -topPos, 116, 10, 134, 28) && w==0)
		{		
			 c.back = true;
			 HelperGui.SaveCursorPos();
			 FPPacketHandler.syncWithServer(c);
		}
		
		return super.mouseReleased(mx, my, w);
	}
	
	public static class ContainerAssemblyRezeptCreator extends ActuallyUseableContainer implements IGuiSyncronisedContainer
	{
		
		private boolean back = false;
		public Container in1 = new ResultContainer();
		public Container in2 = new ResultContainer();
		public Container in3 = new ResultContainer();
		
		public Container out = new ResultContainer();
		public Container recIn = new ResultContainer();
		public Container recOut = new ResultContainer();
		
		public Player player;
		private BlockPos pos;
		
		public ContainerAssemblyRezeptCreator(Player pl, BlockPos pos)
		{
			player = pl;
			this.pos = pos;
			int x=26,y=40;
			this.addSlot(new SlotInput(in1, 0, x,y));
			this.addSlot(new SlotInput(in2, 0, x+18,y));
			this.addSlot(new SlotInput(in3, 0, x+18*2,y));
			
			x=98;y=11;
			this.addSlot(new SlotInput(recIn, 0, x,y));
			x=134;y=61;
			this.addSlot(new GuiConstructionTable.SlotClick(recOut, recIn, 0, x,y));
			
			this.addSlot(new SlotOutput(out, 0, 98, 40));//output
			
			HelperContainerSync.addInventorySlots(8, 84, player.getInventory(), this::addSlot);
			
			if(!pl.level.isClientSide && pl instanceof ServerPlayer)
			{
				ServerPlayer mp = (ServerPlayer) pl;
				MessageResearchResponse mes =  new MessageResearchResponse(pl.getServer(), CustomPlayerData.getDataFromPlayer(pl));
				FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> mp), mes);
			}
		}
		
		@Override
		public void writeToBuffer(FriendlyByteBuf buf)  
		{
			buf.writeBoolean(back);
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf buf) 
		{
			if(buf.readBoolean())
			{
				FPGuiHandler.OPTI_ASSEMBLER.openGui(player, pos);
			}
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}
		
		@Override
		public ItemStack quickMoveStack(Player playerIn, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
	        Slot slot = this.slots.get(index);

	        if (slot != null && slot.hasItem())
	        {
	            ItemStack itemstack1 = slot.getItem();
	            itemstack = itemstack1.copy();

	            if(index < 6)
	            {
	            	if (!this.moveItemStackTo(itemstack1, 6, this.slots.size(), true))
	                {
	                    return ItemStack.EMPTY;
	                }
	            	if(index==5)
	            	{
	            		slot.onQuickCraft(itemstack, itemstack1);
	            	}
	            }
	            else
	            {
	            	if (!this.moveItemStackTo(itemstack1, 0, 4, false))
	                {
	                    return ItemStack.EMPTY;
	                }
	            }

	            if (itemstack1.isEmpty())
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }

	            if (itemstack1.getCount() == itemstack.getCount())
	            {
	                return ItemStack.EMPTY;
	            }

	            slot.onTake(playerIn, itemstack1);
	            broadcastChanges();
	        }
			return itemstack;
		}
		
		protected void updateSlotContent(boolean useInput)
		{
			ItemStack[] in = new ItemStack[]{in1.getItem(0), in2.getItem(2), in3.getItem(3)};
			ItemStack output = ItemStack.EMPTY;
			AssemblyRecipe ass = FPAssemblyManager.instance.getMatchingRecipe(in);
			
			if(ass==null)
			{
				out.setItem(0, ItemStack.EMPTY);
				updateRecipe(null, ItemStack.EMPTY);
				return;
			}
			
			output = ass.getOutput(in);
			if(HelperResearch.isUseable(player, output))
			{
				if(!useInput)
					out.setItem(0, output);
				updateRecipe(ass, output);
				
				if(useInput)
				{
					ass.useItems(in);
					in1.setItem(0, in[0]);
					in2.setItem(0, in[1]);
					in3.setItem(0, in[2]);
				}
			}
			else
			{
				updateRecipe(null, ItemStack.EMPTY);
				output = ItemStack.EMPTY;
				return;
			}
		}
		
		protected void updateRecipe(AssemblyRecipe rec, ItemStack output)
		{
			ItemStack assemblyRec = ItemStack.EMPTY;
			
			ItemStack cristall = recIn.getItem(0);
			if(!cristall.isEmpty() && cristall.is(FuturepackTags.item_crystals))
			{
				if(rec !=null && output!=null)
				{
					assemblyRec = new BlueprintAssemblyRecipe(player, rec).createRecipeItem();
				}
				recOut.setItem(0, assemblyRec);
			}
		}
		
		@Override
		public void removed(Player player)
		{
			Container[] inves = new Container[] {in1,in2,in3,recIn};
			for(Container inv : inves)
			{
				for(int i=0;i<inv.getContainerSize();i++)
				{
					ItemStack it = inv.getItem(i);
					if(!it.isEmpty())
					{
						player.drop(it, false);
						inv.setItem(i, ItemStack.EMPTY);
					}
				}
			}
			super.removed(player);
		}
		
		private class SlotOutput extends SlotUses
		{
			public SlotOutput(Container par1iInventory, int index, int x, int y)
			{
				super(par1iInventory, index, x, y);
			}
			
			@Override
			public void onTake(Player thePlayer, ItemStack stack)
			{
				updateSlotContent(true);
				super.onTake(thePlayer, stack);
			}
		}
		
		private class SlotInput extends SlotUses
		{

			public SlotInput(Container par1iInventory, int index, int x, int y)
			{
				super(par1iInventory, index, x, y);
			}
			
			@Override
			public void setChanged()
			{
				super.setChanged();
				updateSlotContent(false);
			}
		}
	}
}
