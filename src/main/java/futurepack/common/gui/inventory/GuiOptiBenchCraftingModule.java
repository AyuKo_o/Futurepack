package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityOptiBenchCraftingModule;
import futurepack.common.block.modification.machines.TileEntityOptiBench;
import futurepack.common.gui.inventory.GuiOptiBench.ContainerOptiBench;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;

public class GuiOptiBenchCraftingModule extends ActuallyUseableContainerScreen<ContainerOptiBench<TileEntityOptiBenchCraftingModule>>
{
	protected ResourceLocation res;
	int nx=7, ny=7;
	
	public GuiOptiBenchCraftingModule(Player pl, TileEntityOptiBenchCraftingModule tile)
	{
		super(new GuiOptiBench.ContainerOptiBench(pl.getInventory(),tile), pl.getInventory(), "gui.opti_crafting");
		res = new ResourceLocation(Constants.MOD_ID, "textures/gui/opti_crafting.png");
		
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
    public TileEntityOptiBenchCraftingModule tile()
    {
    	return ((GuiOptiBench.ContainerOptiBench<TileEntityOptiBenchCraftingModule>)this.getMenu()).tile;
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		RenderSystem.setShaderTexture(0, res);
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		this.blit(matrixStack, leftPos, topPos, 0, 0, this.imageWidth, this.imageHeight);
		this.blit(matrixStack, leftPos+7, topPos+7, 7+8*18-15, 7, 12, 18*4);//hide old energy bar
		this.blit(matrixStack, leftPos+7+8*18+1, topPos+7, 7+8*18-15, 7, 16, 18*4);//hide old sp bar
		this.blit(matrixStack, leftPos+7+8*18+17, topPos+7, 7+8*18-15, 7, 1, 18*4);//hide old sp bar

		
        this.blit(matrixStack, leftPos+80, topPos+34, 176, 0, (int)(29 * (tile().getProperty(TileEntityOptiBench.FIELD_PROGRESS)/10D)), 18);
        
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY)
	{
		//Removed to remove default text (inventory and gui name) super.drawGuiContainerForegroundLayer(matrixStack, mouseX, mouseY);
	}

	@Override
	public boolean mouseReleased(double mx, double my, int but) 
	{
		if(HelperComponent.isInBox(mx-leftPos, my-topPos, 96, 10, 114, 28))
		{
			if(but == 0)
			{
				HelperGui.SaveCursorPos();
				FPPacketHandler.syncWithServer((IGuiSyncronisedContainer) this.getMenu());
				return true;
			}
		}
		return super.mouseReleased(mx, my, but);
	}

}
