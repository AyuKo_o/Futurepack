package futurepack.common.gui.inventory;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.machines.TileEntityOptiAssembler;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotUses;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.sync.FPGuiHandler;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiOptiAssembler extends GuiMachineSupport<TileEntityOptiAssembler>
{

	public GuiOptiAssembler(Player player, TileEntityOptiAssembler tile)
	{
		super(new ContainerOptiAssembler(player.getInventory(), tile), "opti_assembler.png", player.getInventory());
	}

	@Override
	public TileEntityOptiAssembler tile()
	{
		return ((ContainerOptiAssembler)this.getMenu()).tile;
	}
	
	@Override
	public void init() 
	{
		super.init();
		HelperGui.RestoreCursorPos();
	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float f, int mx, int my)
    {
    	super.renderBg(matrixStack, f, mx, my);     
    	int progress = tile().getProperty(TileEntityOptiAssembler.FIELD_PROGRESS);
    	int state = progress / 20;
    	progress %= 20;
        this.blit(matrixStack, leftPos+100, topPos+17, 180, 50*state, (int)(30.0 * (progress/20D)), 50);
  
        //PartRenderer.renderSupport(matrixStack, guiLeft+158, guiTop+7, tile().getSupport(), mx, my);
    }
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		super.renderLabels(matrixStack, p_146979_1_, p_146979_2_);
		//PartRenderer.renderSupportTooltip(matrixStack, guiLeft, guiTop, 158, 7, tile().getSupport(), p_146979_1_, p_146979_2_);
	}
	
	@Override
	public boolean mouseReleased(double mx, double my, int but) 
	{
        if(HelperComponent.isInBox(mx-leftPos, my-topPos, 132, 7, 150, 25) && but == 0)
        {
        	HelperGui.SaveCursorPos();
        	FPPacketHandler.syncWithServer((ContainerOptiAssembler)this.getMenu());
        }
		return super.mouseReleased(mx, my, but);
	}
	
	public static class ContainerOptiAssembler extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		private TileEntityOptiAssembler tile;
		private Player player;
		
		public ContainerOptiAssembler(Inventory inv, TileEntityOptiAssembler tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			player = inv.player;
			
			int x, y;
			for(y=0;y<3;y++)
			{
				for(x=0;x<3;x++)
				{
					addSlot(new SlotUses(tile, y*3 + x, 26 + x*18, 11 + y*23));//recipe input
				}
			}
			
			for(y=0;y<3;y++)
			{
				addSlot(new SlotUses(tile, 9 +y, 26 + 3*18, 11 + y*23));//blueprints
			}
			
			addSlot(new SlotUses(tile, 12, 133, 34));//output
			
			
			//Add PLayer slots;
			HelperContainerSync.addInventorySlots(8, 84, inv, this::addSlot);
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf nbt)  
		{
			
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf nbt) 
		{
			FPGuiHandler.OPTI_ASSEMBLER_RECIPES.openGui(player, tile.getBlockPos());
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return HelperResearch.isUseable(playerIn, tile);
		}
		
		@Override
		public ItemStack quickMoveStack(Player pl, int slotNumber)
		{
			if(!pl.level.isClientSide)
			{
				Slot slot = this.slots.get(slotNumber);
		        if(slot != null && slot.hasItem())
		        {
		        	if(slot.container == tile) {
		        		if(!this.moveItemStackTo(slot.getItem(), 13, slots.size(), false))
		        			return ItemStack.EMPTY;
		        	}
		        	else
		        	{
		        		if(slot.getItem().getItem() == MiscItems.assembly_recipe) 
		        		{
		        			if(!this.moveItemStackTo(slot.getItem(), 9, 12, false))
		        				return ItemStack.EMPTY;
		        		}
		        		else 
		        		{
		        			if(!this.moveItemStackTo(slot.getItem(), 0, 9, false))
		        				return ItemStack.EMPTY;
		        		}
		        	}
		        }
			}
			this.broadcastChanges();
			return ItemStack.EMPTY;
		}
	}
}
