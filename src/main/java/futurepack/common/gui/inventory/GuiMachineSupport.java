package futurepack.common.gui.inventory;

import java.util.Arrays;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.capabilities.IEnergyStorageBase.EnumEnergyMode;
import futurepack.common.block.modification.machines.TileEntityMachineSupport;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperGui;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;

public abstract class GuiMachineSupport<T extends TileEntityMachineSupport> extends GuiModificationBase<T>
{
    protected int sx = 158, sy = 7;

	public GuiMachineSupport(AbstractContainerMenu cont, String gui, Inventory inv)
	{
		super(cont, gui, inv);
	}
	

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY) 
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		HelperGui.renderSupport(matrixStack, leftPos+sx, topPos+sy, tile().getSupport(), mouseX, mouseY);

		if(tile().getChipPower(EnumChipType.SUPPORT) <= 0 && tile().getSupport().getType() == EnumEnergyMode.PRODUCE)
		{	
			HelperComponent.renderSymbol(matrixStack, leftPos-20, topPos+18*2, getBlitOffset(), 6); //SP symbol
			HelperComponent.renderSymbol(matrixStack, leftPos-20, topPos+18*2, getBlitOffset(), 18); //X symbol
		}
		RenderSystem.setShaderTexture(0, res);
	}

	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY)
	{
		super.renderLabels(matrixStack, mouseX, mouseY);
		HelperGui.renderSupportTooltip(matrixStack, leftPos, topPos, sx, sy, tile().getSupport(), mouseX, mouseY);

		if(tile().getChipPower(EnumChipType.SUPPORT) <= 0 && tile().getSupport().getType() == EnumEnergyMode.PRODUCE)
		{
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos , -20, 18*2, -20+18, 18*2+18))
			{
				HelperGui.drawHoveringTextFixedString(matrixStack, Arrays.asList(I18n.get("gui.futurepack.machine.no_support_chip").split("\n")), mouseX-leftPos +12, mouseY-topPos+4, -1, font);
			}
		}
	}	
}
