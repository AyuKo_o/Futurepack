package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityFlashServer;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotUses;
import futurepack.depend.api.helper.HelperChunks;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperRendering;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiFlashServer extends ActuallyUseableContainerScreen
{

	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"textures/gui/flashserver.png");
	
	public GuiFlashServer(Player pl, TileEntityFlashServer tile)
	{
		super(new ContainerFlashServer(pl.getInventory(),tile), pl.getInventory(), "gui.flashserver");
		//this.tile = tile;
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderBg(PoseStack matrixStack, float var1, int mx, int my) 
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		int k = (this.width - this.imageWidth) / 2;
		int l = (this.height - this.imageHeight) / 2;
		this.blit(matrixStack, k, l, 0, 0, this.imageWidth, this.imageHeight);
		
		HelperGui.renderSupport(matrixStack, k+158, l+7, tile().support, mx, my);
	}
			
	private TileEntityFlashServer tile()
	{
		return ((ContainerFlashServer)getMenu()).tile;
	}
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int p_146979_1_, int p_146979_2_)
	{
		super.renderLabels(matrixStack, p_146979_1_, p_146979_2_);
		//this.font.drawString(matrixStack, I18n.format("container.flashserver", new Object[0]), 6, 4, 4210752);
		//this.font.drawString(matrixStack, I18n.format("container.inventory", new Object[0]), 6, this.ySize - 96 + 4, 4210752);
		HelperGui.renderSupportTooltip(matrixStack, leftPos, topPos, 158, 7, tile().support, p_146979_1_, p_146979_2_);
	}
	
	public static class ContainerFlashServer extends ContainerSyncBase
	{
		TileEntityFlashServer tile;
		
		public ContainerFlashServer(Inventory inv, TileEntityFlashServer tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			
			this.addSlot(new SlotUses(tile, 0, 8, 14));
			this.addSlot(new SlotUses(tile, 1, 8, 56));
			for(int i=0;i<4;i++)
			{
				this.addSlot(new SlotUses(tile, i+2, 53 + 18*i, 35));
			}	
			
			
			int l;
			int i1;
									
			for (l = 0; l < 3; ++l)
			{
				for (i1 = 0; i1 < 9; ++i1)
				{
					this.addSlot(new Slot(inv, i1 + l * 9 + 9, 8 + i1 * 18, 84 + l * 18));
				}
			}
			
			for (l = 0; l < 9; ++l)
			{
				this.addSlot(new Slot(inv, l, 8 + l * 18, 142));
			}
		}
		

		@Override
		public boolean stillValid(Player playerIn)
		{
			return true;
		}
		
		@Override
		public ItemStack quickMoveStack(Player playerIn, int index)
		{
			Slot s =this.getSlot(index);
			if(s.hasItem())
			{
				if(index<6)
				{
					this.moveItemStackTo(s.getItem(), 6, this.slots.size(), false);
				}
				else
				{
					this.moveItemStackTo(s.getItem(), 2, 6, false);
				}
				if(s.getItem().getCount()<=0)
				{
					s.set(ItemStack.EMPTY);
				}
			}
			broadcastChanges();
			return ItemStack.EMPTY;
		}
		
		@Override
		public void removed(Player playerIn)
		{
			HelperChunks.renderUpdate(tile.getLevel(), tile.getBlockPos());
			super.removed(playerIn);
		}
	}

}
