package futurepack.common.gui.inventory;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.block.inventory.TileEntityForscher;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.gui.SlotUses;
import futurepack.common.sync.FPGuiHandler;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.ISyncable;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.PlayerInfo;
import net.minecraft.client.resources.DefaultPlayerSkin;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class GuiForscher extends ActuallyUseableContainerScreen<GuiForscher.ContainerForscher>
{
	private static final ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/forscher_gui.png");
	private UUID owner;
	
	private long timeStart;
	private float[] start;
	
	public GuiForscher(Player pl, TileEntityForscher tile)
	{
		super(new ContainerForscher(pl.getInventory(), tile), pl.getInventory(), tile.getGUITitle());
		this.imageHeight=188;
//		this.xSize=256;
//		this.ySize=256;
		this.timeStart = System.currentTimeMillis();
		start = new float[] {-1,-1,-1,-1};
		
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY)
	{
		this.font.draw(matrixStack, this.title.getString(), 8.0F, 6.0F, 4210752);
		//TODO: Check if gui can be adjusted to fit this.font.drawString(matrixStack, this.playerInventory.getDisplayName().getString(), 8.0F, this.ySize - 96 + 5, 4210752);
		
		ItemStack[] its = container().tile.getHoloItems();
		if(its==null)
		{
			return;
		}
		RenderSystem.setShaderColor(1, 1, 1, 1);
		long delta = System.currentTimeMillis() - timeStart;
		
		for(int i=0;i<its.length;i++)
		{
			int x = leftPos +12 + i*28;
			int y = topPos + 30;
			if(its[i] != null && container().tile.getItem(i).isEmpty() && mouseX <= x + 16 && mouseY <= y + 16 && mouseX >= x && mouseY >= y)
				renderTooltip(matrixStack, new TextComponent(its[i].getCount() + "* " + its[i].getHoverName().getString()), mouseX-this.leftPos, mouseY-this.topPos);
			
			y += 20;
			if(HelperComponent.isInBox(mouseX, mouseY, x, y, x+16, y+45))
			{
				float p = (1F - container().tile.getProgess(i));
				String number = new DecimalFormat("#.###").format(p * 100F);
				List<String> lines = new ArrayList<String>(2);
				lines.add(number + " %");
				if(i==1 && container().tile.getProperty(9) > 0)
				{
					lines.add(new TranslatableComponent("gui.futurepack.forscher.modules", container().tile.getProperty(9)).getString());
				}
				if(i>=1 && container().tile.isBlocked(i))
				{
					lines.add(new TranslatableComponent("gui.futurepack.forscher.no_research_modules", new String[] {"NE", "SP", "XP", "unknown"}[i-1]).getString());
				}
				
				if(start[i]>=0)
				{
					float fdelta = p - start[i];
					float time = fdelta / delta;//points per milisecond
					if(time>0)
					{
						float needed = (1F-p) / time;
						int seconds = (int) (needed / 1000);
						int minuits = seconds / 60;
						seconds -= minuits*60;
						
						lines.add(new TranslatableComponent("gui.futurepack.forscher.estimated", minuits + ":" + seconds).getString());
					}
				}
				renderComponentTooltip(matrixStack, lines.stream().map(TextComponent::new).collect(Collectors.toList()), mouseX-this.leftPos, mouseY-this.topPos);
			}
		}
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		RenderSystem.setShaderTexture(0, res);
		blit(matrixStack, leftPos, topPos, 0, 0, imageWidth, imageHeight);
		
		String user = container().username;
		if(user==null)
		{
			user = "?!?";
		}
		if(user.length()>11)
		{
			user = user.substring(0, 10) +".";
		}	
		TextComponent tc = new TextComponent(user);
		tc.setStyle(Style.EMPTY.withFont(HelperComponent.getUnicodeFont()));
		
		this.font.drawShadow(matrixStack, tc, leftPos+122, topPos+61, 0xffffddff);
		
		RenderSystem.setShaderColor(1, 1, 1, 1);
		
		for(int i=0;i<4;i++)
		{
			RenderSystem.setShaderTexture(0, res);
			//blue Bar overlays
			if(container().tile.isBlocked(i))
			{
//				RenderSystem.setShaderColor(0, 0, 0, 1);
				blit(matrixStack, leftPos+7 + i*28, topPos+25, 176, 25, 26, 72);
			}
			//red slot overlays
			if(container().tile.isBlocked(i+4))
			{
				blit(matrixStack, leftPos+9 + i*28, topPos+27, 176, 3, 22, 22);
			}

			if(container().tile.getProperty(0)!=-1)	//0: research
			{	
				float p = 1F - container().tile.getProgess(i);
				if(p>0 && start[i]<0)
					start[i] = p;
				HelperGui.renderSmallBars(matrixStack, leftPos+16 +i*28, topPos+50, p, i+1);
			}
		}
		renderHead(matrixStack, leftPos+158, topPos+52);
		
		HelperComponent.drawRoundButton(matrixStack, mouseX, mouseY, leftPos+131, topPos+8, 18, 18);
		HelperComponent.drawRoundButton(matrixStack, mouseX, mouseY, leftPos+149, topPos+8, 18, 18);
		HelperComponent.drawRoundButton(matrixStack, mouseX, mouseY, leftPos+120, topPos+77, 47, 18);
		HelperComponent.drawRoundButton(matrixStack, mouseX, mouseY, leftPos+152, topPos+31, 13, 13);
		
		RenderSystem.setShaderTexture(0, res);
		RenderSystem.setShaderColor(1, 1, 1, 1);
		blit(matrixStack, leftPos+134 , topPos+11, 211, 3, 11, 12);
		blit(matrixStack, leftPos+152 , topPos+11, 222, 3, 12, 11);
		blit(matrixStack, leftPos+136 , topPos+79, 198, 3, 13, 14);
		
		if(container().pl.getGameProfile().getId().equals(getUUID()) && container().tile.hasRewards)		//Only for Owner
			blit(matrixStack, leftPos+154 , topPos+33, 204, 27, 9, 9);
		else
			blit(matrixStack, leftPos+154 , topPos+33, 213, 27, 9, 9);
		
		renderItems(matrixStack, mouseX, mouseY);
	}
	
	private void renderItems(PoseStack matrixStack, int mouseX, int mouseY)
	{
		ItemStack[] its = container().tile.getHoloItems();
		if(its==null)
		{
			return;
		}
		HelperRendering.disableLighting();
		RenderSystem.setShaderColor(1f, 1f, 1f, 1f);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_DST_ALPHA);
		
		for(int i=0;i<its.length;i++)
		{
			ItemStack it = container().tile.getItem(i);
			if(it.isEmpty() && its[i] != null)
			{
				int x = leftPos +12 + i*28;
				int y = topPos + 30;
				Minecraft.getInstance().getItemRenderer().renderAndDecorateItem(its[i], x, y);
				
			}
		}
		GlStateManager._blendFunc(0, 0);
		GlStateManager._blendFunc(770, 771);
		GlStateManager._disableBlend();
		for(int i=0;i<its.length;i++)
		{
//			ItemStack it = container().tile.getStackInSlot(i);
			if(its[i]!=null)
			{
				int x = leftPos +12 + i*28;
				int y = topPos + 30;
				matrixStack.translate(0, 0, 160F);
				this.font.draw(matrixStack, its[i].getCount() + "", x, y, 0x008f00);
				matrixStack.translate(0, 0, -160F);
			}
		}
		
	}
	
	//GL11.glBlendFunc(GL11.GL_SRC_COLOR, GL11.GL_ONE_MINUS_CONSTANT_ALPHA); f�r die haldtransparenten items
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		if(mouseButton==0)
		{
			container().state = 0;
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 120, 77, 120+47, 77+18))
			{
				container().state = 1;
				FPPacketHandler.syncWithServer(container());
				return true;
			}
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 131, 8, 131+18, 8+18))
			{
				container().state = 2;
				FPPacketHandler.syncWithServer(container());
				return true;
			}
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 149, 8, 149+18, 8+18))
			{
				container().state = 3;
				FPPacketHandler.syncWithServer(container());
				return true;
			}
			if(HelperComponent.isInBox(mouseX-leftPos, mouseY-topPos, 152, 31, 152+13, 31+13))
			{
				container().state = 4;
				FPPacketHandler.syncWithServer(container());
				return true;
			}
		}
		else
		{
			container().state = -1;
		}
		return super.mouseClicked(mouseX, mouseY, mouseButton);
	}
	
	private ContainerForscher container()
	{
		return this.getMenu();
	}
	
	private UUID getUUID()
	{
		if(owner!=null)
		{
			return owner;
		}
		
		if(container().owner==null)
			return new UUID(0,0); //this should not happen...

		owner = container().owner;
		return owner;
	}
	
	private void renderHead(PoseStack matrixStack, int x, int y)
	{
		HelperRendering.glColor4f(1F, 1F, 1F, 1F);
		ResourceLocation loc = null;
		
		//NetworkPlayerInfo info = this.minecraft.getNetHandler().getPlayerInfo(getUUID());
		PlayerInfo info = this.minecraft.getConnection().getPlayerInfo(container().username);
		if(info!=null)
		{
			loc = info.getSkinLocation();
			
		}
		else
		{
			loc = DefaultPlayerSkin.getDefaultSkin(getUUID());
		}
		RenderSystem.setShaderTexture(0, loc);
		blit(matrixStack, x, y, 8, 8, 8, 8, 64, 64);
		blit(matrixStack, x, y, 40, 8, 8, 8, 64, 64);
	}
	
	public static class ContainerForscher extends ContainerSyncBase implements IGuiSyncronisedContainer, ISyncable
	{
		Player pl;
		TileEntityForscher tile;
		byte state = -1;
		
		String username="";
		int pointer=0;
		byte[] buffer;
		private UUID owner;
		
		public ContainerForscher(Inventory inv, TileEntityForscher tile)
		{
			super(tile, tile.getLevel().isClientSide());			
			this.pl = inv.player;
			this.tile = tile;
			int x,y;
			
			for(x=0;x<4;x++)
			{
				addSlot(new Slot(tile, x, 12 + x*28, 30));
			}
			
			addSlot(new SlotUses(tile, 4, 134, 32));
			
			for(y=0;y<3;y++)
			{
				for(x=0;x<9;x++)
				{
				
					this.addSlot(new Slot(inv, 9 + y*9 +x, 8+x*18, 106+y*18));
				}
			}
			
			for(x=0;x<9;x++)
			{
				this.addSlot(new Slot(inv, x, 8+x*18, 164));
			}
			
			username = tile.getUsername();		
		}
		
		@Override
		public void writeToBuffer(FriendlyByteBuf buf)
		{
			buf.writeByte(state);
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf buf)
		{
			state = buf.readByte();
			switch (state)
			{
			case 1:
				tile.startResearch();
				break;
			case 2:
				tile.downloadReserch();
				break;
			case 3:
				tile.deleteResearch();
				break;
			case 4:
				if(pl.getGameProfile().getId().equals(tile.getOwnerID()) && !tile.getResearchRewards().isEmpty())		//Only for Owner
					FPGuiHandler.RESEARCH_REWARDS.openGui(pl, tile.getSenderPosition());

				break;
			}	
		}

		
		@Override
		public void broadcastChanges()
		{
			if(pl.level.isClientSide)
			{
				new IllegalStateException("ContainerForscher.detectAndSendChanges() got called on client, this should not happen!").printStackTrace();
				return;
			}
			if(username!=null)
			{
				FPPacketHandler.sendToClient(this, pl);
				username=null;
			}
			super.broadcastChanges();
		}
		
		//TODO: this must to ALL Containers
		@Override
		public boolean stillValid(Player playerIn)
		{
			return tile.stillValid(playerIn);
		}
		
		@Override
		public ItemStack quickMoveStack(Player playerIn, int index)
		{
			ItemStack itemstack = ItemStack.EMPTY;
			Slot slot = this.getSlot(index);
			if (slot != null && slot.hasItem())
	        {
				ItemStack item = slot.getItem();
				itemstack = item.copy();
				
				if(index<5)
				{
					//Shift Click at scan input
					if(!this.moveItemStackTo(item, 5, 41, false))
					{	
						return ItemStack.EMPTY;
					}
				}
				else
				{
					return ItemStack.EMPTY;
				}
//				else if(index==1)
//				{
//					//Shift Click at Crafting Output
//					if(!this.mergeItemStack(item, 2, 36, false))
//					{	
//						return null;
//					}
//				}
//				else
//				{
//					//Shift Click at Inventory
//					if(!this.mergeItemStack(item, 0, 1, false))
//					{	
//						return null;
//					}
//				}
				
				if (item.getCount() == 0)
	            {
	                slot.set(ItemStack.EMPTY);
	            }
	            else
	            {
	                slot.setChanged();
	            }
				
				slot.onTake(playerIn, item);
	        }

	        return itemstack;
		}
		
		@Override
		public void setData(int id, int data)
		{
			super.setData(id, data);
		}

		@Override
		public void writeAdditional(DataOutputStream buffer) throws IOException 
		{
			if(username!=null)
				buffer.writeUTF(username);
			else
				buffer.writeUTF("#null");
			
			UUID uid = tile.getOwnerID();
			if(uid==null)
			{
				tile.setOwner(pl);
				uid = tile.getOwnerID();
			}
			buffer.writeLong(uid.getMostSignificantBits());
			buffer.writeLong(uid.getLeastSignificantBits());
			
		}

		@Override
		public void readAdditional(DataInputStream buffer) throws IOException 
		{
			this.username = buffer.readUTF();
			this.owner = new UUID(buffer.readLong(), buffer.readLong());
		}
		
		
	}

	
	
}
