package futurepack.common.gui.inventory;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.api.interfaces.tilentity.ITileAntenne;
import futurepack.api.interfaces.tilentity.ITilePropertyStorage;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperRendering;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.block.entity.BlockEntity;

public class GuiAntenne<T extends BlockEntity & ITileAntenne & ITilePropertyStorage> extends ActuallyUseableContainerScreen<GuiAntenne.ContainerAntenne<T>>
{
	public static ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/gui/antenna_gui.png");

	public GuiAntenne(Player pl, T tile)
	{
		super(new ContainerAntenne<T>(pl.getInventory(), tile), pl.getInventory(), "gui.antenna");
		
	}

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);
    }
	
	@Override
	protected void renderLabels(PoseStack matrixStack, int mouseX, int mouseY) 
	{
		this.font.draw(matrixStack, "Range: " + ((ITileAntenne)container().tile).getRange(), 8.0F, 6.0F, 4210752);
	}
	
	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
		RenderSystem.setShaderTexture(0, res);
		blit(matrixStack, leftPos, topPos, 0, 0, imageWidth, imageHeight);
		
		int f = ((ITileAntenne)container().tile).getFrequenz();
		f &= 0xFFF; // 4096 possibilities
		
		int[] freq = new int[3];
		freq[0] = f & 0xF;
		freq[1] = (f >> 4) & 0xF;
		freq[2] = (f >> 8) & 0xF;
		
		for(int i=0;i<freq.length;i++)
		{
			int x = leftPos + 34 + i*54;
			int y = topPos + 43;
			
			matrixStack.pushPose();
			matrixStack.translate(x, y, 0);
			double rot = 22.5D * freq[i];
			matrixStack.mulPose(Vector3f.ZP.rotationDegrees((float) rot));
			
			blit(matrixStack, -18, -18, 176, 0, 36, 36);
			
			matrixStack.popPose();
			
			int bgr = DyeColor.byId(freq[i]).getTextColor();
			float r = (bgr & 0xFF) / 255F;
			float g = ((bgr >> 8) & 0xFF) / 255F;
			float b = ((bgr >> 16) & 0xFF) / 255F;			
			HelperRendering.glColor4f(r, g, b, 1.0F);
			blit(matrixStack, x - 3, y-3, 212, 0, 6, 6);
			HelperRendering.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
			
//			matrixStack.pushPose();
//			matrixStack.translate(x, y, 0);
//			matrixStack.mulPose(Vector3f.ZP.rotationDegrees(rot));
//			PoseStack matrixStack = new PoseStack();
//			blit(matrixStack, -18, -18, 176, 0, 36, 36);
//				
//			matrixStack.popPose();
		}
		
				
		
				
				
	}
	
	@Override
	public boolean mouseReleased(double mouseX, double mouseY, int state)
	{
		if(state == 0)
		{
			int f = ((ITileAntenne)container().tile).getFrequenz();
			f &= 0xFFF; // 4096 possibilities
			
			int[] freq = new int[3];
			freq[0] = f & 0xF;
			freq[1] = (f >> 4) & 0xF;
			freq[2] = (f >> 8) & 0xF;
			
			for(int i=0;i<freq.length;i++)
			{
				int x = leftPos + 34 + i*54;
				int y = topPos + 43;
				
				double dx = mouseX-x;
				double dy = mouseY-y;
				
				if(HelperComponent.isInBox(dx, dy, -24, -24, 24, 24))
				{
					double d = dx / dy;
					if(d<0)
						d =-d;
						
					double reg = Math.toDegrees(Math.atan(d));
					if(dx>=0 && dy>0)
					{
						reg = 180 - reg;
					}
					else if(dx>0 && dy<0)
					{
						//richtig
					}
					else if(dx<0 && dy>0)
					{
						reg = 180 + reg;
					}
					else if(dx<0 && dy<=0)
					{
						reg = 360 - reg;
					}
						
					int w1 = (int)(reg / 22.5D);
					int w2 = (w1 +1 ) % 16;
						
					double d1 = w1 * 22.5D;
					double d2 = w2 * 22.5D;
					if(w1==15 && d2==0)
					{
						d2 = 360;
					}
					d1 = reg - d1;
					d2 = d2 - reg;
						
					if(d2 < d1)
					{
						freq[i] = w2;
					}
					else
					{
						freq[i] = w1;
					}
					
					int nf = (freq[2]<<8) | (freq[1]<<4) | freq[0];
					if(f != nf)
					{
						((ITileAntenne)container().tile).setFrequenz(nf);
						FPPacketHandler.syncWithServer(container());
					}
					
					return true;
				}
			}
		}
		return super.mouseReleased(mouseX, mouseY, state);

	}
	
	private ContainerAntenne<T> container()
	{
		return this.getMenu();
	}
	
	public static class ContainerAntenne<T extends BlockEntity & ITileAntenne & ITilePropertyStorage> extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		T tile;

		public ContainerAntenne(Inventory inv, T tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			HelperContainerSync.addInventorySlots(8, 84, inv, this::addSlot);
		}

		@Override
		public boolean stillValid(Player pl)
		{
			return HelperResearch.isUseable(pl, tile);
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf nbt) 
		{
			nbt.writeVarInt(tile.getFrequenz());
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf nbt) 
		{
			tile.setFrequenz(nbt.readVarInt());
		}
		
		
		
	}

}
