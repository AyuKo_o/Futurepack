package futurepack.common.gui.inventory;

import java.util.ArrayList;
import java.util.Collections;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.modification.TileEntityLaserBase;
import futurepack.common.block.modification.TileEntityRocketLauncher;
import futurepack.common.gui.ContainerSyncBase;
import futurepack.common.sync.FPPacketHandler;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperContainerSync;
import futurepack.depend.api.helper.HelperGui;
import futurepack.depend.api.helper.HelperResearch;
import futurepack.depend.api.interfaces.IGuiSyncronisedContainer;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.client.resources.sounds.SimpleSoundInstance;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.SlotItemHandler;

public class GuiLaserEditor extends GuiModificationBase<TileEntityLaserBase>
{
	private ArrayList<ToggleButton> list = new ArrayList<ToggleButton>();
	
	boolean needUpdate = false;
	
	public GuiLaserEditor(Player pl, TileEntityLaserBase<?> tile)
	{
		super(new ContainerLaserEditor(pl.getInventory(), tile), "turret_gui.png", pl.getInventory());
		
		super.nx = 5;
		super.ny = 8;
		
		imageWidth = 172;
		imageHeight = 89;

		list.add(new ToggleButton(21, 7, 176, 64, 16, 16, "attack.player", "laser.attack.player.name", "laser.tooltip.noai"));
		list.add(new ToggleButton(21, 28, 192, 64, 16, 16, "attack.neutral", "laser.attack.neutral.name", "laser.tooltip.noai"));
		list.add(new ToggleButton(21, 47, 208, 64, 16, 16, "attack.mobs", "laser.attack.mobs.name", "laser.tooltip.noai"));
		
		if(tile instanceof TileEntityRocketLauncher)
			list.add(new ToggleButton(21, 66, 224, 64, 16, 16, "kill.not", "laser.ballista.name", "laser.tooltip.noai"));
		else
			list.add(new ToggleButton(21, 66, 224, 64, 16, 16, "kill.not", "laser.kill.not.name", "laser.tooltip.noai")); //dont work anyway
		
		list.add(new NeededButton("attack.player", 48, 7, 176, 32, 16, 16, "player.warn", "laser.player.warn.name"));
		
		list.add(new NeededButton("attack.player", 73, 7, 176, 0, 16, 16, "player.distance", "laser.player.distance.name"));
		
		list.add(new NeededButton("attack.player", 92, 7, 192, 0, 16, 16, "wait.10", "laser.wait.10.name"));
		list.add(new NeededButton("attack.player", 111, 7, 208, 0, 16, 16, "wait.15", "laser.wait.15.name"));
		list.add(new NeededButton("attack.player", 130, 7, 224, 0, 16, 16, "wait.30", "laser.wait.30.name"));
		list.add(new NeededButton("attack.player", 149, 7, 240, 0, 16, 16, "wait.60", "laser.wait.60.name"));

		//To Fix Render Issues of Tooltips rendering behind other buttons
		Collections.reverse(list);
	}

	@Override
	protected void renderBg(PoseStack matrixStack, float partialTicks, int mouseX, int mouseY)
	{
		super.renderBg(matrixStack, partialTicks, mouseX, mouseY);
		
		matrixStack.translate(leftPos, topPos, 0);
		
		list.forEach( b -> b.draw(matrixStack, mouseX-leftPos, mouseY-topPos));
		
		matrixStack.translate(-leftPos, -topPos, 0);
	}
	
	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{		
		needUpdate = false;
		
		list.forEach( b -> b.onClick(mouseX-leftPos, mouseY-topPos, mouseButton));
		
		if(needUpdate)
		{
			FPPacketHandler.syncWithServer((ContainerLaserEditor)this.getMenu());
			return true;
		}
		
		return super.mouseClicked(mouseX, mouseY, mouseButton);
	}

	private class ToggleButton
	{
		int x, y;
		int width, height;	
		int u,v;
		String key;	
		String tooltip;
		String disabledMsg;
		
		public ToggleButton(int x, int y, int u, int v, int width, int height, String key, String tooltip, String disabledMsgKey)
		{
			super();
			this.x = x;
			this.y = y;
			this.u = u;
			this.v = v;
			this.width = width;
			this.height = height;
			this.key = key;
			this.disabledMsg = I18n.get(disabledMsgKey);
			this.tooltip =  I18n.get(tooltip);
		}

		public ToggleButton(int x, int y, int u, int v, int width, int height, String key, String tooltip)
		{
			this(x, y, u, v, width, height, key, tooltip, "");
		}

		public boolean isDisabled() {
			return !tile().getConfiguration("has.ai");
		}

		public boolean isActive()
		{
			return tile().getConfiguration(key);
		}
		
		public boolean isHovered(double d, double e)
		{
			return HelperComponent.isInBox(d, e, x, y, x+width, y+height);
		}
		
		public void draw(PoseStack matrixStack, int mx, int my)
		{
			if(isActive())
			{
				RenderSystem.setShaderTexture(0, res);
				GuiComponent.blit(matrixStack, x, y, u, v, width, height, 256, 256);
			}
			if(isHovered(mx, my)) {
				if(isDisabled()) {
					HelperGui.renderText(matrixStack, mx, my, disabledMsg);
				}
				else if(tooltip != null) {
					HelperGui.renderText(matrixStack, mx, my, tooltip);
				}
			}
		}
		
		public void onClick(double d, double e, int key)
		{
			if(isDisabled()) {
				return;
			}

			if(isHovered(d, e) && key==0)
			{
				tile().setConfig(this.key, !isActive());
				needUpdate = true;
				minecraft.getSoundManager().play(SimpleSoundInstance.forUI(SoundEvents.UI_BUTTON_CLICK, 1.0F));
			}
		}
	}
	
	private class NeededButton extends ToggleButton
	{
		private String dep;

		public NeededButton(String dep, int x, int y, int u, int v, int width, int height, String key, String tooltip)
		{
			super(x, y, u, v, width, height, key, tooltip);			
			this.dep = dep;
		}
		
		@Override
		public boolean isActive()
		{
			if(tile().getConfiguration(dep))
			{
				return super.isActive();
			}
			else
			{
				tile().setConfig(dep, false);
				return false;
			}
		}
		
		@Override
		public void draw(PoseStack matrixStack, int mx, int my)
		{
			if(!tile().getConfiguration(dep))
			{
				RenderSystem.setShaderTexture(0, res);
				GuiComponent.blit(matrixStack, x, y, u, v+16, width, height, 256, 256);
			}
			super.draw(matrixStack, mx, my);
		}
		
		@Override
		public void onClick(double mx, double my, int key)
		{
			if(tile().getConfiguration(dep))
				super.onClick(mx, my, key);
		}
	}


	@Override
	public TileEntityLaserBase<?> tile()
	{
		return ((ContainerLaserEditor)this.getMenu()).tile;
	}
	
	public static class ContainerLaserEditor extends ContainerSyncBase implements IGuiSyncronisedContainer
	{
		private TileEntityLaserBase<?> tile;
		
		public ContainerLaserEditor(Inventory inv, TileEntityLaserBase<?> tile)
		{
			super(tile, tile.getLevel().isClientSide());
			this.tile = tile;
			if(tile instanceof TileEntityRocketLauncher)
			{
				IItemHandlerModifiable handler = ((TileEntityRocketLauncher) tile).getGui();
				int x = 6;
				int y = 86;
				for(int i=0;i<9;i++)
					this.addSlot(new SlotItemHandler(handler, i, x + i*18, y));
				
				y=122;
				HelperContainerSync.addInventorySlots(x, y, inv, this::addSlot);
			}
		}
		
		@Override
		public ItemStack quickMoveStack(Player playerIn, int index)
		{
			Slot slot = this.slots.get(index);
	        if(slot != null && slot.hasItem())
	        {
	        	if(index < 9)
	        	{
	        		this.moveItemStackTo(slot.getItem(), 9, slots.size(), false);
	        	}
	        	else
	        	{
	        		this.moveItemStackTo(slot.getItem(), 0, 9, false);
	        	}
	        	if(slot.getItem().getCount()==0)
	        	{
	        		slot.set(ItemStack.EMPTY);
	        	}
	        }
		
	        this.broadcastChanges();
	        return ItemStack.EMPTY;
		}

		@Override
		public boolean stillValid(Player playerIn)
		{
			return HelperResearch.isUseable(playerIn, tile);
		}

		@Override
		public void writeToBuffer(FriendlyByteBuf nbt)
		{
			short[] shorts = tile.getConfigAsShorts();
			nbt.writeVarInt(shorts.length);
			for(short s : shorts)
				nbt.writeShort(s);
//			byte[] data = new byte[shorts.length*2];
//			for(int i=0;i<shorts.length;i++)
//			{
//				data[i*2] = (byte) (shorts[i] & 0xFF);
//				data[i*2+1] = (byte) ((shorts[i] >> 8) & 0xFF);
//			}
//			
//			
//			nbt.putByteArray("config", data);
		}

		@Override
		public void readFromBuffer(FriendlyByteBuf nbt)
		{
			short[] shorts = new short[nbt.readVarInt()];
			for(int i=0;i<shorts.length;i++)
			{
				shorts[i] = nbt.readShort();
			}
//			byte[] data = nbt.getByteArray("config");
//			short[] shorts = new short[data.length/2];
//			for(int i=0;i<shorts.length;i++)
//			{
//				shorts[i] = (short) (data[i*2]&0xFF | ((data[i*2+1]<<8)&0xFF00));
//			}
//
			tile.setConfigFromShort(shorts);
		}
	}
}
