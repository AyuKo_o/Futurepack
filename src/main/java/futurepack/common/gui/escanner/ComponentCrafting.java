package futurepack.common.gui.escanner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.FPLog;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperJSON;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.multiplayer.ClientPacketListener;
import net.minecraft.core.NonNullList;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.Recipe;
import net.minecraftforge.common.crafting.IShapedRecipe;

public class ComponentCrafting extends ComponentBase
{
	List<ItemStack>[] crafting;
	List<ItemStack> result;
	
	public ComponentCrafting(JsonObject obj)
	{
		super(obj);
		if(obj.has("load"))
		{
			ClientPacketListener netty = Minecraft.getInstance().player.connection;
			if(obj.get("load").isJsonPrimitive())
			{
				ResourceLocation res = new ResourceLocation(obj.get("load").getAsString());
				Recipe<?> rec = netty.getRecipeManager().byKey(res).orElse(null);
				if(rec == null)
					throw new IllegalStateException("Craftingrecipe " + res + " not found");
				result = Collections.singletonList(rec.getResultItem());
				crafting = getCraftingGrid(rec);
			}
			else if(obj.get("load").isJsonArray())
			{
				JsonArray arr = obj.getAsJsonArray("load");
				
				result = new ArrayList<ItemStack>();
				crafting = new List[9];
				for(int i=0;i<crafting.length;i++)
				{
					crafting[i] = new ArrayList<ItemStack>();
				}
				
				for(JsonElement elm : arr)
				{
					ResourceLocation res = new ResourceLocation(elm.getAsString());
					Recipe<?> rec = netty.getRecipeManager().byKey(res).orElse(null);
					
					List<ItemStack>[] lists = getCraftingGrid(rec);
					int size = HelperComponent.fillListToSameSize(lists, 1);
					
					for(int i=0;i<size;i++)
					{
						result.add(rec.getResultItem());
					}
					for(int i=0;i<Math.min(crafting.length, lists.length);i++)
					{
						if(lists[i]!=null)
							crafting[i].addAll(lists[i]);
					}
				}
			}
			else
			{
				FPLog.logger.warn("Unsupported JsonType '" + obj.get("load")+ "'");
			}
		}
		else if(obj.has("slots") && obj.get("slots").isJsonObject())
		{
			JsonObject jo = obj.getAsJsonObject("slots");
			result = HelperJSON.getItemFromJSON(jo.get("out"), true);
			HelperJSON.setupRendering(result);
			
			crafting = new List[9];
			for(int i=0;i<crafting.length;i++)
			{
				String num = "" + (i+1);
				if(jo.has(num))
				{
					crafting[i] = HelperJSON.getItemFromJSON(jo.get(num), true);
					HelperJSON.setupRendering(crafting[i]);
				}
			}
		}
	}

	private static List<ItemStack>[] getCraftingGrid(Recipe<?> rec)
	{
		if(rec == null)
		{
			FPLog.logger.warn(" Recipe is null! Propaply wrong Resource Location");
			return new List[0];	
		}
		List<ItemStack>[] crafting = new List[9];
		
		if(rec instanceof IShapedRecipe)
		{
			IShapedRecipe<?> shaped = (IShapedRecipe<?>) rec;
			
			int mx1 = Math.min(3, shaped.getRecipeWidth());
			int mx2 = shaped.getRecipeWidth();
			int my1 = Math.min(3, shaped.getRecipeHeight());
			int my2 = shaped.getRecipeHeight();
			
			for(int x=0;x<mx1;x++)
			{
				for(int y=0;y<my1;y++)
				{
					int i1 = y * 3 + x;
					int i2 = y * mx2 + x;
					
					NonNullList<Ingredient> nnl = shaped.getIngredients();
					crafting[i1] = Arrays.asList(nnl.get(i2).getItems());
				}
			}
		}
		else
		{
			for(int i=0;i<Math.min(9, rec.getIngredients().size());i++)
			{
				NonNullList<Ingredient> nnl = rec.getIngredients();
				crafting[i] = Arrays.asList(nnl.get(i).getItems());
			}
		}
		return crafting;
	}
	
	@Override
	public void init(int maxWidth, Screen gui)
	{
		
	}

	@Override
	public int getWidth()
	{
		return 100;
	}

	@Override
	public int getHeight()
	{
		return 64;
	}

	@Override
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		Lighting.setupFor3DItems();
		RenderSystem.enableDepthTest();
		
		HelperComponent.drawBackground(matrixStack, x, y, this);
		
		HelperComponent.renderArrow(matrixStack, x+59, y+5+18, blitOffset + 1);
		
		for(int j=0;j<3;j++)
		{
			for(int k=0;k<3;k++)
			{
				HelperComponent.renderItemStackWithSlot(matrixStack, HelperComponent.getStack(crafting[j*3 +k]), x+5 + k*18, y+5 + j*18, blitOffset + 1);			
			}
		}
		HelperComponent.renderItemStackWithSlot(matrixStack, HelperComponent.getStack(result), x+77, y+5+18, blitOffset + 1);		
		
		Lighting.setupFor3DItems();
	}

	@Override
	public void postRendering(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui)
	{
		for(int j=0;j<3;j++)
		{
			for(int k=0;k<3;k++)
			{
				HelperComponent.renderItemText(matrixStack, HelperComponent.getStack(crafting[j*3 +k]), x+5 + k*18, y+5 + j*18, mouseX,mouseY, gui);			
			}
		}
		HelperComponent.renderItemText(matrixStack, HelperComponent.getStack(result), x+77, y+5+18, mouseX, mouseY, gui);
	}

	
	@Override
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui)
	{
		if(mouseButton==0)
		{
			if(HelperComponent.isInBox(mouseX, mouseY, x+78, y+24, x+78+16, y+24+16))
			{
				HelperComponent.researchItem(HelperComponent.getStack(this.result), gui.getResearchGui());
			}
			else 
			{
				for(int j=0;j<3;j++)
				{
					for(int k=0;k<3;k++)
					{
						if(HelperComponent.isInBox(mouseX, mouseY, x+6 + k*18, y+6 + j*18, x+6 + k*18+16, y+6 + j*18+16))
						{
							HelperComponent.researchItem(HelperComponent.getStack(crafting[j*3 +k]), gui.getResearchGui());
						}
					}
				}
			}
		}
	}
}
