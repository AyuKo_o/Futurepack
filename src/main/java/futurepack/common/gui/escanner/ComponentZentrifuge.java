package futurepack.common.gui.escanner;

import java.util.List;

import com.google.gson.JsonObject;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperJSON;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.world.item.ItemStack;


public class ComponentZentrifuge extends ComponentBase
{
	List<ItemStack> in;
	List<ItemStack>[] out = new List[3];
	int time, support;
	
	public ComponentZentrifuge(JsonObject obj)
	{
		super(obj);
		JsonObject jo = obj.get("slots").getAsJsonObject();
		in = HelperJSON.getItemFromJSON(jo.get("in"), true);
		out[0] = HelperJSON.getItemFromJSON(jo.get("out1"), true);
		out[1] = HelperJSON.getItemFromJSON(jo.get("out2"), true);
		out[2] = HelperJSON.getItemFromJSON(jo.get("out3"), true);
		HelperJSON.setupRendering(in);
		HelperJSON.setupRendering(out[0]);
		HelperJSON.setupRendering(out[1]);
		HelperJSON.setupRendering(out[2]);
		
		time = obj.get("time").getAsInt();
		support = obj.get("support").getAsInt();
	}

	@Override
	public void init(int maxWidth, Screen gui)
	{
		
	}

	@Override
	public int getWidth() 
	{
		return 96;
	}

	@Override
	public int getHeight()
	{
		return 67;
	}

	@Override
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		RenderSystem.enableDepthTest();
		Lighting.setupFor3DItems();
		
		HelperComponent.drawBackground(matrixStack, x, y, this);
		gui.getMinecraft().font.draw(matrixStack, support+"SP", x+24, y+10, 0x3a3a3a);
		gui.getMinecraft().font.draw(matrixStack, (time*0.05F)+"s", x+24, y+48, 0x3a3a3a);
		
		ItemStack in = HelperComponent.getStack(this.in);
		ItemStack out1 = HelperComponent.getStack(this.out[0]);
		ItemStack out2 = HelperComponent.getStack(this.out[1]);
		ItemStack out3 = HelperComponent.getStack(this.out[2]);
		
		HelperComponent.renderArrow(matrixStack, x+39, y+24, blitOffset + 1);
		HelperComponent.renderSupport(matrixStack, x+6, y+5, blitOffset + 1);
		HelperComponent.renderSymbol(matrixStack, x+6, y+43, blitOffset + 1, 11);
		
		Lighting.setupForFlatItems();
		
		HelperComponent.renderItemStackWithSlot(matrixStack, in, x+6, y+24, blitOffset + 1);
		HelperComponent.renderItemStackWithSlot(matrixStack, out1, x+72, y+5, blitOffset + 1);
		HelperComponent.renderItemStackWithSlot(matrixStack, out2, x+72, y+24, blitOffset + 1);
		HelperComponent.renderItemStackWithSlot(matrixStack, out3, x+72, y+43, blitOffset + 1);
	}

	@Override
	public void postRendering(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui)
	{
		ItemStack in = HelperComponent.getStack(this.in);
		ItemStack out1 = HelperComponent.getStack(this.out[0]);
		ItemStack out2 = HelperComponent.getStack(this.out[1]);
		ItemStack out3 = HelperComponent.getStack(this.out[2]);
		
		HelperComponent.renderItemText(matrixStack, in, x+6, y+24, mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, out1, x+72, y+5, mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, out2, x+72, y+24, mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, out3, x+72, y+43, mouseX, mouseY, gui);
	}
	
	@Override
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui)
	{
		if(mouseButton==0)
		{
			if(isOverSlot(mouseX, mouseY, x+7, y+25))
			{
				ItemStack it = HelperComponent.getStack(in);
				HelperComponent.researchItem(it, gui.getResearchGui());
			}
			else if(isOverSlot(mouseX, mouseY, x+73, y+6))
			{
				ItemStack it = HelperComponent.getStack(this.out[0]);
				HelperComponent.researchItem(it, gui.getResearchGui());
			}
			else if(isOverSlot(mouseX, mouseY, x+73, y+25))
			{
				ItemStack it = HelperComponent.getStack(this.out[1]);
				HelperComponent.researchItem(it, gui.getResearchGui());
			}
			else if(isOverSlot(mouseX, mouseY, x+73, y+44))
			{
				ItemStack it = HelperComponent.getStack(this.out[2]);
				HelperComponent.researchItem(it, gui.getResearchGui());
			}
		}
	}
	
	private boolean isOverSlot(double mouseX, double mouseY, int x, int y)
	{
		return HelperComponent.isInBox(mouseX, mouseY, x, y, x+16, y+16);
	}
}
