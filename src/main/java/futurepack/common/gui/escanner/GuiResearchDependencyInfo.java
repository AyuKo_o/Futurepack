package futurepack.common.gui.escanner;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.IntSupplier;
import java.util.function.Predicate;

import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.datafixers.util.Pair;

import futurepack.common.research.CustomPlayerData;
import futurepack.common.research.Research;
import futurepack.common.research.ResearchPage;
import futurepack.depend.api.helper.HelperRendering;

public class GuiResearchDependencyInfo extends GuiResearchTreePage
{
	public final Research main;

	private DependencyGraphBuilder builder;
	private Map<Research, int[]> pos;
	
	public GuiResearchDependencyInfo(ResearchPage page, CustomPlayerData custom, GuiResearchMainOverview parent, Research r)
	{
		super(page, custom, parent);
		this.main = r;
		builder = new DependencyGraphBuilder(r, custom::hasResearch);
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks)
	{
		matrixStack.pushPose();
		matrixStack.translate(0, 0, -2000);
		RenderSystem.disableDepthTest();
		renderBackground(matrixStack, 0);
		RenderSystem.enableDepthTest();
		RenderSystem.getModelViewStack().pushPose();
		RenderSystem.getModelViewStack().translate(0, 0, 500);
		RenderSystem.applyModelViewMatrix();
		itemRenderer.blitOffset+=500;
		
		//super.render(matrixStack, mouseX, mouseY, partialTicks);
		List<Set<Research>> list = builder.getResult();
		if(pos == null)
		{
			pos = new TreeMap<>(Comparator.comparingInt(r -> r.id));
			int maxHeight  = list.stream().mapToInt(Set::size).max().orElse(1);
			for(int i=0;i<list.size();i++)
			{
				int j=0;
				Set<Research> s = list.get(i);
				int offset = (maxHeight - s.size()) / 2;
				for(Research r : s)
				{
					pos.put(r, new int[] {list.size()-i-1, offset+j});
					j++;
				}
			}
		}
		
		drawResearches(matrixStack, mouseX, mouseY);
		RenderSystem.getModelViewStack().popPose();
		itemRenderer.blitOffset-=500;
		matrixStack.popPose();
	}
	
	@Override
	protected void drawResearches(PoseStack matrixStack, int mx, int my)
	{
		HelperRendering.glColor4f(1F, 1F, 1F, 1F);
		Lighting.setupForFlatItems(); 
		float rx = ((width - 224)/2/scale);
		float ry = ((height - 224)/2/scale);
		mx/=scale;
		my/=scale;
		
		boolean inWindow = true;//mx>=rx && mx<=rx+224/scale && my>=ry && my<=ry+224/scale;
			
		rx+=(bgw-224)/2;
		ry+=(bgh-224)/2;
		
		this.hovered = null;
		
		List<Set<Research>> list = builder.getResult();
		for(int i=0;i<list.size();i++)
		{
			for(Research r : list.get(i))
			{
				drawResearch(matrixStack, r, mx, my, inWindow, rx, ry);
			}
		}
		
		Lighting.setupFor3DItems();
	}
	
	@Override
	protected float[] getResearchPos(Research res, float rx, float ry)
	{
		float x,y;
		x = rx + posX + 102 - 200;
		y = ry + posY + 102;
		int[] xy = pos.getOrDefault(res, new int[] {0,0});
		x += xy[0] * 24;
		y += xy[1] * 24;
		
		return new float[] {x,y};
	}
	
	@Override
	protected boolean isParentAvailable(Research res)
	{
		return true;
	}
	
	private static class DependencyGraphBuilder
	{
		private Map<Research, IntSupplier> research2levelMap = new HashMap<>();
		private Set<Pair<Research, IntSupplier>> todo = new HashSet<>();
		private List<Set<Research>> dependencyList;
		private final Predicate<Research> hasResearch;
		
		public DependencyGraphBuilder(Research main, Predicate<Research> hasResearch)
		{
			this.hasResearch = hasResearch;
			setDependencyLevel(main, ()->0);
		}
		
		public IntSupplier getDependencyLevel(Research r)
		{
			return research2levelMap.getOrDefault(r, null);
		}
		
		private void setDependencyLevel(Research r, IntSupplier level)
		{
			research2levelMap.merge(r, level, (a,b) -> cashed(() -> Math.max(a.getAsInt(), b.getAsInt())));
			addParents(r);
		}
		
		private void setDependencyLevelinList(Research res, int level)
		{
			if(!hasResearch.test(res) || level<=4)
			{
			while(dependencyList.size()-1 < level)
				{
					dependencyList.add(new TreeSet<>(Comparator.comparingInt(r -> r.id)));
				}
				dependencyList.get(level).add(res);
			}
		}
		
		private void addParents(Research r)
		{
			for(Research p : r.getParents())
			{
				todo.add(new Pair<Research, IntSupplier>(p, cashed(() -> getDependencyLevel(r).getAsInt()+1)));
			}
		}
		
		private IntSupplier cashed(IntSupplier sup)
		{
			return new IntSupplier()
			{
				int i = sup.getAsInt();
				
				@Override
				public int getAsInt()
				{
					return i;
				}
			};
		}
		
		private void build()
		{
			Set<Pair<Research, IntSupplier>> oldTodo = new HashSet<>();
			while(!todo.isEmpty())
			{
				Set<Pair<Research, IntSupplier>> oldTodo2 = oldTodo;
				oldTodo = todo;
				todo = oldTodo2;
				for(var p : oldTodo)
				{
					setDependencyLevel(p.getFirst(), p.getSecond());
				}
				oldTodo.clear();
			}
		}
		
		public List<Set<Research>> getResult()
		{
			if(dependencyList == null)
			{
				build();
				todo = null;
				dependencyList = new ArrayList<>();
				research2levelMap.forEach((r,s) -> setDependencyLevelinList(r, s.getAsInt()));
			}
			
			return dependencyList;
		}
	}

}
