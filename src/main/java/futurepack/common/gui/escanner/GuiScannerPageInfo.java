package futurepack.common.gui.escanner;

import java.util.ArrayList;
import java.util.Arrays;

import futurepack.common.research.ScannerPageResearch;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.HoverEvent;
import net.minecraft.network.chat.HoverEvent.Action;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.network.chat.Style;

public class GuiScannerPageInfo extends GuiScannerPageBase
{	
	public GuiScannerPageInfo(boolean showInDungeon, Component... chat)
	{
		super(convert(chat), null, showInDungeon);
	}
	
	protected static IGuiComponent[] convert(Component[] chat)
	{
		ArrayList<IGuiComponent> list = new ArrayList<IGuiComponent>(chat.length);
		for(Component co : chat)
		{
			if(co!=null)
			{
				if(co.getStyle()!=null)
				{
					Style s = co.getStyle();
					if(s.getHoverEvent()!=null)
					{
						HoverEvent he = s.getHoverEvent();
						if(he.getAction() == Action.SHOW_TEXT && he.getValue(Action.SHOW_TEXT) != null)
						{
							String value = he.getValue(Action.SHOW_TEXT).getString();
							if(value!=null && value.startsWith("load_research="))
							{
								String research_name = value.split("=")[1];
								list.addAll(Arrays.asList(ScannerPageResearch.getComponetsFromName(research_name)));
								continue;
							}
						}
					}
				}
				
				list.add(new ComponentInteractiveText((MutableComponent) co));
			}			
		}
		return list.toArray(new IGuiComponent[list.size()]);
	}
	
	@Override
	public int getIconIndex()
	{
		return 1;
	}
}
