package futurepack.common.gui.escanner;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperJSON;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Blocks;

/**
 * blocks:
 * [
 *   [
 *     ["dirt","dirt","dirt"], //oben-hinten
 *     ["dirt","dirt","dirt"]  //oben-vorn
 *   ],
 *   [
 *     ["dirt","dirt","dirt"], 	//unten
 *     ["dirt","dirt","dirt"]
 *   ]
 * ]
 */
public class ComponentBuilding extends ComponentBase
{
	private List<ItemStack>[][][] blocks; //[x][y][z] -> x links/rechts, y hoch/runter, z hinten/vorn -> 0-0-0 ist links oben hinten
	private int w=0,h=0,d=0;
	
	private int partH=8;
	private int totalWX=8,totalWZ=8;
	private int totalW=0;
	
	public ComponentBuilding(JsonObject obj)
	{
		super(obj);
		if(obj.has("blocks") && obj.get("blocks").isJsonArray())
		{
			blocks = getBlocks3D(obj.getAsJsonArray("blocks"));
			w = blocks.length;
			if(w>0)
			{
				h = blocks[0].length;
				if(h>0)
				{
					d = blocks[0][0].length;
				}	
			}	
		}
	}

	@Override
	public void init(int maxWidth, Screen gui)
	{	
		double t = Math.cos(Math.toRadians(30F));	
		partH=8;
		totalWX=8;
		totalWZ=8;
		totalW=0;
	
		totalWX += t * 16*w;
		totalWZ += t * 16*d;
		
		t = Math.sin(Math.toRadians(30F));
		partH +=  16*w*t + 16*d*t; 
	}

	@Override
	public int getWidth()
	{
		return totalWX+ totalWZ;
	}

	@Override
	public int getHeight()
	{
		return partH * h;
	}

	@Override
	public void render(PoseStack matrixStack, int gx, int gy, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{
		HelperComponent.drawBackground(matrixStack, gx, gy, this);
		Lighting.setupForFlatItems();
		
		for(int y=0;y<h;y++)
		{
			for(int z=0;z<d;z++)
			{
				for(int x=0;x<w;x++)
				{
					ItemStack item = HelperComponent.getStack(blocks[x][y][z]);
					if(item==null || item.isEmpty())
					{
						item = new ItemStack(Blocks.AIR);
					}
					
					int fy = gy+y * partH - (d==1 ? 18 : 9);
					int fx = gx - (d==1 ? 18 : 5);
					
					float px = (float)x/(float)w;
					float pz = 1F-(float)z/(float)d;
					fx += px*totalWX;
					fx += totalWX- pz*totalWZ;
					
					fy += 0.5*partH*px;
					fy += 0.5*partH*pz;
					
					if(item.isEmpty())
					{
						GuiComponent.fill(matrixStack, fx+2, fy+2, fx+12, fy+12, 0xff89c9da);
					}
					else
					{
						HelperComponent.renderItemStackNormal(matrixStack, item, fx, fy, blitOffset, false);
					}
					
				}
			}
		}
		Lighting.setupFor3DItems();
	}

	@Override
	public void postRendering(PoseStack matrixStack, int gx, int gy, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui)
	{
		for(int y=0;y<h;y++)
		{
			for(int z=0;z<d;z++)
			{
				for(int x=0;x<w;x++)
				{
					ItemStack item = HelperComponent.getStack(blocks[x][y][z]);
					if(item==null || item.isEmpty())
					{
						item = new ItemStack(Blocks.AIR);
					}
					
					int fy = gy+y * partH - (d==1 ? 18 : 9);
					int fx = gx - (d==1 ? 18 : 5);
					
					float px = (float)x/(float)w;
					float pz = 1F-(float)z/(float)d;
					fx += px*totalWX;
					fx += totalWX- pz*totalWZ;
					
					fy += 0.5*partH*px;
					fy += 0.5*partH*pz;
					
					HelperComponent.renderItemText(matrixStack, item, fx, fy, mouseX, mouseY, gui);
				}
			}
		}
	}
	
	private static List<ItemStack>[][][] getBlocks3D(JsonArray arr)
	{
		List<ItemStack>[][][] lists;
		int w=0,h=0,d=0;
		
		h = arr.size();
		
		for(JsonElement elm : arr)
		{
			JsonArray Zarr = elm.getAsJsonArray();
			d = Math.max(d, Zarr.size());
			for(JsonElement elm2 : Zarr)
			{
				JsonArray Xarr = elm2.getAsJsonArray();
				w = Math.max(w, Xarr.size());
			}
		}
		lists = new ArrayList[w][h][d];

		for(int y=0;y<h;y++)
		{
			JsonArray Zarr = arr.get(y).getAsJsonArray();
			for(int z=0;z<Zarr.size();z++)
			{
				JsonArray Xarr = Zarr.get(z).getAsJsonArray();
				for(int x=0;x<Xarr.size();x++)
				{
					JsonElement elm = Xarr.get(x);
					lists[x][y][z] = HelperJSON.getItemFromJSON(elm, false);
					HelperJSON.setupRendering(lists[x][y][z]);				
				}
			}
		}
		return lists;
	}	
}
