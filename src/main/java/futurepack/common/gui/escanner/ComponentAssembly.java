package futurepack.common.gui.escanner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.ItemPredicateBase;
import futurepack.common.FPLog;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.depend.api.helper.HelperComponent;
import futurepack.depend.api.helper.HelperJSON;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.world.item.ItemStack;

public class ComponentAssembly extends ComponentBase
{
	List<ItemStack>[] in = new List[3];
	List<ItemStack> out;

	public ComponentAssembly(JsonObject obj)
	{
		super(obj);
		if(obj.has("load"))
		{
			List<AssemblyRecipe> list = null;
			if(obj.get("load").isJsonPrimitive())
			{
				String s = obj.get("load").getAsString();
				list = Arrays.asList(FPAssemblyManager.instance.getMatchingRecipes(s));
			}
			else if(obj.get("load").isJsonArray())
			{
				list = new ArrayList<AssemblyRecipe>();
				for(JsonElement elm : obj.getAsJsonArray("load"))
				{
					String s = elm.getAsString();
					list.addAll(Arrays.asList(FPAssemblyManager.instance.getMatchingRecipes(s)));
				}
			}
			if(list!=null)
			{
				if(list.isEmpty())
				{
					FPLog.logger.warn("Did not find any matching assembly recipes %s", obj.get("load"));
				}
				else
				{
					bakeItems(list);
				}
				
			}
			else
			{
				FPLog.logger.warn("Unsupported recipe format (only string & array are supported): %s", obj.get("load"));
			}
		}
		else if(obj.has("slots"))
		{
			JsonObject jo = obj.get("slots").getAsJsonObject();
			in[0] = HelperJSON.getItemFromJSON(jo.get("in1"), true);
			in[1] = HelperJSON.getItemFromJSON(jo.get("in2"), true);
			in[2] = HelperJSON.getItemFromJSON(jo.get("in3"), true);
			out = HelperJSON.getItemFromJSON(jo.get("out"), true);
			HelperJSON.setupRendering(in[0]);
			HelperJSON.setupRendering(in[1]);
			HelperJSON.setupRendering(in[2]);
			HelperJSON.setupRendering(out);
			
			//debug: -- id search
			boolean print = false;
			for(AssemblyRecipe r : FPAssemblyManager.instance.recipes)
			{
				ItemStack o = r.getOutput();
				for(ItemStack is : out)
				{
					if(o.sameItem(is))
					{
						System.out.println("\t" + r + " [" + is + "]");
						print = true;
					}
				}
			}
			if(!print)
				System.out.println("\tUnknown");
		}
		
	}
	
	private  void bakeItems(List<AssemblyRecipe> list)
	{
		List<ItemStack>[][] inputs = new List[3][list.size()];
		List<ItemStack>[] outputs = new List[list.size()];
		
		for(int i=0;i<list.size();i++)
		{
			AssemblyRecipe r =  list.get(i);
			ItemStack output = r.getOutput();
			ItemPredicateBase[] input = r.getInput();
			int itemCount = 0;
			for(int j=0;j<3;j++)
			{
				if(j<input.length)
				{
					inputs[j][i] = input[j].collectAcceptedItems(new ArrayList<ItemStack>());
					itemCount = Math.max(itemCount, inputs[j][i].size());
				}
				else
				{
					inputs[j][i] = new ArrayList<ItemStack>();
					inputs[j][i].add(ItemStack.EMPTY);
				}
			}
			outputs[i] = new ArrayList<ItemStack>(4);
			outputs[i].add(output);
			//untested if this will work
			HelperComponent.fillListToSameSize(new List[]{inputs[0][i], inputs[1][i], inputs[2][i], outputs[i]}, itemCount); //every list needs to have the same length so we dont mix different recipes
		}
		
		in = new List[3];
		
		out = new ArrayList<ItemStack>();
		for(List<ItemStack> l : outputs)
			out.addAll(l);
		HelperJSON.setupRendering(out);
		
		for(int i=0;i<3;i++)
		{
			in[i] = new ArrayList<ItemStack>();
			for(List<ItemStack> l : inputs[i])
				in[i].addAll(l);
			HelperJSON.setupRendering(in[i]);
		}
	}

	@Override
	public void init(int maxWidth, Screen gui)
	{
		
	}

	@Override
	public int getWidth()
	{
		return 79;
	}

	@Override
	public int getHeight()
	{
		return 54;
	}

	@Override
	public void render(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, GuiScannerBase gui)
	{	
		HelperComponent.drawBackground(matrixStack, x, y, this);
		Lighting.setupForFlatItems();
		
		ItemStack in1 = HelperComponent.getStack(this.in[0]);
		ItemStack in2 = HelperComponent.getStack(this.in[1]);
		ItemStack in3 = HelperComponent.getStack(this.in[2]);
		ItemStack out = HelperComponent.getStack(this.out);
		
		HelperComponent.renderItemStackWithSlot(matrixStack, in1, x+11, y+6, blitOffset);
		HelperComponent.renderItemStackWithSlot(matrixStack, in2, x+11+19, y+6, blitOffset);
		HelperComponent.renderItemStackWithSlot(matrixStack, in3, x+11+19*2, y+6, blitOffset);
		HelperComponent.renderItemStackWithSlot(matrixStack, out, x+11+19, y+12+18, blitOffset);
	}

	
	@Override
	public void postRendering(PoseStack matrixStack, int x, int y, int blitOffset, int mouseX, int mouseY, boolean hover, GuiScannerBase gui)
	{
		ItemStack in1 = HelperComponent.getStack(this.in[0]);
		ItemStack in2 = HelperComponent.getStack(this.in[1]);
		ItemStack in3 = HelperComponent.getStack(this.in[2]);
		ItemStack out = HelperComponent.getStack(this.out);
		
		HelperComponent.renderItemText(matrixStack, in1, x+11, y+6, mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, in2, x+11+19, y+6, mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, in3, x+11+19*2, y+6, mouseX, mouseY, gui);
		HelperComponent.renderItemText(matrixStack, out, x+11+19, y+12+18, mouseX, mouseY, gui);
	}

	@Override
	public void onClicked(int x, int y, int mouseButton, double mouseX, double mouseY, GuiScannerBase gui)
	{
		if(mouseButton==0)
		{
			if(isOverSlot(mouseX, mouseY, x+12, y+7))
			{
				ItemStack it = HelperComponent.getStack(this.in[0]);
				HelperComponent.researchItem(it, gui.getResearchGui());
			}
			else if(isOverSlot(mouseX, mouseY, x+31, y+7))
			{
				ItemStack it = HelperComponent.getStack(this.in[1]);
				HelperComponent.researchItem(it, gui.getResearchGui());
			}
			else if(isOverSlot(mouseX, mouseY, x+50, y+7))
			{
				ItemStack it = HelperComponent.getStack(this.in[2]);
				HelperComponent.researchItem(it, gui.getResearchGui());
			}
			else if(isOverSlot(mouseX, mouseY, x+31, y+31))
			{
				ItemStack it = HelperComponent.getStack(this.out);
				HelperComponent.researchItem(it, gui.getResearchGui());
			}
		}
	}
	
	private boolean isOverSlot(double mouseX, double mouseY, int x, int y)
	{
		return HelperComponent.isInBox(mouseX, mouseY, x, y, x+16, y+16);
	}
}
