package futurepack.common.gui;

import futurepack.depend.api.interfaces.IContainerScrollable;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class SlotScrollable extends SlotItemHandler
{
	private IContainerScrollable table;
	
	public int startY;
	private boolean isFakeSlot = false;
	
	public SlotScrollable(IContainerScrollable table, IItemHandler itemHandler, int index, int xPosition, int yPosition)
	{
		super(itemHandler, index, xPosition, yPosition);
		this.table = table;
		startY = yPosition;
	}

	@Override
	public boolean isActive()
	{
		return table.isEnabled(this);
	}
	
	public boolean isFakeSlot()
	{
		return isFakeSlot;
	}
	
	@Override
	public boolean mayPickup(Player playerIn)
	{
		return isFakeSlot() ? false : super.mayPickup(playerIn);
	}
	
	@Override
	public boolean mayPlace(ItemStack stack)
	{
		return isFakeSlot() ? false : super.mayPlace(stack);
	}
	
	public SlotScrollable setFakeSlot(boolean fake)
	{
		isFakeSlot = fake;
		return this;
	}
	
}
