package futurepack.common.gui;

import net.minecraft.world.Container;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.ItemStack;

public class SlotUses extends Slot
{
	int limit;

	public SlotUses(Container par1iInventory, int index, int x, int y) 
	{
		this(par1iInventory, index, x, y, par1iInventory.getMaxStackSize());
	}
	
	public SlotUses(Container par1iInventory, int index, int x, int y, int limit) 
	{
		super(par1iInventory, index, x, y);
		this.limit = limit;
	}
	
	@Override
	public boolean mayPlace(ItemStack it) 
	{
		return container.canPlaceItem(getSlotIndex(), it);
	}
	
	@Override
	public int getMaxStackSize()
	{
		return limit;
	}
	
	@Override
	public void setChanged()
	{
		super.setChanged();
		
	}
}

