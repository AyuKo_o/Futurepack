package futurepack.common.gui;

import java.util.function.DoubleSupplier;

import futurepack.common.FuturepackTags;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.modification.EnumChipType;
import net.minecraft.util.Mth;
import net.minecraft.world.Container;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.RecipeHolder;
import net.minecraft.world.inventory.Slot;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;

public class SlotBaseXPOutput extends Slot
{
	private Player thePlayer;
    private int sacksize;
	private DoubleSupplier base;
    
	public SlotBaseXPOutput(Player player, TileEntityModificationBase base, int slotIndex, int xPosition, int yPosition)
    {
        super((Container) base, slotIndex, xPosition, yPosition);
        this.thePlayer = player;
        this.base = () -> base.getChipPower(EnumChipType.ULTIMATE);
    }
	
	public SlotBaseXPOutput(Player player, Container base, int slotIndex, int xPosition, int yPosition, DoubleSupplier ultimateChipPower)
    {
        super((Container) base, slotIndex, xPosition, yPosition);
        this.thePlayer = player;
        this.base = ultimateChipPower;
    }

    @Override
	public boolean mayPlace(ItemStack stack)
    {
    	return container.canPlaceItem(getSlotIndex(), stack);
    }

    @Override
	public ItemStack remove(int amount)
    {
        if (this.hasItem())
        {
        	sacksize += Math.min(amount, this.getItem().getCount());
        }

        return super.remove(amount);
    }

    @Override
	public void onTake(Player playerIn, ItemStack stack)
    {
        this.checkTakeAchievements(stack);
        super.onTake(playerIn, stack);
    }

    @Override
	protected void onQuickCraft(ItemStack stack, int amount)
    {
        this.sacksize += amount;
        this.checkTakeAchievements(stack);
    }

    @Override
	protected void checkTakeAchievements(ItemStack stack)
    {
        stack.onCraftedBy(this.thePlayer.level, this.thePlayer, this.sacksize);

        if (!this.thePlayer.level.isClientSide)
        {
        	Item crafted = stack.getItem();
            if(crafted.builtInRegistryHolder().is(FuturepackTags.GEMS) || crafted.builtInRegistryHolder().is(FuturepackTags.INGOTS))
            {
            	double f = 0.1D * (1D + base.getAsDouble());

                int i = stack.getCount();
                if (f == 0.0F) {
                   i = 0;
                } else if (f < 1.0F) {
                   int j = Mth.floor(i * f);
                   if (j < Mth.ceil(i * f) && Math.random() < i * f - j) {
                      ++j;
                   }

                   i = j;
                }

                while(i > 0) {
                   int k = ExperienceOrb.getExperienceValue(i);
                   i -= k;
                   this.thePlayer.level.addFreshEntity(new ExperienceOrb(this.thePlayer.level, this.thePlayer.getX(), this.thePlayer.getY() + 0.5D, this.thePlayer.getZ() + 0.5D, k));
                }
             }
            
            if(container instanceof RecipeHolder)
            	((RecipeHolder)this.container).awardUsedRecipes(this.thePlayer);
        }

        this.sacksize = 0;
    }
}
