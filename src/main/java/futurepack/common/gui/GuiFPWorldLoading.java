package futurepack.common.gui;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.client.CreditsManager;
import futurepack.client.CreditsManager.CreditsEntry;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.FormattedCharSequence;

public class GuiFPWorldLoading extends Screen
{
	ResourceLocation res = new ResourceLocation(Constants.MOD_ID,"fp_screen.png");

	long seed = 7528787;
	int i = 0;

	TranslatableComponent text;
	private ArrayList<FormattedCharSequence> trimmedLines;
	private int creditsWidth, creditsHeight;
	
	public GuiFPWorldLoading(TranslatableComponent text)
	{
		super(new TextComponent("futurepack world loading screen"));
		seed = System.currentTimeMillis();
		this.text = text;
	}
	
	@Override
	protected synchronized void init()
	{
		super.init();
		trimmedLines = null;
		creditsHeight = 0;
		creditsWidth = 0;
		getTrimmedLines();
		
	}
	
	private synchronized ArrayList<FormattedCharSequence> getTrimmedLines()
	{
		if(trimmedLines!=null)
			return trimmedLines;
		
		List<Component> lines = getCreditLines(CreditsManager.list);
		creditsWidth = 1;
		for(Component tc : lines)
		{
			creditsWidth = Math.max(creditsWidth, font.width(tc));
		}
		
		if(creditsWidth +25 > (width-256)/2)
		{
			creditsWidth = (width-256)/2 -25;
		}
		trimmedLines = new ArrayList<>(lines.size());
		for(Component tc : lines)
		{
			trimmedLines.addAll(font.split(tc, creditsWidth));
		}
		trimmedLines.trimToSize();
		creditsHeight = 20 + trimmedLines.size() * (font.lineHeight+1);
		
		return trimmedLines;
	}
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY, float partialTicks) 
	{
		
		if(this.minecraft == null)
			return;
		
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		
		fill(matrixStack, 0, 0, width, height, 0xff000000);
		drawStars(matrixStack);

		RenderSystem.setShaderTexture(0, res);
		RenderSystem.setShaderColor(1, 1, 1, 1);
		blit(matrixStack, (width-256)/2, (height-256)/8, 0, 0, 256, 256);//futurepack logo
		
		int y = (height-256)/8 + 64*3;
		
		/*
		 * Draw black background around text to ensure there are no stars behind text
		 */
		fill(matrixStack, 
				(width)/2 - (font.width(text.getString())/2) - 10, 
				y - 10, 
				(width)/2 + (font.width(text.getString())/2) + 10, 
				y + 2*font.lineHeight, 0xff000000);
		
		drawCenteredString(matrixStack, font, text, (width)/2, y, 0xFFFFFFFF);
		
		fill(matrixStack, 5, 5, 5+creditsWidth, 5+creditsHeight, 0xff000000);
		
		int x=15;
		y=15;
		for(FormattedCharSequence rp : getTrimmedLines())
		{
			if(rp!=null)
				font.drawShadow(matrixStack, rp, x, y, 0xFFFFFFFF);
			y+= (font.lineHeight +1);
		}
		
		super.render(matrixStack, mouseX, mouseY, partialTicks);
	}
	
	private static List<Component> getCreditLines(List<CreditsEntry> list)
	{
		ArrayList<Component> lines = new ArrayList<>(list.size() + CreditsManager.EnumCreditsType.values().length);
		
		CreditsManager.EnumCreditsType last = null;
		
		for(CreditsManager.CreditsEntry entry : list)
		{
			if(entry.type!=last)
			{
				lines.add(new TextComponent(entry.type.headline));
				last = entry.type;
			}
			lines.add(new TextComponent(entry.type.formatting + entry.name));
		}
		
		return lines;
		
	}
	
	
	public void drawStars(PoseStack matrixStack)
	{
		Random r = new Random(seed);
		
		for(int x=0;x<width;x++)
		{
			for(int y=0;y<height;y++)
			{
				if(r.nextInt(150)==0)
				{
					
					int base= (100+r.nextInt(156))<<16 | (100+r.nextInt(156))<<8 | (100+r.nextInt(156))<<0;
					fill( matrixStack, x, y, x+1, y+1, 255<<24 | base);
					int a = (int) ((Math.sin(  (this.i+ x*y)/3.0)) * 64) +128;
					fill(matrixStack, x-1, y, x+2, y+1, a<<24 | base);
					fill(matrixStack, x, y-1, x+1, y+2, a<<24 | base);
				}
			}
		}
			
	}
}
