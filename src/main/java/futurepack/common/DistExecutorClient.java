package futurepack.common;

import javax.annotation.Nullable;

import futurepack.api.Constants;
import futurepack.client.ClientEvents;
import futurepack.client.color.ItemBlockColoring;
import futurepack.client.render.RenderDefaultGrenade;
import futurepack.client.render.RenderLaserBowArrow;
import futurepack.client.render.RenderMonitor;
import futurepack.client.render.block.ModelDeepCoreMiner;
import futurepack.client.render.block.ModelEater;
import futurepack.client.render.block.ModelNeonEngine2;
import futurepack.client.render.block.ModelRadar;
import futurepack.client.render.block.ModelRocketLauncher;
import futurepack.client.render.block.ModelSchleuse;
import futurepack.client.render.block.RenderAirlockDoor;
import futurepack.client.render.block.RenderClaime;
import futurepack.client.render.block.RenderCompositeChest;
import futurepack.client.render.block.RenderDeepCoreMiner;
import futurepack.client.render.block.RenderDungeonSpawner;
import futurepack.client.render.block.RenderFallingTreeFast;
import futurepack.client.render.block.RenderFastHologram;
import futurepack.client.render.block.RenderFluidPump;
import futurepack.client.render.block.RenderFluidTank;
import futurepack.client.render.block.RenderForceFieldBlock;
import futurepack.client.render.block.RenderLaserBase;
import futurepack.client.render.block.RenderModularDoor;
import futurepack.client.render.block.RenderMovingBlocksFast;
import futurepack.client.render.block.RenderNeonEngine;
import futurepack.client.render.block.RenderPipe;
import futurepack.client.render.block.RenderRadar;
import futurepack.client.render.block.RenderTectern;
import futurepack.client.render.block.RenderWireRedstone;
import futurepack.client.render.dimension.DimensionRenderTypeAsteroidBelt;
import futurepack.client.render.dimension.DimensionRenderTypeEnvia;
import futurepack.client.render.dimension.DimensionRenderTypeMenelaus;
import futurepack.client.render.dimension.DimensionRenderTypeTyros;
import futurepack.client.render.entity.ModelAlphaJawaul;
import futurepack.client.render.entity.ModelCrawler;
import futurepack.client.render.entity.ModelDungeonSpider;
import futurepack.client.render.entity.ModelEvilRobot;
import futurepack.client.render.entity.ModelForstmaster;
import futurepack.client.render.entity.ModelGehuf;
import futurepack.client.render.entity.ModelHeuler;
import futurepack.client.render.entity.ModelMiner;
import futurepack.client.render.entity.ModelMonoCart;
import futurepack.client.render.entity.ModelRocket;
import futurepack.client.render.entity.ModelWolba;
import futurepack.client.render.entity.RenderAlphaJawaul;
import futurepack.client.render.entity.RenderCrawler;
import futurepack.client.render.entity.RenderCyberZombie;
import futurepack.client.render.entity.RenderDungeonSpider;
import futurepack.client.render.entity.RenderEvilRobot;
import futurepack.client.render.entity.RenderForceField;
import futurepack.client.render.entity.RenderForstmaster;
import futurepack.client.render.entity.RenderGehuf;
import futurepack.client.render.entity.RenderHeuler;
import futurepack.client.render.entity.RenderHook;
import futurepack.client.render.entity.RenderJawaul;
import futurepack.client.render.entity.RenderLaserProjektile;
import futurepack.client.render.entity.RenderMiner;
import futurepack.client.render.entity.RenderMonoCart;
import futurepack.client.render.entity.RenderRocket;
import futurepack.client.render.entity.RenderWolba;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.block.inventory.InventoryBlocks;
import futurepack.common.block.logistic.LogisticBlocks;
import futurepack.common.block.misc.MiscBlocks;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.entity.throwable.EntityGrenadeBase;
import futurepack.common.fluids.FPFluids;
import futurepack.common.gui.GuiFPLoading;
import futurepack.common.item.tools.ItemGrablingHook;
import futurepack.common.item.tools.ItemLogisticEditor;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.recipes.RecipeManagerSyncer;
import futurepack.common.sync.KeyBindAuto;
import futurepack.extensions.albedo.AlbedoMain;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.gui.screens.TitleScreen;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.DimensionSpecialEffects;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.ThrownItemRenderer;
import net.minecraft.client.renderer.item.ItemProperties;
import net.minecraft.client.renderer.item.ItemPropertyFunction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.resources.ReloadableResourceManager;
import net.minecraft.server.packs.resources.ResourceManager;
import net.minecraft.server.packs.resources.SimplePreparableReloadListener;
import net.minecraft.util.profiling.ProfilerFiller;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.projectile.ItemSupplier;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.client.event.EntityRenderersEvent.RegisterRenderers;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLLoadCompleteEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.RegistryObject;

public class DistExecutorClient extends DistExecutorBase
{

	@Override
	public void earlySetup()
	{
		MinecraftForge.EVENT_BUS.register(ClientEvents.INSTANCE);

		IEventBus modBus = FMLJavaModLoadingContext.get().getModEventBus();

		modBus.addListener(this::bindTileRenderers);
		modBus.addListener(this::binEntityRenderers);
		modBus.addListener(this::registerModels);
	}

	@Override
	public void postInitClient(FMLClientSetupEvent event)
	{
		bindItemProperties();
		registerDimensionRenderers();

		AlbedoMain.INSTANCE.init();

		ResourceManager mng = Minecraft.getInstance().getResourceManager();
		if (mng instanceof ReloadableResourceManager)
		{
			((ReloadableResourceManager) mng).registerReloadListener(new SimplePreparableReloadListener<RecipeManagerSyncer>()
			{

				@Override
				protected RecipeManagerSyncer prepare(ResourceManager resourceManagerIn, ProfilerFiller profilerIn)
				{
					return RecipeManagerSyncer.INSTANCE;
				}

				@Override
				protected void apply(RecipeManagerSyncer splashList, ResourceManager resourceManagerIn, ProfilerFiller profilerIn)
				{
					splashList.onClientRecipeReload();
				}
			});
		}

		setRenderLayer(InventoryBlocks.t0_generator, RenderType.cutout());
		setRenderLayer(TerrainBlocks.crystal_neon, RenderType.cutout());
		setRenderLayer(TerrainBlocks.crystal_alutin, RenderType.cutout());
		setRenderLayer(TerrainBlocks.crystal_bioterium, RenderType.cutout());
		setRenderLayer(TerrainBlocks.crystal_glowtite, RenderType.cutout());
		setRenderLayer(TerrainBlocks.crystal_retium, RenderType.cutout());
		setRenderLayer(TerrainBlocks.grass_t, RenderType.cutoutMipped());
		setRenderLayer(MiscBlocks.extern_cooler, RenderType.cutoutMipped());
		setRenderLayer(ModifiableBlocks.external_core, RenderType.cutout());
		setRenderLayer(InventoryBlocks.flash_server_b, RenderType.cutoutMipped());
		setRenderLayer(InventoryBlocks.flash_server_g, RenderType.cutoutMipped());
		setRenderLayer(InventoryBlocks.flash_server_w, RenderType.cutoutMipped());
		setRenderLayer(ModifiableBlocks.fluid_pump, RenderType.cutoutMipped());
		setRenderLayer(LogisticBlocks.fluid_tank, RenderType.cutoutMipped());
		setRenderLayer(LogisticBlocks.fluid_tank_mk2, RenderType.cutoutMipped());
		setRenderLayer(LogisticBlocks.fluid_tank_mk3, RenderType.cutoutMipped());
		setRenderLayer(MiscBlocks.force_field, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_white, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_orange, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_magenta, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_light_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_yellow, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_lime, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_pink, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_light_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_cyan, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_purple, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_brown, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_green, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_red, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_slab_black, RenderType.cutout());
		setRenderLayer(DecoBlocks.metal_gitter_slab, RenderType.cutout());

		setRenderLayer(DecoBlocks.color_gitter_stair_white, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_orange, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_magenta, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_light_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_yellow, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_lime, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_pink, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_light_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_cyan, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_purple, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_brown, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_green, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_red, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_stair_black, RenderType.cutout());
		setRenderLayer(DecoBlocks.metal_stair_gitter, RenderType.cutout());

		setRenderLayer(DecoBlocks.color_gitter_white, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_orange, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_magenta, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_light_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_yellow, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_lime, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pink, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_light_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_cyan, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_purple, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_brown, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_green, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_red, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_black, RenderType.cutout());
		setRenderLayer(DecoBlocks.metal_block_gitter, RenderType.cutout());

		setRenderLayer(DecoBlocks.color_gitter_pane_white, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_orange, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_magenta, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_light_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_yellow, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_lime, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_pink, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_light_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_cyan, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_purple, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_brown, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_green, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_red, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_gitter_pane_black, RenderType.cutout());
		setRenderLayer(DecoBlocks.metal_gitter_pane, RenderType.cutout());

		setRenderLayer(DecoBlocks.color_glass_white, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_orange, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_magenta, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_light_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_yellow, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_lime, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_pink, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_light_gray, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_cyan, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_purple, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_blue, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_brown, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_green, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_red, RenderType.cutout());
		setRenderLayer(DecoBlocks.color_glass_black, RenderType.cutout());
		setRenderLayer(DecoBlocks.metal_glass, RenderType.cutout());

		setRenderLayer(DecoBlocks.ironladder, RenderType.cutout());

		setRenderLayer(LogisticBlocks.monorail, RenderType.cutout());
		setRenderLayer(LogisticBlocks.monorail_booster, RenderType.cutout());
		setRenderLayer(LogisticBlocks.monorail_charger, RenderType.cutout());
		setRenderLayer(LogisticBlocks.monorail_detector, RenderType.cutout());
		setRenderLayer(LogisticBlocks.monorail_lift, RenderType.cutout());
		setRenderLayer(LogisticBlocks.monorail_oneway, RenderType.cutout());
		setRenderLayer(LogisticBlocks.monorail_station, RenderType.cutout());
		setRenderLayer(LogisticBlocks.monorail_waypoint, RenderType.cutout());
//		setRenderLayer(LogisticBlocks.plasma_core_t1, RenderType.solid());
		setRenderLayer(LogisticBlocks.plasma_pipe_t1, RenderType.solid());

		setRenderLayer(TerrainBlocks.prismide, RenderType.cutout());
		setRenderLayer(TerrainBlocks.blueshroom, RenderType.cutout());
		setRenderLayer(TerrainBlocks.bubbleshroom, RenderType.cutout());
		setRenderLayer(TerrainBlocks.hyticus, RenderType.cutout());
		setRenderLayer(TerrainBlocks.peakhead, RenderType.cutout());

		setRenderLayer(TerrainBlocks.door_tyros, RenderType.cutout());
		setRenderLayer(TerrainBlocks.door_menelaus_mushroom, RenderType.cutout());
		setRenderLayer(TerrainBlocks.door_tyros, RenderType.cutout());
		setRenderLayer(TerrainBlocks.door_palirie, RenderType.cutout());

		setRenderLayer(TerrainBlocks.trapdoor_menelaus_mushroom, RenderType.cutout());
		setRenderLayer(TerrainBlocks.trapdoor_palirie, RenderType.cutout());
		setRenderLayer(TerrainBlocks.trapdoor_tyros, RenderType.cutout());

		setRenderLayer(DecoBlocks.composite_door, RenderType.cutout());

		setRenderLayer(DecoBlocks.glass_door, RenderType.cutout());
		setRenderLayer(DecoBlocks.glass_trapdoor, RenderType.cutout());
		setRenderLayer(DecoBlocks.quartz_glass, RenderType.cutout());

		setRenderLayer(PlantBlocks.mendel_berry, RenderType.cutout());
		setRenderLayer(PlantBlocks.erse, RenderType.cutout());
		setRenderLayer(PlantBlocks.glowmelo, RenderType.cutout());
		setRenderLayer(PlantBlocks.leaves_tyros, RenderType.cutout());
		setRenderLayer(PlantBlocks.oxades, RenderType.cutout());
		setRenderLayer(PlantBlocks.sapling_mushroom, RenderType.cutout());
		setRenderLayer(PlantBlocks.sapling_palirie, RenderType.cutout());
		setRenderLayer(PlantBlocks.sapling_tyros, RenderType.cutout());
		setRenderLayer(PlantBlocks.topinambur, RenderType.cutout());

		setRenderLayer(LogisticBlocks.pipe_normal, RenderType.cutout());
		setRenderLayer(LogisticBlocks.pipe_neon, RenderType.cutout());
		setRenderLayer(LogisticBlocks.pipe_redstone, RenderType.cutout());
		setRenderLayer(LogisticBlocks.pipe_support, RenderType.cutout());
		setRenderLayer(LogisticBlocks.insert_node, RenderType.cutout());

		ItemBlockRenderTypes.setRenderLayer(FPFluids.neonFluidStill, RenderType.translucent());
		ItemBlockRenderTypes.setRenderLayer(FPFluids.neonFluid_flowing, RenderType.translucent());
		ItemBlockRenderTypes.setRenderLayer(FPFluids.salt_waterStill, RenderType.translucent());
		ItemBlockRenderTypes.setRenderLayer(FPFluids.salt_water_flowing, RenderType.translucent());

		setRenderLayer(LogisticBlocks.plasma_jar, RenderType.cutout());
		setRenderLayer(LogisticBlocks.plasma_converter_t0, RenderType.cutout());
		setRenderLayer(LogisticBlocks.plasma_2_neon_t0, RenderType.translucent());

		setRenderLayer(ModifiableBlocks.radar, RenderType.cutout());
	}

	private static void setRenderLayer(RegistryObject<Block> block, RenderType type)
	{
		setRenderLayer(block.get(), type);
	}

	private static void setRenderLayer(Block block, RenderType type)
	{
		ItemBlockRenderTypes.setRenderLayer(block, type);
	}

	private void bindTileRenderers(EntityRenderersEvent.RegisterRenderers event)
	{

		event.registerBlockEntityRenderer(FPTileEntitys.COMPOSITE_CHEST, RenderCompositeChest::new);
		event.registerBlockEntityRenderer(FPTileEntitys.WIRE_REDSTONE, RenderWireRedstone::new);

		event.registerBlockEntityRenderer(FPTileEntitys.RESEARCH_EXCHANGE, RenderTectern::new);
		event.registerBlockEntityRenderer(FPTileEntitys.AIRLOCK_DDOR, RenderAirlockDoor::new);

		// Die sind eigentlich okay nur die TE Renderer sind unvollständig

		event.registerBlockEntityRenderer(FPTileEntitys.WIRE_NETWORK, RenderFastHologram::new);
		event.registerBlockEntityRenderer(FPTileEntitys.WIRE_NORMAL, RenderFastHologram::new);
		event.registerBlockEntityRenderer(FPTileEntitys.WIRE_SUPER, RenderFastHologram::new);
		event.registerBlockEntityRenderer(FPTileEntitys.WIRE_SUPPORT, RenderFastHologram::new);
		event.registerBlockEntityRenderer(FPTileEntitys.PIPE_NORMAL, RenderPipe::new);
		event.registerBlockEntityRenderer(FPTileEntitys.PIPE_NEON, RenderPipe::new);
		event.registerBlockEntityRenderer(FPTileEntitys.PIPE_SUPPORT, RenderPipe::new);
		event.registerBlockEntityRenderer(FPTileEntitys.INSERT_NODE, RenderPipe::new);
		event.registerBlockEntityRenderer(FPTileEntitys.ENTITY_EATER, RenderLaserBase::new);
		event.registerBlockEntityRenderer(FPTileEntitys.ENTITY_HEALER, RenderLaserBase::new);
		event.registerBlockEntityRenderer(FPTileEntitys.ENTITY_KILLER, RenderLaserBase::new);
		event.registerBlockEntityRenderer(FPTileEntitys.ROCKET_LAUNCHER, RenderLaserBase::new);

		event.registerBlockEntityRenderer(FPTileEntitys.NEON_ENGINE, RenderNeonEngine::new);
		event.registerBlockEntityRenderer(FPTileEntitys.FALLING_TREE, RenderFallingTreeFast::new);
		event.registerBlockEntityRenderer(FPTileEntitys.DUNGEON_SPAWNER, RenderDungeonSpawner::new);
		event.registerBlockEntityRenderer(FPTileEntitys.FORCE_FIELD, RenderForceFieldBlock::new);
//	    event.registerBlockEntityRenderer(FPTileEntitys.PLASMA_CORE_T1, RenderPlasmaTank::new);
		event.registerBlockEntityRenderer(FPTileEntitys.DEEPCORE_MAIN, RenderDeepCoreMiner::new);
		event.registerBlockEntityRenderer(FPTileEntitys.CLAIME, RenderClaime::new);
		event.registerBlockEntityRenderer(FPTileEntitys.MODULAR_DOOR, RenderModularDoor::new);

		event.registerBlockEntityRenderer(FPTileEntitys.FLUID_PUMP, RenderFluidPump::new);
		event.registerBlockEntityRenderer(FPTileEntitys.FLUID_TANK, RenderFluidTank::new);

		event.registerBlockEntityRenderer(FPTileEntitys.MOVING_BLOCKS, RenderMovingBlocksFast::new);

		event.registerBlockEntityRenderer(FPTileEntitys.RADAR, RenderRadar::new);
		event.registerBlockEntityRenderer(FPTileEntitys.FORCEFIELD_GENERATOR, RenderClaime::new);

	}

	private void binEntityRenderers(EntityRenderersEvent.RegisterRenderers event)
	{
		entityRenderItem(FPEntitys.ENTITY_EGGER, event);
		entityRenderItem(FPEntitys.GRENADE_PLASMA, event);
		entityRenderItem(FPEntitys.GRENADE_BLAZE, event);
		entityRenderItem(FPEntitys.GRENADE_SLIME, event);
		entityRenderItem(FPEntitys.WAKURIUM_THROWN, event);
		event.registerEntityRenderer(FPEntitys.LASER_PROJECTILE, RenderLaserProjektile::new);
		event.registerEntityRenderer(FPEntitys.CRAWLER, RenderCrawler::new);
		event.registerEntityRenderer(FPEntitys.GEHUF, RenderGehuf::new);
		event.registerEntityRenderer(FPEntitys.WOLBA, RenderWolba::new);
		event.registerEntityRenderer(FPEntitys.MONOCART, RenderMonoCart::new);
		event.registerEntityRenderer(FPEntitys.MINER, RenderMiner::new);
		event.registerEntityRenderer(FPEntitys.HEULER, RenderHeuler::new);
		entityGrenade(FPEntitys.GRENADE_NORMAL, "textures/model/granate_geschoss.png", event);
		entityGrenade(FPEntitys.GRENADE_KOMPOST, "textures/model/kompost_granate_geschoss.png", event);
		entityGrenade(FPEntitys.GRENADE_SAAT, "textures/model/saat_granate_geschoss.png", event);
		entityGrenade(FPEntitys.GRENADE_FUTTER, "textures/model/futter_granate_geschoss.png", event);
		entityRenderItem(FPEntitys.GRENADE_ENTITY_EGGER, event);
		event.registerEntityRenderer(FPEntitys.MONITOR, RenderMonitor::new);
		event.registerEntityRenderer(FPEntitys.LASER, RenderLaserBowArrow::new);
		event.registerEntityRenderer(FPEntitys.ROCKET_BIOTERIUM, RenderRocket::new);
		event.registerEntityRenderer(FPEntitys.ROCKET_BLAZE, RenderRocket::new);
		event.registerEntityRenderer(FPEntitys.ROCKET_NORMAL, RenderRocket::new);
		event.registerEntityRenderer(FPEntitys.ROCKET_PLASMA, RenderRocket::new);
		event.registerEntityRenderer(FPEntitys.EVIL_ROBOT, RenderEvilRobot::new);
		event.registerEntityRenderer(FPEntitys.CYBER_ZOMBIE, RenderCyberZombie::new);
		event.registerEntityRenderer(FPEntitys.FORESTMASTER, RenderForstmaster::new);
		event.registerEntityRenderer(FPEntitys.FORCE_FIELD, RenderForceField::new);
		event.registerEntityRenderer(FPEntitys.HOOK, RenderHook::new);
		event.registerEntityRenderer(FPEntitys.JAWAUL, RenderJawaul::new);
		event.registerEntityRenderer(FPEntitys.DUNGEON_SPIDER, RenderDungeonSpider::new);
		event.registerEntityRenderer(FPEntitys.ALPHA_JAWAUL, RenderAlphaJawaul::new);
	}

	public void bindItemProperties()
	{
		ItemProperties.register(ToolItems.spawner_chest, new ResourceLocation("full"), new ItemPropertyFunction()
		{
			@Override
			public float call(ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity liv, int specialSeedThing)
			{
				if (stack.getTagElement("spawner") != null)
					return 1F;

				return 0F;
			}
		});
		ItemProperties.register(ToolItems.escanner, new ResourceLocation("color"), new ItemPropertyFunction()
		{
			@Override
			public float call(ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity liv, int specialSeedThing)
			{
				if (stack.hasTag() && stack.getTag().getBoolean("s"))
					return 1F;

				return 0F;
			}
		});
		ItemProperties.register(ToolItems.grappling_hook, new ResourceLocation("throwing"), new ItemPropertyFunction()
		{
			@Override
			public float call(ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity liv, int specialSeedThing)
			{
				return liv != null && ((ItemGrablingHook) ToolItems.grappling_hook).map(true).containsKey(liv) ? 1F : 0F;
			}
		});
		ItemPropertyFunction pull = new ItemPropertyFunction()
		{
			@Override
			public float call(ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity liv, int specialSeedThing)
			{
				if (liv == null)
				{
					return 0.0F;
				} else
				{
					return liv.getUseItem() == stack ? (stack.getUseDuration() - liv.getUseItemRemainingTicks()) / 20.0F : 0.0F;
				}
			}
		};
		ItemProperties.register(ToolItems.grenade_launcher, new ResourceLocation("pull"), pull);
		ItemProperties.register(ToolItems.laser_bow, new ResourceLocation("pull"), pull);
		ItemPropertyFunction pulling = (p_174630_, p_174631_, p_174632_, p_174633_) -> {
			return p_174632_ != null && p_174632_.isUsingItem() && p_174632_.getUseItem() == p_174630_ ? 1.0F : 0.0F;
		};
		ItemProperties.register(ToolItems.laser_bow, new ResourceLocation("pulling"), pulling);
		ItemProperties.register(ToolItems.grenade_launcher, new ResourceLocation("pulling"), pulling);
		ItemProperties.register(ToolItems.logisticEditor, new ResourceLocation("modes"), new ItemPropertyFunction()
		{
			@Override
			public float call(ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity liv, int specialSeedThing)
			{
				float f = ItemLogisticEditor.getModeFromItem(stack).ID / 10F;
				return f;
			}
		});
		ItemProperties.register(ToolItems.scrench, new ResourceLocation("rotate"), new ItemPropertyFunction()
		{
			@Override
			public float call(ItemStack stack, @Nullable ClientLevel world, @Nullable LivingEntity liv, int specialSeedThing)
			{
				if (stack.hasTag() && stack.getTag().getBoolean("rotate"))
					return 1F;

				return 0F;
			}
		});
	}

	private static final <T extends Entity & ItemSupplier> void entityRenderItem(EntityType<T> cls, RegisterRenderers event)
	{
		event.registerEntityRenderer(cls, m -> new ThrownItemRenderer<>(m, 1F, false));
	}

	private static final void entityGrenade(EntityType<? extends EntityGrenadeBase> cls, String texture, RegisterRenderers event)
	{
		event.registerEntityRenderer(cls, m -> new RenderDefaultGrenade(m, new ResourceLocation(Constants.MOD_ID, texture)));
	}

	private void registerDimensionRenderers()
	{
		DimensionSpecialEffects.EFFECTS.put(new ResourceLocation(Constants.MOD_ID, "menelaus"), new DimensionRenderTypeMenelaus());
		DimensionSpecialEffects.EFFECTS.put(new ResourceLocation(Constants.MOD_ID, "tyros"), new DimensionRenderTypeTyros());
		DimensionSpecialEffects.EFFECTS.put(new ResourceLocation(Constants.MOD_ID, "envia"), new DimensionRenderTypeEnvia());
		DimensionSpecialEffects.EFFECTS.put(new ResourceLocation(Constants.MOD_ID, "space_sky"), new DimensionRenderTypeAsteroidBelt());
	}

	@Override
	public void setupFinished(FMLLoadCompleteEvent event)
	{
//		FuturepackDefaultResources.downloadFuturepackRersources(); TODO reanble this and rework JSOn resource system

		super.setupFinished(event);

		ItemBlockColoring.setupColoring();
		KeyBindAuto.init();

		Thread t = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				FPLog.logger.info("Starting Menue Thread");
				while (FPConfig.CLIENT.futurepackStartMenu.get())
				{
					GuiComponent g = Minecraft.getInstance().screen;
					if (g instanceof TitleScreen)
					{
						Minecraft.getInstance().setScreen(new GuiFPLoading());
						FPLog.logger.info("Hacked Menue");
						break;
					}
					try
					{
						Thread.sleep(1);
					} catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		}, "FP-Menue Thread");
		t.start();

//	    IReloadableResourceManager res = (IReloadableResourceManager) Minecraft.getInstance().getResourceManager();
//	    res.addReloadListener(new DataFolderWalker.RecipeReloadListener(null));
	}

	public void registerModels(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(ModelGehuf.LAYER_LOCATION, ModelGehuf::createBodyLayer);
		event.registerLayerDefinition(ModelDeepCoreMiner.LAYER_LOCATION, ModelDeepCoreMiner::createBodyLayer);
		event.registerLayerDefinition(ModelEater.LAYER_LOCATION, ModelEater::createBodyLayer);
		event.registerLayerDefinition(ModelRocketLauncher.LAYER_LOCATION, ModelRocketLauncher::createBodyLayer);
		event.registerLayerDefinition(ModelWolba.LAYER_LOCATION, ModelWolba::createBodyLayer);
		event.registerLayerDefinition(ModelMonoCart.LAYER_LOCATION, ModelMonoCart::createBodyLayer);
		event.registerLayerDefinition(ModelMiner.LAYER_LOCATION, ModelMiner::createBodyLayer);
		event.registerLayerDefinition(ModelHeuler.LAYER_LOCATION, ModelHeuler::createBodyLayer);
		event.registerLayerDefinition(ModelRocket.LAYER_LOCATION, ModelRocket::createBodyLayer);
		event.registerLayerDefinition(ModelForstmaster.LAYER_LOCATION, ModelForstmaster::createBodyLayer);
		event.registerLayerDefinition(ModelCrawler.LAYER_LOCATION, ModelCrawler::createBodyLayer);
		event.registerLayerDefinition(ModelDungeonSpider.LAYER_LOCATION, ModelDungeonSpider::createBodyLayer);
		event.registerLayerDefinition(ModelNeonEngine2.LAYER_LOCATION, ModelNeonEngine2::createBodyLayer);
		event.registerLayerDefinition(ModelSchleuse.LAYER_LOCATION, ModelSchleuse::createBodyLayer);
		event.registerLayerDefinition(ModelAlphaJawaul.LAYER_LOCATION, ModelAlphaJawaul::createBodyLayer);
		event.registerLayerDefinition(ModelRadar.LAYER_LOCATION, ModelRadar::createBodyLayer);

		event.registerLayerDefinition(ModelEvilRobot.LAYER_LOCATION, ModelEvilRobot::createBodyLayer);
		event.registerLayerDefinition(ModelEvilRobot.LAYER_LOCATION_ARMOR, ModelEvilRobot::createArmorLayer);
	}
}
