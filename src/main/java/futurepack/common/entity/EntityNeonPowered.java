package futurepack.common.entity;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.NetworkHooks;

public abstract class EntityNeonPowered extends Entity
{
	private static final EntityDataAccessor<Float> power = SynchedEntityData.defineId(EntityNeonPowered.class, EntityDataSerializers.FLOAT);

	/**
	 * This is the Maximal amount of power that can be stored
	 */
	private final float maxPowerStorage;
	
	protected boolean consumePowerForTick = true;
	
	public EntityNeonPowered(EntityType<? extends EntityNeonPowered> type, Level w, float maxStorage)
	{
		super(type, w);
		maxPowerStorage = maxStorage;
	}

	@Override
	protected void defineSynchedData()
	{
		this.entityData.define(power, 0F);//power
	}

	@Override
	protected void readAdditionalSaveData(CompoundTag nbt)
	{
		setPower(nbt.getFloat("power"));
	}

	@Override
	protected void addAdditionalSaveData(CompoundTag nbt)
	{
		nbt.putFloat("power", getPower());
	}
	
	public float getPower()
	{
		return this.entityData.get(power);
	}	
	public void setPower(float f)
	{
		this.entityData.set(power, f);
	}

	public float getMaxPower()
	{
		return maxPowerStorage;
	}
	
	/**
	 * @return how many Energie used per Tick
	 */
	protected abstract float getEnergieUse();
	
	public boolean consumePower()
	{
		float power = getPower();
		if(power>getEnergieUse())
		{
			power-= getEnergieUse();
			setPower(power);
			return true;
		}
		return false;
	}
	
	@Override
	public void tick() 
	{
		if(getPower()<getMaxPower())
		{
			tryCharge();
		}
		if(consumePowerForTick)
		{
			if(consumePower())
			{
				super.tick();
			}
		}
		else
		{
			super.tick();
		}
	}
	
	protected abstract void tryCharge();

	@Override
	public Packet<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
