package futurepack.common.entity;

import java.util.ArrayList;

import com.google.common.collect.Lists;

import futurepack.common.FPEntitys;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.item.tools.ToolItems;
import futurepack.depend.api.helper.HelperBoundingBoxes;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.decoration.Painting;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.entity.IEntityAdditionalSpawnData;
import net.minecraftforge.network.NetworkHooks;

public class EntityMonitor extends Painting implements IEntityAdditionalSpawnData
{	
	private EntityMonitor.EnumMonitor art;
	private EntityMonitor.EnumMonitor poweredArt = null;

	private static final EntityDataAccessor<Integer> overwirttenArt = SynchedEntityData.defineId(EntityMonitor.class, EntityDataSerializers.INT);
	
	public EntityMonitor(Level worldIn)
	{
		super(FPEntitys.MONITOR, worldIn);
	}
	
	public EntityMonitor(EntityType<EntityMonitor> type, Level worldIn)
	{
		super(type, worldIn);
	}


	public EntityMonitor(Level worldIn, BlockPos pos, Direction side)
	{
		super(FPEntitys.MONITOR, worldIn);
		this.pos = pos;
		
		ArrayList<EnumMonitor> arraylist = Lists.newArrayList();
		EntityMonitor.EnumMonitor[] aEnumMonitor = EntityMonitor.EnumMonitor.values();
		int i = aEnumMonitor.length;

		for (int j = 0; j < i; ++j)
		{
			EntityMonitor.EnumMonitor EnumMonitor = aEnumMonitor[j];
			this.art = EnumMonitor;
			this.setDirection(side);

			if (this.survives())
			{
				arraylist.add(EnumMonitor);
			}
		}

		if (!arraylist.isEmpty())
		{
			this.art = arraylist.get(this.random.nextInt(arraylist.size()));
		}

		this.setDirection(side);
		return;
	}
	
	//@ TODO: OnlyIn(Dist.CLIENT)
	public EntityMonitor(Level worldIn, BlockPos pos, Direction side, String name)
	{
		this(worldIn, pos, side);
		EntityMonitor.EnumMonitor[] aEnumMonitor = EntityMonitor.EnumMonitor.values();
		int i = aEnumMonitor.length;

		for (int j = 0; j < i; ++j)
		{
			EntityMonitor.EnumMonitor EnumMonitor = aEnumMonitor[j];

			if (EnumMonitor.title.equals(name))
			{
				this.art = EnumMonitor;
				break;
			}
		}

		this.setDirection(side);
	}
	
	@Override
	protected void defineSynchedData() 
	{
		super.defineSynchedData();
		this.entityData.define(overwirttenArt, -1);
	}

	private int tickCooldown=0;
	private int block = 0;
	BlockPos[][][] posses = null;
	private boolean powered = false;
	
	private boolean isPowered = false;
	
	@Override
	public void tick()
	{
		this.setDeltaMovement(Vec3.ZERO);
		
		super.tick();
		if(!this.level.isClientSide)
		{
			
			if(tickCooldown--<=0)
			{
				tickCooldown=3;
				
				if(posses == null)
				{
					posses = HelperBoundingBoxes.splitToBlocks(this.getBoundingBox());
				}
				
				int x = block % posses.length;
				int y = (block / posses.length) % posses[x].length;
				int z = (block / posses.length / posses[x].length);
				
				if(posses[x][y].length <= z)
				{
					if(poweredArt==null)
						poweredArt = art;
					
					this.entityData.set(overwirttenArt, (powered ? poweredArt : art).ordinal());
					isPowered = powered;
					posses = null;
					block = 0;
					powered = false;
				}
				else
				{
					if(direction.getAxis() == Axis.X)
					{
						block += posses.length;
					}
					else if(direction.getAxis() == Axis.Z)
					{
						block += posses[x][y].length;
					}
					else
					{
						block++;
					}
					
					BlockPos pos = posses[x][y][z];
					
					((ServerLevel)level).addParticle(ParticleTypes.NOTE, pos.getX(), pos.getY(), pos.getZ(), 0, 0, 0);
					
					if(level.hasNeighborSignal(pos))
					{
						powered = true;
					}
				}
			}
		}
	}
	
	@Override
	public InteractionResult interact(Player player, InteractionHand hand) 
	{
		if(!level.isClientSide)
		{
			if(player.getItemInHand(hand).getItem() == ToolItems.scrench && !player.getItemInHand(hand).isEmpty())
			{
				EnumMonitor now = isPowered ? poweredArt : art;
				if(now==null)
					now = art;
				
				int j = now.ordinal();
				EnumMonitor[] possible = EnumMonitor.values();
				do
				{
					j++;
					j %= possible.length;
				}
				while(!possible[j].sameSize(now));
				
				if(isPowered)
					poweredArt = possible[j];
				else
					art = possible[j];
			}
		}
		return super.interact(player, hand);
	}
	
	



	/**
	 * (abstract) Protected helper method to write subclass entity data to NBT.
	 */
	@Override
	public void addAdditionalSaveData(CompoundTag tagCompound)
	{
		tagCompound.putString("MotiveOver", this.art.title);
		if(this.poweredArt !=null)
			tagCompound.putString("MotivePowered", this.poweredArt.title);
		super.addAdditionalSaveData(tagCompound);
	}

	/**
	 * (abstract) Protected helper method to read subclass entity data from NBT.
	 */
	@Override
	public void readAdditionalSaveData(CompoundTag tagCompund)
	{
		String s = tagCompund.getString("MotiveOver");
		EntityMonitor.EnumMonitor[] aEnumMonitor = EntityMonitor.EnumMonitor.values();
		int i = aEnumMonitor.length;

		for (int j = 0; j < i; ++j)
		{
			EntityMonitor.EnumMonitor mon = aEnumMonitor[j];

			if (mon.title.equals(s))
			{
				this.art = mon;
			}
		}

		if (this.art == null)
		{
			this.art = EntityMonitor.EnumMonitor.Monitor1;
		}

		if(tagCompund.contains("MotivePowered"))
		{
			String sp = tagCompund.getString("MotivePowered");
			
			for(EnumMonitor m : EnumMonitor.values())
			{
				if (m.title.equals(sp))
				{
					this.poweredArt = m;
				}
			}
		}
		
		super.readAdditionalSaveData(tagCompund);
	}

	@Override
	public int getWidth()
	{
		return art==null ? 16: this.art.sizeX;
	}

	@Override
	public int getHeight()
	{
		return art==null ? 16: this.art.sizeY;
	}

	/**
	 * Called when this entity is broken. Entity parameter may be null.
	 */
	@Override
	public void dropItem(Entity entity)
	{
		if (this.level.getGameRules().getBoolean(GameRules.RULE_DOBLOCKDROPS))
		{
			 this.playSound(SoundEvents.METAL_BREAK, 1.0F, 1.0F);
			
			if (entity instanceof Player)
			{
				Player entityplayer = (Player)entity;

				if (entityplayer.isCreative())
				{
					return;
				}
			}

			this.spawnAtLocation(new ItemStack(MiscItems.display), 0.0F);
		}
	}

	/**
	 * Sets the location and Yaw/Pitch of an entity in the world
	 */
	@Override
	public void moveTo(double x, double y, double z, float yaw, float pitch)
	{
		this.setPos(x, y, z);
	}

	/**
	 * Set the position and rotation values directly without any clamping.
	 */
	@Override
	//@ TODO: OnlyIn(Dist.CLIENT)
	public void lerpTo(double x, double y, double z, float yaw, float pitch, int posRotationIncrements, boolean teleport)
	{
		BlockPos blockpos = this.pos.offset(x - this.getX(), y - this.getY(), z - this.getZ());
		this.setPos(blockpos.getX(), blockpos.getY(), blockpos.getZ());
	}
	
	public static enum EnumMonitor
	{
		Monitor1("Monitor1", 16, 16, 0, 0),
		Monitor2("Monitor2", 16, 16, 16, 0),
		Monitor3("Monitor3", 16, 16, 32, 0),
		Monitor4("Monitor4", 16, 16, 48, 0),
		Monitor5("Monitor5", 16, 16, 64, 0),
		Monitor6("Monitor6", 16, 16, 80, 0),
		Monitor7("Monitor7", 16, 16, 96, 0),
		Monitor8("Monitor8", 16, 16, 112, 0),
		Monitor9("Monitor9", 16, 16, 128, 0),
		Monitor10("Monitor10", 16, 16, 144, 0),
		Monitor11("Monitor11", 16, 16, 160, 0),
		Monitor12("Monitor12", 16, 16, 176, 0),
		
		Monitor21("Monitor21", 16, 16, 0, 16),
		Monitor22("Monitor22", 16, 16, 16, 16),
		Monitor23("Monitor23", 16, 16, 32, 16),
		Monitor24("Monitor24", 16, 16, 48, 16),
		Monitor25("Monitor25", 16, 16, 64, 16),
		Monitor26("Monitor26", 16, 16, 80, 16),
		Monitor27("Monitor27", 16, 16, 96, 16),
		Monitor28("Monitor28", 16, 16, 112, 16),
		Monitor29("Monitor29", 16, 16, 128, 16),
		Monitor210("Monitor210", 16, 16, 144, 16),
		Monitor211("Monitor211", 16, 16, 160, 16),
		Monitor212("Monitor212", 16, 16, 176, 16),
		
		doppelMonitor1("doppelMonitor1", 32, 16, 0, 32),
		doppelMonitor2("doppelMonitor2", 32, 16, 32, 32),
		doppelMonitor3("doppelMonitor3", 32, 16, 64, 32),
		doppelMonitor4("doppelMonitor4", 32, 16, 96, 32),
		doppelMonitor5("doppelMonitor5", 32, 16, 128, 32),
		doppelMonitor6("doppelMonitor6", 32, 16, 160, 32),
		doppelMonitor7("doppelMonitor7", 32, 16, 0, 48),
		doppelMonitor8("doppelMonitor8", 32, 16, 32, 48),
		doppelMonitor9("doppelMonitor9", 32, 16, 64, 48),
		doppelMonitor10("doppelMonitor10", 32, 16, 96, 48),
		doppelMonitor11("doppelMonitor11", 32, 16, 128, 48),
		doppelMonitor12("doppelMonitor12", 32, 16, 160, 48),
		
		quadMonitor1("quadMonitor1", 32, 32, 0, 64),
		quadMonitor2("quadMonitor2", 32, 32, 32, 64),
		quadMonitor3("quadMonitor3", 32, 32, 64, 64),
		quadMonitor4("quadMonitor4", 32, 32, 96, 64),
		quadMonitor5("quadMonitor5", 32, 32, 128, 64),
		quadMonitor6("quadMonitor6", 32, 32, 160, 64),
		quadMonitor7("quadMonitor7", 32, 32, 192, 64),
		quadMonitor8("quadMonitor8", 32, 32, 224, 64),
		quadMonitor9("quadMonitor9", 32, 32, 0, 96),
		quadMonitor10("quadMonitor10", 32, 32, 32, 96),
		quadMonitor11("quadMonitor11", 32, 32, 64, 96),
		quadMonitor12("quadMonitor12", 32, 32, 96, 96),
		quadMonitor13("quadMonitor13", 32, 32, 128, 96),
		quadMonitor14("quadMonitor14", 32, 32, 160, 96),
		quadMonitor15("quadMonitor15", 32, 32, 192, 96),
		quadMonitor16("quadMonitor16", 32, 32, 224, 96),
		
		hexMonitor1("hexMonitor1", 48, 48, 0, 128),
		hexMonitor2("hexMonitor2", 48, 48, 48, 128),
		hexMonitor3("hexMonitor3", 48, 48, 96, 128),
		hexMonitor4("hexMonitor4", 48, 48, 144, 128),
		hexMonitor5("hexMonitor5", 48, 48, 192, 128),
		
		highMonitor1("highMonitor1", 16, 32, 0, 176),
		highMonitor2("highMonitor2", 16, 32, 16, 176),
		highMonitor3("highMonitor3", 16, 32, 32, 176),
		highMonitor4("highMonitor4", 16, 32, 48, 176),
		highMonitor5("highMonitor5", 16, 32, 64, 176),
		highMonitor6("highMonitor6", 16, 32, 80, 176),
		highMonitor7("highMonitor7", 16, 32, 96, 176),
		highMonitor8("highMonitor8", 16, 32, 112, 176),
		highMonitor9("highMonitor9", 16, 32, 128, 176),
		highMonitor10("highMonitor10", 16, 32, 144, 176),
		highMonitor11("highMonitor11", 16, 32, 160, 176),
		highMonitor12("highMonitor12", 16, 32, 176, 176),
		highMonitor13("highMonitor13", 16, 32, 192, 176),
		highMonitor14("highMonitor14", 16, 32, 208, 176),
		highMonitor15("highMonitor15", 16, 32, 224, 176),
		highMonitor16("highMonitor16", 16, 32, 240, 176),
		
		bigMonitor1("bigMonitor1", 64, 48, 0, 208),
		bigMonitor2("bigMonitor2", 64, 48, 64, 208),
		bigMonitor3("bigMonitor3", 64, 48, 128, 208),
		bigMonitor4("bigMonitor4", 64, 48, 192, 208),
		
		Background("Background", 48, 48, 12*16, 0),
		;

		/** Holds the maximum length of paintings art title. */
		public static final int maxArtTitleLength = "doppelMonitor10".length();
		/** Painting Title. */
		public final String title;
		public final int sizeX;
		public final int sizeY;
		public final int offsetX;
		public final int offsetY;

		private static final String __OBFID = "CL_00001557";

		private EnumMonitor(String name, int width, int height, int uvX, int uvY)
		{
			this.title = name;
			this.sizeX = width;
			this.sizeY = height;
			this.offsetX = uvX;
			this.offsetY = uvY;
		}

		public boolean sameSize(EnumMonitor now) 
		{
			return now==null ? false : (now.sizeX==sizeX && now.sizeY == sizeY);
		}

		public float getMinU() 
		{
			return offsetX / 256F;
		}

		public float getMaxU() 
		{
			return (offsetX + sizeX) / 256F;
		}

		public float getMinV() 
		{
			return offsetY / 256F;
		}

		public float getMaxV() 
		{
			return (offsetY + sizeY) / 256F;
		}
		
		public float getInterpolatedU(double d) 
		{
			return (float) ((offsetX + sizeX * d/16F) / 256F);
		}

		public float getInterpolatedV(double d) 
		{
			return (float) ((offsetY + sizeY*d/16F) / 256F);
		}

		
	}

	@Override
	public void writeSpawnData(FriendlyByteBuf buffer)
	{
		buffer.writeVarInt(this.art.ordinal());
		
		buffer.writeVarInt(this.pos.getX());
		buffer.writeVarInt(this.pos.getY());
		buffer.writeVarInt(this.pos.getZ());
		buffer.writeByte(this.getDirection().get3DDataValue());
	}

	@Override
	public void readSpawnData(FriendlyByteBuf buffer)
	{
		EntityMonitor.EnumMonitor[] aenumart = EntityMonitor.EnumMonitor.values();
		this.art = aenumart[buffer.readVarInt()];
		
		int x = buffer.readVarInt();
		int y = buffer.readVarInt();
		int z = buffer.readVarInt();
	   
		BlockPos pos = new BlockPos(x, y, z);
		this.pos = pos;
		this.setDirection(Direction.from3DDataValue((buffer.readByte())));
	}

	@Override
	public void playPlacementSound()
	{
		this.playSound(SoundEvents.METAL_PLACE, 1.0F, 1.0F);
	}
	
	/**
	 * Called when the entity is attacked.
	 */
	@Override
	public boolean hurt(DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(source))
		{
			return false;
		}
		else
		{
			if(source.getEntity() instanceof Player)
			{
				if (!this.isAlive()==false && !this.level.isClientSide)
				{
					this.discard();
					this.markHurt();
					this.dropItem(source.getEntity());
				}
				return true;
			}
		}
		return false;
	}
	
	
	public EnumMonitor getArt()
	{
		int overwrite = this.entityData.get(overwirttenArt);
		if(overwrite>=0)
		{
			return EnumMonitor.values()[overwrite];
		}
		return art;
	}

	@Override
	public Packet<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
