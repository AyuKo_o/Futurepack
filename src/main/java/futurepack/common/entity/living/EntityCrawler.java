package futurepack.common.entity.living;

import java.util.UUID;

import javax.annotation.Nullable;

import futurepack.common.FPEntitys;
import futurepack.common.block.terrain.TerrainBlocks;
import futurepack.common.entity.ai.AIBuildNest;
import futurepack.common.entity.ai.EntityAIHide;
import futurepack.common.entity.ai.IHideableCreature;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.BlockParticleOption;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.Pose;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.monster.Spider;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class EntityCrawler extends Spider implements IHideableCreature
{
	private static final EntityDataAccessor<Boolean> HIDING = SynchedEntityData.defineId(EntityCrawler.class, EntityDataSerializers.BOOLEAN);
	private static final EntityDataAccessor<Boolean> IS_CHILD = SynchedEntityData.<Boolean>defineId(EntityCrawler.class, EntityDataSerializers.BOOLEAN);
	private static final EntityDataAccessor<Byte> MATING = SynchedEntityData.defineId(EntityCrawler.class, EntityDataSerializers.BYTE);
	private static final EntityDataAccessor<Boolean> BUILDING = SynchedEntityData.defineId(EntityCrawler.class, EntityDataSerializers.BOOLEAN);

	private static final UUID BABY_SPEED_BOOST_ID = UUID.fromString("B9766B59-9566-4402-BC1F-2EE2A276D836");
	private static final AttributeModifier BABY_SPEED_BOOST = new AttributeModifier(BABY_SPEED_BOOST_ID, "Baby speed boost", 0.25D, Operation.MULTIPLY_BASE);
	private static final UUID BABY_ATTACK_DROP_ID = UUID.fromString("B9766B59-9566-4402-BC1F-3AA7B29648C6");
	private static final AttributeModifier BABY_ATTACK_DROP = new AttributeModifier(BABY_ATTACK_DROP_ID, "Baby attack drop", -1.5D, Operation.ADDITION);

	private float hidingTime;

	public float offsetY = 0F;

	public EntityCrawler(Level w)
	{
		super(FPEntitys.CRAWLER, w);
	}

	public EntityCrawler(EntityType<EntityCrawler> type, Level w)
	{
		super(type, w);
		hidingTime = 40F;
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();
		this.goalSelector.addGoal(5, new EntityAIHide(this, 20*60*5));
		this.goalSelector.addGoal(6, new AIBuildNest(this, TerrainBlocks.crawler_hive.get().defaultBlockState(), 20*10));
	}

	@Override
	protected void defineSynchedData()
	{
		super.defineSynchedData();
		this.entityData.define(HIDING, false);
		this.getEntityData().define(IS_CHILD, false);
		this.getEntityData().define(MATING, (byte)0);
		this.getEntityData().define(BUILDING, false);
	}

	public static AttributeSupplier.Builder registerAttributes()
	{
		return Spider.createAttributes().add(Attributes.MAX_HEALTH, 22.0D);
	}

	@Override
	public void setHiding(boolean hiding)
	{
		this.entityData.set(HIDING, hiding);
		this.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN,(int) hidingTime));
	}

	@Override
	public boolean isHiding()
	{
		return this.entityData.get(HIDING);
	}

	@Override
	public boolean isBaby()
	{
		return this.entityData.get(IS_CHILD);
	}

	public void setBaby(boolean isChild)
	{
		this.getEntityData().set(IS_CHILD, Boolean.valueOf(isChild));

		if(this.level != null && !this.level.isClientSide)
		{
			AttributeInstance iattributeinstance = this.getAttribute(Attributes.MOVEMENT_SPEED);
			AttributeInstance attak = this.getAttribute(Attributes.ATTACK_DAMAGE);
			iattributeinstance.removeModifier(BABY_SPEED_BOOST);
			attak.removeModifier(BABY_ATTACK_DROP);
			this.getAttribute(Attributes.MAX_HEALTH).setBaseValue(22.0D);

			if (isChild)
			{
				iattributeinstance.addTransientModifier(BABY_SPEED_BOOST);
				attak.addTransientModifier(BABY_ATTACK_DROP);
				this.getAttribute(Attributes.MAX_HEALTH).setBaseValue(5.0D);
			}
		}
	}

	public boolean canMate()
	{
		return !isBaby() && entityData.get(MATING) < 100;
	}

	public boolean canBuildNest()
	{
		return !isBaby() && this.entityData.get(MATING) >= 2;
	}

	private void addMate()
	{
		byte b = (byte)(this.entityData.get(MATING)+1);
		this.entityData.set(MATING, b);
		this.addEffect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, 20 * 60, b));
		this.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 20 * 10, b*2));
	}


	@Override
	public void onSyncedDataUpdated(EntityDataAccessor<?> key)
	{
		if (IS_CHILD.equals(key))
		{
//			this.setChildSize(this.isChild());
		}

		super.onSyncedDataUpdated(key);
	}

	@Override
	public void onHiddenAttack(LivingEntity base)
	{
		base.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 100, 10));
		base.hurt(DamageSource.mobAttack(this), 10F);
		this.setTarget(base);
	}

	@Override
	public void killed(ServerLevel world, LivingEntity entityLivingIn)
	{
		super.killed(world, entityLivingIn);
		this.addEffect(new MobEffectInstance(MobEffects.WEAKNESS,50));
		this.heal(2F);

		if(entityLivingIn.getType() == this.getType())
		{
			EntityCrawler mate = (EntityCrawler) entityLivingIn;
			if(this.canMate() && mate.canMate())
			{
				addMate();
			}
		}
	}

	@Override
	public void aiStep()
	{
		super.aiStep();
		if(level.isClientSide)
		{
			if(isHiding() && offsetY > -this.getBbHeight())
			{
				offsetY -= this.getBbHeight() / hidingTime;
				makeCloud();
			}
			else if(!isHiding() && offsetY<0F)
			{
				offsetY += this.getBbHeight() / hidingTime;
				makeCloud();
			}
			else if(isBuilding())
			{
				makeCloud();
			}
		}
	}

	private void makeCloud()
	{
		int amount = isBuilding() ? 5 : 20;
//		this.worldObj.spawnParticle(ParticleTypes.CLOUD, , 0, 0.02F, 0);
		for(int i=0;i<amount;i++)
		{
			BlockPos pos = new BlockPos(this.getX(), this.getY()-1, this.getZ());
			BlockState state = level.getBlockState(pos);
			if(state.getMaterial()==Material.AIR)
				break;
			float rx = this.random.nextFloat()*this.getBbWidth() - this.getBbWidth()*0.5F;
			float rz = this.random.nextFloat()*this.getBbWidth() - this.getBbWidth()*0.5F;
			float mx = (this.random.nextFloat()-this.random.nextFloat())*0.5F;
			float my = this.random.nextFloat()*0.5F +0.1F;
			float mz = (this.random.nextFloat()-this.random.nextFloat())*0.5F;
			this.level.addParticle(new BlockParticleOption(ParticleTypes.BLOCK , state), this.getX()+rx, this.getY(), this.getZ()+rz, mx, my, mz);
		}
	}

	@Override
	public boolean canHide()
	{
		return !isBaby();
	}

	@Override
	public float getStandingEyeHeight(Pose pose, EntityDimensions size)
	{
		return isBaby() ? 0.2F :  super.getStandingEyeHeight(pose, size);
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);

		if (this.isBaby())
		{
			compound.putBoolean("IsBaby", true);
		}
		if(this.isHiding())
		{
			compound.putBoolean("hiding", true);
		}
		if(this.isBuilding())
		{
			compound.putBoolean("building", true);
		}
		compound.putByte("mating", this.entityData.get(MATING));
	}

	@Override
	public void readAdditionalSaveData(CompoundTag compound)
	{
		super.readAdditionalSaveData(compound);

		if (compound.getBoolean("IsBaby"))
		{
			this.setBaby(true);
		}
		if(compound.getBoolean("hiding"))
		{
			setHiding(true);
		}
		if(compound.getBoolean("building"))
		{
			setBuilding(true);
		}

		entityData.set(MATING, compound.getByte("mating"));
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason, @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag)
	{
		if(random.nextInt(50)==0)
		{
			setBaby(true);
		}
		return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
	}

	static class AISpiderAttack extends MeleeAttackGoal
	{
		public AISpiderAttack(Spider spider)
		{
			super(spider, 1.0D, true);
		}



		@Override
		public boolean canContinueToUse()
		{
			float f = this.mob.getBrightness();

			if (f >= 0.5F && this.mob.getRandom().nextInt(100) == 0)
			{
				this.mob.setTarget((LivingEntity)null);
				return false;
			}
			else
			{
				return super.canContinueToUse();
			}
		}

		@Override
		protected double getAttackReachSqr(LivingEntity attackTarget)
		{
			return (this.mob.getBbWidth()*this.mob.getBbWidth() + attackTarget.getBbWidth()) * (this.mob.isBaby()?0.3 : 1);
		}
	}

	public void onBuildNest()
	{
		byte b = (byte) (this.entityData.get(MATING)-2);
		this.entityData.set(MATING, b);
	}

	public void setBuilding(boolean b)
	{
		this.entityData.set(BUILDING, b);
	}

	public boolean isBuilding()
	{
		return this.entityData.get(BUILDING);
	}

	@Override
	protected boolean shouldDropExperience()
	{
		return true;
	}

}
