package futurepack.common.entity.drones;

import java.util.ArrayList;

import net.minecraft.core.BlockPos;

public abstract class ZoneBase 
{
	BlockPos min;
	BlockPos max;
	
	/**
	 * List of Tasks generated for handling the zone
	 */
	ArrayList<TaskBase> tasks;
	
	/**
	 * Checks if the zone have changed and the tasks have to be regenerated
	 * @return
	 */
	abstract boolean isDirty();
	
	/**
	 * Checks if the zone should be used
	 * @return
	 */
	abstract boolean isEnabled();
	
	/**
	 * Fill up the tasks List with new Tasks
	 * Important: always use clearTasks 
	 */
	abstract void generateTasks();
	
	/**
	 * Deactivates the zone and cancels all work in it
	 */
	void clearTasks()
	{
		tasks.clear();
	}
	
}
