package futurepack.common.entity.ai;

import java.util.EnumSet;

import futurepack.common.entity.living.EntityHeuler;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.phys.Vec3;

public class AIRestOnWater extends Goal
{
	private final Animal parentEntity;
	private int restTime = 0;
	private int childs = 0;
	
	public AIRestOnWater(Animal liv)
	{
		parentEntity = liv;
		this.setFlags(EnumSet.of(Goal.Flag.MOVE));
	}

	@Override
	public boolean canUse()
	{
		if(!parentEntity.level.isDay())
			return false;
		
		return parentEntity.isInWater();
	}
	
	@Override
	public void start()
	{
		super.start();
		restTime = (int) (20F * 60F * (1F+parentEntity.getRandom().nextFloat()));
		if(parentEntity.isInLove())
			childs = 1 + getInLove()/300;
	}
	
	private static final Vec3 up = new Vec3(0, 0.5, 0);
	
	@Override
	public void tick()
	{
		super.tick();
		parentEntity.setAirSupply(100);
		
		if(parentEntity.invulnerableTime>0)
		{
			parentEntity.setDeltaMovement(up);
			restTime = 0;
		}
		else if(parentEntity.hurtTime>0)
		{
			restTime = 0;
			parentEntity.setDeltaMovement(up);
		}
		
		if(childs>0 && getMoonPhase()/2 == 0)
		{
			if(parentEntity.isInWater())
			{
				if(parentEntity.hurt(DamageSource.GENERIC, 1))
				{
					childs++;
				}
				if(parentEntity.isAlive()==false || parentEntity.getHealth() <= 0F)
				{
					for(int i=0;i<childs;i++)
					{
						EntityHeuler heuler= new EntityHeuler(parentEntity.level);
						heuler.absMoveTo(parentEntity.getX(), parentEntity.getY(), parentEntity.getZ(), parentEntity.getYRot(), parentEntity.getXRot());
						parentEntity.level.addFreshEntity(heuler);
					}
				}
			}
			
		}
	}
	
	private int getMoonPhase()
	{
		return parentEntity.level.dimensionType().moonPhase(parentEntity.level.getGameTime());
	}
	
	private int getInLove()
	{
		CompoundTag tag = new CompoundTag();
		parentEntity.addAdditionalSaveData(tag);
		
		return tag.getInt("InLove");
	}
	
	@Override
	public boolean canContinueToUse()
	{
		return parentEntity.level.isDay() && --restTime > 0;
	}
	
	@Override
	public void stop()
	{
		super.stop();
		restTime = 0;
		childs = 0;
	}
}
