package futurepack.common.entity.ai.hivemind;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;

import futurepack.common.entity.living.EntityDungeonSpider;
import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.util.INBTSerializable;

public abstract class HVTask implements INBTSerializable<CompoundTag>
{	
	protected boolean isDone = false;
	
	public HVTask()
	{
		
	}
	
	public HVTask(CompoundTag nbt)
	{
		deserializeNBT(nbt);
	}
	
	public abstract BlockPos getSpiderPosition();

	/**
	 * @param spider the Entity executing this
	 * @return if this Entity is finished with the task
	 */
	public abstract boolean execute(EntityDungeonSpider spider);
	
	/**
	 * @return if this Task is finished
	 */
	public boolean isFinished(Level w)
	{
		return isDone;
	}
	
	public Iterable<HVTask> getFollowUpTasks(Level w)
	{
		return Collections.emptyList();
	}
	
	public boolean canExecute(EntityDungeonSpider spider, AssignedTask assignedTask)
	{
		return true;
	}
	
	//only 4 entries so we can use brute forcing
	private static Map<Class<? extends HVTask>, String> class2IdMap = new Object2ObjectArrayMap<Class<? extends HVTask>, String>(4);
	private static Map<String, Function<CompoundTag, HVTask>> constructorMap = new Object2ObjectArrayMap<String, Function<CompoundTag,HVTask>>(4);

	static
	{
		register(BreakBlockTask.class, BreakBlockTask::new, "break_block");
		register(GoToTask.class, GoToTask::new, "go_to");
		register(PickUpItemsTask.class, PickUpItemsTask::new, "pickup_items");
		register(PutItemIntoTask.class, PutItemIntoTask::new, "put_item_into");
	}
	
	public static <T extends HVTask> void register(Class<T> cls, Function<CompoundTag, T> func, String name)
	{
		if(constructorMap.containsKey(name))
			throw new IllegalArgumentException("This name " + name + " is already registered");
		
		class2IdMap.put(cls, name);
		constructorMap.put(name, (Function<CompoundTag, HVTask>) func);
	}
	
	public static HVTask load(CompoundTag compound) 
	{
		String s = compound.getString("taskID");
		Function<CompoundTag, HVTask> func = constructorMap.get(s);
		if(func!=null)
		{
			return func.apply(compound);
		}
		else
		{
			throw new IllegalArgumentException("Unknown Task id " + s);
		}
	}

	public static CompoundTag save(HVTask task) 
	{
		CompoundTag nbt = task.serializeNBT();
		nbt.putString("taskID", class2IdMap.getOrDefault(task.getClass(), task.getClass().getName()));
		return nbt;
	}
}
