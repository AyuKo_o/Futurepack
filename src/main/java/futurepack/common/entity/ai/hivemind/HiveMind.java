package futurepack.common.entity.ai.hivemind;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.Tag;
import net.minecraft.server.level.ServerLevel;
import net.minecraftforge.common.util.INBTSerializable;

public class HiveMind implements INBTSerializable<CompoundTag>
{
	private final ServerLevel world;
	private BlockPos pos;
	
	private List<AssignedTask> taskList = new ArrayList<AssignedTask>();
	
	public HiveMind(ServerLevel world, BlockPos pos) 
	{
		super();
		this.world = world;
		this.pos = pos;
	}
	
	public HiveMind(ServerLevel world, CompoundTag nbt) 
	{
		this.world = world;
		deserializeNBT(nbt);
	}

	@Override
	public CompoundTag serializeNBT() 
	{
		CompoundTag nbt = new CompoundTag();
		HelperSerialisation.putBlockPos(nbt, "pos", pos);
		ListTag list = new ListTag();
		for(AssignedTask t : taskList)
		{
			list.add(t.serializeNBT());
		}
		nbt.put("tasks", list);
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt) 
	{
		pos = HelperSerialisation.getBlockPos(nbt, "pos");
		ListTag list = nbt.getList("tasks", nbt.getId());
		taskList.clear();
		for(Tag tag : list)
		{
			taskList.add(new AssignedTask((HVTask) tag));
		}
	}
	
	public BlockPos getPosition()
	{
		return pos;
	}
	
	public void updateTaskList()
	{
		List<AssignedTask> newTasks = taskList.stream()
				.filter(t -> t.isFinished(world))
				.map(AssignedTask::getTask)
				.map(t -> t.getFollowUpTasks(world))
				.flatMap(t -> StreamSupport.stream(Spliterators.spliteratorUnknownSize(t.iterator(), Spliterator.ORDERED), false))
				.map(AssignedTask::new)
				.collect(Collectors.toList());
		taskList.removeIf(t -> t.isFinished(world));
		taskList.addAll(newTasks);
	}
	
	public HVTask getTaskFor(EntityDungeonSpider spider)
	{
		updateTaskList();
		Optional<TaskWithDistance> opt = taskList.stream()
				.filter(t -> t.canExecute(spider)).map(t -> new TaskWithDistance(t, spider.position())).sorted((a,b) -> a.distance == b.distance ? 0 :  (a.distance > b.distance ? 1 : -1)).findFirst();
		if(opt.isPresent())
		{
			return opt.get().task.assign(spider);
		}
		if(!spider.getMainHandItem().isEmpty())
		{
			AssignedTask task = new AssignedTask(new PutItemIntoTask(pos.above(), Direction.DOWN));
			taskList.add(task);
			return task.assign(spider);
		}
		
		if(world.random.nextInt(100) == 0)
		{
			BlockPos pos = this.pos.offset(world.random.nextInt(10)-5, 0, world.random.nextInt(10)-5);
			while(world.isEmptyBlock(pos))
			{
				pos = pos.below();
			}
			while(!world.isEmptyBlock(pos))
			{
				pos = pos.above();
			}
			
			AssignedTask task = new AssignedTask(new BreakBlockTask(pos.below(), pos));
			taskList.add(task);
			return task.assign(spider);
		}
		
		return null;
	}
}