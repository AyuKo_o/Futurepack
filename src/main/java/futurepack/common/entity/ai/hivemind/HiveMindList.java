//package futurepack.common.entity.ai.hivemind;
//
//import java.util.Map;
//import java.util.Map.Entry;
//
//import futurepack.common.entity.living.EntityDungeonSpider;
//import it.unimi.dsi.fastutil.objects.Object2ObjectRBTreeMap;
//import net.minecraft.core.BlockPos;
//import net.minecraft.nbt.CompoundTag;
//import net.minecraft.nbt.ListTag;
//import net.minecraft.nbt.Tag;
//import net.minecraft.server.level.ServerLevel;
//import net.minecraft.world.level.saveddata.SavedData;
//
//public class HiveMindList extends SavedData
//{
//	private static final String name = "futurepack_hive_mind";
//	
//	private final ServerLevel world;
//	private Map<BlockPos, HiveMind> pos2mind;
//	
//	public HiveMindList(ServerLevel world) 
//	{
//		super(name);
//		this.world = world;
//		pos2mind = new Object2ObjectRBTreeMap<BlockPos, HiveMind>();
//	}
//
//	public static HiveMindList getHiveMindsIn(ServerLevel world)
//	{
//		return world.getDataStorage().computeIfAbsent(() -> new HiveMindList(world), name);
//	}
//
//	@Override
//	public void load(CompoundTag nbt) 
//	{
//		ListTag list = nbt.getList("minds", nbt.getId());
//		for(Tag tag : list)
//		{
//			HiveMind mind = new HiveMind(world, (CompoundTag) tag);
//			pos2mind.put(mind.getPosition(), mind);
//		}
//	}
//
//	@Override
//	public CompoundTag save(CompoundTag compound) 
//	{
//		ListTag list = new ListTag();
//		for(Entry<BlockPos, HiveMind> e : pos2mind.entrySet())
//		{
//			list.add(e.getValue().serializeNBT());
//		}
//		compound.put("minds", list);
//		
//		return compound;
//	}
//	
//	public HiveMind getHiveMindFor(EntityDungeonSpider spider)
//	{
//		BlockPos pos = spider.getRestrictCenter();
//		return pos2mind.computeIfAbsent(pos, p -> new HiveMind(world, p));
//	}
//}
