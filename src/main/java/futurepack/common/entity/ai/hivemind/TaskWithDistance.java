package futurepack.common.entity.ai.hivemind;

import net.minecraft.core.Position;

public class TaskWithDistance
{
	protected final AssignedTask task;
	protected final double distance;
	
	public TaskWithDistance(AssignedTask t, double Vector3d) 
	{
		super();
		this.task = t;
		this.distance = Vector3d;
	}
	
	public TaskWithDistance(AssignedTask task, Position pos) 
	{
		this(task, task.getTask().getSpiderPosition().distToCenterSqr(pos));
	}
}