package futurepack.common.entity.ai.hivemind;

import java.util.Arrays;

import futurepack.common.entity.living.EntityDungeonSpider;
import futurepack.depend.api.helper.HelperSerialisation;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.level.Level;

public class BreakBlockTask extends HVTask 
{
	protected BlockPos blockToBreak;
	protected BlockPos spiderPos;
	
	
	public BreakBlockTask(BlockPos blockToBreak, BlockPos sPiderPos)
	{
		super();
		this.blockToBreak = blockToBreak;
		this.spiderPos = sPiderPos;
	}
	
	public BreakBlockTask(CompoundTag nbt)
	{
		super(nbt);
	}

	@Override
	public CompoundTag serializeNBT() 
	{
		CompoundTag nbt = new CompoundTag();
		HelperSerialisation.putBlockPos(nbt, "break", blockToBreak);
		HelperSerialisation.putBlockPos(nbt, "spider", spiderPos);
		return nbt;
	}

	@Override
	public void deserializeNBT(CompoundTag nbt) 
	{
		blockToBreak = HelperSerialisation.getBlockPos(nbt, "break");
		spiderPos = HelperSerialisation.getBlockPos(nbt, "spider");
	}

	@Override
	public BlockPos getSpiderPosition() 
	{
		return spiderPos;
	}
	
	@Override
	public boolean execute(EntityDungeonSpider spider) 
	{
		spider.getCommandSenderWorld().destroyBlock(blockToBreak, true);
		isDone = true;
		return true;
	}
	
	@Override
	public Iterable<HVTask> getFollowUpTasks(Level w) 
	{
		BlockPos.MutableBlockPos mut = new BlockPos.MutableBlockPos().set(blockToBreak);
		while(mut.getY() > 1 && w.isEmptyBlock(mut))
		{
			mut.move(Direction.DOWN);
		}
		mut.move(Direction.UP);
		
		return Arrays.asList(new PickUpItemsTask(mut.immutable()));
	}

}
