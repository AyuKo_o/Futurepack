package futurepack.common.entity;

import java.util.UUID;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.level.Level;

public abstract class EntityMovingShipBase extends Entity
{

	public EntityMovingShipBase(EntityType<?> entityTypeIn, Level worldIn) 
	{
		super(entityTypeIn, worldIn);
	}
	public abstract  void setShipID(UUID shipID);
	
	public abstract void setDestination(BlockPos endCoords);
	
	public abstract void addChair(LivingEntity liv);
	
	public abstract UUID getShipID();

}
