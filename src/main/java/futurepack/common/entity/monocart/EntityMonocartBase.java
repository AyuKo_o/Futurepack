package futurepack.common.entity.monocart;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockMonocartWaypoint;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.common.FPBlockSelector;
import futurepack.common.block.logistic.monorail.BlockMonorailBasic;
import futurepack.common.block.logistic.monorail.BlockMonorailBasic.EnumMonorailStates;
import futurepack.common.entity.EntityNeonPowered;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.VoxelShape;

public class EntityMonocartBase extends EntityNeonPowered
{
	protected static final EntityDataAccessor<Integer> state = SynchedEntityData.defineId(EntityMonocartBase.class, EntityDataSerializers.INT);
	private static final EntityDataAccessor<BlockPos> navi = SynchedEntityData.defineId(EntityMonocartBase.class, EntityDataSerializers.BLOCK_POS);
	
	
	private FPBlockSelector selector;
	private CardNavigator localNavigator;
	
	public static final byte isRolling = 1;
	public static final byte isPaused = 2;
	public static final byte isHighSpeed = 4;
	
	private BlockPos[] waypoints;
	private int pos;
	
	private static IBlockSelector selRail =  new IBlockSelector() 
	{	
		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{			
			BlockState state = w.getBlockState(pos);
			if(BlockMonorailBasic.isMonorail(state))
			{		
				BlockState parent_state = w.getBlockState(parent);
				
				boolean canPass = ((BlockMonorailBasic)state.getBlock()).canMonocartPass(w, pos, state, parent, parent_state);//Removed because of oneway rail bug. Seems not to have any function && ((BlockMonorailBasic)pState.getBlock()).canMonocartPass(w, parent, pState, pos, state);
				
				if(diagonal)
				{		
					EnumMonorailStates ps = parent_state.getValue(BlockMonorailBasic.getPropertie((BlockMonorailBasic) parent_state.getBlock()));
					EnumMonorailStates en = state.getValue(BlockMonorailBasic.getPropertie((BlockMonorailBasic) state.getBlock()));
					if(!ps.isYLoocked())
					{	
						if(parent.above().west().equals(pos))
						{
							return ps == EnumMonorailStates.ASCENDING_WEST  && canPass;
						}
						if(parent.above().east().equals(pos))
						{
							return ps == EnumMonorailStates.ASCENDING_EAST  && canPass;
						}
						if(parent.above().north().equals(pos))
						{
							return ps == EnumMonorailStates.ASCENDING_NORTH && canPass;
						}
						if(parent.above().south().equals(pos))
						{
							return ps == EnumMonorailStates.ASCENDING_SOUTH && canPass;
						}
					}
					if(!en.isYLoocked())
					{
						if(parent.below().west().equals(pos))
						{
							return en == EnumMonorailStates.ASCENDING_EAST  && canPass;
						}
						if(parent.below().east().equals(pos))
						{
							return en == EnumMonorailStates.ASCENDING_WEST  && canPass;
						}
						if(parent.below().north().equals(pos))
						{
							return en == EnumMonorailStates.ASCENDING_SOUTH && canPass;
						}
						if(parent.below().south().equals(pos))
						{
							return en == EnumMonorailStates.ASCENDING_NORTH && canPass;
						}
					}
				}
				else
				{
					return canPass;
				}					
			}
			return false;
		}

		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean dia, ParentCoords parent)
		{
			return true;
		}
	};
	
	private static IBlockValidator selWaypoints = new IBlockValidator()
	{	
		@Override
		public boolean isValidBlock(Level w, ParentCoords pos)
		{
			return w.getBlockState(pos).getBlock() instanceof IBlockMonocartWaypoint;
		}
	};
	
	
	private BlockPos currentRail = null;
	
	public EntityMonocartBase(EntityType<? extends EntityMonocartBase> type, Level par1World) 
	{
		super(type, par1World, 100F);
		selector = new FPBlockSelector(par1World,selRail);
		blocksBuilding=true;
		localNavigator = new CardNavigator(this);
		consumePowerForTick = false;
	}

	@Override
	protected MovementEmission getMovementEmission()
	{
		return MovementEmission.NONE;
	}
	
//	@Override
//	public AxisAlignedBB getCollisionBox(Entity e) 
//	{
//		return e.getBoundingBox();
//	}
//	
//	@Override
//	public AxisAlignedBB getCollisionBoundingBox()
//	{
//		return getBoundingBox();
//	}
	
	@Override
	public boolean isPushable()
    {
        return false;
    }
	
	@Override
	public boolean isPickable() 
	{
		return !isAlive()==false;
	}
	
	@Override
	protected void defineSynchedData() 
	{
		super.defineSynchedData();
		this.entityData.define(state, 0);
		this.entityData.define(navi, new BlockPos(0,-1,0));
	}

	@Override
	protected void readAdditionalSaveData(CompoundTag nbt)
	{	
		super.readAdditionalSaveData(nbt);
		this.entityData.set(state, nbt.getInt("state"));
		setWaypoints(nbt.getIntArray("path"));
	}

	@Override
	protected void addAdditionalSaveData(CompoundTag nbt)
	{
		super.addAdditionalSaveData(nbt);
		nbt.putInt("state", this.entityData.get(state));
		nbt.putIntArray("path", getWaypointsAsInt());
	}
	
	@Override
	public boolean hurt(DamageSource damageSource, float damage) 
	{
		if(!this.level.isClientSide && damage > 0)
		{
			this.discard();
		}
		return super.hurt(damageSource, damage);
	}
	
	@Override
	public void tick() 
	{
		super.tick();
		if(!level.isClientSide)
		{
			setRolling(!localNavigator.pathless());
		}
		
		if(!isPaused())
		{
			localNavigator.doTasks();
		}
		if(!onGround && !isOnMonorail())
		{
			if(!level.isClientSide)
				localNavigator.deletePath();
			
			double y = this.getDeltaMovement().y;
			y-= 0.05;
			if(y<0.1)
			{
				y = -0.1;
			}
			setDeltaMovement(this.getDeltaMovement().x, y, this.getDeltaMovement().z);
		}
		noPhysics=isOnMonorail();
		if(isPaused() || !consumePower())
		{		
			noPhysics=false;
			this.setDeltaMovement(0, getDeltaMovement().y, 0);
		}
		move(MoverType.SELF, this.getDeltaMovement());
		
		if(!level.isClientSide)
		{
			notifyRail();
		}
		
		if(!isPaused())
		{		
			if(isOnMonorail())
			{
				updatePath();
				setDeltaMovement(this.getDeltaMovement().scale(0.999));
			}
			else
			{
				setDeltaMovement(this.getDeltaMovement().scale(0.5));
			}
		}
		else
		{
			setDeltaMovement(Vec3.ZERO);
		}
	}
	
	@Override
	public void baseTick()
	{
		super.baseTick();
		AABB box = this.getBoundingBox().inflate(1, 1, 1);
		List<EntityMonocartBase> list = level.getEntitiesOfClass(EntityMonocartBase.class, box, new Predicate<EntityMonocartBase>()
		{
			@Override
			public boolean apply(EntityMonocartBase input)
			{
				return input != EntityMonocartBase.this && !input.isPaused();
			}
		});
		if(!list.isEmpty())
		{
			this.setPaused(true);
		}
	}
	
	private void updatePath()
	{
		if(level.isClientSide)
		{
			if(!isRolling() && !localNavigator.pathless())
			{
				localNavigator.deletePath();
			}			
			if(!localNavigator.pathless())
			{
				BlockPos p1 = new BlockPos(localNavigator.getPathEnd());
				BlockPos p2 = this.getPath();
				if(!p2.equals(p1))
				{
					localNavigator.deletePath();
				}
			}			
			if(localNavigator.pathless())
			{		
				selector.clear();
				selector.selectBlocks(currentRail);
				Collection<ParentCoords> list = selector.getValidBlocks(selWaypoints);
			
				if(list.size()>0)
				{
					BlockPos pos = getPath();
					if(pos!=null)
					{
						for(ParentCoords c : list)
						{
							if(c.equals(pos))
							{
								localNavigator.initPath(c);
								break;
							}
						}
					}				
				}
			}
		}
		else if(localNavigator.pathless())
		{
			selector.clear();
			selector.selectBlocks(currentRail);
			Collection<ParentCoords> list = selector.getValidBlocks(selWaypoints);
		
			if(list.size()>0)
			{
				if(waypoints!=null && waypoints.length>0)
				{
					pos++;
					pos %= waypoints.length;
					BlockPos pos = waypoints[this.pos];
					
					for(ParentCoords c : list)
					{
						if(c.equals(pos))
						{
							setPath(pos);
							localNavigator.initPath(c);
							break;
						}
					}
				}
					
//				int h = worldObj.rand.nextInt(list.size()); 
//				ParentCoords pos = (ParentCoords) list.toArray()[h];
//				setPath(pos);
//				localNavigator.initPath(pos);				
			}
			selector.clear();
		}
	}
	
	
	public boolean isOnMonorail()
	{
		currentRail = this.blockPosition();
		BlockState b = level.getBlockState(currentRail);
		if(BlockMonorailBasic.isMonorail(b))
		{
			return true;
		}
		currentRail = currentRail.below();
		b = level.getBlockState(currentRail);
		if(BlockMonorailBasic.isMonorail(b))
		{
			return !b.getValue(BlockMonorailBasic.getPropertie((BlockMonorailBasic) b.getBlock())).isYLoocked();
		}
		currentRail=null;
		return false;
	}
	
	private void notifyRail()
	{
		if(currentRail!=null)
		{
			BlockState b = level.getBlockState(currentRail);
			if(BlockMonorailBasic.isMonorail(b))
			{
				((BlockMonorailBasic)b.getBlock()).onMonocartPasses(level, currentRail, b, this);
			}
		}
	}	
	
	protected static int setByte(int base, int bit, boolean state)
	{
		return state? base | bit : base & ~bit;
	}
	
	protected static boolean getBool(int base, int bit)
	{
		return (base & bit) == bit;
	}
	
	public boolean isRolling()
	{
		return getBool(this.entityData.get(state), isRolling);
	}	
	private void setRolling(boolean b)
	{
		int by = this.entityData.get(state);
		this.entityData.set(state, setByte(by, isRolling, b));
	}
	
	public boolean isPaused()
	{
		return getBool(this.entityData.get(state), isPaused);
	}
	public void setPaused(boolean b)
	{
		int by = this.entityData.get(state);
		this.entityData.set(state, setByte(by, isPaused, b));
	}
	
	public boolean isHighSpeed()
	{
		return getBool(this.entityData.get(state), isHighSpeed);
	}
	public void setHighSpeed(boolean b)
	{
		int by = this.entityData.get(state);
		this.entityData.set(state, setByte(by, isHighSpeed, b));
	}
	
	protected BlockPos getPath()
	{
		BlockPos pso = this.entityData.get(navi);
		return pso.getY()==-1?null:pso;
	}
	
	protected void setPath(BlockPos sh)
	{
		this.entityData.set(navi, sh);
	}
	
	public float getSpeed()
	{
		return isHighSpeed() ? 0.5F : 0.2F;
	}
	
	@Override
	protected float getEnergieUse()
	{
		return isHighSpeed() ? 0.25F : 0.1F;
	}
	
	public Collection<ParentCoords> getAllWaypoints()
	{
		if(currentRail==null)
			return Collections.emptyList();
		
		selector.clear();
		selector.selectBlocks(currentRail);
		Collection<ParentCoords> list = selector.getValidBlocks(selWaypoints);
		return list;
	}
	
	public void setWaypoints(BlockPos[] path)
	{
		this.waypoints = path;
		this.pos = 0;
	}
	
	public void setWaypoints(int[] ints)
	{
		if(ints.length>0)
		{
			BlockPos[] pos = new BlockPos[ints.length / 3];
			for(int i=0;i<pos.length;i++)
			{
				pos[i] = new BlockPos(ints[i*3], ints[i*3+1], ints[i*3+2]);
			}
			setWaypoints(pos);
		}
	}
	
	private int[] getWaypointsAsInt()
	{
		if(waypoints==null)
			return new int[0];
		
		int[] ints = new int[waypoints.length*3];
		for(int i=0;i<waypoints.length;i++)
		{
			BlockPos point = waypoints[i];
			ints[i*3] = point.getX();
			ints[i*3+1] = point.getY();
			ints[i*3+2] = point.getZ();
		}
		
		return ints;
	}
	
	@Nullable
	public BlockPos[] getWaypoint()
	{
		return this.waypoints;
	}

	public boolean canPass(Vec3 next)
	{
		BlockPos pos = new BlockPos(next);
		if(level.isEmptyBlock(pos))
			pos = pos.below();
		
		BlockState state = level.getBlockState(pos);
		
		if(!BlockMonorailBasic.isMonorail(state))
		{
			Stream<VoxelShape> shapes = StreamSupport.stream(level.getBlockCollisions(this, new AABB(pos)).spliterator(), false);//getCollisionShapes
			boolean empty = shapes.filter(v -> !v.isEmpty()).count() <= 0;
			if(empty)
			{
				pos = pos.below();
				state = level.getBlockState(pos);
				if(!BlockMonorailBasic.isMonorail(state))
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
			
		
		if(currentRail==null)
			if(!isOnMonorail())
				return false;
		
		BlockState pState = level.getBlockState(currentRail);
		
		return ((BlockMonorailBasic)state.getBlock()).canMonocartPass(level, pos, state, currentRail, pState);
	}

	@Override
	protected void tryCharge() 
	{
		
	}
}
