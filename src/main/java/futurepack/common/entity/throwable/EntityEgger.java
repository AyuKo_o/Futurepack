package futurepack.common.entity.throwable;

import futurepack.common.FPEntitys;
import futurepack.common.item.tools.ToolItems;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.boss.EnderDragonPart;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.projectile.ItemSupplier;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.EntityHitResult;
import net.minecraft.world.phys.HitResult;
import net.minecraft.world.phys.HitResult.Type;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.network.NetworkHooks;
@OnlyIn(
	value = Dist.CLIENT,
	_interface = ItemSupplier.class
)
public class EntityEgger extends ThrowableProjectile implements ItemSupplier
{

	public EntityEgger(Level w, double x, double y, double z) 
	{
		super(FPEntitys.ENTITY_EGGER, x, y, z, w);
	}

	public EntityEgger(Level w, LivingEntity p_i1777_2_)
	{
		super(FPEntitys.ENTITY_EGGER, p_i1777_2_, w);
	}

	public EntityEgger(EntityType<EntityEgger> type, Level w) 
	{
		super(type, w);
	}

	@Override
	protected void onHit(HitResult pos) 
	{		
		if(level.isClientSide)
			return;
		
		if(pos.getType()==Type.ENTITY)
		{
			Entity hit = ((EntityHitResult)pos).getEntity();
			
			if(hit == getOwner())
				return;
			
			if(hit instanceof EnderDragonPart)
			{
				EnderDragonPart p = (EnderDragonPart) hit;
				if(p.parentMob!=null)
				{
					hit = p.parentMob;
				}
			}
			EntityType id = hit.getType();
			if(id == null || id == EntityType.PLAYER)
			{
				ItemStack it = new ItemStack(ToolItems.entity_egger);
				ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), it);
				level.addFreshEntity(item);
				discard();
				return;
			}
			
			ItemStack it = new ItemStack(ToolItems.entity_egger_full);
			it.setTag(new CompoundTag());
			hit.saveAsPassenger(it.getTag());
			hit.discard();	
			it.setHoverName(hit.getName());
			ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), it);
			level.addFreshEntity(item);
			discard();
		}
		else
		{
			ItemStack it = new ItemStack(ToolItems.entity_egger);
			ItemEntity item = new ItemEntity(level, getX(), getY(), getZ(), it);
			level.addFreshEntity(item);
			discard();
		}
	}

	@Override
	protected void defineSynchedData() 
	{
		
	}

	@Override
	public ItemStack getItem() 
	{
		return new ItemStack(ToolItems.entity_egger);
	}

	@Override
	public Packet<?> getAddEntityPacket() 
	{
		return NetworkHooks.getEntitySpawningPacket(this);
	}
}
