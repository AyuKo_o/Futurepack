package futurepack.common.research;

import futurepack.api.interfaces.IScanPart;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class ScanPartHeadline implements IScanPart
{

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		BlockState state = w.getBlockState(pos);
		Item blIt = state.getBlock().asItem();
//		TileEntity tile = w.getTileEntity(pos);
		int redstone = w.getBestNeighborSignal(pos);

		ItemStack stack = new ItemStack(blIt,1);
		
		TranslatableComponent trans = new TranslatableComponent((inGUI ? ChatFormatting.BLUE : ChatFormatting.AQUA) + (blIt==Items.AIR?state.getBlock().getDescriptionId() : stack.getHoverName().getContents()));
		TextComponent hover = new TextComponent(state.toString());
		hover.setStyle(Style.EMPTY.withColor(inGUI ? ChatFormatting.DARK_BLUE : ChatFormatting.DARK_AQUA).withItalic(true));
		trans.append(hover);
		if(redstone>0)
			trans.append(new TextComponent( (inGUI ? ChatFormatting.DARK_RED : ChatFormatting.RED) + " Redstone-Level:" + redstone));
		return trans;
	}

	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI)
	{
		ResourceLocation name = null;
//TODO test if this is displayed correct		if(EntityList.CLASS_TO_NAME.containsKey(e.getClass()))	
//		{
//			name = EntityList.CLASS_TO_NAME.get(e.getClass()).toString();
//			id = EntityList.getEntityID(e);
//		}
//		else
		{
			EntityType<?> type = e.getType();
			if(type != null)
			{
				name = HelperItems.getRegistryName(type);
			}
		}
		String type = e.getMobType() + "";	
		TranslatableComponent trans = new TranslatableComponent((inGUI ? ChatFormatting.BLUE : ChatFormatting.AQUA) + name.toString());
		return trans;
	}

}
