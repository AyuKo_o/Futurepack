package futurepack.common.research;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Nullable;

import com.mojang.authlib.GameProfile;

import futurepack.api.event.ResearchFinishEvent;
import futurepack.client.research.LocalPlayerResearchHelper;
import futurepack.common.FPConfig;
import futurepack.common.FPLog;
import futurepack.common.block.inventory.TileEntityForscher;
import futurepack.common.player.FakePlayerFactory;
import futurepack.depend.api.helper.HelperChunks;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementList;
import net.minecraft.advancements.AdvancementProgress;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.Criterion;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientAdvancements;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.PlayerAdvancements;
import net.minecraft.server.ServerAdvancementManager;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;

public class CustomPlayerData 
{
	public static final String savename = "FP-RESEARCH";
	
	public static CustomPlayerData getDataFromPlayer(Player pl)
	{
		if(pl.level.isClientSide)
		{
			return createForLocalPlayer();
		}
		ServerLevel w = (ServerLevel) pl.level;
		return getDataFromProfile(pl.getGameProfile(), w.getServer());
	}
	
	public static CustomPlayerData getDataFromUUID(UUID uuid, MinecraftServer server)
	{
		return new CustomPlayerData(PlayerDataLoader.getResearches(uuid),uuid, server);
//		return getDataFromProfile(server.getPlayerProfileCache().get(uuid));
	}
	
	public static CustomPlayerData getDataFromProfile(GameProfile f, MinecraftServer server)
	{
		if(f==null)
			return null;
		
		return getDataFromUUID(f.getId(), server);
		
//		return new CustomPlayerData(PlayerDataLoader.getResearches(f),f);
	}
	
	public static CustomPlayerData createForLocalPlayer()
	{
		LocalPlayer sp = Minecraft.getInstance().player;
		if(sp==null)
		{
			FPLog.logger.warn("Try to get Player Data, but Singleplayer is null");
			return null;
		}
		CustomPlayerData data = new CustomPlayerData(LocalPlayerResearchHelper.researches, sp.getGameProfile().getId(), null);
		data.setupClient();
		return data;
	}
	
	private final Collection<String> researches;
	public final UUID player;
	@Nullable
	private MinecraftServer server;
	private IAdvancementManager advancements;
	
//	public static CustomPlayerData getDataFromPlayer(EntityPlayer pl)
//	{
//		
//		CustomPlayerData data = (CustomPlayerData) pl.getExtendedProperties(savename);
//		if(data==null)
//		{
//			data = new CustomPlayerData();
//			pl.registerExtendedProperties(savename, data);
//		}
//		return data;
//	}	
	
	private CustomPlayerData(Collection<String> r, UUID gp, MinecraftServer server)
	{
		if(r==null) //THis prevents a crash if the list is null
			r=new ArrayList<String>();
		researches = r;
		player = gp;
		this.server = server;
	}
//	
//	@Override
//	public void saveNBTData(NBTTagCompound compound)
//	{
//		NBTTagCompound nbt = new NBTTagCompound();
//		NBTTagList l = new NBTTagList();
//		for(String s : reseraches)
//		{
//			NBTTagString ns = new NBTTagString(s);
//			l.add(ns);
//		}		
//		compound.setTag(savename, nbt);
//	}
//
//	@Override
//	public void loadNBTData(NBTTagCompound compound)
//	{
//		NBTTagCompound nbt = compound.getCompoundTag(savename);
//		NBTTagList l = nbt.getTagList("known", NBT.TAG_STRING);
//		for(int i=0;i<l.size();i++)
//		{
//			reseraches.add(l.getStringTagAt(i));
//		}
//	}
//
//	@Override
//	public void init(Entity entity, World world) { }
//

	private String lastResearch = null;
	private boolean lastResult = false;
	
	public boolean hasResearch(String reserach)
	{
		if(FPConfig.RESEARCH.disable_researching.get())
			return true;
		
		if(reserach==lastResearch)
			return lastResult;
		
		return lastResult = researches.contains(lastResearch=reserach);
	}
	public boolean hasResearch(Research reserach)
	{
		return hasResearch(reserach.getName());
	}
	
	public boolean canResearch(String research)
	{
		return canResearch(ResearchManager.getResearch(research));
	}
	
	public boolean canResearch(Research research)
	{
		return canResearch(research, false);
	}
	
	public boolean canResearch(Research research, boolean auto)
	{
		Research[] p = research.getParents();
		Advancement[] a = searchAdvancements(research.getGrandparents());
		if(p!=null)
		{
			for(Research needed : p)
			{
				if(!hasResearch(needed))
					return false;
			}
		}
		if(a!=null)
		{
			for(Advancement needed : a)
			{
				IAdvancementManager adv = getAdvancements();
				AdvancementProgress prog = adv.getProgress(needed);
				if(!prog.isDone())
				{
					return false;
				}
			}
		}
		if(auto && a==null && p==null)
		{
			return false;
		}
		return true;
	}
	
	private Advancement[] searchAdvancements(ResourceLocation[] ress)
	{
		ArrayList<Advancement> list = new ArrayList<Advancement>();
		
		if(server!=null)
		{	
			ServerAdvancementManager manager = server.getAdvancements();
			for(ResourceLocation path : ress)
			{
				Advancement adv = manager.getAdvancement(path);
				if(adv!=null)
				{
					list.add(adv);
				}
				else
				{
					FPLog.logger.error("Cant find Advancement %s", adv);
				}
			}
		}
		else
		{
			searchClientSide(list, ress);
		}
		return list.toArray(new Advancement[list.size()]);
	}
	
	private static final HashMap<ResourceLocation,Advancement> notFound = new HashMap<>(); 
	
	private void searchClientSide(ArrayList<Advancement> list, ResourceLocation[] ress)
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				AdvancementList manager = Minecraft.getInstance().player.connection.getAdvancements().getAdvancements();
				for(ResourceLocation path : ress)
				{
					Advancement adv = manager.get(path);
					if(adv!=null)
					{
						list.add(adv);
					}
					else 
					{
						adv = notFound.get(path);
						if(adv!=null)
						{
							list.add(adv);
						}		
						else
						{
							HashMap<String, Criterion> map = new HashMap<>();
							Criterion not = new Criterion();
							map.put("impossible", not);
							adv = new Advancement(path, null, null, AdvancementRewards.EMPTY, map, new String[][]{{"impossible"}});
							list.add(adv);
							notFound.put(path, adv);
							FPLog.logger.error("[Client] Cant find Advancement %s ... adding unobtainalbe client dummy", path);
						}
					}
				}
			}
		});
	}
	
//	private StatisticsManager getStats()
//	{
//		if(stats!=null)
//		{
//			return stats;
//		}
//		
//		
//		if(server != null && server.getPlayerList() != null)
//		{
//			EntityPlayerMP pl = server.getPlayerList().getPlayerByUUID(player);
//			if(pl!=null)
//			{
//				stats =  pl.getStatFile();
//			}
//			else
//			{
//				try
//				{
//					File file1 = new File(server.worldServerForDimension(0).getSaveHandler().getWorldDirectory(), "stats");
//					File file2 = new File(file1, player.toString() + ".json");
//					stats = new StatisticsManagerServer(server, file2);
//					((StatisticsManagerServer)stats).readStatFile();
//				}
//				catch(RuntimeException e)
//				{
//					e.printStackTrace();
//					setupClient();
//				}
//			}
//		}
//		else
//		{
//			setupClient();
//		}
//		
//		return stats;
//	}
	
	public IAdvancementManager getAdvancements()
	{
		if(advancements!=null)
		{
			return advancements;
		}
		else
		{
			ServerPlayer mp = server.getPlayerList().getPlayer(player);
			if(mp!=null)
			{
				PlayerAdvancements pl = mp.getAdvancements();
				advancements = new IAdvancementManager()
				{				
					@Override
					public AdvancementProgress getProgress(Advancement adv)
					{				
						return pl.getOrStartProgress(adv);
					}
				};
				return advancements;
			}
			else
			{
				File dir = HelperChunks.getDimensionDir(server.getLevel(Level.OVERWORLD));
				File file1 = new File(dir, "advancements");
				File file2 = new File(file1, player + ".json");
				PlayerAdvancements pl = new PlayerAdvancements(server.getFixerUpper(), server.getPlayerList(), server.getAdvancements(), file2, FakePlayerFactory.INSTANCE.getPlayer(server.getLevel(Level.OVERWORLD)));
				advancements = new IAdvancementManager()
				{				
					@Override
					public AdvancementProgress getProgress(Advancement adv)
					{				
						return pl.getOrStartProgress(adv);
					}
				};
				return advancements;
			}
		}
	}
	
	private void setupClient()
	{
		DistExecutor.runWhenOn(Dist.CLIENT, () -> new Runnable()
		{
			@Override
			public void run() 
			{
				LocalPlayer sp = Minecraft.getInstance().player;
				final ClientAdvancements manager = sp.connection.getAdvancements();
				advancements = new ClientManager(manager);
			}
		});
	}
	
	public boolean canProduce(ItemStack it)
	{
		Set<Research> set = ResearchLoader.getReqiredResearch(it);
		if (set==null)
			return true;
		if(set.isEmpty())
			return false;
		
		for(Research r : set)
			if(!hasResearch(r))
				return false;
		
		return true;
	}
	
	public boolean addResearch(Research r)
	{
		if(hasResearch(r))
			return true;
		if(!canResearch(r))
		{
			return false;
		}
		lastResearch = null;
		lastResult = false;
		
		researches.add(r.getName());
		FPLog.logger.info(player + " get research "+r.getName());
		Player pl = server.getPlayerList().getPlayer(player);
		MinecraftForge.EVENT_BUS.post(new ResearchFinishEvent.Server(player, r));
		if(pl != null)
		{
			TranslatableComponent t1 = new TranslatableComponent(r.getTranslationKey());
			TranslatableComponent trans = new TranslatableComponent("chat.research.add", t1);
			pl.displayClientMessage(trans, false);
		}
		checkInstandResearches();
		return true;
	}
	
	public boolean addResearchUnsafe(Research r)
	{
		if(hasResearch(r))
			return true;
		
		lastResearch = null;
		lastResult = false;
		
		researches.add(r.getName());
		FPLog.logger.info(player + " get research "+r.getName());
		Player pl = server.getPlayerList().getPlayer(player);
		if(pl != null)
		{
			TranslatableComponent t1 = new TranslatableComponent(r.getTranslationKey());
			TranslatableComponent trans = new TranslatableComponent("chat.research.add", t1);
			pl.displayClientMessage(trans, false);
		}
		checkInstandResearches();
		return true;
	}
	
	private void checkInstandResearches()
	{
		ArrayList<String> set = new ArrayList<String>(ResearchManager.getAllReseraches());
		set.removeAll(researches);
		for(String s : set)
		{
			Research r = ResearchManager.getResearch(s);
			if(canResearch(r, true))
			{
				if(r.time == 0)
				{
					addResearch(r);
				}
			}
		}
	}
	
	public Collection<String> getAllResearches()
	{
		return Collections.unmodifiableCollection(this.researches);
	}
	
	public void reset()
	{
		this.researches.clear();
		lastResearch = null;
	}

	public void addAchievement(Advancement achievement)
	{
		checkInstandResearches();
	}

	public Integer[] getResearchesInProgress(MinecraftServer server)
	{
		ArrayList<Integer> inProg = new ArrayList<Integer>();
		for(ServerLevel serv : server.getAllLevels())
		{
			addInPogressReaserches(serv, inProg);
		}
		return inProg.toArray(new Integer[inProg.size()]);
	}
	
	private void addInPogressReaserches(ServerLevel serv, ArrayList<Integer> inProg)
	{
		TileEntityForscher.researchers.removeIf(r -> r==null || r.get()==null);
		
		for(int i=0;i<TileEntityForscher.researchers.size();i++)
		{	
			TileEntityForscher tile = TileEntityForscher.researchers.get(i).get(); //Needs to be a indexed for loop to prevent concurennt modification exeption
			if(tile!=null && !tile.isRemoved() && tile.getLevel() == serv)
			{
				if(tile.getClass()==TileEntityForscher.class)//FIXME: add way to display when other players research this in a connected network with TileEntitySharedResearcher
				{
					TileEntityForscher forsch = (TileEntityForscher) tile;
					if(forsch.isOwner(player))
					{
						Research research = forsch.getResearch();
						if(research!=null)
						{
							inProg.add(research.id);
						}
					}
				}
			}
		}
	}
	
	public static interface IAdvancementManager
	{
		public AdvancementProgress getProgress(Advancement adv);
	}
	
	private static class ClientManager implements IAdvancementManager
	{
		Map<Advancement, AdvancementProgress> map;
		
		private AdvancementProgress none;
		
		private static Field f_map;
		
		static
		{
			Class<ClientAdvancements> cls = ClientAdvancements.class;
			Field[] ff = cls.getDeclaredFields();
			for(Field f : ff)
			{
				if(f.getType()==Map.class)
				{
					f_map = f;
					f.setAccessible(true);
					break;
				}
			}		
		}
		public ClientManager(ClientAdvancements mang)
		{
			try
			{
				map = (Map<Advancement, AdvancementProgress>) f_map.get(mang);
			}
			catch (IllegalArgumentException e)
			{
				e.printStackTrace();
			}
			catch (IllegalAccessException e)
			{
				e.printStackTrace();
			}
			none = new AdvancementProgress();
		}

		@Override
		public AdvancementProgress getProgress(Advancement adv)
		{
			return map.getOrDefault(adv, none);
		}
		
	}
}
