 package futurepack.common.research;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.mojang.authlib.GameProfile;

import futurepack.depend.api.helper.HelperChunks;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.TickEvent.Phase;
import net.minecraftforge.event.entity.player.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

/**
 * All research files MUST be loaded because the machines from the player check if they have the researches. 
 */
public class PlayerDataLoader
{
	public static final PlayerDataLoader instance = new PlayerDataLoader();
	
	private HashMap<UUID, CloseableCollection> profileMap;
	private File researchDir;
	private Map<UUID, Consumer<CloseableCollection>> patchMap;
	

	private PlayerDataLoader()
	{ 
		profileMap = new HashMap<UUID, CloseableCollection>(); //An empty map to prevent NullPointers.
	}
	
	@SubscribeEvent
	public void onWorldLoaded(WorldEvent.Load event)
	{
		if(event.getWorld().isClientSide())
			return;
		
		Level w = (Level) event.getWorld();
		if(w.dimension().location().equals(new ResourceLocation("overworld")))
		{
			File dir = new File(HelperChunks.getDimensionDir(w), "FP_Research");
			dir.mkdir();
			researchDir = dir;
			profileMap = new HashMap<UUID, CloseableCollection>(dir.list().length);
	
			loadResearchFolder(dir);
		}
	}
	
	private void loadResearchFolder(File dir)
	{
		for(File file : dir.listFiles())
		{
			if(file.getName().endsWith(".dat"))
			{
				String uid = file.getName().substring(0,file.getName().length()-4);
				try
				{
					UUID uuid = UUID.fromString(uid);
					ArrayList<String> list = loadResearchFile(uuid);
					if(list!=null)
						profileMap.put(uuid, new CloseableCollection(uuid, list));
				}
				catch(Exception e)
				{
					System.err.println(file + " is no research file");
					System.err.println("Caused by" + e);
				}
			}
		}
	}
	
	private ArrayList<String> loadResearchFile(UUID uid) throws FileNotFoundException, IOException
	{
		File file = new File(this.researchDir, uid.toString() + ".dat");
		if(file.exists())
		{
			GZIPInputStream in = new GZIPInputStream(new FileInputStream(file));
			JsonReader read = new JsonReader(new InputStreamReader(in));
			JsonArray arr = new Gson().fromJson(read, JsonArray.class);
			read.close();
			ArrayList<String> list = new ArrayList<String>(arr.size());
			for(int i=0;i<arr.size();i++)
			{
				list.add(arr.get(i).getAsString());
			}
			return list;
		}
		return null;
	}
	
	private void saveResearchFile(UUID uuid, CloseableCollection list) throws FileNotFoundException, IOException
	{
		JsonArray arr = new JsonArray();
		for(String s : list.col)//bypass closeable collection to prevent timer reset
		{
			arr.add(new JsonPrimitive(s));
		}
		if(uuid!=null)
		{
			File file = new File(researchDir, uuid + ".dat");
			GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(file));
			JsonWriter write = new JsonWriter(new OutputStreamWriter(out));
			new Gson().toJson(arr, write);
			write.close();
		}
	}
	
	@SubscribeEvent
	public void onWorldSaved(WorldEvent.Save event)
	{
		if(event.getWorld().isClientSide())
			return;
		
		Level w = (Level) event.getWorld();
		
		if(w.dimension().location().equals(new ResourceLocation("overworld")))
		{
			for(Entry<UUID, CloseableCollection> set : profileMap.entrySet())
			{
				try
				{
					saveResearchFile(set.getKey(), set.getValue());
				}
				catch(Exception e)
				{
					
					System.err.println("Unable to Save " + set.getKey());
					System.err.println("Caused by " + e);
				}
			}
		}
	}
	
	@SubscribeEvent
	public void onPlayerLogout(PlayerLoggedOutEvent event)
	{
		GameProfile prof = event.getPlayer().getGameProfile();
		UUID uid = prof.getId();
		CloseableCollection reas = profileMap.get(uid);
		if(reas != null)
		{
			try
			{
				reas.close();
				saveResearchFile(uid, reas);
				profileMap.remove(uid);
			}
			catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private int ticks = 0;
	
	@SubscribeEvent
	public void onServerTickEnd(TickEvent.ServerTickEvent event)
	{
		if(event.phase == Phase.END)
		{
			Map<UUID, Consumer<CloseableCollection>> map = patchMap;
			patchMap = null;
			if(map!=null)
			{
				for(Entry<UUID, Consumer<CloseableCollection>> e : map.entrySet())
				{
					try
					{
						ArrayList<String> s = loadResearchFile(e.getKey());
						if(s!=null)
						{
							CloseableCollection col = new CloseableCollection(e.getKey(), s);
							e.getValue().accept(col);
							col.close();
							saveResearchFile(e.getKey(), col);
						}
					}
					catch(IOException ee)
					{
						ee.printStackTrace();
					}
				}
				map.clear();
			}
			if(++ticks > 20)
			{
				ticks = 0;
				Iterator<Entry<UUID, CloseableCollection>> iter = profileMap.entrySet().iterator();
				long now = System.currentTimeMillis();
				while(iter.hasNext())
				{
					Entry<UUID, CloseableCollection> e = iter.next();
					long diff = now - e.getValue().getLastModificationTime();
					if(e.getValue().isClosed() || diff > 1000 * 60 * 5)
					{
						//now access since 5 minutes
						try
						{
							e.getValue().close();
							saveResearchFile(e.getKey(), e.getValue());
							iter.remove();
						}
						catch (IOException e1)
						{
							e1.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	/**
	 * Only this instance of the List will get saved, so if you dont modify it and just copy it. it will not get saved
	 */
 	public static Collection<String> getResearches(UUID id)
	{
		if(id==null)
			return null;
		
		Collection<String> s = instance.profileMap.get(id);
		if(s==null)
		{
			try {
				s = instance.loadResearchFile(id);
			} catch (FileNotFoundException e) {
			} catch (IOException e) {
				e.printStackTrace();
			}
			if(s==null)
				s = new ArrayList<String>();
			
			s = new CloseableCollection(id, s);
			instance.profileMap.put(id, (CloseableCollection) s);
		}
		
		return s;
	}
	
	public void addResearchPatch(UUID uid, String research)
	{
		if(patchMap==null)
			patchMap = new HashMap<>();
		
		patchMap.put(uid, r -> r.add(research));
	}
	
	public static class CloseableCollection implements Collection<String>, Closeable
	{
		private long time;
		private final Collection<String> col;
		private final UUID uuid;
		
		private boolean open = true;
		
		public CloseableCollection(UUID uid, Collection<String> col)
		{
			super();
			this.col = col;
			this.uuid = uid;
			time = System.currentTimeMillis();
			if(col == null)
				throw new NullPointerException("Collection was null");
		}

		public boolean isClosed()
		{
			return !open;
		}

		@Override
		public int size()
		{
			time = System.currentTimeMillis();
			return col.size();
		}

		@Override
		public boolean isEmpty()
		{
			time = System.currentTimeMillis();
			return col.isEmpty();
		}

		@Override
		public boolean contains(Object paramObject)
		{
			time = System.currentTimeMillis();
			return col.contains(paramObject);
		}

		@Override
		public Iterator<String> iterator()
		{
			time = System.currentTimeMillis();
			return col.iterator();
		}

		@Override
		public Object[] toArray()
		{
			time = System.currentTimeMillis();
			return col.toArray();
		}

		@Override
		public <T> T[] toArray(T[] paramArrayOfT)
		{
			time = System.currentTimeMillis();
			return col.toArray(paramArrayOfT);
		}

		@Override
		public boolean add(String paramE)
		{
			time = System.currentTimeMillis();
			if(open)
				return col.add(paramE);
			else
			{
				instance.addResearchPatch(uuid, paramE);
				return true;
			}
		}

		@Override
		public boolean remove(Object paramObject)
		{
			time = System.currentTimeMillis();
			if(open)
				return col.remove(paramObject);
			else
				return false;
		}

		@Override
		public boolean containsAll(Collection<?> paramCollection)
		{
			time = System.currentTimeMillis();
			return col.containsAll(paramCollection);
		}

		@Override
		public boolean addAll(Collection<? extends String> paramCollection)
		{
			time = System.currentTimeMillis();
			if(open)
				return col.addAll(paramCollection);
			else
			{
				boolean edit = false;
				for(String t : paramCollection)
					edit |= add(t);
				return edit;
			}
		}

		@Override
		public boolean removeAll(Collection<?> paramCollection)
		{
			time = System.currentTimeMillis();
			if(open)
				return col.removeAll(paramCollection);
			else
			{
				boolean edit = false;
				for(Object t : paramCollection)
					edit |= remove(t);
				return edit;
			}
		}

		@Override
		public boolean retainAll(Collection<?> paramCollection)
		{
			time = System.currentTimeMillis();
			if(open)
				return col.retainAll(paramCollection);
			else
				throw new UnsupportedOperationException("retainAll");
		}

		@Override
		public void clear()
		{
			time = System.currentTimeMillis();
			if(open)
				col.clear();
		}

		@Override
		public void close() throws IOException
		{
			open = false;	
		}
		
		public long getLastModificationTime()
		{
			return time;
		}
		
	}
}
