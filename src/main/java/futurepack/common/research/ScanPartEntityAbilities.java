package futurepack.common.research;

import futurepack.api.interfaces.IScanPart;
import net.minecraft.ChatFormatting;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.animal.horse.Horse;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.BlockHitResult;

public class ScanPartEntityAbilities implements IScanPart
{

	@Override
	public Component doBlock(Level w, BlockPos pos, boolean inGUI, BlockHitResult res)
	{
		return null;
	}

	@Override
	public Component doEntity(Level w, LivingEntity e, boolean inGUI)
	{
		float h = e.getHealth();
		float mh = (float) e.getAttribute(Attributes.MAX_HEALTH).getValue();
		float speed = (float) e.getAttribute(Attributes.MOVEMENT_SPEED).getValue();
		float attack = 0;
		if(e.getAttribute(Attributes.ATTACK_DAMAGE)!=null)
			attack = (float) e.getAttribute(Attributes.ATTACK_DAMAGE).getValue();
		float jump = 0;
		if(e instanceof Horse)
			jump = (float) ((Horse)e).getCustomJump();
	
		return new TextComponent((inGUI ? ChatFormatting.DARK_GREEN : ChatFormatting.GREEN) + "HP:" + h + "/" + mh + " Speed:" + speed + (attack!=0?" Atk:" + attack:"") + (jump!=0?" Jump:" + jump : ""));
	}

}
