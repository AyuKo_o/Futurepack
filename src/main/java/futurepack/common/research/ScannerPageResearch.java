package futurepack.common.research;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.function.Function;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import futurepack.common.FPLog;
import futurepack.common.gui.escanner.ComponentAssembly;
import futurepack.common.gui.escanner.ComponentBuilding;
import futurepack.common.gui.escanner.ComponentCrafting;
import futurepack.common.gui.escanner.ComponentHeading;
import futurepack.common.gui.escanner.ComponentIndustrialNeonFurnace;
import futurepack.common.gui.escanner.ComponentIndustrialfurnace;
import futurepack.common.gui.escanner.ComponentPartpress;
import futurepack.common.gui.escanner.ComponentPicture;
import futurepack.common.gui.escanner.ComponentRevision;
import futurepack.common.gui.escanner.ComponentSmelting;
import futurepack.common.gui.escanner.ComponentText;
import futurepack.common.gui.escanner.ComponentTranslate;
import futurepack.common.gui.escanner.ComponentZentrifuge;
import futurepack.depend.api.interfaces.IGuiComponent;
import net.minecraft.client.Minecraft;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;

//@ TODO: OnlyIn(Dist.CLIENT)
public class ScannerPageResearch
{
//	private  String text;
//	public int maxpages;
//	public String[] pages;
//	
//	public ResearchPage(String t)
//	{
//		text = t;
//	}
//	
//	@Deprecated
//	public static ResearchPage createFromJson(JsonArray ob)
//	{
//		StringBuilder b = new StringBuilder();
//		
//		for(int i=0;i<ob.size();i++)
//		{
//			if(i>0)
//				b.append("\n\n");
//			b.append(ob.get(i).getAsString());		
//		}
//		ResearchPage page = new ResearchPage(b.toString());;
//		return page;
//	}
//	
	private static HashMap<String, Function<JsonObject, IGuiComponent>> StringToConstructor = new HashMap<>();
	private static String currentResearchId = null;
	
	static
	{
		registerComponent("furnace", ComponentSmelting.class);
		registerComponent("crafting", ComponentCrafting.class);
		registerComponent("industrialfurnace", ComponentIndustrialfurnace.class);
		registerComponent("image", ComponentPicture.class);
		registerComponent("partpress", ComponentPartpress.class);
		registerComponent("assembly", ComponentAssembly.class);
		registerComponent("industrialneonfurnace", ComponentIndustrialNeonFurnace.class);
		registerComponent("translate", ComponentTranslate.class);
		registerComponent("zentrifuge", ComponentZentrifuge.class);
		registerComponent("building", ComponentBuilding.class);
		registerComponent("revision", ComponentRevision::new);
	}
	
	public static IGuiComponent[] getComponetsFromName(String researchId)
	{
		try
		{
			currentResearchId = researchId;
			IGuiComponent[] base = getComponetsFromJSON(ResearchLoader.instance.getLocalisated(researchId));
			IGuiComponent[] arr = new IGuiComponent[base.length+1];
			System.arraycopy(base, 0, arr, 1, base.length);
			arr[0] = new ComponentHeading(ResearchManager.instance.getHeading(researchId));
			currentResearchId = null;
			return arr;
		}
		catch(Exception e)
		{
			throw new RuntimeException("Error while loading " + researchId +  " for " +  Minecraft.getInstance().getLanguageManager().getSelected(), e);
		}
	}
	
	public static IGuiComponent[] getComponetsFromJSON(JsonArray ob)
	{		
		List<IGuiComponent> comps = new ArrayList<IGuiComponent>(ob.size()+1);
		for(int i=0;i<ob.size();i++)
		{
			JsonElement elm = ob.get(i);
			
			if(elm.isJsonPrimitive())
			{
				JsonPrimitive prim = elm.getAsJsonPrimitive();
				if(prim.isString())
				{
					String text = prim.getAsString();
					comps.add(new ComponentText(new TextComponent(text)));
				}		
			}
			else if(elm.isJsonObject())
			{
				JsonObject obj = elm.getAsJsonObject();
				
				JsonElement type = obj.get("type");
				if(type!=null)
				{
					Function<JsonObject, IGuiComponent> c = StringToConstructor.get(type.getAsString());
					if(c!=null)
					{	
						IGuiComponent comp = c.apply(obj);
						comps.add(comp);
					}
				}
				else
				{
					JsonElement link = obj.get("link");
					if(link!=null && link.isJsonPrimitive())
					{
						ResourceLocation res = new ResourceLocation(link.getAsString());
						try
						{
							JsonArray arr = ResearchLoader.loadResource(res, JsonArray.class);
							IGuiComponent[] arrC = getComponetsFromJSON(arr);
							comps.addAll(Arrays.asList(arrC));
						}
						catch (IOException e)
						{
							e.printStackTrace();
							comps.add(new ComponentHeading(e.toString()));
						}
					}
				}
/**
*  {
    "type": "crafting",
    "slots": {
        "1": {#item#},
        "2": {#item#},
        "3": {#item#},
        "4": {#item#},
        "5": {#item#},
        "6": {#item#},
        "7": {#item#},
        "8": {#item#},
        "9": {#item#},
        "out": {#item#}
    }
}


{
    "type": "furnace",
    "slots": {
        "in": {#item#},
        "out": {#item#}
    }
}
*/
			}
			
		}
		
		return comps.toArray(new IGuiComponent[comps.size()]);
	}
	
	public static String getCurrentlyLoadingResearch()
	{
		return currentResearchId;
	}
	
	public static void registerComponent(String type, Class<? extends IGuiComponent> cls)
	{
		try
		{
			Constructor<? extends IGuiComponent> cons = cls.getConstructor(JsonObject.class);
			Function<JsonObject, IGuiComponent> ctor = obj -> {
				try
				{
					return cons.newInstance(obj);
				}
				catch (IllegalAccessException e)
				{
					FPLog.logger.error("The registered Class %s has a private contruktor", cls);
				}
				catch (InstantiationException e)
				{
					e.printStackTrace();
				}
				catch (InvocationTargetException e)
				{
					FPLog.logger.error("There was an error in the constructor of an gui component");
					FPLog.logger.catching(e.getCause());
				}
				
				return null;
			}; 
			registerComponent(type, ctor);
		}
		catch (NoSuchMethodException e)
		{
			FPLog.logger.error("The registered Class %s has not the cunstruktor C(JsonObject.class), deleting", cls);
			StringToConstructor.remove(type);
		}
		catch (SecurityException e)
		{
			FPLog.logger.error("The registered Class %s is not accesible", cls);
		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
	}
	
	public static void registerComponent(String type, Function<JsonObject, IGuiComponent> constructor)
	{
		StringToConstructor.put(type, constructor);
	}
	
//	private static ItemStack getStack(JsonObject iob)
//	{
//		Item it = Item.getByNameOrId(iob.get("id").getAsString());
//		int meta = iob.getAsJsonPrimitive("meta").getAsInt();
//		return new ItemStack(it, 1, meta);
//	}
//

//	public void setup(FontRenderer fontRenderer, int w, int h)
//	{
//		int maxlines = h / fontRenderer.FONT_HEIGHT;
//		List<String> l = fontRenderer.listFormattedStringToWidth(text, w);
//		
//		maxpages = l.size() / maxlines;
//		maxpages += Math.min(1, l.size() % maxlines);
//		
//		pages = new String[maxpages];
//		
//		for(int i=0;i<maxpages;i++)
//		{
//			ArrayList<String> sub = new ArrayList(l.subList(i*maxlines, Math.min(l.size(), i*maxlines + maxlines)));
//			pages[i] = "";
//			for(String ss : sub)
//			{
//				pages[i] += ss + "\n";				
//			}
//		}
//	}
	
	
}
