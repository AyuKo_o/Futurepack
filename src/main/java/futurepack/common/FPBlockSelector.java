package futurepack.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nullable;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.api.interfaces.IBlockValidator;
import futurepack.api.interfaces.ISelector;
import futurepack.api.interfaces.IStatisticsManager;
import futurepack.depend.api.helper.HelperChunks;
import it.unimi.dsi.fastutil.longs.Long2BooleanAVLTreeMap;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.material.Material;

public class FPBlockSelector implements ISelector
{
	protected final Level w;
	private final IBlockSelector valid;

	private final HashMap<ParentCoords, Boolean> blocks = new HashMap<ParentCoords, Boolean>();
	private ArrayList<ParentCoords> aktive = new ArrayList<ParentCoords>();


	private List<ParentCoords> finishedBlockList;
	private float milis = -1F;

	private final IStatisticsManager statistiks;

	//End
	public static IBlockSelector getBase(HelperChunks.BlockCache cache)
	{
		return new IBlockSelector()
		{

			@Override
			public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean b, ParentCoords p)
			{
				return cache.getBlockState(w, pos).getMaterial() == m;
			}

			@Override
			public boolean canContinue(Level w, BlockPos pos, Material m, boolean dia, ParentCoords p)
			{
				return true;
			}
		};
	}

	public FPBlockSelector(Level w, IBlockSelector sel)
	{
		this(w, sel, null);
	}

	public FPBlockSelector(Level w, IBlockSelector sel, IStatisticsManager statistiks)
	{
		this.w = w;
		this.valid = sel;
		this.statistiks = statistiks;
		if(w==null)
			System.err.println("The World is null");

	}

	@Override
	public Level getWorld()
	{
		return w;
	}


//	/**
//	 * Short , this Starts the Selection of Blocks by {@link IBlockSelector}
//	*/
//	public void selectShip(BlockPos pos, Material m)
//	{
//		if(w.getBlockState(pos).getBlock().getMaterial() == m)
//		{
//			aktive.add(new ParentCoords(pos,null));
//
//			block(pos, m);
//
//			sx = ex = pos.getX();
//			sy = ey = pos.getY();
//			sz = ez = pos.getZ();
//			setPosition();
//			calcHighMap();
//
//		}
//	}

	public void selectBlocks(BlockPos pos)
	{
		long tome = System.nanoTime();
		aktive.add(new ParentCoords(pos,null));

		block(pos, null);
		float ms = (System.nanoTime()-tome)/1000000F;
		if(ms > 5F)
		{
				FPLog.logger.debug("Block Selection took %sms (Found %s blocks)", ms, blocks.size());
		}
		milis = ms;
	}

	public int getBlocks(BlockPos xyz, Material m)
	{
		if(w.getBlockState(xyz).getMaterial() == m)
		{
			aktive.add(new ParentCoords(xyz,null));

			block(xyz, m);

			return blocks.size();
		}
		return 0;
	}

	/**
	 * This runs while Blocks can Selected
	*/
	private void block(BlockPos pos, Material m)
	{
		while(aktive.size() > 0)
		{
			ArrayList<ParentCoords> aktive = this.aktive;
			this.aktive = new ArrayList<ParentCoords>(aktive.size());
			Iterator<ParentCoords> iter = aktive.iterator();
			ParentCoords p;
			while(iter.hasNext())
			{
				p = iter.next();
				blocks.put(p, false);
				ab(p, m);
			}
		}
		aktive.trimToSize();
	}

	/**
	 * This add new Blocks
	*/
	private void ab(ParentCoords c, Material m)
	{
		addBlock(c.offset(0,1,0),m,false, c);
		addBlock(c.offset(0, -1, 0),m,false, c);

		addBlock(c.offset(+1, 0, 0),m,false, c);
		addBlock(c.offset(-1, 0, 0),m,false, c);

		addBlock(c.offset(0, 0, +1),m,false, c);
		addBlock(c.offset(0, 0, -1),m,false, c);
		//
		addBlock(c.offset(-1, 0, +1),m, true, c);
		addBlock(c.offset(+1, 0, -1),m, true, c);
		addBlock(c.offset(+1, 0, +1),m, true, c);
		addBlock(c.offset(-1, 0, -1),m, true, c);

		addBlock(c.offset(0, +1, +1),m, true, c);
		addBlock(c.offset(0, +1, -1),m, true, c);
		addBlock(c.offset(+1, +1, 0),m, true, c);
		addBlock(c.offset(-1, +1, 0),m, true, c);

		addBlock(c.offset(0, -1, +1),m, true, c);
		addBlock(c.offset(0, -1, -1),m, true, c);
		addBlock(c.offset(+1, -1, 0),m, true, c);
		addBlock(c.offset(-1, -1, 0),m, true, c);
		//
		addBlock(c.offset(+1, +1, +1),m, true, c);
		addBlock(c.offset(-1, +1, -1),m, true, c);
		addBlock(c.offset(-1, +1, +1),m, true, c);
		addBlock(c.offset(+1, +1, -1),m, true, c);

		addBlock(c.offset(+1, -1, +1),m, true, c);
		addBlock(c.offset(-1, -1, -1),m, true, c);
		addBlock(c.offset(-1, -1, +1),m, true, c);
		addBlock(c.offset(+1, -1, -1),m, true, c);

	}

	/**
	 * Check if Block can add
	*/
	private void addBlock(BlockPos pos, Material m, boolean d, ParentCoords parent)
	{
		if(!isRegistert(pos) && valid.isValidBlock(w, pos, m, d, parent))
		{
			boolean b = valid.canContinue(w, pos, m, d, parent);
			ParentCoords coo = new ParentCoords(pos, parent);
			blocks.put(coo, b);
			if(b)
			{
				aktive.add(coo);
			}
			addBlockToStatistiks(pos);
		}
	}

	/**
	 * Check if the Block already is in the Block List
	*/
	private boolean isRegistert(BlockPos pos)
	{
		return blocks.containsKey(pos);
	}

	public void addBlockToStatistiks(BlockPos pos)
	{
		addBlockToStatistiks(w.getBlockState(pos).getBlock());
	}
	public void addBlockToStatistiks(Block b)
	{
		if(statistiks!=null)
		{
			statistiks.addBlockToStatistics(b);
		}
	}

	@Override
	public IStatisticsManager getStatisticsManager()
	{
		return statistiks;
	}

	@Override
	public Collection<ParentCoords> getValidBlocks(@Nullable IBlockValidator valid)
	{
		if(valid==null)
		{
			//return new ArrayList(blocks.keySet());
			return getBlocks();
		}

		List<ParentCoords> l = new ArrayList<ParentCoords>();
		Iterator<ParentCoords> iter = getBlocks().iterator();

		Long2BooleanAVLTreeMap isChunkLoaded = new Long2BooleanAVLTreeMap();

		while(iter.hasNext())
		{
			ParentCoords c = iter.next();

			if ( isChunkLoaded.computeIfAbsent(ChunkPos.asLong(c.getX()>>4, c.getZ()>>4), ll -> w.hasChunk(ChunkPos.getX(ll), ChunkPos.getZ(ll))) )
			{
				if(valid.isValidBlock(w, c))
				{
					l.add(c);
				}
			}
		}
		return l;
	}

	public void clear()
	{
		blocks.clear();
		aktive.clear();
		finishedBlockList=null;
		if(statistiks!=null)
			statistiks.clear();
	}

	private List<ParentCoords> getBlocks()
	{
		if(finishedBlockList!=null)
		{
			return finishedBlockList;
		}
		try
		{
			ParentCoords[] arr = new ParentCoords[blocks.size()];
			arr = blocks.keySet().toArray(arr);
			finishedBlockList = Arrays.asList(arr);
		}
		catch(ConcurrentModificationException e)
		{
			e.printStackTrace();
			System.out.println(this.aktive.size());
		}
		return finishedBlockList;
	}

	public Boolean isValid(BlockPos pos)
	{
		return blocks.get(pos);
	}

	/**
	 * @return the time needed in miliseconds. -1F is no blocks have been selected yet
	 */
	public float getNeededTime()
	{
		return milis;
	}
}
