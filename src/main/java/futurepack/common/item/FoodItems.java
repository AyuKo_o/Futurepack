package futurepack.common.item;

import futurepack.api.Constants;
import futurepack.common.block.plants.PlantBlocks;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class FoodItems 
{
	public static final Item.Properties food = new Item.Properties().tab(CreativeModeTab.TAB_FOOD);
	
	public static final FoodProperties mendel_berry_def = new FoodProperties.Builder().nutrition(2).saturationMod(0.2F).fast().build();
	public static final FoodProperties glowmelo_def = new FoodProperties.Builder().nutrition(20).saturationMod(100F).build();
	public static final FoodProperties astofood1_def = new FoodProperties.Builder().nutrition(20).saturationMod(20F).effect(new MobEffectInstance(MobEffects.HEALTH_BOOST, 3600, 4, false, false), 1F).effect(new MobEffectInstance(MobEffects.HEAL, 20, 20, false, false), 1F).build();
	public static final FoodProperties astofood2_def = new FoodProperties.Builder().nutrition(20).saturationMod(20F).effect(new MobEffectInstance(MobEffects.FIRE_RESISTANCE, 900, 1, false, false), 1F).effect(new MobEffectInstance(MobEffects.DAMAGE_RESISTANCE, 900, 1, false, false), 1F).build();
	public static final FoodProperties astofood3_def = new FoodProperties.Builder().nutrition(20).saturationMod(20F).effect(new MobEffectInstance(MobEffects.DIG_SPEED, 3600, 2, false, false), 1F).effect(new MobEffectInstance(MobEffects.MOVEMENT_SPEED, 3600, 2, false, false), 1F).build();
	public static final FoodProperties astrofoo4_def = new FoodProperties.Builder().nutrition(20).saturationMod(20F).effect(new MobEffectInstance(MobEffects.SATURATION, 3600, 1, false, false), 1F).effect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 3600, 3, false, false), 1F).effect(new MobEffectInstance(MobEffects.LUCK, 3600, 2, false, false), 1F).build();
	public static final FoodProperties ersemarmelade_def = new FoodProperties.Builder().nutrition(4).saturationMod(0.4F).build();
	public static final FoodProperties ersebrot_def = new FoodProperties.Builder().nutrition(10).saturationMod(0.8F).build();
	public static final FoodProperties glowshroom_raw_def = new FoodProperties.Builder().nutrition(1).saturationMod(0F).effect(new MobEffectInstance(MobEffects.POISON, 200, 3, false, true), 1F).build();
	public static final FoodProperties glowshroom_stew_def = new FoodProperties.Builder().nutrition(10).saturationMod(0.6F).build();
	public static final FoodProperties salad_def = new FoodProperties.Builder().nutrition(6).saturationMod(0.4F).build();
	public static final FoodProperties hufsteak_def = new FoodProperties.Builder().nutrition(3).saturationMod(0.3F).meat().build();
	public static final FoodProperties grillhufsteak_def = new FoodProperties.Builder().nutrition(10).saturationMod(0.9F).meat().build();
	public static final FoodProperties topinambur_potato_def = new FoodProperties.Builder().nutrition(4).saturationMod(0.6F).build();
	public static final FoodProperties palirie_nut_def = new FoodProperties.Builder().nutrition(1).saturationMod(0.4F).fast().build();
	
	public static final Item mendel_berry = HelperItems.setRegistryName(new Item(food(mendel_berry_def)), Constants.MOD_ID, "mendel_berry");
	public static final Item astrofood1 = HelperItems.setRegistryName(new ItemContainedFood(food(astofood1_def), new ItemStack(CraftingItems.astrofood_empty), 128), Constants.MOD_ID, "astrofood1");	//Fleisch mit Ketshup
	public static final Item astrofood2 = HelperItems.setRegistryName(new ItemContainedFood(food(astofood2_def), new ItemStack(CraftingItems.astrofood_empty), 128), Constants.MOD_ID, "astrofood2");	//Curryso�e
	public static final Item astrofood3 = HelperItems.setRegistryName(new ItemContainedFood(food(astofood3_def), new ItemStack(CraftingItems.astrofood_empty), 128), Constants.MOD_ID, "astrofood3");	//Gr�nzeug
	public static final Item astrofood4 = HelperItems.setRegistryName(new ItemContainedFood(food(astrofoo4_def), new ItemStack(CraftingItems.astrofood_empty), 128), Constants.MOD_ID, "astrofood4");	//Goo
	public static final Item ersemarmelade = HelperItems.setRegistryName(new Item(food(ersemarmelade_def)), Constants.MOD_ID, "ersemarmelade");
	public static final Item ersebrot = HelperItems.setRegistryName(new Item(food(ersebrot_def)), Constants.MOD_ID, "ersebrot");
	public static final Item glowshroom_raw = HelperItems.setRegistryName(new ItemContainedFood(food(glowshroom_raw_def), new ItemStack(Items.BOWL)), Constants.MOD_ID, "glowshroom_raw");
	public static final Item glowshroom_stew = HelperItems.setRegistryName(new ItemContainedFood(food(glowshroom_stew_def), new ItemStack(Items.BOWL)), Constants.MOD_ID, "glowshroom_stew");
	public static final Item salad = HelperItems.setRegistryName(new ItemContainedFood(food(salad_def), new ItemStack(Items.BOWL)), Constants.MOD_ID, "salad");
	public static final Item hufsteak = HelperItems.setRegistryName(new ItemContainedFood(food(hufsteak_def), new ItemStack(Items.BONE)), Constants.MOD_ID, "hufsteak");
	public static final Item grillhufsteak = HelperItems.setRegistryName(new ItemContainedFood(food(grillhufsteak_def), new ItemStack(Items.BONE)), Constants.MOD_ID, "grillhufsteak");
	public static final Item topinambur_potato = HelperItems.setRegistryName(new BlockItem(PlantBlocks.topinambur, food(topinambur_potato_def)), Constants.MOD_ID, "topinambur_potato");
	public static final Item erse = HelperItems.setRegistryName(new ItemErse(PlantBlocks.erse, food), Constants.MOD_ID, "erse");
	public static final Item mendel_seed = HelperItems.setRegistryName(new ItemMendelSeed(food), Constants.MOD_ID, "mendel_seed");
	public static final Item oxades_seeds = HelperItems.setRegistryName(new BlockItem(PlantBlocks.oxades, food), Constants.MOD_ID, "oxades");
	public static final Item glowmelo = HelperItems.setRegistryName(new ItemGlowmelo(food(glowmelo_def)), Constants.MOD_ID, "glowmelo");
	public static final Item palirie_nut = HelperItems.setRegistryName(new Item(food(palirie_nut_def)), Constants.MOD_ID, "palirie_nut");

	public static void register(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
				
		r.registerAll(mendel_berry, glowmelo, astrofood1, astrofood2, astrofood3, astrofood4, ersemarmelade, ersebrot, glowshroom_raw, glowshroom_stew, salad, hufsteak, grillhufsteak, topinambur_potato);
		r.registerAll(erse, mendel_seed, oxades_seeds, palirie_nut);
	}
	
	private static Item.Properties food(FoodProperties food)
	{
		return new Item.Properties().tab(CreativeModeTab.TAB_FOOD).food(food);
	}
}
