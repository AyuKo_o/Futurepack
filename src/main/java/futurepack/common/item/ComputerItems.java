package futurepack.common.item;

import futurepack.api.Constants;
import futurepack.common.FuturepackMain;
import futurepack.common.modification.EnumChipType;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Item.Properties;
import net.minecraftforge.event.RegistryEvent;

public class ComputerItems 
{
	private static final Properties defaultP = new Properties().tab(FuturepackMain.tab_items);
	
	public static final Item logic_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.LOGIC, false), Constants.MOD_ID, "chip_logic");
	public static final Item ai_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.AI, true), Constants.MOD_ID, "chip_ai");
	public static final Item transport_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.TRANSPORT, false), Constants.MOD_ID, "chip_transport");
	public static final Item navigation_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.NAVIGATION, false), Constants.MOD_ID, "chip_navigation");
	public static final Item network_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.NETWORK, true), Constants.MOD_ID, "chip_network");
	public static final Item industrie_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.INDUSTRIE, false), Constants.MOD_ID, "chip_industrie");
	public static final Item redstone_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.REDSTONE, true), Constants.MOD_ID, "chip_redstone");
	public static final Item support_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.SUPPORT, true), Constants.MOD_ID, "chip_support");
	public static final Item tactic_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.TACTIC, false), Constants.MOD_ID, "chip_tactic");
	public static final Item ultimate_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.ULTIMATE, true), Constants.MOD_ID, "chip_ultimate");
	public static final Item damage_control_chip = HelperItems.setRegistryName(new ItemChip(defaultP, EnumChipType.DAMAGE_CONTROL, false), Constants.MOD_ID, "chip_damage_control");
	
	public static final Item standart_core = HelperItems.setRegistryName(new ItemCore(defaultP, 1), Constants.MOD_ID, "core_standart");
	public static final Item a1_core = HelperItems.setRegistryName(new ItemCore(defaultP, 2), Constants.MOD_ID, "core_a1");
	public static final Item p2_core = HelperItems.setRegistryName(new ItemCore(defaultP, 3), Constants.MOD_ID, "core_p2");
	public static final Item tct_core = HelperItems.setRegistryName(new ItemCore(defaultP, 4), Constants.MOD_ID, "core_tct");
	public static final Item master_core = HelperItems.setRegistryName(new ItemCore(defaultP, 5), Constants.MOD_ID, "core_master");
	public static final Item non_core = HelperItems.setRegistryName(new ItemCore(defaultP, 7), Constants.MOD_ID, "core_non");
	public static final Item dungeon_core = HelperItems.setRegistryName(new ItemCore(defaultP, 7), Constants.MOD_ID, "core_dungeon");
	public static final Item torus_core = HelperItems.setRegistryName(new ItemCore(defaultP, 8), Constants.MOD_ID, "core_torus");
	public static final Item zombie_core = HelperItems.setRegistryName(new ItemCore(defaultP, 0), Constants.MOD_ID, "core_zombie");
	public static final Item entronium_core = HelperItems.setRegistryName(new ItemCore(defaultP, 10), Constants.MOD_ID, "core_entronium");
	
	public static final Item standart_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 1), Constants.MOD_ID, "ram_standart");
	public static final Item a_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 2), Constants.MOD_ID, "ram_a");
	public static final Item p_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 3), Constants.MOD_ID, "ram_p");
	public static final Item tct_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 4), Constants.MOD_ID, "ram_tct");
	public static final Item master_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 5), Constants.MOD_ID, "ram_master");
	public static final Item non_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 6), Constants.MOD_ID, "ram_non");
	public static final Item dungeon_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 6), Constants.MOD_ID, "ram_dungeon");
	public static final Item torus_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 8), Constants.MOD_ID, "ram_torus");
	public static final Item zombie_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 0), Constants.MOD_ID, "ram_zombie");
	public static final Item entronium_ram = HelperItems.setRegistryName(new ItemRam(defaultP, 10), Constants.MOD_ID, "ram_entronium");
	
	public static final Item toasted_chip = HelperItems.setRegistryName(new Item(defaultP), Constants.MOD_ID, "toasted_chip");
	public static final Item toasted_ram = HelperItems.setRegistryName(new Item(defaultP), Constants.MOD_ID, "toasted_ram");
	public static final Item toasted_core = HelperItems.setRegistryName(new Item(defaultP), Constants.MOD_ID, "toasted_core");
	
	public static void register(RegistryEvent.Register<Item> event)
	{
		event.getRegistry().registerAll(logic_chip, ai_chip, transport_chip, navigation_chip, network_chip, industrie_chip, redstone_chip, support_chip, tactic_chip, ultimate_chip, damage_control_chip);
		event.getRegistry().registerAll(standart_core, a1_core, p2_core, tct_core, master_core, non_core, dungeon_core, torus_core, zombie_core, entronium_core);
		event.getRegistry().registerAll(standart_ram, a_ram, p_ram, tct_ram, master_ram, non_ram, dungeon_ram, torus_ram, zombie_ram, entronium_ram);
		event.getRegistry().registerAll(toasted_chip, toasted_ram, toasted_core);
	}
	
}
