package futurepack.common.item.misc;

import futurepack.common.block.modification.TileEntityRocketLauncher;
import futurepack.common.entity.throwable.EntityRocket;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemRocket extends Item
{
	public ItemRocket(Item.Properties props)
	{
		super(props);
//		setCreativeTab(FPMain.tab_items);
//		setMaxStackSize(1);
//		setMaxDamage(16);
	}

	public EntityRocket createRocket(Level world, LivingEntity target, TileEntityRocketLauncher tileEntityRocketLauncher, ItemStack stack)
	{
		stack.hurtAndBreak(1, target, ent -> {});
		return new EntityRocket.EntityNormalRocket(world, target);
	}
	
	public static class ItemBlazeRocket extends ItemRocket
	{
		public ItemBlazeRocket(Properties props) 
		{
			super(props);
		}

		@Override
		public EntityRocket createRocket(Level world, LivingEntity target, TileEntityRocketLauncher tileEntityRocketLauncher, ItemStack stack)
		{
			stack.hurtAndBreak(1, target, ent -> {});
			return new EntityRocket.EntityBlazeRocket(world, target);
		}
	}
	
	public static class ItemPlasmaRocket extends ItemRocket
	{
		public ItemPlasmaRocket(Properties props) 
		{
			super(props);
		}

		@Override
		public EntityRocket createRocket(Level world, LivingEntity target, TileEntityRocketLauncher tileEntityRocketLauncher, ItemStack stack)
		{
			stack.hurtAndBreak(1, target, ent -> {});
			return new EntityRocket.EntityPlasmaRocket(world, target);
		}
	}
	
	public static class ItemBioteriumRocket extends ItemRocket
	{
		public ItemBioteriumRocket(Properties props) 
		{
			super(props);
		}

		@Override
		public EntityRocket createRocket(Level world, LivingEntity target, TileEntityRocketLauncher tileEntityRocketLauncher, ItemStack stack)
		{
			stack.hurtAndBreak(1, target, ent -> {});
			return new EntityRocket.EntityBioteriumRocket(world, target);
		}
	}
}
