package futurepack.common.item.misc;

import java.util.List;

import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.MessageEScanner;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.HoverEvent;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.network.PacketDistributor;

public class ItemDungeonTablet extends Item 
{

	public ItemDungeonTablet(Properties properties) 
	{
		super(properties);
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level w, Player pl, InteractionHand hand) 
	{
		ItemStack itemstack = pl.getItemInHand(hand);
		if(itemstack.hasTag())
		{
			String path = itemstack.getTag().getString("research_path");
			if(!path.isEmpty())
			{
				InteractionResultHolder.success(itemstack);
				TextComponent txt = new TextComponent(path);
				txt.setStyle(Style.EMPTY.withHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent("load_research=" + path))));
				MessageEScanner msg = new MessageEScanner(true, txt);
				if(pl instanceof ServerPlayer)
				{
					FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.PLAYER.with(() -> (ServerPlayer)pl), msg);
				}
				return InteractionResultHolder.success(itemstack);
			}		
		}
		return super.use(w, pl, hand);
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		if(stack.hasTag())
		{
			String path = stack.getTag().getString("research_path");
			if(!path.isEmpty())
			{
				if(path.contains(":"))
				{
					path = path.split(":", 2)[1];
				}
				tooltip.add(new TranslatableComponent("research." + path));
			}		
		}
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}
}
