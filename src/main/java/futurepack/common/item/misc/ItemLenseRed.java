package futurepack.common.item.misc;

import java.util.ArrayList;
import java.util.Optional;
import java.util.WeakHashMap;

import futurepack.api.PacketBase;
import futurepack.api.helper.HelperTags;
import futurepack.api.interfaces.IDeepCoreLogic;
import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.api.interfaces.tilentity.ITileNetwork;
import futurepack.common.FPLog;
import futurepack.common.block.misc.TileEntityBedrockRift;
import futurepack.common.block.multiblock.TileEntityDeepCoreMinerMain;
import futurepack.common.item.ItemLenseBase;
import futurepack.common.modification.EnumChipType;
import futurepack.common.network.FunkPacketExperienceDistribution;
import futurepack.depend.api.helper.HelperInventory;
import futurepack.depend.api.helper.HelperItemFilter;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.world.scanning.ChunkData;
import net.minecraft.core.BlockPos;
import net.minecraft.core.NonNullList;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.ExperienceOrb;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemHandlerHelper;

public class ItemLenseRed extends ItemLenseBase
{

	public ItemLenseRed(Properties properties) 
	{
		super(properties, 100);
	}

	@Override
	public boolean isWorking(ItemStack item, IDeepCoreLogic logic)
	{
		CompoundTag mining = item.getOrCreateTagElement("mining");
		if(mining.contains("items"))
			return false;
			
		TileEntityBedrockRift rift = (TileEntityBedrockRift) logic.getRift();	
		return rift!=null && rift.getFillrate() > 0D && rift.isScanned();		
	}
	
	@Override
	public int getMaxDurability(ItemStack item, IDeepCoreLogic logic)
	{
		return 4;
	}
	
	@Override
	public boolean updateProgress(ItemStack item, IDeepCoreLogic logic)
	{
		return updateMining(item, logic);
	}

	@Override
	public int getColor(ItemStack item, IDeepCoreLogic logic)
	{
		return 0xFF0000;
	}
	
	private boolean updateMining(ItemStack item, IDeepCoreLogic logic)
	{
		TileEntityBedrockRift rift = (TileEntityBedrockRift) logic.getRift();
		TileEntityDeepCoreMinerMain main = (TileEntityDeepCoreMinerMain) logic.getTileEntity();
		if(rift==null || !rift.isScanned())
			return false;
		
		ChunkData data = rift.getData();
		float total = data.getTotalOres();
		float maxTicks = 350F / total * 1200F;
		
		if(main.getChipPower(EnumChipType.SUPPORT) > 0)
		{
			maxTicks *= 3;
		}
		if(main.getChipPower(EnumChipType.NAVIGATION) > 0)
		{
			maxTicks *= 3;
		}
		
		CompoundTag nbt = item.getOrCreateTagElement("mining");
		
		if(nbt.contains("items"))
		{
			ListTag oldItems = nbt.getList("items", 10);
			ListTag newItem = new ListTag();
			
			IItemHandler handler = main.getDeepCoreInventory(true);
			for(int i=0;i<oldItems.size();i++)
			{	
				ItemStack notInserted = ItemHandlerHelper.insertItem(handler, ItemStack.of(oldItems.getCompound(i)), false);
				if(!notInserted.isEmpty())
				{
					newItem.add(notInserted.serializeNBT());
				}
			}
			if(newItem.isEmpty())
			{
				nbt.remove("items");
			}
			else
			{
				nbt.put("items", newItem);
			}
		}
		int progress = nbt.getInt("progress");
		
		if(++progress > maxTicks)
		{
			progress = 0;
			double random = rift.getLevel().random.nextDouble();
			if(random < rift.getFillrate())
			{			
				String tagPath = data.getRandomOre(rift.getLevel().random.nextInt((int) total));
				if(tagPath!=null)
				{	
					int stackSize = (int) (4 + main.getChipPower(EnumChipType.INDUSTRIE) * 10);
						if(stackSize>64)
							stackSize = 64;
					boolean silk_touch = main.getChipPower(EnumChipType.TACTIC) > 0;	
					
					TagKey<Block> oreTag = BlockTags.create(new ResourceLocation(tagPath));
					Optional<Block> anyBlock = HelperTags.getValuesBlocks(oreTag).unordered().findFirst();
					if(anyBlock.isEmpty())
					{
						FPLog.logger.warn("Random Ore Tag was empty!!! (" + tagPath + ")");
						return false;
					}
					Block entry = anyBlock.get();
					
					ItemStack ore = new ItemStack(HelperOreDict.FuturepackConveter.getChangedItem(entry.asItem()), 1);
					ore.setCount(stackSize);
					IItemHandler handler = main.getDeepCoreInventory(true);	
					float ultimate = main.getChipPower(EnumChipType.ULTIMATE);
						
					NonNullList<ItemStack> drops = NonNullList.create();
					
					if(!silk_touch)
					{
						if(ore.getItem() instanceof BlockItem)
						{
							Block bl = ((BlockItem)ore.getItem()).getBlock();
							BlockState state = bl.defaultBlockState();
												
							for(int i=0;i<stackSize;i++)
							{
								drops.addAll(Block.getDrops(state, (ServerLevel) rift.getLevel(), new BlockPos(0,0,0), null));
							}
								
						}
						else
						{
							throw new IllegalArgumentException(ore + "is not a normal ItemBlock");
						}
					}
					else
					{
						drops.add(ore);
					}	
					
					drops = getFilteredDrops(drops, main);
					if(!drops.isEmpty())
					{
						stackSize = 0;
						for(int i=0;i<drops.size();i++)
						{
							stackSize += drops.get(i).getCount();
							ItemStack notInserted = ItemHandlerHelper.insertItem(handler, drops.get(i), false);
							if(!notInserted.isEmpty())
							{
								ListTag list;
								if(nbt.contains("items"))
									list = nbt.getList("items", 10);
								else							
									list = new ListTag();
								list.add(notInserted.serializeNBT());
								nbt.put("items", list);
							}
						}
						
						if(main.getChipPower(EnumChipType.SUPPORT) > 0)
							main.support.add(stackSize);
											
						if(ultimate>0)
						{
							float xp = ultimate  * stackSize;
							if(xp>0)
							{
								int val = (int) xp;
								if(val<xp)
									val++;
								
								if(main.getChipPower(EnumChipType.NETWORK)>0)
								{
									FunkPacketExperienceDistribution exp = new FunkPacketExperienceDistribution(main.getBlockPos(), new ITileNetwork()
						            {			
										@Override
										public void onFunkPacket(PacketBase pkt) { }
										
										@Override
										public boolean isWire()
										{
											return false;
										}
										
										@Override
										public boolean isNetworkAble()
										{
											return true;
										}
									}, val);
									if(!HelperInventory.sendPacket(main.getLevel(), main.getBlockPos(), exp))
									{
										BlockPos p = main.getBlockPos().relative(main.getFacing());
										Vec3 pos = new Vec3(p.getX()+0.5, p.getY()+0.5, p.getZ()+0.5);
										ExperienceOrb orb = new ExperienceOrb(main.getLevel(), pos.x, pos.y, pos.z, val);
										main.getLevel().addFreshEntity(orb);
									}
								}
								else
								{
									BlockPos p = main.getBlockPos().relative(main.getFacing());
									Vec3 pos = new Vec3(p.getX()+0.5, p.getY()+0.5, p.getZ()+0.5);
									ExperienceOrb orb = new ExperienceOrb(main.getLevel(), pos.x, pos.y, pos.z, val);
									main.getLevel().addFreshEntity(orb);
								}				
							}
						}
						
						double removed = (stackSize *stackSize/16384D); 
						rift.removeOres(removed);
					}
				}	
				else
				{
					FPLog.logger.warn("Random Ore Name was null!!!");
				}
			}
			nbt.putInt("progress", 0);
			logic.setProgress(0F);
			return true;
		}
		nbt.putInt("progress", progress);
		logic.setProgress(progress / maxTicks);
		return false;
	}
	
	static class FilterCache
	{
		private ArrayList<ItemStack> 		filterItems		= new ArrayList<ItemStack>();
		public ArrayList<TagKey<Item>> 	validTags 		= new ArrayList<>();
		public ArrayList<IItemFilter> 		itemFilters 	= new ArrayList<IItemFilter>();

		private boolean check(ItemStack[] targetFilter)
		{
			if(filterItems.size() == targetFilter.length)
			{
				for(int i = 0; i < filterItems.size(); i ++)
				{
					if(!HelperInventory.areItemsEqualNoSize(filterItems.get(i), targetFilter[i]))
					{
						return false;
					}
				}
				
				return true;
			}
			
			return false;
		}
		
		public boolean validateOrSetup(ItemStack[] targetFilter) 
		{
			//Check Cache
			if(check(targetFilter))
			{
				//System.out.println("Cache Hit");
				return false;
			}
			
			//Rewrite Cache
			filterItems.clear();		
			for(ItemStack st : targetFilter)
			{
				filterItems.add(st);
			}
			
			//Generate Fitlers
			//System.out.println("Parsing Filters ...");
			validTags.clear();
			itemFilters.clear();
			for(ItemStack st : filterItems)
			{
				st.getTags().forEach(key -> {
					ResourceLocation tag = key.location();
					if(tag.getPath().startsWith("ores/") || tag.getPath().startsWith("dusts/") || tag.getPath().startsWith("gems/") || tag.getPath().startsWith("ingot/"))
					{
						//System.out.println("Accepting Tag: " + tag.toString() + " for Item {" + st.toString() + "}");
						validTags.add(key);
					}
					else
					{
						//System.out.println("Discard   Tag: " + tag.toString() + " for Item {" + st.toString() + "}");
					}
				});
				
				itemFilters.add(HelperItemFilter.getFilter(st));
			}
			
			return true;
		}
		
	};
	
	private static WeakHashMap<TileEntityDeepCoreMinerMain, FilterCache> filterCacheMap = new WeakHashMap<TileEntityDeepCoreMinerMain, FilterCache>();
	
	private NonNullList<ItemStack> getFilteredDrops(NonNullList<ItemStack> ore, TileEntityDeepCoreMinerMain main)//not realy possible with tags anymore...
	{
		if(main.getChipPower(EnumChipType.NAVIGATION)  <= 0)
			return ore;
		
		
		FilterCache cache = filterCacheMap.get(main);

		if(cache == null)
		{
			cache = new FilterCache();
			filterCacheMap.put(main, cache);
		}
		
		NonNullList<ItemStack> filtered = NonNullList.create();
		
		if(cache.validateOrSetup(main.getFilters()))
		{
		//	filterCacheMap.put(main, cache);
		}
		
			
		DROPS:
		for(ItemStack drop : ore)
		{
			for(IItemFilter iif : cache.itemFilters)
			{
				if(iif.test(drop))
				{
					iif.amountTransfered(drop);
					//System.out.println("Accepting Drop: {" + drop.toString() + "} using Filter");
					filtered.add(drop);
					continue DROPS;					
				}
				
			}
			
			for(TagKey<Item> tag : cache.validTags)
			{
				if(drop.is(tag))
				{
					//System.out.println("Accepting Drop: {" + drop.toString() + "} using Tag " + tag.toString());
					filtered.add(drop);
					continue DROPS;
				}	
			}
			
			//System.out.println("Discard   Drop: {" + drop.toString() + "} ");
		}
		
		return filtered;
	}
	
	@Override
	public ParticleOptions randomParticle(ItemStack lenseStack, IDeepCoreLogic logic)
	{
		return logic.getTileEntity().getLevel().random.nextInt(10)<3 ? ParticleTypes.LAVA : null;
	}
}
