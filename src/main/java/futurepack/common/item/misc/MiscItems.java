package futurepack.common.item.misc;

import futurepack.api.Constants;
import futurepack.common.FPEntitys;
import futurepack.common.FPSounds;
import futurepack.common.FuturepackMain;
import futurepack.common.item.ItemGreandeBase;
import futurepack.common.item.recycler.ItemAnalyzer;
import futurepack.common.item.recycler.ItemLaserCutter;
import futurepack.common.item.recycler.ItemShredder;
import futurepack.common.item.recycler.ItemTimeManipulator;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Rarity;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.registries.IForgeRegistry;

public class MiscItems 
{
	public static final Item RESEARCH_BLUEPRINT = HelperItems.setRegistryName(new ItemResearchBlueprint(new Item.Properties().stacksTo(16).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "research_blueprint");
	
	public static final Item lack_tank_empty = HelperItems.setRegistryName(new Item(new Item.Properties().tab(FuturepackMain.tab_items)), Constants.MOD_ID, "lack_tank_empty");
	private static final Item.Properties tank = new Item.Properties().craftRemainder(lack_tank_empty).defaultDurability(256).tab(FuturepackMain.tab_tools);
	public static final Item lack_tank_black = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.BLACK), Constants.MOD_ID, "lack_tank_black");
	public static final Item lack_tank_blue = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.BLUE), Constants.MOD_ID, "lack_tank_blue");
	public static final Item lack_tank_brown = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.BROWN), Constants.MOD_ID, "lack_tank_brown");
	public static final Item lack_tank_cyan = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.CYAN), Constants.MOD_ID, "lack_tank_cyan");
	public static final Item lack_tank_gray = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.GRAY), Constants.MOD_ID, "lack_tank_gray");
	public static final Item lack_tank_green = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.GREEN), Constants.MOD_ID, "lack_tank_green");
	public static final Item lack_tank_lightblue = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.LIGHT_BLUE), Constants.MOD_ID, "lack_tank_light_blue");
	public static final Item lack_tank_lightgray = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.LIGHT_GRAY), Constants.MOD_ID, "lack_tank_light_gray");
	public static final Item lack_tank_lime = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.LIME), Constants.MOD_ID, "lack_tank_lime");
	public static final Item lack_tank_magenta = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.MAGENTA), Constants.MOD_ID, "lack_tank_magenta");
	public static final Item lack_tank_orange = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.ORANGE), Constants.MOD_ID, "lack_tank_orange");
	public static final Item lack_tank_pink = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.PINK), Constants.MOD_ID, "lack_tank_pink");
	public static final Item lack_tank_purple = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.PURPLE), Constants.MOD_ID, "lack_tank_purple");
	public static final Item lack_tank_red = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.RED), Constants.MOD_ID, "lack_tank_red");
	public static final Item lack_tank_white = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.WHITE), Constants.MOD_ID, "lack_tank_white");
	public static final Item lack_tank_yellow = HelperItems.setRegistryName(new ItemLackTank(tank, DyeColor.YELLOW), Constants.MOD_ID, "lack_tank_yellow");
	
	private static final Item.Properties notstakable = new Item.Properties().stacksTo(1).tab(FuturepackMain.tab_items);
	public static final Item record_futurepack = HelperItems.setRegistryName(new ItemFpRecord(FPSounds.SOUNDTRACK, notstakable), Constants.MOD_ID, "record_futurepack");
	public static final Item record_menelaus = HelperItems.setRegistryName(new ItemFpRecord(FPSounds.MENELAUS, notstakable), Constants.MOD_ID, "record_fp_menelaus");
	public static final Item record_tyros = HelperItems.setRegistryName(new ItemFpRecord(FPSounds.TYROS, notstakable), Constants.MOD_ID, "record_fp_tyros");
	public static final Item record_entros = HelperItems.setRegistryName(new ItemFpRecord(FPSounds.ENTROS, notstakable), Constants.MOD_ID, "record_fp_entros");
	public static final Item record_unknown = HelperItems.setRegistryName(new ItemFpRecord(FPSounds.UNKNOWN, notstakable), Constants.MOD_ID, "record_fp_unknown");
	public static final Item record_envia = HelperItems.setRegistryName(new ItemFpRecord(FPSounds.ENVIA, notstakable), Constants.MOD_ID, "record_fp_envia");
	public static final Item record_lyrara = HelperItems.setRegistryName(new ItemFpRecord(FPSounds.LYRARA, notstakable), Constants.MOD_ID, "record_fp_lyrara");
	
	
	public static final Item aiFlash0 = HelperItems.setRegistryName(new ItemAIFlash(notstakable,128), Constants.MOD_ID, "aiflash0");
	public static final Item aiFlash1 = HelperItems.setRegistryName(new ItemAIFlash(notstakable,256), Constants.MOD_ID, "aiflash1");
	public static final Item aiFlash2 = HelperItems.setRegistryName(new ItemAIFlash(notstakable,512), Constants.MOD_ID, "aiflash2");
	public static final Item aiFlash3 = HelperItems.setRegistryName(new ItemAIFlash(notstakable,1024), Constants.MOD_ID, "aiflash3");
	public static final Item aiFlash4 = HelperItems.setRegistryName(new ItemAIFlash(notstakable,2048), Constants.MOD_ID, "aiflash4");
	public static final Item aiFlash5 = HelperItems.setRegistryName(new ItemAIFlash(notstakable,4096), Constants.MOD_ID, "aiflash5");
	
	public static final Item NormalBatery = HelperItems.setRegistryName(new ItemBatterie(notstakable, 2000), Constants.MOD_ID, "battery_n");
	public static final Item LargeBatery = HelperItems.setRegistryName(new ItemBatterie(notstakable, 4000), Constants.MOD_ID, "battery_l");
	public static final Item NeonBatery = HelperItems.setRegistryName(new ItemBatterie(notstakable, 12000), Constants.MOD_ID, "battery_neon");
	public static final Item EnergyCell = HelperItems.setRegistryName(new ItemBatterie(notstakable, 80000), Constants.MOD_ID, "energy_cell");
	public static final Item CompactEnergyCell = HelperItems.setRegistryName(new ItemBatterie(notstakable, 320000), Constants.MOD_ID, "compact_energy_cell");
	public static final Item CrystalEnergyCell = HelperItems.setRegistryName(new ItemBatterie(notstakable, 1000000), Constants.MOD_ID, "crystal_energy_cell");	
	
	public static final Item crafting_recipe = HelperItems.setRegistryName(new ItemRecipe(notstakable), Constants.MOD_ID, "crafting_recipe");
	public static final Item assembly_recipe = HelperItems.setRegistryName(new ItemRecipe(notstakable), Constants.MOD_ID, "assembly_recipe");
	
	public static final Item display = HelperItems.setRegistryName(new ItemDisplay(new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "display");
	public static final Item lense_red = HelperItems.setRegistryName(new ItemLenseRed(notstakable), Constants.MOD_ID, "lense_red");
	public static final Item lense_green = HelperItems.setRegistryName(new ItemLenseGreen(notstakable), Constants.MOD_ID, "lense_green");
	public static final Item lense_white = HelperItems.setRegistryName(new ItemLenseWhite(notstakable), Constants.MOD_ID, "lense_white");
	public static final Item lense_purple = HelperItems.setRegistryName(new ItemLensePurple(notstakable), Constants.MOD_ID, "lense_purple");
	
	public static final Item kompost = HelperItems.setRegistryName(new ItemKompost(new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "kompost");
	public static final Item shredder = HelperItems.setRegistryName(new ItemShredder(new Item.Properties().defaultDurability(1024).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "shredder");
	public static final Item analyzer = HelperItems.setRegistryName(new ItemAnalyzer(new Item.Properties().defaultDurability(256).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "analyzer");
	public static final Item lasercutter = HelperItems.setRegistryName(new ItemLaserCutter(new Item.Properties().defaultDurability(2048).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "lasercutter");
	public static final Item timemanipulator = HelperItems.setRegistryName(new ItemTimeManipulator(new Item.Properties().defaultDurability(8192).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "timemanipulator");
	
	public static final Item spawn_note = HelperItems.setRegistryName(new ItemSpawnNote(new Item.Properties().rarity(Rarity.UNCOMMON)), Constants.MOD_ID, "spawn_note");
	public static final Item spacecoordinats = HelperItems.setRegistryName(new Item(new Item.Properties().stacksTo(1)), Constants.MOD_ID, "spacecoordinats");
	
	private static final Item.Properties grenade = new Item.Properties().stacksTo(64).tab(FuturepackMain.tab_items);
	public static final Item grenade_normal = HelperItems.setRegistryName(new ItemGreandeBase(grenade, FPEntitys.GRENADE_NORMAL::create), Constants.MOD_ID, "grenade_normal");
	public static final Item grenade_blaze = HelperItems.setRegistryName(new ItemGreandeBase(grenade, FPEntitys.GRENADE_BLAZE::create), Constants.MOD_ID, "grenade_blaze");
	public static final Item grenade_plasma = HelperItems.setRegistryName(new ItemGreandeBase(grenade, FPEntitys.GRENADE_PLASMA::create), Constants.MOD_ID, "grenade_plasma");
	public static final Item grenade_slime = HelperItems.setRegistryName(new ItemGreandeBase(grenade, FPEntitys.GRENADE_SLIME::create), Constants.MOD_ID, "grenade_slime");
	public static final Item grenade_futter = HelperItems.setRegistryName(new ItemGreandeBase(grenade, FPEntitys.GRENADE_FUTTER::create), Constants.MOD_ID, "grenade_futter");
	public static final Item grenade_saat = HelperItems.setRegistryName(new ItemGreandeBase(grenade, FPEntitys.GRENADE_SAAT::create), Constants.MOD_ID, "grenade_saat");
	public static final Item grenade_kompost = HelperItems.setRegistryName(new ItemGreandeBase(grenade, FPEntitys.GRENADE_KOMPOST::create), Constants.MOD_ID, "grenade_kompost");
	
	public static final Item.Properties rockets = new Item.Properties().stacksTo(1).defaultDurability(16).tab(FuturepackMain.tab_items);
	public static final Item rocket = HelperItems.setRegistryName(new ItemRocket(rockets), Constants.MOD_ID, "rocket");
	public static final Item rocket_plasma = HelperItems.setRegistryName(new ItemRocket.ItemPlasmaRocket(rockets), Constants.MOD_ID, "rocket_plasma");
	public static final Item rocket_blaze = HelperItems.setRegistryName(new ItemRocket.ItemBlazeRocket(rockets), Constants.MOD_ID, "rocket_blaze");
	public static final Item rocket_bioterium = HelperItems.setRegistryName(new ItemRocket.ItemBioteriumRocket(rockets), Constants.MOD_ID, "rocket_bioterium");
	
	public static final Item icon_plasma = HelperItems.setRegistryName(new Item(new Item.Properties().stacksTo(1).rarity(Rarity.EPIC)), Constants.MOD_ID, "icon_plasma");
	public static final Item icon_blaze = HelperItems.setRegistryName(new Item(new Item.Properties().stacksTo(1).rarity(Rarity.EPIC)), Constants.MOD_ID, "icon_blaze");
	public static final Item icon_slime = HelperItems.setRegistryName(new Item(new Item.Properties().stacksTo(1).rarity(Rarity.EPIC)), Constants.MOD_ID, "icon_slime");
	public static final Item telescope = HelperItems.setRegistryName(new ItemTelescope(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "telescope");
	public static final Item script_filter = HelperItems.setRegistryName(new ItemScriptFilter(new Item.Properties().stacksTo(1).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "script_filter");
	
	public static final Item composite_horse_armor = HelperItems.setRegistryName(new ItemHorseArmorComposite(), Constants.MOD_ID, "composite_horse_armor");
	public static final Item dungeon_tablet = HelperItems.setRegistryName(new ItemDungeonTablet(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON)), Constants.MOD_ID, "dungeon_tablet");
	public static final Item ancient_energy = HelperItems.setRegistryName(new ItemAncientEnergy(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON)), Constants.MOD_ID, "ancient_energy");
	public static final Item hambone = HelperItems.setRegistryName(new ItemHambone(new Item.Properties().stacksTo(1).rarity(Rarity.UNCOMMON).tab(FuturepackMain.tab_items)), Constants.MOD_ID, "hambone");
	
	public static void register(RegistryEvent.Register<Item> event)
	{
		IForgeRegistry<Item> r = event.getRegistry();
		
		r.register(RESEARCH_BLUEPRINT);
		r.registerAll(lack_tank_black, lack_tank_blue, lack_tank_brown, lack_tank_cyan, lack_tank_gray, lack_tank_green, lack_tank_lightblue, lack_tank_lightgray, lack_tank_lime, lack_tank_magenta, lack_tank_orange, lack_tank_pink, lack_tank_purple, lack_tank_red, lack_tank_white, lack_tank_yellow);
		r.register(lack_tank_empty);
		r.registerAll(record_entros, record_envia, record_futurepack, record_menelaus, record_tyros, record_unknown, record_lyrara);
		r.registerAll(aiFlash0, aiFlash1, aiFlash2, aiFlash3, aiFlash4, aiFlash5);
		r.registerAll(NormalBatery, LargeBatery, NeonBatery, EnergyCell, CompactEnergyCell, CrystalEnergyCell);
		r.registerAll(crafting_recipe, assembly_recipe);
		r.registerAll(display, lense_red, lense_green, lense_white, lense_purple, kompost);
		r.registerAll(shredder, analyzer, lasercutter, timemanipulator);
		r.registerAll(spawn_note, spacecoordinats);
		r.registerAll(grenade_normal, grenade_blaze, grenade_plasma, grenade_slime, grenade_futter, grenade_saat, grenade_kompost);
		r.registerAll(rocket, rocket_plasma, rocket_blaze, rocket_bioterium);
		r.registerAll(icon_plasma, icon_blaze, icon_slime, telescope, script_filter);
		r.registerAll(composite_horse_armor, dungeon_tablet, ancient_energy);
		r.registerAll(hambone);
	}
}
