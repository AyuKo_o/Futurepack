package futurepack.common.item;

import java.util.function.BiFunction;

import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.stats.Stats;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.projectile.ThrowableProjectile;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemThrowable extends Item
{
	private final BiFunction<Level, Player, ThrowableProjectile> throwable;

	public ItemThrowable(Properties properties, BiFunction<Level, Player, ThrowableProjectile> func)
	{
		super(properties);
		throwable = func;
	}
	
	@Override
	public InteractionResultHolder<ItemStack> use(Level worldIn, Player playerIn, InteractionHand handIn)
	{
		if(throwable==null)
			return super.use(worldIn, playerIn, handIn);
		
		ItemStack itemstack = playerIn.getItemInHand(handIn);
		if (!playerIn.getAbilities().instabuild) 
		{
			itemstack.shrink(1);
		}

		worldIn.playSound((Player)null, playerIn.getX(), playerIn.getY(), playerIn.getZ(), SoundEvents.SNOWBALL_THROW, SoundSource.NEUTRAL, 0.5F, 0.4F / (worldIn.random.nextFloat() * 0.4F + 0.8F));
		if (!worldIn.isClientSide) 
		{
			ThrowableProjectile e = throwable.apply(worldIn, playerIn);
			e.shootFromRotation(playerIn, playerIn.getXRot(), playerIn.getYRot(), 0.0F, 1.5F, 1.0F);
			worldIn.addFreshEntity(e);
		}

		playerIn.awardStat(Stats.ITEM_USED.get(this));
		return new InteractionResultHolder<>(InteractionResult.SUCCESS, itemstack);
	}

}
