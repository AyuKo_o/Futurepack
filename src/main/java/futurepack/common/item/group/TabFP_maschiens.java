package futurepack.common.item.group;

import futurepack.common.block.inventory.InventoryBlocks;
import net.minecraft.world.item.ItemStack;

public class TabFP_maschiens extends TabFB_Base
{
	public TabFP_maschiens(String label) 
	{
		super(label);
	}

	@Override
	public ItemStack makeIcon()
	{
		return new ItemStack(InventoryBlocks.techtable);
	}
}
