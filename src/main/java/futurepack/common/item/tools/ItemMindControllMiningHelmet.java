package futurepack.common.item.tools;

import java.util.List;
import java.util.WeakHashMap;

import futurepack.api.FacingUtil;
import futurepack.api.ParentCoords;
import futurepack.common.FPConfig;
import futurepack.common.FuturepackTags;
import futurepack.common.commands.OreSearcher;
import futurepack.depend.api.helper.HelperEntities;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;


public class ItemMindControllMiningHelmet extends ArmorItem
{
	
    public ItemMindControllMiningHelmet(ArmorMaterial mat, EquipmentSlot slots, Item.Properties builder)
	{
		super(mat, slots, builder);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type)
	{
		if(slot== EquipmentSlot.CHEST||slot== EquipmentSlot.FEET || slot== EquipmentSlot.HEAD)
			return "futurepack:textures/models/armor/mindcontroll_mining_layer_1.png";
		else if(slot== EquipmentSlot.LEGS)
			return "futurepack:textures/models/armor/mindcontroll_mining_layer_2.png";
		else
			return super.getArmorTexture(stack, entity, slot, type);
	}
	
	@Override
	protected boolean allowdedIn(CreativeModeTab group) 
	{
		if(FPConfig.IS_RESEARCH_DEBUG.getAsBoolean())
			return super.allowdedIn(group);
		return false;
	}

	@Override
	public InteractionResult interactLivingEntity(ItemStack stack, Player playerIn, LivingEntity target, InteractionHand hand)
	{
		if(target.getItemBySlot(getSlot()).isEmpty())
		{
			target.setItemSlot(getSlot(), stack);
			return InteractionResult.CONSUME;
		}
		return super.interactLivingEntity(stack, playerIn, target, hand);
	}
	
	private static WeakHashMap<Mob, OreSearcher> map = new WeakHashMap<Mob, OreSearcher>();
	
	public static boolean onMobArmorTick(Mob mob)
	{
		Level w = mob.getCommandSenderWorld();
		if(!w.isClientSide)
		{
			ItemStack helm = mob.getItemBySlot(EquipmentSlot.HEAD);
			
			if(!helm.isEmpty() && helm.is(FuturepackTags.MINING_MINDCONTROL))
			{
				final OreSearcher search = map.computeIfAbsent(mob, m -> new OreSearcher(new BlockPos(m.blockPosition())) );
				search.maxrange = 16 * 5;
				
				if(mob.getPassengers().isEmpty())
				{
					List<ParentCoords> ores = search.getOres(w);
					
					if(ores.isEmpty())
					{
						map.remove(mob);
					}
					else
					{
						BlockPos orePos = ores.get(0);
						
						boolean air = false;
						for(Direction dir : FacingUtil.VALUES)
						{
							if(w.isEmptyBlock(orePos.relative(dir)))
							{
								air = true;
								break;
							}
						}
						
						if(air)
						{
							double d = 0;
							((ServerLevel)w).sendParticles(ParticleTypes.NOTE, orePos.getX()+0.5, orePos.getY()+0.5, orePos.getZ()+0.5, 1, 0, 0, 0, 0);
							if((d = orePos.distToCenterSqr(mob.position())) < 5)
							{
								BlockState state = w.getBlockState(orePos);
								if(state.isAir())
								{
									ores.remove(0);
								}
								else
								{	
									FallingBlockEntity block = new FallingBlockEntity(w, orePos.getX()+0.5, orePos.getY()+0.5, orePos.getZ()+0.5, state);
									w.addFreshEntity(block);
									block.tick();
									block.startRiding(mob, true);
								}
							}
							else
							{
								HelperEntities.navigateEntity(mob, orePos, false);
							}
						}
						else
						{
							ores.remove(0);
						}
					}
				}
				else
				{
					BlockPos start = search.getStartPos();
					
					if(start.distToCenterSqr(mob.position()) < 5)
					{
						//att start
						mob.getPassengers().stream().filter(p -> p instanceof FallingBlockEntity).forEach(p -> ((FallingBlockEntity)p).time += 100);
						
					}
					else
					{
						HelperEntities.navigateEntity(mob, start, false);
					}
					
				}
				
				return true;
			}
			
			

		}
		
		return false;
	}
	
	
}
