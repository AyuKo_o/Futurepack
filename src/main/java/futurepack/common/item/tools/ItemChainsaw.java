package futurepack.common.item.tools;

import futurepack.world.dimensions.TreeUtils;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.BlockTags;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class ItemChainsaw extends ItemPoweredMineToolBase
{

	public ItemChainsaw(Item.Properties props, Tier tier) 
	{
		super(props, tier, BlockTags.MINEABLE_WITH_AXE);
	}
	
	@Override
	public boolean isMineable(BlockState bs)
	{
		return bs.getMaterial()==Material.WOOD;
	}
    
    @Override
    public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, Player player)
    {
    	Level w = player.getCommandSenderWorld();
    	if(!w.isClientSide)
    	{
	    	if(getNeon(itemstack) > 0)
			{
				BlockPos log = TreeUtils.getFarWoodInHeight(w, pos);
				if(pos.equals(log))
				{
					TreeUtils.selectTree(w, log, Direction.fromYRot(player.getYHeadRot()).getOpposite());
					return true;
				}
			}
    	}
    	return super.onBlockStartBreak(itemstack, pos, player);
    }
    
    @Override
	public boolean canPerformAction(ItemStack stack, net.minecraftforge.common.ToolAction toolAction) 
	{
		return net.minecraftforge.common.ToolActions.DEFAULT_AXE_ACTIONS.contains(toolAction);
	}
}
