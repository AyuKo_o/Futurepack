package futurepack.common.item.tools.compositearmor;

import java.util.List;
import java.util.UUID;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;

import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeModifier.Operation;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.ForgeMod;

public class ItemDungeonArmor extends ItemCompositeArmor 
{
	private static final UUID speed_mod_id = new UUID("dungeon_armor".hashCode(), "speed_mod".hashCode());
	private static final UUID jump_mod_id = new UUID("dungeon_armor".hashCode(), "jump_mod".hashCode());
	private static final UUID damage_mod_id = new UUID("dungeon_armor".hashCode(), "damage_mod".hashCode());
	private static final UUID knockback_mod_id = new UUID("dungeon_armor".hashCode(), "knockback_mod".hashCode());
	
	
	public ItemDungeonArmor(ArmorMaterial materialIn, EquipmentSlot equipmentSlotIn, int slotCount, Properties props) 
	{
		super(materialIn, equipmentSlotIn, slotCount, props);
	}
	
	@Override
	public String getArmorTexture(ItemStack stack, Entity entity, EquipmentSlot slot, String type)
	{
		if(slot== EquipmentSlot.CHEST||slot== EquipmentSlot.FEET || slot== EquipmentSlot.HEAD)
			return "futurepack:textures/models/armor/dungeon_layer_1.png";
		else if(slot== EquipmentSlot.LEGS)
			return "futurepack:textures/models/armor/dungeon_layer_2.png";
		else
			return super.getArmorTexture(stack, entity, slot, type);
	}
	
	@Override
	public void onArmorTick(ItemStack it, Level world, Player player) 
	{
		if(this.slot == EquipmentSlot.HEAD)
		{
			if(CompositeArmorInventory.hasDungeonSetBonus(player) )
			{
				double reachDistance = player.getAttributeValue(ForgeMod.REACH_DISTANCE.get());
				int enemyCount = world.getEntitiesOfClass(Monster.class, player.getBoundingBox().inflate(reachDistance)).size();
				player.getItemBySlot(EquipmentSlot.HEAD).getOrCreateTag().putInt("enemyCount", enemyCount);
				player.getItemBySlot(EquipmentSlot.CHEST).getOrCreateTag().putInt("enemyCount", enemyCount);
				player.getItemBySlot(EquipmentSlot.LEGS).getOrCreateTag().putInt("enemyCount", enemyCount);
				player.getItemBySlot(EquipmentSlot.FEET).getOrCreateTag().putInt("enemyCount", enemyCount);
			}
			else
			{
				player.getItemBySlot(EquipmentSlot.HEAD).getOrCreateTag().putInt("enemyCount", 0);
				player.getItemBySlot(EquipmentSlot.CHEST).getOrCreateTag().putInt("enemyCount", 0);
				player.getItemBySlot(EquipmentSlot.LEGS).getOrCreateTag().putInt("enemyCount", 0);
				player.getItemBySlot(EquipmentSlot.FEET).getOrCreateTag().putInt("enemyCount", 0);
			}
		}
		if(!world.isClientSide && this.slot == EquipmentSlot.LEGS && it.hasTag() && it.getTag().contains("enemyCount"))
		{
			int enemyCount = it.getTag().getInt("enemyCount");
			if(enemyCount>0)
			{
				player.addEffect(new MobEffectInstance(MobEffects.JUMP, 10, (int)Math.sqrt(enemyCount)));
				
			}
		}
		super.onArmorTick(it, world, player);
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getAttributeModifiers(EquipmentSlot equipmentSlot, ItemStack stack) 
	{	
		Multimap<Attribute, AttributeModifier> buffs = HashMultimap.create(super.getAttributeModifiers(equipmentSlot, stack));
		
		if(stack.hasTag() && stack.getTag().contains("enemyCount") && equipmentSlot ==this.slot)
		{
			int enemyCount = stack.getTag().getInt("enemyCount");
			if(enemyCount > 0)
			{
				AttributeModifier mod = null;
				Attribute attr = null;
				switch (this.slot)
				{
				case FEET:
					mod = new AttributeModifier(speed_mod_id, "speed_mod", 0.05 * enemyCount, Operation.MULTIPLY_TOTAL);
					attr = Attributes.MOVEMENT_SPEED;
					break;
				case CHEST:
					attr = Attributes.ATTACK_KNOCKBACK;
					mod = new AttributeModifier(knockback_mod_id, "knockback_mod", 0.5 * enemyCount, Operation.ADDITION);
					break;
				case LEGS:
					break;
				case HEAD:
					attr = Attributes.ATTACK_DAMAGE;
					mod = new AttributeModifier(damage_mod_id, "damage_mod", 1 * enemyCount, Operation.ADDITION);
					break;
				default:
					break;
				}
				if(mod != null && attr != null)
				{
					buffs.put(attr, mod);
				}
				
			}
		}
		
		return buffs;
	}
	
	@Override
	public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) 
	{
		tooltip.add(new TranslatableComponent("futurepack.item.has_set_effect"));
		super.appendHoverText(stack, worldIn, tooltip, flagIn);
	}

}
