package futurepack.common.item.tools.compositearmor;

import futurepack.common.ManagerGleiter;
import futurepack.common.item.IItemColorable;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulParaglider extends ItemModulArmorBase implements IItemColorable
{

	public ItemModulParaglider(Item.Properties props) 
	{
		super(EquipmentSlot.CHEST, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv)
	{
		if(player.fallDistance > 3.0f)
		{
			ManagerGleiter.setGleiterOpen(player, true);
		}
	}
	
	@Override
	public int getColor(ItemStack stack)
    {
		 CompoundTag tag = stack.getTag();

         if (tag != null)
         {
             CompoundTag nbt = tag.getCompound("display");

             if (nbt != null && nbt.contains("color", 3))
             {
                 return nbt.getInt("color");
             }
         }

         return 0x3df9f7;
    }

}
