package futurepack.common.item.tools.compositearmor;

import java.util.UUID;

import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableMultimap.Builder;
import com.google.common.collect.Multimap;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulPanzer extends ItemModulStatChange
{

	private final int damageReduceAmount;
	private final float toughness;
	protected final float knockbackResistance;
	
	public ItemModulPanzer(Properties props, ArmorMaterial materialIn)
	{
		super(null, props);
		this.damageReduceAmount = materialIn.getDefenseForSlot(EquipmentSlot.HEAD);
		this.toughness = materialIn.getToughness();
		this.knockbackResistance = materialIn.getKnockbackResistance();
	}
	
	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory inv)
	{
		
	}
	
	@Override
	public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot slot)
	{
		if(slot == EquipmentSlot.MAINHAND || slot == EquipmentSlot.OFFHAND)
		{
			return ImmutableMultimap.of();
		}
	
		Builder<Attribute, AttributeModifier> builder = ImmutableMultimap.builder();
		UUID uuid = getUUID(slot);
		builder.put(Attributes.ARMOR, new AttributeModifier(uuid, "Armor modifier", (double)this.damageReduceAmount, AttributeModifier.Operation.ADDITION));
		builder.put(Attributes.ARMOR_TOUGHNESS, new AttributeModifier(uuid, "Armor toughness", (double)this.toughness, AttributeModifier.Operation.ADDITION));
		if (this.knockbackResistance > 0)
		{
			builder.put(Attributes.KNOCKBACK_RESISTANCE, new AttributeModifier(uuid, "Armor knockback resistance", (double)this.knockbackResistance, AttributeModifier.Operation.ADDITION));
		}
		return builder.build();
	}

}
