package futurepack.common.item.tools.compositearmor;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public abstract class ItemModulNeonContainer extends ItemModulArmorBase implements IItemNeon
{
	private int defaultMaxNeon;
	
	
	public ItemModulNeonContainer(EquipmentSlot slot, int maxNE, Item.Properties props)
	{
		super(slot, props);
		defaultMaxNeon = maxNE;
	}
	
	@Override
	public int getBarColor(ItemStack stack)
	{
		return Mth.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public int getBarWidth(ItemStack stack)
	{
		return (int) (13 * ( ((double)getNeon(stack) / (double)getMaxNeon(stack))));
	}
	
	@Override
	public boolean isBarVisible(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
	
	@Override
	public boolean isNeonable(ItemStack it)
	{
		return true;
	}

	@Override
	public int getMaxNeon(ItemStack it)
	{
		CompoundTag nbt = it.getTagElement("neon");
		if(nbt==null)
		{
			nbt = constructNBT(it);
			return defaultMaxNeon;
		}
		return nbt.getInt("maxNE");
	}
	
	private CompoundTag constructNBT(ItemStack it)
	{
		CompoundTag nbt = new CompoundTag();
		nbt.putInt("maxNE", ((ItemModulNeonContainer)(it.getItem())).defaultMaxNeon);
		nbt.putInt("ne", 0);
		it.addTagElement("neon", nbt);
		return nbt;
	}
	
	@OnlyIn(Dist.CLIENT)
	@Override
	public void appendHoverText(ItemStack stack, Level w, List<Component> tooltip, TooltipFlag advanced) 
	{
		tooltip.add(HelperItems.getTooltip(stack, this));
		super.appendHoverText(stack, w, tooltip, advanced);
	}
	
	private static final EquipmentSlot slots[] = {EquipmentSlot.CHEST, EquipmentSlot.HEAD, EquipmentSlot.LEGS, EquipmentSlot.FEET};
	
	public void pullEnergy(CompositeArmorInventory armor, ItemStack target)
	{	
		int ne = getNeon(target);
		int max = getMaxNeon(target);
		
		if(ne >= max)
			return;
		
		for(EquipmentSlot e : slots)
		{
			CompositeArmorPart p = armor.getPart(e);
			if(p!=null)
			{
				for(int i=0;i < p.getSlots();i++)
				{
					ItemStack m = p.getStackInSlot(i);
					
					if(m != target && m.getItem() instanceof ItemModulNeonContainer && ((ItemModulNeonContainer)m.getItem()).isEnergyProvider())
					{
						int other_ne = getNeon(m);
						
						if(other_ne > max-ne)
						{
							addNeon(target, max-ne);
							addNeon(m, -(max-ne));
							return;
						}
						else if(other_ne > 0)
						{
							addNeon(target, other_ne);
							addNeon(m, -(other_ne));
							ne += other_ne;
						}
					}
				}
			}	
		}
	}
	
	public void pushEnergy(CompositeArmorInventory armor, ItemStack target)
	{	
		int ne = getNeon(target);

		if(ne <= 0)
			return;
		
		for(EquipmentSlot e : slots)
		{
			CompositeArmorPart p = armor.getPart(e);
			if(p!=null)
			{
				for(int i=0;i < p.getSlots();i++)
				{
					ItemStack m = p.getStackInSlot(i);
					
					if(m != target && m.getItem() instanceof ItemModulNeonContainer && ((ItemModulNeonContainer)m.getItem()).isEnergyConsumer())
					{
						int other_ne = getNeon(m);
						int other_maxne = getMaxNeon(m);
						
						if(other_maxne - other_ne > ne)
						{
							addNeon(m, ne);
							addNeon(target, -ne);
							return;
						}
						else if(other_maxne - other_ne > 0)
						{
							int a = other_maxne - other_ne;
							addNeon(m, a);
							addNeon(target, -(a));
							ne -= a;
						}
					}
				}
			}	
		}
	}
	
	public boolean isEnergyProvider()
	{
		return false;
	}
	
	public boolean isEnergyConsumer()
	{
		return false;
	}
}
