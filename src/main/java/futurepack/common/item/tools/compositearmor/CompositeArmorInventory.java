package futurepack.common.item.tools.compositearmor;

import java.util.function.Predicate;

import javax.annotation.Nullable;

import futurepack.common.FPLog;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.sync.FPGuiHandler;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import futurepack.common.sync.KeyManager.EventFuturepackKey;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class CompositeArmorInventory
{
	public final Player player;
	public final CompositeArmorPart head, chest, legs, feet;	
	
	public CompositeArmorInventory(Player pl)
	{
		player = pl;
		
		head = CompositeArmorPart.getInventory(pl.getInventory().armor.get(3));
		chest = CompositeArmorPart.getInventory(pl.getInventory().armor.get(2));
		legs = CompositeArmorPart.getInventory(pl.getInventory().armor.get(1));
		feet = CompositeArmorPart.getInventory(pl.getInventory().armor.get(0));
	}
	
	@Nullable
	public CompositeArmorPart getPart(EquipmentSlot slot)
	{
		switch (slot)
		{
		case HEAD:return head;
		case CHEST:return chest;
		case LEGS:return legs;
		case FEET:return feet;
		default:return null;
		}
	}

	public void saveAll()
	{
		if(head!=null)
			head.saveInventoryInItem();
		if(chest!=null)
			chest.saveInventoryInItem();
		if(legs!=null)
			legs.saveInventoryInItem();
		if(feet!=null)
			feet.saveInventoryInItem();
	}
	
	@SubscribeEvent
	public static void onCompositeArmorOpened(EventFuturepackKey event)
	{
		if(event.type == EnumKeyTypes.COMPOSITEARMOR)
		{
			CompositeArmorInventory inv = new CompositeArmorInventory(event.player);
			if(inv.chest!=null || inv.head!=null || inv.legs!=null || inv.feet!=null)
			{
				FPGuiHandler.COMPOSITE_ARMOR.openGui(event.player, (Object[])null);
			}
		}
	}
	
	public static boolean hasAirTightSetBonus(LivingEntity liv)
	{		
		Predicate<Item> isCompositeArmor = item -> item instanceof ItemCompositeArmor;
		return hasSetBonus(liv, isCompositeArmor, isCompositeArmor, isCompositeArmor, isCompositeArmor);
	}
	
	public static boolean hasSetBonus(LivingEntity liv, Item head, Item chest, Item legs, Item feet)
	{		
		return hasSetBonus(liv, i -> i==head, i -> i==chest, i -> i==legs, i -> i==feet);
	}
	
	public static boolean hasSetBonus(LivingEntity liv, Predicate<Item> head, Predicate<Item> chest, Predicate<Item> legs, Predicate<Item> feet)
	{		
		try
		{
			if(liv==null)
				return false;
			if(!liv.isAlive())
				return false;
				
			if(!head.test(liv.getItemBySlot(EquipmentSlot.HEAD).getItem()))
				return false;
			if(!chest.test(liv.getItemBySlot(EquipmentSlot.CHEST).getItem()))
				return false;
			if(!legs.test(liv.getItemBySlot(EquipmentSlot.LEGS).getItem()))
				return false;
			if(!feet.test(liv.getItemBySlot(EquipmentSlot.FEET).getItem()))
				return false;
			return true;
		}
		catch(NullPointerException e)
		{
			FPLog.logger.error("%s#getItemStackFromSlot is returning null, this is not allowed! it should be an ItemStack.EMPTY", liv.getClass());
			return false;
		}
	}
	
	

	public static boolean hasDungeonSetBonus(LivingEntity liv) 
	{
		return hasSetBonus(liv, ToolItems.dungeon_helmet, ToolItems.dungeon_chestplate, ToolItems.dungeon_leggings, ToolItems.dungeon_boots);
	}
}
