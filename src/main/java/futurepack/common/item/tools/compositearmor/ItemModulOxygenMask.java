package futurepack.common.item.tools.compositearmor;

import futurepack.api.interfaces.IAirSupply;
import futurepack.world.dimensions.atmosphere.AtmosphereManager;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class ItemModulOxygenMask extends ItemModulOxygenContainer 
{

	public ItemModulOxygenMask(Item.Properties props) 
	{
		super(EquipmentSlot.HEAD, 600, props);
	}

	@Override
	public void onArmorTick(Level world, Player player, ItemStack it, CompositeArmorInventory armor)
	{			
 		int ox = getOxygen(it);
		
		IAirSupply supply = AtmosphereManager.getAirSupplyFromEntity(player);
		if(supply.getAir() >= 300)
		{	
			int filled = addOxygen(it, 6);
			
			if(filled > 0)
				supply.reduceAir(filled);
		}
		else
		{
			/*Acess other moduls*/
			int max_ox = 0;
			int available_ox = 0;
			boolean done = false;
			
			EquipmentSlot slots[] = {EquipmentSlot.CHEST, EquipmentSlot.FEET, EquipmentSlot.LEGS, EquipmentSlot.HEAD};
			for(EquipmentSlot e : slots)
			{
				CompositeArmorPart p = armor.getPart(e);
				if(p!=null)
				{
					for(int i=0;i < p.getSlots();i++)
					{
						ItemStack m = p.getStackInSlot(i);
						
						if(m.getItem() instanceof ItemModulOxygenContainer)
						{
							max_ox += getMaxOxygen(m);
							int amount = getOxygen(m);
							
							if(!done && m!= it && amount > 0)
							{
								done = true;
								if(!world.isClientSide)
									addOxygen(m, -1);
							}
							
							available_ox += amount;			
						}
					}
				}	
			}
			
			if(ox > 0 && !done && !world.isClientSide)
				addOxygen(it, -1);
			
			if(ox > 1)
			{
				supply.addAir(1);
				AtmosphereManager.setAirTanks((float)available_ox/max_ox, player);
			}
			else if(ox == 1)
			{
				AtmosphereManager.setAirTanks(0F, player);
			}
		}
	}
	
}
