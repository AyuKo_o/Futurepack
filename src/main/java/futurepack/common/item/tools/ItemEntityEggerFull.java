package futurepack.common.item.tools;

import java.util.Optional;

import futurepack.common.FPEntitys;
import futurepack.common.entity.throwable.EntityGrenadeBase;
import futurepack.common.item.ItemGreandeBase;
import net.minecraft.core.BlockPos;
import net.minecraft.core.BlockSource;
import net.minecraft.core.Direction;
import net.minecraft.core.Position;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.Vec3;

public class ItemEntityEggerFull extends ItemGreandeBase 
{

	public ItemEntityEggerFull(Properties properties) 
	{
		super(properties, FPEntitys.GRENADE_ENTITY_EGGER::create);
	}

	@Override
	public boolean isFoil(ItemStack it)
	{
		return true;
	}
	
	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Level w = context.getLevel();
		
		if(!w.isClientSide)
		{
			BlockPos pos = context.getClickedPos();
			Direction face = context.getClickedFace();
			Player pl = context.getPlayer();
			ItemStack it = context.getItemInHand();
			
			Optional<Entity> opt = EntityType.create(it.getTag(), w);
			if(opt.isPresent())
			{
				Entity e = opt.orElseThrow(NullPointerException::new);
				e.setPos(pos.relative(face).getX()+0.5,pos.relative(face).getY()+0.1,pos.relative(face).getZ()+0.5);
				e.setUUID(Mth.createInsecureUUID(w.random));
				w.addFreshEntity(e);
				
				pl.setItemInHand( pl.getMainHandItem() == it ? InteractionHand.MAIN_HAND: InteractionHand.OFF_HAND, ItemStack.EMPTY);
				if(!pl.isCreative())
				{
					it = getEmpty();
				}

				ItemEntity item = new ItemEntity(w, pl.getX(), pl.getY(), pl.getZ(), it);
				item.setPickUpDelay(5);
				item.setDeltaMovement(Vec3.ZERO);
				w.addFreshEntity(item);
			}
			return InteractionResult.SUCCESS;
		}
		return InteractionResult.SUCCESS;
	}
	
	private ItemStack getEmpty()
	{
		return new ItemStack(ToolItems.entity_egger);
	}
	
	@Override
	public ItemStack dispense(BlockSource src, ItemStack it, Position pos, Direction enumfacing)
	{
		ItemStack item = it.split(1);
		
		Optional<Entity> opt = EntityType.create(it.getTag(), src.getLevel());
		opt.ifPresent( e -> {
			e.setPos(pos.x(), pos.y(), pos.z());
			src.getLevel().addFreshEntity(e);
			ItemEntity itemegg = new ItemEntity(src.getLevel(), pos.x(), pos.y(), pos.z(), getEmpty());
			src.getLevel().addFreshEntity(itemegg);
		});
		
//		ArrayList<SlotContent> list = new ArrayList<HelperInventory.SlotContent>();
//		list.add(new SlotContent(null, 0, itemegg.getEntityItem(),itemegg));
//		List<SlotContent> done = HelperInventory.putItemsOld(src.getWorld(), src.getBlockPos().up(), EnumFacing.DOWN, list);
//		for(SlotContent slot : done)
//		{
//			slot.remove();
//		}
		return it;
	}
	
	@Override
	protected EntityGrenadeBase getGrande(Level w, ItemStack it, Player pl)
	{
		EntityGrenadeBase base = new EntityGrenadeBase.EnityEggerFullGranade(w, pl, it.getTag());
		return base;
	}
}
