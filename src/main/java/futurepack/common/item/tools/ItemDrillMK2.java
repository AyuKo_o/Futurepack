package futurepack.common.item.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.Predicate;

import futurepack.api.ParentCoords;
import futurepack.api.interfaces.IBlockSelector;
import futurepack.common.FPBlockSelector;
import futurepack.common.FuturepackTags;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.ServerPlayerGameMode;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.Material;

public class ItemDrillMK2 extends ItemDrillMK1
{

	public ItemDrillMK2(Item.Properties props, Tier tier) 
	{
		super(props, tier);
		max_speed = 100F;
	}
	
	@Override
	public int getMaxNeon(ItemStack it) 
	{
		return 25000;
	}
	
	private boolean isRedirecting = false;
	
	@Override
	public boolean onBlockStartBreak(ItemStack itemstack, BlockPos pos, Player player)
	{
		if(!isRedirecting)
		{
			Level w = player.getCommandSenderWorld();
			BlockState state = w.getBlockState(pos);
			if(!w.isClientSide && state.is(FuturepackTags.BLOCK_ORES))
			{
				StateSelector selector = new StateSelector(b -> b==state);
				BlockPos away = getFurthestBlock(selector, w, pos);
				if(!pos.equals(away))
				{
					isRedirecting = true;
					ServerPlayerGameMode im = ((ServerPlayer)player).gameMode;
					im.destroyBlock(away);
					isRedirecting = false;
					
					return true;
				}
			}
		}
		return super.onBlockStartBreak(itemstack, pos, player);
	}

	public static BlockPos getFurthestBlock(StateSelector selector, Level w, BlockPos start)
	{
		FPBlockSelector sel = new FPBlockSelector(w, selector);
		sel.selectBlocks(start);
		Collection<ParentCoords> col = sel.getAllBlocks();
		ArrayList<ParentCoords> list = new ArrayList<ParentCoords>(col);
		list.remove(start);
		if(list.isEmpty())
		{
			return start;
		}
		list.sort(new Comparator<ParentCoords>()
		{
			@Override
			public int compare(ParentCoords o1, ParentCoords o2)
			{
				return Mth.floor(start.distSqr(o2) - start.distSqr(o1));
			}
		});
			
		return list.get(0);
	}
	
	public static class StateSelector implements IBlockSelector
	{
		Predicate<BlockState> isState;
		
		public StateSelector(Predicate<BlockState> isState)
		{
			this.isState = isState;
		}
		
		@Override
		public boolean isValidBlock(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			BlockState state = w.getBlockState(pos);				
			return isState.test(state);
		}

		@Override
		public boolean canContinue(Level w, BlockPos pos, Material m, boolean diagonal, ParentCoords parent)
		{
			return parent.getDepth() < 64;
		}
		
	}
}
