package futurepack.common.item.tools;

import java.util.List;

import futurepack.api.interfaces.IItemNeon;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.TieredItem;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

public abstract class ItemPoweredMineToolBase extends TieredItem implements IItemNeon
{

	protected float max_speed = 10F;
	protected final TagKey<Block> toolTypeTag;
	
	public ItemPoweredMineToolBase(Item.Properties props, Tier tier, TagKey<Block> toolTypeTag) 
	{
		super(tier, props.setNoRepair().defaultDurability(0).stacksTo(1));
		this.toolTypeTag = toolTypeTag;
	}
	
    public abstract boolean isMineable(BlockState bs);

	@Override
	public float getDestroySpeed(ItemStack it, BlockState b)
    {
        return isMineable(b) && getNeon(it)>0 ?max_speed : 1.0F;
    }
	
	@Override
	public boolean isCorrectToolForDrops(BlockState st)
	{
		return isMineable(st) && isCorrectToolForDropsVanilla(st, getTier(), toolTypeTag);
	}
	
	@Override
	public boolean mineBlock(ItemStack it, Level w, BlockState state, BlockPos pos, LivingEntity p_150894_7_)
	{
		if (state.getDestroySpeed(w, pos) > 0.0D && getNeon(it)>0)
		{
			addNeon(it, -10);
		}	
		 
		return true;
	}

	@Override
	public int getMaxNeon(ItemStack it) 
	{
		return 2500;
	}
	
	@Override
	public int getBarColor(ItemStack stack)
	{
		return Mth.hsvToRgb(0.52F, 1.0F, (0.5F + (float)getNeon(stack) / (float)getMaxNeon(stack) * 0.5F));
	}
	
	@Override
	public int getBarWidth(ItemStack stack)
	{
		return (int) (13 * ( ((double)getNeon(stack) / (double)getMaxNeon(stack))));
	}
	
	@Override
	public boolean isBarVisible(ItemStack stack)
	{
		return getNeon(stack) < getMaxNeon(stack);
	}
	
	@Override
	public void appendHoverText(ItemStack it, Level w, List<Component> l, TooltipFlag p_77624_4_) 
	{
		l.add(HelperItems.getTooltip(it, this));
		super.appendHoverText(it, w, l, p_77624_4_);
	}
	
	public boolean isCorrectToolForDropsVanilla(BlockState pBlock, Tier tier, TagKey<Block> toolFilter) 
	{
		if (net.minecraftforge.common.TierSortingRegistry.isTierSorted(tier)) {
			return net.minecraftforge.common.TierSortingRegistry.isCorrectTierForDrops(tier, pBlock) && pBlock.is(toolFilter);
		}
		int i = tier.getLevel();
		if (i < 3 && pBlock.is(BlockTags.NEEDS_DIAMOND_TOOL)) {
			return false;
		} else if (i < 2 && pBlock.is(BlockTags.NEEDS_IRON_TOOL)) {
			return false;
		} else {
			return i < 1 && pBlock.is(BlockTags.NEEDS_STONE_TOOL) ? false : pBlock.is(toolFilter);
		}
	}

}
