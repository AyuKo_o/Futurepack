package futurepack.common.item;

import futurepack.common.block.plants.PlantBlocks;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemNameBlockItem;

public class ItemMendelSeed extends ItemNameBlockItem 
{

//	private Block my_crops;

	public ItemMendelSeed(Item.Properties props) 
	{
		super(PlantBlocks.mendel_berry, props);
//		my_crops = FPBlocks.mendelBerry;
//		setCreativeTab(FPMain.tab_items);
	}
	
//    @Override
//    public PlantType getPlantType(IBlockReader world, BlockPos pos) 
//    {
//    	return PlantType.Desert;
//    }
    
//    @Override
//    public EnumActionResult onItemUse(EntityPlayer pl, World worldIn, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
//    {
//    	ItemStack stack = pl.getHeldItem(hand);
//        net.minecraft.block.state.IBlockState state = worldIn.getBlockState(pos);
//        if (facing == EnumFacing.UP && pl.canPlayerEdit(pos.offset(facing), facing, stack) && state.getBlock().canSustainPlant(state, worldIn, pos, EnumFacing.UP, (IPlantable) this.my_crops) && worldIn.isAirBlock(pos.up()))
//        {
//            worldIn.setBlockState(pos.up(), this.my_crops.getDefaultState());
//            stack.shrink(1);
//            return EnumActionResult.SUCCESS;
//        }
//        else
//        {
//            return EnumActionResult.FAIL;
//        }
//    }
}
