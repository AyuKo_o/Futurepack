package futurepack.common.recipes;

import java.util.function.BiConsumer;
import java.util.function.Function;

import futurepack.api.ItemPredicateBase;
import futurepack.common.recipes.assembly.AssemblyArmorRecipe;
import futurepack.common.recipes.assembly.AssemblyDualRecipe;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.centrifuge.ZentrifugeRecipe;
import futurepack.common.recipes.crushing.CrushingRecipe;
import futurepack.common.recipes.industrialfurnace.IndNeonRecipe;
import futurepack.common.recipes.industrialfurnace.IndRecipe;
import futurepack.common.recipes.recycler.RecyclerAnalyzerRecipe;
import futurepack.common.recipes.recycler.RecyclerLaserCutterRecipe;
import futurepack.common.recipes.recycler.RecyclerShredderRecipe;
import futurepack.common.recipes.recycler.RecyclerTimeManipulatorRecipe;
import futurepack.common.spaceships.PlanetBase;
import futurepack.depend.api.ItemNBTPredicate;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.ListPredicate;
import futurepack.depend.api.NullPredicate;
import futurepack.depend.api.VanillaTagPredicate;
import net.minecraft.network.FriendlyByteBuf;

public enum EnumRecipeSync
{
	
	ITEM_NBT_PREDICATE(ItemNBTPredicate.class, ItemNBTPredicate::write, ItemNBTPredicate::read),
	ITEM_PREDICATE(ItemPredicate.class, ItemPredicate::write, ItemPredicate::read),
	NULL_PREDICATE(NullPredicate.class, NullPredicate::write, NullPredicate::read),
	LIST_PREDICATE(ListPredicate.class, ListPredicate::write, ListPredicate::read),
	VANILLA_TAG_PREDICATE(VanillaTagPredicate.class, VanillaTagPredicate::write, VanillaTagPredicate::read),
	
	IND_RECIPE(IndRecipe.class, IndRecipe::write, IndRecipe::read),
	IND_NEON_RECIPE(IndNeonRecipe.class, IndNeonRecipe::write, IndNeonRecipe::read),
	ASSEMBLY_RECIPE(AssemblyRecipe.class, AssemblyRecipe::write, AssemblyRecipe::read),
	ASSEMBLY_DUAL_RECIPE(AssemblyDualRecipe.class, AssemblyDualRecipe::write, AssemblyDualRecipe::read),
	ASSEMBLY_ARMOR_RECIPE(AssemblyArmorRecipe.class, AssemblyArmorRecipe::write, AssemblyArmorRecipe::read),
	CENTRIFUGE_RECIPE(ZentrifugeRecipe.class, ZentrifugeRecipe::write, ZentrifugeRecipe::read),
	CRUSHING_RECIPE(CrushingRecipe.class, CrushingRecipe::write, CrushingRecipe::read),
	RECYCLER_ANALYSER_RECIPE(RecyclerAnalyzerRecipe.class, RecyclerAnalyzerRecipe::write, RecyclerAnalyzerRecipe::read),
	RECYCLER_LASER_CUTTER_RECIPE(RecyclerLaserCutterRecipe.class, RecyclerLaserCutterRecipe::write, RecyclerLaserCutterRecipe::read),
	RECYCLER_SHREDDER_RECIPE(RecyclerShredderRecipe.class, RecyclerShredderRecipe::write,RecyclerShredderRecipe::read),
	RECYCLER_TIME_MANIPULATOR_RECIPE(RecyclerTimeManipulatorRecipe.class, RecyclerTimeManipulatorRecipe::write, RecyclerTimeManipulatorRecipe::read),
	
	PLANET_BASE(PlanetBase.class, PlanetBase::write, PlanetBase::read),
	;
	
	private final BiConsumer<?, FriendlyByteBuf> write;
	private final Function<FriendlyByteBuf, ?> read;
	private final Class<?> type;
	
	private <T> EnumRecipeSync(Class<T> type, BiConsumer<T, FriendlyByteBuf> write, Function<FriendlyByteBuf, T> read) 
	{
		this.type = type;
		this.write = write;
		this.read = read;
	}

	@SuppressWarnings("unchecked")
	public void writeToBuffer(Object obj, FriendlyByteBuf buf)
	{
		((BiConsumer<Object, FriendlyByteBuf>)write).accept(obj, buf);
	}
	
	public Object readFromBuffer(FriendlyByteBuf buf)
	{
		return read.apply(buf);
	}

	private static final EnumRecipeSync[] VALUES = EnumRecipeSync.values();
	
	public static void writeUnknown(Object b, FriendlyByteBuf buf) 
	{
		Class t = b.getClass();
		for(EnumRecipeSync rs : VALUES)
		{
			if(rs.type == t)
			{
				buf.writeVarInt(rs.ordinal());
				rs.writeToBuffer(b, buf);
				return;
			}
		}
		throw new IllegalArgumentException("Object " + b + "of class " + t + " is not supported");
	}
	
	public static Object readUnknown(FriendlyByteBuf buf)
	{
		return VALUES[buf.readVarInt()].readFromBuffer(buf);
	}
	
	public static void writePredicates(ItemPredicateBase[] preds, FriendlyByteBuf buf)
	{
		buf.writeVarInt(preds.length);
		for(ItemPredicateBase b : preds)
			EnumRecipeSync.writeUnknown(b, buf);
	}
	
	public static ItemPredicateBase[] readPredicates(FriendlyByteBuf buf)
	{
		ItemPredicateBase[] arr = new ItemPredicateBase[buf.readVarInt()];
		for(int i=0;i<arr.length;i++)
		{
			arr[i] = (ItemPredicateBase) readUnknown(buf);
		}
		return arr;
	}
}
