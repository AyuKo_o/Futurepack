package futurepack.common.recipes.crafting;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;

import futurepack.api.Constants;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.DyeableLeatherItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.CustomRecipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.crafting.CraftingHelper;

public class RecipeDyeing extends CustomRecipe
{
	private final ItemStack colorable;
	
	public RecipeDyeing(ResourceLocation res, ItemStack stack)
	{
		super(res);
		colorable = stack;
	}

	/**
	 * Used to check if a recipe matches current crafting inventory
	 */
	@Override
	public boolean matches(CraftingContainer inv, Level worldIn)
	{
		if (!(inv instanceof CraftingContainer))
		{
			return false;
		}
		else
		{
			ItemStack itemstack = ItemStack.EMPTY;
			List<ItemStack> list = Lists.newArrayList();

			for (int i = 0; i < inv.getContainerSize(); ++i)
			{
				ItemStack itemstack1 = inv.getItem(i);
				if (!itemstack1.isEmpty()) {
					if (itemstack1.sameItem(colorable))
					{
						if (!itemstack.isEmpty()) {
							return false;
						}

						itemstack = itemstack1;
					} else {
						if (!itemstack1.is(net.minecraftforge.common.Tags.Items.DYES)) {
							return false;
						}

						list.add(itemstack1);
					}
				}
			}

			return !itemstack.isEmpty() && !list.isEmpty();
		}
	}

	/**
	 * Returns an Item that is the result of this recipe
	 */
	@Override
	public ItemStack assemble(CraftingContainer inv) {
		ItemStack itemstack = ItemStack.EMPTY;
		int[] aint = new int[3];
		int i = 0;
		int j = 0;
		DyeableLeatherItem itemarmordyeable = null;

		for (int k = 0; k < inv.getContainerSize(); ++k) {
			ItemStack itemstack1 = inv.getItem(k);
			if (!itemstack1.isEmpty()) {
				Item item = itemstack1.getItem();
				if (item instanceof DyeableLeatherItem) {
					itemarmordyeable = (DyeableLeatherItem) item;
					if (!itemstack.isEmpty()) {
						return ItemStack.EMPTY;
					}

					itemstack = itemstack1.copy();
					itemstack.setCount(1);
					if (itemarmordyeable.hasCustomColor(itemstack1)) {
						int l = itemarmordyeable.getColor(itemstack);
						float f = (l >> 16 & 255) / 255.0F;
						float f1 = (l >> 8 & 255) / 255.0F;
						float f2 = (l & 255) / 255.0F;
						i = (int) (i + Math.max(f, Math.max(f1, f2)) * 255.0F);
						aint[0] = (int) (aint[0] + f * 255.0F);
						aint[1] = (int) (aint[1] + f1 * 255.0F);
						aint[2] = (int) (aint[2] + f2 * 255.0F);
						++j;
					}
				} else {
					DyeColor color = DyeColor.getColor(itemstack1);
					if (color == null) {
						return ItemStack.EMPTY;
					}

					float[] afloat = color.getTextureDiffuseColors();
					int l1 = (int) (afloat[0] * 255.0F);
					int i2 = (int) (afloat[1] * 255.0F);
					int k2 = (int) (afloat[2] * 255.0F);
					i += Math.max(l1, Math.max(i2, k2));
					aint[0] += l1;
					aint[1] += i2;
					aint[2] += k2;
					++j;
				}
			}
		}

		if (itemarmordyeable == null) {
			return ItemStack.EMPTY;
		} else {
			int i1 = aint[0] / j;
			int j1 = aint[1] / j;
			int k1 = aint[2] / j;
			float f3 = (float) i / (float) j;
			float f4 = Math.max(i1, Math.max(j1, k1));
			i1 = (int) (i1 * f3 / f4);
			j1 = (int) (j1 * f3 / f4);
			k1 = (int) (k1 * f3 / f4);
			int j2 = (i1 << 8) + j1;
			j2 = (j2 << 8) + k1;
			itemarmordyeable.setColor(itemstack, j2);
			return itemstack;
		}
	}

	/**
	 * Used to determine if this recipe can fit in a grid of the given width/height
	 */
	@Override
	public boolean canCraftInDimensions(int width, int height) {
		return width * height >= 2;
	}

	public static class Factory implements RecipeSerializer<RecipeDyeing>
	{
		public static final ResourceLocation NAME = new ResourceLocation(Constants.MOD_ID, "dyeing");
		
		@Override
		public RecipeDyeing fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new RecipeDyeing(recipeId, CraftingHelper.getItemStack(json.getAsJsonObject("input"), false));
		}

		@Override
		public RecipeDyeing fromNetwork(ResourceLocation recipeId, FriendlyByteBuf buffer)
		{
			return new RecipeDyeing(recipeId, buffer.readItem());
		}

		@Override
		public void toNetwork(FriendlyByteBuf buffer, RecipeDyeing recipe)
		{
			buffer.writeItem(recipe.colorable);
		}

		@Override
		public ResourceLocation getRegistryName() 
		{
			return NAME;
		}

		@Override
		public Class<RecipeSerializer<?>> getRegistryType() 
		{
			return null;//fuck you java
		}

		@Override
		public RecipeSerializer<?> setRegistryName(ResourceLocation name) 
		{
			return this;
		}
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return FPSerializers.DYING;
	}
}
