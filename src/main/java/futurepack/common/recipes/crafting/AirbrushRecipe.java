package futurepack.common.recipes.crafting;

import com.google.gson.JsonObject;

import futurepack.api.Constants;
import futurepack.common.item.misc.ItemLackTank;
import futurepack.common.item.tools.ItemAirBrush;
import futurepack.common.recipes.airbrush.AirbrushRegistry;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.ShapelessRecipe;

public class AirbrushRecipe extends ShapelessRecipeWithResearch
{
	public AirbrushRecipe(ResourceLocation id, String group, ItemStack result, NonNullList<Ingredient> inputs)
	{
		super(id, group, result, inputs);
	}

	public AirbrushRecipe(ShapelessRecipe base)
	{
		this(base.getId(), base.getGroup(), base.getResultItem(), base.getIngredients());
	}

	@Override
	public NonNullList<ItemStack> getRemainingItems(CraftingContainer pInv)
	{
		NonNullList<ItemStack> list = NonNullList.withSize(pInv.getContainerSize(), ItemStack.EMPTY);
		for(int i=0;i<pInv.getContainerSize();i++)
		{
			ItemStack it = pInv.getItem(i);
			if(it.getItem() instanceof ItemAirBrush)
			{
				list.set(i, ItemAirBrush.getItem(it.getTag()));
				break;
			}
		}
		return list;
	}

	@Override
	public ItemStack assemble(CraftingContainer pInv)
	{
		ItemStack airbrush = null;
		ItemStack other = null;
		for(int i=0;i<pInv.getContainerSize();i++)
		{
			ItemStack it = pInv.getItem(i);
			if(airbrush==null && it.getItem() instanceof ItemAirBrush)
			{
				airbrush = it;
			}
			else if(other == null && !it.isEmpty())
			{
				other = it;
			}
		}

		ItemStack result =  super.assemble(pInv);

		if(airbrush!=null && result.getItem() instanceof ItemAirBrush)
		{
			if(other!=null)
			{
//				ItemStack inside = ItemAirBrush.getItem(airbrush.getTag());

				CompoundTag nbt = result.getOrCreateTag().copy();
				ItemAirBrush.setItem(nbt, other.copy().split(1));
				result.setTag(nbt);
			}
			else
				result.setTag(airbrush.getOrCreateTag().copy());
		}

		return result;
	}

	public static class Factory extends ShapelessRecipe.Serializer
	{
		public static final ResourceLocation NAME = new ResourceLocation(Constants.MOD_ID, "airbrush");

		@Override
		public AirbrushRecipe fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new AirbrushRecipe(super.fromJson(recipeId, json));
		}

		@Override
		public AirbrushRecipe fromNetwork(ResourceLocation recipeId, FriendlyByteBuf buffer)
		{
			return new AirbrushRecipe(super.fromNetwork(recipeId, buffer));
		}
	}

	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return FPSerializers.AIRBRUSH;
	}
}
