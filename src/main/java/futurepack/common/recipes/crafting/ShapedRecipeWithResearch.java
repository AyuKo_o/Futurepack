package futurepack.common.recipes.crafting;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import com.google.gson.JsonObject;
import com.mojang.datafixers.util.Pair;

import futurepack.api.Constants;
import futurepack.common.FPLog;
import futurepack.common.research.CustomPlayerData;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.inventory.CraftingMenu;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.ShapedRecipe;
import net.minecraft.world.level.Level;
import net.minecraftforge.common.ForgeHooks;

public class ShapedRecipeWithResearch extends ShapedRecipe
{
	
	public ShapedRecipeWithResearch(ResourceLocation res, String groupIn, int w, int h, NonNullList<Ingredient> inputs, ItemStack result)
	{
		super(res, groupIn, w, h, inputs, result);
	}
	
	public ShapedRecipeWithResearch(ShapedRecipe base)
	{
		this(base.getId(), base.getGroup(), base.getWidth(), base.getHeight(), base.getIngredients(), base.getResultItem());
	}
	
	private static Field f_uuid = null;
	private static boolean noFieldError = false;
	
	@Override
	public boolean matches(CraftingContainer inv, Level world)
	{
		if(inv instanceof InventoryCraftingForResearch)
		{
			InventoryCraftingForResearch research = (InventoryCraftingForResearch) inv;
			
			if(isUseable(research.getUser(), this.getResultItem(), world))
			{
				return super.matches(inv, world);	
			}				
		}
		else
		{
			String clsName = inv.getClass().getName();
//			System.out.println(clsName);
			if(clsName.startsWith("com.refinedmods.refinedstorage") && !noFieldError)
			{
				
				String s = "com.refinedmods.refinedstorage.apiimpl.autocrafting.CraftingPattern$DummyCraftingInventory";
				
				if(clsName.equals(s))
				{
					try
					{
						if(f_uuid==null)
						{
							f_uuid = inv.getClass().getDeclaredField("requester");
							f_uuid.setAccessible(true);
						}	
						UUID requester = (UUID) f_uuid.get(inv);
					
						if(isUseable(requester, this.getResultItem(), world))
						{
							return super.matches(inv, world);	
						}
					}
					catch (NoSuchFieldException e)
					{
						noFieldError = true;
					}
					catch(Exception e)
					{
						FPLog.logger.error("Exception in while getting Player from CraftingInventory");
						FPLog.logger.throwing(org.apache.logging.log4j.Level.ERROR, e);
					}
				}
			}	
			
			Pair<Player, Boolean> pl = getPlayerFromWorkbench(inv);
			if(pl!=null)
			{
				if(pl.getSecond() || (pl.getFirst()!=null && isUseable(pl.getFirst(), this.getResultItem(), world)))
				{
					return super.matches(inv, world);	
				}				
			}
		}
		return false;
	}
	
	public boolean isLocalResearched()
	{
		return isUseable(null, this.getResultItem(), null);
	}
	
	public static boolean isUseable(Object id, ItemStack it, Level w)
	{
		if(id instanceof Player)
		{
			CustomPlayerData data = CustomPlayerData.getDataFromPlayer((Player) id);
			return data.canProduce(it);
		}
		else if(w instanceof ServerLevel)
		{
			CustomPlayerData data = CustomPlayerData.getDataFromUUID((UUID)id, w.getServer());
			return data.canProduce(it);
		}
		else
		{
			CustomPlayerData data = CustomPlayerData.createForLocalPlayer();
			return data.canProduce(it);
		}
	}
	
	private static Field f_container = null;
	private static Map<Class<? extends AbstractContainerMenu>, Function<AbstractContainerMenu, Player>> map = new HashMap<>();
	
	private static final Function<AbstractContainerMenu, Player> NULL_FCT = c -> null;//can not determine a player
	
	
	
	static
	{
		Field[] fields = CraftingContainer.class.getDeclaredFields();
		for(Field f : fields)
		{
			if(f.getType() == AbstractContainerMenu.class)
			{
				f_container = f;
				f_container.setAccessible(true);
			}
		}
		map.computeIfAbsent(CraftingMenu.class, ShapedRecipeWithResearch::getPlayerFromImpl);
	}
	
	
	protected static Pair<Player,Boolean> getPlayerFromWorkbench(Container inv)
	{
		
		try
		{
			AbstractContainerMenu c = (AbstractContainerMenu) f_container.get(inv);
			if(c!=null)
			{
				Class<? extends AbstractContainerMenu> cls = c.getClass();
				
				if("appeng.menu.NullMenu".equals(cls.getName()) || "appeng.menu.AutocraftingMenu".equals(cls.getName()))//so autocrafting for 
				{
					return new Pair(ForgeHooks.getCraftingPlayer(), true);
				}
				Function<AbstractContainerMenu, Player> fct = map.computeIfAbsent(cls, ShapedRecipeWithResearch::getPlayerFromImpl);
				if(fct!=null && fct != NULL_FCT)
				{
					return new Pair(fct.apply(c), false);
				}
			}
		}
		catch(Exception e)
		{
			FPLog.logger.error("Exception while getting Player from InventoryCrafting");
			FPLog.logger.throwing(org.apache.logging.log4j.Level.ERROR, e);	
		}
		return new Pair(ForgeHooks.getCraftingPlayer(), false);
	}
	
	@Nullable
	private static Function<AbstractContainerMenu, Player> getPlayerFromImpl(Class<? extends AbstractContainerMenu> cls)
	{
		if(cls==null)
			return NULL_FCT;
		if(cls == AbstractContainerMenu.class)
			return NULL_FCT;
		
		Stream<Field> st =  Arrays.stream(cls.getDeclaredFields()).filter(f -> Player.class.isAssignableFrom(f.getType())).map(f -> {f.setAccessible(true);return f;});
		final Field[] players = st.toArray(Field[]::new);
		if(players.length == 1)
		{
			FPLog.logger.info("Added %s to allowed Research-Crafting containers (Player:%s)", cls, players[0]);
			return c -> {
				try 
				{
					return (Player) players[0].get(c);
				}
				catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				return null;
			};
		}
		else if(players.length > 1)
		{
			FPLog.logger.warn("Unable to determine player for %s (Examined Fields:%s)", cls, Arrays.toString(players));
			return NULL_FCT;//stop if there are too many possibilities
		}
		else if(players.length == 0)
		{
			FPLog.logger.warn("Unable to determine player for %s (Examined Fields:%s)", cls, Arrays.toString(players));
			
			st =  Arrays.stream(cls.getDeclaredFields()).filter(f -> Inventory.class.isAssignableFrom(f.getType())).map(f -> {f.setAccessible(true);return f;});
			final Field[] inventoreis = st.toArray(Field[]::new);
			if(inventoreis.length==1)
			{
				FPLog.logger.info("Added %s to allowed Research-Crafting containers (Player:%s)", cls, inventoreis[0]);
				return c -> 
				{
					try 
					{
						return ((Inventory) inventoreis[0].get(c)).player;
					}
					catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
					return null;
				};
			}
			else if(inventoreis.length > 1)
			{
				FPLog.logger.warn("Unable to determine player for %s (Examined Fields:%s)", cls, Arrays.toString(inventoreis));
				return NULL_FCT;//stop if there are too many possibilities
			}
		}
		Class<?> parent = cls.getSuperclass();
		if(parent!=null && parent!=AbstractContainerMenu.class)
			return getPlayerFromImpl((Class<? extends AbstractContainerMenu>) parent);
		
		return NULL_FCT;
	}
	
	public static class Factory extends ShapedRecipe.Serializer
	{
		public static final ResourceLocation NAME = new ResourceLocation(Constants.MOD_ID, "research_shaped");
	      
		@Override
		public ShapedRecipeWithResearch fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new ShapedRecipeWithResearch(super.fromJson(recipeId, json));
		} 

		@Override
		public ShapedRecipeWithResearch fromNetwork(ResourceLocation recipeId, FriendlyByteBuf buffer) 
		{
			return new ShapedRecipeWithResearch(super.fromNetwork(recipeId, buffer));
		}
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return FPSerializers.RESEARCH_SHAPED;
	}
	
}
