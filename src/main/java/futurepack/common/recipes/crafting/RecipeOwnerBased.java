package futurepack.common.recipes.crafting;

import java.util.UUID;

import com.google.gson.JsonObject;
import com.mojang.datafixers.util.Pair;

import futurepack.api.Constants;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.ShapedRecipe;

public class RecipeOwnerBased extends ShapedRecipeWithResearch
{

	public RecipeOwnerBased(ShapedRecipe base)
	{
		super(base);
	}
	
	@Override
	public ItemStack assemble(CraftingContainer inv)
	{
		ItemStack it = super.assemble(inv);
		UUID player = null;
		int charges = 0;
		
		for(int i=0;i<inv.getContainerSize();i++)
		{
			ItemStack in = inv.getItem(i);
			if(in.getTagElement("owner") != null)
			{
				CompoundTag nbt = in.getTagElement("owner");
				player = nbt.getUUID("creator");
				charges += nbt.getInt("charges");//special case for entity egger
			}
		}
		
		if(player==null)
		{
			if(inv instanceof InventoryCraftingForResearch)
			{
				InventoryCraftingForResearch research = (InventoryCraftingForResearch) inv;
				Object o = research.getUser();
				if(o instanceof UUID)
				{
					player = (UUID) o;
				}
				else if(o instanceof Player)
				{
					player = ((Player) o).getGameProfile().getId();
				}
			}
			else
			{
				Pair<Player, Boolean> pl = getPlayerFromWorkbench(inv);
				if(pl!=null && pl.getFirst()!=null)
				{
						player = pl.getFirst().getGameProfile().getId();		
				}
			}
		}
		charges++;
		
		if(player!=null)
		{
			CompoundTag nbt = it.getOrCreateTagElement("owner");
			nbt.putUUID("creator", player);
			nbt.putInt("charges", charges);
		}
		return it;
	}
	
	public static class Factory extends ShapedRecipe.Serializer 
	{
		public static final ResourceLocation NAME = new ResourceLocation(Constants.MOD_ID, "owner_shaped");

		@Override
		public RecipeOwnerBased fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new RecipeOwnerBased(super.fromJson(recipeId, json));
		} 

		@Override
		public RecipeOwnerBased fromNetwork(ResourceLocation recipeId, FriendlyByteBuf buffer) 
		{
			return new RecipeOwnerBased(super.fromNetwork(recipeId, buffer));
		}
	}
	
	@Override
	public RecipeSerializer<?> getSerializer() 
	{
		return FPSerializers.OWNER_SHAPED;
	}
}
