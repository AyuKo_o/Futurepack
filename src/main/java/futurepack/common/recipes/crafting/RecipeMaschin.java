package futurepack.common.recipes.crafting;

import java.util.function.Predicate;

import com.google.gson.JsonObject;

import futurepack.api.Constants;
import futurepack.common.block.modification.ModifiableBlocks;
import futurepack.common.block.modification.TileEntityElektroMagnet;
import futurepack.common.block.modification.TileEntityModificationBase;
import futurepack.common.block.modification.TileEntityModulT1Calculation;
import futurepack.common.item.CraftingItems;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.ShapedRecipe;

public class RecipeMaschin extends ShapedRecipeWithResearch
{
	public static final Predicate<ItemStack> IS_MASCHINEBOARD = new ItemStack(CraftingItems.maschineboard)::sameItem;
	public static final Predicate<ItemStack> IS_DOUBLE_MASCHINEBOARD = new ItemStack(CraftingItems.double_maschineboard)::sameItem;
	
	public RecipeMaschin(ShapedRecipe base)
    {
		super(base);
    }
	
	@Override
	public ItemStack assemble(CraftingContainer inv) 
	{
		ItemStack it = super.assemble(inv);
		if(IS_MASCHINEBOARD.test(it))
		{
			TileEntityModificationBase m = new TileEntityElektroMagnet(new BlockPos(0,0,0), ModifiableBlocks.electro_magnet.defaultBlockState());
			ItemStack ram = inv.getItem(1).copy(); //row & column : 1, 0
			ram.setCount(1);
			ItemStack core = inv.getItem(4).copy(); //row & column : 1, 1
			core.setCount(1);
			ItemStack chip = inv.getItem(7).copy(); //row & column : 1, 2
			chip.setCount(1);
			m.getInventory().setItem(0, chip);
			m.getInventory().setItem(9, core);
			m.getInventory().setItem(10, ram);
			CompoundTag nbt = m.saveWithFullMetadata();
			nbt.remove("id");
			if(!it.hasTag())
				it.setTag(new CompoundTag());
			
			it.getTag().put("tile", nbt);
		}
		else if(IS_DOUBLE_MASCHINEBOARD.test(it))
		{
			TileEntityModulT1Calculation m = new TileEntityModulT1Calculation(new BlockPos(0,0,0), ModifiableBlocks.electro_magnet.defaultBlockState());
			ItemStack ram = inv.getItem(7).copy();// row & column : 1,2
			ram.setCount(1);
			ItemStack core1 = inv.getItem(0).copy();// row & column : 0,0
			ItemStack core2 = inv.getItem(2).copy();// row & column : 2,0
			core1.setCount(1);
			core2.setCount(1);
			ItemStack chip = inv.getItem(4).copy();// row & column : 1,1
			chip.setCount(1);
			m.getInventory().setItem(0, chip);
			m.getInventory().setItem(3, core1);
			m.getInventory().setItem(4, core2);
			m.getInventory().setItem(5, ram);
			CompoundTag nbt = m.saveWithFullMetadata();
			nbt.remove("id");
			if(!it.hasTag())
				it.setTag(new CompoundTag());
			
			it.getTag().put("tile", nbt);
		}
		else
		{
			ItemStack board = null;
			for(int i=0;i<inv.getContainerSize();i++)
			{
				ItemStack c = inv.getItem(i);
				if(c!=null && c.hasTag() && c.getTag().contains("tile"))
				{
					if(board==null)
						board = c;
					else
					{
						CompoundTag nbt = board.getTag().getCompound("tile");
						CompoundTag tag = c.getTag().getCompound("tile");
						nbt.merge(tag);
						board.getTag().put("tile", nbt);
					}
				}
			}
			if(board != null)
			{
				if(board.getTag().contains("tile"))
				{
					board.getTag().getCompound("tile").remove("logisticFaces");
				}
				it.setTag(board.getTag().copy());
			}
			
		}
		return it;
	}
	
	public static class Factory extends ShapedRecipe.Serializer 
	{
		public static final ResourceLocation NAME = new ResourceLocation(Constants.MOD_ID, "maschin_shaped");
	      
		@Override
		public RecipeMaschin fromJson(ResourceLocation recipeId, JsonObject json)
		{
			return new RecipeMaschin(super.fromJson(recipeId, json));
		} 

		@Override
		public RecipeMaschin fromNetwork(ResourceLocation recipeId, FriendlyByteBuf buffer) 
		{
			return new RecipeMaschin(super.fromNetwork(recipeId, buffer));
		}
	}
	
	@Override
	public RecipeSerializer<?> getSerializer()
	{
		return FPSerializers.MASCHIN_SHAPED;
	}
}
