package futurepack.common.recipes.recycler;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import futurepack.api.ItemPredicateBase;
import futurepack.common.AsyncTaskManager;
import futurepack.common.FPConfig;
import futurepack.common.FPLog;
import futurepack.common.WorldGenRegistry.CrystalCaveInserter;
import futurepack.common.conditions.ConditionRegistry;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.helper.HelperItems;
import futurepack.depend.api.helper.HelperJSON;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.extensions.jei.FuturepackUids;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.TieredItem;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.registries.ForgeRegistries;

public class FPRecyclerTimeManipulatorManager implements ISyncedRecipeManager<RecyclerTimeManipulatorRecipe>
{
	public static final String NAME = "recycler_time";

	public static FPRecyclerTimeManipulatorManager instance = new FPRecyclerTimeManipulatorManager();
	static
	{
		instance.asyncInit = true;
	}

	public ArrayList<RecyclerTimeManipulatorRecipe> recipes = new ArrayList<RecyclerTimeManipulatorRecipe>();

	private boolean asyncInit = false;

	private static void ready()
	{
		while(!instance.asyncInit)
		{
			FPLog.logger.warn("FPRecyclerTimeManipulatorManager: Waiting for Init!");
			try
			{
				Thread.sleep(10);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static RecyclerTimeManipulatorRecipe addRecipe(ItemPredicateBase it, ItemStack out, int support, int time)
	{
		ready();
		return instance.addRecyclerRecipe(it, out, support, time);
	}

	public static RecyclerTimeManipulatorRecipe addRecipe(ItemPredicateBase it)
	{
		ready();
		return instance.addRepairRecipe(it);
	}

	private RecyclerTimeManipulatorRecipe addRecyclerRecipe(ItemPredicateBase it, ItemStack out, int support, int time)
	{
		RecyclerTimeManipulatorRecipe z = new RecyclerTimeManipulatorRecipe(it, out, support, time);
		recipes.add(z);
		FPLog.logger.debug(z.toString());
		return z;
	}

	private RecyclerTimeManipulatorRecipe addRepairRecipe(ItemPredicateBase it)
	{
		RecyclerTimeManipulatorRecipe z = new RecyclerTimeManipulatorRecipe(it);
		recipes.add(z);
		FPLog.logger.debug(z.toString());
		return z;
	}

	@Override
	public ArrayList<RecyclerTimeManipulatorRecipe> getRecipes()
	{
		ready();
		return recipes;
	}

	@Override
	public void addRecipe(RecyclerTimeManipulatorRecipe t)
	{
		recipes.add(t);
	}

	@Override
	public ResourceLocation getName()
	{
		return FuturepackUids.RECYCLER;
	}

	public RecyclerTimeManipulatorRecipe getMatchingRecipe(ItemStack in)
	{
		//ready();
		for(RecyclerTimeManipulatorRecipe rec : recipes)
		{
			if(rec.match(in))
			{
				return rec;
			}
		}

		return null;
	}

	public static void init(JsonArray recipes)
	{
		instance = new FPRecyclerTimeManipulatorManager();
		FPLog.logger.info("Setup Recycler-TimeManipulator Recipes");
		for(JsonElement elm : recipes)
		{
			setupObject(elm);
		}

		//TODO: add recipe to turn toasted chips/cores/rams back to useable items; also can undo damage

		AsyncTaskManager.addTask(AsyncTaskManager.RESOURCE_RELOAD, new Runnable()
		{
			@Override
			public void run()
			{
				blacklisted_items = null;

				StreamSupport.stream(ForgeRegistries.ITEMS.spliterator(), true).forEach(join ->
				{
					try
					{
						if(isItemRepairable(join))
						{
							instance.addRepairRecipe(new ItemPredicate(join, 1));
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
				});


				instance.asyncInit = true;
			}
		}, true);
	}

	private static void setupObject(JsonElement o)
	{
		if(o.isJsonObject())
		{
			JsonObject obj = o.getAsJsonObject();

			if(!ConditionRegistry.checkCondition(obj))
				return;

			String type = obj.get("type").getAsString();
			ItemPredicateBase in = HelperJSON.getItemPredicateFromJSON(obj.get("input"));

			if(in.collectAcceptedItems(new ArrayList<>()).isEmpty())
			{
				FPLog.logger.warn("Broken recylcer-time recipe with empty input %s", obj.get("input").toString());
				return;
			}
			if(type.equals("repair"))
			{
				instance.addRepairRecipe(in);
			}
			else
			{
				ItemStack out = null;
				List<ItemStack> outl = HelperJSON.getItemFromJSON(obj.get("output"), false);
				if(!outl.isEmpty())
				{
					int original = outl.get(0).getCount();
					out = HelperOreDict.FuturepackConveter.getChangedItem(outl.get(0)).copy();
					out.setCount(original);
					int time = obj.get("time").getAsInt();
					int support = obj.get("support").getAsInt();

					instance.addRecyclerRecipe(in, out, support, time);
				}

			}
		}
		else
		{
			FPLog.logger.error("Wrong JSON Type for a Recipe:  \"" + o + "\"");
		}
	}

	private static Predicate<Item> blacklisted_items;

	private static synchronized boolean isBlackListed(Item item)
	{
		if(blacklisted_items == null)
		{
			Predicate[] preds = FPConfig.SERVER.bl_recycler_repair.get().stream()
					.map(s -> {
						if(s.startsWith("#"))
						{
							String tag = s.substring(1);
							HelperOreDict.waitForTagsToLoad();
							TagKey<Item> tagkey = HelperOreDict.getOptionalTag(new ResourceLocation(tag));
							return (Predicate<Item>)((Item i) -> i.builtInRegistryHolder().is(tagkey));
						}
						else
						{

							Predicate<String> pred = CrystalCaveInserter.getStringMatcher(s);
							return (Predicate<Item>)((Item i) -> pred.test(HelperItems.getRegistryName(i).toString()));
						}
					})
					.toArray(Predicate[]::new);
			blacklisted_items = (Item e) -> {
				for(Predicate<Item> p : preds)
				{
					if(p.test(e))
						return true;
				}
				return false;
			};
		}

		return blacklisted_items.test(item);


	}

	public static boolean isItemRepairable(Item item)
	{
		if(isBlackListed(item))
			return false;
		else if(item.isRepairable(new ItemStack(item)))
			return true;
		else if(!item.canBeDepleted())
			return false;
		else if(item instanceof TieredItem)
		{
			TieredItem tiered = (TieredItem) item;
			Tier tier = tiered.getTier();
			return !tier.getRepairIngredient().isEmpty();
		}
		else if(item instanceof ArmorItem)
		{
			ArmorItem armored = (ArmorItem) item;
			ArmorMaterial armor = armored.getMaterial();
			return !armor.getRepairIngredient().isEmpty();
		}
		return false;
	}


}
