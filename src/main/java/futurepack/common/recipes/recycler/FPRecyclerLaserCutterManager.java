package futurepack.common.recipes.recycler;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import futurepack.api.ItemPredicateBase;
import futurepack.common.FPLog;
import futurepack.common.conditions.ConditionRegistry;
import futurepack.common.recipes.ISyncedRecipeManager;
import futurepack.common.recipes.assembly.AssemblyRecipe;
import futurepack.common.recipes.assembly.FPAssemblyManager;
import futurepack.depend.api.ItemPredicate;
import futurepack.depend.api.helper.HelperJSON;
import futurepack.depend.api.helper.HelperOreDict;
import futurepack.extensions.jei.FuturepackUids;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;

public class FPRecyclerLaserCutterManager implements ISyncedRecipeManager<RecyclerLaserCutterRecipe>
{
	public static final String NAME = "recycler_laser";
	
	public static FPRecyclerLaserCutterManager instance = new FPRecyclerLaserCutterManager();
	public ArrayList<RecyclerLaserCutterRecipe> recipes = new ArrayList<RecyclerLaserCutterRecipe>();


	public static void addRecipe(ItemPredicateBase it, ItemStack[] singleOut, int time, float ...prob)
	{
		instance.addRecyclerRecipe(it, singleOut, time, prob);
	}
	
	public RecyclerLaserCutterRecipe addRecyclerRecipe(ItemPredicateBase it, ItemStack[] singleOut, int time, float ...prob)
	{
		RecyclerLaserCutterRecipe z = new RecyclerLaserCutterRecipe(it, singleOut, time, prob);
		recipes.add(z);
		FPLog.logger.debug(z.toString());
		return z;
	}
	
	
	public ArrayList<RecyclerLaserCutterRecipe> getRecipesJEI()
	{
		ArrayList<RecyclerLaserCutterRecipe> rr = new ArrayList<RecyclerLaserCutterRecipe>(recipes);
		
		for(AssemblyRecipe ar : FPAssemblyManager.instance.recipes)
		{
			ItemPredicateBase[] preds = ar.getInput();
			ItemStack[] output = new ItemStack[preds.length];
			for(int i=0;i<preds.length;i++)
			{
				output[i] = preds[i].getRepresentItem();
			}
			rr.add(new RecyclerLaserCutterRecipe(new ItemPredicate(ar.getOutput()), output, 50, 0.99f, 0.66f, 0.33f));
		}
		
		return rr;
	}
	
	@Override
	public ArrayList<RecyclerLaserCutterRecipe> getRecipes()
	{
		return recipes;
	}
	
	@Override
	public void addRecipe(RecyclerLaserCutterRecipe t) 
	{
		if(t==null)
			throw new NullPointerException("tried to add null recipe to " + this.getName());
		recipes.add(t);
	}
	
	@Override
	public ResourceLocation getName() 
	{
		return FuturepackUids.RECYCLER;
	}
	
	public RecyclerLaserCutterRecipe getMatchingRecipe(ItemStack in)
	{	
		for(RecyclerLaserCutterRecipe rec : recipes)
		{
			if(rec.match(in))
			{
				return rec;
			}
		}
		
		for(AssemblyRecipe ar : FPAssemblyManager.instance.recipes)
		{
			if(ar.getOutput().sameItem(in))
			{
				ItemPredicateBase[] preds = ar.getInput();
				ItemStack[] output = new ItemStack[preds.length];
				for(int i=0;i<preds.length;i++)
				{
					output[i] = preds[i].getRepresentItem();
				}
				return new RecyclerLaserCutterRecipe(new ItemPredicate(ar.getOutput()), output, 50, 0.99f, 0.66f, 0.33f);
			}
		}
		
		return null;
	}
	
	public static void init(JsonArray recipes)
	{
		instance = new FPRecyclerLaserCutterManager();
		FPLog.logger.info("Setup Recycler-LaserCutter Recipes");
		for(JsonElement elm : recipes)
		{
			setupObject(elm);
		}
	}
	
	private static void setupObject(JsonElement o)
	{
		if(o.isJsonObject())
		{
			JsonObject obj = o.getAsJsonObject();
			
			if(!ConditionRegistry.checkCondition(obj))
				return;
			
			//List<ItemStack> in = HelperJSON.getItemFromJSON(obj.get("input"), false);
			ItemPredicateBase in = HelperJSON.getItemPredicateFromJSON(obj.get("input"));
			
			if(in.collectAcceptedItems(new ArrayList<>()).isEmpty())
			{
				FPLog.logger.warn("Broken recylcer-cutter recipe with empty input %s", obj.get("input").toString());
				return;
			}
			JsonArray arr = obj.get("output").getAsJsonArray();
			List<ItemStack>[] out = new List[arr.size()];
			for(int i=0;i<arr.size();i++)
			{
				out[i] = HelperJSON.getItemFromJSON(arr.get(i), false);
				if(out[i]==null)
					return;
			}
			
			ItemStack[] singleOut = new ItemStack[out.length];
			for(int i=0;i<singleOut.length;i++)
			{
				if(out[i].isEmpty())
				{
					continue;
				}
				int original = out[i].get(0).getCount();
				singleOut[i] = HelperOreDict.FuturepackConveter.getChangedItem(out[i].get(0)).copy();
				singleOut[i].setCount(original);
			}
			
			arr = obj.get("chance").getAsJsonArray();
			float[] probs = new float[arr.size()];

			for(int i=0;i<arr.size();i++)
			{
				probs[i] = arr.get(i).getAsFloat();
			}
			
			int time=20;
			if(obj.has("time"))
			{
				time = obj.get("time").getAsInt();
			}

			instance.addRecyclerRecipe(in, singleOut, time, probs);
			
		}
		else
		{
			FPLog.logger.error("Wrong JSON Type for a Recipe:  \"" + o + "\"");
		}
	}
	

	
}
