package futurepack.common.recipes.recycler;

import java.util.Collections;
import java.util.List;

import futurepack.api.ItemPredicateBase;
import futurepack.api.interfaces.IRecyclerRecipe;
import futurepack.common.item.misc.MiscItems;
import futurepack.common.recipes.EnumRecipeSync;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class RecyclerTimeManipulatorRecipe implements IRecyclerRecipe 
{
	private ItemPredicateBase input;
	private ItemStack output;
	private int support;
	private int time;
	
	@Override
	public ItemStack[] getMaxOutput() 
	{
		ItemStack[] its = new ItemStack[1];
		
		if(output.isEmpty())
			its[0] = input.getRepresentItem();
		else
			its[0] = output;
		
		return its;
	}

	@Override
	public ItemPredicateBase getInput()
	{
		return input;
	}
	
	public RecyclerTimeManipulatorRecipe(ItemPredicateBase i, ItemStack o, int sp, int time)
	{
		input = i;
		support = sp;
		this.time = time;
		output = o==null?ItemStack.EMPTY:o;
	}
	
	public RecyclerTimeManipulatorRecipe(ItemPredicateBase i)
	{
		input = i;
		support = 10;
		time = 10;
		output = ItemStack.EMPTY;
	}
	
	public int getSupport(ItemStack target) 
	{
		return output.isEmpty() ? target.getBaseRepairCost()+1 : support;
	}
	
	public int getMaxProgress() 
	{
		return time;
	}
		
	public boolean match(ItemStack in) 
	{
		if(in!=null)
		{
			return input.apply(in, false);
		}
		return false;
	}

	public boolean isRepairRecipe() 
	{
		return output.isEmpty();
	}
	
	@Override
	public List<ItemStack> getToolItemStacks() 
	{
		return Collections.singletonList(new ItemStack(MiscItems.timemanipulator));
	}

	@Override
	public float[] getChances() 
	{
		return null;
	}	
	
	@Override
	public String getJeiInfoText() 
	{
		return I18n.get("jei.recycler.timemanipulator.needsupport", support);
	}	
	
	@Override
	public String toString() 
	{
		return String.format("TimeManipulator{in:%s,out:%s,time:%s,sp:%s}", input.toString(), output.isEmpty()?"repaired input":output, time, support);
	}
	
	public void write(FriendlyByteBuf buf)
	{
		EnumRecipeSync.writeUnknown(input, buf);
		buf.writeItem(output);
		buf.writeVarInt(support);
		buf.writeVarInt(time);
	}
	
	public static RecyclerTimeManipulatorRecipe read(FriendlyByteBuf buf)
	{
		return new RecyclerTimeManipulatorRecipe((ItemPredicateBase) EnumRecipeSync.readUnknown(buf), buf.readItem(), buf.readVarInt(), buf.readVarInt());
	}
}
