package futurepack.common.recipes.airbrush;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import futurepack.api.Constants;
import futurepack.api.helper.HelperTags;
import futurepack.common.FuturepackTags;
import futurepack.common.block.deco.DecoBlocks;
import futurepack.common.item.misc.ItemLackTank;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.registries.ForgeRegistries;

/**
 * This is only important server side, so no syncing to client
 */
public class AirbrushRegistry
{
	private static Registry REGISTRY;

	public static boolean init()
	{
		REGISTRY = new Registry();

		registerColorRecipes(HelperItems.getRegistryName(DecoBlocks.metal_block.get()), (c) -> new ResourceLocation(Constants.MOD_ID, "color_iron_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_block_ventilation"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_luftung_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_block_gitter"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_gitter_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_slab"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_iron_slab_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_ventilation_slab"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_luftung_slab_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_stair"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_iron_stair_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_stair_gitter"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_gitter_stair_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_fence"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_iron_fence_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_gitter_pane"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_gitter_pane_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_fence_gate"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_iron_fence_gate_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_gitter_slab"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_gitter_slab_" + c.getName()));
		registerColorRecipes(new ResourceLocation(Constants.MOD_ID, "metal_glass"), (c) -> new ResourceLocation(Constants.MOD_ID, "color_glass_" + c.getName()));
		return true;
	}

	private static void registerColorRecipes(ResourceLocation uncolor, Function<DyeColor, ResourceLocation> resources)
	{
		Block uncolored = getBlock(uncolor);
		for(DyeColor col : DyeColor.values())
		{
			Block bl = getBlock(resources.apply(col));
			addColorRecipe(uncolored, bl, col);
			addUncolorRecipe(bl, uncolored);
		}
	}

	private static Block getBlock(ResourceLocation res)
	{
		Block bl = ForgeRegistries.BLOCKS.getValue(res);
		if(bl == null || bl == Blocks.AIR)
			throw new NullPointerException("Block " +res+" is null!");

		return bl;
	}

	public static void addUncolorRecipe(Block input, Block output)
	{
		REGISTRY.uncolor().put(input, output);
	}

	public static void addColorRecipe(Block input, Block output, DyeColor color)
	{
		REGISTRY.color(color).put(input, output);
	}

	@Nullable
	public static Block getColoredBlock(Block input, DyeColor color)
	{
		return REGISTRY.color(color).get(input);
	}

	@Nullable
	public static Block getUncoloredBlock(Block input)
	{
		return REGISTRY.uncolor().get(input);
	}

	public static boolean isUncolorItem(ItemStack it)
	{
		return it.is(FuturepackTags.UNCOLOR);
	}

	private static class Registry
	{
		private Map<Block, Block> uncolor = new IdentityHashMap<>();
		@SuppressWarnings("unchecked")
		private Map<Block, Block>[] color = new IdentityHashMap[DyeColor.values().length];

		public Map<Block, Block> uncolor()
		{
			return uncolor;
		}

		public Map<Block, Block> color(DyeColor color)
		{
			int i = color.ordinal();
			if(this.color[i]!=null)
			{
				return this.color[i];
			}
			else
			{
				this.color[i] = new IdentityHashMap<>();
				return this.color[i];
			}
		}
	}

	public static ItemStack asItem(Block state)
	{
		return new ItemStack(state);
	}

	public static ItemStack asItem(DyeColor color)
	{
		return new ItemStack(ItemLackTank.getItemFromColor(color));
	}

	public static Stream<AirbrushRecipeJEI> asJEIRecipes(Map<Block, Block> recipes, ItemStack airbrush)
	{
		return recipes.entrySet().stream().map(e -> new AirbrushRecipeJEI(airbrush, asItem(e.getKey()), asItem(e.getValue())));
	}

	public static Stream<AirbrushRecipeJEI> asJEIRecipes()
	{
		if(REGISTRY!=null)
		{
			@SuppressWarnings("unchecked")
			Stream<AirbrushRecipeJEI>[] s = new Stream[REGISTRY.color.length];
			for(int i=0;i<REGISTRY.color.length;i++)
			{
				s[i] = asJEIRecipes(REGISTRY.color[i], asItem(DyeColor.byId(i)));
			}
			Stream<AirbrushRecipeJEI> f = Stream.of(s).flatMap(i -> i);
			return Stream.concat(asJEIRecipes(REGISTRY.uncolor, new ItemStack(Blocks.SAND)), f);
		}
		else
		{
			if(HelperTags.areTagsLoaded()) //are tags loaded
			{
				init();
				return asJEIRecipes();
			}
			return null;
		}
	}
}
