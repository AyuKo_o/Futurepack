package futurepack.common;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import javax.annotation.Nullable;

import futurepack.common.entity.CapabilityPlayerData;
import futurepack.common.item.tools.ToolItems;
import futurepack.common.item.tools.compositearmor.CompositeArmorInventory;
import futurepack.common.sync.FPPacketHandler;
import futurepack.common.sync.KeyManager.EnumKeyTypes;
import futurepack.common.sync.KeyManager.EventFuturepackKey;
import futurepack.common.sync.MessageFPData;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.PacketDistributor.TargetPoint;

public class ManagerGleiter
{
	public static List<Function<Player, ItemStack>> gleiterProvider = new ArrayList<>(1);
	
	@SubscribeEvent
	public static void onPlayerTick(TickEvent.PlayerTickEvent event)
	{

		if(isGleiterOpen(event.player))
		{
			ItemStack gleiter = getPlayerGleiter(event.player);
			
			if(gleiter != null)
			{
					
				float speed = gleiter.getItem() == ToolItems.modul_paraglider ? -0.15f : -0.3f;
				
				
				if(event.player.getDeltaMovement().y < speed)
				{
					event.player.setDeltaMovement(event.player.getDeltaMovement().x, speed, event.player.getDeltaMovement().z);
					event.player.fallDistance = 0;
					
					if(gleiter.getItem() == ToolItems.modul_paraglider)
						event.player.flyingSpeed = 0.05f;
					
					if(event.player instanceof ServerPlayer)
					{
						// this should prevent the Fly Kick
						//important: do not sync with client (he will fly away)
						//PlayerCapabilities pc = event.player.capabilities;
						if(!event.player.getAbilities().instabuild)
						{
							event.player.getAbilities().mayfly = true;
							event.player.getAbilities().flying = false;
						}
					}
				}
				else if(event.player.isInWater() || event.player.isOnGround() || event.player.isInLava())
				{
					setGleiterOpen(event.player, false);
					// this should prevent the Fly Kick
					//important: do not sync with client (he will fly away)
					if(!event.player.getAbilities().instabuild)
					{
						event.player.getAbilities().mayfly = false;
						event.player.getAbilities().flying = false;
						//event.player.jumpMovementFactor = 0.02f;
					}
					
				}
			}
			else
			{
				setGleiterOpen(event.player, false);
			}
		}
	}
	
	@SubscribeEvent
	public static void onPlayerLogin(EntityJoinWorldEvent event)
	{
		if(event.getEntity() instanceof ServerPlayer)
		{
			sync((ServerPlayer) event.getEntity());
		}	
	}
	
	@SubscribeEvent
	public static void onKeyPressed(EventFuturepackKey key)
	{
		if(key.type==EnumKeyTypes.GLEITER)
		{
			if(getPlayerGleiter(key.player) != null)
			{
				boolean open = isGleiterOpen(key.player);
				setGleiterOpen(key.player, !open);
			}
		}
		
	}
	
	@Nullable
	public static ItemStack getPlayerGleiter(Player pl)
	{	
		if(pl.getInventory().armor.get(2).getItem() == ToolItems.gleiter)
			return pl.getInventory().armor.get(2);
		
		CompositeArmorInventory armor = new CompositeArmorInventory(pl);
		
		if(armor.chest != null)
		{
			for(int i=0; i < armor.chest.getSlots(); i++)
			{
				if(armor.chest.getStackInSlot(i).getItem() == ToolItems.modul_paraglider)
					return armor.chest.getStackInSlot(i);
			}
		}
		
		for(var func : gleiterProvider)
		{
			ItemStack gleiter = func.apply(pl);
			if(gleiter!=null && !gleiter.isEmpty())
				return gleiter;
		}
		
		return null;
	}
	
	public static boolean isGleiterOpen(Player pl)
	{
		if(pl.isSpectator())
		{
			setGleiterOpen(pl, false);
			return false;
		}
		
		final CompoundTag fp=CapabilityPlayerData.getPlayerdata(pl);
		return fp.getBoolean("gleiter");
	}
	
	public static void setGleiterOpen(Player pl, boolean open)
	{
		final CompoundTag fp=CapabilityPlayerData.getPlayerdata(pl);
		fp.putBoolean("gleiter", open);

		if(pl instanceof ServerPlayer)
		{
			sync((ServerPlayer) pl);
		}
	}
	
	public static void sync(ServerPlayer pl)
	{
		final CompoundTag fp=CapabilityPlayerData.getPlayerdata(pl);
		FPPacketHandler.CHANNEL_FUTUREPACK.send(PacketDistributor.NEAR.with(() -> new TargetPoint(pl.getX(), pl.getY(), pl.getZ(), 50, pl.getCommandSenderWorld().dimension())) , new MessageFPData(fp, pl.getId()));
	}
	
	
}
