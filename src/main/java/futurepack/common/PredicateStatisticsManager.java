package futurepack.common;

import java.util.Arrays;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import futurepack.api.interfaces.IStatisticsManager;
import net.minecraft.world.level.block.Block;

public class PredicateStatisticsManager implements IStatisticsManager 
{

	private final Predicate<Block>[] predicates;
	private final int[] stats;
	
	public PredicateStatisticsManager(Predicate<Block> ...preds) 
	{
		predicates = preds;
		stats = new int[preds.length];
	}
	
	@Override
	public void addBlockToStatistics(Block bl) 
	{
		int p = getPredicatePos(bl);
		if(p>=0)
			stats[p]++;
	}

	@Override
	public int getBlockCount(Block bl) 
	{
		int p = getPredicatePos(bl);
		if(p>=0)
			return stats[p];
		
		throw new IllegalArgumentException("This block is not in any predicates");
	}

	@Nullable
	public  Predicate<Block> getPredicateForBlock(Block bl)
	{
		for(Predicate<Block> p : predicates)
		{
			if(p.test(bl))
				return p;
		}
		return null;
	}
	
	private int getPredicatePos(Block bl)
	{
		for(int i=0;i<predicates.length;i++)
		{
			if(predicates[i].test(bl))
				return i;
		}
		return -1;
	}
	
	public int getCountById(int id)
	{
		return stats[id];
	}

	@Override
	public int getBlockCountFromPredicate(Predicate<Block> pred)
	{
		for(int i=0;i<predicates.length;i++)
		{
			if(pred == predicates[i])
			{
				return stats[i];
			}
		}
		throw new IllegalArgumentException("This Predicate was not registered");
	}
	
	@Override
	public void clear() 
	{
		Arrays.fill(stats, 0);
	}
}
