package futurepack.depend.api.interfaces;


import futurepack.common.gui.inventory.GuiCompositeChest;
import net.minecraft.world.Container;
import net.minecraft.world.entity.player.Inventory;

public abstract interface ITileInventoryProvider 
{
	public abstract Container getInventory();
	
	public default GuiCompositeChest.GenericContainer getInventoryContainer(Inventory inv)
	{
		return new GuiCompositeChest.GenericContainer(inv, this);
	}
	
	public abstract String getGUITitle();
}
