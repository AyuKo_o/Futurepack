package futurepack.depend.api;

import java.util.List;

import futurepack.api.ItemPredicateBase;
import futurepack.depend.api.helper.HelperItems;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.world.item.ItemStack;

public class ItemNBTPredicate extends ItemPredicateBase
{
	private final ItemStack itemstack;
	
	public ItemNBTPredicate(ItemStack stack)
	{
		itemstack = stack.copy();
	}

	@Override
	public boolean apply(ItemStack input) 
	{
		if(ItemStack.isSame(itemstack, input))
		{
			return ItemStack.tagMatches(itemstack, input);
		}
		return false;
	}

	@Override
	public ItemStack getRepresentItem() 
	{
		return itemstack;
	}

	@Override
	public int getMinimalItemCount(ItemStack item) 
	{
		return itemstack.getCount();
	}

	@Override
	public List<ItemStack> collectAcceptedItems(List<ItemStack> list) 
	{
		list.add(itemstack.copy());
		return list;
	}

	@Override
	public String toString() 
	{
		return HelperItems.getRegistryName(itemstack.getItem()) + "x" + itemstack.getCount() + " with " + itemstack.getTag();
	}
	
	public void write(FriendlyByteBuf buf)
	{
		buf.writeItem(itemstack);
	}
	
	public static ItemNBTPredicate read(FriendlyByteBuf buf)
	{
		return new ItemNBTPredicate(buf.readItem());
	}
}
