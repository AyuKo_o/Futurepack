package futurepack.depend.api.helper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.common.base.Predicate;

import futurepack.common.FuturepackTags;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.entity.animal.IronGolem;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.vehicle.AbstractMinecart;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;

public class HelperMagnetism
{
	public static int magnet_range = 5;
	public static float magnet_power = 0.4F;
	
	public static void doMagnetism(Level w, BlockPos pos, int range, float power)
	{
		doMagnetism(w, pos.getX()+0.5, pos.getY()+0.5, pos.getZ()+0.5, range, power);
	}
	
	public static void doMagnetism(Level w, double x, double y, double z, int range, float power)
	{
		final Set<Entity> anti = new HashSet<Entity>();
		List<Entity> list = w.getEntitiesOfClass(Entity.class, new AABB(-range, -range, -range, range, range, range).move(x, y, z), new Predicate<Entity>() //getEntitiesWithinAABBExcludingEntity
		{	
			@Override
			public boolean apply(Entity var1) 
			{
				if(var1 instanceof ItemEntity || var1 instanceof AbstractMinecart || var1 instanceof IronGolem)
				{
					return true;
				}
				if(var1 instanceof LivingEntity)
				{
					LivingEntity base = (LivingEntity) var1;
					EquipmentSlot[] armor = new EquipmentSlot[]{EquipmentSlot.HEAD, EquipmentSlot.CHEST, EquipmentSlot.LEGS, EquipmentSlot.FEET};
					for(EquipmentSlot arm : armor)
					{
						ItemStack it = base.getItemBySlot(arm);
						if(it == null || it.isEmpty())
							continue;
						
						if(it.is(FuturepackTags.MAGNET))
						{
							anti.add(var1);
							return true;
						}
						if(it.is(FuturepackTags.MAGNETIC))
						{
							return true;
						}
					}
				}				
				return false;
			}
		});
//		int meta = getBlockMetadata();
//		int dir = meta / 2 + meta%2;
		for(Entity item : list)
		{
			double d3 = (x) - item.getX() ;
			double d4 = (y) - item.getY() ;
			double d5 = (z) - item.getZ() ;

			d3 = mincheck(d3);
			d4 = mincheck(d4);
			d5 = mincheck(d5);
			
			double dis = Math.sqrt(item.distanceToSqr(x, y, z));
			
			if(item instanceof LivingEntity)
			{
				if(dis > 0.09 * range * range)
					continue;
			}
			
			
			d3 = d3/dis * power;
			d4 = d4/dis * power;
			d5 = d5/dis * power;
			
			if(anti.contains(item))
			{
				d3*=-1;
				d4*=-1;
				d5*=-1;
			}
			if(dis < 1.5)
			{
				item.setDeltaMovement(d3, d4, d5);
			}
			else
			{
				if(Math.abs(item.getDeltaMovement().x) < Math.abs(d3*3))
				{
					d3 = item.getDeltaMovement().x + d3;
				}
				if(Math.abs(item.getDeltaMovement().y) < Math.abs(d4*3))
				{
					d4 = item.getDeltaMovement().y + d4;
				}
				if(Math.abs(item.getDeltaMovement().z) < Math.abs(d5*3))
				{
					d5 = item.getDeltaMovement().z + d5;
				}
				
				item.setDeltaMovement(d3, d4, d5);
			}
			
			item.fallDistance = 0F;
			if(!w.isClientSide)
			{
				item.move(MoverType.SELF ,item.getDeltaMovement());
			}
		}
	}
	
	public static void doMagnetism(Level w, BlockPos pos)
	{
		doMagnetism(w, pos, magnet_range, magnet_power);
	}
	
	private static double mincheck(double d)
	{
		if(Math.abs(d)<0.5)
		{
			d = 0;
		}
		return d;
	}
}
