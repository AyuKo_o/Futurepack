package futurepack.depend.api.helper;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.WeakHashMap;
import java.util.function.LongConsumer;

import javax.annotation.Nullable;

import futurepack.common.FPConfig;
import futurepack.common.entity.EntityDrone;
import it.unimi.dsi.fastutil.longs.AbstractLong2ObjectMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectAVLTreeMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectArrayMap;
import it.unimi.dsi.fastutil.longs.Long2ObjectOpenHashMap;
import it.unimi.dsi.fastutil.longs.LongArraySet;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;
import it.unimi.dsi.fastutil.longs.LongRBTreeSet;
import it.unimi.dsi.fastutil.longs.LongSet;
import net.minecraft.core.BlockPos;
import net.minecraft.core.SectionPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.TicketType;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkStatus;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.saveddata.SavedData;

public class HelperChunks
{
	public static final TicketType<ChunkPos> FUTUREPACK = TicketType.create("futurepack", Comparator.comparingLong(ChunkPos::toLong));
	public static final int TICKET_DISTANCE = 2; //same as force chunk


	private static class TicketKeeper extends SavedData implements Runnable
	{
		private Map<Entity, EntityTicket<Entity>> ticketMap = new WeakHashMap<Entity, HelperChunks.EntityTicket<Entity>>();
		private LongSet chunksToReload = new LongRBTreeSet();

		public final ServerLevel world;

		private TicketKeeper(ServerLevel world)
		{
//			super("futurepack_chunks");
			this.world = world;
		}

		public static TicketKeeper load(ServerLevel world, CompoundTag nbt)
		{
			TicketKeeper tk = new TicketKeeper(world);
			long[] chunks = nbt.getLongArray("chunks");
			synchronized (tk.chunksToReload)
			{
				for(long l : chunks)
				{
					tk.chunksToReload.add(l);
				}
			}
			Thread t = new Thread(tk, "FP Reload Chunks");
			t.setDaemon(true);
			t.start();

			return tk;
		}

		@Override
		public CompoundTag save(CompoundTag nbt)
		{
			LongSet chunks;
			synchronized (chunksToReload)
			{
				chunks = new LongArraySet(chunksToReload.size() + ticketMap.size());
				chunks.addAll(chunksToReload);
			}
			ticketMap.keySet().stream().mapToLong(e -> new ChunkPos(e.blockPosition()).toLong()).forEach(chunks::add);
			nbt.putLongArray("chunks", chunks.toLongArray());
			return nbt;
		}

		@Override
		public void run()
		{
			try  {
				Thread.sleep(50);
			} catch (InterruptedException e)  {
				e.printStackTrace();
			}

			Optional<Long> opt;
			synchronized (chunksToReload)
			{
				opt = chunksToReload.stream().findAny();
			}
			LongSet focedChunksToUnforce = new LongOpenHashSet();

			while(opt.isPresent())
			{
				long pos = opt.get();

				world.getServer().submitAsync(() ->
				{
					if(world.setChunkForced(ChunkPos.getX(pos), ChunkPos.getZ(pos), true))
						focedChunksToUnforce.add(pos);
				}).isDone();
				synchronized (chunksToReload)
				{
					chunksToReload.remove(pos);
				}

				try  {
					Thread.sleep(50);
				} catch (InterruptedException e)  {
					e.printStackTrace();
				}

				synchronized (chunksToReload)
				{
					opt = chunksToReload.stream().findAny();
				}
			}

			try  {
				Thread.sleep(50 * 50);//50 ticks
			} catch (InterruptedException e)  {
				e.printStackTrace();
			}

			for(long pos : focedChunksToUnforce)
			{
				world.getServer().submitAsync(() ->
				{
					ChunkAccess c = world.getChunk(ChunkPos.getX(pos), ChunkPos.getZ(pos), ChunkStatus.FULL, true); //loads the chunk
//					if(c instanceof Chunk)
//					{
//						Chunk ch = (Chunk) c;
//						for(ClassInheritanceMultiMap<Entity> map : ch.getEntityLists())
//						{
//							for(Entity e : map)
//							{
//								world.updateEntity(e);//tickk all entities once
//							}
//						}
//					}
				}).isDone();

				try  {
					Thread.sleep(50 * 10); //hopefully do 10 ticks
				} catch (InterruptedException e)  {
					e.printStackTrace();
				}
			}

			for(long pos : focedChunksToUnforce)
			{
				world.getServer().submitAsync(() ->
				{
					world.setChunkForced(ChunkPos.getX(pos), ChunkPos.getZ(pos), false);
				});
			}
			focedChunksToUnforce.clear();
		}

	}

	public static TicketKeeper getTicketKeeper(ServerLevel world)
	{
		return world.getDataStorage().computeIfAbsent(nbt -> TicketKeeper.load(world, nbt), () -> new TicketKeeper(world), "futurepack_chunks");
	}


	private static class EntityTicket<T extends Entity>
	{
		private LongSet loadedChunks = new LongRBTreeSet();
		private final T owner;
		private final ServerLevel world;

		public EntityTicket(T owner, ServerLevel world)
		{
			this.owner = owner;
			this.world = world;
		}

		public void addChunk(int x, int z)
		{
			addChunk(ChunkPos.asLong(x, z));
		}

		public void addChunk(long l)
		{
			if(!loadedChunks.contains(l))
			{
				ChunkPos pos = new ChunkPos(l);
				world.getChunkSource().addRegionTicket(FUTUREPACK, pos, TICKET_DISTANCE, pos); // register
				loadedChunks.add(l);
			}
		}

		public void removeChunk(int x, int z)
		{
			removeChunk(ChunkPos.asLong(x, z));
		}

		public void removeChunk(long l)
		{
			if(loadedChunks.contains(l))
			{
				ChunkPos pos = new ChunkPos(l);
				world.getChunkSource().removeRegionTicket(FUTUREPACK, pos, TICKET_DISTANCE, pos); // release
				loadedChunks.remove(l);
			}
		}

		public void clear()
		{
			for(long l : loadedChunks)
			{
				ChunkPos pos = new ChunkPos(l);
				world.getChunkSource().removeRegionTicket(FUTUREPACK, pos, 3, pos); // release
			}
			loadedChunks.clear();
		}

		public void update()
		{
			if(!owner.isAlive())
			{
				removeTicket(world, this);
			}
		}

		public void onlyLoad(long[] add)
		{
			LongSet rem = new LongArraySet(loadedChunks.size());

			rem.addAll(loadedChunks);
			for(long l : add)
			{
				rem.remove(l);
				addChunk(l);
			}
			rem.forEach((LongConsumer) this::removeChunk);
		}
	}

	private static void removeTicket(ServerLevel world, EntityTicket<?> ticket)
	{
		ticket.clear();
		TicketKeeper keeper = getTicketKeeper(world);
		if(keeper.ticketMap.remove(ticket.owner)!=null)
		{
			keeper.setDirty(true);
		}
	}

	private static EntityTicket<Entity> getTicket(ServerLevel world, Entity entity)
	{
		TicketKeeper keeper = getTicketKeeper(world);
		return keeper.ticketMap.computeIfAbsent(entity, e ->
		{
			keeper.setDirty(true);
			return new EntityTicket(e, world);
		});
	}

	public static void forceloadChunksForEntity(EntityDrone e)
	{
		if(!FPConfig.SERVER.disableMinerChunkloading.get())
		{
			Level w = e.getCommandSenderWorld();
			if(!w.isClientSide)
			{
				ServerLevel sw = (ServerLevel) w;
				EntityTicket<EntityDrone> ticket = ((EntityTicket)getTicket(sw, e));
				int i = Mth.floor(e.getX())>>4;
				int j = Mth.floor(e.getZ())>>4;

				int ip = Mth.floor(e.getX()+5)>>4;
				int jp = Mth.floor(e.getZ()+5)>>4;
				int im = Mth.floor(e.getX()-5)>>4;
				int jm = Mth.floor(e.getZ()-5)>>4;

				//only 1 chunk is loaded when entity is near the middle of a chunk otherwise at best 4 + home position

//				System.out.println(e);

				ticket.onlyLoad(new long[]
				{
					ChunkPos.asLong(i, j),
					ChunkPos.asLong(ip,jp),
					ChunkPos.asLong(im, jm),
					ChunkPos.asLong(ip, jm),
					ChunkPos.asLong(im, jp),
					new ChunkPos(e.getInventoryPos()).toLong()
				});
			}
		}
	}

	public static void removeTicketIfNeeded(ServerLevel world, Entity entity)
	{
		TicketKeeper keeper = getTicketKeeper(world);
		EntityTicket<Entity> t = keeper.ticketMap.get(entity);
		if(t!=null)
		{
			t.update();
		}
	}

	@Nullable
	public static File getDimensionDir(Level w)
	{
		return ((ServerLevel)w).getDataStorage().dataFolder;
	}

	@Nullable
	public static File getDimensionDir(LevelAccessor w)
	{
		return getDimensionDir((Level)w);
	}
	public static boolean forceChunkIfNeeded(Level w, int x, int z)
	{
		if(!w.isClientSide)
		{
			ServerLevel serv = (ServerLevel) w;
			return serv.setChunkForced(x, z, true);
		}
		return false;
	}

	public static void unforceChunks(Level w, int x, int z)
	{
		if(!w.isClientSide)
		{
			ServerLevel serv = (ServerLevel) w;
			serv.setChunkForced(x, z, false);
		}
	}

	public static void renderUpdate(Level w, BlockPos pos)
	{
//		w.notifyBlockUpdate(pos, Blocks.AIR.getDefaultState(), w.getBlockState(pos), 3);
	}


	public static class BlockCache implements Closeable
	{
		public BlockState getBlockState(Level world, BlockPos pPos)
		{
			if (world.isOutsideBuildHeight(pPos)) {
				return Blocks.VOID_AIR.defaultBlockState();
			} else {
				LevelChunk levelchunk = getChunkCached(world, SectionPos.blockToSectionCoord(pPos.getX()), SectionPos.blockToSectionCoord(pPos.getZ()));
				return levelchunk.getBlockState(pPos);
			}
		}

		AbstractLong2ObjectMap<LevelChunk> pos2chunk = new Long2ObjectOpenHashMap<>(100);

		public LevelChunk getChunkCached(Level world, int pChunkX, int pChunkZ)
		{
			LevelChunk c = pos2chunk.computeIfAbsent(ChunkPos.asLong(pChunkX, pChunkZ), l -> world.getChunk(ChunkPos.getX(l), ChunkPos.getZ(l)));
			return c;
		}

		@Override
		public void close() throws IOException
		{
			System.out.println("HelperChunks.BlockCache.close() cache size " + pos2chunk.size());
			pos2chunk.clear();
		}
	}


}
