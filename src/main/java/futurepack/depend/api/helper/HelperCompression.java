package futurepack.depend.api.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import futurepack.api.StatsManager;

public class HelperCompression
{
	public static String safeCompressString(String s)
	{
		String smaller = StatsManager.compress(s);
		if(smaller !=null && smaller.length() + 3 < s.length())
		{
			return "b64" + smaller;
		}
		else
		{
			return s;
		}
	}

	public static String safeDecompressString(String s)
	{
		if(s.startsWith("b64"))
		{
			try
			{
				ByteArrayInputStream bin = new ByteArrayInputStream(asBytes(s.substring(3)));
				GZIPInputStream in = new GZIPInputStream(Base64.getDecoder().wrap(bin));

				String bigger = new String(in.readAllBytes(), StandardCharsets.UTF_8);
				in.close();
				return bigger;
			} catch (IOException e)
			{
				e.printStackTrace();
			}
			return null;
		}
		else
		{
			return s;
		}
	}

	public static byte[] asBytes(String s)
	{
		return s.getBytes(StandardCharsets.UTF_8);
	}

	public static byte[] compress(byte[] compress)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream(compress.length);
		try
		{
			GZIPOutputStream gout = new GZIPOutputStream(out);
			gout.write(compress);
			gout.close();
			out.close();
			return out.toByteArray();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

	}

	public static byte[] decompress(byte[] decompress)
	{
		ByteArrayInputStream in = new ByteArrayInputStream(decompress);
		ByteArrayOutputStream out = new ByteArrayOutputStream(decompress.length);
		try
		{
			GZIPInputStream gin = new GZIPInputStream(in);
			byte[] buf = new byte[1024 * 1024];
			while(gin.available()>0)
			{
				int read = gin.read(buf, 0 , buf.length);
				if(read > 0)
					out.write(buf, 0, read);
			}
			gin.close();
			out.close();
			return out.toByteArray();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}

	public static String decompressString(byte[] decompress)
	{
		return new String(decompress(decompress), StandardCharsets.UTF_8);
	}

	public static byte[] compressString(String uncompressed)
	{
		return compress(uncompressed.getBytes(StandardCharsets.UTF_8));
	}
}
