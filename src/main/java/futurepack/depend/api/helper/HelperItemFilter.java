package futurepack.depend.api.helper;

import java.util.ArrayList;
import java.util.List;

import futurepack.api.interfaces.filter.IItemFilter;
import futurepack.api.interfaces.filter.IItemFilterFactory;
import futurepack.common.filter.ItemStackFilter;
import futurepack.common.filter.OrGateFilter;
import futurepack.common.filter.ScriptItemFilterFactory;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NumericTag;
import net.minecraft.nbt.Tag;
import net.minecraft.world.item.ItemStack;

public class HelperItemFilter
{
	public static FactoryRegistry registry;
	static
	{
		registry = new FactoryRegistry(ItemStackFilter::new);
		registry.registerFactory(new ScriptItemFilterFactory());
	}

	public static class FactoryRegistry
	{
		private List<IItemFilterFactory> factories;
		private final IItemFilterFactory fallbackFactory;

		public FactoryRegistry(IItemFilterFactory fallbackFactory)
		{
			super();
			this.fallbackFactory = fallbackFactory;
			factories = new ArrayList<IItemFilterFactory>();
		}


		public void registerFactory(IItemFilterFactory fac)
		{
			factories.add(fac);
		}

		public IItemFilter createFilter(ItemStack stack)
		{
			for(IItemFilterFactory fac : factories)
			{
				IItemFilter filter = fac.createFilter(stack);
				if(filter!=null)
					return filter;
			}
			return fallbackFactory.createFilter(stack);
		}

	}

	public static IItemFilter getFilter(ItemStack item)
	{
		return registry.createFilter(item);
	}

	public static OrGateFilter createBasicFilter(ItemStack...it)
	{
		ArrayList<IItemFilter> bases = new ArrayList<IItemFilter>(it.length);
		for(int i=0;i<it.length;i++)
		{
			if(it[i]!=null && !it[i].isEmpty())
				bases.add(getFilter(it[i]));
		}
		return new OrGateFilter(bases.toArray(new IItemFilter[bases.size()]));
	}

	//oredict filter
	//(itemstack filter)
	//(item filter)
	//tools filter (all tools, only weapons ...)
	//Gates, NOT, OR, AND

	public static ItemStack getTranferedItem(ItemStack beforetransfer, ItemStack nottranfered)
	{
		if(nottranfered.isEmpty())
			return beforetransfer;
		if(beforetransfer.isEmpty())
			return ItemStack.EMPTY;
		if(beforetransfer == nottranfered)
			return ItemStack.EMPTY;

		int amount = beforetransfer.getCount() - nottranfered.getCount();
		ItemStack tranfered = beforetransfer.copy();
		tranfered.setCount(amount);
		return tranfered;
	}

	public static void tranfer(boolean simulate, ItemStack beforetransfer, ItemStack nottranfered, IItemFilter filter)
	{
		if(!simulate)
		{
			ItemStack transfered = HelperItemFilter.getTranferedItem(beforetransfer, nottranfered);
			if(!transfered.isEmpty())
				filter.amountTransfered(transfered);
		}
	}

	public static boolean tagMatches(ItemStack pStack, ItemStack pOther)
	{
		 if (pStack.isEmpty() && pOther.isEmpty()) {
	         return true;
	      } else if (!pStack.isEmpty() && !pOther.isEmpty()) {
	         if (pStack.getTag() == null && pOther.getTag() != null) {
	            return false;
	         } else {
	            return (pStack.getTag() == null || areEqual(pStack.getTag(), pOther.getTag())) && pStack.areCapsCompatible(pOther);
	         }
	      } else {
	         return false;
	      }
	}

	public static boolean areEqual(CompoundTag tag, CompoundTag other)
	{
		if(tag == other)
			return true;
		else if(tag == null || other == null)
			return false;
		else if(tag.getAllKeys().size() == other.getAllKeys().size())
		{
			for(String key : tag.getAllKeys())
			{
				Tag t = tag.get(key);
				Tag o = other.get(key);

				if(t.equals(o))
					continue;
				else if(t instanceof NumericTag nt && o instanceof NumericTag no)
				{
					if( nt.getAsDouble() == no.getAsDouble())
						continue;
					else
						return false;
				}
				else if(t instanceof CompoundTag ct && o instanceof CompoundTag co)
				{
					if(areEqual(ct, co))
						continue;
					else
						return false;
				}

				return false;
			}
			return true;
		}
		return false;
	}
}
