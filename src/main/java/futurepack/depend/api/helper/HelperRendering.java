package futurepack.depend.api.helper;

import com.mojang.blaze3d.systems.RenderSystem;

public class HelperRendering
{
	public static void glColor4f(float red, float green, float blue, float alpha)
	{
		 RenderSystem.setShaderColor(red, green, blue, alpha);
	}
	
	public static void glColor3f(float red, float green, float blue)
	{
		glColor4f(red, green, blue, 1F);
	}
	
	public static void enableFog()
	{
//		RenderSystem.enableFog();
	}
	
	public static void disableFog()
	{
//		RenderSystem.disableFog();
	}
	
	public static void disableAlphaTest()
	{
//		RenderSystem.disableAlphaTest();
	}
	
	public static void enableAlphaTest()
	{
//		RenderSystem.enableAlphaTest();
	}
	
	public static void disableLighting()
	{
//		GlStateManager._disableLighting();
	}

	public static void enableLighting()
	{
//        GlStateManager._enableLighting();
	}
}
