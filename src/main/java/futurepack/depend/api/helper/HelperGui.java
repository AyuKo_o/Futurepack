package futurepack.depend.api.helper;

import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.GLFW;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.Lighting;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.Tesselator;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Matrix4f;

import futurepack.api.Constants;
import futurepack.api.capabilities.INeonEnergyStorage;
import futurepack.api.capabilities.ISupportStorage;
import futurepack.api.interfaces.IFluidTankInfo;
import futurepack.api.interfaces.tilentity.ITileXpStorage;
import net.minecraft.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Font;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.gui.screens.inventory.tooltip.ClientTooltipComponent;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.texture.TextureAtlas;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.locale.Language;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.FormattedText;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TextComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.FormattedCharSequence;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.client.event.RenderTooltipEvent;
import net.minecraftforge.client.gui.GuiUtils;
import net.minecraftforge.common.MinecraftForge;

public class HelperGui
{
	private static ResourceLocation resEnergyBarTex = new ResourceLocation(Constants.MOD_ID, "textures/gui/energie_bar.png");

	public static int ZLEVEL_HOVER = 300;
	public static int ZLEVEL_BACKGROUND = 0;
	public static int ZLEVEL_ITEM = 100;
	public static int ZLEVEL_FP_BARS = 10;

	/* Core Helpers */

	public static void drawQuadWithTexture(PoseStack matrixStack, ResourceLocation tex, int x, int y, float u, float v, int width, int height, int widthOnTexture, int heightOnTexture,
			int textureWidth, int textureHeight, int zLevel)
	{
		RenderSystem.setShaderTexture(0, tex);
		GuiComponent.innerBlit(matrixStack, x, x + width, y, y + height, zLevel, widthOnTexture, heightOnTexture, u, v, textureWidth, textureHeight);
	}

	public static void drawHoveringTextFixedString(PoseStack matrixStack, List<String> textLines, int mouseX, int mouseY, int maxTextWidth, Font font)
	{
		drawHoveringTextFixed(matrixStack, ItemStack.EMPTY, textLines.stream().map(TextComponent::new).collect(Collectors.toList()), mouseX, mouseY, maxTextWidth, font);
	}

	public static int drawHoveringTextFixed(PoseStack matrixStack, List<Component> textLines, int mouseX, int mouseY, int maxTextWidth, Font font)
	{
		return drawHoveringTextFixed(matrixStack, ItemStack.EMPTY, textLines, mouseX, mouseY, maxTextWidth, font);
	}

	/**
	 * See GuiUtils Fixed Version to re-enable Depth Test and State Validation
	 *
	 * @param matrixStack  TODO
	 * @param stack        Mainly for Forge Events, ItemStack.Empty is valid
	 * @param textLines    List of Text lines
	 * @param mouseX       Current Mouse Pos
	 * @param mouseY       Current Mouse Pos
	 * @param maxTextWidth Max Width of Tooltip, can be -1 for infinite
	 * @param font         FontRenderer to use
	 * @param screenWidth  Size of the Screen (Drawable area relative to Vanilla
	 *                     "Screen" origin)
	 * @param screenHeight Size of the Screen (Drawable area relative to Vanilla
	 *                     "Screen" origin)
	 *
	 *                     maybe needs to be replaced with Screen.renderTooltip
	 */
	public static int drawHoveringTextFixed(PoseStack mStack, @Nonnull final ItemStack stack, List<? extends FormattedText> textLines, int mouseX, int mouseY, int maxTextWidth, Font font)
	{
		int screenWidth = Minecraft.getInstance().getWindow().getGuiScaledWidth();
		int screenHeight = Minecraft.getInstance().getWindow().getGuiScaledHeight();

		List<ClientTooltipComponent> components = net.minecraftforge.client.ForgeHooksClient.gatherTooltipComponents(stack, textLines, mouseX, screenWidth, screenHeight, font, font);

		int backgroundColor = GuiUtils.DEFAULT_BACKGROUND_COLOR, borderColorStart = GuiUtils.DEFAULT_BORDER_COLOR_START, borderColorEnd = GuiUtils.DEFAULT_BORDER_COLOR_END;

		if (!textLines.isEmpty())
		{
			RenderTooltipEvent.Pre event = new RenderTooltipEvent.Pre(stack, mStack, mouseX, mouseY, screenWidth, screenHeight, font, components);
			if (MinecraftForge.EVENT_BUS.post(event))
				return -1;
			mouseX = event.getX();
			mouseY = event.getY();
			screenWidth = event.getScreenWidth();
			screenHeight = event.getScreenHeight();
			font = event.getFont();

			Lighting.setupFor3DItems(); // change
//            RenderSystem.disableRescaleNormal();
			RenderSystem.enableDepthTest(); // change
			int tooltipTextWidth = 0;

			for (FormattedText textLine : textLines)
			{
				int textLineWidth = font.width(textLine);
				if (textLineWidth > tooltipTextWidth)
					tooltipTextWidth = textLineWidth;
			}

			boolean needsWrap = false;

			int titleLinesCount = 1;
			int tooltipX = mouseX + 12;
			if (tooltipX + tooltipTextWidth + 4 > screenWidth)
			{
				tooltipX = mouseX - 16 - tooltipTextWidth;
				if (tooltipX < 4) // if the tooltip doesn't fit on the screen
				{
					if (mouseX > screenWidth / 2)
						tooltipTextWidth = mouseX - 12 - 8;
					else
						tooltipTextWidth = screenWidth - 16 - mouseX;
					needsWrap = true;
				}
			}

			if (maxTextWidth > 0 && tooltipTextWidth > maxTextWidth)
			{
				tooltipTextWidth = maxTextWidth;
				needsWrap = true;
			}

			if (needsWrap)
			{
				int wrappedTooltipWidth = 0;
				List<FormattedText> wrappedTextLines = new ArrayList<>();
				for (int i = 0; i < textLines.size(); i++)
				{
					FormattedText textLine = textLines.get(i);
					List<FormattedText> wrappedLine = font.getSplitter().splitLines(textLine, tooltipTextWidth, Style.EMPTY);
					if (i == 0)
						titleLinesCount = wrappedLine.size();

					for (FormattedText line : wrappedLine)
					{
						int lineWidth = font.width(line);
						if (lineWidth > wrappedTooltipWidth)
							wrappedTooltipWidth = lineWidth;
						wrappedTextLines.add(line);
					}
				}
				tooltipTextWidth = wrappedTooltipWidth;
				textLines = wrappedTextLines;

				if (mouseX > screenWidth / 2)
					tooltipX = mouseX - 16 - tooltipTextWidth;
				else
					tooltipX = mouseX + 12;
			}

			int tooltipY = mouseY - 12;
			int tooltipHeight = 8;

			if (textLines.size() > 1)
			{
				tooltipHeight += (textLines.size() - 1) * 10;
				if (textLines.size() > titleLinesCount)
					tooltipHeight += 2; // gap between title lines and next lines
			}

			if (tooltipY < 4)
				tooltipY = 4;
			else if (tooltipY + tooltipHeight + 4 > screenHeight)
				tooltipY = screenHeight - tooltipHeight - 4;

			final int zLevel = 400;
			RenderTooltipEvent.Color colorEvent = new RenderTooltipEvent.Color(stack, mStack, tooltipX, tooltipY, font, backgroundColor, borderColorStart, borderColorEnd, components);
			MinecraftForge.EVENT_BUS.post(colorEvent);
			backgroundColor = colorEvent.getBackgroundStart();
			borderColorStart = colorEvent.getBorderStart();
			borderColorEnd = colorEvent.getBorderEnd();

			mStack.pushPose();
			Matrix4f mat = mStack.last().pose();
			// TODO, lots of unnessesary GL calls here, we can buffer all these together.
			// done - all 1 GL call
			Tesselator tes = drawGradientRectPre();
			drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY - 4, tooltipX + tooltipTextWidth + 3, tooltipY - 3, backgroundColor, backgroundColor, tes);
			drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY + tooltipHeight + 3, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 4, backgroundColor, backgroundColor, tes);
			drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY - 3, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 3, backgroundColor, backgroundColor, tes);
			drawGradientRect(mat, zLevel, tooltipX - 4, tooltipY - 3, tooltipX - 3, tooltipY + tooltipHeight + 3, backgroundColor, backgroundColor, tes);
			drawGradientRect(mat, zLevel, tooltipX + tooltipTextWidth + 3, tooltipY - 3, tooltipX + tooltipTextWidth + 4, tooltipY + tooltipHeight + 3, backgroundColor, backgroundColor,
					tes);
			drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY - 3 + 1, tooltipX - 3 + 1, tooltipY + tooltipHeight + 3 - 1, borderColorStart, borderColorEnd, tes);
			drawGradientRect(mat, zLevel, tooltipX + tooltipTextWidth + 2, tooltipY - 3 + 1, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 3 - 1, borderColorStart,
					borderColorEnd, tes);
			drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY - 3, tooltipX + tooltipTextWidth + 3, tooltipY - 3 + 1, borderColorStart, borderColorStart, tes);
			drawGradientRect(mat, zLevel, tooltipX - 3, tooltipY + tooltipHeight + 2, tooltipX + tooltipTextWidth + 3, tooltipY + tooltipHeight + 3, borderColorEnd, borderColorEnd, tes);
			drawGradientRectPost(tes);

			// MinecraftForge.EVENT_BUS.post(new RenderTooltipEvent.PostBackground(stack,
			// mStack, tooltipX, tooltipY, tooltipTextWidth, tooltipHeight, font,
			// components));

			MultiBufferSource.BufferSource renderType = MultiBufferSource.immediate(Tesselator.getInstance().getBuilder());
			mStack.translate(0.0D, 0.0D, zLevel);

			int tooltipTop = tooltipY;

			for (int lineNumber = 0; lineNumber < textLines.size(); ++lineNumber)
			{
				FormattedText line = textLines.get(lineNumber);
				if (line != null)
					font.drawInBatch(Language.getInstance().getVisualOrder(line), (float) tooltipX, (float) tooltipY, -1, true, mat, renderType, false, 0, 15728880);

				if (lineNumber + 1 == titleLinesCount)
					tooltipY += 2;

				tooltipY += 10;
			}

			renderType.endBatch();
			mStack.popPose();

			// MinecraftForge.EVENT_BUS.post(new RenderTooltipEvent.PostText(stack,
			// textLines, mStack, tooltipX, tooltipTop, font, tooltipTextWidth,
			// tooltipHeight));

			RenderSystem.enableDepthTest();

			return tooltipY;
//            RenderSystem.enableRescaleNormal();
		}

		return -1;
	}

	public static void drawGradientRect(Matrix4f mat, int zLevel, int left, int top, int right, int bottom, int startColor, int endColor, Tesselator tessellator)
	{
		float startAlpha = (float) (startColor >> 24 & 255) / 255.0F;
		float startRed = (float) (startColor >> 16 & 255) / 255.0F;
		float startGreen = (float) (startColor >> 8 & 255) / 255.0F;
		float startBlue = (float) (startColor & 255) / 255.0F;
		float endAlpha = (float) (endColor >> 24 & 255) / 255.0F;
		float endRed = (float) (endColor >> 16 & 255) / 255.0F;
		float endGreen = (float) (endColor >> 8 & 255) / 255.0F;
		float endBlue = (float) (endColor & 255) / 255.0F;

		BufferBuilder buffer = tessellator.getBuilder();
		buffer.vertex(mat, right, top, zLevel).color(startRed, startGreen, startBlue, startAlpha).endVertex();
		buffer.vertex(mat, left, top, zLevel).color(startRed, startGreen, startBlue, startAlpha).endVertex();
		buffer.vertex(mat, left, bottom, zLevel).color(endRed, endGreen, endBlue, endAlpha).endVertex();
		buffer.vertex(mat, right, bottom, zLevel).color(endRed, endGreen, endBlue, endAlpha).endVertex();
	}

	@SuppressWarnings("deprecation")
	public static Tesselator drawGradientRectPre()
	{
		RenderSystem.enableDepthTest();
		RenderSystem.disableTexture();
		RenderSystem.enableBlend();
		RenderSystem.defaultBlendFunc();
		RenderSystem.setShader(GameRenderer::getPositionColorShader);

		Tesselator tessellator = Tesselator.getInstance();
		BufferBuilder buffer = tessellator.getBuilder();
		buffer.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR);
		return tessellator;
	}

	@SuppressWarnings("deprecation")
	public static void drawGradientRectPost(Tesselator tessellator)
	{
		tessellator.end();

		RenderSystem.setShader(GameRenderer::getPositionColorShader);
		RenderSystem.disableBlend();
		RenderSystem.enableTexture();
	}

	/* Feature Renderers */

	public static void renderNeon(PoseStack matrixStack, int x, int y, INeonEnergyStorage engine, int mouseX, int mouseY)
	{
		// Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 0, 0, 11, 72, 11, 72, 256, 256, ZLEVEL_FP_BARS);

		int p = (int) ((float) engine.get() / (float) engine.getMax() * 72F);

		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (72 - p), 11, 0 + (72 - p), 11, p, 11, p, 256, 256, ZLEVEL_FP_BARS + 1);
	}

	public static void renderNeonTooltip(PoseStack matrixStack, int guiLeft, int guiTop, int nx, int ny, INeonEnergyStorage engine, int mouseX, int mouseY)
	{
		if (HelperComponent.isInBox(mouseX, mouseY, guiLeft + nx, guiTop + ny, guiLeft + nx + 11, guiTop + ny + 72))
		{
			HelperGui.drawHoveringTextFixedString(matrixStack, Arrays.asList(engine.get() + " / " + engine.getMax() + " NE"), mouseX - guiLeft, mouseY - guiTop, -1,
					Minecraft.getInstance().font);
		}
	}

	public static void renderSupport(PoseStack matrixStack, int x, int y, ISupportStorage able, int mouseX, int mouseY)
	{
		// Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 0, 0, 11, 72, 11, 72, 256, 256, ZLEVEL_FP_BARS);

		int p = (int) ((float) able.get() / (float) able.getMax() * 72F);

		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (72 - p), 22, 0 + (72 - p), 11, p, 11, p, 256, 256, ZLEVEL_FP_BARS + 1);
	}

	public static void renderSupportTooltip(PoseStack matrixStack, int guiLeft, int guiTop, int nx, int ny, ISupportStorage able, int mouseX, int mouseY)
	{
		if (HelperComponent.isInBox(mouseX, mouseY, guiLeft + nx, guiTop + ny, guiLeft + nx + 11, guiTop + ny + 72))
		{
			HelperGui.drawHoveringTextFixedString(matrixStack, Arrays.asList(able.get() + " / " + able.getMax() + " SP"), mouseX - guiLeft, mouseY - guiTop, -1,
					Minecraft.getInstance().font);
		}
	}

	public static void renderRedstoneFlux(PoseStack matrixStack, int x, int y, int RF, int maxRF, int mouseX, int mouseY)
	{
		// Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 0, 0, 11, 72, 11, 72, 256, 256, ZLEVEL_FP_BARS);

		int p = (int) ((float) RF / (float) maxRF * 72F);

		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (72 - p), 114, 0 + (72 - p), 11, p, 11, p, 256, 256, ZLEVEL_FP_BARS + 1);

		if (HelperComponent.isInBox(mouseX, mouseY, x, y, x + 10, y + 72))
		{
			drawHoveringTextFixedString(matrixStack, Arrays.asList(RF + " / " + maxRF + " RF"), mouseX, mouseY, -1, Minecraft.getInstance().font);
		}
	}

	public static void renderExp(PoseStack matrixStack, int x, int y, ITileXpStorage xp, int mouseX, int mouseY)
	{
		// Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 33, 0, 18, 66, 18, 66, 256, 256, ZLEVEL_FP_BARS);

		int p = (int) ((float) xp.getXp() / (float) xp.getMaxXp() * 66F);

		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (66 - p), 51, 0 + (66 - p), 18, p, 18, p, 256, 256, ZLEVEL_FP_BARS + 1);

		if (HelperComponent.isInBox(mouseX, mouseY, x, y, x + 18, y + 66))
		{
			drawHoveringTextFixedString(matrixStack, Arrays.asList(xp.getXp() + "/" + (float) xp.getMaxXp() + " XP"), mouseX, mouseY, -1, Minecraft.getInstance().font);
		}
	}

	public static void renderTabBase(PoseStack matrixStack, int x, int y, boolean right, int mouseX, int mouseY, boolean activated, int blitOffset)
	{
		// activated, hover, base; x+6 y+6 16x16

		int u = right ? 0 : 28;
		int v = 146;

		if (HelperComponent.isInBox(mouseX, mouseY, x, y, x + 28, y + 28))
		{
			v = 118;
		}
		if (activated)
		{
			v = 90;
		}

		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, u, v, 28, 28, 28, 28, 256, 256, blitOffset);
	}

	/**
	 * Modes: 0 Black, 1 White, 2 Blue, 3 Yellow, 4 Green;
	 *
	 * @param matrixStack TODO
	 */
	public static void renderSmallBars(PoseStack matrixStack, int x, int y, float f, int mode)
	{
		mode = mode % 5;
		RenderSystem.setShaderTexture(0, resEnergyBarTex);

		int w = 9;
		int h = 45;
		// Background
		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y, 69, 0, w, h, w, h, 256, 256, ZLEVEL_FP_BARS);

		int p = (int) (f * h);

		drawQuadWithTexture(matrixStack, resEnergyBarTex, x, y + (45 - p), 69 + mode * w, 0 + (45 - p), w, p, w, p, 256, 256, ZLEVEL_FP_BARS + 1);
	}

	public static void renderFluidTank(int x, int y, int w, int h, IFluidTankInfo fluid, int mouseX, int mouseY, double zLvl)
	{
		if (fluid.isEmpty())
			return;

		RenderSystem.setShaderTexture(0, TextureAtlas.LOCATION_BLOCKS);

		TextureAtlas map = Minecraft.getInstance().getModelManager().getAtlas(TextureAtlas.LOCATION_BLOCKS);
		TextureAtlasSprite texture = map.getSprite(fluid.getFluidStack().getFluid().getAttributes().getStillTexture(fluid.getFluidStack()));

		int i = fluid.getFluidStack().getFluid().getAttributes().getColor(fluid.getFluidStack());

		int scale = 16;
		float fill = (float) fluid.getFluidStack().getAmount() / (float) fluid.getCapacity();

		int fh = (int) (h * fill);

		float b = (i & 0xFF) / 255F;
		float g = ((i & 0xFF00) >> 8) / 255F;
		float r = ((i & 0xFF0000) >> 16) / 255F;
		HelperRendering.glColor4f(r, g, b, 1);
		drawTextureRect(x, y + h - fh, w, fh, texture, (float) w / scale, (float) fh / scale, zLvl);
		RenderSystem.setShaderColor(1, 1, 1, 1);
	}

	public static void renderFluidTankTooltip(PoseStack matrixStack, int x, int y, int w, int h, IFluidTankInfo fluid, int mouseX, int mouseY)
	{
		HelperRendering.disableLighting();
		GlStateManager._disableDepthTest();
		Lighting.setupFor3DItems();

		if (HelperComponent.isInBox(mouseX, mouseY, x, y, x + w, y + h))
		{
			String s1 = fluid.getFluidStack().getDisplayName().getString();
			String s2 = ChatFormatting.RESET + "" + fluid.getFluidStack().getAmount() + " / " + fluid.getCapacity() + " mB";

			HelperGui.renderText(matrixStack, mouseX, mouseY, s1 + "\n" + s2);
		}

		HelperRendering.enableLighting();
		GlStateManager._enableDepthTest();
		Lighting.setupForFlatItems();
	}

	private static void drawTextureRect(int x, int y, int w, int h, TextureAtlasSprite tex, float xCount, float yCount, double zLvl)
	{
		int xc = (int) xCount;
		int yc = (int) yCount;
		double width = w / xCount;
		double height = h / yCount;

		Tesselator tessellator = Tesselator.getInstance();
		BufferBuilder vertexbuffer = tessellator.getBuilder();
		vertexbuffer.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX); // GL_QUADS = 7

		for (int i = 0; i < xc; i++)
		{
			for (int j = 0; j < yc; j++)
			{
				fill(x + i * width, y + j * height, width, height, tex.getU0(), tex.getV0(), tex.getU1(), tex.getV1(), zLvl);
			}
		}

		double lostV = (yCount - yc);
		double newY = y + yc * height;
		double newV = tex.getV0() + (tex.getV1() - tex.getV0()) * lostV;
		double newH = height * lostV;

		for (int i = 0; i < xc; i++)
		{
			fill(x + i * width, newY, width, newH, tex.getU0(), tex.getV0(), tex.getU1(), newV, zLvl);
		}

		double lostU = (xCount - xc);
		double newX = x + xc * width;
		double newU = tex.getU0() + (tex.getU1() - tex.getU0()) * lostU;
		double newW = width * lostU;

		for (int i = 0; i < yc; i++)
		{
			fill(newX, y + i * height, newW, height, tex.getU0(), tex.getV0(), newU, tex.getV1(), zLvl);
		}
		fill(newX, newY, newW, newH, tex.getU0(), tex.getV0(), newU, newV, zLvl);

		tessellator.end();
	}

	private static void fill(double xCoord, double yCoord, double w, double h, double minU, double minV, double maxU, double maxV, double zLvl)
	{
		Tesselator tessellator = Tesselator.getInstance();
		BufferBuilder vertexbuffer = tessellator.getBuilder();
		vertexbuffer.vertex(xCoord + 0, yCoord + h, zLvl).uv((float) minU, (float) maxV).endVertex();
		vertexbuffer.vertex(xCoord + w, yCoord + h, zLvl).uv((float) maxU, (float) maxV).endVertex();
		vertexbuffer.vertex(xCoord + w, yCoord + 0, zLvl).uv((float) maxU, (float) minV).endVertex();
		vertexbuffer.vertex(xCoord + 0, yCoord + 0, zLvl).uv((float) minU, (float) minV).endVertex();
	}

	@Deprecated
	public static void renderText(PoseStack matrixStack, int x, int y, String string)
	{
		x += 12;
		y += 5;
		renderHoverText(matrixStack, x, y, 450, string.split("\n"));
	}

	@Deprecated
	public static void renderText(PoseStack matrixStack, int x, int y, Component string)
	{
		x += 12;
		y += 5;
		renderHoverText(matrixStack, x, y, 450, Collections.singletonList(string));
	}

	@Deprecated
	public static void renderGradientBox(PoseStack matrixStack, int x, int y, int w, int h, int blitOffset)
	{
		RenderSystem.disableBlend();
		fill(matrixStack, x - 3, y - (h / 2) - 2, x + w + 3, y + (h / 2) + 2, 0xff010111, blitOffset);
		fill(matrixStack, x - 2, y - (h / 2) - 3, x + w + 2, y + (h / 2) + 3, 0xff011101, blitOffset);
		fillGradient(x - 2, y - (h / 2) - 2, x + w + 2, y + (h / 2) + 2, 0xff330099, 0xff110055, blitOffset);
		fill(matrixStack, x - 1, y - (h / 2) - 1, x + w + 1, y + (h / 2) + 1, 0xff110101, blitOffset);
		RenderSystem.setShaderColor(1, 1, 1, 1);
	}

	public static void renderHoverText(PoseStack matrixStack, int x, int y, int blit, List<Component> list)
	{
		renderHoverText(matrixStack, x, y, blit, list, Integer.MAX_VALUE);
	}

	public static final String autokratisch_aurisch = "\\u00a7#g";

	public static void renderHoverText(PoseStack matrixStack, int x, int y, int blit, List<Component> list, int maxWidth)
	{
		Lighting.setupFor3DItems();

		RenderSystem.enableDepthTest();

		Font font = Minecraft.getInstance().font;
		ResourceLocation autokratischFont = HelperComponent.getAutokratischFont();

		int textWidth = 0;

		for (int i = 0; i < list.size(); i++)
		{
			Component text = list.get(i);
			String s = text.getString();
			if (s.startsWith(HelperGui.autokratisch_aurisch))
			{
				s = s.substring(HelperGui.autokratisch_aurisch.length());
				s = HelperComponent.toKryptikMessage(s);
				TextComponent tc = new TextComponent(s);
				tc.setStyle(text.getStyle().withFont(autokratischFont));
				text = tc;
				list.set(i, text);
			}
			textWidth = Math.max(font.width(text), textWidth);
		}

		int w = Math.min(textWidth, maxWidth) + 2;

		ArrayList<FormattedCharSequence> rp = new ArrayList<FormattedCharSequence>();
		for (Component tc : list)
			rp.addAll(font.split(tc, w - 2));

		int h = font.lineHeight * rp.size() + 1;

		renderGradientBox(matrixStack, x, y, w, h, blit);

		matrixStack.pushPose();
		matrixStack.translate(0, 0, blit);

		for (int i = 0; i < rp.size(); i++)
		{
			FormattedCharSequence wrappedText = rp.get(i);
			font.draw(matrixStack, wrappedText, x + 1, y + 1 - (h / 2) + i * font.lineHeight, 0xffffffff); // drawString
		}
		matrixStack.popPose();
	}

	@Deprecated
	public static void renderHoverText(PoseStack matrixStack, int x, int y, int blit, String... textBase)
	{
		renderHoverText(matrixStack, x, y, blit, Arrays.stream(textBase).map(TextComponent::new).collect(Collectors.toList()));
	}

	public static void fillGradient(int left, int top, int right, int bottom, int startColor, int endColor, int blitOffset)
	{
		float f = (startColor >> 24 & 255) / 255.0F;
		float f1 = (startColor >> 16 & 255) / 255.0F;
		float f2 = (startColor >> 8 & 255) / 255.0F;
		float f3 = (startColor & 255) / 255.0F;
		float f4 = (endColor >> 24 & 255) / 255.0F;
		float f5 = (endColor >> 16 & 255) / 255.0F;
		float f6 = (endColor >> 8 & 255) / 255.0F;
		float f7 = (endColor & 255) / 255.0F;
		GlStateManager._disableTexture();
		GlStateManager._enableBlend();
		HelperRendering.disableAlphaTest();
		GlStateManager._blendFuncSeparate(770, 771, 1, 0);
		RenderSystem.setShader(GameRenderer::getPositionColorShader);
		Tesselator tessellator = Tesselator.getInstance();
		BufferBuilder buf = tessellator.getBuilder();
		buf.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR);
		buf.vertex(right, top, blitOffset).color(f1, f2, f3, f);
		buf.vertex(left, top, blitOffset).color(f1, f2, f3, f);
		buf.vertex(left, bottom, blitOffset).color(f5, f6, f7, f4);
		buf.vertex(right, bottom, blitOffset).color(f5, f6, f7, f4);
		tessellator.end();
//		GlStateManager._shadeModel(GL11.GL_FLAT);
		GlStateManager._disableBlend();
		HelperRendering.enableAlphaTest();
		GlStateManager._enableTexture();
	}

	public static void fill(PoseStack matrixStack, int left, int top, int right, int bottom, int color, int blitOffset)
	{
		matrixStack.translate(0, 0, blitOffset);
		GuiComponent.fill(matrixStack, left, top, right, bottom, color);
		matrixStack.translate(0, 0, -blitOffset);
	}

	public static void drawTexturedModalRect(float x, float y, float textureX, float textureY, float width, float height, double blitOffset)
	{
		float f = 0.00390625F; // 1/256
		Tesselator tessellator = Tesselator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuilder();
		bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
		fill(x, y, width, height, textureX * f, textureY * f, (textureX + width) * f, (textureY + height) * f, blitOffset);
		tessellator.end();
	}

	public static void fill(double x0, double y0, double x1, double y1, int color, double blitOffset)
	{
		if (x0 < x1)
		{
			double i = x0;
			x0 = x1;
			x1 = i;
		}

		if (y0 < y1)
		{
			double j = y0;
			y0 = y1;
			y1 = j;
		}

		float f3 = (float) (color >> 24 & 255) / 255.0F;
		float f = (float) (color >> 16 & 255) / 255.0F;
		float f1 = (float) (color >> 8 & 255) / 255.0F;
		float f2 = (float) (color & 255) / 255.0F;
		Tesselator tessellator = Tesselator.getInstance();
		BufferBuilder bufferbuilder = tessellator.getBuilder();
		GlStateManager._enableBlend();
		GlStateManager._disableTexture();
		GlStateManager._blendFuncSeparate(GlStateManager.SourceFactor.SRC_ALPHA.value, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA.value, GlStateManager.SourceFactor.ONE.value,
				GlStateManager.DestFactor.ZERO.value);
		bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR);
		bufferbuilder.vertex(x0, y1, blitOffset).color(f, f1, f2, f3).endVertex();
		bufferbuilder.vertex(x1, y1, blitOffset).color(f, f1, f2, f3).endVertex();
		bufferbuilder.vertex(x1, y0, blitOffset).color(f, f1, f2, f3).endVertex();
		bufferbuilder.vertex(x0, y0, blitOffset).color(f, f1, f2, f3).endVertex();
		tessellator.end();
		GlStateManager._enableTexture();
		GlStateManager._disableBlend();
	}


	public static double cursorPosStoreX;
	public static double cursorPosStoreY;
	public static long time = 0L;

	public static void RestoreCursorPos()
	{
		if (System.currentTimeMillis() -time <= 1000)
		{
			GLFW.glfwSetCursorPos(Minecraft.getInstance().getWindow().getWindow(), HelperGui.cursorPosStoreX, HelperGui.cursorPosStoreY);
		}
	}

	public static void SaveCursorPos()
	{
		DoubleBuffer x = BufferUtils.createDoubleBuffer(1);
		DoubleBuffer y = BufferUtils.createDoubleBuffer(1);
		GLFW.glfwGetCursorPos(Minecraft.getInstance().getWindow().getWindow(), x, y);
		HelperGui.cursorPosStoreX = x.get();
		HelperGui.cursorPosStoreY = y.get();
		time = System.currentTimeMillis();
	}

}
