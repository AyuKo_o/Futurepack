package futurepack.depend.api;

import java.util.ArrayList;

import com.google.gson.JsonArray;

import futurepack.api.Constants;
import net.minecraft.resources.ResourceLocation;

public enum EnumAspects
{
	GEOLOGIE("geologie"),
	CHEMIE("chemie"),
	TIERHALTUNG("tierhaltung"),
	LOGISTIK("logistik"),
	METALLURGIE("metallurgie"),
	BIOLOGIE("biologie"),
	MORPHOLOGIE("morphologie"),
	NEONENERGIE("neonenergie"),
	ENERGIEVERBINDUNG("energieverbindung"),
	THERMODYNAMIC("thermodynamic"),
	MASCHINENBAU("maschinenbau"),
	HYDRAULIC("hydraulic"),
	
	FP("fp"),
	FORSCHUNG("forschung"),
	PRODUCTION("production"),
	PRODUCTION1("production1"),
	PRODUCTION2("production2"),
	PRODUCTION3("production3"),
	WERKZEUGE("werkzeuge"),
	OVERCLOCK("overclock"),
	ENERGIESPEICHER("energiespeicher"),
	SUPPORTSPEICHER("supportspeicher"),
	XPSPEICHER("xpspeicher"),
	OPTIK("optik"),
	LASERTECHNIK1("lasertechnik1"),
	LASERTECHNIK2("lasertechnik2"),
	RAUMFAHRT("raumfahrt"),
	KI("ki"),
	ER_ENERGIE("er-energie"),
	WAFFENTECH("waffentech"),
	BERGBAU("bergbau"),
	BERGBAU2("bergbau2"),
	MAGNETISMUS("magnetismus"),
	EN_ERZEUGUNG("en_erzeug"),
	EN_ERZEUGUNG_1("en_erzeug_1"),
	EN_ERZEUGUNG_2("en_erzeug_2"),
	EN_ERZEUGUNG_3("en_erzeug_3"),
	ASTRONOMIE("astronomie"),
	CHIP_ASSEMBLY("chip_assembly"),
	ENLOGISTIK("enlogistik"),
	HOLOTECH("holotech"),
	IONTECH("iontech"),
	ITEMLOG("itemlog"),
	NETZWERK("netzwerk"),
	QUANTENTECH("quantentech"),
	ENTOOLS("entools"),
	PLANET_ENTROS("planet_entros"),
	PLANET_TYROS("planet_tyros"),
	PLANET_MENELAUS("planet_menelaus"),
	PLANET_MINCRA("planet_mincra"),
	DRONES("drones"),
	UNGREIFBAR("ungreifbar"),
	ENTITY_MINER("entity_miner"),
	ENTITY_MONOCART("entity_monocart"),
	ENTITY_FORSTMASTER("entity_forstmaster"),
	RECYCLING("recycling"),
	KRYPTOGRAPHIE("krypto"),
	TIMEMANIPULATION("timemanipulation"),
	NANOMANIP("nanomanip"),
	MENCAVE("mencave"),
	MENLIFE("menlife"),
	MENRESULT("menresult"),
	MENSHROOM("menshroom"),
	MENTERRA("menterra"),
	DEEP_CORE("deep_core"),
	FLUIDLOG("fluidlog"),
	ARMOR("armor"),
	COMBO("combo"),
	DEEPCOREMANUAL("deepcoremanual"),
	MANUAL("manual"),
	TYROSTERRA("tyrosterra"),
	TYROSRESULT("tyrosresult"),
	TYROSCAVE("tyroscave"),
	TYROSTREE("tyrostree"),
	TYROSDEAD("tyrosdead"),
	TYROSLIFE("tyroslife"),
	WAFFENTECH2("waffentech2"),
	;
	
	public static EnumAspects[] buttons = new EnumAspects[]
	{
		EnumAspects.GEOLOGIE,
		EnumAspects.CHEMIE,
		EnumAspects.TIERHALTUNG,
		EnumAspects.LOGISTIK,
		EnumAspects.METALLURGIE,
		EnumAspects.BIOLOGIE,
		EnumAspects.MORPHOLOGIE,
		EnumAspects.NEONENERGIE,
		EnumAspects.ENERGIEVERBINDUNG,
		EnumAspects.THERMODYNAMIC,
		EnumAspects.MASCHINENBAU,
		EnumAspects.HYDRAULIC
	};	
	
	
	
	public final String id;
	private final ResourceLocation pic;
	
	EnumAspects(String id)
	{
		this.id = id;
		pic = new ResourceLocation(Constants.MOD_ID, "textures/aspects/" + id + ".png");
	}
	
	public ResourceLocation getResourceLocation()
	{
		return pic;
	}
	
	public static EnumAspects[] getAspects(JsonArray arr)
	{
		ArrayList<EnumAspects> l = new ArrayList<EnumAspects>(arr.size());
		for(int i=0;i<arr.size();i++)
		{
			String s = arr.get(i).getAsString().toUpperCase();
			l.add(EnumAspects.valueOf(s));
		}
		return l.toArray(new EnumAspects[l.size()]);
	}
}
