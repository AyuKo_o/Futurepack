//package futurepack.depend.api.main;
//
//import java.util.concurrent.Callable;
//
//import futurepack.api.interfaces.tilentity.ITileHologramAble;
//import futurepack.depend.api.helper.HelperHologram;
//import net.minecraft.core.Direction;
//import net.minecraft.nbt.CompoundTag;
//import net.minecraft.nbt.Tag;
//import net.minecraft.world.level.block.state.BlockState;
//import net.minecraftforge.common.capabilities.Capability;
//import net.minecraftforge.common.capabilities.Capability.IStorage;
//
//public class CapabilityHologram implements ITileHologramAble
//{
//
//	public static class Factory implements Callable<ITileHologramAble>
//	{
//		@Override
//		public CapabilityHologram call() throws Exception 
//		{
//			return new CapabilityHologram();
//		}
//		
//	}
//	
//	public static class Storage implements IStorage<ITileHologramAble>
//	{
//
//		@Override
//		public Tag writeNBT(Capability<ITileHologramAble> capability, ITileHologramAble instance, Direction side)
//		{
//			if(instance.hasHologram())
//			{
//				return HelperHologram.toNBT(instance.getHologram());
//			}
//			return null;
//		}
//
//		@Override
//		public void readNBT(Capability<ITileHologramAble> capability, ITileHologramAble instance, Direction side, Tag nbt)
//		{
//			instance.setHologram(HelperHologram.fromNBT((CompoundTag) nbt));
//		}
//		
//	}
//	
//	
//	BlockState render;
//	
//	@Override
//	public BlockState getHologram()
//	{
//		return render;
//	}
//
//	@Override
//	public boolean hasHologram()
//	{
//		return render!=null;
//	}
//
//	@Override
//	public void setHologram(BlockState state)
//	{
//		render = state;
//	}
//
//}
