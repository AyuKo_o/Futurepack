package futurepack.dev;

import java.io.File;
import java.io.FileWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import futurepack.depend.api.helper.HelperItems;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.registries.ForgeRegistries;

public class RecipeJSONGenerator {
	
	public static void generateRecipe(ItemStack[] grid, ItemStack out) throws Throwable 
	{
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();

		Gson gson = builder.create();
		
		JsonObject root = new JsonObject();
		root.addProperty("type", "futurepack:research_shaped");
		root.addProperty("group", "futurepack");
		

		
		
		/*Find Symbols*/
		class InputEntry
		{
			ItemStack stack;
			char symbol;
		}
		
		HashMap<String, InputEntry> InputToSymbol = new HashMap<String, InputEntry>();
		
		char[] symbolGrid = new char[9];
		
		for(int i = 0; i < 9; i++)
		{
			if(grid[i].isEmpty())
			{
				symbolGrid[i] = ' ';
				continue;
			}
			
			InputEntry it = InputToSymbol.get(HelperItems.getRegistryName(grid[i].getItem()).toString());
			
			if(it == null)
			{
				it = new InputEntry();
				it.symbol = GetSymbol(grid[i], symbolGrid, (char)(i+65));
				it.stack = grid[i];
				
				InputToSymbol.put(HelperItems.getRegistryName(grid[i].getItem()).toString(), it);
			}	
			
			symbolGrid[i] = it.symbol;
			
		}
		
		
		boolean col3 = symbolGrid[2] != ' ' || symbolGrid[5] != ' ' || symbolGrid[8] != ' ';
		boolean col2 = col3 || symbolGrid[1] != ' ' || symbolGrid[4] != ' ' || symbolGrid[7] != ' ';
		
		boolean line3 = symbolGrid[6] != ' ' || symbolGrid[7] != ' ' || symbolGrid[8] != ' ';
		boolean line2 = line3 || symbolGrid[3] != ' ' || symbolGrid[4] != ' ' || symbolGrid[5] != ' ';
		
		JsonArray pattern = new JsonArray();
		pattern.add("" + symbolGrid[0] + (col2 ? symbolGrid[1] : "") + (col3 ? symbolGrid[2] : "") );
		if(line2)
			pattern.add("" + symbolGrid[3] + (col2 ? symbolGrid[4] : "") + (col3 ? symbolGrid[5] : "") );
		if(line3)
			pattern.add("" + symbolGrid[6] + (col2 ? symbolGrid[7] : "") + (col3 ? symbolGrid[8] : "") );

		root.add("pattern", pattern);
		
		
		JsonObject key = new JsonObject();
		
		for(Entry<String, InputEntry> e : InputToSymbol.entrySet())
		{
			InputEntry ie = e.getValue();
			
			JsonObject jo = EncodeItem(ie.stack);
			key.add("" + ie.symbol, jo);
			
		}
			
		root.add("key", key);
		
		//Result
		JsonObject result = new JsonObject();
		result.addProperty("item", HelperItems.getRegistryName(out.getItem()).toString());
		result.addProperty("count", out.getCount());
		
		root.add("result", result);
		
		File dir = new File("../generated_recipes/");
		dir.mkdirs();
		FileWriter writer = new FileWriter("../generated_recipes/" + HelperItems.getRegistryName(out.getItem()).getPath() + ".json");   
		writer.write(gson.toJson(root));
	    writer.close(); 
	}

	private static JsonObject EncodeItem(ItemStack stack) 
	{
		JsonObject result = new JsonObject();
	
		Collection<ResourceLocation> tags = stack.getTags().map(TagKey::location).toList();
		
		if(tags.size() <= 0)
		{
			result.addProperty("item", HelperItems.getRegistryName(stack.getItem()).toString());
		}
		else 
		{
			if(tags.size() > 1)
			{
				System.out.println("Warning: Multiple Tags found for " + HelperItems.getRegistryName(stack.getItem()).toString());
			}
			
			int bestCount = Integer.MAX_VALUE;
			ResourceLocation bestTag = null;
			
			for(ResourceLocation loc : tags)
			{
				int count = ForgeRegistries.ITEMS.tags().getTag(ItemTags.create(loc)).size();
				if(count < bestCount)
				{
					bestCount = count;
					bestTag = loc;
				}
			}
			
			result.addProperty("tag", bestTag.toString());
			
		}
		

		
		return result;
	}

	private static char GetSymbol(ItemStack itemStack, char[] symbolGrid, char b) 
	{
		System.out.println(itemStack.getDisplayName().getString());
		char a = Character.toUpperCase(itemStack.getDisplayName().getString().charAt(1));
		
		for(int i = 0; i < symbolGrid.length; i++)
		{
			if(a == symbolGrid[i])
			{
				return Character.toUpperCase(b);
			}
		}
			
		return a;
	}

}
