package futurepack.client.render;

import java.awt.Color;
import java.lang.ref.SoftReference;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.platform.MemoryTracker;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexFormat;

import futurepack.client.render.block.RenderLogistic;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.Vec3;

public class RenderRoomAnalyzer
{

	private static ArrayList<SoftReference<AirChunk>> list;

	public static void addAirDataReceived(BlockPos pos, byte[] data, int cw, int ch, int cd)
	{
		pos = pos.offset(-16, -16, -16);
		AirChunk air = new AirChunk(cw, cd, ch, pos, data);
		SoftReference<AirChunk> soft = new SoftReference<AirChunk>(air);
		if(list==null)
		{
			list = new ArrayList<SoftReference<AirChunk>>();
		}
		synchronized (list)
		{
			Iterator<SoftReference<AirChunk>> iter = list.iterator();
			while(iter.hasNext())
			{
				SoftReference<AirChunk> ref = iter.next();
				AirChunk aaa = ref.get();
				if(aaa!=null)
				{
					if(aaa.pos.equals(pos))
					{
						iter.remove();
						continue;
					}
				}
				else
				{
					iter.remove();
					continue;
				}
			}
			list.add(soft);
		}
	}

	public static class AirChunk
	{
		private final int cw, cd, ch;
		private final BlockPos pos;
		private final byte[] data;
		public final long time;

		private Runnable renderCall;

		public AirChunk(int cw, int cd, int ch, BlockPos pos, byte[] data)
		{
			super();
			this.cw = cw;
			this.cd = cd;
			this.ch = ch;
			this.pos = pos;
			this.data = data;
			time = System.currentTimeMillis();
		}

		public void render(float partialTicks, PoseStack matrixStack)
		{
			float apartialTicks = 1F - partialTicks;
			if(renderCall == null)
			{
				BufferBuilder builder = new BufferBuilder(data.length);
				builder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_COLOR);
				render(builder, data);
				builder.end();
				renderCall = RenderLogistic.createStaticRenderCall(builder);
			}
			LocalPlayer sp = Minecraft.getInstance().player;
			double dx,dy,dz;
			Vec3 p = sp.getEyePosition(partialTicks).scale(-1F).add(pos.getX(), pos.getY(), pos.getZ());
			dx = p.x();
			dy = p.y();
			dz = p.z();

//			double dis = dx*dx + dy*dy + dz*dz;
			//if(dis < 50*50)
			{
				matrixStack.pushPose();
				//matrixStack.translate(dx, dy, dz);


				FuturepackRenderTypes.ROOM_ANALYZER.setupRenderState();

				//RenderSystem.disableTexture();
				//RenderSystem.enableBlend();
				//RenderSystem.depthMask(false);

				RenderSystem.getModelViewStack().pushPose();
				RenderSystem.getModelViewMatrix().load(matrixStack.last().pose());

				//GlStateManager._blendFunc(SourceFactor.SRC_ALPHA.value, DestFactor.ONE_MINUS_SRC_ALPHA.value);

				renderCall.run();

				//RenderSystem.enableTexture();
				//RenderSystem.disableBlend();
				//RenderSystem.depthMask(true);

				FuturepackRenderTypes.ROOM_ANALYZER.clearRenderState();


				RenderSystem.getModelViewStack().popPose();
				RenderSystem.applyModelViewMatrix();
				matrixStack.popPose();
			}
		}

		private void render(BufferBuilder buffer, byte[] data)
		{
			for(int x=0;x<16*cw;x++)
			{
				for(int y=0;y<16*ch;y++)
				{
					for(int z=0;z<16*cd;z++)
					{
						int index = (x)*cd*ch*256 + (y)*cd*16 + (z);
						int air = data[index] + 128;

						if(air<=0)
						{
							continue;
						}
						float angle = air / 256F * 0.6666F;
						//Color col = new Color(0xA5000000 | Color.HSBtoRGB(angle, 1F, 1F));
						int rgb = Color.HSBtoRGB(angle, 1F, 1F) & 0xFFFFFF;
						Color col = new Color(0x1a000000 | rgb, true);
						//sides

						//sides
						int x0 = Float.floatToIntBits(x);
						int x1 = Float.floatToIntBits(x + 1);
						int y0 = Float.floatToIntBits(y);
						int y1 = Float.floatToIntBits(y + 1);
						int z0 = Float.floatToIntBits(z);
						int z1 = Float.floatToIntBits(z + 1);
						int c = col.getAlpha() <<24 | col.getRed() | col.getGreen()<<8 | col.getBlue()<<16;

						int vertexData[] =
						{
								x0,y0,z0,c,
								x0,y1,z0,c,
								x1,y1,z0,c,
								x1,y0,z0,c,

								x0,y0,z1,c,
								x0,y1,z1,c,
								x0,y1,z0,c,
								x0,y0,z0,c,

								x1,y0,z1,c,
								x1,y1,z1,c,
								x0,y1,z1,c,
								x0,y0,z1,c,

								x1,y0,z0,c,
								x1,y1,z0,c,
								x1,y1,z1,c,
								x1,y0,z1,c,

								x0,y1,z0,c,
								x0,y1,z1,c,
								x1,y1,z1,c,
								x1,y1,z0,c,

								x1,y0,z0,c,
								x1,y0,z1,c,
								x0,y0,z1,c,
								x0,y0,z0,c
						};
						ByteBuffer bytebuffer = MemoryTracker.create(vertexData.length * 4);
						bytebuffer.asIntBuffer().put(vertexData);

						buffer.putBulkData(bytebuffer);
					}
				}
			}
		}

	}


	public static void render(float partialTicks, PoseStack matrixStack)
	{

		long time = System.currentTimeMillis();
		if(list==null)
			return;

		matrixStack.pushPose();

		synchronized (list)
		{
			Iterator<SoftReference<AirChunk>> iter = list.iterator();
			while(iter.hasNext())
			{
				SoftReference<AirChunk> soft = iter.next();
				AirChunk air = soft.get();
				if(air!=null)
				{
					if(time - air.time > 1000 * 60)
					{
						iter.remove();
					}
					else
					{
						air.render(partialTicks, matrixStack);
					}
				}
				else
				{
					iter.remove();
				}
			}
		}

		matrixStack.popPose();
	}

}
