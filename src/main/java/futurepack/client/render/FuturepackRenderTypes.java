package futurepack.client.render;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.VertexFormat;

import net.minecraft.client.renderer.RenderStateShard;
import net.minecraft.client.renderer.RenderType;

public abstract class FuturepackRenderTypes extends RenderType
{
	public static final RenderStateShard.TransparencyStateShard HOLOGRAM_TRANSPARENCY = new RenderStateShard.TransparencyStateShard("hologram_transparency", () -> {
		RenderSystem.enableBlend();
		RenderSystem.blendFunc(GlStateManager.SourceFactor.ONE_MINUS_DST_COLOR, GlStateManager.DestFactor.ONE);
	}, () -> {
		RenderSystem.disableBlend();
		RenderSystem.defaultBlendFunc();
	});
	protected static final RenderStateShard.TransparencyStateShard ROOM_TRANSPARENCY = new RenderStateShard.TransparencyStateShard("lightning_transparency", () -> {
	      RenderSystem.enableBlend();
	      RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
	   }, () -> {
	      RenderSystem.disableBlend();
	      RenderSystem.defaultBlendFunc();
	   });
	//262144 is same as used in translucent
	public static final RenderType HOLOGRAM = create("hologram",
			DefaultVertexFormat.BLOCK,
			VertexFormat.Mode.QUADS,
			262144,
			true,
			true,
			RenderType.CompositeState.builder()
			.setShaderState(RENDERTYPE_CUTOUT_SHADER)
			.setLightmapState(LIGHTMAP)
			.setTextureState(BLOCK_SHEET_MIPPED)
			.setTransparencyState(HOLOGRAM_TRANSPARENCY)
			.createCompositeState(true));
	public static final RenderType CLAIME = create("CLAIME",
			DefaultVertexFormat.POSITION_COLOR_LIGHTMAP,
			VertexFormat.Mode.QUADS,
			256, false, false,
			RenderType.CompositeState.builder()
			.setShaderState(RENDERTYPE_LEASH_SHADER)
			.setTextureState(NO_TEXTURE)
			.setCullState(NO_CULL)
			.setLightmapState(LIGHTMAP)
			.createCompositeState(false));

	public static final RenderType ROOM_ANALYZER = create("room_analyzer",
			DefaultVertexFormat.POSITION_COLOR,
			VertexFormat.Mode.QUADS,
			256, false, true,
			RenderType.CompositeState.builder()
			.setShaderState(RENDERTYPE_LIGHTNING_SHADER)
			.setWriteMaskState(COLOR_DEPTH_WRITE)
			.setTransparencyState(ROOM_TRANSPARENCY)
			.setOutputState(WEATHER_TARGET).createCompositeState(false));



	public FuturepackRenderTypes(String name, VertexFormat format, VertexFormat.Mode renderMode, int p_173181_, boolean p_173182_, boolean p_173183_, Runnable p_173184_, Runnable p_173185_)
	{
		super(name, format, renderMode, p_173181_, p_173182_, p_173183_, p_173184_, p_173185_);
	}

}
