package futurepack.client.render;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Matrix3f;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.common.entity.EntityMonitor;
import futurepack.common.entity.EntityMonitor.EnumMonitor;
import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class RenderMonitor extends EntityRenderer<EntityMonitor> 
{
	private static final ResourceLocation paintings = new ResourceLocation(Constants.MOD_ID, "textures/dekomonitore.png");

	public RenderMonitor(EntityRendererProvider.Context renderManagerIn) 
	{
		super(renderManagerIn);
	}

	public void render(EntityMonitor entityIn, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn) 
	{
		matrixStackIn.pushPose();
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(180.0F - entityYaw));
		float f = 0.0625F;
		matrixStackIn.scale(0.0625F, 0.0625F, 0.0625F);
		VertexConsumer ivertexbuilder = bufferIn.getBuffer(RenderType.entitySolid(this.getTextureLocation(entityIn)));
		this.renderPainting(matrixStackIn, ivertexbuilder, entityIn, entityIn.getWidth(), entityIn.getHeight(), entityIn.getArt(), EnumMonitor.Background);
		matrixStackIn.popPose();
		super.render(entityIn, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}

	/**
	 * Returns the location of an entity's texture.
	 */
	public ResourceLocation getTextureLocation(EntityMonitor entity) 
	{
		return paintings;
	}

	private void renderPainting(PoseStack matSTack, VertexConsumer buffer, EntityMonitor entity, int p_229122_4_, int p_229122_5_, EnumMonitor p_229122_6_, EnumMonitor p_229122_7_) 
	{
		PoseStack.Pose matrixstack$entry = matSTack.last();
		Matrix4f matrix4f = matrixstack$entry.pose();
		Matrix3f matrix3f = matrixstack$entry.normal();
		float f = (float) (-p_229122_4_) / 2.0F;
		float f1 = (float) (-p_229122_5_) / 2.0F;
		float f2 = 0.5F;
		float f3 = p_229122_7_.getMinU();
		float f4 = p_229122_7_.getMaxU();
		float f5 = p_229122_7_.getMinV();
		float f6 = p_229122_7_.getMaxV();
		float f7 = p_229122_7_.getMinU();
		float f8 = p_229122_7_.getMaxU();
		float f9 = p_229122_7_.getMinV();
		float f10 = p_229122_7_.getInterpolatedV(1.0D);
		float f11 = p_229122_7_.getMinU();
		float f12 = p_229122_7_.getInterpolatedU(1.0D);
		float f13 = p_229122_7_.getMinV();
		float f14 = p_229122_7_.getMaxV();
		int i = p_229122_4_ / 16;
		int j = p_229122_5_ / 16;
		double d0 = 16.0D / (double) i;
		double d1 = 16.0D / (double) j;

		for (int k = 0; k < i; ++k) {
			for (int l = 0; l < j; ++l) {
				float x0 = f + (float) ((k + 1) * 16);
				float x1 = f + (float) (k * 16);
				float y1 = f1 + (float) ((l + 1) * 16);
				float y0 = f1 + (float) (l * 16);
				int i1 = Mth.floor(entity.getX());
				int j1 = Mth.floor(entity.getY() + (double) ((y1 + y0) / 2.0F / 16.0F));
				int k1 = Mth.floor(entity.getZ());
				Direction direction = entity.getDirection();
				if (direction == Direction.NORTH) {
					i1 = Mth.floor(entity.getX() + (double) ((x0 + x1) / 2.0F / 16.0F));
				}

				if (direction == Direction.WEST) {
					k1 = Mth.floor(entity.getZ() - (double) ((x0 + x1) / 2.0F / 16.0F));
				}

				if (direction == Direction.SOUTH) {
					i1 = Mth.floor(entity.getX() - (double) ((x0 + x1) / 2.0F / 16.0F));
				}

				if (direction == Direction.EAST) {
					k1 = Mth.floor(entity.getZ() + (double) ((x0 + x1) / 2.0F / 16.0F));
				}

				int l1 = LevelRenderer.getLightColor(entity.level, new BlockPos(i1, j1, k1));
				float z1 = p_229122_6_.getInterpolatedU(d0 * (double) (i - k));
				float z0 = p_229122_6_.getInterpolatedU(d0 * (double) (i - (k + 1)));
				float f21 = p_229122_6_.getInterpolatedV(d1 * (double) (j - l));
				float f22 = p_229122_6_.getInterpolatedV(d1 * (double) (j - (l + 1)));
				this.putEdge(matrix4f, matrix3f, buffer, x0, y0, z0, f21, -0.5F, 0, 0, -1, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y0, z1, f21, -0.5F, 0, 0, -1, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y1, z1, f22, -0.5F, 0, 0, -1, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y1, z0, f22, -0.5F, 0, 0, -1, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y1, f3, f5, 0.5F, 0, 0, 1, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y1, f4, f5, 0.5F, 0, 0, 1, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y0, f4, f6, 0.5F, 0, 0, 1, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y0, f3, f6, 0.5F, 0, 0, 1, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y1, f7, f9, -0.5F, 0, 1, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y1, f8, f9, -0.5F, 0, 1, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y1, f8, f10, 0.5F, 0, 1, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y1, f7, f10, 0.5F, 0, 1, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y0, f7, f9, 0.5F, 0, -1, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y0, f8, f9, 0.5F, 0, -1, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y0, f8, f10, -0.5F, 0, -1, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y0, f7, f10, -0.5F, 0, -1, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y1, f12, f13, 0.5F, -1, 0, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y0, f12, f14, 0.5F, -1, 0, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y0, f11, f14, -0.5F, -1, 0, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x0, y1, f11, f13, -0.5F, -1, 0, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y1, f12, f13, -0.5F, 1, 0, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y0, f12, f14, -0.5F, 1, 0, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y0, f11, f14, 0.5F, 1, 0, 0, l1);
				this.putEdge(matrix4f, matrix3f, buffer, x1, y1, f11, f13, 0.5F, 1, 0, 0, l1);
			}
		}

	}

	private void putEdge(Matrix4f mat, Matrix3f normalMat, VertexConsumer buffer, float x, float y, float u, float v, float z, int normalX, int normalY, int normalZ, int lightMap) 
	{
		buffer.vertex(mat, x, y, z).color(255, 255, 255, 255)
				.uv(u, v).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(lightMap)
				.normal(normalMat, (float) normalX, (float) normalY, (float) normalZ).endVertex();
	}
}
