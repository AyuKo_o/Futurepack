package futurepack.client.render.block;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.platform.GlStateManager.DestFactor;
import com.mojang.blaze3d.platform.GlStateManager.SourceFactor;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.logistic.BlockWireRedstone;
import futurepack.common.block.logistic.RedstoneSystem;
import futurepack.common.block.logistic.RedstoneSystem.EnumRedstone;
import futurepack.common.block.logistic.TileEntityWireRedstone;
import futurepack.depend.api.helper.HelperRenderBlocks;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RedStoneWireBlock;
import net.minecraft.world.level.block.state.BlockState;

public class RenderWireRedstone extends RenderFastHologram<TileEntityWireRedstone>
{
	public RenderWireRedstone(BlockEntityRendererProvider.Context rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	private void renderWire(TileEntityWireRedstone t,double x, double y, double z, float f1, PoseStack matrixStack, final MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		matrixStack.pushPose();
		GlStateManager._blendFunc(SourceFactor.ONE.value, DestFactor.ZERO.value);
		matrixStack.translate(x, y, z);

		Level w = t.getLevel();
		BlockPos jkl = t.getBlockPos();
		BlockState state = t.getBlockState();

		if(state.hasProperty(BlockWireRedstone.REDSTONE) && state.getValue(BlockWireRedstone.REDSTONE).booleanValue()
				&& state.hasProperty(BlockWireRedstone.REDSTONE_DUST) && state.getValue(BlockWireRedstone.REDSTONE_DUST).booleanValue())
		{
			renderRedstone(w, jkl, Direction.SOUTH, state.getValue(RedstoneSystem.STATE)!=EnumRedstone.OFF, matrixStack, bufferIn, combinedLightIn, combinedOverlayIn);
		}


		matrixStack.popPose();
	}

	@Override
	public final void renderDefault(TileEntityWireRedstone tile, float partialTicks, PoseStack matrixStackIn, final MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		BlockPos pos = tile.getBlockPos();

		renderWire(tile, pos.getX(), pos.getY(), pos.getZ(), partialTicks, matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn);
	}

	private void renderRedstone(Level w, BlockPos pos, Direction face, boolean on, PoseStack matrixStackIn, final MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		w.getProfiler().push("renderRedstone");
		BlockState state = Blocks.REDSTONE_WIRE.defaultBlockState().setValue(RedStoneWireBlock.POWER, on ? 15 : 0);
		state = Blocks.REDSTONE_WIRE.updateShape(state, Direction.UP, state, w, pos, pos.above());
		HelperRenderBlocks.renderBlockSlow(state, pos, w, matrixStackIn, bufferIn, true);
		w.getProfiler().pop();
	}
}
