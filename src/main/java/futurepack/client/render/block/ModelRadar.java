package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import futurepack.api.Constants;
import net.minecraft.client.model.Model;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;

public class ModelRadar extends Model {
//    private final ModelPart Radar;
    private final ModelPart Spiegel;
//    private final ModelPart boneWest;
//    private final ModelPart SpiegelWest_r1;
//    private final ModelPart boneOst;
//    private final ModelPart SpiegelOst_r1;
//    private final ModelPart boneParabolEmpf;
//    private final ModelPart EmpfArm_r1;
//    private final ModelPart boneLMB;

    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "radar"), "main");
	private final ModelPart Radar;

	public ModelRadar(ModelPart root) 
	{
		super(RenderType::entityCutout);
		this.Radar = root.getChild("Radar");
		Spiegel = Radar.getChild("Spiegel");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition Radar = partdefinition.addOrReplaceChild("Radar", CubeListBuilder.create().texOffs(0, 0).addBox(-8.0F, -6.0F, -8.0F, 16.0F, 6.0F, 16.0F, new CubeDeformation(0.0F))
		.texOffs(49, 0).addBox(-4.0F, -7.0F, -4.0F, 8.0F, 1.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition Spiegel = Radar.addOrReplaceChild("Spiegel", CubeListBuilder.create().texOffs(64, 15).addBox(-2.5F, -2.0F, -2.5F, 5.0F, 2.0F, 5.0F, new CubeDeformation(0.0F))
		.texOffs(0, 23).addBox(-1.5F, -3.0F, -4.0F, 3.0F, 2.0F, 10.0F, new CubeDeformation(0.0F))
		.texOffs(17, 24).addBox(-4.0F, -8.0F, 5.5F, 8.0F, 6.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -7.0F, 0.0F));

		PartDefinition boneWest = Spiegel.addOrReplaceChild("boneWest", CubeListBuilder.create(), PartPose.offset(4.0F, -5.0F, 6.0F));

		PartDefinition SpiegelWest_r1 = boneWest.addOrReplaceChild("SpiegelWest_r1", CubeListBuilder.create().texOffs(36, 25).addBox(-2.0F, -7.5F, 6.5F, 6.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-4.0F, 5.0F, -6.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition boneOst = Spiegel.addOrReplaceChild("boneOst", CubeListBuilder.create(), PartPose.offset(-4.0F, -5.0F, 6.0F));

		PartDefinition SpiegelOst_r1 = boneOst.addOrReplaceChild("SpiegelOst_r1", CubeListBuilder.create().texOffs(51, 25).addBox(-4.0F, -7.5F, 6.5F, 6.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(4.0F, 5.0F, -6.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition boneParabolEmpf = Spiegel.addOrReplaceChild("boneParabolEmpf", CubeListBuilder.create(), PartPose.offset(0.0F, -2.0F, -4.0F));

		PartDefinition EmpfArm_r1 = boneParabolEmpf.addOrReplaceChild("EmpfArm_r1", CubeListBuilder.create().texOffs(0, 7).addBox(-1.0F, -0.6F, -3.5F, 2.0F, 2.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.4F, 0.5F, -0.6109F, 0.0F, 0.0F));

		PartDefinition boneLMB = boneParabolEmpf.addOrReplaceChild("boneLMB", CubeListBuilder.create().texOffs(0, 0).addBox(-1.5F, -2.0F, -1.0F, 3.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(0, 25).addBox(-1.0F, -1.6F, 2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -2.4F, -2.5F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}
    
//    public ModelRadar() {
//        super(RenderType::entityCutout);
//        texWidth = 128;
//        texHeight = 128;
//
//        Radar = new ModelPart(this);
//        Radar.setPos(0.0F, 24.0F, 0.0F);
//        Radar.texOffs(0, 0).addBox(-8.0F, -6.0F, -8.0F, 16.0F, 6.0F, 16.0F, 0.0F, false);
//        Radar.texOffs(49, 0).addBox(-4.0F, -7.0F, -4.0F, 8.0F, 1.0F, 8.0F, 0.0F, false);
//
//        Spiegel = new ModelPart(this);
//        Spiegel.setPos(0.0F, -7.0F, 0.0F);
//        Radar.addChild(Spiegel);
//        Spiegel.texOffs(64, 15).addBox(-2.5F, -2.0F, -2.5F, 5.0F, 2.0F, 5.0F, 0.0F, false);
//        Spiegel.texOffs(0, 23).addBox(-1.5F, -3.0F, -4.0F, 3.0F, 2.0F, 10.0F, 0.0F, false);
//        Spiegel.texOffs(17, 24).addBox(-4.0F, -8.0F, 5.5F, 8.0F, 6.0F, 1.0F, 0.0F, false);
//
//        boneWest = new ModelPart(this);
//        boneWest.setPos(4.0F, -5.0F, 6.0F);
//        Spiegel.addChild(boneWest);
//
//
//        SpiegelWest_r1 = new ModelPart(this);
//        SpiegelWest_r1.setPos(-4.0F, 5.0F, -6.0F);
//        boneWest.addChild(SpiegelWest_r1);
//        setRotationAngle(SpiegelWest_r1, 0.0F, 0.7854F, 0.0F);
//        SpiegelWest_r1.texOffs(36, 25).addBox(-2.0F, -7.5F, 6.5F, 6.0F, 5.0F, 1.0F, 0.0F, false);
//
//        boneOst = new ModelPart(this);
//        boneOst.setPos(-4.0F, -5.0F, 6.0F);
//        Spiegel.addChild(boneOst);
//
//
//        SpiegelOst_r1 = new ModelPart(this);
//        SpiegelOst_r1.setPos(4.0F, 5.0F, -6.0F);
//        boneOst.addChild(SpiegelOst_r1);
//        setRotationAngle(SpiegelOst_r1, 0.0F, -0.7854F, 0.0F);
//        SpiegelOst_r1.texOffs(51, 25).addBox(-4.0F, -7.5F, 6.5F, 6.0F, 5.0F, 1.0F, 0.0F, false);
//
//        boneParabolEmpf = new ModelPart(this);
//        boneParabolEmpf.setPos(0.0F, -2.0F, -4.0F);
//        Spiegel.addChild(boneParabolEmpf);
//
//
//        EmpfArm_r1 = new ModelPart(this);
//        EmpfArm_r1.setPos(0.0F, -0.4F, 0.5F);
//        boneParabolEmpf.addChild(EmpfArm_r1);
//        setRotationAngle(EmpfArm_r1, -0.6109F, 0.0F, 0.0F);
//        EmpfArm_r1.texOffs(0, 7).addBox(-1.0F, -0.6F, -3.5F, 2.0F, 2.0F, 4.0F, 0.0F, false);
//
//        boneLMB = new ModelPart(this);
//        boneLMB.setPos(0.0F, -2.4F, -2.5F);
//        boneParabolEmpf.addChild(boneLMB);
//        boneLMB.texOffs(0, 0).addBox(-1.5F, -2.0F, -1.0F, 3.0F, 3.0F, 3.0F, 0.0F, false);
//        boneLMB.texOffs(0, 25).addBox(-1.0F, -1.6F, 2.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
//    }

    public void rotateSpiegel(float degree) 
    {
        Spiegel.yRot = degree;
    }

    @Override
    public void renderToBuffer(PoseStack matrixStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
        Radar.render(matrixStack, buffer, packedLight, packedOverlay);
    }

    public void setRotationAngle(ModelPart modelRenderer, float x, float y, float z) {
        modelRenderer.xRot = x;
        modelRenderer.yRot = y;
        modelRenderer.zRot = z;
    }
}
