package futurepack.client.render.block;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.interfaces.tilentity.ITileHologramAble;
import futurepack.client.render.FuturepackRenderTypes;
import futurepack.depend.api.helper.HelperHologram;
import futurepack.depend.api.helper.HelperRenderBlocks;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.world.level.block.entity.BlockEntity;

public class RenderFastHologram<T extends BlockEntity & ITileHologramAble> implements BlockEntityRenderer<T>
{
	
	
	public RenderFastHologram(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
//		super(rendererDispatcherIn);
	}
	
	@Override
	public final void render(T te, float partialTicks, PoseStack matrixStackIn, final MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		te.getLevel().getProfiler().push("renderHologram");
		
		boolean debug = HelperHologram.isHologramDebug();
		
		te.getLevel().getProfiler().push("other");
		if(debug || (!((ITileHologramAble)te) .hasHologram()))
			renderDefault(te, partialTicks, matrixStackIn, bufferIn, combinedLightIn, combinedOverlayIn);
		
		te.getLevel().getProfiler().popPush("hologram");
		
		MultiBufferSource hologramBuffer = debug ? (t -> bufferIn.getBuffer(FuturepackRenderTypes.HOLOGRAM)) : bufferIn;
		
		if(te.hasHologram())
		{
			renderHologram(te, partialTicks, matrixStackIn, hologramBuffer, combinedLightIn, combinedOverlayIn);
		}
		te.getLevel().getProfiler().pop();
		te.getLevel().getProfiler().pop();
	}

	public void renderDefault(T te, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		
	}

	public final void renderHologram(T te, float partialTicks, PoseStack matrixStackIn, final MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn)
	{
		if(te.hasHologram()) 
		{
			HelperRenderBlocks.renderBlock(te.getHologram(), te.getBlockPos(), te.getLevel(), matrixStackIn, bufferIn);
		}
	}
	
//	@Override
//    public final void render(T te, double x, double y, double z, float partialTicks, int destroyStage)
//    {	
//		te.getWorld().getProfiler().startSection("renderHologramWrapper");
//		
//		boolean debug = HelperHologram.isHologramDebug();
//		
//		te.getWorld().getProfiler().startSection("renderSlow");
//		if(debug || (!((ITileHologramAble)te) .hasHologram()))
//			renderSlow(te, x, y, z, partialTicks, destroyStage);
//		te.getWorld().getProfiler().endStartSection("renderTESR");
//
//		if(debug)
//		{
//			te.getWorld().getProfiler().startSection("setup");
//			Tessellator tessellator = Tessellator.getInstance();
//			BufferBuilder buffer = tessellator.getBuffer();
//			this.bindTexture(AtlasTexture.LOCATION_BLOCKS_TEXTURE);
//			RenderHelper.disableStandardItemLighting();
//			GlStateManager.enableBlend();
//			GlStateManager.disableCull();
//			if(debug)
//			{
//				GL11.glBlendFunc(GL11.GL_ONE_MINUS_DST_COLOR, GL11.GL_ONE);
//			}
//			else
//			{
//				GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
//			}  
//		
//			if (Minecraft.isAmbientOcclusionEnabled())
//			{
//				GlStateManager.shadeModel(GL11.GL_SMOOTH);
//			}
//			else
//			{
//				GlStateManager.shadeModel(GL11.GL_FLAT);
//			}
//		
//			buffer.begin(VertexFormat.Mode.QUADS, DefaultVertexFormats.BLOCK);
//			te.getWorld().getProfiler().endSection();
//			renderTileEntityFast(te, x, y, z, partialTicks, destroyStage, buffer);
//			buffer.setTranslation(0, 0, 0);
//		
//			te.getWorld().getProfiler().startSection("draw");
//			tessellator.draw();
//			te.getWorld().getProfiler().endStartSection("cleanup");
//			RenderHelper.enableStandardItemLighting();
//			if(debug)
//			{
//				GlStateManager.disableBlend();
//				GlStateManager.enableCull();
//				GlStateManager.blendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
//			}
//			te.getWorld().getProfiler().endSection();
//		}
//        te.getWorld().getProfiler().endSection();    
//        te.getWorld().getProfiler().endSection();
//    }
//	
//	
//    public void renderSlow(T te, double x, double y, double z, float partialTicks, int destroyStage)
//    {
//		
//    }
}
