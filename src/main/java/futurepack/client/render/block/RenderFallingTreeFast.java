package futurepack.client.render.block;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.common.block.misc.TileEntityFallingTree;
import futurepack.depend.api.MiniWorld;
import futurepack.depend.api.helper.HelperRenderBlocks;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;

public class RenderFallingTreeFast implements BlockEntityRenderer<TileEntityFallingTree>
{

	public RenderFallingTreeFast(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
//		super(rendererDispatcherIn);
	}
	
	@Override
	public boolean shouldRenderOffScreen(TileEntityFallingTree te)
	{
		return true;
	}

	@Override
	public void render(TileEntityFallingTree te, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		GlStateManager._enableCull();
		
		MiniWorld world = te.getMiniWorld();
		if(world!=null)
		{
			if(te.maxticks==0)
				te.maxticks=1200;
			float progress =  1F-( ( te.ticks-partialTicks ) /te.maxticks);
			world.setRotation(90F * progress );
			HelperRenderBlocks.renderFast(world, partialTicks, matrixStackIn, bufferIn);
			
			//HelperRenderBlocks.renderFastBase(world, buf); // <-- geht
		}
	}

}
