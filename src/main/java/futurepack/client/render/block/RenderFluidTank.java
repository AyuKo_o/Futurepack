package futurepack.client.render.block;

import java.util.function.Function;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Matrix3f;
import com.mojang.math.Matrix4f;

import futurepack.api.interfaces.IFluidTankInfo.FluidTankInfo;
import futurepack.common.block.logistic.TileEntityFluidTank;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.BlockEntityRenderer;
import net.minecraft.client.renderer.blockentity.BlockEntityRendererProvider;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.inventory.InventoryMenu;
import net.minecraft.world.level.Level;

public class RenderFluidTank implements BlockEntityRenderer<TileEntityFluidTank>
{
	public RenderFluidTank(BlockEntityRendererProvider.Context rendererDispatcherIn) 
	{
//		super(rendererDispatcherIn);
	}

	public static void renderTank(float x, float y, float z, float w, float h, float d, FluidTankInfo info, MultiBufferSource bufferIn, PoseStack matrixStackIn, Level world, BlockPos pos)
	{
		int color = info.getFluidStack().getFluid().getAttributes().getColor();
		try
		{
			color = info.getFluidStack().getFluid().getAttributes().getColor(world, pos);
		}
		catch(Exception e) {}
		
		int block = info.getFluidStack().getFluid().getAttributes().getLuminosity(info.getFluidStack());
		int combinedLightIn = LevelRenderer.getLightColor(world, pos);
		block = Math.max(block, 15 & (combinedLightIn >> 4));
		combinedLightIn = combinedLightIn | (block << 4);
		
		renderTank(x, y, z, w, h, d, info, bufferIn, matrixStackIn, combinedLightIn, color);
	}
	
	public static void renderTank(float x, float yy, float z, float w, float hh, float d, FluidTankInfo info, MultiBufferSource bufferIn, PoseStack matrixStackIn, int combinedLightIn, int color)
	{		

		float maxH = hh;
		hh *= info.getFluidStack().getAmount()/(float)info.getCapacity();
		
		if(info.getFluidStack().getFluid().getAttributes().isGaseous(info.getFluidStack()))
		{
			yy = yy+ maxH - hh;
		}
		
		final float h = hh, y = yy;
		
		Matrix4f matrix = matrixStackIn.last().pose();
		Matrix3f normals = matrixStackIn.last().normal();
		
		
		int red = ((color>>16) & 0xFF);
		int green = ((color>>8) & 0xFF);
		int blue = ((color>>0) & 0xFF);
		int alpha = 255;
		
		
		RenderType type = ItemBlockRenderTypes.canRenderInLayer(info.getFluidStack().getFluid().defaultFluidState(), RenderType.solid()) ? RenderType.solid() : RenderType.translucentNoCrumbling();
		
		VertexConsumer builder = bufferIn.getBuffer(type);//RenderType.getTranslucentNoCrumbling());
		//add(POSITION_3F).add(COLOR_4UB).add(TEX_2F).add(TEX_2SB).add(NORMAL_3B).add(PADDING_1B)
		//pos color UV lightmap normals
		
		Function<ResourceLocation, TextureAtlasSprite> map = Minecraft.getInstance().getTextureAtlas(InventoryMenu.BLOCK_ATLAS);
		TextureAtlasSprite texture = map.apply(info.getFluidStack().getFluid().getAttributes().getStillTexture(info.getFluidStack()));
			
		
		
		float minU = texture.getU0();
		float minV = texture.getV0();
		float maxU = texture.getU1();
		float maxV = texture.getV1();
		
		float difU = maxU-minU;
		float difV = maxV-minV;
		
		maxV = minV+ difV * h;
		maxU = minU+ difU * w;
		
		difU /= 16D;
		
		quad(matrix, normals, builder, x, y, z, w, h, 0, minU, minV, maxU, maxV, combinedLightIn, red, green, blue, alpha, 0, 0, 1F);
		quad(matrix, normals, builder, x+w, y, z+d, -w, h, 0, minU, minV, maxU, maxV, combinedLightIn, red, green, blue, alpha, 0, 0, -1F);
		
		quad(matrix, normals, builder, x+w, y, z, 0, h, d, minU, minV, maxU, maxV, combinedLightIn, red, green, blue, alpha, 1F, 0, 0);
		quad(matrix, normals, builder, x, y, z+d, 0, h, -d, minU, minV, maxU, maxV, combinedLightIn, red, green, blue, alpha, -1F, 0, 0);
				
		maxV = minV+ difV * d;
		
		
		builder.vertex(matrix, x + 0, y + h, z + d).color(red, green, blue, alpha).uv(minU, maxV).uv2(combinedLightIn).normal(normals, 0, 1, 0).endVertex();
		builder.vertex(matrix, x + w, y + h, z + d).color(red, green, blue, alpha).uv(maxU, maxV).uv2(combinedLightIn).normal(normals, 0, 1, 0).endVertex();
		builder.vertex(matrix, x + w, y + h, z + 0).color(red, green, blue, alpha).uv(maxU, minV).uv2(combinedLightIn).normal(normals, 0, 1, 0).endVertex();
		builder.vertex(matrix, x + 0, y + h, z + 0).color(red, green, blue, alpha).uv(minU, minV).uv2(combinedLightIn).normal(normals, 0, 1, 0).endVertex();
		
		builder.vertex(matrix, x + w, y, z + d).color(red, green, blue, alpha).uv(minU, maxV).uv2(combinedLightIn).normal(normals, 0, -1, 0).endVertex();
		builder.vertex(matrix, x + 0, y, z + d).color(red, green, blue, alpha).uv(maxU, maxV).uv2(combinedLightIn).normal(normals, 0, -1, 0).endVertex();
		builder.vertex(matrix, x + 0, y, z + 0).color(red, green, blue, alpha).uv(maxU, minV).uv2(combinedLightIn).normal(normals, 0, -1, 0).endVertex();
		builder.vertex(matrix, x + w, y, z + 0).color(red, green, blue, alpha).uv(minU, minV).uv2(combinedLightIn).normal(normals, 0, -1, 0).endVertex();
	
		
	}
	
	public static void quad(Matrix4f matrix, Matrix3f normals, VertexConsumer builder ,float x, float y, float z, float w, float h, float d, float minU, float minV, float maxU, float maxV, int combinedLightIn, int red, int green, int blue, int alpha, float normalX, float normalY, float normalZ)
	{  		
		builder.vertex(matrix, x + 0, y + h, z + 0).color(red, green, blue, alpha).uv(minU, maxV).uv2(combinedLightIn).normal(normals, normalX, normalY, normalZ).endVertex();
		builder.vertex(matrix, x + w, y + h, z + d).color(red, green, blue, alpha).uv(maxU, maxV).uv2(combinedLightIn).normal(normals, normalX, normalY, normalZ).endVertex();
		builder.vertex(matrix, x + w, y + 0, z + d).color(red, green, blue, alpha).uv(maxU, minV).uv2(combinedLightIn).normal(normals, normalX, normalY, normalZ).endVertex();
		builder.vertex(matrix, x + 0, y + 0, z + 0).color(red, green, blue, alpha).uv(minU, minV).uv2(combinedLightIn).normal(normals, normalX, normalY, normalZ).endVertex();
	}
	
	@Override
	public void render(TileEntityFluidTank te, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int combinedLightIn, int combinedOverlayIn) 
	{
		te.getLevel().getProfiler().push("renderFluidTank");
		FluidTankInfo info = te.getTankInfo();
		if(info!=null )
		{	
			if(te.getLastTankInfo().getFluidStack().isEmpty())
			{
				te.getLevel().getProfiler().pop();
				return;
			}
				
			matrixStackIn.pushPose();
			
			FluidTankInfo fake = te.getLastTankInfo();
			float d = 0.0625F;
			
			if(!fake.getFluidStack().isEmpty())
			{				
				if(te.isInputAktiv(Direction.UP))
				{
					renderTank(d*7, 0.005F, d*7, d*2, 0.99F, d*2, fake, bufferIn, matrixStackIn, te.getLevel(), te.getBlockPos());
				}
				if(te.isInputAktiv(Direction.WEST))
				{
					renderTank(0.005F, 0.005F, d*7, d*2, d*9, d*2, fake, bufferIn, matrixStackIn, te.getLevel(), te.getBlockPos());
				}
				if(te.isInputAktiv(Direction.EAST))
				{
					renderTank(d*14, 0.005F, d*7, d*2, d*9, d*2, fake, bufferIn, matrixStackIn, te.getLevel(), te.getBlockPos());
				}
				if(te.isInputAktiv(Direction.NORTH))
				{
					renderTank(d*7, 0.005F, 0.005F, d*2, d*9, d*2, fake, bufferIn, matrixStackIn, te.getLevel(), te.getBlockPos());
				}
				if(te.isInputAktiv(Direction.SOUTH))
				{
					renderTank(d*7, 0.005F, d*14, d*2, d*9, d*2, fake, bufferIn, matrixStackIn, te.getLevel(), te.getBlockPos());
				}
			}
				
			if(!info.getFluidStack().isEmpty() && info.getFluidStack().getAmount()>0)
			{
				if(te.getLastFailTime() <= 0 && info.getFluidStack().getAmount() < 100)
				{
					//dont render to prevent fluid squads;
				}
				else
				{
					renderTank(0.005F, 0.005F, 0.005F, 0.99F, 0.99F, 0.99F, info, bufferIn, matrixStackIn, te.getLevel(), te.getBlockPos());
				}
			}
			matrixStackIn.popPose();
		}
		te.getLevel().getProfiler().pop();	
	}
}
