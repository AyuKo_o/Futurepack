package futurepack.client.render.entity;

import futurepack.api.Constants;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.WolfRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.animal.Wolf;

public class RenderJawaul extends WolfRenderer 
{

	public RenderJawaul(EntityRendererProvider.Context renderManagerIn) 
	{
		super(renderManagerIn);
	}

	private static final ResourceLocation WOLF_TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/jawaul/jawaul.png");
	private static final ResourceLocation TAMED_WOLF_TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/jawaul/jawaul_tame.png");
	private static final ResourceLocation ANGRY_WOLF_TEXTURES = new ResourceLocation(Constants.MOD_ID, "textures/entity/jawaul/jawaul_angry.png");

	@Override
	public ResourceLocation getTextureLocation(Wolf entity) 
	{
		if (entity.isTame()) 
		{
			return TAMED_WOLF_TEXTURES;
		}
		else 
		{
			return entity.isAngry() ? ANGRY_WOLF_TEXTURES : WOLF_TEXTURES;
		}
	}
}
