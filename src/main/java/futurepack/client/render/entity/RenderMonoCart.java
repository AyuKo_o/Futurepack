package futurepack.client.render.entity;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.common.entity.monocart.EntityMonocart;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;

public class RenderMonoCart extends EntityRenderer<EntityMonocart>
{
	private ModelMonoCart model;
	
	public RenderMonoCart(EntityRendererProvider.Context m)
	{
		super(m);
		model = new ModelMonoCart(m.bakeLayer(ModelMonoCart.LAYER_LOCATION));
	}
	
	@Override
	public void render(EntityMonocart entity, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn) 
	{
		entity.getCommandSenderWorld().getProfiler().push("renderMonocart");
		matrixStackIn.pushPose();
		
		super.render(entity, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		
		VertexConsumer buf = bufferIn.getBuffer(model.renderType(getTextureLocation(entity)));
		
		float y = 0F;
		if(entity.getXRot()!=0)
			y+=0.1;
		if(!entity.isRolling() && entity.getPower()<1F)
			y+=0.2;
		matrixStackIn.translate(0, y + 0.65 -0.4, 0);
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(180F));
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(entity.getYRot()+90));
		matrixStackIn.mulPose(Vector3f.ZP.rotationDegrees(entity.getXRot()));
		matrixStackIn.scale(0.99F, 0.99F, 0.99F);
		model.setupAnim(entity, entityYaw, partialTicks, packedLightIn, y, y);
		model.renderToBuffer(matrixStackIn, buf, packedLightIn, OverlayTexture.NO_OVERLAY, 1F, 1F, 1F, 1F);
		matrixStackIn.popPose();
		
		entity.getCommandSenderWorld().getProfiler().pop();
	}
	
	
	@Override
	public ResourceLocation getTextureLocation(EntityMonocart entity)
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/monocart.png");
	}

}
