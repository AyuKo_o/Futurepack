package futurepack.client.render.entity;

import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityEvilRobot;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.LivingEntityRenderer;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.resources.ResourceLocation;

public class LayerEvilRobot extends RenderLayer<EntityEvilRobot, ModelEvilRobot>
{

	private static final ResourceLocation TEX_OVER = new ResourceLocation(Constants.MOD_ID, "textures/entity/bot_overlay.png");
	private final LivingEntityRenderer<EntityEvilRobot, ModelEvilRobot> renderer;
	private ModelEvilRobot layerModel;
	
	public LayerEvilRobot(LivingEntityRenderer<EntityEvilRobot, ModelEvilRobot> p_i47131_1_, ModelEvilRobot modelEvilRobot)
	{
		super(p_i47131_1_);
		this.renderer = p_i47131_1_;
		this.layerModel = modelEvilRobot;
	}

	@Override
	public void render(PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn, EntityEvilRobot entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch) 
	{
		if(entitylivingbaseIn.getState().hasArmor)
		{
			coloredCutoutModelCopyLayerRender(this.getParentModel(), this.layerModel, TEX_OVER, matrixStackIn, bufferIn, packedLightIn, entitylivingbaseIn, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, partialTicks, 1F, 1F, 1F);
		}
	}

}
