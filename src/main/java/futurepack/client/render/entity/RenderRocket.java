package futurepack.client.render.entity;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.common.entity.throwable.EntityRocket;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;

public class RenderRocket extends EntityRenderer<EntityRocket>
{
	private ModelRocket model;

	public RenderRocket(EntityRendererProvider.Context renderManagerIn)
	{
		super(renderManagerIn);
		model = new ModelRocket(renderManagerIn.bakeLayer(ModelRocket.LAYER_LOCATION));
	}

	@Override
	public void render(EntityRocket e, float entityYaw, float partialTicks, PoseStack matrixStackIn, MultiBufferSource bufferIn, int packedLightIn) 
	{
		matrixStackIn.pushPose();
		
		float yaw = e.yRotO + (e.getYRot() - e.yRotO) * partialTicks;
		float pitch = e.xRotO + (e.getXRot() - e.xRotO) * partialTicks;
		
		matrixStackIn.mulPose(Vector3f.YP.rotationDegrees(yaw + 180));
		matrixStackIn.mulPose(Vector3f.XP.rotationDegrees(pitch));
		
		VertexConsumer builder = bufferIn.getBuffer(model.renderType(getTextureLocation(e)));
		model.renderToBuffer(matrixStackIn, builder, packedLightIn, OverlayTexture.NO_OVERLAY, 1F, 1F, 1F, 1F);
		
		matrixStackIn.popPose();
		super.render(e, entityYaw, partialTicks, matrixStackIn, bufferIn, packedLightIn);
	}
	
	@Override
	public ResourceLocation getTextureLocation(EntityRocket p_110775_1_) 
	{
		return new ResourceLocation(Constants.MOD_ID, "textures/model/rocket.png");
	}
}
