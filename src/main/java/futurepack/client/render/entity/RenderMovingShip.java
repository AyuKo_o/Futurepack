/*package futurepack.client.render.entity;

import java.util.UUID;

import org.lwjgl.opengl.GL11;

import futurepack.common.entity.EntityMovingShipBase;
import futurepack.common.spaceships.SpaceshipCashClient;
import futurepack.depend.api.MiniWorld;
import futurepack.depend.api.helper.HelperRenderBlocks;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.AtlasTexture;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

//TODO: Sky:Auskommentiert bei 1.15 Port weil nicht benötigt aktuell

public class RenderMovingShip extends DefaultRenderer
{

	public RenderMovingShip(EntityRendererManager renderManagerIn)
	{
		super(renderManagerIn);
		
	}
	
	@Override
	public ResourceLocation getEntityTexture(Entity entity)
	{
		return AtlasTexture.LOCATION_BLOCKS_TEXTURE;
	}
	
	private UUID getSpaceshipID(EntityMovingShipBase e)
	{
		return e.getShipID();
	}
	
	@Override
	public void doRender(Entity entity, double x, double y, double z, float entityYaw, float partialTicks) 
	{
		MiniWorld mini = SpaceshipCashClient.getShipFromID(entity.world, getSpaceshipID((EntityMovingShipBase) entity));
		if(mini==null)
		{
			super.doRender(entity, x, y, z, entityYaw, partialTicks);
			return;
		}
		mini.rot = 0;
		
		matrixStack.pushPose();
		bindEntityTexture(entity);
		GL11.glTranslated(x, y +  mini.height/2.0, z);
		GL11.glRotatef(entityYaw, 0, -1, 0);
		matrixStack.mulPose(Vector3f.XP.rotationDegrees(entity.rotationPitch));
		//GL11.glTranslated(mini.width/2.0, mini.height/2.0, mini.depth/2.0);
		
		BufferBuilder builder = Tessellator.getInstance().getBuffer();
		builder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormats.BLOCK);
		
		HelperRenderBlocks.renderFast(mini, builder, 0, 0, 0);
		
		Tessellator.getInstance().draw();
		
		matrixStack.popPose();
	}
}
*/