package futurepack.client.render.entity;
import com.google.common.collect.ImmutableList;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;

import futurepack.api.Constants;
import futurepack.common.entity.living.EntityAlphaJawaul;
import net.minecraft.client.model.ColorableAgeableListModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.phys.Vec3;

public class ModelAlphaJawaul extends ColorableAgeableListModel<EntityAlphaJawaul>
{
	
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(Constants.MOD_ID, "alpha_jawaul"), "main");
	private final ModelPart head;
	private final ModelPart body;
	private final ModelPart upperBody;
	private final ModelPart leg_back_right;
	private final ModelPart leg_back_left;
	private final ModelPart leg_front_right;
	private final ModelPart leg_front_left;
	private final ModelPart tail;

	private final ModelPart doggyBag0;
	private final ModelPart doggyBag1;
	private final ModelPart doggyBag2;
	private final ModelPart saddle;
	
	public ModelAlphaJawaul(ModelPart root) 
	{
		this.head = root.getChild("head");
		this.body = root.getChild("body");
		this.upperBody = root.getChild("upperBody");
		this.leg_back_right = root.getChild("leg_back_right");
		this.leg_back_left = root.getChild("leg_back_left");
		this.leg_front_right = root.getChild("leg_front_right");
		this.leg_front_left = root.getChild("leg_front_left");
		this.tail = root.getChild("tail");
		
		this.saddle = body.getChild("saddle");
		this.doggyBag0 = saddle.getChild("doggyBag0");
		this.doggyBag1 = saddle.getChild("doggyBag1");
		this.doggyBag2 = saddle.getChild("doggyBag2");
		
		ImmutableList.Builder<ModelPart> builder = ImmutableList.builder();
		builder.add(body);
		builder.add(upperBody);
		builder.add(tail);
		
		builder.add(leg_back_right);
		builder.add(leg_back_left);
		builder.add(leg_front_right);
		builder.add(leg_front_left);
		
		this.parts = builder.build();
	}

	public static LayerDefinition createBodyLayer() 
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-3.0F, -4.0F, -2.0F, 6.0F, 6.0F, 4.0F, new CubeDeformation(1.0F))
		.texOffs(16, 14).addBox(-3.0F, -6.9962F, 0.0872F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.5F))
		.texOffs(16, 14).addBox(1.0F, -6.9962F, 0.0872F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.5F))
		.texOffs(0, 10).addBox(-1.5F, -0.0156F, -5.0F, 3.0F, 3.0F, 4.0F, new CubeDeformation(0.75F))
		.texOffs(56, 11).addBox(-2.35F, 1.3F, -5.8F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(56, 11).addBox(1.35F, 1.3F, -5.8F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.0F, 13.5F, -9.0F, 0.0873F, 0.0F, 0.0F));

		PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(18, 14).addBox(-4.0F, -2.0F, -2.0F, 6.0F, 9.0F, 6.0F, new CubeDeformation(1.2F)), PartPose.offsetAndRotation(0.0F, 14.0F, 3.0F, 1.5708F, 0.0F, 0.0F));

		PartDefinition saddle = body.addOrReplaceChild("saddle", CubeListBuilder.create().texOffs(45, 21).addBox(-5.0F, -4.0F, -6.0F, 8.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -1.0F, 0.0F, 0.0F, 3.1416F, -3.1416F));

		PartDefinition doggyBag0 = saddle.addOrReplaceChild("doggyBag0", CubeListBuilder.create().texOffs(43, 13).addBox(0.0F, -2.0F, -4.0F, 2.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.0F, -1.0F, -5.5F, 3.1416F, 0.0F, 0.0F));
		PartDefinition doggyBag1 = saddle.addOrReplaceChild("doggyBag1", CubeListBuilder.create().texOffs(43, 13).addBox(0.0F, -2.0F, -4.0F, 2.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-5.0F, -1.0F, -5.5F, 3.1416F, 0.0F, -3.1416F));
		PartDefinition doggyBag2 = saddle.addOrReplaceChild("doggyBag2", CubeListBuilder.create().texOffs(43, 13).addBox(-0.9F, -2.0F, -4.0F, 2.0F, 4.0F, 4.0F, new CubeDeformation(0.5F)), PartPose.offsetAndRotation(-1.0F, -4.0F, -7.0F, 0.0F, 1.6581F, 1.5708F));

		PartDefinition upperBody = partdefinition.addOrReplaceChild("upperBody", CubeListBuilder.create().texOffs(21, 0).addBox(-4.0F, 2.0F, -4.5F, 8.0F, 6.0F, 7.0F, new CubeDeformation(1.0F)), PartPose.offsetAndRotation(-1.0F, 14.0F, 2.0F, -1.5708F, 0.0F, 0.0F));

		PartDefinition leg_back_right = partdefinition.addOrReplaceChild("leg_back_right", CubeListBuilder.create().texOffs(0, 18).addBox(-1.8F, -1.0F, -2.0F, 2.0F, 8.0F, 2.0F, new CubeDeformation(0.75F)), PartPose.offset(-2.5F, 16.0F, 9.0F));
		PartDefinition leg_back_left = partdefinition.addOrReplaceChild("leg_back_left", CubeListBuilder.create().texOffs(0, 18).addBox(-0.2F, -1.0F, 0.0F, 2.0F, 8.0F, 2.0F, new CubeDeformation(0.75F)), PartPose.offset(0.5F, 16.0F, 7.0F));
		PartDefinition leg_front_right = partdefinition.addOrReplaceChild("leg_front_right", CubeListBuilder.create().texOffs(0, 18).addBox(-2.0F, -1.0F, -1.0F, 2.0F, 8.0F, 2.0F, new CubeDeformation(0.75F)), PartPose.offset(-2.5F, 16.0F, -4.0F));
		PartDefinition leg_front_left = partdefinition.addOrReplaceChild("leg_front_left", CubeListBuilder.create().texOffs(0, 18).addBox(0.0F, -1.0F, -1.0F, 2.0F, 8.0F, 2.0F, new CubeDeformation(0.75F)), PartPose.offset(0.5F, 16.0F, -4.0F));

		PartDefinition tail = partdefinition.addOrReplaceChild("tail", CubeListBuilder.create().texOffs(9, 18).addBox(-1.0F, 1.6383F, 0.1472F, 2.0F, 8.0F, 2.0F, new CubeDeformation(0.75F)), PartPose.offsetAndRotation(-1.0F, 12.0F, 8.0F, 0.9599F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 32);
	}
//	private final ModelPart head, head_baby;
//	private final ModelPart body;


//	private final ModelPart upperBody;
//	private final ModelPart leg_back_right;
//	private final ModelPart leg_back_left;
//	private final ModelPart leg_front_right;
//	private final ModelPart leg_front_left;
//	private final ModelPart tail;
//
//	public ModelAlphaJawaul()
//	{
//		texWidth = 64;
//		texHeight = 32;
//
//		head = new ModelPart(this);
//		head.setPos(-1.0F, 13.5F, -9.0F);
//		setRotationAngle(head, 0.0873F, 0.0F, 0.0F);
//		head.texOffs(0, 0).addBox(-3.0F, -4.0F, -2.0F, 6.0F, 6.0F, 4.0F, 1.0F, false);
//		head.texOffs(16, 14).addBox(-3.0F, -6.9962F, 0.0872F, 2.0F, 2.0F, 1.0F, 0.5F, false);
//		head.texOffs(16, 14).addBox(1.0F, -6.9962F, 0.0872F, 2.0F, 2.0F, 1.0F, 0.5F, false);
//		head.texOffs(0, 10).addBox(-1.5F, -0.0156F, -5.0F, 3.0F, 3.0F, 4.0F, 0.75F, false);
//		//z�hne
//		head.texOffs(56, 11).addBox(-2.35F, 1.3F, -5.8F, 1.0F, 4.0F, 1.0F, 0.0F, false);
//		head.texOffs(56, 11).addBox(1.35F, 1.3F, -5.8F, 1.0F, 4.0F, 1.0F, 0.0F, false);
//
//		//wie normal aber ohne z�hne
//		head_baby = new ModelPart(this);
//		head_baby.setPos(-1.0F, 13.5F, -9.0F);
//		setRotationAngle(head_baby, 0.0873F, 0.0F, 0.0F);
//		head_baby.texOffs(0, 0).addBox(-3.0F, -4.0F, -2.0F, 6.0F, 6.0F, 4.0F, 1.0F, false);
//		head_baby.texOffs(16, 14).addBox(-3.0F, -6.9962F, 0.0872F, 2.0F, 2.0F, 1.0F, 0.5F, false);
//		head_baby.texOffs(16, 14).addBox(1.0F, -6.9962F, 0.0872F, 2.0F, 2.0F, 1.0F, 0.5F, false);
//		head_baby.texOffs(0, 10).addBox(-1.5F, -0.0156F, -5.0F, 3.0F, 3.0F, 4.0F, 0.75F, false);
//		
//		body = new ModelPart(this);
//		body.setPos(0.0F, 14.0F, 3.0F);
//		setRotationAngle(body, 1.5708F, 0.0F, 0.0F);
//		body.texOffs(18, 14).addBox(-4.0F, -2.0F, -2.0F, 6.0F, 9.0F, 6.0F, 1.2F, false);
//
//		saddle = new ModelPart(this);
//		saddle.setPos(0.0F, -1.0F, 0.0F);
//		body.addChild(saddle);
//		setRotationAngle(saddle, 0.0F, 3.1416F, -3.1416F);
//		saddle.texOffs(45, 21).addBox(-5.0F, -4.0F, -6.0F, 8.0F, 8.0F, 1.0F, 0.0F, false);
//
//		doggyBag0 = new ModelPart(this);
//		doggyBag0.setPos(3.0F, -1.0F, -5.5F);
//		saddle.addChild(doggyBag0);
//		setRotationAngle(doggyBag0, 3.1416F, 0.0F, 0.0F);
//		doggyBag0.texOffs(43, 13).addBox(0.0F, -2.0F, -4.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
//
//		doggyBag1 = new ModelPart(this);
//		doggyBag1.setPos(-5.0F, -1.0F, -5.5F);
//		saddle.addChild(doggyBag1);
//		setRotationAngle(doggyBag1, 3.1416F, 0.0F, -3.1416F);
//		doggyBag1.texOffs(43, 13).addBox(0.0F, -2.0F, -4.0F, 2.0F, 4.0F, 4.0F, 0.0F, false);
//
//		doggyBag2 = new ModelPart(this);
//		doggyBag2.setPos(-1.0F, -4.0F, -7.0F);
//		saddle.addChild(doggyBag2);
//		setRotationAngle(doggyBag2, 0.0F, 1.6581F, 1.5708F);
//		doggyBag2.texOffs(43, 13).addBox(-0.9F, -2.0F, -4.0F, 2.0F, 4.0F, 4.0F, 0.5F, false);
//
//		upperBody = new ModelPart(this);
//		upperBody.setPos(-1.0F, 14.0F, 2.0F);
//		setRotationAngle(upperBody, -1.5708F, 0.0F, 0.0F);
//		upperBody.texOffs(21, 0).addBox(-4.0F, 2.0F, -4.5F, 8.0F, 6.0F, 7.0F, 1.0F, false);
//
//		leg_back_right = new ModelPart(this);
//		leg_back_right.setPos(-2.5F, 16.0F, 9.0F);
//		leg_back_right.texOffs(0, 18).addBox(-1.8F, -1.0F, -2.0F, 2.0F, 8.0F, 2.0F, 0.75F, false);
//
//		leg_back_left = new ModelPart(this);
//		leg_back_left.setPos(0.5F, 16.0F, 7.0F);
//		leg_back_left.texOffs(0, 18).addBox(-0.2F, -1.0F, 0.0F, 2.0F, 8.0F, 2.0F, 0.75F, false);
//
//		leg_front_right = new ModelPart(this);
//		leg_front_right.setPos(-2.5F, 16.0F, -4.0F);
//		leg_front_right.texOffs(0, 18).addBox(-2.0F, -1.0F, -1.0F, 2.0F, 8.0F, 2.0F, 0.75F, false);
//
//		leg_front_left = new ModelPart(this);
//		leg_front_left.setPos(0.5F, 16.0F, -4.0F);
//		leg_front_left.texOffs(0, 18).addBox(0.0F, -1.0F, -1.0F, 2.0F, 8.0F, 2.0F, 0.75F, false);
//
//		tail = new ModelPart(this);
//		tail.setPos(-1.0F, 12.0F, 8.0F);
//		setRotationAngle(tail, 0.9599F, 0.0F, 0.0F);
//		tail.texOffs(9, 18).addBox(-1.0F, 1.6383F, 0.1472F, 2.0F, 8.0F, 2.0F, 0.75F, false);
//		

//	}

	@Override
	public void setupAnim(EntityAlphaJawaul entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch)
	{
		this.head.xRot = headPitch * ((float) Math.PI / 180F);
		this.head.yRot = netHeadYaw * ((float) Math.PI / 180F);
//		this.head_baby.xRot = headPitch * ((float) Math.PI / 180F);
//		this.head_baby.yRot = netHeadYaw * ((float) Math.PI / 180F);
		this.tail.xRot = ageInTicks;
	}
	
	private Vec3 sizeMod;
	private boolean no_teeth;
	
	public void prepareMobModel(EntityAlphaJawaul entityIn, float limbSwing, float limbSwingAmount, float partialTick)
	{
		boolean showChest = entityIn.hasChest();
		this.doggyBag0.visible = showChest;
		this.doggyBag1.visible = showChest;
		this.doggyBag2.visible = showChest;
		this.saddle.visible = entityIn.isSaddled();
		
		sizeMod = entityIn.getSizeModifier();
		
		no_teeth = !entityIn.isOldEnough();
		
		if (entityIn.isHorseJumping())
		{
			this.tail.yRot = 0.0F;
		} 
		else
		{
			this.tail.yRot = Mth.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
		}

		if (!entityIn.isVehicle() && entityIn.isInSittingPose())
		{
			this.head.setPos(-1.0F, 17.5F, -9.0F);
			this.upperBody.setPos(-1.0F, 17.0F, 2.0F);
			this.body.setPos(0.0F, 18.0F, 2.0F);
			
			this.leg_back_right.setPos(-2.5F, 21.8F, 8.0F);
			this.leg_back_left.setPos(0.5F, 19.8F, 5.0F);
			this.leg_front_right.setPos(-2.5F, 19.8F, -4.0F);
			this.leg_front_left.setPos(0.5F, 19.8F, -4.0F);
			
			this.leg_front_left.xRot = (float) Math.PI*-0.34F ;
			this.leg_front_left.yRot = (float) Math.PI*-0.14F ;
			this.leg_front_right.xRot = (float) Math.PI*-0.33F ;
			this.leg_front_right.yRot = (float) Math.PI*0.05F ;
			
			this.leg_back_left.xRot = (float) Math.PI*-0.44F ;
			this.leg_back_left.yRot = (float) Math.PI*-0.04F ;
			this.leg_back_right.xRot = (float) Math.PI*-0.43F ;
			this.leg_back_right.yRot = (float) Math.PI*0.02F ;
			
			
			
			
			this.upperBody.xRot = -((float) Math.PI / 2F);
			
//			this.upperBody.setRotationPoint(-1.0F, 16.0F, -3.0F);
//			this.upperBody.rotateAngleX = 1.2566371F;
//			this.upperBody.rotateAngleY = 0.0F;
			
			this.body.xRot = ((float) Math.PI / 2.7F);
//			this.tail.setRotationPoint(-1.0F, 21.0F, 6.0F);
//			this.leg_back_right.setRotationPoint(-2.5F, 22.7F, 2.0F);
//			this.leg_back_right.rotateAngleX = ((float) Math.PI * 1.5F);
//			this.leg_back_left.setRotationPoint(0.5F, 22.7F, 2.0F);
//			this.leg_back_left.rotateAngleX = ((float) Math.PI * 1.5F);
//			this.leg_front_right.rotateAngleX = 5.811947F;
//			this.leg_front_right.setRotationPoint(-2.49F, 17.0F, -4.0F);
//			this.leg_front_left.rotateAngleX = 5.811947F;
//			this.leg_front_left.setRotationPoint(0.51F, 17.0F, -4.0F);
			
			tail.setPos(-1.0F, 20.0F, 7.0F);
			tail.xRot =  (float) (0F * Math.PI);
			tail.zRot =  (float) (0F * Math.PI);
			tail.yRot =  (float) (0F * Math.PI);
		} 
		else
		{
			this.head.setPos(-1.0F, 13.5F, -9.0F);
			
			this.body.setPos(0.0F, 14.0F, 2.0F);
			this.body.xRot = ((float) Math.PI / 2F);
			this.upperBody.setPos(-1.0F, 14.0F, 2.0F);
			this.upperBody.xRot = -this.body.xRot;
			this.tail.setPos(-1.0F, 12.0F, 8.0F);
			
			this.leg_back_right.setPos(-2.5F, 16.0F, 9.0F);
			this.leg_back_left.setPos(0.5F, 16.0F, 7.0F);
			this.leg_front_right.setPos(-2.5F, 16.0F, -4.0F);
			this.leg_front_left.setPos(0.5F, 16.0F, -4.0F);
			
//			this.leg_back_right.setRotationPoint(-2.5F, 16.0F, 7.0F);
//			this.leg_back_left.setRotationPoint(0.5F, 16.0F, 7.0F);
//			this.leg_front_right.setRotationPoint(-2.5F, 16.0F, -4.0F);
//			this.leg_front_left.setRotationPoint(0.5F, 16.0F, -4.0F);
			this.leg_back_right.xRot = Mth.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
			this.leg_back_left.xRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
			this.leg_front_right.xRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
			this.leg_front_left.xRot = Mth.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
			
			this.leg_front_left.yRot = 0F;
			this.leg_front_right.yRot = 0F;
			this.leg_back_left.yRot = 0F;
			this.leg_back_right.yRot = 0F;
			
			tail.setPos(-1.0F, 12.0F, 8.0F);
		}

//		this.headChild.rotateAngleZ = entityIn.getInterestedAngle(partialTick)
//				+ entityIn.getShakeAngle(partialTick, 0.0F);
//		this.mane.rotateAngleZ = entityIn.getShakeAngle(partialTick, -0.08F);
//		this.body.rotateAngleZ = entityIn.getShakeAngle(partialTick, -0.16F);
//		this.tailChild.rotateAngleZ = entityIn.getShakeAngle(partialTick, -0.2F);
	}

	
	
	protected float redTint = 1.0F;
	protected float greenTint = 1.0F;
	protected float blueTint = 1.0F;

	@Override
	public void setColor(float p_228253_1_, float p_228253_2_, float p_228253_3_) 
	{
		this.redTint = p_228253_1_;
		this.greenTint = p_228253_2_;
		this.blueTint = p_228253_3_;
	}	
	
	@Override
	public void renderToBuffer(PoseStack matrixStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha)
	{
		matrixStack.pushPose();
		
		float xyz_scale = (float) sizeMod.x, y_height = (float) sizeMod.y, thickness = (float) sizeMod.z;
		matrixStack.translate(0.0625F, (1F - (xyz_scale * y_height))*1.5F, 0F);
		matrixStack.scale(xyz_scale, xyz_scale * y_height, xyz_scale);
		
		if(this.young)
		{
			super.renderToBuffer(matrixStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
		}
		else
		{

			this.headParts().forEach((model) -> 
			{
				matrixStack.pushPose();
				float no_scale = (1F / xyz_scale) * Math.min(1.05F, xyz_scale)* 1.1F;
				
				matrixStack.scale(no_scale, no_scale, no_scale);
				
				model.render(matrixStack, buffer, packedLight, packedOverlay, red*redTint, green*greenTint, blue*blueTint, alpha);
				matrixStack.popPose();
			});
			this.bodyParts().forEach((model) -> 
			{
				matrixStack.pushPose();
				
				if(model==body || model==upperBody)
				{
					float t = thickness;
					
					matrixStack.translate(0F, 1F-t, 0F);
					
					matrixStack.scale(t, t, 1F);
				}
				if(model==leg_back_left || model==leg_back_right || model==leg_front_left || model==leg_front_right)
				{
					matrixStack.scale(thickness, 1F, thickness);
				}
				if(model==tail)
				{
					float t = thickness;
					matrixStack.translate(0F, (1F-t)*0.5, 0F);
				}
				
				model.render(matrixStack, buffer, packedLight, packedOverlay, red*redTint, green*greenTint, blue*blueTint, alpha);
				
				matrixStack.popPose();
			});
		}
		
		matrixStack.popPose();
	}

	public void setRotationAngle(ModelPart modelRenderer, float x, float y, float z)
	{
		modelRenderer.xRot = x;
		modelRenderer.yRot = y;
		modelRenderer.zRot = z;
	}

	ImmutableList<ModelPart> parts;
	 
	@Override
	protected Iterable<ModelPart> headParts()
	{
		return ImmutableList.of(/**no_teeth ? this.head_baby :*/ this.head);
	}
	
	@Override
	protected Iterable<ModelPart> bodyParts() 
	{
		return parts;
	}
}