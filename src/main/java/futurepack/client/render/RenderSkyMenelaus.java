package futurepack.client.render;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.BufferBuilder;
import com.mojang.blaze3d.vertex.BufferUploader;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.VertexFormat;
import com.mojang.math.Matrix4f;

import futurepack.api.Constants;
import net.minecraft.resources.ResourceLocation;

//@ TODO: OnlyIn(Dist.CLIENT)
public class RenderSkyMenelaus extends RenderSkyBase
{
	
	private ResourceLocation locationMoonPhasesPng = new ResourceLocation(Constants.MOD_ID, "textures/mond_menelaus.png");


	public RenderSkyMenelaus()
	{
		super();
	}
	
	@Override
	protected void renderMoon(float size, int phase, BufferBuilder bufferbuilder, Matrix4f matrix4f1) 
	{
		RenderSystem.setShaderTexture(0, locationMoonPhasesPng);
		int k = phase % 16;
		float u0 = (k%4) / 4.0F;
		float u1 = u0 + 0.25F;
		float v0 = (k/4)  /4F;
		float v1 = v0 +0.25F;
        bufferbuilder.begin(VertexFormat.Mode.QUADS, DefaultVertexFormat.POSITION_TEX);
        bufferbuilder.vertex(matrix4f1, -size, -100.0F, size).uv(u1, v1).endVertex();
        bufferbuilder.vertex(matrix4f1, size, -100.0F, size).uv(u0, v1).endVertex();
        bufferbuilder.vertex(matrix4f1, size, -100.0F, -size).uv(u0, v0).endVertex();
        bufferbuilder.vertex(matrix4f1, -size, -100.0F, -size).uv(u1, v0).endVertex();
        bufferbuilder.end();
        BufferUploader.end(bufferbuilder);
	}
}
