package futurepack.client.render;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import com.mojang.authlib.GameProfile;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;

import futurepack.api.Constants;
import futurepack.common.ManagerGleiter;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;

public class RenderGleiter
{
	private static RenderGleiter instance = new RenderGleiter();

	public static void onRender(RenderPlayerEvent event)
	{
		Player pl = event.getPlayer();
		ItemStack gleiter = ManagerGleiter.getPlayerGleiter(pl);
		float rot = pl.yBodyRotO + (pl.yBodyRot-pl.yBodyRotO)*event.getPartialTick();


		PoseStack matrixStack = event.getPoseStack();

		matrixStack.pushPose();

//		GlStateManager._enableDepthTest();
//		GlStateManager._enableCull();

//		FloatBuffer matF = MemoryTracker.create(16 * 4).asFloatBuffer();
//		event.getMatrixStack().last().pose().store(matF);
//
//		GL11.glLoadMatrixf(matF);



		instance.render(matrixStack, gleiter, 0, 0, 0, rot, pl, event.getMultiBufferSource(), event.getPackedLight());

		matrixStack.translate(rot, rot, rot);

		matrixStack.popPose();
//		GlStateManager._disableDepthTest();
//		GlStateManager._disableCull();
	}

	public static void onRender(RenderHandEvent event)
	{
		LocalPlayer pl = Minecraft.getInstance().player;
		ItemStack gleiter = ManagerGleiter.getPlayerGleiter(pl);
		float rot = pl.yBodyRotO + (pl.yBodyRot-pl.yBodyRotO)*event.getPartialTicks();
		PoseStack matrixStack = event.getPoseStack();
		matrixStack.pushPose();

		GlStateManager._enableCull();

		matrixStack.mulPose(Vector3f.XP.rotationDegrees(pl.getXRot() +90));

		matrixStack.mulPose(Vector3f.XP.rotationDegrees(-90));

		matrixStack.mulPose(Vector3f.YP.rotationDegrees((pl.yHeadRotO + (pl.yHeadRot-pl.yHeadRotO)*event.getPartialTicks())));
		instance.render(matrixStack, gleiter, 0, -1.5, 0, rot, pl, event.getMultiBufferSource(), event.getPackedLight());
		matrixStack.popPose();
	}

	private ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/model/gleiter.png");

	public void render(PoseStack matrixStack, ItemStack gleiter, double x, double y, double z, float rotation, Player pl, MultiBufferSource buffers, int packedLight)
	{
//		Random r = new Random(1);

		matrixStack.pushPose();
		matrixStack.translate(x, y+4, z);
		matrixStack.mulPose(Vector3f.YP.rotationDegrees(-rotation+180F));

//		RenderSystem.setShaderTexture(0, res);

		int color = 0xe15b5b;
		if(gleiter!=null)
		{
			color = Minecraft.getInstance().getItemColors().getColor(gleiter, 1);
		}

		renderGleiter(color, matrixStack, buffers.getBuffer(RenderType.entitySolid(res)), packedLight);
		renderLines(matrixStack.last().pose(), buffers, packedLight);


		ResourceLocation res = getTexture(pl.getGameProfile());
		if(res!=null)
		{
			RenderSystem.setShaderTexture(0, res);
			renderGleiter(0xFFFFFFFF, matrixStack, buffers.getBuffer(RenderType.entitySolid(res)), packedLight);
		}

		matrixStack.popPose();
	}

	private static void renderGleiter(int color, PoseStack matrixStack, VertexConsumer vertexBuf, int packedLight)
	{
		float r = 1F;
		float g = 1F;
		float b = 1F;

		double d = 0;

		Matrix4f mat = matrixStack.last().pose();

		//down
		// ----
		// X---
		addVertex(-2.0, -0.6, -1.0,     0, 1.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.2, -1.0,  0.25, 1.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.1,  0.0 , 0.25, 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-2.0, -0.5,  0.0 ,    0, 0.75, vertexBuf, r, g, b, packedLight, mat);
		// ----
		// -X--
		addVertex(-1.0, -0.2, -1.0, 0.25, 1.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex( 0.0, -0.1, -1.0, 0.5 , 1.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex( 0.0, -0.0,  0.0, 0.5 , 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.1,  0.0, 0.25, 0.75, vertexBuf, r, g, b, packedLight, mat);
		// ----
		// --X-
		addVertex(0.0, -0.1, -1.0, 0.5 , 1.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.2, -1.0, 0.75, 1.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.1,  0.0, 0.75, 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(0.0, -0.0,  0.0, 0.5 , 0.75, vertexBuf, r, g, b, packedLight, mat);
		// ----
		// ---X
		addVertex(1.0, -0.2, -1.0, 0.75, 1.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(2.0, -0.6, -1.0, 1   , 1.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(2.0, -0.5,  0.0, 1   , 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.1,  0.0, 0.75, 0.75, vertexBuf, r, g, b, packedLight, mat);
		// ---X
		// ----
		addVertex(1.0, -0.1, 0.0, 0.75, 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(2.0, -0.5, 0.0, 1   , 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(2.0, -0.6, 1.0, 1   , 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.2, 1.0, 0.75, 0.5, vertexBuf, r, g, b, packedLight, mat);
		// --X-
		// ----
		addVertex(0.0, 0.0 , 0.0, 0.5 , 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.1, 0.0, 0.75, 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.2, 1.0, 0.75, 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(0.0, -0.1, 1.0, 0.5 , 0.5, vertexBuf, r, g, b, packedLight, mat);
		// -X--
		// ----
		addVertex(-1.0, -0.1, 0.0, 0.25, 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex( 0.0,  0.0, 0.0, 0.5 , 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex( 0.0, -0.1, 1.0, 0.5 , 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.2, 1.0, 0.25, 0.5, vertexBuf, r, g, b, packedLight, mat);
		// X---
		// ----
		addVertex(-2.0, -0.5, 0.0, 0   , 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.1, 0.0, 0.25, 0.75, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.2, 1.0, 0.25, 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-2.0, -0.6, 1.0, 0   , 0.5, vertexBuf, r, g, b, packedLight, mat);


		r = ((color>>16) & 0xFF) / 255F;
		g = ((color>>8) & 0xFF) / 255F;
		b = ((color) &  0xFF) / 255F;

		//up
		// ----
		// X---
		addVertex(-2.0, -0.5 +d,  0.0, 0   , 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.1 +d,  0.0, 0.25, 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.2 +d, -1.0, 0.25, 0.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-2.0, -0.6 +d, -1.0, 0   , 0.0, vertexBuf, r, g, b, packedLight, mat);
		// ----
		// -X--
		addVertex(-1.0, -0.1 +d,  0.0, 0.25, 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex( 0.0, -0.0 +d,  0.0, 0.5 , 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex( 0.0, -0.1 +d, -1.0, 0.5 , 0.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.2 +d, -1.0, 0.25, 0.0, vertexBuf, r, g, b, packedLight, mat);
		// ----
		// --X-
		addVertex(0.0, -0.0 +d,  0.0, 0.5 , 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.1 +d,  0.0, 0.75, 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.2 +d, -1.0, 0.75, 0.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(0.0, -0.1 +d, -1.0, 0.5 , 0.0, vertexBuf, r, g, b, packedLight, mat);
		// ----
		// ---X
		addVertex(1.0, -0.1 +d,  0.0, 0.75, 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(2.0, -0.5 +d,  0.0, 1   , 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(2.0, -0.6 +d, -1.0, 1   , 0.0, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.2 +d, -1.0, 0.75, 0.0, vertexBuf, r, g, b, packedLight, mat);
		// ---X
		// ----
		addVertex(1.0, -0.2 +d, 1.0, 0.75, 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(2.0, -0.6 +d, 1.0, 1   , 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(2.0, -0.5 +d, 0.0, 1   , 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.1 +d, 0.0, 0.75, 0.25, vertexBuf, r, g, b, packedLight, mat);
		// --X-
		// ----
		addVertex(0.0, -0.1 +d, 1.0, 0.5 , 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.2 +d, 1.0, 0.75, 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(1.0, -0.1 +d, 0.0, 0.75, 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(0.0, -0.0 +d, 0.0, 0.5 , 0.25, vertexBuf, r, g, b, packedLight, mat);
		// -X--
		// ----
		addVertex(-1.0, -0.2 +d, 1.0, 0.25, 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex( 0.0, -0.1 +d, 1.0, 0.5 , 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex( 0.0, -0.0 +d, 0.0, 0.5 , 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.1 +d, 0.0, 0.25, 0.25, vertexBuf, r, g, b, packedLight, mat);
		// X---
		// ----
		addVertex(-2.0, -0.6 +d, 1.0, 0   , 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.2 +d, 1.0, 0.25, 0.5, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-1.0, -0.1 +d, 0.0, 0.25, 0.25, vertexBuf, r, g, b, packedLight, mat);
		addVertex(-2.0, -0.5 +d, 0.0, 0   , 0.25, vertexBuf, r, g, b, packedLight, mat);
	}

	private static void renderLines(Matrix4f matrix, MultiBufferSource buffers, int packedLight)
	{
		VertexConsumer vertexBuf = buffers.getBuffer(RenderType.lines());


//		GL11.glDisable(GL11.GL_TEXTURE_2D);

//		GL11.glBegin(GL11.GL_LINES);

		float x=-0.35F,y=-2.55F,z=0;


		float h = (packedLight >> 20) / 15F;
		h *= Math.max(0, (Minecraft.getInstance().level.getSunAngle(1F) / Math.PI) * 0.25 -0.25);

		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, -2.0F,  -0.6F,  -1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, -1.0F,  -0.2F,  -1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, 0.0F,  -0.1F,  -1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();

		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, -2.0F,  -0.6F,  1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, -1.0F,  -0.2F,  1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, 0.0F,  -0.1F,  1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();

		x=0.35F;

		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, 0.0F,  -0.1F,  -1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, 1.0F,  -0.2F,  -1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, 2.0F,  -0.6F,  -1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();

		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, 0.0F,  -0.1F,  1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, 1.0F,  -0.2F,  1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, x, y, z).color(h, h, h, 1F).normal(0, 1, 0).endVertex();
		vertexBuf.vertex(matrix, 2.0F,  -0.6F,  1.0F).color(h, h, h, 1F).normal(0, 1, 0).endVertex();

//		GL11.glEnd();
//
//		GL11.glEnable(GL11.GL_TEXTURE_2D);
	}

	private static void addVertex(double x, double y, double z, double u, double v, VertexConsumer vertexBuf, float r, float g, float b, int packedLight, Matrix4f mat)
	{
		int overlay = 0;
		vertexBuf.vertex(mat, (float)x, (float)y, (float)z);
		vertexBuf.color(r, g, b, 1F);
		vertexBuf.uv((float)u, (float)v);
		vertexBuf.overlayCoords(overlay);
		vertexBuf.uv2(packedLight);
		vertexBuf.normal(0, 1F, 0);
		vertexBuf.endVertex();

	}

	private ResourceLocation getTexture(GameProfile prof)
	{
		String name = prof.getName().toLowerCase();

		List<String> rs_name = Arrays.asList("mcenderdragon", "mantes", "wugand", "f_roepert");
		List<UUID> rs_uuid = Arrays.asList(UUID.fromString("3cf92882-db51-4816-948f-78a81087f886"), UUID.fromString("62280c0e-9f1d-4f18-b04c-d086a0060b5b"), UUID.fromString("e72a9a48-a7a5-4070-98fd-eabac76bc9cf"), UUID.fromString("ac6d3aad-bba2-412d-bcb9-c850068d6d3b"));

		if(rs_uuid.contains(prof.getId()))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/rs_schirm.png");
		}
		if(rs_name.contains(name))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/rs_schirm.png");
		}

		//krypto
		if(prof.getId().equals(UUID.fromString("c1b7353e-5e74-4d88-b507-524330b03210")) || prof.getId().equals(UUID.fromString("ab28e20a-b30f-4e6b-ba1f-2b658c8cd270")) || prof.getId().equals(UUID.fromString("08702939-f5dd-4b27-ae04-9720b1ec42da")))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/kry_schirm.png");
		}
		else if(name.equals("masterwolf164") || name.equals("JajaSteele"))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/kry_schirm.png");
		}

		//patreon
		if(prof.getId().equals(UUID.fromString("5e2fbfc2-7874-4081-854b-5fbf0d1cf46f")))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/realm_schirm.png");
		}
		else if(name.equals("therealm18"))
		{
			return new ResourceLocation(Constants.MOD_ID, "textures/model/realm_schirm.png");
		}

		return null;
	}
}
