package futurepack.client.sos;

import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.gui.components.events.GuiEventListener;

public abstract class AnimationBase implements GuiEventListener
{
	protected int xPos,yPos,width,height;
	
	public void init(int xPos, int yPos, int width, int height)
	{
		this.xPos=xPos;
		this.yPos=yPos;
		this.width=width;
		this.height=height;
	}
	
	public abstract void render(PoseStack matrixStack, int mouseX, int mouseY);
	
	public abstract boolean isFinished();
}
