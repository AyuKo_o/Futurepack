package futurepack.client.sos;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.gui.GuiComponent;
import net.minecraft.resources.ResourceLocation;

public class AnimationPicture extends AnimationBase
{

	private int time;
	private ResourceLocation res;
	protected long ticks = -1;
	
	public AnimationPicture(ResourceLocation loc, int time)
	{
		this.time = time;
		res = loc;
	}
		
	
	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY)
	{
		if(ticks==-1)
		{
			ticks = System.currentTimeMillis();
		}
		
		int w = 48;
		int h = 48;
		int x = xPos + (width - w)/2;
		int y = yPos + (height - h)/2;
		
		RenderSystem.setShaderTexture(0, res);
		
		GuiComponent.blit(matrixStack, x, y, w, h, 0, 0, 1, 1, 1, 1);
	}

	@Override
	public boolean isFinished()
	{
		return ticks!=-1 && System.currentTimeMillis() - ticks >= time;
	}

}
