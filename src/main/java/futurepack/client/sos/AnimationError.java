package futurepack.client.sos;

import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;

import futurepack.api.Constants;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.resources.ResourceLocation;

public class AnimationError extends AnimationBase
{
	ResourceLocation res = new ResourceLocation(Constants.MOD_ID, "textures/os/bluesc.png");

	@Override
	public void render(PoseStack matrixStack, int mouseX, int mouseY)
	{
		RenderSystem.setShaderTexture(0, res);
		
		int x= xPos;
		int y= yPos;
		GL11.glEnable(GL11.GL_BLEND);
		Random r = new Random();
		boolean flag1 = r.nextInt(100) == 0;
		boolean flag2 = r.nextInt(100) == 0;
		GuiComponent.blit(matrixStack, x, y, 0, 0, 83, 80, flag1?-83:83, flag2?-80:80);
	}

	@Override
	public boolean isFinished()
	{
		return false;
	}

}
