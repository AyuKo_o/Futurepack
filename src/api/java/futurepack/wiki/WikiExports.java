//package futurepack.wiki;
//
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.lang.reflect.Field;
//import java.util.Collection;
//import java.util.List;
//import java.util.Map;
//import java.util.Map.Entry;
//
//import com.google.common.io.Files;
//
//import net.minecraft.client.Minecraft;
//import net.minecraft.client.renderer.ItemModelMesher;
//import net.minecraft.client.renderer.block.model.ModelResourceLocation;
//import net.minecraft.client.resources.SimpleReloadableResourceManager;
//import net.minecraft.item.Item;
//import net.minecraft.item.ItemStack;
//import net.minecraft.util.ResourceLocation;
//import net.minecraftforge.client.ItemModelMesherForge;
//import net.minecraftforge.fml.common.FMLCommonHandler;
//import net.minecraftforge.fml.common.Loader;
//import net.minecraftforge.fml.common.Mod;
//import net.minecraftforge.fml.common.Mod.EventHandler;
//import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
//import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
//import net.minecraftforge.oredict.OreDictionary;
//import net.minecraftforge.registries.IRegistryDelegate;
//
//@Mod(modid = "fp.wiki.exports", name = "Wiki Exporter", version = "1.2", clientSideOnly=true)
//public class WikiExports
//{
//	public static String version;
//	
//	@EventHandler
//	public void preInit(FMLPreInitializationEvent event)
//	{
//		version = Loader.instance().getIndexedModList().get(Constants.MOD_ID).getVersion();
//		
//		System.setProperty("futurepack.common.research.check", "true");
//	}
//	
//	@EventHandler
//	public void postInit(FMLPostInitializationEvent event)
//	{
//		try
//		{
//			blameMissingLangFiles();
//		}
//		catch(Throwable t)
//		{
//			t.printStackTrace();
//		}
//		if(!WikiExports.doExport())
//		{
//			FMLCommonHandler.instance().exitJava(1, false);
//		}
//	}
//	
//	private void blameMissingLangFiles() throws Throwable
//	{
//		Class rm = Class.forName("futurepack.common.research.ResearchManager");
//		Collection<String> c = (Collection<String>) rm.getMethod("getAllReseraches").invoke(null);
//		SimpleReloadableResourceManager man = (SimpleReloadableResourceManager) Minecraft.getInstance().getResourceManager();
//		String[] langs = new String[]{"en_us", "de_de"};
//		for(String lang : langs)
//		{
//			StringBuilder string = new StringBuilder("\nMissing Researches");
//			string.append("\nLang: " + lang);
//			c.forEach(s -> {
//				String name = new ResourceLocation(s).getPath();
//				ResourceLocation en = new ResourceLocation(Constants.MOD_ID, "lang/"+ lang +"/" + name + ".json");
//				try
//				{
//					man.getResource(en).getInputStream();
//				}
//				catch(IOException e){
//					string.append('\n').append('\t');
//					string.append(s);
//				}
//			});
//			System.out.println(string);
//		}
//	}
//	
//	public static boolean doExport()
//	{
//		try
//		{
//			Class cls = Class.forName("net.minecraft.client.Minecraft");
//			if(cls!=null)
//			{
//				Field f = cls.getField("player");
//				if(f!=null)
//				{
//					itemModels();
//					oreDicts();
//					copyRecipeJsons();
//					System.out.println("Done exporting wiki information");
//					return true;
//				}
//				
//			}	
//		} catch (ClassNotFoundException e) {
//		} catch (NoSuchFieldException e) {
//		} catch (SecurityException e) {
//		} catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//		return false;
//	}
//	
//	
//	public static void itemModels() throws Exception
//	{
//		//ModelLoader.setCustomModelResourceLocation(item, meta, new ModelResourceLocation(s, "inventory"));	
//			FileOutputStream out = new FileOutputStream("./../wiki exports/map_model.php");
//			
//			String start = "<?php\n"
//							+ "//version " + version + "\n"
//							+ "global $model, $names;\n"
//							+ "$model=array();\n"
//							+ "$names=array();\n";
//			out.write(start.getBytes());
//			
//			//Vanilla
//			
//			ItemModelMesher mesher = Minecraft.getInstance().getItemRenderer().getItemModelMesher();
//			Field f_map = ItemModelMesherForge.class.getDeclaredField("locations");
//			
//			f_map.setAccessible(true);
//			Map<IRegistryDelegate<Item>, Map<Integer, ModelResourceLocation>> locations = (Map<IRegistryDelegate<Item>, Map<Integer, ModelResourceLocation>>) f_map.get(mesher);
//			for (Entry<IRegistryDelegate<Item>, Map<Integer, ModelResourceLocation>> e : locations.entrySet())
//			{
//				String line1 = "$model[\"" + e.getKey().name() + ";";
//				String line2 = "$names[\"" + e.getKey().name() + ";";
//				for(Integer meta : e.getValue().keySet())
//				{
//					ModelResourceLocation res = e.getValue().get(meta);
//					if(!res.getVariant().equals("inventory"))
//						continue;
//					
//					String line = line1 + meta+  "\"] = \"" + res.getNamespace() + ":" + res.getPath() + "\";\n";
//					out.write(line.getBytes());
//					try
//					{
//						ItemStack it = new ItemStack(e.getKey().get(), 1, meta);
//						line = line2 + meta+  "\"] = \"" + it.getTranslationKey() + ".name\";\n";
//						out.write(line.getBytes());
//					}
//					catch(Exception ex){}
//					
//					out.flush();				
//				}				
//			}		
//			
//			String end = "\n?>\n";
//			out.write(end.getBytes());
//			
//			out.close();
//	}
//
//	public static void oreDicts()
//	{
//		FileOutputStream out = null;
//		try
//		{
//			out = new FileOutputStream("./../wiki exports/map_oredict.php");
//		
//			String start = "<?php\n"
//					+ "//version " + version + "\n"
//					+ "global $oredict;\n"
//					+ "$oredict=array();\n";
//			out.write(start.getBytes());
//			out.flush();
//			
//			for(String name : OreDictionary.getOreNames())
//			{
//				String line = "$oredict[\"" + name + "\"] = array(";
//				List<ItemStack> list = OreDictionary.getOres(name, false);
//				if(list.isEmpty())
//					continue;
//				for(ItemStack it : list)
//				{
//					line += phpItem(it) + ",";
//				}
//				line = line.substring(0, line.length()-1);
//				line += ");\n";
//				out.write(line.getBytes());
//				out.flush();
//			}		
//			out.close();
//		} 
//		catch (IOException e)
//		{
//			e.printStackTrace();
//			try {
//				out.close();
//			} catch (IOException ee) {
//				e.printStackTrace();
//			}
//		}
//	}
//	
//	private static String phpItem(ItemStack it)
//	{
//		String s = "array(";
//		
//		s+= "\"id\" => \"" + it.getItem().getRegistryName() + "\"";
//		if(it.getItemDamage()==Short.MAX_VALUE)
//		{
//			s+= ", \"meta\" => \"" + 0 + "\"";
//		}
//		else
//		{
//			s+= ", \"meta\" => \"" + it.getItemDamage() + "\"";
//		}
//		
//		
//		return s + " )";
//	}
//
//	private static void copyRecipeJsons() throws IOException
//	{
//		File dir = new File("./../src/main/java/futurepack/common/");
//		File[] recipes = dir.listFiles( (d,n) -> n.endsWith(".json"));
//		for(File r : recipes)
//		{
//			Files.copy(r, new File("./../wiki exports/",r.getName()));
//		}
//		File r = new File("./../src/main/java/futurepack/common/research/research.json");
//		Files.copy(r, new File("./../wiki exports/",r.getName()));
//	}
//}
