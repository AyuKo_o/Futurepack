package futurepack.generating;

import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Supplier;

public class RegistryDef
{
	final String block_class;
	final String registry_name, namespace;
	final String varName;
	final String args;
	final String type;

	final String def_registry;

	public RegistryDef(String type, String block_class, String args, String registry_name)
	{
		this(type, block_class, args, registry_name, registry_name.substring(registry_name.lastIndexOf(':')+1));
	}

	public RegistryDef(String type, String block_class, String args, String registry_name, String variable_name)
	{
		super();
		this.type = type;
		this.block_class = block_class;
		this.args = args;
		this.registry_name = registry_name;
		int l = registry_name.lastIndexOf(':');
		varName = variable_name;
		namespace = registry_name.substring(0, l);

		def_registry = "REGISTRY_" + type + "_" + namespace;
	}

	@Override
	public String toString()
	{
		String s = "\tpublic static final RegistryObject<%s> %s = %s.register(\"%s\", () -> new %s(%s)   ) ;\r\n";
		return String.format(s, type, getVariableName(), def_registry,  registry_name.substring(registry_name.lastIndexOf(':')+1), block_class, args);
	}

	public String getVariableName()
	{
		return varName;
	}

	public String getRegistryeName()
	{
		return registry_name;
	}


	public Supplier<AbstractVarDefinition> createDeferredRegistry(String type2)
	{
		return () -> new AbstractVarDefinition(def_registry, "DeferredRegister<"+type2+">", "DeferredRegister.create", "Registry." +type2.toUpperCase() + "_REGISTRY" , "\"" + namespace + "\"").setStatic().setFinal();
	}



	public static void writeRegistryDefs(ClassBuilder build, RegistryDef[] defs, String type, BuilderBase base)
	{
//		build.imports.add("net.minecraftforge.event.RegistryEvent");
//		build.imports.add("net.minecraftforge.registries.IForgeRegistry");

		build.imports.add("net.minecraftforge.registries.RegistryObject");
		build.imports.add("net.minecraftforge.registries.DeferredRegister");
		build.imports.add("net.minecraft.core.Registry");
		build.imports.add("net.minecraftforge.eventbus.api.IEventBus");

		ArrayList<String> registries = new ArrayList<>();

		for(RegistryDef d : defs)
			if(base.addVarIfAbsent(d.def_registry, d.createDeferredRegistry(type)))
				registries.add(d.def_registry);

		build.methods.add(w -> {
			try
			{
				for(RegistryDef d : defs)
					w.write(d.toString());
				w.append("\r\n");
				w.flush();
			}
			catch(IOException e){
				e.printStackTrace();
			}
		});
		final String s = "	public static void init"+type+"s(IEventBus modBus)\r\n" +
				"	{\r\n" ;

		build.methods.add(w -> {

			try
			{
				w.write(s);
				for(String d : registries)
					w.append("\t\t").append(d).append(".register(modBus);\r\n");
				w.write("	}\r\n\r\n");
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		});
	}
}
