package futurepack.generating;

import java.io.File;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;

public class Blocks
{
	public static final Gson gson;
	
	public static final Gson gson_out;
	
	static
	{
		GsonBuilder builder = new GsonBuilder();
		JsonDeserializer<BlockDef> des = BlockDef::jsonToBlockDef;
		builder.registerTypeAdapter(BlockDef.class, des);
		JsonDeserializer<ModelDefinition> modeldef = ModelDefinition::jsonToModelDef;
		builder.registerTypeAdapter(ModelDefinition.class, modeldef);
		JsonDeserializer<AbstractVarDefinition> vardef = AbstractVarDefinition::jsonToAbstrVar;
		builder.registerTypeAdapter(AbstractVarDefinition.class, vardef);
		JsonDeserializer<ItemDef> itemdef = ItemDef::jsonToItemDef;
		builder.registerTypeAdapter(ItemDef.class, itemdef);
		gson = builder.create();
		
		builder = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping();
		gson_out = builder.create();
		
	}
	
	public static void deko(File srcOutput, File resOutput)
	{
//		System.out.println(srcOutput + " " + resOutput);
//		
//		String j = "{class:BlockA, material:b, registry:block, model:'futurepack:block.png'}";
//		BlockDef def = gson.fromJson(j, BlockDef.class);
//		System.out.println(def);
		
		try
		{
		
			BlockBuilder builder = BlockBuilder.create(srcOutput, resOutput);
			
			builder.addBlock("BlockDekoMeta", "futurepack:color_iron", "futurepack:blocks/color_iron_0", "");
			
			builder.build("futurepack.common.block.deko", "DekoBlocks", "FPamin.tab:deco");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
