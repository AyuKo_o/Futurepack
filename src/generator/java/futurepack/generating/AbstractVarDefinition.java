package futurepack.generating;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class AbstractVarDefinition
{
	public static AbstractVarDefinition create(String o)
	{
		return Blocks.gson.fromJson(o, AbstractVarDefinition.class);
	}

	public static AbstractVarDefinition jsonToAbstrVar(JsonElement json, Type typeOfT, JsonDeserializationContext context)
	{
		if(json.isJsonObject())
		{
			JsonObject obj = json.getAsJsonObject();

			String t = obj.get("type").getAsString();
			String n = obj.get("name").getAsString();

			AbstractVarDefinition def;

			if(obj.has("constructor"))
			{
				JsonElement elm = obj.get("constructor");
				if(elm.isJsonObject())
				{
					JsonObject jo = elm.getAsJsonObject();
					String con = "new " + jo.get("name").getAsString();
					String[] args = context.deserialize(jo.get("args"), String[].class);
					def = new AbstractVarDefinition(n, t, con, args);
				}
				else
				{
					def = new AbstractVarDefinition(n, t, "new " + elm.getAsString(), new String[0]);
				}

			}
			else if(obj.has("call"))
			{
				JsonElement elm = obj.get("call");
				if(elm.isJsonObject())
				{
					JsonObject jo = elm.getAsJsonObject();
					String con = jo.get("name").getAsString();
					String[] args = context.deserialize(jo.get("args"), String[].class);
					def = new AbstractVarDefinition(n, t, con, args);
				}
				else
				{
					def = new AbstractVarDefinition(n, t, elm.getAsString(), new String[0]);
				}
			}
			else if(obj.has("value"))
			{
				def = new AbstractVarDefinition(n, t, obj.get("value").getAsString());
			}
			else
			{
				def = new AbstractVarDefinition(n, t, "new " + t + "()");
			}


			if(obj.has("final"))
				def.isFinal = obj.get("final").getAsBoolean();
			if(obj.has("static"))
				def.isStatic = obj.get("static").getAsBoolean();

			if(obj.has("methods"))
			{
				JsonElement elm = obj.get("methods");
				if(elm.isJsonObject())
				{
					JsonObject jo = elm.getAsJsonObject();
					for(Entry<String, JsonElement> e : jo.entrySet())
					{
						def.addMethod(e.getKey(), context.deserialize(e.getValue(), String[].class));
					}
				}
			}
			return def;
		}
		throw new IllegalArgumentException(json + " is not a JsonObject("+ json.getClass() +")");
	}

	private boolean isFinal = false, isStatic = false;
	private String name, type;

	private String value;
	private List<String> methods;

	public AbstractVarDefinition(String varname, String vartype, String constructor, String...constructorArgs)
	{
		this(varname, vartype, toString(constructor, constructorArgs));
	}

	public AbstractVarDefinition setStatic()
	{
		isStatic = true;
		return this;
	}

	public AbstractVarDefinition setFinal()
	{
		isFinal = true;
		return this;
	}

	public AbstractVarDefinition(String varname, String vartype, String value)
	{
		name = varname;
		type = vartype;
		this.value = value;
	}

	private static String toString(String constructor, String[] constructorArgs)
	{
		StringBuilder build = new StringBuilder().append(constructor).append('(');
		for(int i=0;i<constructorArgs.length;i++)
		{
			if(i>0)
			{
				build.append(", ");
			}
			build.append(constructorArgs[i]);
		}
		build.append(')');
		return build.toString();
	}

	public void addMethod(String name, String args[])
	{
		if(methods==null)
		{
			methods = new ArrayList<String>();
		}
		StringBuilder build = new StringBuilder(".").append(name).append('(');
		for(int i=0;i<args.length;i++)
		{
			if(i>0)
			{
				build.append(", ");
			}
			build.append(args[i]);
		}
		build.append(')');
		methods.add(build.toString());
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("\tpublic ");
		if(isStatic)
			builder.append("static ");
		if(isFinal)
			builder.append("final ");

		builder.append(type).append(' ').append(name).append(" = ").append(value);
		if(methods!=null)
		methods.forEach(builder::append);

		builder.append(";\r\n");
		return builder.toString();
	}

	public String getVariableName()
	{
		return name;
	}
}
