package futurepack.generating.connected;

import java.util.HashMap;
import java.util.Map;

public class ModelElement {
	public int[] from;
	public int[] to;
	public Map<String, ModelFace> faces;
	
	public ModelElement(int[] from, int[] to) {
		this.from = from;
		this.to = to;
		this.faces = new HashMap<>();
	}
	
	public void addFace(String name, ModelFace face) {
		this.faces.put(name, face);
	}
}
