package futurepack.generating;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class BlockDef extends RegistryDef
{
	ModelDefinition model;
	String loot_table;

	public BlockDef(String block_class, String args, String registry_name)
	{
		super("Block", block_class, args, registry_name);

		loot_table = "{\r\n" +
				"  \"type\": \"minecraft:block\",\r\n" +
				"  \"pools\": [\r\n" +
				"    {\r\n" +
				"      \"rolls\": 1,\r\n" +
				"      \"entries\": [\r\n" +
				"        {\r\n" +
				"          \"type\": \"minecraft:item\",\r\n" +
				"          \"name\": \"" + registry_name + "\"\r\n" +
				"        }\r\n" +
				"      ],\r\n" +
				"      \"conditions\": [\r\n" +
				"        {\r\n" +
				"          \"condition\": \"minecraft:survives_explosion\"\r\n" +
				"        }\r\n" +
				"      ]\r\n" +
				"    }\r\n" +
				"  ]\r\n" +
				"}";
	}

	public ItemDef createItemDef(String group)
	{
		return new ItemDef("BlockItem", String.format("%s.get(), (new Item.Properties()).tab(%s)", this.getVariableName(), group), registry_name, "item_" + varName);
	}

	public static BlockDef fromJson(String s)
	{
		return Blocks.gson.fromJson(s, BlockDef.class);
	}

	public static BlockDef jsonToBlockDef(JsonElement json, Type typeOfT, JsonDeserializationContext context)
	{
		JsonObject obj = json.getAsJsonObject();

		BlockDef def = new BlockDef(obj.get("class").getAsString(), obj.get("args").getAsString(), obj.get("registry").getAsString());
		def.model = context.deserialize(obj.get("model"), ModelDefinition.class);
		def.model.registry_name = def.registry_name;
		if(obj.has("loot_table"))
		{
			def.loot_table = Blocks.gson_out.toJson(obj.get("loot_table"));
		}
		return def;
	}

	public static void writeBlockDefs(ClassBuilder build, BlockDef[] defs, BuilderBase base)
	{
		RegistryDef.writeRegistryDefs(build, defs, "Block", base);
	}

	public void createLoottables(File resOutput) throws IOException
	{
		String[] a =  registry_name.split(":", 2);
		String modId = a[0];
		String path = a[1];

		File lootableDir = new File(resOutput, "data" + File.separator + modId + File.separator + "loot_tables" + File.separator + "blocks");
		lootableDir.mkdirs();

		File out1 = new File(lootableDir, path + ".json");
		System.out.println(out1);
		FileWriter writer1 = new FileWriter(out1);
		writer1.append(loot_table);
		writer1.close();
	}
}