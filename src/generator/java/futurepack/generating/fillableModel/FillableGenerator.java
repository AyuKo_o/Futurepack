package futurepack.generating.fillableModel;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;

import com.google.gson.GsonBuilder;

import futurepack.generating.connected.ConnectedGenerator;
import futurepack.generating.connected.GenBlockState;
import futurepack.generating.connected.GenBlockStateModel;

public class FillableGenerator {
	
	private static GsonBuilder gsonB = new GsonBuilder().disableHtmlEscaping().setPrettyPrinting();
	
	private HashMap<String, FillableBlock> blocks = new HashMap<>();
	
	private File resOut;
	
	public FillableGenerator(File resOut) {
		this.resOut = resOut;
	}
	
	public void addBlock(String name, String changingProp, int maxLevel, String baseModel, String texturePath) {
		System.out.println("Added rotateable " + name);
		blocks.put(name, new FillableBlock(changingProp, maxLevel, baseModel, texturePath));
	}
	
	public void addBlock(String name, String changingProp, int maxLevel, String baseModel, String texturePath, String rotationProp) {
		if(rotationProp.equals(""))
			System.out.println("Added " + name);
		else
			System.out.println("Added rotateable " + name);
		blocks.put(name, new FillableBlock(changingProp, maxLevel, baseModel, texturePath, rotationProp));
	}
	
	public void addFixedBlock(String name, String changingProp, int maxLevel, String baseModel, String texturePath) {
		this.addBlock(name, changingProp, maxLevel, baseModel, texturePath, "");
	}
	
	public static String toOutputPath(File sourceBase, String fileName, String relPath) {
		StringBuilder b = new StringBuilder(sourceBase.getAbsolutePath());
		
		String[] packParts = relPath.split("/");
		
		for(String s : packParts) {
			b.append(File.separator + s);
		}
		
		b.append(File.separator + fileName);
		return b.toString();
	}
	
	private String getFacingFromRotation(int rotation) {
		switch(rotation) {
			case 0: return "south";
			case 90: return "west";
			case 180: return "north";
			case 270: return "east";
			default: return "north";
		}
	}
	
	public void generateFiles() {
		
		Iterator<String> it = blocks.keySet().iterator();
				
		while(it.hasNext()) {
			String block = it.next();
			
			System.out.println("Generating Block " + block);
			
			FillableBlock blockData = blocks.get(block);
			
			GenBlockState state = new GenBlockState();
			
			for(int i = blockData.minLevel; i < blockData.maxLevel; i++) {
				
				System.out.println("Generating Level " + i);
				
				try {
					Files.write(Paths.get(toOutputPath(this.resOut, block + "_" + i + ".json", "assets/futurepack/models/block")), generateModelFor(blockData, i).getBytes());
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				if(blockData.rotationState.equals("")) {
					state.addVariant(blockData.changingState + "=" + i, new GenBlockStateModel("futurepack:block/" + block + "_" + i));
				}
				else {
					for(int r = 0; r < 360; r += 90) {
						String baseVariant = blockData.rotationState + "=" + getFacingFromRotation(r);
						
						GenBlockStateModel m = new GenBlockStateModel("futurepack:block/" + block + "_" + i);
						m.y = r;
						
						state.addVariant(baseVariant + "," + blockData.changingState + "=" + i, m);
					}
				}
			}
			
			String itemJson = "{\r\n" + 
					"  \"parent\": \"futurepack:block/" + block + "_" + blockData.minLevel + "\"\r\n" + 
					"}";
			
			try {
				Files.write(Paths.get(toOutputPath(this.resOut, block + ".json", "assets/futurepack/models/item")), itemJson.getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				Files.write(Paths.get(toOutputPath(this.resOut, block + ".json", "assets/futurepack/blockstates")), ConnectedGenerator.blockstateToJson(state).getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private String generateModelFor(FillableBlock data, int level) {
		return  "{\r\n" + 
				"	\"parent\": \"futurepack:" + data.baseModel + "\",\r\n" + 
				"	\"textures\": {\r\n" + 
				"		\"change\": \"futurepack:" + data.texturePath.replace("-0-", Integer.toString(level)) + "\"\r\n" + 
				"	}\r\n" + 
				"}\r\n" + 
				"";
	}
	
	public static FillableGenerator create(File resOut) {
		return new FillableGenerator(resOut);
	}
}
