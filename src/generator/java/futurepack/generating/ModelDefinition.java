package futurepack.generating;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ModelDefinition
{
	String registry_name;
	
	Map<String, Model> models = new HashMap<>();
	Model inventory;
	JsonObject blockstate;
	
	public void createModels(File resOutput) throws IOException
	{
		String[] a =  registry_name.split(":", 2);
		String modId = a[0];
		String path = a[1];
		
		File blockstatesDir = new File(resOutput, "assets" + File.separator + modId + File.separator + "blockstates");
		if(!blockstatesDir.exists())
			blockstatesDir.mkdirs();
		
		File out1 = new File(blockstatesDir, path + ".json");
		System.out.println(out1);
		FileWriter writer1 = new FileWriter(out1);
		Blocks.gson_out.toJson(getBlockStates(), Blocks.gson_out.newJsonWriter(writer1));
		writer1.close();
		if(inventory!=null)
		{
			File modelsDir = new File(resOutput, "assets" + File.separator + modId + File.separator +  "models" +  File.separator + "item");
			if(!modelsDir.exists())
				modelsDir.mkdirs();
			
			File out2 = new File(modelsDir,  path+".json");
			System.out.println(out2);
			FileWriter writer2 = new FileWriter(out2);
			Blocks.gson_out.toJson(inventory.asJson(), Blocks.gson_out.newJsonWriter(writer2));
			writer2.close();
		}
		
		for(Entry<String, Model> e : models.entrySet())
		{
			a = e.getKey().split(":", 2);
			modId = a[0];
			path = a[1];
			
			File modelsDir = new File(resOutput, "assets" + File.separator + modId + File.separator +  "models" +  File.separator + "block");
			if(!modelsDir.exists())
				modelsDir.mkdirs();
			
			File out2 = new File(modelsDir,  path+".json");
			System.out.println(out2);
			FileWriter writer2 = new FileWriter(out2);
			Blocks.gson_out.toJson(e.getValue().asJson(), Blocks.gson_out.newJsonWriter(writer2));
			writer2.close();
		}
	}
	
	public JsonObject  getBlockStates()
	{
		if(blockstate!=null)
			return blockstate;	
		blockstate = new JsonObject();
		JsonObject variant = new JsonObject();
		blockstate.add("variants", variant);
		JsonObject model = new JsonObject();
		variant.add("", model);
		
		if(models.size() > 1)
		{
			throw new IllegalStateException("Multiply models but no blockstate given, please add custom blockstae");
		}
		else if(model.size() == 1)
		{
			String path = models.keySet().stream().findAny().get()+ "_model";
			model.addProperty("model", path);
			inventory = new Model(path, null);
		}
		else
		{
			String[] a =  registry_name.split(":", 2);
			String modId = a[0];
			String path = a[1];
			model.addProperty("model", modId + ":block/" + path + "_model");
			inventory = new Model(modId + ":block/" + path + "_model", null);
		}
		return blockstate;
	}

	public void addModel(String name, String parent, Map<String, String> textures)
	{
		this.models.put(name, new Model(parent, textures));
	}
	
	public static ModelDefinition jsonToModelDef(JsonElement json, Type typeOfT, JsonDeserializationContext context)
	{
		ModelDefinition def = new ModelDefinition();
		JsonObject obj = json.getAsJsonObject();
		
		if(obj.has("blockstate"))
		{
			def.blockstate = obj.getAsJsonObject("blockstate");
		}
		obj.entrySet().stream().filter(e -> !e.getKey().equals("blockstate")).forEach(e -> def.models.put(e.getKey(), new Model(e.getValue(), context)));
		def.inventory = def.models.remove("inventory");
		return def;
	}
	
}