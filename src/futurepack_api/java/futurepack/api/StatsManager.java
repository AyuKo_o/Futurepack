package futurepack.api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

import net.minecraftforge.fml.ModContainer;
import net.minecraftforge.fml.ModList;
import net.minecraftforge.fml.loading.moddiscovery.ModFileInfo;
import net.minecraftforge.forgespi.locating.IModFile;

public class StatsManager
{
	public static String getModList()
	{
		ModList md = ModList.get();
		return md.applyForEachModFile(StatsManager::fileToLine).collect(Collectors.joining("\n"));
	}

	private static String fileToLine(IModFile mf)
	{
		return String.format("%s|%s|%s|%s|%s|%s",
				mf.getFileName(),
				mf.getModInfos().get(0).getDisplayName(),
				mf.getModInfos().get(0).getModId(),
				mf.getModInfos().get(0).getVersion(),
				ModList.get().getModContainerById(mf.getModInfos().get(0).getModId()).map(ModContainer::getCurrentState).map(Object::toString).orElse("NONE"),
				((ModFileInfo) mf.getModFileInfo()).getCodeSigningFingerprint().orElse("NOSIGNATURE"));
	}

	public static String compress(String input)
	{
		// encoding
		try
		{
			ByteArrayOutputStream bout = new ByteArrayOutputStream(input.length() * 2);
			GZIPOutputStream gzip = new GZIPOutputStream(Base64.getEncoder().wrap(bout));

			gzip.write(input.getBytes(StandardCharsets.UTF_8));
			gzip.close();
			return bout.toString();
		} catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public static String sendPost(String url_, String data) throws IOException
	{
		URL url = new URL(url_);
		URLConnection con = url.openConnection();
		HttpURLConnection http = (HttpURLConnection) con;
		http.setRequestMethod("POST"); // PUT is another valid option
		http.setDoOutput(true);

		data = URLEncoder.encode("data", "UTF-8") + "=" + URLEncoder.encode(data, "UTF-8");

		byte[] out = data.getBytes(StandardCharsets.UTF_8);
		http.setFixedLengthStreamingMode(out.length);
		http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		http.connect();
		try (OutputStream os = http.getOutputStream())
		{
			os.write(out);
		}
		String result = http.getResponseMessage();
		http.disconnect();
		return result;
	}

	public static String sendVersionRequest(String url)
	{
		try
		{
			return sendPost(url, compress(getModList()));
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
}
