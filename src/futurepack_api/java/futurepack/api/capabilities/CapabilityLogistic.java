package futurepack.api.capabilities;

import java.util.Arrays;

import futurepack.api.EnumLogisticIO;
import futurepack.api.EnumLogisticType;
import futurepack.api.LogisticStorage;
import net.minecraft.core.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;

public class CapabilityLogistic implements ILogisticInterface
{
	public static final Capability<ILogisticInterface> cap_LOGISTIC = CapabilityManager.get(new CapabilityToken<>(){});
	
	
//	public static class Storage implements IStorage<ILogisticInterface>
//	{
//
//		@Override
//		public Tag writeNBT(Capability<ILogisticInterface> capability, ILogisticInterface instance, Direction side)
//		{
//			
//			return null;
//		}
//
//		@Override
//		public void readNBT(Capability<ILogisticInterface> capability, ILogisticInterface instance, Direction side, Tag nbt)
//		{
//			
//		}
//		
//	}
	
	private LogisticStorage storage;
	
	public CapabilityLogistic()
	{
		storage = new LogisticStorage(null, this::onLogisticChange);
	}

	@Override
	public EnumLogisticIO getMode(EnumLogisticType mode)
	{
		return storage.getModeForFace(Direction.UP, mode);
	}

	@Override
	public boolean setMode(EnumLogisticIO log, EnumLogisticType mode)
	{
		return storage.setModeForFace(Direction.UP, log, mode);
	}

	@Override
	public boolean isTypeSupported(EnumLogisticType type)
	{
		return Arrays.binarySearch(storage.getLogisticModes(), type) >= 0;
	}
	
	public void onLogisticChange(Direction face, EnumLogisticType log)
	{
		
	}

}
