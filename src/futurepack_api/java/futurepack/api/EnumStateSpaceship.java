package futurepack.api;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Style;
import net.minecraft.network.chat.TranslatableComponent;


/**
 * This enum used to check if a spaceship can jump.  
 */
public enum EnumStateSpaceship
{
	/**
	 * The BoardComputer have not enough neon energy
	 */
	lowEnergie,
	/**
	 * The spaceship is not airtight
	 */
	open,
	/**
	 * The spaceship has not enough trusters for its size. 
	 */
	missingThruster,
	/**
	 * The spaceship is missing something that can produce neon energie
	 */
	missingEngine,
	/**
	 * The spaceship is missing a teleporter
	 */
	missingBeam,
	/**
	 * The spaceship has not enough fuel to power a trusters
	 */
	missingFuel,
	/**
	 * Everything is fine
	 */
	GO,
	/**
	 * Wrong coordinates for jump (outside the world borders or higher than allowed building height)
	 */
	WRONG_COORDS,
	/**
	 * Destination is obstructed
	 */
	DESTINATION_OBSTRUCTED;

	/**
	 * This returns the unformated (colors, ...) translated chat message
	 */
	public TranslatableComponent getRawChat(Object ...args)
	{
		return new TranslatableComponent("chat.spaceship." + this.name(),args);
	}
	
	/**
	 * Return the translated chat message with colors.
	 */
	public TranslatableComponent getChatFormated(Object ...args)
	{
		TranslatableComponent trans = getRawChat(args);
		trans.setStyle(Style.EMPTY.withColor(ChatFormatting.RED));
		return trans;
	}
}
