package futurepack.api.interfaces.tilentity;

import java.util.UUID;

import net.minecraft.world.entity.player.Player;

/**
 * This is to store the owning player in the block
 */
public interface ITileWithOwner
{
	public void setOwner(Player pl);
	
	public boolean isOwner(Player pl);
	
	public boolean isOwner(UUID pl);
}
