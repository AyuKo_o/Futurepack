package futurepack.api.interfaces;

import futurepack.api.interfaces.tilentity.ITileClientTickable;
import futurepack.api.interfaces.tilentity.ITileServerTickable;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;

public interface IBlockBothSidesTickingEntity<T extends BlockEntity & ITileServerTickable & ITileClientTickable> extends IBlockServerOnlyTickingEntity<T>
{
	@Override
	default BlockEntityType<? extends ITileClientTickable> getClientEnityType(Level pLevel, BlockState pState) 
	{
		return getTileEntityType(pState);
	}
}
