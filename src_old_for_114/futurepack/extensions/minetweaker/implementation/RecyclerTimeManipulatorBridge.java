package futurepack.extensions.minetweaker.implementation;

import java.util.Arrays;

import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.oredict.IOreDictEntry;
import futurepack.api.ItemPredicates;
import futurepack.common.recipes.recycler.FPRecyclerTimeManipulatorManager;
import futurepack.common.recipes.recycler.RecyclerTimeManipulatorRecipe;
import futurepack.depend.api.ItemPredicate;
import futurepack.extensions.jei.RecipeRuntimeEditor;
import futurepack.extensions.minetweaker.ClassRegistry;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenRegister
@ZenClass("mods.futurepack.recycler.timemanipulator")
public class RecyclerTimeManipulatorBridge
{

    private static void addRecipe(IItemStack out, IIngredient in, int support, int time)
    {
        ItemPredicates predicate = ClassRegistry.getPredicate(in);
        if (predicate == null)
        {
            CraftTweakerAPI.logError("Invalid items for recycler: " + Arrays.toString(in.getItemArray()));
            return;
        }

        ItemStack isO = ClassRegistry.getItem(out);

        ClassRegistry.addRecipe(new Add(isO, predicate, support, time));
    }

    @ZenMethod
    public static void add(IItemStack out, IIngredient ins, int support, int time)
    {
        addRecipe(out, ins, support, time);
    }

    @ZenMethod
    public static void addRepair(IItemStack in)
    {
        ItemStack isI = ClassRegistry.getItem(in);

        if (isI != null)
        {
            ClassRegistry.addRecipe(new Add(new ItemPredicate(isI)));
        }
        else
        {
            CraftTweakerAPI.logError("Invalid repair item for recycler: " + Arrays.toString(in.getItemArray()));
        }
    }

    private static class Add implements IAction
    {

        private final ItemPredicates input;
        private ItemStack output;
        private int support;
        private int time;

        public Add(ItemStack output, ItemPredicates input, int support, int time)
        {
            this.output = output;
            this.input = input;
            this.support = support;
            this.time = time;
        }

        public Add(ItemPredicates input)
        {
            this.input = input;
        }

        @Override
        public void apply()
        {
            RecyclerTimeManipulatorRecipe recipe;
            if (output != null)
            {
                recipe = FPRecyclerTimeManipulatorManager.addRecipe(input, output, support, time);
            }
            else
            {
                if (input.getRepresentItem().isItemStackDamageable())
                {
                    recipe = FPRecyclerTimeManipulatorManager.addRecipe(input);
                    RecipeRuntimeEditor.addRecipe(recipe);
                }
                else
                {
                    throw new IllegalArgumentException(String.format("Could not add the recipe for Recycler (Time Manipulator) because '%s' is not damageable", input.toString()));
                }
            }

            RecipeRuntimeEditor.addRecipe(recipe);
        }

        @Override
        public String describe()
        {
            return "Adding Recycler (Time Manipulator) Recipe for " + (output != null ? output : input).toString();
        }
    }

    @ZenMethod
    public static void remove(IIngredient iin)
    {
        ItemStack in;
        if (iin instanceof IItemStack)
        {
            in = ClassRegistry.getItem((IItemStack) iin);
        }
        else if (iin instanceof IOreDictEntry)
        {
            in = ClassRegistry.getItem(((IOreDictEntry) iin).getFirstItem());
        }
        else return;

        ClassRegistry.removeRecipe(new Remove(in));
    }

    @ZenMethod
    public static void removeRepair(IItemStack iin)
    {
        ItemStack in = ClassRegistry.getItem(iin);

        ClassRegistry.removeRecipe(new Remove(in));
    }

    private static class Remove implements IAction
    {

        private final ItemStack input;

        public Remove(ItemStack input)
        {
            this.input = input;
        }

        @Override
        public void apply()
        {
            RecyclerTimeManipulatorRecipe recipe = FPRecyclerTimeManipulatorManager.instance.getMatchingRecipe(input);
            if(recipe != null)
            {
                if(FPRecyclerTimeManipulatorManager.instance.recipes.remove(recipe))
                {
                    RecipeRuntimeEditor.removeRecipe(recipe);
                }
            }
            else
            {
                ClassRegistry.onRecipeRemoveFail(input);
            }
        }

        @Override
        public String describe()
        {
            return String.format("Removing Recycler (Time Manipulator) Recipe with %s as input", input.toString());
        }
    }
}