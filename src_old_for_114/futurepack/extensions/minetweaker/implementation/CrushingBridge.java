package futurepack.extensions.minetweaker.implementation;

import java.util.Arrays;

import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.oredict.IOreDictEntry;
import futurepack.api.ItemPredicates;
import futurepack.common.recipes.crushing.CrushingRecipe;
import futurepack.common.recipes.crushing.FPCrushingManager;
import futurepack.extensions.jei.RecipeRuntimeEditor;
import futurepack.extensions.minetweaker.ClassRegistry;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenRegister
@ZenClass("mods.futurepack.crushing")
public class CrushingBridge 
{

	private static void addRecipe(IItemStack out, IIngredient in)
	{
		ItemStack isO = ClassRegistry.getItem(out);
		ItemPredicates isI = ClassRegistry.getPredicate(in);
		if (isI == null)
		{
			CraftTweakerAPI.logError("Invalid item for crusher: " + Arrays.toString(in.getItemArray()));
			return;
		}

		ClassRegistry.addRecipe(new Add(isO, isI));
	}

	@ZenMethod
	public static void add(IItemStack out, IIngredient ins)
	{
		addRecipe(out, ins);
	}

	private static class Add implements IAction
	{

		private final ItemStack output;
		private final ItemPredicates input;

		public Add(ItemStack output, ItemPredicates input)
		{
			this.output = output;
			this.input = input;
		}

		@Override
		public void apply()
		{
			CrushingRecipe recipe = FPCrushingManager.addCrusherRecipe(input, output);
			RecipeRuntimeEditor.addRecipe(recipe);
		}

		@Override
		public String describe()
		{
			return "Adding Crusher Recipe for " + output.toString();
		}
	}

	@ZenMethod
	public static void remove(IIngredient iin)
	{
		ItemStack in;
		if (iin instanceof IItemStack)
		{
			in = ClassRegistry.getItem((IItemStack) iin);
		}
		else if (iin instanceof IOreDictEntry)
		{
			in = ClassRegistry.getItem(((IOreDictEntry) iin).getFirstItem());
		}
		else return;

		ClassRegistry.removeRecipe(new Remove(in));
	}

	private static class Remove implements IAction
	{

		private final ItemStack input;

		public Remove(ItemStack input)
		{
			this.input = input;
		}

		@Override
		public void apply()
		{
			CrushingRecipe recipe = FPCrushingManager.instance.getRecipe(input);
			if(recipe != null)
			{
				if(FPCrushingManager.instance.recipes.remove(recipe))
				{
					RecipeRuntimeEditor.removeRecipe(recipe);
				}
			}
			else
			{
				ClassRegistry.onRecipeRemoveFail(input);
			}
		}

		@Override
		public String describe()
		{
			return String.format("Removing Crusher Recipe with %s as input", input.toString());
		}
	}
}