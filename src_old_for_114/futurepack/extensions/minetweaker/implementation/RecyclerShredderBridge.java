package futurepack.extensions.minetweaker.implementation;

import java.util.Arrays;

import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.oredict.IOreDictEntry;
import futurepack.api.ItemPredicates;
import futurepack.common.recipes.recycler.FPRecyclerShredderManager;
import futurepack.common.recipes.recycler.RecyclerShredderRecipe;
import futurepack.extensions.jei.RecipeRuntimeEditor;
import futurepack.extensions.minetweaker.ClassRegistry;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.Optional;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenRegister
@ZenClass("mods.futurepack.recycler.shredder")
public class RecyclerShredderBridge
{

    private static void addRecipe(IItemStack[] out, IIngredient in, int time, float[] probabilities)
    {
        ItemPredicates isI = ClassRegistry.getPredicate(in);
        if (isI == null)
        {
            CraftTweakerAPI.logError("Invalid items for recycler: " + Arrays.toString(in.getItemArray()));
            return;
        }

        ItemStack[] isO = new ItemStack[out.length];
        for(int i=0;i<isO.length;i++)
        {
            isO[i] = ClassRegistry.getItem(out[i]);
        }

        if (probabilities == null)
        {
            probabilities = new float[]{};
        }

        float[] probs = probabilities;
        if (probabilities.length < out.length)
        {
            probs = new float[out.length];
            for (int i = 0; i < probs.length; i++)
            {
                if (i < probabilities.length)
                {
                    probs[i] = probabilities[i];
                }
                else
                {
                    probs[i] = 1.0F;
                }
            }
        }

        ClassRegistry.addRecipe(new Add(isO, isI, time, probs));
    }

    @ZenMethod
    public static void add(IItemStack[] out, IIngredient ins, int time, @Optional float[] probabilities)
    {
        addRecipe(out, ins, time, probabilities);
    }

    private static class Add implements IAction
    {

        private final ItemStack[] output;
        private final ItemPredicates input;
        private final int time;
        private final float[] probs;

        public Add(ItemStack[] output, ItemPredicates input, int time, float[] probs)
        {
            this.output = output;
            this.input = input;
            this.time = time;
            this.probs = probs;
        }

        @Override
        public void apply()
        {
            RecyclerShredderRecipe recipe = FPRecyclerShredderManager.instance.addRecyclerRecipe(input, output, time, probs);
            RecipeRuntimeEditor.addRecipe(recipe);
        }

        @Override
        public String describe()
        {
            return "Adding Recycler (Shredder) Recipe for " + Arrays.toString(output);
        }
    }

    @ZenMethod
    public static void remove(IIngredient iin)
    {
        ItemStack in;
        if (iin instanceof IItemStack)
        {
            in = ClassRegistry.getItem((IItemStack) iin);
        }
        else if (iin instanceof IOreDictEntry)
        {
            in = ClassRegistry.getItem(((IOreDictEntry) iin).getFirstItem());
        }
        else return;

        ClassRegistry.removeRecipe(new Remove(in));
    }

    private static class Remove implements IAction
    {

        private final ItemStack input;

        public Remove(ItemStack input)
        {
            this.input = input;
        }

        @Override
        public void apply()
        {
            RecyclerShredderRecipe recipe = FPRecyclerShredderManager.instance.getMatchingRecipe(input);
            if(recipe != null)
            {
                if(FPRecyclerShredderManager.instance.recipes.remove(recipe))
                {
                    RecipeRuntimeEditor.removeRecipe(recipe);
                }
            }
            else
            {
                ClassRegistry.onRecipeRemoveFail(input);
            }
        }

        @Override
        public String describe()
        {
            return String.format("Removing Recycler (Shredder) Recipe with %s as input", input.toString());
        }
    }
}