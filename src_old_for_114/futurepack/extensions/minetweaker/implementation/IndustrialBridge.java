package futurepack.extensions.minetweaker.implementation;

import java.util.Arrays;
import java.util.List;

import crafttweaker.CraftTweakerAPI;
import crafttweaker.IAction;
import crafttweaker.annotations.ZenRegister;
import crafttweaker.api.item.IIngredient;
import crafttweaker.api.item.IItemStack;
import crafttweaker.api.oredict.IOreDictEntry;
import futurepack.api.ItemPredicates;
import futurepack.common.recipes.FPIndustrialFurnaceManager;
import futurepack.common.recipes.IndRecipe;
import futurepack.extensions.jei.RecipeRuntimeEditor;
import futurepack.extensions.minetweaker.ClassRegistry;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

@ZenRegister
@ZenClass("mods.futurepack.industrial")
public class IndustrialBridge
{
	private static void addRecipe(String id, IItemStack out, IIngredient[] in)
	{
		ItemStack isO = ClassRegistry.getItem(out);
		ItemPredicates[] isI = new ItemPredicates[in.length];
		for(int i=0;i<isI.length;i++)
		{
			isI[i] = ClassRegistry.getPredicate(in[i]);
			if (isI[i] == null)
			{
				CraftTweakerAPI.logError("Invalid item for industrial furnace: " + Arrays.toString(in[i].getItemArray()));
				return;
			}
		}
		
		ClassRegistry.addRecipe(new Add(id, isO, isI));
	}

	@ZenMethod
	public static void add(String id, IItemStack out, IIngredient[] in)
	{
		add(id, out, in, in.length-1, new IItemStack[in.length]);
	}
	
	private static void add(String id, IItemStack out, IIngredient[] in, int layer, IItemStack[] stacks)
	{
		List<IItemStack> in1 = in[layer].getItems();
		for(IItemStack iitem : in1)
		{
			stacks[layer] = iitem;
			if(layer>0)
			{
				add(id, out, in, layer-1, stacks);
			}
			else
			{
				addRecipe(id, out, stacks);
			}
		}
	}

	private static class Add implements IAction
	{

		private final String name;
		private final ItemStack output;
		private final ItemPredicates[] input;

		public Add(String name, ItemStack output, ItemPredicates[] input)
		{
			this.name = name;
			this.output = output;
			this.input = input;
		}

		@Override
		public void apply()
		{
			IndRecipe recipe = FPIndustrialFurnaceManager.addRecipe(name, output, input);
			RecipeRuntimeEditor.addRecipe(recipe);
		}

		@Override
		public String describe()
		{
			return "Adding Industrial Furnace Recipe for " + output.toString();
		}
	}

	@ZenMethod
	public static void remove(IIngredient[] in)
	{
		ItemStack[] isI = new ItemStack[in.length];
		for(int i=0;i<isI.length;i++)
		{
			if (in[i] instanceof IItemStack)
			{
				isI[i] = ClassRegistry.getItem((IItemStack) in[i]);
			}
			else if (in[i] instanceof IOreDictEntry)
			{
				isI[i] = ClassRegistry.getItem(((IOreDictEntry) in[i]).getFirstItem());
			}
		}

		ClassRegistry.removeRecipe(new Remove(isI));
	}

	private static class Remove implements IAction
	{

		private final ItemStack[] input;

		public Remove(ItemStack[] input)
		{
			this.input = input;
		}

		@Override
		public void apply()
		{
			IndRecipe[] recipes = FPIndustrialFurnaceManager.instance.getMatchingRecipes(input);
			if(recipes.length==0)
			{
				ClassRegistry.onRecipeRemoveFail(input);
			}
			for(IndRecipe rec : recipes)
			{
				if(FPIndustrialFurnaceManager.instance.recipes.remove(rec))
				{
					RecipeRuntimeEditor.removeRecipe(rec);
				}

			}
		}

		@Override
		public String describe()
		{
			return String.format("Removing Industrial Furnace Recipe with %s as input", Arrays.toString(input));
		}
	}
}