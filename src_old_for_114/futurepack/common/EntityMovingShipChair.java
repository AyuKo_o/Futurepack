package futurepack.common.entity;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.world.World;

public class EntityMovingShipChair extends Entity
{
	private static final DataParameter<Float> relX = EntityDataManager.<Float>createKey(EntityMovingShipChair.class, DataSerializers.FLOAT);
	private static final DataParameter<Float> relY = EntityDataManager.<Float>createKey(EntityMovingShipChair.class, DataSerializers.FLOAT);
	private static final DataParameter<Float> relZ = EntityDataManager.<Float>createKey(EntityMovingShipChair.class, DataSerializers.FLOAT);
	
	public EntityMovingShipChair(World w)
	{
		super(w);
		this.setSize(1F, 1F);
	}

	public EntityMovingShipChair(EntityMovingShip ship, LivingEntity passenger)
	{
		this(ship.world);
		
		double dx = passenger.posX;
		double dy = passenger.posY;
		double dz = passenger.posZ;
		
		dx = ((int)dx) + 0.5;
		dy = ((int)dy) + 0.2;
		dz = ((int)dz) + 0.5;
		
		setRelativ(dx - ship.posX, (dy - ship.posY), dz - ship.posZ);
		
		setLocationAndAngles(passenger.posX, passenger.posY, passenger.posZ, passenger.rotationYaw, passenger.rotationPitch);
		ship.world.addEntity(this);
		
		passenger.startRiding(this);
		this.startRiding(ship);
	}
	
	@Override
	protected void registerData()
	{
		this.dataManager.register(relX, 0F);
		this.dataManager.register(relY, 0F);
		this.dataManager.register(relZ, 0F);
	}

	@Override
	protected void readAdditional(CompoundNBT nbt)
	{
		setRelativ(nbt.getDouble("relX"), nbt.getDouble("relY"),  nbt.getDouble("relZ"));
	}

	@Override
	protected void writeAdditional(CompoundNBT nbt)
	{
		nbt.putDouble("relX", getDX());
		nbt.putDouble("relY", getDY());
		nbt.putDouble("relZ", getDZ());
	}
	
	
	@Override
	public void baseTick()
	{
		super.baseTick();
		
		if(!this.isPassenger() || !this.isBeingRidden())
		{
			this.removePassengers();
			remove();
		}
	}
	
	private void setRelativ(double dx, double dy, double dz)
	{
		setRelativ((float)dx, (float)dy, (float)dz);
	}
	
	private void setRelativ(float dx, float dy, float dz)
	{
		dataManager.set(relX, dx);
		dataManager.set(relY, dy);
		dataManager.set(relZ, dz);
	}
	
	private float getDX()
	{
		return dataManager.get(relX);
	}
	
	private float getDY()
	{
		return dataManager.get(relY);
	}
	private float getDZ()
	{
		return dataManager.get(relZ);
	}

	public void alignToShip(double posX, double posY, double posZ, float rotationYaw, float rotationPitch)
	{
		double pX = getDX();
		double pY = getDY();
		double pZ = getDZ();
		
		double rad, cos, sin;
		double f1,f2;
		
		
		rad = Math.PI / 180D * rotationPitch;
		cos = Math.cos(rad);
		sin = Math.sin(rad);
		f1 = cos * pY - sin*pZ;
		f2 = sin * pY + cos*pZ;
		pY = f1;
		pZ = f2;
		
		rad = Math.PI / 180D * rotationYaw;
		cos = Math.cos(rad);  // ( cos -sin )
		sin = Math.sin(rad);  // ( sin  cos )
		f1 = cos * pX - sin*pZ;
		f2 = sin * pX + cos*pZ;
		pX = f1;
		pZ = f2;

		this.setLocationAndAngles(posX+pX, posY+pY, posZ+pZ, rotationYaw, rotationPitch);
	}

	@Override
	public double getMountedYOffset()
    {
        return (double)this.height * 0.5D;
    }
}
