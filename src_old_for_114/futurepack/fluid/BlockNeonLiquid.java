package futurepack.common.block;

import futurepack.common.FPPotions;
import futurepack.common.item.tools.compositearmor.CompositeArmorInventory;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.block.Blocks;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.EffectInstance;
import net.minecraft.block.BlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.BlockFluidClassic;
import net.minecraftforge.fluids.Fluid;

public class BlockNeonLiquid extends BlockFluidClassic
{

	public BlockNeonLiquid(Fluid fluid)
	{
		super(fluid, Material.WATER);
		//setCreativeTab(FPMain.tab_items);
		setLightLevel(0.667F);
	}
	
	@Override
	public BlockRenderType getRenderType(BlockState state)
	{
		return BlockRenderType.MODEL;
	}
	
	@Override
	protected void flowIntoBlock(World world, BlockPos pos, int meta)
	{
		if (meta < 0)
			return;
		
		Block b = world.getBlockState(pos).getBlock();
		if(b==Blocks.FLOWING_WATER || b==Blocks.WATER)
		{
			BlockState state = FPBlocks.neonsand.getDefaultState();
			world.setBlockState(pos, state, 3);
			return;
		}
		super.flowIntoBlock(world, pos, meta);
	}
	
	@Override
	public void onEntityCollision(World worldIn, BlockPos pos, BlockState state, Entity entityIn)
	{
		if(!worldIn.isRemote)
			if(entityIn instanceof LivingEntity)
			{
				if(!CompositeArmorInventory.hasSetBonus((LivingEntity) entityIn))
				{
					((LivingEntity) entityIn).addPotionEffect(new EffectInstance(FPPotions.paralyze, 20*2, 1));
				}			
			}
	}
	
}
