package futurepack.common.block;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeColorHelper;
import net.minecraftforge.fluids.Fluid;

public class FluidSaltWater extends Fluid
{

	public FluidSaltWater(String fluidName, ResourceLocation still, ResourceLocation flowing)
	{
		super(fluidName, still, flowing);
	}
	
	@Override
	public int getColor(World w, BlockPos pos)
	{
		return w != null && pos != null ? BiomeColorHelper.getWaterColorAtPos(w, pos) : -1;
		//return super.getColor(world, pos);
	}

}
