// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class forstmaster<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "forstmaster"), "main");
	private final ModelPart drone;

	public forstmaster(ModelPart root) {
		this.drone = root.getChild("drone");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition drone = partdefinition.addOrReplaceChild("drone", CubeListBuilder.create().texOffs(22, 10).addBox(4.0F, -5.0F, -5.0F, 1.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(22, 10).addBox(-5.0F, -5.0F, -5.0F, 1.0F, 2.0F, 8.0F, new CubeDeformation(0.0F))
		.texOffs(41, 0).addBox(-4.0F, -5.0F, -5.0F, 8.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(52, 4).addBox(-1.0F, -5.0F, 4.0F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8.0F, 6.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 2.0F));

		PartDefinition thruster = drone.addOrReplaceChild("thruster", CubeListBuilder.create().texOffs(51, 11).addBox(2.0F, -1.0F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(51, 11).addBox(-4.0F, -1.0F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(41, 3).addBox(1.0F, -1.5F, 0.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(41, 3).addBox(4.0F, -1.5F, 0.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(41, 3).addBox(-2.0F, -1.5F, 0.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(41, 3).addBox(-5.0F, -1.5F, 0.0F, 1.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -4.0F, 3.0F));

		PartDefinition saw_arms = drone.addOrReplaceChild("saw_arms", CubeListBuilder.create(), PartPose.offset(0.0F, -4.0F, -4.0F));

		PartDefinition saw_chest_2_r1 = saw_arms.addOrReplaceChild("saw_chest_2_r1", CubeListBuilder.create().texOffs(0, 28).addBox(-5.0F, -1.0F, -3.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-2.0F, 2.0F, -2.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition saw_chest_1_r1 = saw_arms.addOrReplaceChild("saw_chest_1_r1", CubeListBuilder.create().texOffs(0, 28).addBox(0.0F, -1.0F, -3.0F, 5.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, 2.0F, -2.0F, 0.0F, 0.7854F, 0.0F));

		PartDefinition saw_arm_2_r1 = saw_arms.addOrReplaceChild("saw_arm_2_r1", CubeListBuilder.create().texOffs(29, 21).addBox(0.0F, 0.0F, -1.0F, 1.0F, 6.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(29, 21).addBox(-7.0F, 0.0F, -1.0F, 1.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.0F, 0.0F, 0.0F, -1.0828F, 0.0F, 0.0F));

		PartDefinition sawblade_1 = saw_arms.addOrReplaceChild("sawblade_1", CubeListBuilder.create().texOffs(47, 17).addBox(-1.5F, -0.6F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.5F, 3.0F, -7.0F));

		PartDefinition Sageblatt2_r1 = sawblade_1.addOrReplaceChild("Sageblatt2_r1", CubeListBuilder.create().texOffs(47, 17).addBox(-1.5F, -1.0F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition sawblade_2 = saw_arms.addOrReplaceChild("sawblade_2", CubeListBuilder.create().texOffs(47, 17).addBox(-1.5F, -0.6F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(3.5F, 3.0F, -7.0F));

		PartDefinition Sageblatt4_r1 = sawblade_2.addOrReplaceChild("Sageblatt4_r1", CubeListBuilder.create().texOffs(47, 17).addBox(-1.5F, -1.0F, -1.5F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -0.7854F, 0.0F));

		PartDefinition head = drone.addOrReplaceChild("head", CubeListBuilder.create().texOffs(25, 0).addBox(-2.0F, -1.0F, -4.0F, 4.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -5.0F, -4.0F));

		PartDefinition trapdoor_1 = drone.addOrReplaceChild("trapdoor_1", CubeListBuilder.create().texOffs(0, 17).addBox(-3.0F, -0.5F, -3.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(3.0F, -2.0F, 0.0F));

		PartDefinition trapdoor_2 = drone.addOrReplaceChild("trapdoor_2", CubeListBuilder.create().texOffs(0, 17).addBox(0.0F, -0.5F, 0.0F, 3.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(-3.0F, -2.0F, -3.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		drone.render(poseStack, buffer, packedLight, packedOverlay);
	}
}