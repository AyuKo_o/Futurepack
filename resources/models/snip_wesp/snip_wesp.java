// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class snip_wesp<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "snip_wesp"), "main");
	private final ModelPart wesp;

	public snip_wesp(ModelPart root) {
		this.wesp = root.getChild("wesp");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition wesp = partdefinition.addOrReplaceChild("wesp", CubeListBuilder.create().texOffs(56, 0).addBox(-2.5F, -2.5F, -2.0F, 5.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 16.0F, -2.0F));

		PartDefinition head = wesp.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-1.5F, -2.0F, -4.0F, 3.0F, 4.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, -2.0F));

		PartDefinition fuler1 = head.addOrReplaceChild("fuler1", CubeListBuilder.create().texOffs(51, 0).addBox(-2.0F, -7.0F, -1.0F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.7436F, 0.0F, 0.0F));

		PartDefinition fuler2 = head.addOrReplaceChild("fuler2", CubeListBuilder.create().texOffs(51, 0).addBox(1.0F, -7.0F, -1.0F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.7436F, 0.0F, 0.0F));

		PartDefinition eyes = head.addOrReplaceChild("eyes", CubeListBuilder.create().texOffs(14, 0).addBox(-3.0F, -1.5F, -3.5F, 6.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition zange1 = head.addOrReplaceChild("zange1", CubeListBuilder.create().texOffs(33, 0).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.0F, 2.0F, -3.0F, 0.0F, 0.0873F, 0.0F));

		PartDefinition zange11 = zange1.addOrReplaceChild("zange11", CubeListBuilder.create().texOffs(37, 4).addBox(1.1F, 0.0F, -17.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 0.0F, 14.0F, 0.0F, 0.1309F, 0.0F));

		PartDefinition zange2 = head.addOrReplaceChild("zange2", CubeListBuilder.create().texOffs(42, 0).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.0F, 2.0F, -3.0F, 0.0F, -0.0873F, 0.0F));

		PartDefinition zange21 = zange2.addOrReplaceChild("zange21", CubeListBuilder.create().texOffs(42, 4).addBox(-1.2F, 0.0F, -4.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 1.0F, 0.0F, -0.0436F, 0.0F));

		PartDefinition Wing1 = wesp.addOrReplaceChild("Wing1", CubeListBuilder.create().texOffs(76, 0).addBox(-7.0F, 0.0F, 0.0F, 7.0F, 0.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -2.0F, 1.0F));

		PartDefinition Wing2 = wesp.addOrReplaceChild("Wing2", CubeListBuilder.create().texOffs(76, 0).addBox(-7.0F, 0.0F, 0.0F, 7.0F, 0.0F, 13.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -2.0F, 1.0F, 0.0F, 0.0F, 3.1416F));

		PartDefinition leg_l_1 = wesp.addOrReplaceChild("leg_l_1", CubeListBuilder.create(), PartPose.offsetAndRotation(2.5F, 2.5F, -2.0F, 0.0F, 0.5236F, 0.5061F));

		PartDefinition leg_l_1_r1 = leg_l_1.addOrReplaceChild("leg_l_1_r1", CubeListBuilder.create().texOffs(0, 19).addBox(-1.5F, -24.65F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, 23.5F, 0.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition shin_l_1 = leg_l_1.addOrReplaceChild("shin_l_1", CubeListBuilder.create(), PartPose.offsetAndRotation(2.0F, -0.3F, 0.0F, 0.0F, -0.2182F, 1.1345F));

		PartDefinition shin_l_1_r1 = shin_l_1.addOrReplaceChild("shin_l_1_r1", CubeListBuilder.create().texOffs(12, 19).addBox(-0.5F, -0.35F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.15F, 0.0F, 0.0F, 0.0F, -1.5708F));

		PartDefinition leg_l_2 = wesp.addOrReplaceChild("leg_l_2", CubeListBuilder.create(), PartPose.offsetAndRotation(2.5F, 2.5F, 0.0F, 0.0F, 0.0F, 0.5061F));

		PartDefinition leg_l_1_r2 = leg_l_2.addOrReplaceChild("leg_l_1_r2", CubeListBuilder.create().texOffs(0, 19).addBox(-1.5F, -24.651F, -3.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, 23.5F, -2.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition shin_l_2 = leg_l_2.addOrReplaceChild("shin_l_2", CubeListBuilder.create(), PartPose.offsetAndRotation(2.0F, -0.15F, 0.0F, 0.0F, 0.0F, 1.0908F));

		PartDefinition shin_l_2_r1 = shin_l_2.addOrReplaceChild("shin_l_2_r1", CubeListBuilder.create().texOffs(12, 19).addBox(-0.5F, -0.55F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.5708F));

		PartDefinition leg_l_3 = wesp.addOrReplaceChild("leg_l_3", CubeListBuilder.create(), PartPose.offsetAndRotation(2.5F, 2.5F, 2.0F, 0.0F, -0.5672F, 0.5061F));

		PartDefinition leg_l_1_r3 = leg_l_3.addOrReplaceChild("leg_l_1_r3", CubeListBuilder.create().texOffs(0, 19).addBox(-1.5F, -24.65F, -3.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.5F, 23.5F, -2.0F, 0.0F, 3.1416F, 0.0F));

		PartDefinition shin_l_3 = leg_l_3.addOrReplaceChild("shin_l_3", CubeListBuilder.create(), PartPose.offsetAndRotation(2.0F, -0.15F, 0.0F, 0.0F, 0.2182F, 1.0908F));

		PartDefinition shin_l_3_r1 = shin_l_3.addOrReplaceChild("shin_l_3_r1", CubeListBuilder.create().texOffs(12, 19).addBox(-0.5F, -0.55F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -1.5708F));

		PartDefinition leg_r_1 = wesp.addOrReplaceChild("leg_r_1", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.5F, 2.5F, -2.0F, 0.0F, -0.5236F, -0.5061F));

		PartDefinition leg_r_1_r1 = leg_r_1.addOrReplaceChild("leg_r_1_r1", CubeListBuilder.create().texOffs(0, 19).mirror().addBox(-1.5F, -24.65F, -1.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(-0.5F, 23.5F, 0.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition shin_r_1 = leg_r_1.addOrReplaceChild("shin_r_1", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.0F, -0.3F, 0.0F, 0.0F, 0.2182F, -1.1345F));

		PartDefinition shin_r_1_r1 = shin_r_1.addOrReplaceChild("shin_r_1_r1", CubeListBuilder.create().texOffs(12, 19).mirror().addBox(-0.5F, -0.35F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(0.0F, 0.15F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition leg_r_2 = wesp.addOrReplaceChild("leg_r_2", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.5F, 2.5F, 0.0F, 0.0F, 0.0F, -0.5061F));

		PartDefinition leg_r_2_r1 = leg_r_2.addOrReplaceChild("leg_r_2_r1", CubeListBuilder.create().texOffs(0, 19).mirror().addBox(-1.5F, -24.651F, -3.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(-0.5F, 23.5F, -2.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition shin_r_2 = leg_r_2.addOrReplaceChild("shin_r_2", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.0F, -0.15F, 0.0F, 0.0F, 0.0F, -1.0908F));

		PartDefinition shin_r_2_r1 = shin_r_2.addOrReplaceChild("shin_r_2_r1", CubeListBuilder.create().texOffs(12, 19).mirror().addBox(-0.5F, -0.55F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition leg_r_3 = wesp.addOrReplaceChild("leg_r_3", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.5F, 2.5F, 2.0F, 0.0F, 0.5672F, -0.5061F));

		PartDefinition leg_r_3_r1 = leg_r_3.addOrReplaceChild("leg_r_3_r1", CubeListBuilder.create().texOffs(0, 19).mirror().addBox(-1.5F, -24.65F, -3.0F, 3.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(-0.5F, 23.5F, -2.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition shin_r_3 = leg_r_3.addOrReplaceChild("shin_r_3", CubeListBuilder.create(), PartPose.offsetAndRotation(-2.0F, -0.15F, 0.0F, 0.0F, -0.2182F, -1.0908F));

		PartDefinition shin_r_3_r1 = shin_r_3.addOrReplaceChild("shin_r_3_r1", CubeListBuilder.create().texOffs(12, 19).mirror().addBox(-0.5F, -0.55F, -0.5F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 1.5708F));

		PartDefinition verbindung = wesp.addOrReplaceChild("verbindung", CubeListBuilder.create().texOffs(0, 10).addBox(-1.0F, -1.0F, -1.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 2.0F, -0.1115F, 0.0F, 0.0F));

		PartDefinition hinterteil = verbindung.addOrReplaceChild("hinterteil", CubeListBuilder.create().texOffs(7, 9).addBox(-2.0F, -2.0F, -0.5F, 4.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.5F, 1.0F, -0.1693F, 0.0F, 0.0F));

		PartDefinition hinterteil2 = hinterteil.addOrReplaceChild("hinterteil2", CubeListBuilder.create().texOffs(20, 9).addBox(-2.5F, -2.5F, 0.0F, 5.0F, 5.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 1.0F, -0.1222F, 0.0F, 0.0F));

		PartDefinition hinterteil3 = hinterteil2.addOrReplaceChild("hinterteil3", CubeListBuilder.create().texOffs(40, 9).addBox(-1.5F, -3.0F, 0.0F, 3.0F, 4.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.5F, 3.0F, -0.3802F, 0.0F, 0.0F));

		PartDefinition ende = hinterteil3.addOrReplaceChild("ende", CubeListBuilder.create().texOffs(54, 10).addBox(-1.0F, -1.5F, 0.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, -0.5F, 2.5F, -0.148F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 32);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		wesp.render(poseStack, buffer, packedLight, packedOverlay);
	}
}