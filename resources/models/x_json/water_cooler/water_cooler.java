// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class water_cooler<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "water_cooler"), "main");
	private final ModelPart root;

	public water_cooler(ModelPart root) {
		this.root = root.getChild("root");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition Sockel = root.addOrReplaceChild("Sockel", CubeListBuilder.create().texOffs(0, 25).addBox(-8.0F, -3.0F, -8.0F, 16.0F, 3.0F, 16.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Mittelteil = root.addOrReplaceChild("Mittelteil", CubeListBuilder.create().texOffs(0, 0).addBox(-6.0F, -15.0F, -6.0F, 12.0F, 12.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Leitung1 = root.addOrReplaceChild("Leitung1", CubeListBuilder.create().texOffs(0, 25).addBox(2.0F, -13.0F, -8.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Leitung2 = root.addOrReplaceChild("Leitung2", CubeListBuilder.create().texOffs(0, 25).addBox(-4.0F, -13.0F, -8.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Leitung3 = root.addOrReplaceChild("Leitung3", CubeListBuilder.create().texOffs(0, 25).addBox(2.0F, -13.0F, -8.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition Leitung4 = root.addOrReplaceChild("Leitung4", CubeListBuilder.create().texOffs(0, 25).addBox(-4.0F, -13.0F, -8.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition Leitung5 = root.addOrReplaceChild("Leitung5", CubeListBuilder.create().texOffs(0, 25).addBox(2.0F, -13.0F, -8.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Leitung6 = root.addOrReplaceChild("Leitung6", CubeListBuilder.create().texOffs(0, 25).addBox(-4.0F, -13.0F, -8.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Leitung7 = root.addOrReplaceChild("Leitung7", CubeListBuilder.create().texOffs(0, 25).addBox(-4.0F, -13.0F, -8.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -4.7124F, 0.0F));

		PartDefinition Leitung8 = root.addOrReplaceChild("Leitung8", CubeListBuilder.create().texOffs(0, 25).addBox(2.0F, -13.0F, -8.0F, 2.0F, 10.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -4.7124F, 0.0F));

		PartDefinition Anschluss1 = root.addOrReplaceChild("Anschluss1", CubeListBuilder.create().texOffs(48, 0).addBox(-2.0F, -10.0F, -8.0F, 4.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		PartDefinition Anschluss2 = root.addOrReplaceChild("Anschluss2", CubeListBuilder.create().texOffs(48, 0).addBox(-2.0F, -10.0F, -8.0F, 4.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -1.5708F, 0.0F));

		PartDefinition Anschluss3 = root.addOrReplaceChild("Anschluss3", CubeListBuilder.create().texOffs(48, 0).addBox(-2.0F, -10.0F, -8.0F, 4.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -3.1416F, 0.0F));

		PartDefinition Anschluss4 = root.addOrReplaceChild("Anschluss4", CubeListBuilder.create().texOffs(48, 0).addBox(-2.0F, -10.0F, -8.0F, 4.0F, 4.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, -4.7124F, 0.0F));

		PartDefinition UberdtuckOffnung = root.addOrReplaceChild("UberdtuckOffnung", CubeListBuilder.create().texOffs(0, 44).addBox(-5.0F, -16.0F, -5.0F, 10.0F, 1.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 64, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		root.render(poseStack, buffer, packedLight, packedOverlay);
	}
}