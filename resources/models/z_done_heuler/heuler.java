// Made with Blockbench 4.0.0-beta.3
// Exported for Minecraft version 1.17 with Mojang mappings
// Paste this class into your mod and generate all required imports


public class heuler<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("modid", "heuler"), "main");
	private final ModelPart heuler;

	public heuler(ModelPart root) {
		this.heuler = root.getChild("heuler");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition heuler = partdefinition.addOrReplaceChild("heuler", CubeListBuilder.create().texOffs(0, 0).addBox(-1.0F, -3.0F, 0.0F, 2.0F, 2.0F, 3.0F, new CubeDeformation(0.0F))
		.texOffs(20, 0).addBox(-2.0F, -2.5F, 0.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
		.texOffs(20, 0).addBox(1.0F, -2.5F, 0.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

		PartDefinition head = heuler.addOrReplaceChild("head", CubeListBuilder.create(), PartPose.offset(0.0F, -2.0F, 0.0F));

		PartDefinition eye_2_r1 = head.addOrReplaceChild("eye_2_r1", CubeListBuilder.create().texOffs(10, 6).addBox(0.2F, -0.5F, -2.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
		.texOffs(10, 6).addBox(-1.2F, -0.5F, -2.5F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3491F, 0.0F, 0.0F));

		PartDefinition head_r1 = head.addOrReplaceChild("head_r1", CubeListBuilder.create().texOffs(0, 6).addBox(-1.0F, -1.0F, -2.0F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.3718F, 0.0F, 0.0F));

		PartDefinition wing_1 = heuler.addOrReplaceChild("wing_1", CubeListBuilder.create().texOffs(18, 6).addBox(-5.0F, 0.0F, 0.0F, 5.0F, 0.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.5F, -2.0F, 0.0F, 0.0F, 0.3491F, 0.3491F));

		PartDefinition wing_1_1 = heuler.addOrReplaceChild("wing_1_1", CubeListBuilder.create().texOffs(18, 6).addBox(-5.0F, 0.0F, 0.0F, 5.0F, 0.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-1.5F, -2.0F, 0.0F, 0.0F, 0.3491F, -0.3491F));

		PartDefinition wing_2 = heuler.addOrReplaceChild("wing_2", CubeListBuilder.create().texOffs(35, 6).addBox(0.0F, 0.0F, 0.0F, 5.0F, 0.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.5F, -2.0F, 0.0F, 0.0F, -0.3491F, -0.3491F));

		PartDefinition wing_2_1 = heuler.addOrReplaceChild("wing_2_1", CubeListBuilder.create().texOffs(35, 6).addBox(0.0F, 0.0F, 0.0F, 5.0F, 0.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(1.5F, -2.0F, 0.0F, 0.0F, -0.3491F, 0.3491F));

		PartDefinition back = heuler.addOrReplaceChild("back", CubeListBuilder.create(), PartPose.offset(0.0F, -2.0F, 3.0F));

		PartDefinition back_r1 = back.addOrReplaceChild("back_r1", CubeListBuilder.create().texOffs(28, 0).addBox(-1.0F, -0.5F, 0.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.4461F, 0.0F, 0.0F));

		PartDefinition sting = back.addOrReplaceChild("sting", CubeListBuilder.create(), PartPose.offset(0.0F, 0.0F, 2.0F));

		PartDefinition sting_r1 = sting.addOrReplaceChild("sting_r1", CubeListBuilder.create().texOffs(37, 0).addBox(-0.5F, 0.5F, 0.0F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.8179F, 0.0F, 0.0F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {

	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		heuler.render(poseStack, buffer, packedLight, packedOverlay);
	}
}