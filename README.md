[Forum (German)](http://redsnake-games.de/forum/viewtopic.php?f=14&t=7)
[Forum (English)](http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods/2644868-futurepack-mod-discover-new-dimensions)

[![build status](https://gitlab.com/MCenderdragon/Futurepack/badges/master/build.svg)](https://gitlab.com/MCenderdragon/Futurepack/commits/master)
[![](http://cf.way2muchnoise.eu/full_futurepack_downloads.svg)](https://minecraft.curseforge.com/projects/futurepack)
[![](http://cf.way2muchnoise.eu/versions/Available%20For%20Minecraft_futurepack_all.svg)](https://minecraft.curseforge.com/projects/futurepack)

[Downloads(All)](http://redsnake-games.de/Downloads/fp/fp.php)
 [Curse](https://mods.curse.com/mc-mods/minecraft/237333-futurepack)
 [Curseforge](https://minecraft.curseforge.com/projects/futurepack)

